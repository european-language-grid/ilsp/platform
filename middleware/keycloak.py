# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Marcos Pereira <marcospereira.mpj@gmail.com>
# Modified by Sairam Krish <haisairam@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

from django.contrib.auth.models import Group
from django.db import IntegrityError
from django.http.response import JsonResponse
from django.utils.deprecation import MiddlewareMixin
from jose import ExpiredSignatureError, JWTError
from keycloak import KeycloakOpenID
from keycloak.exceptions import KeycloakInvalidTokenError
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied

from accounts.models import ELGUser
from management.models import Validator
from django.conf import settings
from registry.models import Person

LOGGER = logging.getLogger(__name__)


class KeycloakMiddleware(MiddlewareMixin):

    def __init__(self, get_response):
        """
        :param get_response:
        """

        # Create Keycloak instance
        self.keycloak = KeycloakOpenID(server_url=settings.KEYCLOAK_SERVER_URL,
                                       client_id=settings.KEYCLOAK_CLIENT,
                                       realm_name='')
        # Django
        self.get_response = get_response
        super().__init__(get_response)

    @property
    def keycloak(self):
        return self._keycloak

    @keycloak.setter
    def keycloak(self, value):
        self._keycloak = value

    @property
    def config(self):
        return self._config

    @config.setter
    def config(self, value):
        self._config = value

    @property
    def server_url(self):
        return self._server_url

    @server_url.setter
    def server_url(self, value):
        self._server_url = value

    @property
    def client_id(self):
        return self._client_id

    @client_id.setter
    def client_id(self, value):
        self._client_id = value

    @property
    def client_secret_key(self):
        return self._client_secret_key

    @client_secret_key.setter
    def client_secret_key(self, value):
        self._client_secret_key = value

    @property
    def client_public_key(self):
        return self._client_public_key

    @client_public_key.setter
    def client_public_key(self, value):
        self._client_public_key = value

    @property
    def realm(self):
        return self._realm

    @realm.setter
    def realm(self, value):
        self._realm = value

    @property
    def keycloak_authorization_config(self):
        return self._keycloak_authorization_config

    @keycloak_authorization_config.setter
    def keycloak_authorization_config(self, value):
        self._keycloak_authorization_config = value

    @property
    def method_validate_token(self):
        return self._method_validate_token

    @method_validate_token.setter
    def method_validate_token(self, value):
        self._method_validate_token = value

    def __call__(self, request):
        """
        :param request:
        :return:
        """
        return self.get_response(request)

    def _get_auth_info(self, request, token_info):
        try:
            user = ELGUser.objects.get(username=token_info.get('preferred_username'))
        except ELGUser.DoesNotExist:
            user = None
            # user.set_password(token_info.get('sub').split('-')[0])
            LOGGER.error(f'User {user} not found')

        return {'username': token_info.get('preferred_username'), 'user': user, 'groups': user.groups.all()}

    def process_view(self, request, view_func, view_args, view_kwargs):
        """
        Validate only the token introspect.
        :param request: django request
        :param view_func:
        :param view_args: view args
        :param view_kwargs: view kwargs
        :return:
        """

        try:
            if 'HTTP_AUTHORIZATION' not in request.META:
                # let django decide assuming anonymous user
                return None

            auth_header = request.META.get('HTTP_AUTHORIZATION').split()
            token = auth_header[1] if len(auth_header) == 2 else auth_header[0]
            try:
                token_info = (self.keycloak.decode_token(token, settings.KEYCLOAK_PUBLIC_KEY))
            except ExpiredSignatureError:
                return JsonResponse({
                    "detail": 'Token has expired'},
                    status=AuthenticationFailed.status_code)
            except JWTError:
                return JsonResponse({
                    "detail": 'Token verification failed'},
                    status=AuthenticationFailed.status_code)
            auth_info = self._get_auth_info(request, token_info)

            request.user = auth_info.get('user')
            if not request.user:
                return JsonResponse({'detail': f'User {auth_info.get("username")} does not exist'},
                                    status=AuthenticationFailed.status_code)
            return None

        except KeycloakInvalidTokenError as e:
            return JsonResponse({"detail": AuthenticationFailed.default_detail},
                                status=AuthenticationFailed.status_code)
