from dle.dle_report_index.documents import DLEReportDocument
from management.catalog_report_index.documents import CatalogReportDocument
from management.management_dashboard_index.documents import (MyResourcesDocument, MyValidationsDocument)
from registry.search_indexes.documents import MetadataDocument


def update_indices(instance):
    """
    Updates all indices for a given instance
    """
    MetadataDocument().catalogue_update(instance)
    DLEReportDocument().dle_report_update(instance)
    MyResourcesDocument().update(instance.management_object)
    MyValidationsDocument().update(instance.management_object)
    CatalogReportDocument().catalogue_report_update(instance)