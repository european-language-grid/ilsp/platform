from django.db.models import QuerySet

from management.models import Manager
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer


def find_related_records(instances):
    single_instance_id = []
    if isinstance(instances, Manager):
        single_instance_id.append(instances.metadata_record_of_manager.pk)
        instances = [instances]
    if isinstance(instances, QuerySet):
        instances = list(instances)
    related_pks_set = set()
    return_list = []
    for instance in instances:
        record_instance = instance.metadata_record_of_manager
        d_repr = MetadataRecordSerializer(record_instance,
                                          context={'uncensored': True}).to_display_representation(record_instance)
        reverse_relations = d_repr['described_entity']['field_value'].get('reverse_relations')
        get_reverse_related_pks(reverse_relations, related_pks_set)
    related_records = MetadataRecord.objects.filter(
        id__in=list(related_pks_set)).exclude(id__in=single_instance_id)
    for record in related_records:
        return_list.append(record.__str__() + f' (id {record.pk})')
    return return_list, related_pks_set


def get_reverse_related_pks(ordered_dict, related_pks_set):
    if ordered_dict:
        for relation in ordered_dict['field_value']:
            # Ignore Inverse relation isReplacesWith for deletion check
            if relation.get('field_label')['en'] in ['New version']:
                continue
            for rel in relation.get('field_value'):
                try:
                    if rel.get('full_metadata_record'):
                        # find the pks of the related records
                        related_pks_set.add(
                            int(rel['full_metadata_record']['field_value'].get('link', dict())['field_value'].split('/')[-2])
                        )
                except (KeyError, TypeError, ValueError):
                    pass
