import copy

from django.conf import settings

def to_elg_registry(original_image, registry_prefix):
    """
    Maps a Docker image reference to the location to which it will have been copied in the ELG registry.  The rules are as follows:

    1. Images with no slashes (image:tag) are Docker hub "library" images, so expanded with docker.io/library/ prefix
    2. Images with no dots before the first slash (user/image:tag) are in a user namespace on Docker hub, so expanded with docker.io/ prefix
    3. Images _with_ dots before the first slash are already fully qualified with a registry name, so are not expanded.
    4. Following this expansion, if any, the registry hostname is removed and the rest of the image reference is appended to the ELG registry prefix.
    """
    if "/" not in original_image:
        original_image = f"docker.io/library/{original_image}"

    registry, image = original_image.split('/', 1)
    if '.' not in registry:
        # registry isn't actually a registry, it's the username on docker hub
        registry = "docker.io"
        image = original_image
        
    # at this point registry is the registry hostname and image is everything after the first slash
    return f"{registry_prefix}{image}"

def map_docker_locations(dist):
    initial_docker_download_location = copy.deepcopy(dist.docker_download_location)
    initial_service_adapter_download_location = copy.deepcopy(dist.service_adapter_download_location)
    dist.initial_docker_download_location = initial_docker_download_location
    dist.initial_service_adapter_download_location = initial_service_adapter_download_location
    harbor_space = 'public' if not dist.private_resource else 'private'
    registry_prefix = f'{settings.ELG_DOCKER_REGISTRY}/{harbor_space}/'
    if not initial_docker_download_location.startswith(registry_prefix):
        dist.docker_download_location = to_elg_registry(initial_docker_download_location, registry_prefix)
    if initial_service_adapter_download_location and not initial_service_adapter_download_location.startswith(registry_prefix):
        dist.service_adapter_download_location = to_elg_registry(initial_service_adapter_download_location, registry_prefix)
