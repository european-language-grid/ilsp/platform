from test_utils import SerializerTestCase

from utils.string_utils import to_snake_case


class TestToSnakeCase(SerializerTestCase):

    def test_to_snake_works_as_expected(self):
        camel_case = 'CreateSnakeCase'
        self.assertEquals(to_snake_case(camel_case), 'create_snake_case')