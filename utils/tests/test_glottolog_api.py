from unittest import TestCase

import requests
from django.conf import settings

from utils.glottolog_service.requests import get_name, get_alternatives, check_glottocode, get_glottocode


class TestGlottologAPI(TestCase):

    def test_root(self):
        response = requests.get(settings.GLOTTOLOG_SERVICE_ENDPOINT)
        assert response.json().get('title') == 'Glottolog'

    def test_get_name(self):
        response = get_name('bava1246')
        assert response == 'Bavarian'

    def test_get_alternatives(self):
        response = get_alternatives('bava1246')
        expected = [
            "German (Bavarian)",
            "German (Upper Austrian)",
            "German (Viennese)",
            "Austro-Bavarian language"
        ]
        assert response == expected

    def test_check_id(self):
        response = check_glottocode('bava1246')
        assert response is True

    def test_get_glottocode(self):
        response = get_glottocode('bar')
        assert response == 'bava1246'

    def test_get_glottocode_invalid(self):
        response = get_glottocode('foo')
        assert response is None

    def test_get_invalid_name(self):
        response = get_name('foo')
        assert response is None

    def test_get_invalid_alternatives(self):
        response = get_alternatives('foo')
        assert response is None

    def test_check_invalid_id(self):
        response = check_glottocode('foo')
        assert response is False
