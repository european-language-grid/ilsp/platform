import requests

from catalogue_backend.settings.private_settings import MS_ONTOLOGY_SERVICE


def get_function_categories(function):
    return requests.get(f'{MS_ONTOLOGY_SERVICE}category/?function={function}').json()
