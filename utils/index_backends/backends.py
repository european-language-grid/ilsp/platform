import copy

from django_elasticsearch_dsl_drf.constants import MATCHING_OPTION_MUST, MATCHING_OPTIONS
from django_elasticsearch_dsl_drf.filter_backends import BaseSearchFilterBackend
from django_elasticsearch_dsl_drf.filter_backends.search.query_backends import (
    MatchPhraseQueryBackend,
    SimpleQueryStringQueryBackend
)
from elasticsearch import ImproperlyConfigured
from elasticsearch_dsl import Q


class MatchPhraseFilterBackend(BaseSearchFilterBackend):
    """Compound search backend."""

    query_backends = [
        MatchPhraseQueryBackend
    ]


class CustomQueryBackend(SimpleQueryStringQueryBackend):
    """Custom query string query backend."""

    @classmethod
    def split_lookup_name(cls, value, maxsplit=-1):
        """Split lookup value.

        :param value: Value to split.
        :param maxsplit: The `maxsplit` option of `string.split`.
        :type value: str
        :type maxsplit: int
        :return: Lookup value split into a list.
        :rtype: list
        """
        return value.split('::', maxsplit)

    @classmethod
    def construct_search(cls, request, view, search_backend):
        """Construct search.

        In case of multi match, we always look in a group of fields.
        Thus, matching per field is no longer valid use case here. However,
        we might want to have multiple fields enabled for multi match per
        view set, and only search in some of them in specific request.

        Example:

            /search/books/?search_simple_query_string=
                "fried eggs" %2B(eggplant | potato) -frittata
            /search/books/?search_simple_query_string=
                title,summary:"fried eggs" +(eggplant | potato) -frittata

        Note, that multiple searches are not supported (would not raise
        an exception, but would simply take only the first):

            /search/books/?search_simple_query_string=
                title,summary:"fried eggs" +(eggplant | potato) -frittata
                &search_simple_query_string=
                author,publisher="fried eggs" +(eggplant | potato) -frittata

        In the view-set fields shall be defined in a very simple way. The
        only accepted argument would be boost (per field).

        Example 1 (complex):

            simple_query_string_search_fields = {
                'title': {'field': 'title.english', 'boost': 4},
                'summary': {'boost': 2},
                'description': None,
            }

        Example 2 (simple list):

            simple_query_string_search_fields = (
                'title',
                'summary',
                'description',
            )

        Query examples:

            http://localhost:8000/search
                /books-simple-query-string-search-backend
                /?search_simple_query_string=%22Pool%20of%20Tears%22

            http://localhost:8000/search
                /books-simple-query-string-search-backend
                /?search_simple_query_string=%22Pool%20of%20Tears%22
                -considering

        :param request:
        :param view:
        :param search_backend:
        :return:
        """
        if hasattr(view, 'simple_query_string_search_fields'):
            view_search_fields = copy.copy(
                getattr(view, 'simple_query_string_search_fields')
            )
        else:
            view_search_fields = copy.copy(view.search_fields)

        __is_complex = isinstance(view_search_fields, dict)

        # Getting the list of search query params.
        query_params = search_backend.get_search_query_params(request)
        __queries = []
        for search_term in query_params[:1]:
            search_term = cls._clean_search_term(search_term)
            __values = cls.split_lookup_name(search_term, 1)
            __len_values = len(__values)
            __search_term = search_term

            query_fields = []

            # If we're dealing with case like
            # /search/books/?search_multi_match=title,summary:lorem ipsum
            if __len_values > 1:
                _field, value = __values
                __search_term = value
                fields = search_backend.split_lookup_complex_multiple_value(
                    _field
                )
                for field in fields:
                    if field in view_search_fields:
                        if __is_complex:
                            query_fields.append(
                                cls.get_field(field, view_search_fields[field])
                            )
                        else:
                            query_fields.append(field)

            # If it's just a simple search like
            # /search/books/?search_multi_match=lorem ipsum
            # Fields shall be defined in a very simple way.
            else:
                # It's a dict, see example 1 (complex)
                if __is_complex:
                    for field, options in view_search_fields.items():
                        query_fields.append(
                            cls.get_field(field, options)
                        )

                # It's a list, see example 2 (simple)
                else:
                    query_fields = copy.copy(view_search_fields)

            # The multi match query
            __queries.append(
                Q(
                    cls.query_type,
                    query=__search_term,
                    fields=query_fields,
                    **cls.get_query_options(request, view, search_backend)
                )
            )

        return __queries

    @classmethod
    def _clean_search_term(cls, search_term):
        special_characters = ['\\', '/', '(', ')', '[', ']', '"', '{', '}', '!', '^']
        for char in special_characters:
            if char in search_term:
                if char == '"' \
                        and search_term.count(char) % 2 == 0:
                    continue
                else:
                    search_term = search_term.replace(char, '\\' + char)
        return search_term


class LuceneStringSearchFilterBackend(BaseSearchFilterBackend):
    """Simple query string search filter backend."""

    search_param = 'search_lucene'

    matching = MATCHING_OPTION_MUST

    query_backends = [
        CustomQueryBackend,
    ]

    def filter_queryset(self, request, queryset, view):
        """Filter the queryset.

        :param request: Django REST framework request.
        :param queryset: Base queryset.
        :param view: View.
        :type request: rest_framework.request.Request
        :type queryset: elasticsearch_dsl.search.Search
        :type view: rest_framework.viewsets.ReadOnlyModelViewSet
        :return: Updated queryset.
        :rtype: elasticsearch_dsl.search.Search
        """
        if self.matching not in MATCHING_OPTIONS:
            raise ImproperlyConfigured(
                "Your `matching` value does not match the allowed matching"
                "options: {}".format(', '.join(MATCHING_OPTIONS))
            )

        __query_backends = self._get_query_backends(request, view)
        if len(__query_backends) > 1:
            __queries = []
            for query_backend in __query_backends:
                __queries.extend(
                    query_backend.construct_search(
                        request=request,
                        view=view,
                        search_backend=self
                    )
                )
            if __queries:
                queryset = queryset.query(
                    'bool',
                    **{self.matching: __queries}
                )

        elif len(__query_backends) == 1:
            __query = __query_backends[0].construct_search(
                request=request,
                view=view,
                search_backend=self
            )
            try:
                queryset = queryset.query('query_string', **{'query': __query[0].query})
            except IndexError:
                queryset = queryset.query('bool', **{self.matching: __query})
        else:
            raise ImproperlyConfigured(
                "Search filter backend shall have at least one query_backend"
                "specified either in `query_backends` property or "
                "`get_query_backends` method. Make appropriate changes to"
                "your {} class".format(self.__class__.__name__)
            )
        return queryset
