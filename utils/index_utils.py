from django_elasticsearch_dsl_drf.pagination import PageNumberPagination
from elasticsearch_dsl import (
    analyzer, normalizer, token_filter
)
from es_synonyms import load_synonyms
from rest_framework.utils.urls import replace_query_param

from registry.choices import CONDITION_OF_USE_CHOICES

# Synonym filter for LT Class Recommended CV from omtd-share OperationTaxonomy
operation_synonyms = load_synonyms('data/operation.synonyms')
operation_synonym_filter = token_filter(
    'operation_synonym_filter',  # Any name for the filter
    'synonym_graph',  # Synonym filter type
    synonyms=operation_synonyms
)

# Synonym filter for Languages
language_synonyms = load_synonyms('data/languages.synonyms')
language_synonym_filter = token_filter(
    'language_synonym_filter',  # Any name for the filter
    'synonym_graph',  # Synonym filter type
    synonyms=language_synonyms
)

# Analyzer using synonym_filters
synonym_analyzer = analyzer(
    'synonym_analyzer',
    tokenizer='standard',
    filter=[
        'lowercase',
        operation_synonym_filter,
        language_synonym_filter
    ])

# Analyzer to strip html tags
html_strip = analyzer(
    'html_strip',
    tokenizer='standard',
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)

# Lowercase normalizer
lowercase_normalizer = normalizer(
    'lowercase_normalizer',
    filter=['lowercase']
)

# Lowercase analyzer
lowercase_analyzer = analyzer(
    'lowercase_analyzer',
    tokenizer="keyword",
    filter=['lowercase'],
)

EU_LANGUAGES = [
    'Bulgarian',
    'Croatian',
    'Czech',
    'Danish',
    'Dutch',
    'English',
    'Estonian',
    'Finnish',
    'French',
    'German',
    'Hungarian',
    'Irish',
    'Italian',
    'Latvian',
    'Lithuanian',
    'Maltese',
    'Modern Greek (1453-)',
    'Polish',
    'Portuguese',
    'Romanian',
    'Slovak',
    'Slovenian',
    'Spanish',
    'Swedish'
]

OTHER_EUROPEAN = [
    "Abkhazian", "Albanian", "Aragonese", "Armenian", "Asturian", "Bavarian", "Azerbaijani", "Belarusian", "Basque",
    "Bosnian", "Breton",
    "Catalan", "Chechen", "Cornish", "Corsican", "Faroese", "Georgian", "Galician", "Icelandic", "Kazakh", "Ligurian",
    "Lombard", "Low German", "Lower Sorbian", "Luxembourgish", "Macedonian",
    "Neapolitan", "Northern Frisian", "Norwegian Nynorsk", "Northern Sami", "Norwegian", u"Norwegian BokmÃ¥l",
    "Occitan (post 1500)", "Ossetian", "Piemontese", "Russian",
    "Sami", "Sardinian", "Scots", "Scottish Gaelic", "Serbian", "Serbo-Croatian", "Sicilian", "Silesian", "Skolt Sami",
    "Swedish Sign Language", "Swiss German", "Turkish",
    "Ukrainian", "Upper Sorbian", "Venetian", "Walloon", "Welsh", "Western Frisian"
]


def map_resource_type(resource_type):
    resource_mapping = {
        'LanguageDescription': 'Language description',
        'LexicalConceptualResource': 'Lexical/Conceptual resource',
        'ToolService': 'Tool/Service'
    }
    return resource_mapping.get(resource_type, resource_type)


def map_ld_subclass(ld_subclass):
    ld_mapping = {
        'model': 'Model',
        'grammar': 'Grammar',
        'other': 'Uncategorized Language Description'
    }
    return ld_mapping.get(ld_subclass, ld_subclass)


# TODO create condition of use mapping from external file
condition_of_use_mapping = {
    CONDITION_OF_USE_CHOICES.ACADEMIC_USE_ONLY: 'research use allowed',
    CONDITION_OF_USE_CHOICES.ACADEMIC_USER: 'other specific restrictions',
    CONDITION_OF_USE_CHOICES.ATTRIBUTION: 'attribution required',
    CONDITION_OF_USE_CHOICES.EVALUATION_USE: 'other specific restrictions',
    CONDITION_OF_USE_CHOICES.NO_CONDITIONS: 'no conditions',
    CONDITION_OF_USE_CHOICES.NO_DERIVATIVES: 'derivatives not allowed',
    CONDITION_OF_USE_CHOICES.NO_REDISTRIBUTION: 'redistribution not allowed',
    CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE: 'commercial uses not allowed',
    CONDITION_OF_USE_CHOICES.OTHER: 'other specific restrictions',
    CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS: 'other specific restrictions',
    CONDITION_OF_USE_CHOICES.RESEARCH_USE: 'research use allowed',
    CONDITION_OF_USE_CHOICES.SHARE_ALIKE: 'other specific restrictions',
    CONDITION_OF_USE_CHOICES.TRAINING_USE: 'other specific restrictions',
    CONDITION_OF_USE_CHOICES.UNSPECIFIED: 'unspecified',
    CONDITION_OF_USE_CHOICES.USER_IDENTIFIED: 'other specific restrictions'
}


def map_condition_of_use(condition_of_use):
    return condition_of_use_mapping.get(condition_of_use, 'other specific restrictions')


# TODO create condition of use mapping from external file
access_rights_statement_mapping = {
    'free': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'licensed with a fee': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'licensed without a fee for all uses': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'licensed without a fee for specific uses': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'restricted': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'unspecified': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'BSD': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'CLARIN_PUB': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'CLARIN_PUB-BY-NC': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE
    ],
    'CLARIN_RES': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'CLARIN_RES-PRIV': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'Creative Commons Attribution': [CONDITION_OF_USE_CHOICES.ATTRIBUTION],
    'Creative Commons Attribution No Derivatives': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NO_DERIVATIVES
    ],
    'Creative Commons Attribution Non Commercial': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE
    ],
    'Creative Commons Attribution Non Commercial No Derivatives': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.NO_DERIVATIVES
    ],
    'Creative Commons Attribution Non Commercial Share Alike': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.SHARE_ALIKE,
    ],
    'Creative Commons Attribution Share Alike': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.SHARE_ALIKE,
    ],
    'European Union Public License': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.SHARE_ALIKE,
    ],
    'for research purposes': [CONDITION_OF_USE_CHOICES.RESEARCH_USE],
    'for research purposes: Privileged users (researchers entitled to a CLARIN authentication) log in via https://onderzoek.zoeken.fame.frl/. For them the raw audio and speech recognition results are available for download.': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'Free for non-commercial academic research, access via researchers only': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'free for non-commercial educational, teaching or research purposes; request required': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'GNU': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'GNU Free Documentation License': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'GNU General Public License': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'GNU Lesser General Public License': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'GNU Library General Public License': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'internal use only at the moment': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'Non Commercial Use, Research': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE
    ],
    'Non-standard/ Other Licence/ Terms': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'not publicly available': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'under negotiation': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'under review': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'unknown': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'Copyright of DCU': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'Mozilla Public License': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'Unrestricted Use': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'CLARIN_ACA': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'CLARIN_ACA-NC': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'public domain': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'Copyright of Võro Instituut, free for non commercial use': [
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'for research purposes: CLARIN users log in via https://onderzoek.zoeken.fame.frl/. Raw audio and speech recognition results available for download.': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
}


def get_access_right_map_insensitive_key_value(input_dict, key):
    return next((value for dict_key, value in input_dict.items() if dict_key.strip().lower() == key.strip().lower()),
                [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS])


def map_access_rights_statement(access_rights):
    conditions_of_use = get_access_right_map_insensitive_key_value(access_rights_statement_mapping, access_rights)
    if conditions_of_use is None:
        return None
    conditions_of_use_grouping = set()
    for c in conditions_of_use:
        mapped_condition = map_condition_of_use(c)
        if mapped_condition:
            conditions_of_use_grouping.add(mapped_condition)

    return conditions_of_use_grouping


class CataloguePageNumberPagination(PageNumberPagination):

    def get_next_link(self):
        if not self.page.has_next():
            return None
        url = self.request.build_absolute_uri()
        page_number = self.page.next_page_number()
        page_size = self.get_page_size(self.request)
        if 10000 / (page_number * page_size) <= 1:
            return None
        return replace_query_param(url, self.page_query_param, page_number)