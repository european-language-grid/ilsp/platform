from urllib.parse import unquote, quote

from django.core.serializers.json import DjangoJSONEncoder
from django.utils import encoding
from django.utils.encoding import force_text
from django.utils.functional import Promise


def encode_iri_to_uri(data, property_name):
    """
    :param data:
    :param property_name:
    :return: update data[property_name] to contain string encoded to uri
    """
    if property_name in data:
        if isinstance(data[property_name], list):
            new_data_property = []
            for item in data[property_name]:
                new_data_property.append(quote(unquote(encoding.iri_to_uri(item))).replace('%3A', ':'))
            data[property_name] = new_data_property
        elif isinstance(data[property_name], str) and len(data[property_name]) > 0:
            data[property_name] = quote(unquote(encoding.iri_to_uri(data[property_name]))).replace('%3A', ':')
    return data


class LazyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Promise):
            return force_text(obj)
        return super(LazyEncoder, self).default(obj)
