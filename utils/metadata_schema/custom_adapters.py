from drf_auto_endpoint.adapters import BaseAdapter, MetaDataInfo, GETTER


class JsonAdapter(BaseAdapter):
    metadata_info = [
        MetaDataInfo('fields', GETTER, [])
    ]

    @classmethod
    def adapt_field(cls, field):
        keys_to_delete = ('related_endpoint',
                          'choices',
                          'recommended_choices')
        if 'choices' in field:
            field['enum'] = [x['value'] for x in field['choices']]
            field['enumNames'] = [x['label'] for x in field['choices']]

        if 'recommended_choices' in field:
            field['recommended_enum'] = [x['value'] for x in field['recommended_choices']]
            field['recommended_enumNames'] = [x['display_name'] for x in field['recommended_choices']]

        for k in keys_to_delete:
            field.pop(k, None)
        return field

    def adapt_child(self, children_dict, lookup_type):
        child = {
            'type': children_dict.get('type'),
            'read_only': children_dict.get('read_only'),
            'ui': {
                'label': children_dict.get('label')
            },
            'validation': {
                'required': children_dict.get('required')
            }
        }
        if child['type'] in lookup_type:
            child['type'] = lookup_type[child['type']]
        if children_dict.get('help_text'):
            child['ui']['help'] = children_dict['help_text']
        if children_dict.get('recommended'):
            child['validation']['recommended'] = children_dict['recommended']
        if children_dict.get('max_length'):
            child['validation']['max'] = children_dict['max_length']
        if children_dict.get('choices'):
            #child['choices'] = children_dict['choices']
            child['enum'] = [x.get('value') for x in children_dict['choices']]
            child['enumNames'] = [x.get('display_name') for x in children_dict['choices']]
        if children_dict.get('recommended_choices'):
            #child['recommended_choices'] = children_dict['recommended_choices']
            child['recommended_enum'] = [x.get('value') for x in children_dict['recommended_choices']]
            child['recommended_enumNames'] = [x.get('display_name') for x in children_dict['recommended_choices']]
        if children_dict.get('child'):
            child['child'] = self.adapt_child(children_dict['child'], lookup_type)
        if children_dict.get('children'):
            child['children'] = self.adapt_children(children_dict['children'], lookup_type)
        return child

    def adapt_children(self, children_dict, lookup_type):
        return_dict = {}
        for i in children_dict.keys():
            children = {
                'type': children_dict[i].get('type'),
                'read_only': children_dict[i].get('read_only'),
                'ui': {
                    'label': children_dict[i].get('label')
                },
                'validation': {
                    'required': children_dict[i].get('required')
                }
            }
            if children['type'] in lookup_type:
                children['type'] = lookup_type[children['type']]
            if children_dict[i].get('help_text'):
                children['ui']['help'] = children_dict[i]['help_text']
            if children_dict[i].get('recommended'):
                children['validation']['recommended'] = children_dict[i]['recommended']
            if children_dict[i].get('max_length'):
                children['validation']['max'] = children_dict[i]['max_length']
            if children_dict[i].get('choices'):
                #children['choices'] = children_dict[i]['choices']
                children['choices'] = [x.get('value') for x in children_dict[i]['choices']]
                children['enum'] = [x.get('display_name') for x in children_dict[i]['choices']]
            if children_dict[i].get('recommended_choices'):
                #children['recommended_choices'] = children_dict[i]['recommended_choices']
                children['recommended_choices'] = [x.get('value') for x in children_dict[i]['recommended_choices']]
                children['recommended_enum'] = [x.get('display_name') for x in children_dict[i]['recommended_choices']]
            if children_dict[i].get('child'):
                children['child'] = self.adapt_child(children_dict[i]['child'], lookup_type)
            if children_dict[i].get('children'):
                children['children'] = self.adapt_children(children_dict[i]['children'], lookup_type)
            return_dict[i] = children
        return return_dict

    def render_dict(self, config):
        lookup_dict = {
            'field': 'object',
            'nested object': 'object',
            'list': 'array'
        }
        fields = config['fields']
        initial_fields = config['actions']['POST']
        adapted = {}
        for field in fields:
            adapted[field['key']] = self.adapt_field(field)
            if initial_fields[field['key']]['type'] not in ['field', 'nested object', 'list']:
                adapted[field['key']]['type'] = initial_fields[field['key']]['type']
            else:
                adapted[field['key']]['type'] = lookup_dict[initial_fields[field['key']]['type']]
            if initial_fields[field['key']].get('child', None):
                adapted[field['key']]['child'] = self.adapt_child(initial_fields[field['key']]['child'], lookup_dict)
                if 'children' in adapted[field['key']]['child']:
                    # for k, v in adapted[field['key']]['child']['children'].items():
                    adapted[field['key']]['child']['required'] = [k for k, v in adapted[field['key']]['child']['children'].items()
                                                                  if v['validation'].get('required') is True]
            elif initial_fields[field['key']].get('children', None):
                adapted[field['key']]['children'] = self.adapt_children(initial_fields[field['key']]['children'],
                                                                        lookup_dict)
                adapted[field['key']]['required'] = [k for k, v in adapted[field['key']]['children'].items()
                                                              if v['validation'].get('required') is True]

            field.pop('key')

        return adapted

    def render(self, config):
        rendered_dict = self.render_dict(config)
        required = []
        for k, v in rendered_dict.items():
            if v['validation'].get('required'):
                required.append(k)
                v['validation'].pop('required')
        config['fields'] = {
            #'$id':'id',
            #'$schema': 'http://json-schema.org/draft-08/schema#',
            'title': config['name'],
            'description': config['description'],
            'type': 'object',
            'required': required,
            'properties': rendered_dict
        }
        return config