import requests

from catalogue_backend.settings.private_settings import GLOTTOLOG_SERVICE_ENDPOINT


def _graphql(query):
    return requests.post(f'{GLOTTOLOG_SERVICE_ENDPOINT}graphql', json={'query': query}).json()


def get_name(glottocode):
    q = f'{{languoid(glottocode: "{glottocode}"){{name}}}}'
    try:
        return _graphql(q).get('data').get('languoid').get('name')
    except AttributeError:
        return None


def get_glottocode(iso_code):
    q = f'{{languoid(iso: "{iso_code}"){{glottocode}}}}'
    try:
        return _graphql(q).get('data').get('languoid').get('glottocode')
    except AttributeError:
        return None


def check_glottocode(glottocode):
    q = f'{{check(glottocode: "{glottocode}")}}'
    return _graphql(q).get('data').get('check')


def get_alternatives(glottocode):
    q = f'{{languoid(glottocode: "{glottocode}"){{alternativeNamesPruned}}}}'
    try:
        return _graphql(q).get('data').get('languoid').get('alternativeNamesPruned')
    except AttributeError:
        return None
