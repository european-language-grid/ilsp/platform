import logging
from collections import (
    OrderedDict, Mapping
)

from django.core.exceptions import ValidationError as DjangoValidationError
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ParseError
from rest_framework.fields import (  # NOQA # isort:skip
    SkipField, empty, BooleanField
)
from rest_framework.fields import get_error_detail
from rest_framework.fields import (
    set_value, ListField, JSONField,
    CharField
)
from rest_framework.relations import (
    ManyRelatedField, PKOnlyObject
)
from rest_framework.serializers import (
    Serializer, ModelSerializer,
    ListSerializer, ValidationError
)
from rest_framework.settings import api_settings
from rest_polymorphic.serializers import PolymorphicSerializer

LOGGER = logging.getLogger(__name__)


class XMLSerializer(Serializer):
    """
    An representation of OrderedDict used for xml serialization only
    """

    def __init__(self, instance=None, data=empty, **kwargs):
        self.xml_data = kwargs.pop('xml_data', None)
        self.add_id = kwargs.pop('add_id', False)
        super().__init__(instance, data, **kwargs)
        if data is empty and self.xml_data:
            self.initial_data = self.from_xml_representation()

    def to_xml_representation(self, instance=None, add_id=False):
        if instance is None:
            instance = self.instance
        LOGGER.info('XML serialization of model {} instance #{}'.format(self.Meta.model, instance.pk))
        representation = OrderedDict()
        add_id_condition = self.add_id or add_id
        if add_id_condition:
            representation['@id'] = instance.pk
        if isinstance(self, PolymorphicSerializer):
            representation[instance.schema_polymorphism_to_xml[instance._meta.object_name]] = self.model_serializer_mapping[
                type(instance)].to_xml_representation(instance, add_id=add_id_condition)
        else:

            fields = self._readable_fields
            for field in fields:
                try:
                    attribute = field.get_attribute(instance)
                except SkipField:
                    continue

                # Ignore field not in the map to xml
                if field.field_name not in self.Meta.model.schema_fields:
                    continue
                xml_field_name = self.Meta.model.schema_fields[field.field_name]

                # For related fields with `use_pk_only_optimization` we need to
                # resolve the pk value.
                check_for_none = attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
                # Ignore  `None` values
                if check_for_none is not None:
                    # if field is an inner model
                    if isinstance(field, ModelSerializer):
                        LOGGER.debug(
                            'Field {} maps to {} and is {}'.format(field.field_name, xml_field_name, type(field)))
                        representation[xml_field_name] = field.to_xml_representation(instance=attribute, add_id=add_id_condition)
                    # if field is a list of inner models
                    elif isinstance(field, ListSerializer) and \
                            isinstance(field.child, ModelSerializer):
                        representation[xml_field_name] = []
                        LOGGER.debug(
                            'Field {} maps to {} and is {} with children {}'.format(field.field_name, xml_field_name,
                                                                                    type(field), type(field.child)))
                        for child in attribute.all():
                            representation[xml_field_name].append(
                                field.child.to_xml_representation(instance=child, add_id=add_id_condition))
                    # if field is a boolean field
                    elif isinstance(field, BooleanField):
                        LOGGER.debug('Field {} maps to {}'.format(field.field_name, xml_field_name))
                        representation[xml_field_name] = str(field.to_representation(attribute)).lower()
                    # if field is a list of JSONFields, aka multiple Multilingual field
                    elif isinstance(field, ListField) and isinstance(field.child, JSONField):
                        LOGGER.debug('Field {} maps to {}'.format(field.field_name, xml_field_name))
                        representation[xml_field_name] = list()
                        for attr in attribute:
                            for key in attr.keys():
                                tmp_repr = dict()
                                tmp_repr['@xml:lang'] = key
                                tmp_repr['#text'] = attr[key]
                                representation[xml_field_name].append(tmp_repr)
                    # if field is JSONField, aka Multilingual field
                    elif isinstance(field, JSONField):
                        LOGGER.debug('Field {} maps to {}'.format(field.field_name, xml_field_name))
                        representation[xml_field_name] = list()
                        for key in attribute.keys():
                            tmp_repr = dict()
                            tmp_repr['@xml:lang'] = key
                            tmp_repr['#text'] = attribute[key]
                            representation[xml_field_name].append(tmp_repr)
                    # if field is a list of string or literal fields
                    else:
                        LOGGER.info('Field {} maps to {}'.format(field.field_name, xml_field_name))
                        field_repr = field.to_representation(attribute)
                        if isinstance(field_repr, str) and field_repr == '':
                            field_repr = None
                        if isinstance(field_repr, list):
                            field_repr = [repr for repr in field_repr if repr != '']
                            if len(field_repr) == 0:
                                field_repr = None
                        if field_repr is not None:
                            representation[xml_field_name] = field_repr

        return representation

    def from_xml_representation(self, xml_data=empty):
        # LOGGER.info('XML deserialization of model {}'.format(self.Meta.model))
        # Either parameter xml_data is empty or self.xml_data is not None
        assert xml_data is not empty or (
                    hasattr(self, 'xml_data') and self.xml_data is not None), (
            'Either parameter xml_data must be not empty or '
            ' self.xml_data exists and is not None'
        )

        # Set the self.xml_data to parameter xml_data
        # in order to recursive work the XML error override
        if xml_data is not empty:
            self.xml_data = xml_data
        # else get xml_data from self
        else:
            xml_data = self.xml_data

        if not isinstance(xml_data, Mapping):
            # LOGGER.error(
            #    'xml_data is not of type Mapping, but of type {}'.format(
            #        type(xml_data)))
            message = self.error_messages['invalid'].format(
                datatype=type(xml_data).__name__
            )
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            }, code='invalid')

        ret = OrderedDict()
        errors = OrderedDict()
        fields = self._writable_fields

        for field in fields:
            try:
                xml_field_name = self.Meta.model.schema_fields[field.field_name]
                if xml_field_name in xml_data:
                    field_data = xml_data[xml_field_name]
                else:
                    field_data = None

                if isinstance(field, ModelSerializer):
                    # LOGGER.debug(
                    #    'Model - XML field {} maps to {} which is {}'.format(
                    #        xml_field_name, field.field_name,
                    #        type(field)))
                    if field_data:
                        d = field.from_xml_representation(field_data)
                    else:
                        d = None
                elif isinstance(field, ListSerializer) and \
                        isinstance(field.child, ModelSerializer):
                    # LOGGER.debug(
                    #    'XML field {} maps to {} which is {} and has children {}'.format(
                    #        xml_field_name,
                    #        field.field_name, type(field),
                    #        type(field.child)))
                    d = []
                    if field_data:
                        if isinstance(field_data, list):
                            for child in field_data:
                                child_d = field.child.from_xml_representation(
                                    child)
                                d.append(child_d)
                        else:
                            child_d = field.child.from_xml_representation(
                                field_data)
                            d.append(child_d)
                elif isinstance(field, ListField):
                    # LOGGER.debug(
                    #    'XML field {} maps to {} which is {} with child {}'.format(
                    #        xml_field_name, field.field_name,
                    #        type(field), type(field.child)))
                    d = []
                    if field_data:
                        if isinstance(field_data, list):
                            for child in field_data:
                                if isinstance(field.child, JSONField):
                                    child_d = dict()
                                    child_d[child['@xml:lang']] = child['#text']
                                    d.append(child_d)
                                else:
                                    d.append(child)
                        elif isinstance(field_data, OrderedDict) and isinstance(
                                field.child, JSONField):
                            child_d = dict()
                            child_d[field_data['@xml:lang']] = field_data[
                                '#text']
                            d.append(child_d)
                        else:
                            d.append(field_data)
                    else:
                        d = field_data
                elif isinstance(field, ManyRelatedField):
                    # LOGGER.debug(
                    #    'XML field {} maps to {} which is {}'.format(
                    #        xml_field_name, field.field_name, type(field)))
                    if field_data:
                        d = field_data
                    else:
                        d = []
                elif isinstance(field, JSONField):
                    # LOGGER.debug(
                    #    'JSONField - XML field {} maps to {} which is {}'.format(
                    #        xml_field_name,
                    #        field.field_name, type(field)))
                    if field_data is not None:
                        d = dict()
                        if isinstance(field_data, list):
                            for f_data in field_data:
                                d[f_data['@xml:lang']] = f_data['#text']
                        elif isinstance(field_data, OrderedDict):
                            d[field_data['@xml:lang']] = field_data['#text']
                        else:
                            raise ValidationError(
                                '{} is not of valid form'.format(
                                    xml_field_name))
                    else:
                        d = None
                elif isinstance(field, CharField):
                    # LOGGER.debug('CharField - XML field {} maps to {}'.format(
                    #    xml_field_name, field.field_name))
                    if field_data:
                        d = field_data
                    else:
                        d = ''
                else:
                    # LOGGER.debug(type(field))
                    # LOGGER.debug(
                    #    'Rest - XML field {} maps to {}'.format(xml_field_name,
                    #                                            field.field_name))
                    d = field_data

                set_value(ret, field.source_attrs, d)

            except KeyError as e:
                # LOGGER.exception('{} does not a xml equivalence'.format(field.field_name), exc_info=True)
                # LOGGER.info('Field {} does not a xml equivalence'.format(field.field_name))
                pass

        if errors:
            raise ValidationError(errors)

        return ret

    def to_internal_value(self, data):
        """
        Dict of native values <- Dict of primitive datatypes.
        """
        # When to_internal_value is called from_xml_representation
        # Recursive accordingly and override errors to XML names
        if self.xml_data:
            if not isinstance(data, Mapping):
                message = self.error_messages['invalid'].format(
                    datatype=type(data).__name__
                )
                raise ValidationError({
                    api_settings.NON_FIELD_ERRORS_KEY: [message]
                }, code='invalid')

            ret = OrderedDict()
            errors = OrderedDict()
            fields = self._writable_fields

            for field in fields:
                validate_method = getattr(self, 'validate_' + field.field_name,
                                          None)
                primitive_value = field.get_value(data)
                try:
                    if isinstance(field,
                                  PolymorphicSerializer) and primitive_value is None and field.allow_null is True:
                        validated_value = None
                    elif isinstance(field, XMLSerializer):
                        validated_value = field.run_validation(
                            primitive_value)
                    elif isinstance(field, ListSerializer) and isinstance(
                            field.child, XMLSerializer):
                        validated_value = []
                        list_errors = []
                        if not isinstance(primitive_value, list):
                            message = field.error_messages['not_a_list'].format(
                                input_type=type(primitive_value).__name__
                            )
                            list_errors.append(
                                {api_settings.NON_FIELD_ERRORS_KEY: [message]})

                        if not field.allow_empty and len(primitive_value) == 0:
                            message = field.error_messages['empty']
                            list_errors.append(
                                {api_settings.NON_FIELD_ERRORS_KEY: [message]})

                        if len(list_errors) == 0:
                            for item in primitive_value:
                                try:
                                    validated_item = field.child.run_validation(
                                        item)
                                except ValidationError as exc:
                                    list_errors.append(exc.detail)
                                else:
                                    validated_value.append(validated_item)
                        if len(list_errors) != 0:
                            errors[field.field_name] = list_errors
                    else:
                        validated_value = field.run_validation(primitive_value)
                    if validate_method is not None:
                        validated_value = validate_method(validated_value)
                except ValidationError as exc:
                    errors[field.field_name] = exc.detail
                except DjangoValidationError as exc:
                    errors[field.field_name] = get_error_detail(exc)
                except SkipField:
                    pass
                else:
                    set_value(ret, field.source_attrs, validated_value)

            # override errors to provide xml names
            if errors:
                fields_names = list(errors.keys()).copy()
                for field_name in fields_names:
                    errors[
                        self.Meta.model.schema_fields[field_name]] = errors.pop(
                        field_name)

            if errors:
                raise ValidationError(errors)
        else:
            ret = super().to_internal_value(data)
        return ret

    def to_xml_representation_for_recommended_cv(self, xml_recommended_field, recommended_choice,
                                                 xml_name_recommended, xml_name_other):
        if xml_recommended_field is None:
            return xml_recommended_field
        if isinstance(xml_recommended_field, list):
            xml_recommended_field_new = []
            for element in xml_recommended_field:
                if element in recommended_choice:
                    xml_recommended_field_new.append({xml_name_recommended: element})
                else:
                    xml_recommended_field_new.append({xml_name_other: element})
        else:
            if xml_recommended_field in recommended_choice:
                xml_recommended_field_new = {xml_name_recommended: xml_recommended_field}
            else:
                xml_recommended_field_new = {xml_name_other: xml_recommended_field}
        return xml_recommended_field_new

    def from_xml_representation_for_recommended_cv(self, xml_data_field, field,
                                                   recommended_choice,
                                                   xml_name_recommended, xml_name_other):
        xml_data_field_new = []
        parse_errors = []
        if xml_data_field is None:
            return []
        if isinstance(xml_data_field, list):
            for element in xml_data_field:
                keys = element.keys()
                for key in keys:
                    if key == xml_name_recommended:
                        if element[key] in recommended_choice:
                            xml_data_field_new.append(element[key])
                        else:
                            # Throw Validation Error
                            message = _(
                                "{} is not a valid value in {}".format(
                                    element[key], xml_name_recommended))
                            parse_errors.append(message)
                    elif key == xml_name_other:
                        xml_data_field_new.append(element[key])
                    else:
                        # Throw Validation Error
                        message = _(
                            "{} is neither {} nor {}".format(key, xml_name_recommended, xml_name_other))
                        parse_errors.append(message)
        else:
            key = list(xml_data_field.keys())[0]
            if key == xml_name_recommended:
                if xml_data_field[key] in recommended_choice:
                    xml_data_field_new = xml_data_field[key]
                else:
                    # Throw Validation Error
                    message = _(
                        "{} is not a valid value in {}".format(
                            xml_data_field[key], xml_name_recommended))
                    parse_errors.append(message)
            elif key == xml_name_other:
                xml_data_field_new = xml_data_field[key]
            else:
                # Throw Validation Error
                message = _(
                    "{} is neither {} nor {}".format(key, xml_name_recommended, xml_name_other))
                parse_errors.append(message)
        if parse_errors:
            raise ParseError(
                {self.Meta.model.schema_fields[field]: parse_errors},
                code='invalid')
        return xml_data_field_new


class XMLPolymorphicSerializer(XMLSerializer):
    """
    An representation of OrderedDict used for xml serialization only
    of PolymorphicSerializers
    """

    def from_xml_representation(self, xml_data):
        # LOGGER.info('XML deserialization of model {}'.format(self.Meta.model))
        ret = OrderedDict()
        keys = xml_data.keys()
        for key in keys:
            model_name = self.Meta.model.schema_polymorphism_from_xml[key]
            model = self.resource_type_model_mapping[model_name]
            serializer = self.model_serializer_mapping[model]
            # LOGGER.info('{} override from_xml_representation to retrieve proper subclass {}'.format(self.Meta, model))
            ret = serializer.from_xml_representation(xml_data=xml_data[key])
            ret[self.resource_type_field_name] = self.to_resource_type(model)
            return ret
        return ret

    def to_internal_value(self, data):
        resource_type = self._get_resource_type_from_mapping(data)
        serializer = self._get_serializer_from_resource_type(resource_type)

        ret = serializer.to_internal_value(data)
        ret[self.resource_type_field_name] = resource_type
        return ret


