import copy
import io
import json
import logging
import requests
import zipfile
from pyexpat import ExpatError

import xmltodict
from datacite import schema43
from language_tags import tags
from lxml import etree
from rest_framework import (
    viewsets, status, generics
)
from rest_framework.decorators import (
    api_view, permission_classes, action
)
from rest_framework.exceptions import (
    UnsupportedMediaType, MethodNotAllowed
)
from rest_framework.generics import (
    CreateAPIView, RetrieveAPIView
)
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import (
    IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
)
from rest_framework.response import Response
from rolepermissions.checkers import has_role

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

from catalogue_backend.negotiation import IgnoreClientContentNegotiation
from email_notifications.tasks import curator_content_manager_notifications
from management.catalog_report_index.documents import CatalogReportDocument
from management.management_dashboard_index.documents import MyResourcesDocument, MyValidationsDocument
from management.models import (
    Manager, INTERNAL, DRAFT, PUBLISHED, INGESTED
)
from management.serializers import ManagerSerializer
from registry import view_tasks
from registry.models import MetadataRecord
from registry.permissions import (
    CreateMetadataRecordPermission,
    RetrieveMetadataRecordPermission,
    UpdateMetadataRecordPermission,
    ExportXMLRecordPermission,
    BatchExportXMLRecordPermission
)
from registry.registry_identifier import get_registry_identifier
from registry.search_indexes.documents import MetadataDocument
from registry.serializers import (
    MetadataRecordSerializer, GenericPersonSerializer
)
from registry.serializers_datacite import DataCiteMetadataRecordSerializer
from registry.serializers_tombstone import TombstoneMetadataRecordSerializer
from registry.view_tasks import instance_to_xml_string, compute_dle_score, xml_to_rdf
from registry.utils.checks import is_registered_service, queried_by_registered_service, is_tool_service, \
    return_uncensored_display
from registry.utils.retrieval_utils import get_lt_service_info
from registry.utils.task_utils import return_metadatarecord_landing_page

LOGGER = logging.getLogger(__name__)

ACTION_NOT_ALLOWED = {
    "detail": "You do not have permission to perform this action."}


def create_metadata_record(request, request_user, data, context=dict(),
                           xml=False, record_status=INTERNAL):
    """
    Create a MetadataRecord
    If xml=True, in data parameter provide the OrderedDict from xml
    """
    # Create metadata information
    # instantiate the person linked to the request user
    if context is None:
        context = dict()
    # Handle functional service header for metadata-record serializers
    if request.headers.get('functional-service') in ['True', 'true']:
        context.update({'functional_service': True})

    # Handle service compliant datasets
    if request.headers.get('service-compliant-dataset') in ['True', 'true']:
        context.update({'service_compliant_dataset': True})

    # Add request user to serializer context
    context.update({
        'request_user': request.user
    })
    if request_user.person:
        request_person = GenericPersonSerializer(
            instance=request_user.person
        )
    else:
        request_person = GenericPersonSerializer(
            data={
                'given_name': {'en': request_user.username},
                'surname': {'en': request_user.username}
                # 'email': [request.user.email]
            }
        )
        request_person.is_valid()
        request_person.save()
    # Handle xml_upload_with_data header for metadata-record serializers
    xml_upload_with_data = False
    if xml:
        if request.headers.get('xml-upload-with-data') in ['True', 'true']:
            xml_upload_with_data = True
            context.update({'xml_upload_with_data': True})
        # XML to internal OrderedDict format
        serializer = MetadataRecordSerializer(xml_data=data, context=context)
        data = serializer.initial_data

    # Assign request.user to metadata_curator
    data['metadata_curator'] = [request_person.data]
    # TODO: remove as harvest does not hit the endpoint
    # Add request user to metadata_creator if not from internal harvesting
    if 'harvest' not in request.query_params:
        data['metadata_creator'] = request_person.data
    # Clarin - Assign request.user.repository as source_of_metadata_record
    # data['source_of_metadata_record'] = GenericRepositorySerializer(
    #         instance=request_user.repository
    # ).data

    #
    # Create management_object
    data['management_object'] = dict()
    # Assign request.user as curator
    data['management_object']['curator'] = request_user.pk
    # Assign status
    if xml_upload_with_data:
        # set status as draft manually if xml upload with data is true
        # all validations (except for specific assigned ones) still happen
        data['management_object']['status'] = DRAFT
    else:
        data['management_object']['status'] = record_status
    # Handle under construction header
    if request.headers.get('under-construction', None) in ['True', 'true'] and \
            data['described_entity']['entity_type'] == 'LanguageResource':
        data['management_object']['under_construction'] = True
        context.update({'under_construction': True})
    # Handle functional service header
    if request.headers.get('functional-service', None) in ['True', 'true'] and \
            (data['described_entity']['entity_type'] == 'LanguageResource' and
             data['described_entity']['lr_subclass']['lr_type'] == 'ToolService'):
        data['management_object']['functional_service'] = True
    # Handle service compliant dataset header
    if request.headers.get('service-compliant-dataset') in ['True', 'true'] and \
            (data['described_entity']['entity_type'] == 'LanguageResource' and
             data['described_entity']['lr_subclass']['lr_type'] in ['LexicalConceptualResource', 'Corpus']):
        data['management_object']['service_compliant_dataset'] = True
    if not xml:
        serializer = MetadataRecordSerializer(data=data, context=context)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    return serializer


def update_management_object(request, instance, record_status=INTERNAL):
    """
    Update management_object
    """
    management_serializer = ManagerSerializer(instance=instance.management_object).data
    # Assign status
    management_serializer['status'] = record_status
    # handle under construction header
    if request.headers.get('under-construction', None) is not None \
            and instance.described_entity.entity_type == 'LanguageResource':
        u_c = True if request.headers.get('under-construction') in ['True', 'true'] else False
        if instance.management_object.under_construction != u_c:
            management_serializer['under_construction'] = u_c

    # handle functional service header
    if request.headers.get('functional-service', None) is not None \
            and (instance.described_entity.entity_type == 'LanguageResource'
                 and (instance.described_entity.lr_subclass.lr_type == 'ToolService')):
        f_s = True if request.headers.get('functional-service') in ['True', 'true'] else False
        if instance.management_object.functional_service != f_s:
            management_serializer['functional_service'] = f_s

    # Handle service compliant dataset header
    if request.headers.get('service-compliant-dataset', None) is not None \
            and (instance.described_entity.entity_type == 'LanguageResource'
                 and (instance.described_entity.lr_subclass.lr_type in ['LexicalConceptualResource', 'Corpus'])):
        s_c_d = True if request.headers.get('service-compliant-dataset') in ['True', 'true'] else False
        if instance.management_object.service_compliant_dataset != s_c_d:
            management_serializer['service_compliant_dataset'] = s_c_d

    return management_serializer


class MetadataRecordViewSet(viewsets.ModelViewSet):
    """ViewSet for the MetadataRecord class"""
    serializer_class = MetadataRecordSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            records = MetadataRecord.objects.all()
        else:
            # Return 404 for deleted and never published records
            records = MetadataRecord.objects.exclude(
                management_object__deleted=True, management_object__publication_date__isnull=True
            )
        return records

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        updated_permission_classes = [IsAuthenticatedOrReadOnly]
        if self.action == 'create':
            updated_permission_classes.append(CreateMetadataRecordPermission)
        elif self.action == 'retrieve':
            updated_permission_classes.append(RetrieveMetadataRecordPermission)
        elif self.action == 'update' or self.action == 'partial_update':
            updated_permission_classes.append(UpdateMetadataRecordPermission)
        return [permission() for permission in updated_permission_classes]

    @csrf_exempt
    def create(self, request, *args, **kwargs):
        if not isinstance(request.data, dict):
            return Response(
                {'File type error': 'Please import json only.'},
                status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
            )
        record_status = (DRAFT if 'draft' in request.query_params else INTERNAL)
        context = super().get_serializer_context()
        context.update({
            'status': record_status,
            'draft': (True if record_status == DRAFT else False),
        })
        serializer = create_metadata_record(
            request, request.user, request.data,
            context=context, record_status=record_status
        )
        # update published relations of record
        serializer.instance.management_object.update_landing_pages_of_related_records(request_user=request.user)
        # add record to dashboard index
        MyResourcesDocument().update(serializer.instance.management_object, action='index')
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers
        )

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        context = super().get_serializer_context()
        context.update({
            'request': request,
        })
        display_uncensored = return_uncensored_display(instance, request.user)
        if display_uncensored:
            context.update({
                'uncensored': True
            })
        # Is tombone checking
        full_access = request.user.is_superuser or has_role(request.user, 'content_manager')
        own_record = (
                has_role(request.user, 'provider') and instance.management_object.curator == request.user
        )
        is_validator = (
                               request.user in [instance.management_object.legal_validator,
                                        instance.management_object.metadata_validator,
                                        instance.management_object.technical_validator] and instance.management_object.status == INGESTED
        )
        is_tombstone = (
                (
                        instance.management_object.deleted
                        and instance.management_object.publication_date
                        and not full_access
                )
                or (
                        instance.management_object.status != PUBLISHED
                        and instance.management_object.publication_date
                        and not (full_access or own_record or is_validator)
                )
        )

        serializer = self.serializer_class(instance, context=context)
        if 'display' in request.GET:
            if instance.management_object.status == PUBLISHED and not instance.management_object.deleted:
                if instance.management_object.landing_page_display:
                    data = return_metadatarecord_landing_page(instance, display_uncensored)
                else:
                    data = dict(serializer.to_display_representation(instance))
            elif is_tombstone:
                # Display tombstone page
                data = dict(
                    TombstoneMetadataRecordSerializer(instance=instance, context=context)
                    .to_display_representation(instance)
                )
            else:
                data = dict(serializer.to_display_representation(instance))
        else:
            if is_tombstone:
                data = dict(TombstoneMetadataRecordSerializer(instance=instance, context=context).data)
            else:
                data = dict(serializer.data)
        try:
            if is_registered_service(instance) \
                    or queried_by_registered_service(instance):
                data['service_info'] = get_lt_service_info(instance)
        except ObjectDoesNotExist:
            pass
        if 'stat' in request.GET \
                and not (request.headers.get('discard-stats') in ['True', 'true']
                         and request.headers.get('user-agent') == 'ilsp-elg-scrapper'):
            request_user_username = request.user.username if request.user.username else 'AnonymousUser'
            view_tasks.increment_record_views.apply_async(args=[instance.id, request_user_username])
        return Response(data)

    @csrf_exempt
    def update(self, request, *args, **kwargs):
        # get instance and extract partial kwarg
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        # Get MetadataRecordSerializer for keeping declared context
        class_serializer = self.get_serializer_class()
        # handle draft updates
        previous_record_status = instance.management_object.status
        if 'draft' in request.query_params:
            # handle draft updates
            if previous_record_status == DRAFT:
                record_status = DRAFT
            # handle update of internal > draft
            else:
                return Response(
                    {'Update error': 'Cannot revert to Draft, when already Internal.'},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            # if prev status was draft then internal, otherwise do not
            # change status
            record_status = INTERNAL if previous_record_status == DRAFT else previous_record_status

        request.data['management_object'] = update_management_object(
            request, instance, record_status
        )
        # Update context
        context = super().get_serializer_context()
        context.update({
            'status': record_status,
            'prev_status': previous_record_status,
            'draft': (True if record_status == DRAFT else False)

        })
        # Handle functional_service context for software validation rules
        if request.headers.get('functional-service') in ['True', 'true']:
            context.update({'functional_service': True})

        # Handle service compliant datasets
        if request.headers.get('service-compliant-dataset') in ['True', 'true']:
            context.update({'service_compliant_dataset': True})

        # Special handle of partial=True with no data for metadata record
        # only for HMTL headers: eg functional, under-construction, etc
        if partial and 'described_entity' not in request.data:
            manager_serializer = ManagerSerializer(
                instance=instance.management_object,
                data=request.data['management_object']
            )
            manager_serializer.is_valid(raise_exception=True)
            manager_serializer.save()
            MyResourcesDocument().update(manager_serializer.instance)
            response = {
                'pk': instance.pk,
                'management_object': manager_serializer.data
            }
        else:
            # Handle HTML headers and parameters that affect management_object
            serializer = class_serializer(instance, data=request.data, partial=partial, context=context)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            # update published relations of record
            serializer.instance.management_object.update_landing_pages_of_related_records(request_user=request.user)
            MyResourcesDocument().update(serializer.instance.management_object)
            response = serializer.data
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(response)

    def destroy(self, request, *args, **kwargs):
        raise MethodNotAllowed('DELETE')

    def list(self, request, *args, **kwargs):
        raise MethodNotAllowed('GET', detail='Method "GET" not allowed without lookup.')


@api_view(http_method_names=['GET'])
@permission_classes([AllowAny])
def concept_record(request, concept_pid):
    try:
        record = MetadataRecord.objects.get(
            management_object__concept_pid=concept_pid, management_object__is_latest_version=True
        )
    except MetadataRecord.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        return redirect(record.get_display_url())


class MetadataRecordXMLView(viewsets.ViewSet, CreateAPIView, RetrieveAPIView):
    """XML for the MetadataRecord class"""

    queryset = MetadataRecord.objects.all()
    serializer_class = MetadataRecordSerializer
    parser_classes = [MultiPartParser, ]
    content_negotiation_class = IgnoreClientContentNegotiation

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        updated_permission_classes = [IsAuthenticatedOrReadOnly]
        if self.action == 'create':
            updated_permission_classes.append(CreateMetadataRecordPermission)
        elif self.action == 'retrieve':
            updated_permission_classes.append(ExportXMLRecordPermission)
        return [permission() for permission in updated_permission_classes]

    def create(self, request, *args, **kwargs):
        if not request.data.get('file'):
            return Response(
                {'File missing error': 'No xml provided.'},
                status=status.HTTP_400_BAD_REQUEST
            )
        try:
            xml_file = request.data['file'].read()
            xml_validator_url = settings.XML_VALIDATOR_URL
            xml_validation_response = requests.request("POST", xml_validator_url, files={'file': xml_file})
            if xml_validation_response.status_code != 404:
                if 'errors' in xml_validation_response.json().keys():
                    return Response(
                        {'Invalid XML': xml_validation_response.json()['errors']},
                        status=status.HTTP_400_BAD_REQUEST
                    )
            try:
                xml_data = xmltodict.parse(xml_file, xml_attribs=True)[
                    'ms:MetadataRecord']
            except ExpatError:
                return Response(
                    {'File type error': 'Please import xml only.'},
                    status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
                )
            # Create record
            serializer = create_metadata_record(request, request.user,
                                                xml_data, xml=True)
            # Return duplication_error
            if isinstance(serializer, Response):
                return serializer

            # instantly publish if resource is uploaded by elg-system
            if request.user.username == settings.SYSTEM_USER:
                serializer.instance.management_object.handle_version_publication([])
                serializer.instance.management_object.publish(force=True)
                MetadataDocument().catalogue_update(serializer.instance)
                CatalogReportDocument().catalogue_report_update(serializer.instance.management_object)
                compute_dle_score.apply_async(args=[[serializer.instance.id]])
            else:
                # update published relations of record
                serializer.instance.management_object.update_landing_pages_of_related_records(request_user=request.user)
            # Add record to dashboard index
            MyResourcesDocument().update(serializer.instance.management_object, action='index')
            curator_content_manager_notifications.apply_async(args=[[serializer.instance.id],
                                                                    'xml_creation'])
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except UnsupportedMediaType:
            return Response({'File type error': 'Please import xml only.'},
                            status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        (filename, xml_string) = instance_to_xml_string(instance)
        # Create the HttpResponse object with the appropriate XML header.
        output = io.StringIO()
        output.write(xml_string)
        output.seek(0)
        response = HttpResponse(output.read(), content_type='application/xml')
        output.close()
        response['Content-Disposition'] = "attachment; filename={}".format(filename)
        return response


class BatchMetadataRecordXMLView(viewsets.ViewSet, RetrieveAPIView):
    queryset = MetadataRecord.objects.all()
    serializer_class = MetadataRecordSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        updated_permission_classes = [IsAuthenticated]
        if self.action == 'retrieve':
            updated_permission_classes.append(BatchExportXMLRecordPermission)
        return [permission() for permission in updated_permission_classes]

    def retrieve(self, request, *args, **kwargs):
        requested_ids = kwargs.get('pk', None)
        LOGGER.info(f'[BATCH-XML-RETRIEVE] User {request.user} requested the following '
                    f'metadata records:{requested_ids}')
        if not requested_ids:
            return Response(status=status.HTTP_404_NOT_FOUND)
        requested_ids = [int(d.strip()) for d in requested_ids.split(',')]
        permitted_ids = copy.deepcopy(requested_ids)
        instances = MetadataRecord.objects.filter(pk__in=requested_ids)
        instance_list = [instance for instance in instances]
        errors = dict()
        instances_id = []
        # Check permission per record
        for instance in instance_list:
            try:
                instances_id.append(instance.pk)
                # check if information only
                if instance.management_object.for_information_only:
                    errors[instance.id] = "Invalid pk - object is for information only."
                    # Remove from permitted
                    permitted_ids.remove(instance.id)
                    continue
                self.check_object_permissions(request, instance)
            except Exception as exc:
                # Get error message
                response = self.handle_exception(exc)
                errors[instance.pk] = str(response.data['detail'])
                # Remove from permitted
                permitted_ids.remove(instance.pk)
        # Check if requested for record that does not exist
        requested_ids_not_existing = list(set(requested_ids) - set(instances_id))
        if len(requested_ids_not_existing):
            for error_id in requested_ids_not_existing:
                errors[error_id] = "Invalid pk - object does not exist."
                # Remove from permitted
                permitted_ids.remove(error_id)

        view_tasks.batch_xml_retrieve.apply_async(
            args=[request.user.pk, requested_ids, permitted_ids, errors]
        )
        return Response(
            {'detail': 'Request getting processed. You will receive an email upon task completion '
                       'with details about your request.'},
            status.HTTP_202_ACCEPTED
        )


class XMLToRDFView(RetrieveAPIView):
    """XML for the MetadataRecord class"""

    queryset = MetadataRecord.objects.all()
    serializer_class = MetadataRecordSerializer
    parser_classes = [MultiPartParser, ]

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        updated_permission_classes = [IsAuthenticatedOrReadOnly]
        updated_permission_classes.append(ExportXMLRecordPermission)
        return [permission() for permission in updated_permission_classes]

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        (filename, rdf_string) = xml_to_rdf(instance)
        # Create the HttpResponse object with the appropriate XML header.
        output = io.StringIO()
        output.write(rdf_string)
        output.seek(0)
        response = HttpResponse(output.read(), content_type='application/xml')
        output.close()
        response['Content-Disposition'] = "attachment; filename={}".format(filename)
        return response


@api_view(http_method_names=['POST'])
@permission_classes([IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission])
def batch_metadata_upload(request):
    accepted_extensions = ['.zip', '.tar.gz']
    if not request.FILES.get('file'):
        return Response(
            {'File missing error': 'No zip file provided.'},
            status=status.HTTP_400_BAD_REQUEST
        )
    try:
        file_obj = request.FILES['file']
    except MultiValueDictKeyError:
        file_obj = request.data['file']
    try:
        archive = zipfile.ZipFile(file_obj, 'r')
    except (zipfile.BadZipfile, AttributeError):
        return Response(
            {'File type error': 'Please import zip only.'},
            status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
        )
    upload_groups = list()
    # iter records, search for datasets
    xml_metadata = [xml for xml in archive.namelist() if xml.endswith('.xml')]
    for xml_filename in xml_metadata:
        upload_groups.append({'metadata': xml_filename})
    xml_import_errors = []
    if upload_groups:
        for member in upload_groups:
            xml_file = archive.open(member.get('metadata')).read()
            xml_validator_url = settings.XML_VALIDATOR_URL
            xml_validation_response = requests.request("POST", xml_validator_url, files={'file': xml_file})
            if xml_validation_response.status_code != 404:
                if 'errors' in xml_validation_response.json().keys():
                    xml_import_errors.append(
                        {f'{member["metadata"]}': {'Invalid XML': xml_validation_response.json()['errors']}},
                    )
                    continue
            try:
                xml_data = xmltodict.parse(xml_file, xml_attribs=True)[
                    'ms:MetadataRecord']
                member['metadata_record'] = xml_data
            except ExpatError:
                xml_import_errors.append({f'{member["metadata"]}': 'Please import valid xml only.'})
                continue
    # instantiate the person linked to the request user
    request_user_id = request.user.id
    harvest = True
    if 'harvest' not in request.query_params:
        harvest = False
    u_c_header = False
    if request.headers.get('under-construction', None) in ['True', 'true']:
        u_c_header = True
    f_s_header = False
    if request.headers.get('functional-service', None) in ['True', 'true']:
        f_s_header = True
    s_c_d_header = False
    if request.headers.get('service-compliant-dataset', None) in ['True', 'true']:
        s_c_d_header = True
    view_tasks.batch_xml_upload.apply_async(
        args=[upload_groups, xml_import_errors,
              request_user_id, harvest, u_c_header, f_s_header, s_c_d_header]
    )
    return Response({'message': 'Request getting processed. You will receive an email upon upload completion '
                                'with details about your request.'},
                    status.HTTP_201_CREATED)


@api_view(http_method_names=['POST'])
@permission_classes([IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission])
def batch_metadata_upload_json(request):
    if not request.FILES.get('file'):
        return Response(
            {'File missing error': 'No zip file provided.'},
            status=status.HTTP_400_BAD_REQUEST
        )
    file_obj = request.FILES['file']
    try:
        archive = zipfile.ZipFile(file_obj, 'r')
    except (zipfile.BadZipfile, AttributeError):
        return Response({'File type error': 'Please import zip only.'},
                        status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
    # iter records, search for datasets
    metadata_records = [json_st for json_st in archive.namelist() if
                        json_st.endswith('.json')]
    return_data = dict()
    created_ids = []
    for record in metadata_records:
        LOGGER.info('[BATCH] Importing metadata {}'.format(record))
        json_data = json.load(archive.open(record))
        try:
            serializer = create_metadata_record(request, request.user, json_data)
            return_data[record] = get_registry_identifier(serializer.instance)
            created_ids.append(serializer.instance.id)
            # update published relations of record
            serializer.instance.management_object.update_landing_pages_of_related_records(request_user=request.user)
        except Exception as exc:
            return_data[record] = exc.detail
    # add created records to dashboard index
    created_records = Manager.objects.filter(metadata_record_of_manager__pk__in=created_ids)
    MyResourcesDocument().update(created_records)
    return Response(return_data, status=status.HTTP_201_CREATED)


class DMEReportView(generics.RetrieveAPIView):
    """
    Retrieve and transform dme_report index data
    """
    permission_classes = [RetrieveMetadataRecordPermission]

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            records = MetadataRecord.objects.all()
        else:
            # Return 404 for deleted and never published records
            records = MetadataRecord.objects.exclude(
                management_object__deleted=True, management_object__publication_date__isnull=True
            )
        return records

    def retrieve(self, request, *args, **kwargs):
        query_param = request.query_params.get('language')
        if not query_param:
            return Response({'Query Error': 'Please make a query (?language=language_tag)'},
                            status=status.HTTP_400_BAD_REQUEST)
        search_term = tags.description(query_param)[0]
        es_language_query = f'{settings.DJANGO_URL}{"" if settings.DJANGO_URL.endswith("/") else "/"}' \
                            f'catalogue_backend/api/registry/dme_report_index/?language__term={search_term}'
        es_dataset_url = f'{es_language_query}&lr_distinction__term=Dataset'
        es_software_url = f'{es_language_query}&lr_distinction__term=Software'
        es_dataset_response = requests.request("GET", f'{es_dataset_url}').json()['facets']
        es_software_response = requests.request("GET", f'{es_software_url}').json()['facets']
        final_dataset_response = dict()
        final_software_response = dict(media_type={})
        all_keys = set(list(es_dataset_response.keys()) + list(es_software_response.keys()))
        for key in all_keys:
            if key in ['_filter_lr_distinction', '_filter_language']:
                continue
            if key not in ['_filter_language_independent',
                           '_filter_function',
                           '_filter_input_media_type',
                           '_filter_output_media_type']:
                final_dataset_response[key.replace('_filter_', '')] = es_dataset_response[key]
            if key not in ['_filter_linguality_type',
                           '_filter_annotation_type',
                           '_filter_media_type',
                           '_filter_subclass']:
                final_software_response[key.replace('_filter_', '')] = es_software_response[key]
        for k, v in final_dataset_response.items():
            bucket_list = [{d['key']: d} for d in v[k]['buckets']]
            target_value = {k: v for d in bucket_list for k, v in d.items()}
            final_dataset_response[k] = target_value
        for k, v in final_software_response.items():
            if k == 'media_type':
                continue
            bucket_list = [{d['key']: d} for d in v[k]['buckets']]
            target_value = {k: v for d in bucket_list for k, v in d.items()}
            if k == 'input_media_type':
                final_software_response['media_type'].update({'input': target_value})
            elif k == 'output_media_type':
                final_software_response['media_type'].update({'output': target_value})
            else:
                final_software_response[k] = target_value
        remove_keys = ('input_media_type', 'output_media_type')
        for k in remove_keys:
            final_software_response.pop(k, None)
        content = {
            'Datasets': final_dataset_response,
            'Software': final_software_response
        }
        return Response(
            content,
            status=status.HTTP_200_OK
        )


class MoveUnspecifiedToMediaView(generics.UpdateAPIView):
    """
    Move unspecified_part of record to requested X_media_parts
    and unspecified_distribution_feature to requested distribution_X_features
    """
    permission_classes = [UpdateMetadataRecordPermission]
    queryset = MetadataRecord.objects.all()

    def update(self, request, *args, **kwargs):
        pk = kwargs.get('pk', False)
        # Check that user provide media type
        media = request.data.get('media', [])
        if len(media) == 0:
            return Response(
                f'Please specify the media type to move unspecified for this record.',
                status=status.HTTP_400_BAD_REQUEST
            )
        try:
            queryset = self.get_queryset()
            instance = queryset.get(pk=pk)
            if not instance.management_object.unspecified:
                return Response(
                    f'This action is prohibited for this record, it is not qualified with unspecified=True.',
                    status=status.HTTP_403_FORBIDDEN
                )
            # # TODO check media is appropriate for lr type
            # if self.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource' \
            #     and self.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'Corpus':
            #     and not A.issubset(B):
            #     return Response(
            #         f'This record does not contain unspecified part.',
            #         status=status.HTTP_406_NOT_ACCEPTABLE
            #     )

            #                                                                          'LexicalConceptualResource',
            #                                                                           'LanguageDescription'] \

            # TODO Move unspecified to appropriate media
        except MetadataRecord.DoesNotExist:
            return Response(
                f'This record does not exist.',
                status=status.HTTP_404_NOT_FOUND
            )
        content = MetadataRecordSerializer(instance=instance).data
        return Response(
            content,
            status=status.HTTP_200_OK
        )


class DataCiteMetadataRecordViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for viewing accounts.
    """
    permission_classes = [AllowAny]
    serializer_class = DataCiteMetadataRecordSerializer

    def get_queryset(self):
        """
        Accept functional services or hosted lrs
        """
        records = MetadataRecord.objects.filter(
            (
                    Q(management_object__functional_service=True)
                    & Q(described_entity__languageresource__lr_subclass__lr_type='ToolService')
            ) |
            (
               Q(management_object__content_files__isnull=False)
               & ~Q(described_entity__languageresource__lr_subclass__lr_type='ToolService')
            ),
            management_object__status='p',
            management_object__deleted=False,
            described_entity__entity_type='LanguageResource',
        )
        return records

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        datacite_data = serializer.data

        assert schema43.validate(datacite_data)
        return Response(datacite_data)

    @action(detail=True)
    def xml_format(self, request, pk=None):
        """
        Get record in xml in various formats
        :param metadata_prefix: the prefix of the format
        :param record_id: the record id
        :return: the xml record in specified format
        """
        record = self.get_object()
        serializer = DataCiteMetadataRecordSerializer(instance=record)
        datacite_data = serializer.data
        # Validate over schema 4.3
        datacite_xml = schema43.dump_etree(datacite_data)
        datacite_xml_string = etree.tostring(datacite_xml, encoding='unicode', method='xml')

        return HttpResponse(datacite_xml_string, content_type='text/xml')
