import logging
import re
import time
from difflib import SequenceMatcher
from urllib.parse import unquote

from django.conf import settings
from django.db.models import Q
from rest_framework import (
    viewsets, status, mixins
)
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from management.models import Manager
from registry import (
    models, serializers, choices
)
from registry.permissions import CreateMetadataRecordPermission
from registry.utils.retrieval_utils import get_similarity

LOGGER = logging.getLogger(__name__)

ACTION_NOT_ALLOWED = {"detail": "You do not have permission to perform this action."}


def match_specific_lr_type(decoded_query_params, query_entity):
    generic_queryset = models.LanguageResource.objects.filter(
        (Q(resource_name__icontains=decoded_query_params) |
         Q(resource_short_name__icontains=decoded_query_params)) &
        Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('resource_name')
    full_queryset = models.LanguageResource.objects.filter(
        (Q(resource_name__icontains=decoded_query_params) |
         Q(resource_short_name__icontains=decoded_query_params))
        & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
           Q(describedentity_ptr__languageresource__lr_subclass__lr_type=query_entity) &
           Q(
               describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
        'resource_name')
    return generic_queryset, full_queryset


def lookup_query_conditions(request_user):
    if request_user.is_superuser:
        return (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True) |
                (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                 ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status='d')) &
                 Q(
                     describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
    else:
        return (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True) |
                (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                 (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status='p') |
                  (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
                   ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status='d'))) &
                 Q(
                     describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False)))


class OrganizationMatchView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            parent = False
            start_time = time.time()
            generic_queryset = models.Organization.objects.filter(
                (Q(organization_name__icontains=decoded_query_params) |
                 Q(organization_short_name__icontains=decoded_query_params)) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('organization_name')
            full_queryset = models.Organization.objects.filter(
                (Q(organization_name__icontains=decoded_query_params) |
                 Q(organization_short_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'organization_name')
            if 'parent' in request.GET and request.query_params.get('parent', ''):
                parent = True
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
            start_time = time.time()
            gen_instances = [instance for instance in generic_queryset]
            full_instances = [instance for instance in full_queryset]
            u_c_instances = [instance for instance in full_queryset.filter(Q(
                describedentity_ptr__metadata_record_of_described_entity__management_object__under_construction=True))]
            all_instances = gen_instances + full_instances
            LOGGER.info("---Creating queryset cache in %s seconds ---" % (time.time() - start_time))
            proc_time = time.time()
            return_list = []
            for n, instance in enumerate(all_instances):
                similarity = get_similarity(decoded_query_params,
                                            instance.organization_name,
                                            getattr(instance, 'organization_short_name')
                                            )
                return_dict = {
                    'organization_name': instance.organization_name,
                    'id': instance.pk,
                    'full_metadata_record': False,
                    'locked': False,
                    'metadata_record': serializers.GenericOrganizationSerializer(instance).data,
                    'similarity': similarity,
                    'exact': False,
                    'status': None,
                    'curator': None,
                    'division_of': None
                }
                division_list = [serializers.GenericOrganizationSerializer(
                    parent).data for parent in instance.is_division_of.all()]
                if division_list:
                    return_dict['division_of'] = division_list
                if similarity == 1.0 and not parent:
                    return_dict['exact'] = True
                if instance in full_instances:
                    management_object = Manager.objects.get(
                        metadata_record_of_manager__described_entity__actor__organization__pk=instance.pk)
                    return_dict['locked'] = True
                    return_dict['full_metadata_record'] = True
                    return_dict[
                        'metadata_record'] = f"{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/" \
                                             f"{management_object.metadata_record_of_manager.pk}/ "
                    return_dict['status'] = management_object.status
                    return_dict[
                        'curator'] = management_object.curator.username if management_object.curator else None
                    if management_object.is_claimable:
                        return_dict['locked'] = False
                    if instance in u_c_instances and management_object.curator == self.request.user:
                        return_dict.update({
                            'under_construction':
                                f'The metadata record of {instance.__str__()} has'
                                f' the under construction status. If you are looking to add the complete version'
                                f' please unpublish the record, so it can be edited and'
                                f' submitted as such.'
                        })
                return_list.append(return_dict)
            LOGGER.info("---Process query in %s seconds ---" % (time.time() - proc_time))
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class ProjectMatchView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            generic_queryset = models.Project.objects.filter(
                (Q(project_name__icontains=decoded_query_params) |
                 Q(project_short_name__icontains=decoded_query_params)) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('project_name')
            full_queryset = models.Project.objects.filter(
                (Q(project_name__icontains=decoded_query_params) |
                 Q(project_short_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'project_name')
            gen_instances = [instance for instance in generic_queryset]
            full_instances = [instance for instance in full_queryset]
            u_c_instances = [instance for instance in full_queryset.filter(Q(
                describedentity_ptr__metadata_record_of_described_entity__management_object__under_construction=True))]
            all_instances = gen_instances + full_instances
            return_list = []
            for n, instance in enumerate(all_instances):
                similarity = get_similarity(decoded_query_params,
                                            instance.project_name,
                                            getattr(instance, 'project_short_name'))
                return_dict = {
                    'project_name': instance.project_name,
                    'id': instance.pk,
                    'full_metadata_record': False,
                    'locked': False,
                    'metadata_record': serializers.GenericProjectSerializer(instance).data,
                    'similarity': similarity,
                    'exact': False,
                    'status': None,
                    'curator': None
                }
                if return_dict['similarity'] == 1.0:
                    return_dict['exact'] = True
                if instance in full_instances:
                    management_object = Manager.objects.get(
                        metadata_record_of_manager__described_entity__project__pk=instance.pk)
                    return_dict['locked'] = True
                    return_dict['full_metadata_record'] = True
                    return_dict[
                        'metadata_record'] = f"{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/" \
                                             f"{management_object.metadata_record_of_manager.pk}/ "
                    return_dict['status'] = management_object.status
                    return_dict['curator'] = management_object.curator.username if management_object.curator else None
                    if management_object.is_claimable:
                        return_dict['locked'] = False
                    if instance in u_c_instances and management_object.curator == self.request.user:
                        return_dict.update({
                            'under_construction':
                                f'The metadata record of {instance.__str__()} has'
                                f' the under construction status. If you are looking to add the complete version'
                                f' please unpublish the record, so it can be edited and'
                                f' submitted as such.'
                        })
                return_list.append(return_dict)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class PersonMatchView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            pattern = re.compile(r'^([\w\-.]+)(\s*(.+)\s*\b(\w+))?$')
            decoded_query_params = unquote(request.query_params['query'])
            (given_name, _, _, surname) = pattern.findall(decoded_query_params)[0]
            generic_queryset = models.Person.objects.filter(
                (Q(given_name__icontains=given_name) &
                 Q(surname__icontains=surname)) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('surname')
            full_queryset = models.Person.objects.filter(
                (Q(given_name__icontains=given_name) &
                 Q(surname__icontains=surname))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'surname')
            query = 'query'
        elif 'given_name' in request.GET and len(request.query_params['given_name']) > 2:
            given_name, surname = '', ''
            decoded_query_params = unquote(request.query_params['given_name'])
            generic_queryset = models.Person.objects.filter(
                Q(given_name__icontains=decoded_query_params) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('given_name')
            full_queryset = models.Person.objects.filter(
                Q(given_name__icontains=decoded_query_params)
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'given_name')
            query = 'given_name'
        elif 'surname' in request.GET and len(request.query_params['surname']) > 2:
            given_name, surname = '', ''
            decoded_query_params = unquote(request.query_params['surname'])
            generic_queryset = models.Person.objects.filter(
                Q(surname__icontains=decoded_query_params) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('surname')
            full_queryset = models.Person.objects.filter(
                Q(surname__icontains=decoded_query_params)
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'surname')
            query = 'surname'
        else:
            return Response({'Query Error': 'Please make a query'},
                            status=status.HTTP_400_BAD_REQUEST)
        gen_instances = [instance for instance in generic_queryset]
        full_instances = [instance for instance in full_queryset]
        u_c_instances = [instance for instance in full_queryset.filter(Q(
            describedentity_ptr__metadata_record_of_described_entity__management_object__under_construction=True))]
        all_instances = gen_instances + full_instances
        return_list = []
        for n, instance in enumerate(all_instances):
            if query == 'query':
                max_surname_similarity = max([SequenceMatcher(None, name.lower(),
                                                              surname.lower()).ratio() for name
                                              in instance.surname.values()])
                max_given_name_similarity = max([SequenceMatcher(None, name.lower(),
                                                                 given_name.lower()).ratio() for name
                                                 in instance.given_name.values()])
                similarity = (max_surname_similarity + max_given_name_similarity) / 2
            elif query == 'given_name':
                similarity = max([SequenceMatcher(None, name.lower(),
                                                  request.query_params[query].lower()).ratio() for name
                                  in instance.given_name.values()])
            elif query == 'surname':
                similarity = max([SequenceMatcher(None, surname.lower(),
                                                  request.query_params[query].lower()).ratio() for surname
                                  in instance.surname.values()])
            else:
                similarity = 0
            return_dict = {
                'given_name': instance.given_name,
                'surname': instance.surname,
                'id': instance.pk,
                'full_metadata_record': False,
                'locked': False,
                'metadata_record': serializers.GenericPersonSerializer(instance).data,
                'similarity': similarity,
                'exact': False,
                'status': None,
                'curator': None
            }
            if similarity == 1.0:
                return_dict['exact'] = True
            if instance in full_instances:
                management_object = Manager.objects.get(
                    metadata_record_of_manager__described_entity__actor__person__pk=instance.pk)
                return_dict['locked'] = True
                return_dict['full_metadata_record'] = True
                return_dict['metadata_record'] = f"{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/" \
                                                 f"{management_object.metadata_record_of_manager.pk}/ "
                return_dict['status'] = management_object.status
                return_dict['curator'] = management_object.curator.username if management_object.curator else None
                if management_object.is_claimable:
                    return_dict['locked'] = False
                if instance in u_c_instances and management_object.curator == self.request.user:
                    return_dict.update({
                        'under_construction':
                            f'The metadata record of {instance.__str__()} has'
                            f' the under construction status. If you are looking to add the complete version'
                            f' please unpublish the record, so it can be edited and'
                            f' submitted as such.'
                    })
            return_list.append(return_dict)
        return Response(return_list, status.HTTP_200_OK)


class GroupMatchView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            generic_queryset = models.Group.objects.filter(
                (Q(organization_name__icontains=decoded_query_params) |
                 Q(organization_short_name__icontains=decoded_query_params)) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('organization_name')
            full_queryset = models.Group.objects.filter(
                (Q(organization_name__icontains=decoded_query_params) |
                 Q(organization_short_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'organization_name')
            gen_instances = [instance for instance in generic_queryset]
            full_instances = [instance for instance in full_queryset]
            u_c_instances = [instance for instance in full_queryset.filter(Q(
                describedentity_ptr__metadata_record_of_described_entity__management_object__under_construction=True))]
            all_instances = gen_instances + full_instances
            return_list = []
            for n, instance in enumerate(all_instances):
                similarity = get_similarity(decoded_query_params,
                                            instance.organization_name,
                                            getattr(instance, 'organization_short_name'))
                return_dict = {
                    'organization_name': instance.organization_name,
                    'id': instance.pk,
                    'full_metadata_record': False,
                    'locked': False,
                    'metadata_record': serializers.GenericGroupSerializer(instance).data,
                    'similarity': similarity,
                    'exact': False,
                    'status': None,
                    'curator': None
                }
                if return_dict['similarity'] == 1.0:
                    return_dict['exact'] = True
                if instance in full_instances:
                    management_object = Manager.objects.get(
                        metadata_record_of_manager__described_entity__actor__group__pk=instance.pk)
                    return_dict['locked'] = True
                    return_dict['full_metadata_record'] = True
                    return_dict[
                        'metadata_record'] = f"{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/" \
                                             f"{management_object.metadata_record_of_manager.pk}/ "
                    return_dict['status'] = management_object.status
                    return_dict['curator'] = management_object.curator.username if management_object.curator else None
                    if management_object.is_claimable:
                        return_dict['locked'] = False
                    if instance in u_c_instances and management_object.curator == self.request.user:
                        return_dict.update({
                            'under_construction':
                                f'The metadata record of {instance.__str__()} has'
                                f' the under construction status. If you are looking to add the complete version'
                                f' please unpublish the record, so it can be edited and'
                                f' submitted as such.'
                        })
                return_list.append(return_dict)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class DocumentMatchView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            generic_queryset = models.Document.objects.filter(
                (Q(title__icontains=decoded_query_params) |
                 Q(alternative_title__icontains=decoded_query_params)) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('title')
            full_queryset = models.Document.objects.filter(
                (Q(title__icontains=decoded_query_params) |
                 Q(alternative_title__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'title')
            gen_instances = [instance for instance in generic_queryset]
            full_instances = [instance for instance in full_queryset]
            u_c_instances = [instance for instance in full_queryset.filter(Q(
                describedentity_ptr__metadata_record_of_described_entity__management_object__under_construction=True))]
            all_instances = gen_instances + full_instances
            return_list = []
            for n, instance in enumerate(all_instances):
                similarity = get_similarity(decoded_query_params,
                                            instance.title,
                                            getattr(instance, 'alternative_title'))
                return_dict = {
                    'title': instance.title,
                    'id': instance.pk,
                    'full_metadata_record': False,
                    'locked': False,
                    'metadata_record': serializers.GenericDocumentSerializer(instance).data,
                    'similarity': similarity,
                    'exact': False,
                    'status': None,
                    'curator': None
                }
                if return_dict['similarity'] == 1.0:
                    return_dict['exact'] = True
                if instance in full_instances:
                    management_object = Manager.objects.get(
                        metadata_record_of_manager__described_entity__document__pk=instance.pk)
                    return_dict['locked'] = True
                    return_dict['full_metadata_record'] = True
                    return_dict[
                        'metadata_record'] = f"{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/" \
                                             f"{management_object.metadata_record_of_manager.pk}/ "
                    return_dict['status'] = management_object.status
                    return_dict['curator'] = management_object.curator.username if management_object.curator else None
                    if management_object.is_claimable:
                        return_dict['locked'] = False
                    if instance in u_c_instances and management_object.curator == self.request.user:
                        return_dict.update({
                            'under_construction':
                                f'The metadata record of {instance.__str__()} has'
                                f' the under construction status. If you are looking to add the complete version'
                                f' please unpublish the record, so it can be edited and'
                                f' submitted as such.'
                        })
                return_list.append(return_dict)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class LicenceTermsMatchView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 1:
            decoded_query_params = unquote(request.query_params['query'])
            generic_queryset = models.LicenceTerms.objects.filter(
                (Q(licence_terms_name__icontains=decoded_query_params) |
                 Q(licence_terms_short_name__icontains=decoded_query_params)) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('licence_terms_name')
            full_queryset = models.LicenceTerms.objects.filter(
                (Q(licence_terms_name__icontains=decoded_query_params) |
                 Q(licence_terms_short_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'licence_terms_name')
            gen_instances = [instance for instance in generic_queryset]
            full_instances = [instance for instance in full_queryset]
            u_c_instances = [instance for instance in full_queryset.filter(Q(
                describedentity_ptr__metadata_record_of_described_entity__management_object__under_construction=True))]
            all_instances = gen_instances + full_instances
            return_list = []
            for n, instance in enumerate(all_instances):
                similarity = get_similarity(decoded_query_params,
                                            instance.licence_terms_name,
                                            getattr(instance, 'licence_terms_short_name'))
                return_dict = {
                    'licence_terms_name': instance.licence_terms_name,
                    'id': instance.pk,
                    'full_metadata_record': False,
                    'locked': False,
                    'metadata_record': serializers.GenericLicenceTermsSerializer(instance).data,
                    'similarity': similarity,
                    'exact': False,
                    'status': None,
                    'curator': None
                }
                if return_dict['similarity'] == 1.0:
                    return_dict['exact'] = True
                if instance in full_instances:
                    management_object = Manager.objects.get(
                        metadata_record_of_manager__described_entity__licenceterms__pk=instance.pk)
                    return_dict['locked'] = True
                    return_dict['full_metadata_record'] = True
                    return_dict[
                        'metadata_record'] = f"{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/" \
                                             f"{management_object.metadata_record_of_manager.pk}/ "
                    return_dict['status'] = management_object.status
                    return_dict['curator'] = management_object.curator.username if management_object.curator else None
                    if management_object.is_claimable:
                        return_dict['locked'] = False
                    if instance in u_c_instances and management_object.curator == self.request.user:
                        return_dict.update({
                            'under_construction':
                                f'The metadata record of {instance.__str__()} has'
                                f' the under construction status. If you are looking to add the complete version'
                                f' please unpublish the record, so it can be edited and'
                                f' submitted as such.'
                        })
                return_list.append(return_dict)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class RepositoryMatchView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            generic_queryset = models.Repository.objects.filter(
                (Q(repository_name__icontains=decoded_query_params) |
                 Q(repository_additional_name__icontains=decoded_query_params)) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('repository_name')
            full_queryset = models.Repository.objects.filter(
                (Q(repository_name__icontains=decoded_query_params) |
                 Q(repository_additional_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'repository_name')
            gen_instances = [instance for instance in generic_queryset]
            full_instances = [instance for instance in full_queryset]
            u_c_instances = [instance for instance in full_queryset.filter(Q(
                describedentity_ptr__metadata_record_of_described_entity__management_object__under_construction=True))]
            all_instances = gen_instances + full_instances
            return_list = []
            for n, instance in enumerate(all_instances):
                similarity = get_similarity(decoded_query_params,
                                            instance.repository_name,
                                            getattr(instance, 'repository_additional_name'))
                return_dict = {
                    'repository_name': instance.repository_name,
                    'id': instance.pk,
                    'full_metadata_record': False,
                    'locked': False,
                    'metadata_record': serializers.GenericRepositorySerializer(instance).data,
                    'similarity': similarity,
                    'exact': False,
                    'status': None,
                    'curator': None
                }
                if return_dict['similarity'] == 1.0:
                    return_dict['exact'] = True
                if instance in full_instances:
                    management_object = Manager.objects.get(
                        metadata_record_of_manager__described_entity__repository__pk=instance.pk)
                    return_dict['locked'] = True
                    return_dict['full_metadata_record'] = True
                    return_dict[
                        'metadata_record'] = f"{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/" \
                                             f"{management_object.metadata_record_of_manager.pk}/ "
                    return_dict['status'] = management_object.status
                    return_dict['curator'] = management_object.curator.username if management_object.curator else None
                    if management_object.is_claimable:
                        return_dict['locked'] = False
                    if instance in u_c_instances and management_object.curator == self.request.user:
                        return_dict.update({
                            'under_construction':
                                f'The metadata record of {instance.__str__()} has'
                                f' the under construction status. If you are looking to add the complete version'
                                f' please unpublish the record, so it can be edited and'
                                f' submitted as such.'
                        })
                return_list.append(return_dict)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class LanguageResourceMatchView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query_all' in request.GET and len(request.query_params['query_all']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['query_all'])
            generic_queryset = models.LanguageResource.objects.filter(
                (Q(resource_name__icontains=decoded_query_params) |
                 Q(resource_short_name__icontains=decoded_query_params)) &
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True)).order_by('resource_name')
            full_queryset = models.LanguageResource.objects.filter(
                (Q(resource_name__icontains=decoded_query_params) |
                 Q(resource_short_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                   Q(
                       describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))).order_by(
                'resource_name')
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
        elif 'corpus' in request.GET and len(request.query_params['corpus']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['corpus'])
            generic_queryset, full_queryset = match_specific_lr_type(decoded_query_params, 'Corpus')
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
        elif 'tool_service' in request.GET and len(request.query_params['tool_service']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['tool_service'])
            generic_queryset, full_queryset = match_specific_lr_type(decoded_query_params, 'ToolService')
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
        elif 'lcr' in request.GET and len(request.query_params['lcr']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['lcr'])
            generic_queryset, full_queryset = match_specific_lr_type(decoded_query_params, 'LexicalConceptualResource')
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
        elif 'ld' in request.GET and len(request.query_params['ld']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['ld'])
            generic_queryset, full_queryset = match_specific_lr_type(decoded_query_params, 'LanguageDescription')
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
        else:
            return Response({'Query Error': 'Please make a query'},
                            status=status.HTTP_400_BAD_REQUEST)
        start_time = time.time()
        gen_instances = [instance for instance in generic_queryset]
        full_instances = [instance for instance in full_queryset]
        u_c_instances = [instance for instance in full_queryset.filter(Q(
            describedentity_ptr__metadata_record_of_described_entity__management_object__under_construction=True))]
        all_instances = gen_instances + full_instances
        LOGGER.info("---Creating queryset cache in %s seconds ---" % (time.time() - start_time))
        proc_time = time.time()
        return_list = []
        for n, instance in enumerate(all_instances):
            similarity = get_similarity(decoded_query_params,
                                        instance.resource_name,
                                        getattr(instance, 'resource_short_name'))
            return_dict = {
                'resource_name': instance.resource_name,
                'version': instance.version,
                'resource_type': None,
                'id': instance.pk,
                'full_metadata_record': False,
                'locked': False,
                'metadata_record': serializers.GenericLanguageResourceSerializer(instance).data,
                'similarity': similarity,
                'exact': False,
                'status': None,
                'curator': None
            }
            if return_dict['similarity'] == 1.0:
                return_dict['exact'] = True
            if instance in full_instances:
                if getattr(instance, 'lr_subclass'):
                    return_dict['resource_type'] = instance.lr_subclass.lr_type
                management_object = Manager.objects.get(
                    metadata_record_of_manager__described_entity__languageresource__pk=instance.pk)
                return_dict['locked'] = True
                return_dict['full_metadata_record'] = True
                return_dict['metadata_record'] = f"{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/" \
                                                 f"{management_object.metadata_record_of_manager.pk}/ "
                return_dict['status'] = management_object.status
                return_dict['curator'] = management_object.curator.username if management_object.curator else None
                if management_object.is_claimable:
                    return_dict['locked'] = False
                if instance in u_c_instances and management_object.curator == self.request.user:
                    return_dict.update({
                        'under_construction':
                            f' This metadata record is marked as "work in progress".'
                            f' If you have finalised the work and want to update it'
                            f' click on "unpublish" and proceed to editing it.'
                    })
            return_list.append(return_dict)
        LOGGER.info("---Process query in %s seconds ---" % (time.time() - proc_time))
        return Response(return_list, status.HTTP_200_OK)


class CVRecommendedLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        scheme_choices = None
        query = None
        if 'lt_area' in request.GET and len(request.query_params['lt_area']) > 2:
            scheme_choices = getattr(choices, 'LT_CLASS_RECOMMENDED_CHOICES')
            query = 'lt_area'
        elif 'model_type' in request.GET and len(request.query_params['model_type']) > 2:
            scheme_choices = getattr(choices, 'MODEL_TYPE_RECOMMENDED_CHOICES')
            query = 'model_type'
        elif 'model_function' in request.GET and len(request.query_params['model_function']) > 2:
            scheme_choices = getattr(choices, 'MODEL_FUNCTION_RECOMMENDED_CHOICES')
            query = 'model_function'
        elif 'data_format' in request.GET and len(request.query_params['data_format']) > 2:
            scheme_choices = getattr(choices, 'DATA_FORMAT_RECOMMENDED_CHOICES')
            query = 'data_format'
        elif 'size_unit' in request.GET and len(request.query_params['size_unit']) >= 2:
            scheme_choices = getattr(choices, 'SIZE_UNIT_RECOMMENDED_CHOICES')
            query = 'size_unit'
        elif 'development_framework' in request.GET and len(request.query_params['development_framework']) > 2:
            scheme_choices = getattr(choices, 'DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES')
            query = 'development_framework'
        elif 'annotation_type' in request.GET and len(request.query_params['annotation_type']) > 2:
            scheme_choices = getattr(choices, 'ANNOTATION_TYPE_RECOMMENDED_CHOICES')
            query = 'annotation_type'
        if scheme_choices:
            choices_list = list()
            for i in scheme_choices:
                choices_list.append(i)
            return_list = []
            for choice in choices_list:
                if request.query_params[query].lower() in choice[1].lower():
                    return_list.append(choice)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class LanguageLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        return_dict = dict()
        if 'language_label' in request.GET and len(request.query_params['language_label']) > 1:
            scheme_choices = getattr(choices, 'LANGUAGE_ID_CHOICES')
            choices_list = list()
            for i in scheme_choices:
                choices_list.append(i)
            return_list_pair = []
            for choice in choices_list:
                if request.query_params['language_label'].lower() in choice[1].lower():
                    return_list_pair.append(choice)
            return_dict['language_label'] = return_list_pair
        if 'all' in request.GET:
            scheme_choices = getattr(choices, 'LANGUAGE_ID_CHOICES')
            choices_list = list()
            for i in scheme_choices:
                choices_list.append(i)
            return_dict['all'] = choices_list
        if 'language_id' in request.GET and len(request.query_params['language_id']) > 1:
            scheme_choices = getattr(choices, 'LANGUAGE_ID_CHOICES')
            choices_list = list()
            for i in scheme_choices:
                choices_list.append(i)
            return_list_language_label = []
            for choice in choices_list:
                if request.query_params['language_id'].lower() == choice[0].lower():
                    return_list_language_label.append(choice[1])
            return_dict['language_id'] = return_list_language_label
        if len(return_dict) > 0:
            return Response(return_dict, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class CountryLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        return_dict = dict()
        if 'country_label' in request.GET and len(request.query_params['country_label']) > 1:
            scheme_choices = getattr(choices, 'REGION_ID_CHOICES')
            choices_list = list()
            for i in scheme_choices:
                choices_list.append(i)
            return_list_pair = []
            for choice in choices_list:
                if request.query_params['country_label'].lower() in choice[1].lower():
                    return_list_pair.append(choice)
            return_dict['country_label'] = return_list_pair
        if 'all' in request.GET:
            scheme_choices = getattr(choices, 'REGION_ID_CHOICES')
            choices_list = list()
            for i in scheme_choices:
                choices_list.append(i)
            return_dict['all'] = choices_list
        if 'country_id' in request.GET and len(request.query_params['country_id']) > 1:
            scheme_choices = getattr(choices, 'REGION_ID_CHOICES')
            choices_list = list()
            for i in scheme_choices:
                choices_list.append(i)
            return_list_language_label = []
            for choice in choices_list:
                if request.query_params['country_id'].lower() == choice[0].lower():
                    return_list_language_label.append(choice[1])
            return_dict['country_id'] = return_list_language_label
        if len(return_dict) > 0:
            return Response(return_dict, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class OrganizationLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.Organization.objects.filter(
                (Q(organization_name__icontains=decoded_query_params) |
                 Q(organization_short_name__icontains=decoded_query_params)) & lookup_query_conditions(
                    request.user)).order_by(
                'organization_name')
            serializer = serializers.GenericOrganizationSerializer(queryset, many=True)
            return_list = list()
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['organization_name'],
                                            instance.get('organization_short_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class PersonLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            pattern = re.compile(r'^([\w\-.]+)(\s*(.+)\s*\b(\w+))?$')
            decoded_query_params = unquote(request.query_params['query'])
            (given_name, _, _, surname) = pattern.findall(decoded_query_params)[0]
            queryset = models.Person.objects.filter(
                (Q(given_name__icontains=given_name) &
                 Q(surname__icontains=surname)) & lookup_query_conditions(request.user)).order_by('surname')
            query = 'query'
        elif 'given_name' in request.GET and len(request.query_params['given_name']) > 2:
            decoded_query_params = unquote(request.query_params['given_name'])
            queryset = models.Person.objects.filter(
                Q(given_name__icontains=decoded_query_params) & lookup_query_conditions(request.user)).order_by(
                'given_name')
            query = 'given_name'
        elif 'surname' in request.GET and len(request.query_params['surname']) > 2:
            decoded_query_params = unquote(request.query_params['surname'])
            queryset = models.Person.objects.filter(
                Q(surname__icontains=decoded_query_params) & lookup_query_conditions(request.user)).order_by('surname')
            query = 'surname'
        else:
            return Response({'Query Error': 'Please make a query'},
                            status=status.HTTP_400_BAD_REQUEST)
        serializer = serializers.GenericPersonSerializer(queryset, many=True)
        return_list = list()
        for n, instance in enumerate(serializer.data):
            max_surname_similarity = max([SequenceMatcher(None, surname.lower(),
                                                          request.query_params[query].lower()).ratio() for surname
                                          in instance['surname'].values()])
            max_given_name_similarity = max([SequenceMatcher(None, name.lower(),
                                                             request.query_params[query].lower()).ratio() for name
                                             in instance['given_name'].values()])
            similarity_list = [max_surname_similarity,
                               max_given_name_similarity
                               ]
            if query == 'query':
                similarity = (similarity_list[0] + similarity_list[1]) / 2
            elif query == 'given_name':
                similarity = max_given_name_similarity
            elif query == 'surname':
                similarity = max_surname_similarity
            else:
                similarity = 0
            instance['similarity'] = similarity
            return_list.append(instance)
        return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
        return Response(return_list, status.HTTP_200_OK)


class GroupLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.Group.objects.filter(
                (Q(organization_name__icontains=decoded_query_params) |
                 Q(organization_short_name__icontains=decoded_query_params))
                & lookup_query_conditions(request.user)).order_by('organization_name')
            serializer = serializers.GenericGroupSerializer(queryset, many=True)
            return_list = list()
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['organization_name'],
                                            instance.get('organization_short_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class ActorLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            pattern = re.compile(r'^([\w\-.]+)(\s*(.+)\s*\b(\w+))?$')
            decoded_query_params = unquote(request.query_params['query'])
            (given_name, _, _, surname) = pattern.findall(decoded_query_params)[0]
            queryset = models.Actor.objects.filter((Q(Person___given_name__icontains=given_name,
                                                      Person___surname__icontains=surname) |
                                                    (Q(Organization___organization_name__icontains=
                                                       decoded_query_params) |
                                                     Q(Organization___organization_short_name__icontains=
                                                       decoded_query_params)) |
                                                    (Q(Group___organization_name__icontains=decoded_query_params) |
                                                     Q(Group___organization_short_name__icontains=decoded_query_params)
                                                     )) & lookup_query_conditions(request.user))
            serializer = serializers.ActorSerializer(queryset, many=True)
            return_list = list()
            for n, instance in enumerate(serializer.data):
                if instance.get('actor_type') == 'Person':
                    max_surname_similarity = max([SequenceMatcher(None, name.lower(),
                                                                  surname.lower()).ratio() for
                                                  name
                                                  in instance['surname'].values()])
                    max_given_name_similarity = max([SequenceMatcher(None, name.lower(),
                                                                     given_name.lower()).ratio()
                                                     for name
                                                     in instance['given_name'].values()])
                    similarity_list = [max_surname_similarity,
                                       max_given_name_similarity
                                       ]
                    instance['similarity'] = (similarity_list[0] + similarity_list[1]) / 2
                if instance.get('actor_type') == 'Organization':
                    similarity = get_similarity(decoded_query_params,
                                                instance['organization_name'],
                                                instance.get('organization_short_name'))
                    instance['similarity'] = similarity
                if instance.get('actor_type') == 'Group':
                    similarity = get_similarity(decoded_query_params,
                                                instance['organization_name'],
                                                instance.get('organization_short_name'))
                    instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class DomainLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.Domain.objects.filter(
                category_label__icontains=decoded_query_params).order_by('category_label')
            serializer = serializers.DomainSerializer(queryset, many=True)
            return_list = list()
            for instance in serializer.data:
                instance['similarity'] = max([SequenceMatcher(None, category,
                                                              decoded_query_params).ratio() for category in
                                              instance['category_label']])
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class TextTypeLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.TextType.objects.filter(
                category_label__icontains=decoded_query_params).order_by('category_label')
            serializer = serializers.TextTypeSerializer(queryset, many=True)
            return_list = list()
            for instance in serializer.data:
                instance['similarity'] = max([SequenceMatcher(None, category,
                                                              decoded_query_params).ratio() for category in
                                              instance['category_label']])
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class AudioGenreLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.AudioGenre.objects.filter(
                category_label__icontains=decoded_query_params).order_by('category_label')
            serializer = serializers.AudioGenreSerializer(queryset, many=True)
            return_list = list()
            for instance in serializer.data:
                instance['similarity'] = max([SequenceMatcher(None, category,
                                                              decoded_query_params).ratio() for category in
                                              instance['category_label']])
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class SpeechGenreLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.SpeechGenre.objects.filter(
                category_label__icontains=decoded_query_params).order_by('category_label')
            serializer = serializers.SpeechGenreSerializer(queryset, many=True)
            return_list = list()
            for instance in serializer.data:
                instance['similarity'] = max([SequenceMatcher(None, category,
                                                              decoded_query_params).ratio() for category in
                                              instance['category_label']])
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class ImageGenreLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.ImageGenre.objects.filter(
                category_label__icontains=decoded_query_params).order_by('category_label')
            serializer = serializers.ImageGenreSerializer(queryset, many=True)
            return_list = list()
            for instance in serializer.data:
                instance['similarity'] = max([SequenceMatcher(None, category,
                                                              decoded_query_params).ratio() for category in
                                              instance['category_label']])
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class VideoGenreLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.VideoGenre.objects.filter(
                category_label__icontains=decoded_query_params).order_by('category_label')
            serializer = serializers.VideoGenreSerializer(queryset, many=True)
            return_list = list()
            for instance in serializer.data:
                instance['similarity'] = max([SequenceMatcher(None, category,
                                                              decoded_query_params).ratio() for category in
                                              instance['category_label']])
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class TextGenreLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.TextGenre.objects.filter(
                category_label__icontains=decoded_query_params).order_by('category_label')
            serializer = serializers.TextGenreSerializer(queryset, many=True)
            return_list = list()
            for instance in serializer.data:
                instance['similarity'] = max([SequenceMatcher(None, category,
                                                              decoded_query_params).ratio() for category in
                                              instance['category_label']])
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class SubjectLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.Subject.objects.filter(
                category_label__icontains=decoded_query_params).order_by('category_label')
            serializer = serializers.SubjectSerializer(queryset, many=True)
            return_list = list()
            for instance in serializer.data:
                instance['similarity'] = max([SequenceMatcher(None, category,
                                                              decoded_query_params).ratio() for category in
                                              instance['category_label']])
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class AccessRightsStatementLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.AccessRightsStatement.objects.filter(
                category_label__icontains=decoded_query_params).order_by('category_label')
            serializer = serializers.AccessRightsStatementSerializer(queryset, many=True)
            return_list = list()
            for instance in serializer.data:
                instance['similarity'] = max([SequenceMatcher(None, category,
                                                              decoded_query_params).ratio() for category in
                                              instance['category_label']])
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class ProjectLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.Project.objects.filter(
                (Q(project_name__icontains=decoded_query_params) |
                 Q(project_short_name__icontains=decoded_query_params))
                & lookup_query_conditions(request.user)).order_by('project_name')
            serializer = serializers.GenericProjectSerializer(queryset, many=True)
            return_list = list()
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['project_name'],
                                            instance.get('project_short_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class LanguageResourceLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        return_dict = dict()
        if 'query_all' in request.GET and len(request.query_params['query_all']) > 2:
            decoded_query_params = unquote(request.query_params['query_all'])
            start_time = time.time()
            queryset = models.LanguageResource.objects.filter(
                (Q(resource_name__icontains=decoded_query_params) |
                 Q(resource_short_name__icontains=decoded_query_params))
                & lookup_query_conditions(request.user)).order_by('resource_name')
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
            serializer = serializers.GenericLanguageResourceSerializer(queryset, many=True)
            start_time = time.time()
            return_list = list()
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['resource_name'],
                                            instance.get('resource_short_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            LOGGER.info("---Process query in %s seconds ---" % (time.time() - start_time))
            return_dict['all'] = return_list
        if 'corpus' in request.GET and len(request.query_params['corpus']) > 2:
            decoded_query_params = unquote(request.query_params['corpus'])
            queryset = models.LanguageResource.objects.filter(
                (Q(resource_name__icontains=decoded_query_params) |
                 Q(resource_short_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True) |
                   (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                    Q(describedentity_ptr__languageresource__lr_subclass__lr_type='Corpus') &
                    Q(
                        describedentity_ptr__metadata_record_of_described_entity__management_object__status='p') &
                    Q(
                        describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False)))).order_by(
                'resource_name')
            serializer = serializers.GenericLanguageResourceSerializer(queryset, many=True)
            return_list = []
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['resource_name'],
                                            instance.get('resource_short_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return_dict['corpus'] = return_list
        if 'tool_service' in request.GET and len(request.query_params['tool_service']) > 2:
            decoded_query_params = unquote(request.query_params['tool_service'])
            queryset = models.LanguageResource.objects.filter(
                (Q(resource_name__icontains=decoded_query_params) |
                 Q(resource_short_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True) |
                   (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                    Q(describedentity_ptr__languageresource__lr_subclass__lr_type='ToolService') &
                    Q(
                        describedentity_ptr__metadata_record_of_described_entity__management_object__status='p')
                    & Q(
                               describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False)))).order_by(
                'resource_name')
            serializer = serializers.GenericLanguageResourceSerializer(queryset, many=True)
            return_list = []
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['resource_name'],
                                            instance.get('resource_short_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return_dict['tool_service'] = return_list
        if 'lcr' in request.GET and len(request.query_params['lcr']) > 2:
            decoded_query_params = unquote(request.query_params['lcr'])
            queryset = models.LanguageResource.objects.filter(
                (Q(resource_name__icontains=decoded_query_params) |
                 Q(resource_short_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True) |
                   (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                    Q(describedentity_ptr__languageresource__lr_subclass__lr_type='LexicalConceptualResource') &
                    Q(
                        describedentity_ptr__metadata_record_of_described_entity__management_object__status='p')
                    & Q(
                               describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False)))).order_by(
                'resource_name')
            serializer = serializers.GenericLanguageResourceSerializer(queryset, many=True)
            return_list = []
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['resource_name'],
                                            instance.get('resource_short_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return_dict['lcr'] = return_list
        if 'ld' in request.GET and len(request.query_params['ld']) > 2:
            decoded_query_params = unquote(request.query_params['ld'])
            queryset = models.LanguageResource.objects.filter(
                (Q(resource_name__icontains=decoded_query_params) |
                 Q(resource_short_name__icontains=decoded_query_params))
                & (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True) |
                   (Q(describedentity_ptr__metadata_record_of_described_entity__isnull=False) &
                    Q(describedentity_ptr__languageresource__lr_subclass__lr_type='LanguageDescription') &
                    Q(
                        describedentity_ptr__metadata_record_of_described_entity__management_object__status='p')
                    & Q(
                               describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False)))).order_by(
                'resource_name')
            serializer = serializers.GenericLanguageResourceSerializer(queryset, many=True)
            return_list = []
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['resource_name'],
                                            instance.get('resource_short_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return_dict['ld'] = return_list
        if len(return_dict) > 0:
            return Response(return_dict, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class DocumentLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.Document.objects.filter(
                (Q(title__icontains=decoded_query_params) |
                 Q(alternative_title__icontains=decoded_query_params))
                & lookup_query_conditions(request.user)).order_by('title')
            serializer = serializers.GenericDocumentSerializer(queryset, many=True)
            return_list = list()
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['title'],
                                            instance.get('alternative_title'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class LicenceTermsLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 1:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.LicenceTerms.objects.filter(
                (Q(licence_terms_name__icontains=decoded_query_params) |
                 Q(licence_terms_short_name__icontains=decoded_query_params))
                & lookup_query_conditions(request.user)).order_by('-licence_terms_name')
            serializer = serializers.GenericLicenceTermsSerializer(queryset, many=True)
            return_list = list()
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['licence_terms_name'],
                                            instance.get('licence_terms_short_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class RepositoryLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.Repository.objects.filter(
                (Q(repository_name__icontains=decoded_query_params) |
                 Q(repository_additional_name__icontains=decoded_query_params))
                & lookup_query_conditions(request.user)).order_by('repository_name')
            serializer = serializers.GenericRepositorySerializer(queryset, many=True)
            return_list = list()
            for n, instance in enumerate(serializer.data):
                similarity = get_similarity(decoded_query_params,
                                            instance['repository_name'],
                                            instance.get('repository_additional_name'))
                instance['similarity'] = similarity
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class KeywordLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        all_keywords = list()
        if all(['person' in request.GET, 'lang' in request.GET]) and len(request.query_params['person']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['person'])
            queryset = models.Person.objects.filter(keyword__icontains=decoded_query_params)
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
            start_time = time.time()
            query = 'person'
        elif all(['organization' in request.GET, 'lang' in request.GET]) and len(
                request.query_params['organization']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['organization'])
            queryset = models.Organization.objects.filter(keyword__icontains=decoded_query_params)
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
            start_time = time.time()
            query = 'organization'
        elif all(['group' in request.GET, 'lang' in request.GET]) and len(request.query_params['group']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['group'])
            queryset = models.Group.objects.filter(keyword__icontains=decoded_query_params)
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
            start_time = time.time()
            query = 'group'
        elif all(['project' in request.GET, 'lang' in request.GET]) and len(request.query_params['project']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['project'])
            queryset = models.Project.objects.filter(keyword__icontains=decoded_query_params)
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
            start_time = time.time()
            query = 'project'
        elif all(['document' in request.GET, 'lang' in request.GET]) and len(request.query_params['document']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['document'])
            queryset = models.Document.objects.filter(keyword__icontains=decoded_query_params)
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
            start_time = time.time()
            query = 'document'
        elif all(['lr' in request.GET, 'lang' in request.GET]) and len(request.query_params['lr']) > 2:
            start_time = time.time()
            decoded_query_params = unquote(request.query_params['lr'])
            queryset = models.LanguageResource.objects.filter(keyword__icontains=decoded_query_params)
            LOGGER.info("---DB query in %s seconds ---" % (time.time() - start_time))
            start_time = time.time()
            query = 'lr'
        else:
            return Response({'Query Error': 'Please make a query'},
                            status=status.HTTP_400_BAD_REQUEST)
        language_id = request.query_params['lang']
        ext_time = time.time()
        for instance in queryset:
            keyword_list = instance.keyword
            if keyword_list:
                for keyword in keyword_list:
                    if language_id in keyword.keys():
                        all_keywords.append(keyword[language_id])
        all_keywords = list(set(all_keywords))
        LOGGER.info("--Keyword extraction in %s seconds ---" % (time.time() - ext_time))
        comp_time = time.time()
        return_list = [{'keyword': kw, 'similarity': SequenceMatcher(None, kw,
                                                                     request.query_params[query]).ratio()} for kw
                       in all_keywords if
                       request.query_params[query] in kw]
        LOGGER.info("--List comprehension in %s seconds ---" % (time.time() - comp_time))
        return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
        LOGGER.info("---Process query in %s seconds ---" % (time.time() - start_time))
        return Response(return_list, status.HTTP_200_OK)


class ServiceOfferedLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if all(['query' in request.GET, 'lang' in request.GET]) and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.Organization.objects.filter(
                Q(service_offered__icontains=decoded_query_params))
            return_list = list()
            services_offered = list()
            language_id = request.query_params['lang']
            for instance in queryset:
                service_offered_list = instance.service_offered
                if service_offered_list:
                    for service_offered in service_offered_list:
                        if language_id in service_offered.keys():
                            services_offered.append(service_offered[language_id])
                            return_list.append(instance)
            services_offered = list(set(services_offered))
            return_list = [{'service_offered': s_o, 'similarity': SequenceMatcher(None, s_o,
                                                                                  request.query_params[
                                                                                      'query']).ratio()} for s_o
                           in services_offered if
                           request.query_params['query'] in s_o]
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


class CorpusRegisterLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, CreateMetadataRecordPermission]

    def list(self, request, *args, **kwargs):
        if all(['query' in request.GET, 'lang' in request.GET]) and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = models.Corpus.objects.filter(
                Q(register__icontains=decoded_query_params))
            register = list()
            language_id = request.query_params['lang']
            for instance in queryset:
                register_list = instance.register
                if register_list:
                    for reg in register_list:
                        if language_id in reg.keys():
                            register.append(reg[language_id])
            register = list(set(register))
            return_list = [{'service_offered': r, 'similarity': SequenceMatcher(None, r,
                                                                                request.query_params[
                                                                                    'query']).ratio()} for r
                           in register if
                           request.query_params['query'] in r]
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)
