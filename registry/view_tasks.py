import json
import logging
import re
from collections import OrderedDict
from io import StringIO
from time import sleep

from lxml import etree
import requests
import xmltodict
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.text import get_valid_filename
from rest_framework import serializers
from rest_framework.exceptions import UnsupportedMediaType, ValidationError

from analytics.models import MetadataRecordStats
from catalogue_backend.celery import app as celery_app
from catalogue_backend.settings.private_settings import DLE_BACKEND_URL, OPENGPTX_URL
from dle.service import DLEService
from email_notifications.email_notification_utils import (
    mass_xml_upload_notification_mail,
    xml_download_notification_mail
)
from management.catalog_report_index.documents import CatalogReportDocument
from management.management_dashboard_index.documents import MyResourcesDocument
from management.models import Manager, INTERNAL
from opengptx.export import export_to_gaix
from registry.choices import MEDIA_TYPE_CHOICES, LD_SUBCLASS_CHOICES, CORPUS_SUBCLASS_CHOICES, LCR_SUBCLASS_CHOICES, \
    LINGUALITY_TYPE_CHOICES, ANNOTATION_TYPE_RECOMMENDED_CHOICES
from dle.dle_report_index.documents import map_condition_of_use, map_access_rights_statement, DLEReportDocument
from registry.models import MetadataRecord
from registry.search_indexes.documents import MetadataDocument
from registry.serializers import (
    MetadataRecordSerializer, GenericPersonSerializer
)
from registry.utils.retrieval_utils import retrieve_default_lang
from registry.utils.task_utils import order_after_serialization
from utils.ms_ontology_service.requests import get_function_categories

LOGGER = logging.getLogger(__name__)


@celery_app.task(name='batch_xml_upload')
def batch_xml_upload(upload_groups, xml_import_errors, request_user_id, harvest, u_c_header, f_s_header, s_c_d_header):
    context = {}
    if f_s_header:
        context = {'functional_service': True}
    if s_c_d_header:
        context = {'service_compliant_dataset': True}
    final_response = []
    request_user = get_user_model().objects.get(id=request_user_id)
    request_person = GenericPersonSerializer(
        instance=request_user.person)
    if not request_user.person:
        request_person = GenericPersonSerializer(
            data={
                'given_name': {'en': request_user.username},
                'surname': {'en': request_user.username}
            }
        )
        request_person.is_valid()
        request_person.save()
    return_errors = []
    for member in upload_groups:
        errors = []
        try:
            # LOGGER.debug(xml_data)
            if not member.get('metadata_record'):
                continue
            xml_data = OrderedDict(order_after_serialization(member['metadata_record']))
            try:
                serializer = MetadataRecordSerializer(
                    xml_data=xml_data, context=context)

            except ValidationError as err:
                errors.append(err.detail)
                return_errors.append({member['metadata']: err.detail})
                continue

            # Add request user to metadata_curator
            serializer.initial_data['metadata_curator'] = [
                request_person.data]
            # Add request user to metadata_creator if not from internal harvesting
            if not harvest:
                serializer.initial_data['metadata_creator'] = request_person.data
            # Create management_object
            serializer.initial_data['management_object'] = dict()
            # Assign request.user as curator
            serializer.initial_data['management_object']['curator'] = request_user.pk
            # Assign status
            serializer.initial_data['management_object']['status'] = INTERNAL
            # Assign under construction status
            if u_c_header and serializer.initial_data['described_entity']['entity_type'] == 'LanguageResource':
                serializer.initial_data['management_object']['under_construction'] = True
            # Handle functional service header
            if f_s_header and (
                    serializer.initial_data['described_entity']['entity_type'] == 'LanguageResource'
                    and serializer.initial_data['described_entity']['lr_subclass']['lr_type'] == 'ToolService'):
                serializer.initial_data['management_object']['functional_service'] = True
            # Handle functional service header
            if s_c_d_header and (
                    serializer.initial_data['described_entity']['entity_type'] == 'LanguageResource'
                    and serializer.initial_data['described_entity']['lr_subclass']['lr_type'] in [
                        'LexicalConceptualResource', 'Corpus']):
                serializer.initial_data['management_object']['service_compliant_dataset'] = True
            if not serializer.is_valid():
                errors.append(serializer.errors)
                return_errors.append({member['metadata']: serializer.errors})
            else:
                try:
                    serializer.save()
                    # update published relations of record
                    serializer.instance.management_object.update_landing_pages_of_related_records(
                        request_user=request_user)
                except serializers.ValidationError as valerr:
                    errors.append(valerr.detail)
                    return_errors.append({member['metadata']: valerr.detail})
        except UnsupportedMediaType:
            errors.append({'File type error': 'Please import xml only.'})
        record_response = dict()
        record_response['record'] = member.get('metadata')
        record_response['metadata'] = None
        if not errors:
            pk = serializer.instance.pk
            record_response['metadata'] = {'pk': pk}
        elif errors:
            record_response['metadata'] = errors
        final_response.append(record_response)
    record_ids = []
    for uploaded_record in final_response:
        if isinstance(uploaded_record['metadata'], dict) and uploaded_record['metadata'].get('pk'):
            record_ids.append(uploaded_record['metadata'].get('pk'))
    LOGGER.info(f'[BATCH] {len(record_ids)} records imported successfully.')
    record_managers = Manager.objects.filter(metadata_record_of_manager__pk__in=record_ids)
    # instantly publish if resource is uploaded by elg-system
    if request_user.username == settings.SYSTEM_USER:
        instances = [record.metadata_record_of_manager for record in record_managers]
        for mdr in instances:
            mdr.management_object.handle_version_publication([])
            mdr.management_object.publish(force=True)
        MetadataDocument().catalogue_update(instances)
        DLEReportDocument().dle_report_update(instances)
        CatalogReportDocument().catalogue_report_update(record_managers)
    MyResourcesDocument().update(record_managers, action='index')

    content_managers = get_user_model().objects.filter(
        groups__name='content_manager'
    )
    send_to_users = [request_user]
    for c_m in content_managers:
        send_to_users.append(c_m)
    all_errors = xml_import_errors + return_errors
    mass_xml_upload_notification_mail(users=send_to_users,
                                      successful=len(record_ids),
                                      all_records=len(upload_groups),
                                      import_errors=all_errors)
    LOGGER.info(f'[BATCH] {len(record_ids)} records indexed successfully.')


def instance_to_xml_string(instance):
    dict_xml = dict()
    context = {'record_id': instance.id}
    dict_xml['ms:MetadataRecord'] = MetadataRecordSerializer(instance=instance, context=context).to_xml_representation()
    # from orderedDict to xml string
    xml_string = xmltodict.unparse(dict_xml)
    # Replace header
    xml_string = xml_string.replace(
        '<ms:MetadataRecord>',
        (
            f'<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/" '
            f'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            f'xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ {settings.XSD_URL}">'
        )
    )
    # Get project name
    filename_init = settings.PROJECT_NAME.upper()
    # Substitute entity type from str(instance) with Project name
    filename = re.sub('\[.*\]', f'{filename_init}', str(instance))
    # Replace non valid filename characters
    filename = get_valid_filename(filename + '.xml')
    return filename, xml_string


def xml_to_rdf(instance):
    dict_xml = dict()
    context = {'record_id': instance.id}
    dict_xml['ms:MetadataRecord'] = MetadataRecordSerializer(
        instance=instance, context=context).to_xml_representation(add_id=True)
    # from orderedDict to xml string
    xml_string = xmltodict.unparse(dict_xml)
    # Replace header
    xml_string = xml_string.replace(
        '<?xml version="1.0" encoding="utf-8"?>\n<ms:MetadataRecord',
        (
            f'<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/" '
            f'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            f'xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ {settings.XSD_URL}"'
        )
    )
    # convert xml to rdf
    rdf_string = xslt_convert(xml_string)
    # Get project name
    filename_init = settings.PROJECT_NAME.upper()
    # Substitute entity type from str(instance) with Project name
    filename = re.sub('\[.*\]', f'{filename_init}', str(instance))
    # Replace non valid filename characters
    filename = get_valid_filename(filename + '.rdf')
    return filename, rdf_string


def xslt_convert(xml_string):
    xslt_file = etree.XSLT(etree.parse(settings.RDF_XSLT_PATH))
    f = StringIO(xml_string)
    doc = etree.parse(f)
    result = xslt_file(doc)
    return etree.tostring(result, encoding='utf8', method='xml', pretty_print=True).decode()


@celery_app.task(name='batch_xml_retrieve')
def batch_xml_retrieve(request_user_id, requested_ids, permitted_ids, errors):
    request_user = get_user_model().objects.get(id=request_user_id)
    LOGGER.info(f'[BATCH-XML-RETRIEVE-TASK] User {request_user} requested the following '
                f'metadata records:{requested_ids}')
    LOGGER.info(f'[BATCH-XML-RETRIEVE-TASK] User {request_user} got permission for the '
                f'following metadata records:{permitted_ids}')
    LOGGER.info(f'[BATCH-XML-RETRIEVE-TASK] User {request_user} got the following errors: {errors}')
    # Retrieve permitted records
    instances = MetadataRecord.objects.filter(pk__in=permitted_ids)
    xml_records = list()
    for instance in instances:
        (filename, xml_string) = instance_to_xml_string(instance)
        xml_records.append((filename, xml_string))
    #
    # Send the email with zip
    xml_download_notification_mail(user=request_user,
                                   successful=len(permitted_ids),
                                   all_records=len(requested_ids),
                                   export_errors=errors,
                                   xml_records=xml_records
                                   )
    LOGGER.info(
        f'[BATCH-XML-RETRIEVE-TASK]: {request_user} requested {len(requested_ids)} records, export successfully '
        f'{len(permitted_ids)} and got the following errors {errors}.')
    return xml_records


@celery_app.task(name='increment_record_views')
def increment_record_views(record_id, username):
    user = get_user_model().objects.get(username=username)
    instance = MetadataRecord.objects.get(id=record_id)
    if not (user.is_superuser or instance.management_object.curator == user):
        md_stat, created = MetadataRecordStats.objects.get_or_create(metadata_record=instance)
        md_stat.increment_views()


@celery_app.task(name='compute_dle_score')
def compute_dle_score(record_id_list):
    successful_updates = []
    for record_id in record_id_list:
        instance = MetadataRecord.objects.get(id=record_id)
        if not instance.described_entity.entity_type == 'LanguageResource':
            return None
        successful_updates.append(record_id)
        # lr_type = instance.described_entity.lr_subclass.lr_type
        # if not lr_type == 'LanguageDescription':
        #     resource_type = lr_type
        # else:
        #     resource_type = LD_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.ld_subclass)
        # domain_len = len(instance.described_entity.domain.all())
        # if lr_type == 'ToolService':
        #     function_categories = {
        #         'Human Computer Interaction Collection': 0,
        #         'Information Extraction and Information Retrieval Collection': 0,
        #         'Natural Language Generation Collection': 0,
        #         'Speech Processing Collection': 0,
        #         'Support operation Collection': 0,
        #         'Text Processing Collection': 0,
        #         'Translation Technologies Collection': 0,
        #         'Image / Video Processing Collection': 0,
        #         'Other': 0,
        #     }
        #     for f in instance.described_entity.lr_subclass.function:
        #         categories = get_function_categories(f.split('/')[-1])
        #         for category in categories:
        #             if f != 'undefined':
        #                 function_categories[category] += 1
        #     request_dict = {
        #         'Software': {
        #             'resource_type': resource_type,
        #             'language_independent': not instance.described_entity.lr_subclass.language_dependent,
        #             'aggregations': {
        #                 'function': function_categories,
        #                 'domain': domain_len,
        #                 'media_type': {
        #                     'input_media_type': {
        #                         'audio': get_tool_media_type(instance, 'input', 'audio'),
        #                         'text': get_tool_media_type(instance, 'input', 'text'),
        #                         'numerical text': get_tool_media_type(instance, 'input', 'numerical text'),
        #                         'video': get_tool_media_type(instance, 'input', 'video'),
        #                         'image': get_tool_media_type(instance, 'input', 'image'),
        #                     },
        #                     'output_media_type': {
        #                         'audio': get_tool_media_type(instance, 'output', 'audio'),
        #                         'text': get_tool_media_type(instance, 'output', 'text'),
        #                         'numerical text': get_tool_media_type(instance, 'output', 'numerical text'),
        #                         'video': get_tool_media_type(instance, 'output', 'video'),
        #                         'image': get_tool_media_type(instance, 'output', 'image'),
        #                     }
        #                 },
        #                 'type_of_access': get_type_of_access(instance)
        #             }
        #         }
        #     }
        # else:
        #     request_dict = {
        #         'Dataset': {
        #             'resource_type': resource_type,
        #             'subclass': get_dataset_subclass(instance),
        #             'linguality': get_linguality_type(instance),
        #             'aggregations': {
        #                 'annotation_type': get_annotation_type_len(instance),
        #                 'domain': domain_len,
        #                 'media_type': {
        #                     'audio': get_dataset_media_type(instance, 'audio'),
        #                     'text': get_dataset_media_type(instance, 'text'),
        #                     'numerical text': get_dataset_media_type(instance, 'numerical text'),
        #                     'video': get_dataset_media_type(instance, 'video'),
        #                     'image': get_dataset_media_type(instance, 'image'),
        #                 },
        #                 'type_of_access': get_type_of_access(instance)
        #             }
        #         }
        #     }
        # dle_score_response = requests.request("POST",
        #                                       f'{DLE_BACKEND_URL}/api/dle/resource_score',
        #                                       json=request_dict,
        #                                       headers={'Authorization': f'Api-Key {settings.DLE_API_KEY}'},
        #                                       verify=False
        #                                       )
        # instance.management_object.dle_score = dle_score_response.json()
        # instance.management_object.save()
        DLEReportDocument().dle_report_update(instance)
    try:
        DLEService.notify_create(list(set(successful_updates)))
    except:
        return 'DLE service down'
    return 'dle scores computed'


def get_tool_media_type(instance, processing_resource, media_type):
    j = 0
    mapping_dict = {
        'input': instance.described_entity.lr_subclass.input_content_resource.all(),
        'output': instance.described_entity.lr_subclass.output_resource.all()
    }
    if instance.described_entity.entity_type == 'LanguageResource':
        resource_type = instance.described_entity.lr_subclass.lr_type
        try:
            if resource_type == 'ToolService':
                for _processing_resource in mapping_dict[processing_resource]:
                    if not getattr(_processing_resource, 'media_type', None):
                        continue
                    if MEDIA_TYPE_CHOICES.__getitem__(_processing_resource.media_type) == media_type:
                        j += 1
        except IndexError:
            return j
    return j


def get_dataset_media_type(instance, media_type):
    j = 0
    if instance.described_entity.entity_type == 'LanguageResource':
        resource_type = instance.described_entity.lr_subclass.lr_type
        try:
            if resource_type == 'Corpus':
                for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                    if not getattr(media_part, 'media_type', None):
                        continue
                    if MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type) == media_type:
                        j += 1
            elif resource_type == 'LexicalConceptualResource':
                for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                    if not getattr(media_part, 'media_type', None):
                        continue
                    if MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type) == media_type:
                        j += 1
            elif resource_type == 'LanguageDescription':
                for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                    if not getattr(media_part, 'media_type', None):
                        continue
                    if MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type) == media_type:
                        j += 1
        except IndexError:
            return j
    return j


def get_type_of_access(instance):
    result = list()
    if instance.described_entity.entity_type == 'LanguageResource':
        resource_type = instance.described_entity.lr_subclass.lr_type
        try:
            # Get distributions
            distributions = None
            if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                    or resource_type == 'LanguageDescription':
                distributions = instance.described_entity.lr_subclass.dataset_distribution.all()
            elif resource_type == 'ToolService':
                distributions = instance.described_entity.lr_subclass.software_distribution.all()
            for distribution in distributions:
                # Map condition_of_use
                for licence in distribution.licence_terms.all():
                    for c_o_u in licence.condition_of_use:
                        condition = map_condition_of_use(c_o_u)
                        if condition:
                            result.append(condition)
                # Map access_rights_statements
                for access_right in distribution.access_rights.all():
                    conditions = map_access_rights_statement(retrieve_default_lang(access_right.category_label))
                    if conditions:
                        for condition in conditions:
                            result.append(condition)

        except IndexError:
            pass
    len_dict = {}
    for a_r in result:
        try:
            len_dict[a_r] += 1
        except KeyError:
            len_dict[a_r] = 1
    return len_dict


def get_dataset_subclass(instance):
    if instance.described_entity.entity_type == 'LanguageResource':
        lr_type = instance.described_entity.lr_subclass.lr_type
        if lr_type == 'LanguageDescription':
            if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/grammar':
                return 'grammar'
            elif instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                return 'model'
        if lr_type == 'Corpus':
            try:
                return CORPUS_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.corpus_subclass)
            except KeyError:
                return None
        if lr_type == 'LexicalConceptualResource':
            try:
                return LCR_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.lcr_subclass)
            except KeyError:
                return None


def get_linguality_type(instance):
    # handle different cases, based on resource_type
    result = ''
    if instance.described_entity.entity_type == 'LanguageResource':
        resource_type = instance.described_entity.lr_subclass.lr_type
        try:
            if resource_type == 'Corpus':
                for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                    if not getattr(media_part, 'linguality_type', None):
                        continue
                    result = LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type)
            elif resource_type == 'LexicalConceptualResource':
                for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                    if not getattr(media_part, 'linguality_type', None):
                        continue
                    result = LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type)
            elif resource_type == 'LanguageDescription':
                for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                    if not getattr(media_part, 'linguality_type', None):
                        continue
                    result = LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type)
        except IndexError:
            return result
    return result


def get_annotation_type_len(instance):
    # handle different cases, based on resource_type
    j = 0
    if instance.described_entity.entity_type == 'LanguageResource':
        resource_type = instance.described_entity.lr_subclass.lr_type
        try:
            if resource_type == 'Corpus':
                for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                    if not getattr(media_part, 'annotation', None):
                        continue
                    else:
                        for _annotation in media_part.annotation.all():
                            for _type in _annotation.annotation_type:
                                try:
                                    if ANNOTATION_TYPE_RECOMMENDED_CHOICES.__getitem__(_type):
                                        j += 1
                                except KeyError:
                                    j += 0
        except IndexError:
            return j
    return j


@celery_app.task(name='update_landing_pages')
def update_landing_pages(instance_pk, user_pk):
    from registry.serializers import MetadataRecordSerializer
    metadata_record = MetadataRecord.objects.get(id=instance_pk)
    request_user = get_user_model().objects.filter(id=user_pk) if user_pk else None
    MetadataRecordSerializer(metadata_record).update_related_landing_pages(
        request_user=request_user)


@celery_app.task(name='push_to_gaiax')
def push_to_gaiax(record_pk):
    try:
        from registry.models import MetadataRecord
        record = MetadataRecord.objects.get(pk=record_pk)

        gaiax_data = export_to_gaix(record)
        for entry in gaiax_data:
            response = requests.post(OPENGPTX_URL, json=entry, headers={"Content-Type": "application/json"})
            print(record_pk, "Pushed to OPENGPT-X:", response.status_code, response.json())
            sleep(1)
    except Exception as e:
        LOGGER.error("Could not push record", record_pk, "to OPENGPTX", str(e))
    return None
