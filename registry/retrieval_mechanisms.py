import copy
import logging
import re

from django.conf import settings
from django.db.models import Q, Count
from rest_framework.exceptions import ValidationError

from management.models import PUBLISHED, DRAFT
from registry import models
from registry.models import (
    Person, LicenceTerms, Document, LanguageResource, Project, Repository, Organization, Group
)
from registry.models_identifier_mapping import (
    sender_mapping_relation,
    sender_mapping_scheme, sender_mapping_identifier,
    sender_mapping_reverse_relation
)
from registry.utils.retrieval_utils import field_choices_to_list

LOGGER = logging.getLogger(__name__)


def meet_conditions_to_retrieve_single_entity(instance, request_user):
    """
    Helper function to retrieve entities when only one instance has been retrieved
    """
    if (hasattr(instance, 'describedentity_ptr')
        and not hasattr(instance.describedentity_ptr, 'metadata_record_of_described_entity')) \
            or (hasattr(instance, 'describedentity_ptr')
                and hasattr(instance.describedentity_ptr, 'metadata_record_of_described_entity')
                and (instance.describedentity_ptr.metadata_record_of_described_entity.management_object.status == 'p' or
                     (instance.describedentity_ptr.metadata_record_of_described_entity.management_object.status not in [
                         'd']
                      and instance.describedentity_ptr.metadata_record_of_described_entity.management_object.curator == request_user))
                and instance.describedentity_ptr.metadata_record_of_described_entity.management_object.deleted is False):
        return True
    return False


def meet_conditions_to_retrieve_pk_of_model(instance, model_name, request_user):
    """
    Helper function to retrieve instance by pk
    """
    if f'{model_name}' in ['Domain', 'TextType', 'AudioGenre', 'SpeechGenre', 'ImageGenre',
                           'VideoGenre', 'TextGenre', 'Subject', 'AccessRightsStatement']:
        return True
    elif (hasattr(instance, 'describedentity_ptr')
          and not hasattr(instance.describedentity_ptr, 'metadata_record_of_described_entity')) \
            or (hasattr(instance, 'describedentity_ptr')
                and hasattr(instance.describedentity_ptr, 'metadata_record_of_described_entity')
                and (instance.describedentity_ptr.metadata_record_of_described_entity.management_object.status == 'p' or
                     (instance.describedentity_ptr.metadata_record_of_described_entity.management_object.status not in [
                         'd']
                      and instance.describedentity_ptr.metadata_record_of_described_entity.management_object.curator == request_user))
                and instance.describedentity_ptr.metadata_record_of_described_entity.management_object.deleted is False):
        return True
    else:
        return False


def check_entity_name(instance):
    map_name_to_instance = {
        'LanguageResource': 'resource_name',
        'LicenceTerms': 'licence_terms_name',
        'Document': 'title',
        'Person': 'given_name',
        'Organization': 'organization_name',
        'Group': 'organization_name',
        'Project': 'project_name',
        'Repository': 'repository_name',
    }
    entity_name = getattr(instance, map_name_to_instance[instance._meta.model.__name__])
    if 'untitled' in entity_name \
            and bool(re.search('\d', entity_name)):
        return False
    return True


def retrieve_by_registry_identifier(model, validated_data, request_user):
    """
    Retrieval mechanism for Entities (generic or full)
    given platform assigned identifier,
    ie: Person, Organization, Group, LanguageResource,
    Project, Document, Licence, Repository
    """
    # if validated_data has no identifier
    # if sender_mapping_relation[model.__name__] not in validated_data:
    #     return None
    if validated_data.get(sender_mapping_relation[model.__name__], None) is None:
        return None
    # Get the identifier value for ELG registry identifier from validated_data
    identifier_value = None
    vd_identifiers = validated_data[sender_mapping_relation[model.__name__]]
    for vd_id in vd_identifiers:
        if vd_id[sender_mapping_scheme[model.__name__]] == settings.REGISTRY_IDENTIFIER_SCHEMA:
            identifier_value = vd_id.get('value')
            break
    # if validated_data has no ELG registry identifier
    if not identifier_value:
        return None
    # Get all instances of model's identifiers
    model_id = sender_mapping_identifier[model.__name__]
    query_filter = dict()
    query_filter[sender_mapping_scheme[model.__name__]] = settings.REGISTRY_IDENTIFIER_SCHEMA
    query_filter['value'] = identifier_value
    model_id = model_id.objects.filter(**query_filter).first()
    if model_id is not None:
        instance = getattr(model_id,
                           sender_mapping_reverse_relation[model.__name__])
        if meet_conditions_to_retrieve_single_entity(instance, request_user) and check_entity_name(instance):
            LOGGER.info(
                'Retrieved instance {} for model {} given registry id {}'.format(
                    instance.pk, model.__name__,
                    identifier_value))
            return instance
    return None


def retrieve_by_non_registry_identifier(model, validated_data, request_user):
    """
    Retrieval mechanism for Entities (generic or full)
    given non-registry identifier
    ie: Person, Organization, Group, LanguageResource,
    Project, Document, Licence
    """
    # if validated_data has no identifier
    if validated_data.get(sender_mapping_relation[model.__name__], None) is None:
        return None
    valid_schema = field_choices_to_list(model.__name__)
    vd_identifiers = validated_data[sender_mapping_relation[model.__name__]]
    for vd_id in vd_identifiers:
        if vd_id[sender_mapping_scheme[model.__name__]] == settings.REGISTRY_IDENTIFIER_SCHEMA \
                or vd_id[sender_mapping_scheme[model.__name__]] not in valid_schema \
                or not vd_id.get('value'):
            return None
    # Get all instances of model's identifiers
    model_id = sender_mapping_identifier[model.__name__]
    for vd_id in vd_identifiers:
        query_filter = dict()
        query_filter[sender_mapping_scheme[model.__name__]] = vd_id[sender_mapping_scheme[model.__name__]]
        query_filter['value'] = vd_id['value']
        model_identifier = model_id.objects.filter(**query_filter).first()
        if model_identifier is not None:
            instance = getattr(model_identifier,
                               sender_mapping_reverse_relation[model.__name__])
            if meet_conditions_to_retrieve_single_entity(instance, request_user) and check_entity_name(instance):
                LOGGER.info(
                    'Retrieved instance {} for model {} given non-registry registry id {}'.format(
                        instance.pk, model.__name__,
                        vd_id['value']))
                return instance
    return None


def retrieve_person_by_email(validated_data, request_user):
    """
    Retrieval mechanism for Person given email address
    """
    # if validated_data has email
    if validated_data.get('email', None) is None:
        return None
    # Get email to retrieve
    emails = validated_data['email']
    if not emails:
        return None
    for email in emails:
        instances = Person.objects.filter(email__icontains=email)
        if instances:
            full_instances = instances.filter(
                (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
                 (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
                  ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
                Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
            if full_instances:
                instance = full_instances.first()
                LOGGER.info(
                    'Retrieved instance {} for model {} given email {}'.format(
                        instance.pk, Person.__name__, email))
                return instance
            generic_instances = instances.filter(
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
            if generic_instances:
                instance = generic_instances.first()
                LOGGER.info(
                    'Retrieved instance {} for model {} given email {}'.format(
                        instance.pk, Person.__name__, email))
                return instance
    return None


def retrieve_person_by_full_name(validated_data, request_user):
    """
    Retrieval mechanism for Person given surname and name
    """
    # Get given_name and surname to retrieve
    if validated_data.get('surname', None) is None or validated_data.get('given_name', None) is None:
        return None
    surname = validated_data['surname']
    given_name = validated_data['given_name']
    if not surname or not given_name:
        return None
    # matches the english surname and given_name
    instances = Person.objects.filter(surname__en__iexact=surname['en'],
                                      given_name__en__iexact=given_name['en'])
    if instances:
        full_instances = instances.filter(
            (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
             (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
              ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
            Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
        if full_instances:
            instance = full_instances.first()
            LOGGER.info(
                'Retrieved instance {} for model {} given full name {} {}'.format(
                    instance.pk, Person.__name__, instance.given_name['en'], instance.surname['en']))
            return instance
        generic_instances = instances.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        if generic_instances:
            instance = generic_instances.first()
            LOGGER.info(
                'Retrieved instance {} for model {} given full name {} {}'.format(
                    instance.pk, Person.__name__, instance.given_name['en'], instance.surname['en']))
            return instance
    return None


def retrieve_document_by_title(validated_data, request_user):
    """
    Retrieval mechanism for Document given title
    """
    if validated_data.get('title', None) is None:
        return None
    # Get title to retrieve
    title = validated_data['title']
    if not title:
        return None
    # matches the english title
    instances = Document.objects.filter(title__en__iexact=title['en'])
    if instances:
        full_instances = instances.filter(
            (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
             (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
              ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
            Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
        if full_instances:
            instance = full_instances.first()
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved instance {} for model {} given title {}'.format(
                        instance.pk, Document.__name__, instance.title['en']))
                return instance
        generic_instances = instances.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        if generic_instances:
            instance = generic_instances.first()
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved instance {} for model {} given title {}'.format(
                        instance.pk, Document.__name__, instance.title['en']))
                return instance
    return None


def retrieve_model_by_website(model, validated_data, request_user):
    """
    Retrieval mechanism for Project, Organization, Group given website
    """
    # if validated_data has website
    if validated_data.get('website', None) is None:
        return None
    # Get website to retrieve
    websites = validated_data['website']
    if not websites:
        return None
    for website in websites:
        if website.endswith('/'):
            # stores (website url without slash) as index 0 and (website url with slash) as index 1
            pairs = [website[:-1], website]
        else:
            # stores (website url without slash) as index 0 and (website url with slash) as index 1
            pairs = [website, website + '/']
        # remove http(s) from the query link
        http_pattern = re.compile(r"https?://?")
        for i, link in enumerate(pairs):
            pairs[i] = http_pattern.sub('', link).strip()
        # query for model instance that contains website url without slash
        # (will return all instances that contain said url in any form)
        instances = model.objects.filter(website__icontains=pairs[0])
        if instances:
            full_instances = instances.filter(
                (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
                 (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
                  ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
                Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
            if full_instances:
                for instance in full_instances:
                    retrieved_websites = copy.deepcopy(instance.website)
                    for i, link in enumerate(retrieved_websites):
                        retrieved_websites[i] = http_pattern.sub('', link).strip()
                    # make sure that the instance, that contains the exact website url, either with or without slash,
                    # is retrieved
                    if instance is not None and set(pairs).intersection(set(retrieved_websites)) and check_entity_name(instance):
                        LOGGER.info(
                            'Retrieved instance {} for model {} given website {}'.format(
                                instance.pk, model.__name__, website))
                        return instance
            generic_instances = instances.filter(
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
            if generic_instances:
                for instance in generic_instances:
                    retrieved_websites = copy.deepcopy(instance.website)
                    for i, link in enumerate(retrieved_websites):
                        retrieved_websites[i] = http_pattern.sub('', link).strip()
                    # make sure that the instance, that contains the exact website url, either with or without slash,
                    # is retrieved
                    if instance is not None and set(pairs).intersection(set(retrieved_websites)) and check_entity_name(instance):
                        LOGGER.info(
                            'Retrieved instance {} for model {} given website {}'.format(
                                instance.pk, model.__name__, website))
                        return instance
    return None


def retrieve_model_by_organization_name(model, validated_data, request_user):
    """
    Retrieval mechanism for Organization, Group given organization name
    """
    if validated_data.get('organization_name', None) is None:
        return None
    # Get organization_name to retrieve
    name = validated_data['organization_name']
    if not name:
        return None
    parent_organizations = validated_data.get('is_division_of')

    # matches the english name
    instances = model.objects.filter(organization_name__icontains=name['en'])
    matches = []
    if instances:
        # Retrieve published, not deleted full metadata records of organization
        full_instances = instances.filter(
            (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
             (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
              ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
            Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
        has_parent = False
        if model.__name__ == 'Organization':
            if parent_organizations is not None and len(parent_organizations) > 0:
                # To reduce the search space, first retrieve those organizations
                # with just the same length of is_division_of
                full_instances = full_instances.annotate(
                    c=Count('is_division_of')).filter(c=len(parent_organizations))
                parent_matches = []
                for parent_org in parent_organizations:
                    full_instances = full_instances.filter(
                        Q(is_division_of__organization_name__icontains=parent_org['organization_name']['en']))
                    for org in full_instances:
                        for parent in org.is_division_of.all():
                            for k, v in parent.organization_name.items():
                                if v.lower().strip() == parent_org['organization_name']['en'].lower().strip():
                                    parent_matches.append(org)
                matches = set([org for org in parent_matches for k, v in org.organization_name.items() if v == name['en']])
                has_parent = True
            else:
                full_instances = full_instances.filter(
                    Q(is_division_of__isnull=True))
        if not has_parent:
            matches = set([org for org in full_instances for k, v in org.organization_name.items() if v == name['en']])
        published_instance_list = [org for org in matches]
        if published_instance_list:
            instance = published_instance_list[0]
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved full instance {} for model {} given organization name {}'.format(
                        instance.pk, model.__name__, instance.organization_name['en']))
                return instance
        # Retrieve generic instances of organization
        generic_instances = instances.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        if model.__name__ == 'Organization':
            if parent_organizations is not None and len(parent_organizations) > 0:
                # To reduce the search space, first retrieve those organizations
                # with just the same length of is_division_of
                generic_instances = generic_instances.annotate(
                    c=Count('is_division_of')).filter(c=len(parent_organizations))
                parent_matches = []
                for parent_org in parent_organizations:
                    generic_instances = generic_instances.filter(
                        Q(is_division_of__organization_name__en=parent_org['organization_name']['en']))
                    for org in generic_instances:
                        for parent in org.is_division_of.all():
                            for k, v in parent.organization_name.items():
                                if v.lower().strip() == parent_org['organization_name']['en'].lower().strip():
                                    parent_matches.append(org)
                matches = set([org for org in parent_matches for k, v in org.organization_name.items()
                               if v.lower().strip() == name['en'].lower().strip()])
            else:
                generic_instances = generic_instances.filter(
                    Q(is_division_of__isnull=True))
                matches = set([org for org in generic_instances for k, v in org.organization_name.items()
                               if v.lower().strip() == name['en'].lower().strip()])
        generic_instance_list = [org for org in matches]
        if generic_instance_list:
            instance = generic_instance_list[0]
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved generic instance {} for model {} given organization name {}'.format(
                        instance.pk, model.__name__, instance.organization_name['en']))
                return instance
    return None


def retrieve_lr_by_resource_name(validated_data, request_user):
    """
    Retrieval mechanism for Document given title
    """
    if validated_data.get('resource_name', None) is None:
        return None
    # Get title to retrieve
    lr_name = validated_data['resource_name']
    if not lr_name:
        return None
    version = validated_data.get('version')
    instances = LanguageResource.objects.filter(resource_name__icontains=lr_name['en'])
    if instances:
        full_instances = instances.filter(
            (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
             (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
              ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
            Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
        if full_instances:
            if version:
                retrieved_instance = full_instances.filter(Q(version=version))
            else:
                retrieved_instance = full_instances.filter(Q(version='1.0.0 (automatically assigned)'))
            matches = set([lr for lr in retrieved_instance for k, v in lr.resource_name.items()
                           if v.lower().strip() == lr_name['en'].lower().strip()])
            retrieved_instance_list = [lr for lr in matches]
            if retrieved_instance_list:
                instance = retrieved_instance_list[0]
                if check_entity_name(instance):
                    LOGGER.info(
                        'Retrieved instance {} for model {} given title {}'.format(
                            instance.pk, LanguageResource.__name__, instance.resource_name['en']))
                    return instance
        generic_instances = instances.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        if generic_instances:
            if version:
                retrieved_instances = generic_instances.filter(Q(version=version))
            else:
                retrieved_instances = generic_instances.all()
            matches = set([lr for lr in retrieved_instances for k, v in lr.resource_name.items()
                           if v.lower().strip() == lr_name['en'].lower().strip()])
            retrieved_instance_list = [lr for lr in matches]
            if retrieved_instance_list:
                instance = retrieved_instance_list[0]
                if check_entity_name(instance):
                    LOGGER.info(
                        'Retrieved instance {} for model {} given title {}'.format(
                            instance.pk, LanguageResource.__name__, instance.resource_name['en']))
                    return instance
    return None


def retrieve_project_by_grant_number_or_name(validated_data, request_user):
    """
    Retrieval mechanish for Project givern grant number or name
    """
    if validated_data.get('project_name', None) is None:
        return None
    if validated_data.get('grant_number', None):
        grant = validated_data['grant_number']
        instances = Project.objects.filter(grant_number__iregex=r'\y{}\y'.format(grant))
        if instances:
            full_instances = instances.filter(
                (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
                 (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
                  ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
                Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
            if full_instances:
                instance = full_instances.first()
                if check_entity_name(instance):
                    LOGGER.info(
                        'Retrieved instance {} for model {} given grant number {}'.format(
                            instance.pk, Project.__name__, instance.grant_number))
                    return instance
            generic_instances = instances.filter(
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
            if generic_instances:
                instance = generic_instances.first()
                if check_entity_name(instance):
                    LOGGER.info(
                        'Retrieved instance {} for model {} given grant number {}'.format(
                            instance.pk, Project.__name__, instance.grant_number))
                    return instance
    project_name = validated_data['project_name']
    if not project_name:
        return None
    instances = Project.objects.filter(project_name__icontains=project_name['en'])
    if instances:
        full_instances = instances.filter(
            (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
             (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
              ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
            Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
        matches = set([proj for proj in full_instances for k, v in proj.project_name.items() if v.lower().strip() == project_name['en'].lower().strip()])
        published_instance_list = [proj for proj in matches]
        if published_instance_list:
            instance = published_instance_list[0]
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved instance {} for model {} given project name {}'.format(
                        instance.pk, Project.__name__, instance.project_name['en']))
                return instance
        generic_instances = instances.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        matches = set([proj for proj in generic_instances for k, v in proj.project_name.items() if v.lower().strip() == project_name['en'].lower().strip()])
        generic_instance_list = [proj for proj in matches]
        if generic_instance_list:
            instance = generic_instance_list[0]
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved instance {} for model {} given project name {}'.format(
                        instance.pk, Project.__name__, instance.project_name['en']))
                return instance
    return None


def retrieve_licence_by_licence_terms_and_name(validated_data, request_user):
    """
    Retrieval mechanism for Licence given licence terms and name
    """
    # if validated_data has licence_terms
    if validated_data.get('licence_terms_url', None) is None:
        return None
    # Get licence_terms_url to retrieve
    licence_terms_urls = validated_data['licence_terms_url']
    # Get licence_terms_name to retrieve
    licence_terms_name = validated_data['licence_terms_name']['en'] if validated_data.get('licence_terms_name') else ''
    if not licence_terms_urls:
        return None
    for url in licence_terms_urls:
        cc_lang = ''
        if 'creativecommons' in url:
            cc = url.strip().split('/')
            for i, slug in enumerate(list(reversed(cc[-3:]))):
                _i = -(i + 1)
                if len(slug) == 2 and slug != 'by':
                    cc_lang = cc.pop(_i)
                    break

            url = '/'.join([slug for slug in cc if slug]).replace(':', ':/')
        if url.endswith('/'):
            # stores (licence term url without slash) as index 0 and (licence term url with slash) as index 1
            pairs = [url[:-1], url]
        else:
            # stores (licence term url without slash) as index 0 and (licence term url with slash) as index 1
            pairs = [url, url + '/']
        if 'creativecommons' in url:
            pairs.extend([url + '/' + cc_lang, url + '/' + cc_lang + '/'])
        # remove http(s) from the query link
        http_pattern = re.compile(r"https?://?")
        for i, link in enumerate(pairs):
            pairs[i] = http_pattern.sub('', link).strip().lower()
        # query for model instance that contains website url without slash
        # (will return all instances that contain said url in any form)
        instances = LicenceTerms.objects.filter(licence_terms_url__icontains=pairs[0])
        if len(instances) == 1 and instances.first() is not None \
                and meet_conditions_to_retrieve_single_entity(instances.first(), request_user) and check_entity_name(instances.first()):
            # remove http(s) from the retrieved links
            retrieved_urls = [http_pattern.sub('', lt_url).strip().lower() for lt_url in instances.first().licence_terms_url]
            # make sure that the instance, that contains the exact licence_term url, either with or without slash,
            # is retrieved
            if set(pairs).intersection(set(retrieved_urls)):
                LOGGER.info(
                    'Retrieved instance {} for model {} given licence terms url {}'.format(
                        instances.first().pk,
                        LicenceTerms.__name__, url))
                return instances.first()
        elif len(instances) > 1:
            full_instances = instances.filter(
                (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
                 (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
                  ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
                Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
            if full_instances:
                retrieved_urls = []
                for instance in full_instances:
                    instance_pk = instance.pk
                    retrieved_urls.extend([(http_pattern.sub('', lt_url).strip().lower(), instance_pk)
                                      for lt_url in instance.licence_terms_url])
                    # make sure that the instance, that contains the exact licence_term url, either with or without slash,
                    # is retrieved
                if retrieved_urls:
                    ret_urls = [_l[0] for _l in retrieved_urls]
                    _intersection = set(pairs).intersection(set(ret_urls))
                    ret_instances = [_l[1] for _l in retrieved_urls if _l[0] in _intersection]
                    if len(ret_instances) == 1:
                        instance = full_instances.get(pk__in=ret_instances)
                        if check_entity_name(instance):
                            LOGGER.info(
                                'Retrieved instance {} for model {} given licence terms url {} and licence terms name {}'.format(
                                    instance.pk,
                                    LicenceTerms.__name__,
                                    url, instance.licence_terms_name['en']))
                            return instance
                    else:
                        same_url_licences = full_instances.filter(pk__in=ret_instances)
                        same_name_licences = []
                        for instance in same_url_licences:
                            licence_terms_name_match = \
                                licence_terms_name and licence_terms_name.lower().strip() == \
                                instance.licence_terms_name['en'].lower().strip()
                            if instance is not None \
                                    and licence_terms_name_match:
                                same_name_licences.append(instance)
                        if same_name_licences:
                            instance = same_name_licences[0]
                        else:
                            instance = same_url_licences.first()
                            for _instance in same_url_licences:
                                if _instance.pk > instance.pk:
                                    instance = _instance
                        if instance and check_entity_name(instance):
                            LOGGER.info(
                                'Retrieved instance {} for model {} given licence terms url {} and licence terms name {}'.format(
                                    instance.pk,
                                    LicenceTerms.__name__,
                                    url, instance.licence_terms_name['en']))
                            return instance
            generic_instances = instances.filter(
                Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
            if generic_instances:
                retrieved_urls = []
                for instance in generic_instances:
                    instance_pk = instance.pk
                    retrieved_urls.extend([(http_pattern.sub('', lt_url).strip().lower(), instance_pk)
                                      for lt_url in instance.licence_terms_url])
                # make sure that the instance, that contains the exact licence_term url, either with or without slash,
                # is retrieved
                if retrieved_urls:
                    ret_urls = [_l[0] for _l in retrieved_urls]
                    _intersection = set(pairs).intersection(set(ret_urls))
                    ret_instances = [_l[1] for _l in retrieved_urls if _l[0] in _intersection]
                    if len(ret_instances) == 1:
                        instance = generic_instances.get(pk__in=ret_instances)
                        if check_entity_name(instance):
                            LOGGER.info(
                                'Retrieved instance {} for model {} given licence terms url {} and licence terms name {}'.format(
                                    instance.pk,
                                    LicenceTerms.__name__,
                                    url, instance.licence_terms_name['en']))
                            return instance
                    else:
                        same_url_licences = generic_instances.filter(pk__in=ret_instances)
                        same_name_licences = []
                        for instance in same_url_licences:
                            licence_terms_name_match = \
                                licence_terms_name and licence_terms_name.lower().strip() == \
                                instance.licence_terms_name['en'].lower().strip()
                            if instance is not None \
                                    and licence_terms_name_match:
                                same_name_licences.append(instance)
                        if same_name_licences:
                            instance = same_name_licences[0]
                        else:
                            instance = same_url_licences.first()
                            for _instance in same_url_licences:
                                if _instance.pk > instance.pk:
                                    instance = _instance
                        if instance and check_entity_name(instance):
                            LOGGER.info(
                                'Retrieved instance {} for model {} given licence terms url {} and licence terms name {}'.format(
                                    instance.pk,
                                    LicenceTerms.__name__,
                                    url, instance.licence_terms_name['en']))
                            return instance
    return None


def retrieve_licence_terms_given_only_name(validated_data, request_user):
    """
    Retrieval mechanism for LicenceTerms given name
    """
    # if validated_data has licence_terms
    if validated_data.get('licence_terms_name', None) is None:
        return None
    # Get licence_term_name to retrieve
    term_name = validated_data['licence_terms_name']
    if not term_name:
        return None
    instances = LicenceTerms.objects.filter(licence_terms_name__en__iexact=term_name['en'])
    if instances:
        full_instances = instances.filter(
            (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
             (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
              ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
            Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
        if full_instances:
            instance = full_instances.first()
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved instance {} for model {} given licence term name {}'.format(
                        instance.pk, LicenceTerms.__name__, instance.licence_terms_name['en']))
                return instance
        generic_instances = instances.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        if generic_instances:
            instance = generic_instances.first()
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved instance {} for model {} given licence term name {}'.format(
                        instance.pk, LicenceTerms.__name__, instance.licence_terms_name['en']))
                return instance
    return None


def retrieve_repository_by_repository_url_and_name(validated_data, request_user):
    """
    Retrieval mechanism for Repository given repository url and name
    """
    # if validated_data has repository_url
    if validated_data.get('repository_url', None) is None:
        return None
    # Get repository_url to retrieve
    repo_url = validated_data['repository_url']
    # Get repository_name to retrieve
    repo_name = validated_data['repository_name']['en'] if validated_data.get('repository_name') else ''
    if not repo_url:
        return None
    if repo_url.endswith('/'):
        # stores (repository_url without slash) as index 0 and (repository_url with slash) as index 1
        pairs = [repo_url[:-1], repo_url]
    else:
        # stores (repository_url without slash) as index 0 and (repository_url with slash) as index 1
        pairs = [repo_url, repo_url + '/']
    # remove http(s) from the query link
    http_pattern = re.compile(r"https?://?")
    for i, link in enumerate(pairs):
        pairs[i] = http_pattern.sub('', link).strip()
    # query for model instance that contains website url without slash
    # (will return all instances that contain said url in any form)
    instances = Repository.objects.filter(repository_url__icontains=pairs[0])
    if len(instances) == 1 and instances.first() is not None \
            and meet_conditions_to_retrieve_single_entity(instances.first(), request_user) and check_entity_name(instances.first()):
        # remove http(s) from the retrieved links
        retrieved_urls = list(copy.deepcopy(instances.first().repository_url))
        for i, link in enumerate(retrieved_urls):
            retrieved_urls[i] = http_pattern.sub('', link).strip()
        # make sure that the instance, that contains the exact repository_url, either with or without slash,
        # is retrieved
        if set(pairs).intersection(set(retrieved_urls)):
            LOGGER.info(
                'Retrieved instance {} for model {} given repository url {}'.format(
                    instances.first().pk,
                    Repository.__name__, repo_url))
            return instances.first()
    elif len(instances) > 1:
        full_instances = instances.filter(
            (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
             (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
              ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
            Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
        if full_instances:
            for instance in full_instances:
                retrieved_urls = list(copy.deepcopy(instance.repo_url))
                for i, link in enumerate(retrieved_urls):
                    retrieved_urls[i] = http_pattern.sub('', link).strip()
                # make sure that the instance, that contains the exact repo url, either with or without slash,
                # is retrieved
                if instance is not None \
                        and set(pairs).intersection(set(retrieved_urls)) \
                        and (repo_name and repo_name.lower().strip() == instance.repository_name['en'].lower().strip()) and check_entity_name(instance):
                    LOGGER.info(
                        'Retrieved instance {} for model {} given repository url {} and repository name {}'.format(
                            instance.pk,
                            LicenceTerms.__name__,
                            repo_url, instance.repository_name['en']))
                    return instance
        generic_instances = instances.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        if generic_instances:
            for instance in generic_instances:
                retrieved_urls = list(copy.deepcopy(instance.repository_url))
                for i, link in enumerate(retrieved_urls):
                    retrieved_urls[i] = http_pattern.sub('', link).strip()
                # make sure that the instance, that contains the exact repo url, either with or without slash,
                # is retrieved
                if instance is not None \
                        and set(pairs).intersection(set(retrieved_urls)) \
                        and (repo_name and repo_name.lower().strip() == instance.repository_name['en'].lower().strip()) and check_entity_name(instance):
                    LOGGER.info(
                        'Retrieved instance {} for model {} given repository url {} and repository name {}'.format(
                            instance.pk,
                            Repository.__name__,
                            repo_url, instance.repository_name['en']))
                    return instance
    return None


def retrieve_repository_given_only_name(validated_data, request_user):
    """
    Retrieval mechanism for Repository given name
    """
    # if validated_data has repository name
    if validated_data.get('repository_name', None) is None:
        return None
    # Get repository_name to retrieve
    term_name = validated_data['repository_name']
    if not term_name:
        return None
    instances = Repository.objects.filter(repository_name__en__iexact=term_name['en'])
    if instances:
        full_instances = instances.filter(
            (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=PUBLISHED) |
             (Q(describedentity_ptr__metadata_record_of_described_entity__management_object__curator=request_user) &
              ~Q(describedentity_ptr__metadata_record_of_described_entity__management_object__status=DRAFT))),
            Q(describedentity_ptr__metadata_record_of_described_entity__management_object__deleted=False))
        if full_instances:
            instance = full_instances.first()
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved instance {} for model {} given repository name {}'.format(
                        instance.pk, Repository.__name__, instance.repository_name['en']))
                return instance
        generic_instances = instances.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        if generic_instances:
            instance = generic_instances.first()
            if check_entity_name(instance):
                LOGGER.info(
                    'Retrieved instance {} for model {} given repository name {}'.format(
                        instance.pk, Repository.__name__, instance.repository_name['en']))
                return instance
    return None


def retrieve_model_by_all_elements(model, validated_data):
    """
    Retrieval mechanism for Domain, TextType, AudioGenre, SpeechGenre, ImageGenre,
    VideoGenre, TextGenre, Subject and AccessRightsStatements
    """
    allowed_models = [models.Domain.__name__, models.TextType.__name__,
                      models.AudioGenre.__name__, models.SpeechGenre.__name__, models.ImageGenre.__name__,
                      models.VideoGenre.__name__, models.TextGenre.__name__, models.Subject.__name__,
                      models.AccessRightsStatement.__name__]
    assert model.__name__ in allowed_models, '{} is not in allowed models {}'.format(
        model.__name__, allowed_models)
    if not validated_data.get('category_label', None):
        return None
    query_filter = dict()
    query_filter['category_label__en__iregex'] = r'^\s*{}\s*$'.format(
        validated_data['category_label']['en'].strip())
    identifier_relation = sender_mapping_relation[model.__name__]
    if validated_data.get(identifier_relation, None):
        query_filter[identifier_relation + '__value'] = \
            validated_data[identifier_relation].get('value')
        identifier_scheme = sender_mapping_scheme[model.__name__]
        query_filter[identifier_relation + '__' + identifier_scheme] = \
            validated_data[identifier_relation].get(identifier_scheme)
    instance = model.objects.filter(**query_filter).first()
    if instance is not None:
        LOGGER.info(
            'Retrieved instance {} for model {} given all elements'.format(
                instance.pk, model.__name__))
        return instance
    return None


def retrieve_model_by_pk(model, initial_data, request_user):
    """
    Retrieval mechanism for Entities from lookup mechanisms (generic or full)
    given pk ie: Person, Organization, Group, LanguageResource,
    Project, Document, Licence
    """
    # if validated_data has no identifier
    if initial_data.get('pk', None) is None:
        return None
    # Get all instances of model's identifiers
    pk = initial_data.get('pk')
    if not pk:
        return None
    instance = model.objects.filter(pk=pk).first()
    if instance is not None and meet_conditions_to_retrieve_pk_of_model(instance, model.__name__, request_user):
        LOGGER.info(
            'Retrieved instance {} for model {} given instance id {}'.format(
                instance.pk, model.__name__,
                initial_data['pk']))
        return instance
    raise ValidationError
