from django import forms
from django.apps import apps
from django.forms import Select
from model_utils import Choices


class IntermediateUserSelectForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, choices=None, *args, **kwargs):
        super(IntermediateUserSelectForm, self).__init__(*args, **kwargs)
        if choices is not None:
            self.choices = choices
            self.fields['user'] = forms.ModelChoiceField(
                self.choices,
                label="User",
                widget=Select(),
                required=False
            )


class AssignCuratorForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, choices=None, *args, **kwargs):
        super(AssignCuratorForm, self).__init__(*args, **kwargs)
        if choices is not None:
            self.choices = choices
            self.fields['user'] = forms.ModelChoiceField(
                self.choices,
                label="User",
                widget=Select(),
                required=False
            )
            self.fields['for_information_only'] = forms.ChoiceField(
                choices=Choices(
                    (False, 'No'),
                    (True, 'Yes'),
                ),
                label="For Info",
                widget=Select(),
                required=False
            )


class ResolveClaimForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, *args, rejection=None, **kwargs):
        super(ResolveClaimForm, self).__init__(*args, **kwargs)
        self.rejection = rejection
        self.fields['comments'] = forms.CharField(
            label="Comments",
            max_length=1000,
            required=False
        )
        self.fields['comments'].widget.attrs['style'] = 'width: 50em;'


class AdminReasonForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, *args, **kwargs):
        super(AdminReasonForm, self).__init__(*args, **kwargs)
        self.fields['comments'] = forms.CharField(
            label="Comments",
            max_length=1000,
            required=False
        )
        self.fields['comments'].widget.attrs['style'] = 'width: 50em;'


class CsvImportForm(forms.Form):
    csv_file = forms.FileField()


# Alphabetically sorted list with (Model label, Model Verbose name) for form
app_models = list(apps.get_app_config('registry').get_models())
models_name_list = sorted([(model._meta.label, model._meta.verbose_name) for model in app_models])


class CurationCsvImportForm(forms.Form):
    action = forms.ChoiceField(
        choices=Choices(
            (None, '---------------'),
            ('Enrich', 'Enrich'),
            ('Remove', 'Remove'),
        ),
        label="Action",
        widget=Select(),
        required=True
    )
    model_type = forms.ChoiceField(
        choices=Choices(
            (None, '--------------------------------'),
            *models_name_list),
        label="Model",
        widget=Select(),
        required=True
    )
    curation_csv_file = forms.FileField()


class CurationCsvExportForm(forms.Form):
    record_id = forms.CharField(
        label="Metadata record IDs",
        required=True
    )
    model_type = forms.ChoiceField(
        choices=Choices(
            (None, '--------------------------------'),
            *models_name_list),
        label="Model",
        widget=Select(),
        required=True
    )

