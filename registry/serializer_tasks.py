import logging
from django.core.exceptions import ObjectDoesNotExist

from registry.models import MetadataRecord, GenericMetadataRecord, DescribedEntity, Organization, LRSubclass, \
    LanguageDescriptionSubclass, LanguageResource, Project

LOGGER = logging.getLogger(__name__)


class GetRelatedRecords:

    def __init__(self, instance):
        self.instance = instance
        self.pk_set = set()
        self.checked = set()
        self.first_layer_related_objects = None

    def _get_all_m2m_relations(self, instance):
        pk_list = []
        m2m_names = []
        for m2m_relation in instance._meta.many_to_many:
            m2m_names.append(m2m_relation.name)
        for name in m2m_names:
            relation = getattr(instance, name, None)
            if relation:
                try:
                    pk_list.append((name, relation.all()))
                except ObjectDoesNotExist:
                    pk_list.append((name, relation))
        return set(pk_list)

    def _get_all_related_objects_landing_page(self, instance):
        from oai.models import ZenodoRecord
        from accounts.models import ELGUser
        related_names = []
        pk_list = []
        for _relation in instance._meta.related_objects:
            related_names.append(_relation.related_name)
        related_names = [r_n for r_n in related_names if r_n]
        for name in related_names:
            relation = getattr(instance, name, None)
            if relation and not type(relation) in [MetadataRecord, ELGUser, ZenodoRecord, GenericMetadataRecord]:
                try:
                    if relation.exists():
                        for rel in relation.all():
                            if not type(rel) in [MetadataRecord, ELGUser, ZenodoRecord, GenericMetadataRecord]:
                                pk_list.append((name, rel))
                except AttributeError:
                    pk_list.append((name, relation))
        return pk_list

    def _get_all_related_objects(self, instance):
        from oai.models import ZenodoRecord
        from accounts.models import ELGUser
        related_names = []
        pk_list = []
        for _relation in instance._meta.related_objects:
            related_names.append(_relation.related_name)
        related_names = [r_n for r_n in related_names if r_n]
        for name in related_names:
            relation = getattr(instance, name, None)
            if relation and not type(relation) in [ELGUser, ZenodoRecord, GenericMetadataRecord]:
                try:
                    if relation.exists():
                        for rel in relation.all():
                            if not type(rel) in [ELGUser, ZenodoRecord, GenericMetadataRecord]:
                                pk_list.append((name, rel))
                except AttributeError:
                    pk_list.append((name, relation))
        return pk_list

    def _get_related_to_merge(self, instance):
        from oai.models import ZenodoRecord
        from accounts.models import ELGUser
        related_names = []
        pk_list = []
        for _relation in instance._meta.related_objects:
            related_names.append((_relation.related_name, _relation.field.name))
        related_names = [r_n for r_n in related_names if r_n]
        for name in related_names:
            if not name[0]:
                continue
            relation = getattr(instance, name[0], None)
            if relation and not type(relation) in [ELGUser, ZenodoRecord, GenericMetadataRecord]:
                try:
                    if relation.exists():
                        for rel in relation.all():
                            if not type(rel) in [ELGUser, ZenodoRecord, GenericMetadataRecord]:
                                pk_list.append((name[0], rel, name[1]))
                except AttributeError:
                    pk_list.append((name[0], relation, name[1]))
        return pk_list

    def _get_all_fk_relations(self, instance):
        fk_names = []
        for field in instance._meta.fields:
            if field.get_internal_type() in ['ForeignKey']:
                fk_names.append(field.name)
        return fk_names

    def _get_related_pks(self, instance):
        from oai.models import ZenodoRecord
        from accounts.models import ELGUser
        pk_list = []
        related_fk_list = self._get_all_fk_relations(instance)
        for name in related_fk_list:
            relation = getattr(instance, name, None)
            if relation:
                try:
                    if not type(relation) in [ELGUser, ZenodoRecord, GenericMetadataRecord]:
                        pk_list.append((name, relation))
                except ObjectDoesNotExist:
                    pass
        return set(pk_list)

    def get_fields_for_landing_pages(self, instance=None, first=True):
        if not instance:
            instance = self.instance
        related_fk_list = self._get_related_pks(instance)
        related_objects_set = self._get_all_related_objects_landing_page(instance)
        # TODO Change when we start publishing other entities
        if not (isinstance(instance, LanguageResource)
                or isinstance(instance, Organization)
                or isinstance(instance, Project)
                or isinstance(instance, LRSubclass)
                or isinstance(instance, LanguageDescriptionSubclass)):
            return set()
        if not first:
            for _obj in set(related_fk_list):
                if isinstance(_obj[1], DescribedEntity) \
                    or isinstance(_obj[1], LRSubclass) \
                    or isinstance(_obj[1], LanguageDescriptionSubclass):
                        related_objects_set.append(_obj)
        for _obj in set(related_objects_set):
            if _obj not in self.checked:
                self.checked.add(_obj)
                if isinstance(_obj[1], DescribedEntity):
                    if getattr(_obj[1], 'metadata_record_of_described_entity', None):
                        self.pk_set.add(_obj[1])
                        if isinstance(_obj[1], Organization) and _obj[0] == 'organizations_of_is_division_of':
                            self.get_fields_for_landing_pages(_obj[1], False)
                        continue
                    elif isinstance(_obj[1], Organization) and _obj[0] == 'organizations_of_is_division_of':
                        self.get_fields_for_landing_pages(_obj[1], False)
                    else:
                        continue
                else:
                    if not isinstance(_obj[1], MetadataRecord):
                        self.get_fields_for_landing_pages(_obj[1], False)
                    else:
                        self.pk_set.add(_obj[1].described_entity)
                        continue
        return self.pk_set

    def get_all_fields(self, instance=None, first=True):
        if not instance:
            instance = self.instance
        related_fk_list = self._get_related_pks(instance)
        related_objects_set = self._get_all_related_objects(instance)
        if not first:
            for _obj in set(related_fk_list):
                if isinstance(_obj[1], DescribedEntity) \
                    or isinstance(_obj[1], LRSubclass) \
                    or isinstance(_obj[1], LanguageDescriptionSubclass):
                        related_objects_set.append(_obj)
        for _obj in set(related_objects_set):
            if _obj not in self.checked:
                self.checked.add(_obj)
                if isinstance(_obj[1], DescribedEntity):
                    if getattr(_obj[1], 'metadata_record_of_described_entity', None):
                        self.pk_set.add(_obj[1])
                        if isinstance(_obj[1], Organization) and _obj[0] == 'organizations_of_is_division_of':
                            self.get_all_fields(_obj[1], False)
                        continue
                    elif isinstance(_obj[1], Organization) and _obj[0] == 'organizations_of_is_division_of':
                        self.get_all_fields(_obj[1], False)
                    else:
                        continue
                elif isinstance(_obj[1], MetadataRecord):
                    self.pk_set.add(_obj[1].described_entity)
                    continue
                else:
                    self.get_all_fields(_obj[1], False)
        return self.pk_set

    def get_merging_fields(self, instance=None, first=True):
        if not instance:
            instance = self.instance
        related_fk_list = self._get_related_pks(instance)
        related_objects_set = self._get_related_to_merge(instance)
        if first:
            self.first_layer_related_objects = set([(_obj[2], _obj[1]) for _obj in related_objects_set])
        else:
            for _obj in set(related_fk_list):
                if isinstance(_obj[1], DescribedEntity) \
                        or isinstance(_obj[1], LRSubclass) \
                        or isinstance(_obj[1], LanguageDescriptionSubclass):
                    related_objects_set.append(_obj)
        for _obj in set(related_objects_set):
            if _obj not in self.checked:
                self.checked.add(_obj)
                if isinstance(_obj[1], DescribedEntity):
                    if getattr(_obj[1], 'metadata_record_of_described_entity', None):
                        self.pk_set.add(_obj[1])
                        if isinstance(_obj[1], Organization) and _obj[0] == 'organizations_of_is_division_of':
                            self.get_merging_fields(_obj[1], False)
                        continue
                    elif isinstance(_obj[1], Organization) and _obj[0] == 'organizations_of_is_division_of':
                        self.get_merging_fields(_obj[1], False)
                    else:
                        continue
                elif isinstance(_obj[1], MetadataRecord):
                    self.pk_set.add(_obj[1])
                    continue
                else:
                    self.get_merging_fields(_obj[1], False)
        return self.first_layer_related_objects, self.pk_set

    def get_children_pks_of_instance(self, instance=None):
        if not instance:
            instance = self.instance
        field_pk_list = set()
        follow_fields = set()
        fields_to_check = self._get_all_m2m_relations(instance)
        fields_to_check.update(set(self._get_all_related_objects(instance)))
        for field in instance._meta.fields:
            field_name = field.name
            if field.name == 'management_object':
                continue
            relation = getattr(instance, field_name, None)
            try:
                try:
                    fields_to_check.add((field_name, relation.all()))
                except:
                    fields_to_check.add((field_name, relation))
            except:
                pass

        for field in fields_to_check:
            field_name = field[0]
            if field_name == 'management_object':
                continue
            relation = field[1]
            try:
                try:
                    for _rel in relation:
                        field_pk_list.add((field_name, _rel, _rel._meta.label))
                        if (field_name == 'described_entity' or not isinstance(_rel, DescribedEntity)) \
                                and not _rel in self.checked:
                            self.checked.add(_rel)
                            follow_fields.add(_rel)
                except:
                    field_pk_list.add((field_name, relation, relation._meta.label))
                    if (field_name == 'described_entity' or not isinstance(relation, DescribedEntity)) \
                            and not relation in self.checked:
                        self.checked.add(relation)
                        follow_fields.add(relation)
            except:
                pass

        self.pk_set.update(field_pk_list)
        for relation in follow_fields:
            self.get_children_pks_of_instance(relation)
        return self.pk_set


