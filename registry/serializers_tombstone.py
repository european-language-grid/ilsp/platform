from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from django.utils.translation import gettext_lazy as _

from management.serializers import ManagerSerializer
from registry import models
from registry.fields import (
    SerializersMultilingualField, SerializersVersionField
)
from registry.serializers_display import DisplaySerializer


class TombstoneLRSubclassSerializer(
    WritableNestedModelSerializer, DisplaySerializer):
    """
    Introduces the type of language resource that is described in
    the metadata record
    """

    class Meta:
        model = models.LRSubclass
        fields = ['lr_type']

    resource_type_field_name = 'lr_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _('LR type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]


class TombstoneLanguageResourceSerializer(
    WritableNestedModelSerializer, DisplaySerializer):
    """
    A resource composed of linguistic material used in the
    construction, improvement and/or evaluation of language
    processing applications, but also, in a broader sense, in
    language and language-mediated research studies and
    applications; the term is used with a broader meaning,
    encompassing (a) data sets (textual, multimodal/multimedia and
    lexical data, grammars, language models, etc.) in machine
    readable form, and (b) tools/technologies/services used for
    their processing and management.
    """
    resource_name = SerializersMultilingualField()
    version = SerializersVersionField(required=False)
    citation_text = SerializersMultilingualField(
        allow_null=True,
        required=False,
        read_only=True
    )
    # OneToOne relation
    lr_subclass = TombstoneLRSubclassSerializer()

    class Meta:
        model = models.LanguageResource
        fields = (
            'pk',
            'entity_type',
            'resource_name',
            'version',
            'citation_text',
            'lr_subclass',
        )

    def to_internal_value(self, data):
        # Even if empty value is provided, override with default
        if data.get('version', '') == '':
            data['version'] = self.fields.get('version').default
        return super().to_internal_value(data)


class TombstoneOrganizationSerializer(
    WritableNestedModelSerializer, DisplaySerializer):
    """
    A company or other group of people that works together for a
    particular purpose [https://dictionary.cambridge.org/dictionary/
    english/organization]
    """
    organization_name = SerializersMultilingualField()

    class Meta:
        model = models.Organization
        fields = (
            'pk',
            'entity_type',
            'organization_name',
        )


class TombstoneProjectSerializer(
    WritableNestedModelSerializer, DisplaySerializer):
    """
    A set of operations undertaken as a whole by an individual or
    organization and related to some aspect of the lifecycle of the
    language resource (e.g. funding, deployment, etc.)
    """
    project_name = SerializersMultilingualField()

    class Meta:
        model = models.Project
        fields = (
            'pk',
            'entity_type',
            'project_name',
        )


class TombstoneDescribedEntitySerializer(
    PolymorphicSerializer, WritableNestedModelSerializer,
    DisplaySerializer):
    """
    The (type of) entity that is being described by a metadata
    record
    """

    class Meta:
        model = models.DescribedEntity
        fields = '__all__'

    model_serializer_mapping = {
        models.LanguageResource: TombstoneLanguageResourceSerializer,
        models.Organization: TombstoneOrganizationSerializer,
        models.Project: TombstoneProjectSerializer,
    }

    resource_type_field_name = 'entity_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _(
                    'entity type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]


class TombstoneMetadataRecordSerializer(
    WritableNestedModelSerializer, DisplaySerializer):
    """
    A set of formalized structured information used to describe the
    contents, structure, function, etc. of an entity, usually
    according to a specific set of rules (metadata schema)
    """
    # OneToOne relation
    described_entity = TombstoneDescribedEntitySerializer()
    # OneToOne relation
    management_object = ManagerSerializer()

    class Meta:
        model = models.MetadataRecord
        fields = (
            'pk', 'described_entity', 'management_object'
        )
