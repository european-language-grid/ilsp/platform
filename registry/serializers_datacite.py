from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from django.conf import settings
from django.utils.translation import gettext_lazy as _

from management.models import PUBLISHED
from registry import models, choices


DATACITE_MEDIA_TYPE_CHOICES = {
    'http://w3id.org/meta-share/meta-share/audio': 'Sound',
    'http://w3id.org/meta-share/meta-share/image': 'Image',
    'http://w3id.org/meta-share/meta-share/text': 'Text',
    'http://w3id.org/meta-share/meta-share/textNumerical': 'Numerical text',
    'http://w3id.org/meta-share/meta-share/video': 'Audiovisual'
}

# Mapped accepted ELG identifier choices to Datacite values
DATACITE_IDENTIFIER_CHOICES = {
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ARK: 'ARK',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ARXIV: 'arXiv',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.BIBCODE: 'bibcode',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.DOI: 'DOI',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.HANDLE: 'Handle',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ISBN: 'ISBN',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.PURL: 'PURL',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.UPC: 'UPC',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.URL: 'URL',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.URN: 'URN',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.EAN13: 'EAN13',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.EISSN: 'EISSN',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ISSN: 'ISSN',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ISTC: 'ISTC',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.LISSN: 'LISSN',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.LSID: 'LSID',
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.PMID: 'PMID',
}

# Order of identifiers. DOI - Handle - URL and then all the others
ORDERED_IDENTIFIER_CHOICES = {
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ARK: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ARXIV: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.BIBCODE: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.DOI: 0,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.HANDLE: 1,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ISBN: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.PURL: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.UPC: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.URL: 2,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.URN: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.EAN13: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.EISSN: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ISSN: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.ISTC: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.LISSN: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.LSID: 3,
    choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES.PMID: 3,
}

EXCLUDED_ACTOR_IDENTIFIERS = [
    choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES.ELG,
    choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES.INTERNAL,
    choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES.OTHER,
    choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES.UNSPECIFIED
]


class PersonSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    nameType = serializers.ReadOnlyField(default='Personal')
    givenName = serializers.SerializerMethodField()
    familyName = serializers.SerializerMethodField()
    nameIdentifiers = serializers.SerializerMethodField()

    class Meta:
        model = models.Person
        fields = (
            'name', 'nameType', 'givenName', 'familyName', 'nameIdentifiers'
        )

    def get_name(self, instance):
        return f"{instance.surname.get('en')}, {instance.given_name.get('en')}"

    def get_givenName(self, instance):
        return instance.given_name.get('en')

    def get_familyName(self, instance):
        return instance.surname.get('en')

    def get_nameIdentifiers(self, instance):
        personal_identfiers = instance.personal_identifier.exclude(
            personal_identifier_scheme__in=EXCLUDED_ACTOR_IDENTIFIERS
        )
        name_identifiers = []
        for p in personal_identfiers:
            name_identifiers.append(
                {
                    'nameIdentifier': p.value,
                    'nameIdentifierScheme': choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES[p.personal_identifier_scheme],
                    'schemeURI': p.personal_identifier_scheme
                }
            )
        return name_identifiers


class OrganizationSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    nameType = serializers.ReadOnlyField(default='Organizational')
    nameIdentifiers = serializers.SerializerMethodField()

    class Meta:
        model = models.Organization
        fields = (
            'name', 'nameType', 'nameIdentifiers'
        )

    def get_name(self, instance):
        return instance.organization_name.get('en')

    def get_nameIdentifiers(self, instance):
        organization_identfiers = instance.organization_identifier.exclude(
            organization_identifier_scheme__in=EXCLUDED_ACTOR_IDENTIFIERS
        )
        name_identifiers = []
        for o in organization_identfiers:
            name_identifiers.append(
                {
                    'nameIdentifier': o.value,
                    'nameIdentifierScheme': choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES[o.organization_identifier_scheme],
                    'schemeURI': o.organization_identifier_scheme
                }
            )
        return name_identifiers


class GroupSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    nameType = serializers.ReadOnlyField(default='Organizational')
    nameIdentifiers = serializers.SerializerMethodField()

    class Meta:
        model = models.Group
        fields = (
            'name', 'nameType', 'nameIdentifiers'
        )

    def get_name(self, instance):
        return instance.organization_name.get('en')

    def get_nameIdentifiers(self, instance):
        group_identfiers = instance.group_identifier.exclude(
            organization_identifier_scheme__in=EXCLUDED_ACTOR_IDENTIFIERS
        )
        name_identifiers = []
        for g in group_identfiers:
            name_identifiers.append(
                {
                    'nameIdentifier': g.value,
                    'nameIdentifierScheme': choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES[g.organization_identifier_scheme],
                    'schemeURI': g.organization_identifier_scheme
                }
            )
        return name_identifiers


class ActorSerializer(PolymorphicSerializer, WritableNestedModelSerializer):
    """
    A person, organization or group that participates in an event or
    process
    """

    class Meta:
        model = models.Actor
        fields = '__all__'

    model_serializer_mapping = {
        models.Person: PersonSerializer,
        models.Organization: OrganizationSerializer,
        models.Group: GroupSerializer
    }

    resource_type_field_name = 'actor_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _(
                    'Actor type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]

    def to_representation(self, obj):
        representation = super().to_representation(obj)
        representation.pop('actor_type')
        return representation


class ResourceTypeSerializer(serializers.ModelSerializer):
    resourceTypeGeneral = serializers.SerializerMethodField()
    resourceType = serializers.SerializerMethodField(allow_null=False)

    class Meta:
        model = models.LRSubclass
        fields = ['resourceTypeGeneral', 'resourceType']

    def get_resourceTypeGeneral(self, instance):
        lr_type_resource_type_mapper = {
            'Corpus': 'Dataset',
            'ToolService': 'Software',
            'LexicalConceptualResource': 'Dataset',
            'LanguageDescription': 'Dataset'
        }
        lr_type = instance.lr_type
        return lr_type_resource_type_mapper[lr_type]

    def get_resourceType(self, instance):
        lr_type = instance.lr_type

        if lr_type == 'ToolService':
            return 'Software'

        resource_types = [lr_type]
        if lr_type == 'Corpus':
            media_parts = [
                DATACITE_MEDIA_TYPE_CHOICES[m.media_type]
                for m in instance.corpus_media_part.all()
                if getattr(m, 'media_type')
            ]
            resource_types.extend(set(media_parts))
        elif lr_type == 'LanguageDescription':
            if instance.ld_subclass in [choices.LD_SUBCLASS_CHOICES.GRAMMAR, choices.LD_SUBCLASS_CHOICES.MODEL]:
                resource_types.append(
                    choices.LD_SUBCLASS_CHOICES[instance.ld_subclass].capitalize()
                )
        return ', '.join(resource_types)


class LanguageResourceSerializer(WritableNestedModelSerializer):
    identifiers = serializers.SerializerMethodField()
    publisher = serializers.SerializerMethodField()
    creators = serializers.SerializerMethodField()
    titles = serializers.SerializerMethodField()
    descriptions = serializers.SerializerMethodField()
    subjects = serializers.SerializerMethodField()
    dates = serializers.SerializerMethodField()
    rightsList = serializers.SerializerMethodField()
    relatedIdentifiers = serializers.SerializerMethodField()
    contributors = serializers.SerializerMethodField()
    fundingReferences = serializers.SerializerMethodField()

    class Meta:
        model = models.LanguageResource
        fields = (
            'identifiers',
            'publisher',
            'creators',
            'titles',
            'version',
            'descriptions',
            'subjects',
            'dates',
            'rightsList',
            'relatedIdentifiers',
            'contributors',
            'fundingReferences'
        )

    def get_identifiers(self, instance):
        identifiers = [
            {
                'identifierType': 'DOI',
                'identifier': settings.DATACITE_PREFIX
            },
            {
                'identifierType': 'URL',
                'identifier': instance.metadata_record_of_described_entity.get_display_url()
            }
        ]
        islrn = instance.lr_identifier.filter(lr_identifier_scheme=choices.LR_IDENTIFIER_SCHEME_CHOICES.ISLRN)
        if islrn.exists():
            identifiers.append(
                {
                    'identifierType': 'ISLRN',
                    'identifier': islrn.first().value,
                }
            )
        return identifiers

    def get_publisher(self, instance):
        providers = instance.resource_provider.filter(entity_type='Organization').values_list('organization__organization_name__en', flat=True)
        if providers.exists():
            publisher = " and ".join([", ".join(providers[:-1]), providers[-1]] if len(providers) > 2 else providers)
        else:
            publisher = 'ELG'
        return publisher

    def get_creators(self, instance):
        creators = instance.resource_creator.all()
        if creators.exists():
            datacite_creators = ActorSerializer(
                creators,
                many=True,
            ).data
        else:
            datacite_creators = [{'name': 'unav'}]  # TODO: or unkn. See datacite citation api
        return datacite_creators

    def get_titles(self, instance):
        resource_names = instance.resource_name
        resource_short_names = instance.resource_short_name if instance.resource_short_name else {}
        titles = [
            {
                'title': name, 'lang': lang
            }
            for lang, name in resource_names.items()
        ]
        titles += [
            {
                'title': name, 'lang': lang, 'titleType': 'AlternativeTitle'
            }
            for lang, name in resource_short_names.items()
        ]
        return titles

    def get_descriptions(self, instance):
        descriptions = instance.description
        datacite_descriptions = [
            {
                'description': descr, 'lang': lang, 'descriptionType': 'Abstract'
            }
            for lang, descr in descriptions.items()
        ]
        return datacite_descriptions

    def get_subjects(self, instance):
        subjects = instance.subject.values_list('category_label', flat=True).distinct()
        datacite_subjects = [
            {'subject': cat, 'lang': lang}
            for subj in subjects
            for lang, cat in subj.items()
        ]

        keywords = instance.keyword
        datacite_subjects += [
            {'subject': k, 'lang': lang}
            for multilingual_keyw in keywords
            for lang, k in multilingual_keyw.items()

        ]

        if instance.lr_subclass.__class__ == models.ToolService:
            functions = instance.lr_subclass.function
            datacite_subjects += [
                {
                    'subject': choices.LT_CLASS_RECOMMENDED_CHOICES[f] if f in choices.LT_CLASS_RECOMMENDED_CHOICES else f,
                    'lang': 'en'
                }
                for f in functions
            ]
        return list({v['subject']: v for v in datacite_subjects}.values())

    def get_dates(self, instance):
        dates = []
        creation_end_date = instance.creation_end_date
        if creation_end_date:
            date = {
                'date': creation_end_date.strftime('%Y/%m/%d'),
                'dateType': 'Created'
            }
            dates.append(date)

        version_date = instance.version_date
        if version_date:
            date = {
                'date': version_date.strftime('%Y/%m/%d'),
                'dateType': 'Updated',
                'dateInformation': instance.version
            }
            dates.append(date)
        return dates

    def get_rightsList(self, instance):
        if instance.lr_subclass.__class__ == models.ToolService:
            licences = instance.lr_subclass.software_distribution.values_list(
                'licence_terms__licence_terms_name__en', 'licence_terms__licence_terms_url'
            ).distinct()
        else:
            licences = instance.lr_subclass.dataset_distribution.values_list(
                'licence_terms__licence_terms_name__en', 'licence_terms__licence_terms_url'
            ).distinct()
        rights = [
            {
                'rights': lname,
                'rightsUri': lurl[0],
                'lang': 'en'
            }
            for lname, lurl in licences
            if lname and lurl
        ]
        return rights

    def get_contributors(self, instance):
        elg_contributor = [
            {
                "name": "ELG",
                "nameType": "Organizational",
                "contributorType": "HostingInstitution"
            },
            {
                "name": "ELG",
                "nameType": "Organizational",
                "contributorType": "Distributor"
            }
        ]
        contacts = instance.contact.all()
        datacite_contacts = ActorSerializer(contacts, many=True,).data
        # add contributorType
        datacite_contacts = [dict(item, **{'contributorType': 'ContactPerson'}) for item in datacite_contacts]

        ipr_holders = instance.ipr_holder.all()
        datacite_ipr_holders = ActorSerializer(ipr_holders, many=True, ).data
        # add contributorType
        datacite_ipr_holders = [dict(item, **{'contributorType': 'RightsHolder'}) for item in datacite_ipr_holders]

        contributors = elg_contributor + datacite_contacts + datacite_ipr_holders
        return contributors

    def get_fundingReferences(self, instance):
        funding_projects = instance.funding_project.all()
        fundingReferences = []
        funder_identifier_dict = {}

        for fp in funding_projects:
            fname = 'unav'
            funders = fp.funder.all()
            if funders.count() > 1:
                funders_names = []
                for funder in funders:
                    if funder.__class__ in [models.Organization, models.Group]:
                        funders_names.append(funder.organization_name.get('en'))
                    else:
                        funders_names.append(f'{funder.surname.get("en")} {funder.given_name.get("en")}')
                fname = ', '.join(funders_names)
            else:
                funder = funders.first()
                if funder.__class__ in [models.Organization, models.Group]:
                    fname = funder.organization_name.get('en')
                    fidentifier = (
                        funder.organization_identifier
                        .filter(organization_identifier_scheme__in=[
                            choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES.DOI,
                            choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES.GRID,
                            choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES.OTHER
                        ])
                        .order_by('organization_identifier_scheme')
                        .first()
                    )
                    if fidentifier:
                        fidentifier_value = fidentifier.value
                        is_doi = fidentifier.organization_identifier_scheme == choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES.DOI
                        fidentifier_scheme = (
                            'Crossref Funder ID' if is_doi
                            else choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES[fidentifier.organization_identifier_scheme]
                        )
                        fidentifier_schemeURI = (
                            'https://www.crossref.org/services/funder-registry/' if is_doi
                            else fidentifier.organization_identifier_scheme
                        )
                        funder_identifier_dict = {
                            'funderIdentifier': fidentifier_value,
                            'funderIdentifierType': fidentifier_scheme,
                            'SchemeURI': fidentifier_schemeURI
                        }
                elif funder.__class__ == models.Person:
                    fname = f'{funder.surname.get("en")} {funder.given_name.get("en")}'
                    fidentifier = (
                        funder.personal_identifier
                        .filter(personal_identifier_scheme=choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES.OTHER)
                        .first()
                    )
                    if fidentifier:
                        fidentifier_value = fidentifier.value
                        fidentifier_scheme = choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES[
                            fidentifier.personal_identifier_scheme]
                        fidentifier_schemeURI = fidentifier.personal_identifier_scheme

                        funder_identifier_dict = {
                            'funderIdentifier': fidentifier_value,
                            'funderIdentifierType': fidentifier_scheme,
                            'SchemeURI': fidentifier_schemeURI
                        }

            funding_reference = {
                'funderName': fname,
                'awardTitle': fp.project_name.get('en')
            }

            if fp.grant_number:
                funding_reference['awardNumber'] = fp.grant_number

            funding_reference.update(funder_identifier_dict)

            fundingReferences.append(funding_reference)

        return fundingReferences

    def get_relatedIdentifiers(self, instance):
        related_lrs_map = {}
        related_docs_map = {}
        relations = []

        # IsCitedBy
        is_cited_by = (
            instance.is_cited_by
            .filter(
                document_identifier__document_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
            )
            .distinct()
        )
        related_docs_map['IsCitedBy'] = is_cited_by

        # IsDescribedBy
        is_described_by = (
            instance.is_described_by
            .filter(
                document_identifier__document_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
            )
            .distinct()
        )
        related_docs_map['IsDescribedBy'] = is_described_by

        is_documented_by = (
            instance.is_documented_by
            .filter(
                document_identifier__document_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
            )
            .distinct()
        )
        related_docs_map['IsDocumentedBy'] = is_documented_by

        is_reviewed_by = (
            instance.is_reviewed_by
            .filter(
                document_identifier__document_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
            )
            .distinct()
        )
        related_docs_map['IsReviewedBy'] = is_reviewed_by

        # Add all doc relations
        for rel_type, related_docs in related_docs_map.items():
            for doc in related_docs:
                accepted_ids = (
                    doc.document_identifier
                    .filter(document_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys())
                    .values_list('document_identifier_scheme', 'value')
                )
                doc_identifier_scheme, value = min(accepted_ids, key=lambda x: ORDERED_IDENTIFIER_CHOICES[x[0]])
                relation = {
                    'relatedIdentifier': value,
                    'relatedIdentifierType': DATACITE_IDENTIFIER_CHOICES[doc_identifier_scheme],
                    'relationType': rel_type
                }
                relations.append(relation)

        # isContinuedBy
        is_continued_by = (
            models.LanguageResource.objects
            .filter(
                is_continuation_of__pk=instance.pk,
                lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys(),
                metadata_record_of_described_entity__management_object__deleted=False,
                metadata_record_of_described_entity__management_object__status=PUBLISHED
            )
            .distinct()
        )
        related_lrs_map['IsContinuedBy'] = is_continued_by

        # Continues
        is_continuation_of = (
            instance.is_continuation_of
            .filter(
                lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
            )
            .distinct()
        )
        related_lrs_map['Continues'] = is_continuation_of

        # HasVersion
        has_version = (
            models.LanguageResource.objects
            .filter(
                is_version_of=instance,
                lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys(),
                metadata_record_of_described_entity__management_object__deleted=False,
                metadata_record_of_described_entity__management_object__status=PUBLISHED
            )
            .distinct()
        )
        related_lrs_map['HasVersion'] = has_version

        # IsVersionOf
        if (
                instance.is_version_of
                and instance.is_version_of.lr_identifier.filter(lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()).exists()
        ):
            is_version_of = models.LanguageResource.objects.filter(pk=instance.is_version_of.pk)
            related_lrs_map['IsVersionOf'] = is_version_of

        # replaces
        replaces = (
            instance.replaces
            .filter(
                lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
            )
            .distinct()
        )
        related_lrs_map['IsNewVersionOf'] = replaces

        # isReplacedBy
        is_replaced_by = (
            models.LanguageResource.objects
            .filter(
                replaces=instance,
                lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys(),
                metadata_record_of_described_entity__management_object__deleted=False,
                metadata_record_of_described_entity__management_object__status=PUBLISHED
            )
            .distinct()
        )
        related_lrs_map['IsPreviousVersionOf'] = is_replaced_by

        # IsPartOf
        is_part_of = (
            instance.is_part_of
            .filter(
                lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
            )
            .distinct()
        )
        related_lrs_map['IsPartOf'] = is_part_of

        # HasPart
        has_part = (
            models.LanguageResource.objects
            .filter(
                is_part_of=instance,
                lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys(),
                metadata_record_of_described_entity__management_object__deleted=False,
                metadata_record_of_described_entity__management_object__status=PUBLISHED
            )
            .distinct()
        )
        related_lrs_map['HasPart'] = has_part

        # isExactMatchWith
        is_exact_match_with = (
            instance.is_exact_match_with
            .filter(
                lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
            )
            .distinct()
        )
        related_lrs_map['IsIdenticalTo'] = is_exact_match_with

        # hasOutcome
        has_outcome = (
            instance.actual_use
            .filter(
                has_outcome__lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
            )
            .values_list('has_outcome', flat=True)
            .distinct()
        )
        has_outcome_lrs = models.LanguageResource.objects.filter(pk__in=has_outcome)

        related_lrs_map['IsSourceOf'] = has_outcome_lrs

        # isConvertedVersionOf
        # hasOriginalSource
        # requiresLR
        if instance.lr_subclass.__class__ == models.ToolService:
            requires_lr = (
                instance.lr_subclass.requires_lr
                .filter(
                    lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
                )
                .distinct()
            )
            related_lrs_map['Requires'] = requires_lr
        else:
            is_converted_version_of = (
                instance.lr_subclass.is_converted_version_of
                .filter(
                    lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
                )
                .distinct()
            )
            related_lrs_map['IsVariantFormOf'] = is_converted_version_of

        if instance.lr_subclass.__class__ == models.LanguageDescription:
            requires_lr = (
                instance.lr_subclass.language_description_subclass.requires_lr
                .filter(
                    lr_identifier__lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys()
                )
                .distinct()
            )
            related_lrs_map['Requires'] = requires_lr

        # Add all lr relations
        for rel_type, related_lrs in related_lrs_map.items():
            for lr in related_lrs:
                accepted_ids = (
                    lr.lr_identifier
                    .filter(lr_identifier_scheme__in=DATACITE_IDENTIFIER_CHOICES.keys())
                    .values_list('lr_identifier_scheme', 'value')
                )
                lr_identifier_scheme, value = min(accepted_ids, key=lambda x: ORDERED_IDENTIFIER_CHOICES[x[0]])
                relation = {
                    'relatedIdentifier': value,
                    'relatedIdentifierType': DATACITE_IDENTIFIER_CHOICES[lr_identifier_scheme],
                    'relationType': rel_type
                }
                relations.append(relation)

        return relations


class DescribedEntitySerializer(PolymorphicSerializer, WritableNestedModelSerializer):
    """
    The (type of) entity that is being described by a metadata
    record
    """

    class Meta:
        model = models.DescribedEntity
        fields = '__all__'

    model_serializer_mapping = {
        models.LanguageResource: LanguageResourceSerializer,
    }

    resource_type_field_name = 'entity_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _(
                    'entity type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]

    def to_representation(self, obj):
        representation = super().to_representation(obj)
        representation.pop('entity_type')
        return representation


class DataCiteMetadataRecordSerializer(WritableNestedModelSerializer):
    described_entity = DescribedEntitySerializer()
    types = serializers.SerializerMethodField()
    publicationYear = serializers.SerializerMethodField()
    schemaVersion = serializers.ReadOnlyField(default='http://datacite.org/schema/kernel-4')

    class Meta:
        model = models.MetadataRecord
        fields = [
            'described_entity',
            'types',
            'publicationYear',
            'schemaVersion'
        ]

    def to_representation(self, obj):
        """Move fields from profile to user representation."""
        representation = super().to_representation(obj)
        described_entity_representation = representation.pop('described_entity')
        for key in described_entity_representation:
            representation[key] = described_entity_representation[key]

        return representation

    def get_types(self, instance):
        return ResourceTypeSerializer(
            instance.described_entity.lr_subclass,
        ).data

    def get_publicationYear(self, instance):
        publication_date = instance.described_entity.publication_date
        if publication_date:
            publication_year = publication_date.year
        else:
            publication_year = instance.management_object.publication_date.year
        return str(publication_year)
