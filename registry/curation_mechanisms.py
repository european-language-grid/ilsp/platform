import inspect
import logging
import sys

from django.apps import apps
from rest_framework.fields import (
    ListField, CharField, ChoiceField, BooleanField, DateField
)
from rest_framework.serializers import ModelSerializer, ListSerializer

from accounts.models import ELGUser
from catalogue_backend.celery import app as celery_app
from email_notifications.email_notification_utils import admin_curation_notification
from registry.fields import SerializersMultilingualField
from registry.models import (
    Project, Person, Organization, Group, LanguageResource, LicenceTerms, Document, Repository,
    MetadataRecord, DescribedEntity
)
from registry.serializer_tasks import GetRelatedRecords
from registry.serializers import (
    GenericProjectSerializer, GenericPersonSerializer, GenericOrganizationSerializer,
    GenericGroupSerializer, GenericLanguageResourceSerializer,
    GenericLicenceTermsSerializer,
    GenericDocumentSerializer,
    GenericRepositorySerializer, GenericMetadataRecordSerializer
)
from registry.utils.task_utils import update_affected_md_records

LOGGER = logging.getLogger(__name__)

# Dictionary with Model label: Model class
app_models = list(apps.get_app_config('registry').get_models())
model_dict = {model._meta.label: model for model in app_models}

# Dictionary with Model label: Serializer class
serializer_dict = {cls_obj.Meta.model._meta.label: cls_obj
                   for cls_name, cls_obj in inspect.getmembers(sys.modules['registry.serializers'])
                   if (inspect.isclass(cls_obj) and 'serializer' in cls_name.lower() and
                       hasattr(cls_obj, 'Meta'))
                   }

mapping_entities_to_generic_entities = {
    Person: GenericPersonSerializer,
    Organization: GenericOrganizationSerializer,
    Group: GenericGroupSerializer,
    LanguageResource: GenericLanguageResourceSerializer,
    LicenceTerms: GenericLicenceTermsSerializer,
    Document: GenericDocumentSerializer,
    Project: GenericProjectSerializer,
    Repository: GenericRepositorySerializer,
    MetadataRecord: GenericMetadataRecordSerializer,
}


def create_internal_value_for_field_add_value(serializer_to_curate, field_name, value):
    field = serializer_to_curate.fields[field_name]
    field_value_new = serializer_to_curate.data[field_name]
    if field_name == 'pk':
        field_value_new = value
    elif isinstance(field, ListSerializer) and isinstance(field.child, ModelSerializer):
        # Add related_model in m2m or o2m relation
        value_list = value.split('|')
        # Find related model name
        related_model = field.child.Meta().model
        related_model_label = related_model._meta.label
        related_instances = related_model.objects.filter(pk__in=value_list)
        if len(related_instances) != len(value_list):
            raise Exception(
                f'Not all provided ids {value_list} where retrieved. Verify that ids exist for {related_model._meta.verbose_name}')
        related_serializer = serializer_dict[related_model_label](
            related_model.objects.filter(pk__in=value_list), many=True)
        if field_value_new:
            field_value_new = field_value_new + related_serializer.data
        else:
            field_value_new = related_serializer.data
    elif isinstance(field, ModelSerializer):
        # Add/Replace if exists related_model in o2o relation
        # Find related model name
        related_model = field.Meta().model
        related_model_label = related_model._meta.label
        try:
            related_instance = related_model.objects.get(pk=value)
        except related_model.DoesNotExist:
            raise Exception(f"{related_model._meta.verbose_name} with pk {value} does not exist")
        related_serializer = serializer_dict[related_model_label](instance=related_instance)
        field_value_new = related_serializer.data
    elif isinstance(field, ListField):
        # If field is optional, create it
        if field_value_new is None:
            field_value_new = list()
        value_list = value.split('|')
        # Handling multiple multilingual field
        if isinstance(field.child, SerializersMultilingualField):
            # Add multiple, multilingual in en
            for value_to_add in value_list:
                field_value_new.append({'en': value_to_add.strip()})
        # Handling multiple char(url and email) and choice field
        elif isinstance(field.child, CharField) or isinstance(field.child, ChoiceField):
            LOGGER.info(f'{value_list}')
            LOGGER.info(f'{field_value_new}')
            # Add multiple Char (also URL, Email) fields with or without CV
            for value_to_add in value_list:
                field_value_new.append(value_to_add.strip())
    # Handling multilingual field
    elif isinstance(field, SerializersMultilingualField):
        # Replace multilingual field for en language, keep other languages if exists as is
        # For optional field, value may not exist
        if field_value_new is None:
            field_value_new = dict()
        field_value_new['en'] = value.strip()
    # Handling char/choice/boolean/date field
    elif isinstance(field, CharField) or isinstance(field, ChoiceField) or isinstance(field, BooleanField) or \
            isinstance(field, DateField):
        # Replace Char (also URL, Email) fields with or without CV
        field_value_new = value.strip()
    else:
        raise Exception(f'Field with name {field_name} is of type {type(field)} that we do not handle yet')
    return field_value_new


def create_internal_value_for_field_remove_value(serializer_to_curate, field_name, value):
    LOGGER.info('Remove!')
    field = serializer_to_curate.fields[field_name]
    LOGGER.info(type(field))
    field_value_new = serializer_to_curate.data[field_name]
    LOGGER.info(field_value_new)
    if field_name == 'pk':
        field_value_new = value
    elif isinstance(field, ListSerializer) and isinstance(field.child, ModelSerializer):
        # Remove related_model in m2m or o2m relation
        error_msgs = list()
        value_list = value.split('|')
        # Find related model name
        related_model = field.child.Meta().model
        related_model_name = related_model._meta.model_name
        # Remove values that match pk of fields
        for value in field_value_new:
            LOGGER.info(value['pk'])
        for value_to_remove in value_list:
            found_value = False
            for existing_value in field_value_new:
                try:
                    if existing_value['pk'] == int(value_to_remove.strip()):
                        field_value_new.remove(existing_value)
                        found_value = True
                        break
                except ValueError:
                    found_value = True
                    error_msgs.append(f'Request to delete {value_to_remove} from field with name {field_name}, '
                                      f'but that value is not an integer.')
                    break
            if not found_value:
                error_msgs.append(f'Request to delete {value_to_remove} from field with name {field_name}, '
                                  f'but that value does not exist. Field has value {field_value_new}')
        if len(error_msgs) != 0:
            raise Exception(error_msgs)
        LOGGER.info('After processing')
        for value in field_value_new:
            LOGGER.info(value['pk'])
    elif isinstance(field, ModelSerializer):
        # Remove related_model in o2o relation
        # Find related model name
        related_model = field.Meta().model
        if field_value_new is None:
            raise Exception(
                f'Request to delete {value} from field with name {field_name}, '
                f'but that value does not exist. Field has value {field_value_new}')
        try:
            if field_value_new['pk'] == int(value.strip()):
                field_value_new = None
            else:
                raise Exception(f"{related_model._meta.verbose_name} with pk {value} does not exist")
        except ValueError:
            raise Exception(f'Request to delete {value} from field with name {field_name}, '
                            f'but that value is not an integer.')
    elif isinstance(field, ListField):
        error_msgs = list()
        value_list = value.split('|')
        # Handling multiple multilingual field
        if isinstance(field.child, SerializersMultilingualField):
            # Remove values that match en key from multiple, multilingual
            for value_to_remove in value_list:
                value_to_remove = value_to_remove.strip()
                found_value = False
                for existing_value in field_value_new:
                    if value_to_remove == existing_value['en']:
                        field_value_new.remove(existing_value)
                        found_value = True
                        break
                if not found_value:
                    error_msgs.append(f'Request to delete {value_to_remove} from field with name {field_name}, '
                                      f'but that value with en language does not exist. Field has value {field_value_new}')
            if len(field_value_new) == 0:
                field_value_new = None
            if len(error_msgs) != 0:
                raise Exception(error_msgs)
        # Handling multiple char(url and email) and choice field
        if isinstance(field.child, CharField) or isinstance(field.child, ChoiceField):
            # Remove values from multiple Char (also URL, Email) fields with or without CV
            for value_to_remove in value_list:
                value_to_remove = value_to_remove.strip()
                if value_to_remove in field_value_new:
                    field_value_new.remove(value_to_remove)
                else:
                    error_msgs.append(f'Request to delete {value_to_remove} from field with name {field_name}, '
                                      f'but that value does not exist. Field has value {field_value_new}')
            if len(field_value_new) == 0:
                field_value_new = None
            if len(error_msgs) != 0:
                raise Exception(error_msgs)
    # # Handling multilingual field
    elif isinstance(field, SerializersMultilingualField):
        # Remove multilingual field given value for en language
        # For optional field, value may not exist
        if field_value_new is None:
            raise Exception(
                f'Request to delete {value} from field with name {field_name}, '
                f'but that value with en language does not exist. Field has value {field_value_new}')
        if field_value_new is not None and field_value_new['en'] != value.strip():
            raise Exception(
                f'Request to delete {value} from field with name {field_name}, '
                f'but that value with en language does not exist. Field has value {field_value_new}')
        field_value_new = None
    # Handling Char (Email/URL) fields with or without CV
    elif isinstance(field, CharField):
        if str(field_value_new) != value.strip():
            raise Exception(
                f'Request to delete {value} from field with name {field_name}, '
                f'but that value does not exist. Field has value {field_value_new}')
        field_value_new = ''
    # Handling choice/boolean/date field
    elif isinstance(field, CharField) or isinstance(field, ChoiceField) or isinstance(field, BooleanField) or \
            isinstance(field, DateField):
        if str(field_value_new) != value.strip():
            raise Exception(
                f'Request to delete {value} from field with name {field_name}, '
                f'but that value does not exist. Field has value {field_value_new}')
        field_value_new = None
    else:
        raise Exception(f'Field with name {field_name} is of type {type(field)} that we do not handle yet')
    return field_value_new


def curate_model(curation_info_of_instances, type_of_instance, remove=False):
    report = dict()
    records_affected = set()
    if type_of_instance not in serializer_dict.keys():
        report[1] = f'{type_of_instance} is not a valid type of instance'
        return report

    # Retrieve specific serializer and model
    serializer = serializer_dict[type_of_instance]
    try:
        model = serializer.Meta().model
    except AttributeError:
        report[1] = f'{serializer} has no related db model'
        return report
    LOGGER.info(f'Curate {model._meta.verbose_name} instances. Removal:{remove}')

    fields_that_affect_references = tuple()
    if model in mapping_entities_to_generic_entities:
        fields_that_affect_references = mapping_entities_to_generic_entities[model].Meta.fields

    for count, curation_row in enumerate(curation_info_of_instances):
        # +2 due to one line header and zero count start in python
        line = count + 2
        LOGGER.info(f'Working with line {line} - {curation_row}')

        # Find model to curate
        # If no id is given, we cannot know which model to curate
        if 'pk' not in curation_row.keys():
            report[line] = f"No pk header was found in line {line}"
            continue
        # Find model to curate
        try:
            model_to_curate = model.objects.get(pk=curation_row['pk'])
        except model.DoesNotExist:
            report[line] = f"{model._meta.verbose_name} with pk {curation_row['pk']} does not exist"
            continue
        except ValueError as err:
            report[line] = str(err)
            continue

        LOGGER.info(f'Curate {model._meta.verbose_name} - {model_to_curate}')
        serializer_to_curate = serializer(instance=model_to_curate)

        # Find describe entity affected by curation
        # If no entity id is given, we cannot know which entity is being affected by the curation process
        if 'entity_pk' not in curation_row.keys():
            report[line] = f"No entity pk header was found in line {line}"
            continue
        # Find entity affected by curation
        try:
            entity_affected_by_curation = DescribedEntity.objects.get(pk=curation_row['entity_pk'])
        except DescribedEntity.DoesNotExist:
            report[line] = f"{DescribedEntity._meta.verbose_name} with pk {curation_row['entity_pk']} does not exist"
            continue
        except ValueError as err:
            report[line] = str(err)
            continue

        LOGGER.info(f'Affected {DescribedEntity._meta.verbose_name} - {entity_affected_by_curation}')
        metadata_record_affected = None
        # If entity_affected_by_curation has metadata record, add metadata record to affected ones
        if hasattr(entity_affected_by_curation, 'metadata_record_of_described_entity'):
            metadata_record_affected = entity_affected_by_curation.metadata_record_of_described_entity
            records_affected.add(metadata_record_affected.pk)

        check_for_references = False
        # For each column-field
        data = dict()
        row_result = None
        for field_name in curation_row.keys():

            # Ignore entity pk column for enrichment
            if field_name == 'entity_pk':
                continue
            if field_name not in serializer_to_curate.fields:
                if row_result is None:
                    row_result = dict()
                row_result[field_name] = f'Field with name {field_name} does not exist in {model._meta.verbose_name}'
                continue
            if curation_row[field_name]:
                try:
                    LOGGER.info(
                        f'Working with field {field_name} with value {curation_row[field_name]}. Removal:{remove}')
                    if not remove:
                        data[field_name] = create_internal_value_for_field_add_value(serializer_to_curate, field_name,
                                                                                     curation_row[field_name])
                    else:
                        data[field_name] = create_internal_value_for_field_remove_value(serializer_to_curate,
                                                                                        field_name,
                                                                                        curation_row[field_name])
                    if field_name in fields_that_affect_references:
                        check_for_references = True
                except Exception as err:
                    if row_result is None:
                        row_result = dict()
                    row_result[field_name] = str(err)
        LOGGER.info(f'Working with line {line} generated data {data}')
        serializer_to_curate = serializer(instance=model_to_curate, data=data, partial=True)
        if serializer_to_curate.is_valid() and row_result is None:
            serializer_to_curate.save()
            report[line] = f"Curation of {model._meta.verbose_name} with pk {curation_row['pk']} was successful"
        else:
            if row_result is None:
                row_result = serializer_to_curate.errors
            else:
                row_result.update(serializer_to_curate.errors)
            report[line] = row_result

        # Find affected records
        if check_for_references:
            get_related_instances = GetRelatedRecords(model_to_curate)
            relations = get_related_instances.get_all_fields()
            related_mds = [getattr(_rel, 'metadata_record_of_described_entity') for _rel in relations]
            records_affected.update(set(_md.pk for _md in related_mds))

    # Update affected records
    LOGGER.info(f'Update affected records {records_affected}')
    if len(records_affected) != 0:
        update_affected_md_records(records_affected)
    LOGGER.info(f'Curation report :: {report}')
    return report


@celery_app.task(name='admin_entity_enrichment')
def initiate_curation(rows_to_curate, model_type, username, removal):
    LOGGER.info(f'Initiate enrichment for {model_type} using {rows_to_curate}')
    result = curate_model(rows_to_curate, model_type, remove=removal)
    user = ELGUser.objects.get(username=username)
    admin_curation_notification(model_type, removal, result, user=user)
    LOGGER.info(f'Report of the task :: {result}')
    return result
