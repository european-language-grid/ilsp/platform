from collections import Mapping, OrderedDict

from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError
from rest_framework.fields import SkipField, get_error_detail, set_value, empty
from rest_framework.serializers import Serializer, as_serializer_error
from rest_framework.settings import api_settings
from django.core.exceptions import ValidationError as DjangoValidationError
from rest_polymorphic.serializers import PolymorphicSerializer

from registry.models import DocumentIdentifier, SocialMediaOccupationalAccount


class DraftSerializer(Serializer):
    """
    An representation of OrderedDict used for draft serialization only
    """

    # def __init__(self, instance=None, data=empty, **kwargs):
    #     super().__init__(instance, data, **kwargs)

    def get_draft(self):
        return self.context.get('draft')

    def to_internal_value(self, data):
        """
        Dict of native values <- Dict of primitive datatypes.
        """
        if not isinstance(data, Mapping):
            message = self.error_messages['invalid'].format(
                datatype=type(data).__name__
            )
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            }, code='invalid')

        ret = OrderedDict()
        errors = OrderedDict()
        fields = self._writable_fields
        draft_exception_list = ['project_name', 'resource_name', 'version', 'organization_name',
                                'given_name', 'surname', 'licence_terms_name',
                                'title', 'repository_name']
        for field in fields:

            if self.get_draft():
                if 'Generic' in field.parent.__class__.__name__:
                    if 'scheme' in field.field_name:
                        draft_exception_list.append(field.field_name)
                    if field.field_name == 'value' and field.parent.Meta.model != SocialMediaOccupationalAccount:
                        draft_exception_list.append(field.field_name)
                    if field.field_name in ['licence_terms_url', 'condition_of_use']:
                        draft_exception_list.append(field.field_name)

            validate_method = getattr(self, 'validate_' + field.field_name, None)
            primitive_value = field.get_value(data)
            try:
                if isinstance(field, PolymorphicSerializer) and primitive_value is None and field.allow_null is True:
                    validated_value = None
                else:
                    validated_value = field.run_validation(primitive_value)
                if validate_method is not None:
                    validated_value = validate_method(validated_value)
            except ValidationError as exc:
                if self.get_draft() and field.field_name not in draft_exception_list:
                    ignore_errors = self._ignore_null_blank(exc.detail, field.field_name)
                    if ignore_errors:
                        continue

                errors[field.field_name] = exc.detail
            except DjangoValidationError as exc:
                errors[field.field_name] = get_error_detail(exc)
            except SkipField:
                pass
            else:
                set_value(ret, field.source_attrs, validated_value)

        if errors:
            raise ValidationError(errors)
        return ret

    def run_validation(self, data=empty):
        """
        We override the default `run_validation`, because the validation
        performed by validators and the `.validate()` method should
        be coerced into an error dictionary with a 'non_fields_error' key.
        """
        (is_empty_value, data) = self.validate_empty_values(data)
        if is_empty_value:
            return data

        value = self.to_internal_value(data)
        try:
            self.run_validators(value)

            if not self.get_draft():
                value = self.validate(value)

            if self.get_draft() and any(['Generic' in self.__class__.__name__,
                                         self.__class__.__name__ is 'MetadataRecordSerializer']):
                value = self.validate(value)

            assert value is not None, '.validate() should return the validated data'
        except (ValidationError, DjangoValidationError) as exc:
            raise ValidationError(detail=as_serializer_error(exc))

        return value

    def is_valid(self, raise_exception=False):
        if not self.get_draft():
            return super().is_valid(raise_exception=raise_exception)
        else:
            self._validated_data = self.initial_data
            self._errors = self.validate_draft_fields(self.__class__, self._validated_data)
            if self._errors and raise_exception:
                raise ValidationError(self.errors)

            return not bool(self._errors)

    def validate_draft_fields(self, serializer_class, validated_data):
        _errors = {}
        model_entity_type_mapping = {
            'MetadataRecord': ['described_entity'],
            'LanguageResource': ['lr_subclass']
        }
        if hasattr(serializer_class, 'Meta') \
                and 'Generic' not in serializer_class.__name__ \
                and validated_data:
            model = serializer_class.Meta.model
            if model.__name__ in model_entity_type_mapping:
                for i, field in enumerate(model_entity_type_mapping[model.__name__]):
                    try:
                        if not validated_data.get(field, None):
                            raise ValidationError(
                                {model_entity_type_mapping[model.__name__][i]: [_(
                                    f'This field is required.')]},
                                code='invalid',
                            )
                    except ValidationError as exc:
                        _errors.update(exc.detail)

        try:
            self.run_validation(validated_data)
        except ValidationError as exc:
            _errors.update(exc.detail)

        return _errors

    def _ignore_null_blank(self, error, field_name, skip=list()):
        skip = skip
        if isinstance(error, list):
            if isinstance(error[0], dict):
                self._ignore_null_blank(error[0], field_name, skip)
            else:
                if any(['null' in error[0].lower(),
                        'blank' in error[0].lower(),
                        'required' in error[0].lower() and 'a valid number' not in error[0].lower(),
                        'empty' in error[0].lower(),
                        'can be used only for' in error[0].lower(),
                        'cannot be used' in error[0].lower(),
                        'must be' in error[0].lower()]):
                    skip.append(True)
                else:
                    skip.append(False)

        elif isinstance(error, dict):
            for k, v in error.items():
                if isinstance(v, dict):
                    self._ignore_null_blank(v, field_name, skip)
                elif isinstance(v, list):
                    self._ignore_null_blank(v, field_name, skip)
                else:
                    if any(['null' in v.lower(),
                            'blank' in v.lower(),
                            'required' in v.lower() and 'a valid number' not in v.lower(),
                            'empty' in v.lower(),
                            'can be used only for' in v.lower(),
                            'cannot be used' in v.lower(),
                            'must be' in v.lower()]):
                        skip.append(True)
                    else:
                        skip.append(False)
        skip_error = 0
        for i in skip:
            if i:
                skip_error += 1
        return skip_error
