import copy
import logging
from collections import OrderedDict

import xmltodict
from django.conf import settings
from django.contrib.auth import get_user_model
from test_utils import SerializerTestCase
from rest_framework.exceptions import ParseError

from management.models import Manager
from registry.serializers import (
    MetadataRecordSerializer,
    OrganizationSerializer, ToolServiceSerializer, ModelSerializer
)

LOGGER = logging.getLogger(__name__)


class TestMetadataRecordCreatedFromXML(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(cls.xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        cls.data = xmltodict.parse(xml_input, xml_attribs=True)['ms:MetadataRecord']

    def test_serializer_works_with_data_from_xml(self):
        xml_data = copy.deepcopy(self.data)
        serializer = MetadataRecordSerializer(xml_data=xml_data)
        if serializer.is_valid():
            instance = serializer.save()
            manager = Manager.objects.get(id=instance.management_object.id)
            user = get_user_model().objects.create(username='curatorA1')
            manager.curator = user
            manager.save()
            instance.management_object = manager
            instance.save()
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(False)
        LOGGER.info('Setup has finished')
        self.assertTrue(serializer.data)
        self.assertTrue(serializer.is_valid())

    def test_serializer_works_fails_with_bad_data_from_xml(self):
        xml_data_fail = copy.deepcopy(self.data)
        xml_data_fail['ms:DescribedEntity']['ms:LanguageResource']['ms:actualUse']['ms:hasOutcome'] = OrderedDict(
            [('resourceName', v) if k == 'ms:resourceName' else (k, v) for k, v in
             self.data['ms:DescribedEntity']['ms:LanguageResource']['ms:actualUse']['ms:hasOutcome'].items()])
        serializer = MetadataRecordSerializer(xml_data=xml_data_fail)
        self.assertFalse(serializer.is_valid())


class TestFromXMLRepresentationRecommended(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(cls.xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        cls.data = xmltodict.parse(xml_input, xml_attribs=True)

    def test_from_xml_representation_fails_with_bad_data(self):
        data = copy.deepcopy(self.data)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:contact']['ms:Organization']
        xml_data_fail = copy.deepcopy(xml_data)
        xml_data_fail['ms:LTArea'] = OrderedDict(
            [('ms:LTClass', v) if k == 'ms:LTRecommendedClass' else (k, v) for k, v in xml_data['ms:LTArea'][0].items()])
        OrganizationSerializer().from_xml_representation(xml_data=xml_data_fail)
        self.assertRaises(ParseError)

    def test_from_xml_representation_fails_with_more_than_one(self):
        data = copy.deepcopy(self.data)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:contact']['ms:Organization']
        xml_data_fail = copy.deepcopy(xml_data)
        xml_data_fail['ms:LTArea'][0]['ms:LTClassOther'] = 'http://w3id.org/meta-share/omtd-share/Matching'
        OrganizationSerializer().from_xml_representation(xml_data=xml_data_fail)
        self.assertRaises(ParseError)

    def test_from_xml_representation_fails_with_list(self):
        data = copy.deepcopy(self.data)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:contact']['ms:Organization']
        xml_data_fail = copy.deepcopy(xml_data)
        xml_data_fail['ms:LTArea'][0]['ms:LTRecommendedClass'] = ['http://w3id.org/meta-share/omtd-share/Matching',
                                                               'http://w3id.org/meta-share/omtd-share/Viewing']
        try:
            OrganizationSerializer().from_xml_representation(xml_data=xml_data_fail)
            self.assertTrue(False)
        except ParseError:
            self.assertTrue(True)
