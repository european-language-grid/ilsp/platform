import json
import logging
from unittest.mock import patch

from django.core.exceptions import ObjectDoesNotExist

from analytics.models import UserDownloadStats, MetadataRecordStats, ServiceStats
from management.models import Manager, ContentFile
from processing.models import RegisteredLTService
from registry import models
from registry import serializers
from registry.utils.custom_errors import ELGRecordDeletionError
from test_utils import SerializerTestCase, TEST_CONCEPT_DOI_1, TEST_DOI_1

LOGGER = logging.getLogger(__name__)


class TestDeletionFunctionalityMetadataRecordFails(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)
        json_data['described_entity']['resource_name'] = {'en': 'deletion fails'}
        json_data['described_entity']['resource_short_name'] = {'en': 'deletion fails short'}
        json_data['described_entity']['version'] = 52
        json_data['described_entity']['lr_identifier'] = []
        json_data['source_metadata_record'] = {
            "metadata_record_identifier": {
                "metadata_record_identifier_scheme": "http://purl.org/spar/datacite/handle",
                "value": "http://hdl.handle.net/11304/6eacaa76-c275-11e4-ac7e-860aa0063d1f"
            }
        }
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()
        instance.management_object.functional_service = True
        instance.management_object.save()
        instance.management_object.ingest()
        for software_distribution in instance.described_entity.lr_subclass.software_distribution.all():
            if software_distribution.execution_location:
                exec_loc = software_distribution.execution_location
                docker_download_loc = software_distribution.docker_download_location
                service_adapter_loc = software_distribution.service_adapter_download_location
        lt_service = RegisteredLTService.objects.create(
            metadata_record=instance,
            elg_execution_location=exec_loc,
            docker_download_location=docker_download_loc,
            service_adapter_download_location=service_adapter_loc
        )
        lt_service.elg_execution_location = exec_loc + '/difloc/'
        lt_service.status = 'COMPLETED'
        lt_service.elg_hosted = True
        lt_service.accessor_id = 'adsada'
        lt_service.save()
        with patch(
                'management.models.Manager.create_doi', return_value=TEST_CONCEPT_DOI_1,
                side_effect=[TEST_CONCEPT_DOI_1, TEST_DOI_1]
        ):
            instance.management_object.publish(force=True)
        instance.management_object.unpublish()
        service_stats, _ = ServiceStats.objects.get_or_create(lt_service=instance.lt_service,
                                                              bytes=1000,
                                                              elg_resource=instance)
        mdr_stats, _ = MetadataRecordStats.objects.get_or_create(metadata_record=instance)
        mdr_stats.increment_views()
        download_stats, _ = UserDownloadStats.objects.get_or_create(metadata_record=instance)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'metadata_record_identifier'] = instance.metadata_record_identifier.pk
        cls.nested_models['metadata_curator'] = [curator.pk for curator in
                                                 instance.metadata_curator.all()]
        cls.nested_models['metadata_creator'] = instance.metadata_creator.pk
        cls.nested_models[
            'source_metadata_record'] = instance.source_metadata_record.pk
        cls.nested_models[
            'source_of_metadata_record'] = instance.source_of_metadata_record.pk
        cls.nested_models['described_entity'] = instance.described_entity.pk
        cls.nested_models['management_object'] = instance.management_object.pk
        cls.nested_models['service_stats'] = service_stats.pk
        cls.nested_models['mdr_stats'] = mdr_stats.pk
        cls.nested_models['download_stats'] = download_stats.pk
        cls.nested_models['lt_service'] = lt_service.pk
        try:
            instance.delete()
        except ELGRecordDeletionError as err:
            cls.error = err

    def test_mdr_is_not_deleted(self):
        try:
            obj = models.MetadataRecord.objects.get(id=self.instance_pk)
            self.assertTrue(True)
            self.assertTrue(self.error.args[0] == "ELG Records cannot be deleted")
        except ObjectDoesNotExist:
            self.assertTrue(False)



class TestDeletionFunctionalityMetadataRecordExplicit(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)
        json_data['source_metadata_record'] = {
            "metadata_record_identifier": {
                "metadata_record_identifier_scheme": "http://purl.org/spar/datacite/handle",
                "value": "http://hdl.handle.net/11304/6eacaa76-c275-11e4-ac7e-860aa0063d1f"
            }
        }
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()
        instance.management_object.functional_service = True
        instance.management_object.save()
        instance.management_object.ingest()
        for software_distribution in instance.described_entity.lr_subclass.software_distribution.all():
            if software_distribution.execution_location:
                exec_loc = software_distribution.execution_location
                docker_download_loc = software_distribution.docker_download_location
                service_adapter_loc = software_distribution.service_adapter_download_location
        lt_service = RegisteredLTService.objects.create(
            metadata_record=instance,
            elg_execution_location=exec_loc,
            docker_download_location=docker_download_loc,
            service_adapter_download_location=service_adapter_loc
        )
        lt_service.elg_execution_location = exec_loc + '/difloc/'
        lt_service.status = 'COMPLETED'
        lt_service.elg_hosted = True
        lt_service.accessor_id = 'adsada'
        lt_service.save()
        with patch(
                'management.models.Manager.create_doi', return_value=TEST_CONCEPT_DOI_1,
                side_effect=[TEST_CONCEPT_DOI_1, TEST_DOI_1]
        ):
            instance.management_object.publish(force=True)
        instance.management_object.unpublish()
        service_stats, _ = ServiceStats.objects.get_or_create(lt_service=instance.lt_service,
                                                              bytes=1000,
                                                              elg_resource=instance)
        mdr_stats, _ = MetadataRecordStats.objects.get_or_create(metadata_record=instance)
        mdr_stats.increment_views()
        download_stats, _ = UserDownloadStats.objects.get_or_create(metadata_record=instance)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'metadata_record_identifier'] = instance.metadata_record_identifier.pk
        cls.nested_models['metadata_curator'] = [curator.pk for curator in
                                                 instance.metadata_curator.all()]
        cls.nested_models['metadata_creator'] = instance.metadata_creator.pk
        cls.nested_models[
            'source_metadata_record'] = instance.source_metadata_record.pk
        cls.nested_models[
            'source_of_metadata_record'] = instance.source_of_metadata_record.pk
        cls.nested_models['described_entity'] = instance.described_entity.pk
        cls.nested_models['management_object'] = instance.management_object.pk
        cls.nested_models['service_stats'] = service_stats.pk
        cls.nested_models['mdr_stats'] = mdr_stats.pk
        cls.nested_models['download_stats'] = download_stats.pk
        cls.nested_models['lt_service'] = lt_service.pk
        instance.delete(explicit=True)


    def test_management_object_deletion(self):
        try:
            obj = Manager.objects.get(
                id=self.nested_models['management_object'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_mdr_stats_deletion(self):
        try:
            obj = MetadataRecordStats.objects.get(
                id=self.nested_models['mdr_stats'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_service_stats_deletion(self):
        try:
            obj = ServiceStats.objects.get(
                id=self.nested_models['service_stats'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_download_stats_deletion(self):
        try:
            obj = UserDownloadStats.objects.get(
                id=self.nested_models['download_stats'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_lt_service_deletion(self):
        try:
            obj = RegisteredLTService.objects.get(
                id=self.nested_models['lt_service'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_metadata_record_identifier_is_deleted(self):
        try:
            obj = models.MetadataRecordIdentifier.objects.get(
                id=self.nested_models['metadata_record_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_metadata_curators_are_not_deleted(self):
        try:
            for pk in self.nested_models['metadata_curator']:
                obj = models.Person.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metadata_creator_is_not_deleted(self):
        try:
            obj = models.Person.objects.get(
                id=self.nested_models['metadata_creator'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_source_metadata_record_is_deleted(self):
        try:
            obj = models.GenericMetadataRecord.objects.get(
                id=self.nested_models['source_metadata_record'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_source_of_metadata_record_is_not_deleted(self):
        try:
            obj = models.Repository.objects.get(
                id=self.nested_models['source_of_metadata_record'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_described_entity_is_deleted(self):
        LOGGER.info('Described entity id {}'.format(
            self.nested_models['described_entity']))
        try:
            obj = models.DescribedEntity.objects.get(
                id=self.nested_models['described_entity'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_mdr_is_deleted(self):
        try:
            obj = models.MetadataRecord.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityProject(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)['described_entity']
        serializer = serializers.ProjectSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['project_identifier'] = [pid.pk for pid in
                                                   instance.project_identifier.all()]
        cls.nested_models['funder'] = [funder.pk for funder in
                                       instance.funder.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['subject'] = [subject.pk for subject in
                                        instance.subject.all()]
        cls.nested_models['social_media_occupational_account'] = [sm_account.pk
                                                                  for sm_account
                                                                  in
                                                                  instance.social_media_occupational_account.all()]
        cls.nested_models['cost'] = instance.cost.pk
        cls.nested_models[
            'ec_max_contribution'] = instance.ec_max_contribution.pk
        cls.nested_models[
            'national_max_contribution'] = instance.national_max_contribution.pk
        cls.nested_models['coordinator'] = instance.coordinator.pk
        cls.nested_models['participating_organization'] = [org.pk for org in
                                                           instance.participating_organization.all()]
        cls.nested_models['is_related_to_document'] = [doc.pk for doc in
                                                       instance.is_related_to_document.all()]
        LOGGER.info(cls.nested_models)
        instance.delete()

    def test_project_identifier_is_deleted(self):
        try:
            for pk in self.nested_models['project_identifier']:
                obj = models.ProjectIdentifier.objects.get(id=pk)
                LOGGER.info('Project ID {}'.format(obj.pk))
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_funder_are_not_deleted(self):
        try:
            for pk in self.nested_models['funder']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_domain_are_not_deleted(self):
        try:
            for pk in self.nested_models['domain']:
                obj = models.Domain.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_subject_are_not_deleted(self):
        try:
            for pk in self.nested_models['subject']:
                obj = models.Subject.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_social_media_occupational_account_are_deleted(self):
        try:
            for pk in self.nested_models['social_media_occupational_account']:
                obj = models.SocialMediaOccupationalAccount.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_cost_is_deleted(self):
        try:
            obj = models.Cost.objects.get(id=self.nested_models['cost'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_ec_max_contribution_is_deleted(self):
        try:
            obj = models.Cost.objects.get(
                id=self.nested_models['ec_max_contribution'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_national_max_contribution_is_deleted(self):
        try:
            obj = models.Cost.objects.get(
                id=self.nested_models['national_max_contribution'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_coordinator_is_not_deleted(self):
        try:
            obj = models.Organization.objects.get(
                id=self.nested_models['coordinator'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_participating_organizations_are_not_deleted(self):
        try:
            for pk in self.nested_models['participating_organization']:
                obj = models.Organization.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_related_to_document_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_related_to_document']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_project_is_deleted(self):
        try:
            obj = models.Project.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityDocument(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/document.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        serializer.is_valid()
        instance = instance_md.described_entity

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['document_identifier'] = [pid.pk for pid in
                                                    instance.document_identifier.all()]
        cls.nested_models['author'] = [author.pk for author in
                                       instance.author.all()]
        cls.nested_models['editor'] = [editor.pk for editor in
                                       instance.editor.all()]
        cls.nested_models['contributor'] = [contributor.pk for contributor in
                                            instance.contributor.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['subject'] = [subject.pk for subject in
                                        instance.subject.all()]
        LOGGER.info(cls.nested_models)
        instance_md.delete()

    def test_document_identifier_is_deleted(self):
        try:
            for pk in self.nested_models['document_identifier']:
                obj = models.DocumentIdentifier.objects.get(id=pk)
                LOGGER.info('Document ID {}'.format(obj.pk))
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_author_are_not_deleted(self):
        try:
            for pk in self.nested_models['author']:
                obj = models.Person.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_editor_are_not_deleted(self):
        try:
            for pk in self.nested_models['editor']:
                obj = models.Person.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_contributor_are_not_deleted(self):
        try:
            for pk in self.nested_models['contributor']:
                obj = models.Person.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_domain_are_not_deleted(self):
        try:
            for pk in self.nested_models['domain']:
                obj = models.Domain.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_subject_are_not_deleted(self):
        try:
            for pk in self.nested_models['subject']:
                obj = models.Subject.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_document_is_deleted(self):
        try:
            obj = models.Document.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityOrganization(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']
        serializer = serializers.OrganizationSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['organization_identifier'] = [pid.pk for pid in
                                                        instance.organization_identifier.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['member_of_association'] = [member_of_association.pk
                                                      for member_of_association
                                                      in
                                                      instance.member_of_association.all()]
        cls.nested_models['other_office_address'] = [other_office_address.pk
                                                     for other_office_address
                                                     in
                                                     instance.other_office_address.all()]
        cls.nested_models['head_office_address'] = instance.head_office_address.pk
        cls.nested_models['social_media_occupational_account'] = [sm_account.pk
                                                                  for sm_account
                                                                  in
                                                                  instance.social_media_occupational_account.all()]
        cls.nested_models['is_division_of'] = instance.is_division_of.all().first().pk
        LOGGER.info(cls.nested_models)
        instance.delete()

    def test_organization_identifier_is_deleted(self):
        try:
            for pk in self.nested_models['organization_identifier']:
                obj = models.OrganizationIdentifier.objects.get(id=pk)
                LOGGER.info('Organization ID {}'.format(obj.pk))
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_domain_are_not_deleted(self):
        try:
            for pk in self.nested_models['domain']:
                obj = models.Domain.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_member_of_association_are_not_deleted(self):
        try:
            for pk in self.nested_models['member_of_association']:
                obj = models.Organization.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_other_address_set_are_deleted(self):
        try:
            for pk in self.nested_models['other_office_address']:
                obj = models.AddressSet.objects.get(id=pk)
                self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_head_address_set_is_deleted(self):
        try:
            obj = models.AddressSet.objects.get(id=self.nested_models['head_office_address'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_social_media_occupational_account_are_deleted(self):
        try:
            for pk in self.nested_models['social_media_occupational_account']:
                obj = models.SocialMediaOccupationalAccount.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_division_of_is_not_deleted(self):
        try:
            obj = models.Organization.objects.get(id=self.nested_models['is_division_of'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_organization_is_deleted(self):
        try:
            obj = models.Organization.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityPerson(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for Person
        json_file = open('registry/tests/fixtures/person.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']
        json_data['social_media_occupational_account'] = [{
            'social_media_occupational_account_type': 'http://w3id.org/meta-share/meta-share/facebook',
            'value': 'value',
        }]
        json_data['address_set'] = [{
            "address": {
                "en": "address"
            },
            "region": {
                "en": "region"
            },
            "zip_code": "14671",
            "city": {
                "en": "Athens"
            },
            "country": "GR"
        }]
        json_data['affiliation'] = [{
            "position": {
                "en": "position"
            },
            "affiliated_organization": {
                "organization_name": {
                    "en": "random_org"
                },
                "organization_identifier": [
                    {
                        "organization_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                        "value": "doi_value_id"
                    }
                ],
                "website": [
                    "http://www.orgwebsite.com"
                ]
            },
            "email": [
                "www.affiliation@email.com"
            ],
            "homepage": [],
            "address_set": [
                {
                    "address": {
                        "en": "address"
                    },
                    "region": {
                        "en": "region"
                    },
                    "zip_code": "14671",
                    "city": {
                        "en": "Athens"
                    },
                    "country": "GR"
                }
            ],
            "telephone_number": [],
            "fax_number": []
        }]
        serializer = serializers.PersonSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['personal_identifier'] = [pid.pk for pid in
                                                    instance.personal_identifier.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['member_of_association'] = [member_of_association.pk
                                                      for member_of_association
                                                      in
                                                      instance.member_of_association.all()]
        cls.nested_models['address_set'] = [address_set.pk for address_set in
                                            instance.address_set.all()]
        cls.nested_models['social_media_occupational_account'] = [sm_account.pk
                                                                  for sm_account
                                                                  in
                                                                  instance.social_media_occupational_account.all()]
        cls.nested_models['affiliation'] = [affiliation.pk for affiliation in
                                            instance.affiliation.all()]
        LOGGER.info(cls.nested_models)
        instance.delete()

    def test_personal_identifier_is_deleted(self):
        try:
            for pk in self.nested_models['personal_identifier']:
                obj = models.PersonalIdentifier.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_domain_are_not_deleted(self):
        try:
            for pk in self.nested_models['domain']:
                obj = models.Domain.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_member_of_association_are_not_deleted(self):
        try:
            for pk in self.nested_models['member_of_association']:
                obj = models.Organization.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_address_set_are_deleted(self):
        try:
            for pk in self.nested_models['address_set']:
                obj = models.AddressSet.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_social_media_occupational_account_are_deleted(self):
        try:
            for pk in self.nested_models['social_media_occupational_account']:
                obj = models.SocialMediaOccupationalAccount.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_affiliation_are_deleted(self):
        try:
            for pk in self.nested_models['affiliation']:
                obj = models.Affiliation.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_person_are_deleted(self):
        try:
            obj = models.Person.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLicenceTerms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']
        serializer = serializers.LicenceTermsSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['licence_identifier'] = [pid.pk for pid in
                                                   instance.licence_identifier.all()]
        instance.delete()

    def test_licence_identifier_is_deleted(self):
        try:
            for pk in self.nested_models['licence_identifier']:
                obj = models.LicenceIdentifier.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_licence_terms_are_deleted(self):
        try:
            obj = models.LicenceTerms.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityGenericMetadataRecord(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_data = {
            "metadata_record_identifier": {
                "metadata_record_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                "value": "ELG-MDR-LRS-030220-00000006"
            }
        }
        serializer = serializers.GenericMetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'metadata_record_identifier'] = instance.metadata_record_identifier.pk
        instance.delete()

    def test_metadata_record_identifier_is_deleted(self):
        try:
            obj = models.MetadataRecordIdentifier.objects.get(
                id=self.nested_models['metadata_record_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_metadata_record_is_deleted(self):
        try:
            obj = models.MetadataRecord.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityRole(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['role']
        serializer = serializers.RoleSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'role_identifier'] = instance.role_identifier.pk
        instance.delete()

    def test_role_identifier_is_deleted(self):
        try:
            obj = models.RoleIdentifier.objects.get(
                id=self.nested_models['role_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_role_is_deleted(self):
        try:
            obj = models.Role.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityDomain(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['domain']
        serializer = serializers.DomainSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'domain_identifier'] = instance.domain_identifier.pk
        instance.delete()

    def test_domain_identifier_is_deleted(self):
        try:
            obj = models.DomainIdentifier.objects.get(
                id=self.nested_models['domain_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_domain_is_deleted(self):
        try:
            obj = models.Domain.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalitySubject(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['subject']
        serializer = serializers.SubjectSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'subject_identifier'] = instance.subject_identifier.pk
        instance.delete()

    def test_subject_identifier_is_deleted(self):
        try:
            obj = models.SubjectIdentifier.objects.get(
                id=self.nested_models['subject_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_subject_is_deleted(self):
        try:
            obj = models.Subject.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)



class TestDeletionFunctionalityAffiliation(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['affiliation']
        serializer = serializers.AffiliationSerializer(data=cls.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'affiliated_organization'] = instance.affiliated_organization.pk
        cls.nested_models['address_set'] = [address_set.pk for address_set in
                                            instance.address_set.all()]
        instance.delete()

    def test_affiliated_organization_is_not_deleted(self):
        try:
            obj = models.Organization.objects.get(
                id=self.nested_models['affiliated_organization'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_address_set_is_deleted(self):
        try:
            for pk in self.nested_models['address_set']:
                obj = models.AddressSet.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_affiliation_is_deleted(self):
        try:
            obj = models.Affiliation.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityAttribution(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['attribution']
        serializer = serializers.AttributionSerializer(data=cls.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'attributed_agent'] = instance.attributed_agent.pk
        cls.nested_models['had_role'] = [role.pk for role in
                                            instance.had_role.all()]
        instance.delete()

    def test_attributed_agent_is_not_deleted(self):
        try:
            obj = models.Person.objects.get(
                id=self.nested_models['attributed_agent'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_had_role_are_not_deleted(self):
        try:
            for pk in self.nested_models['had_role']:
                obj = models.Role.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityAttributionFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['attribution']
        serializer = serializers.AttributionSerializer(data=cls.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'attributed_agent'] = instance.attributed_agent.pk
        cls.nested_models['had_role'] = [role.pk for role in
                                            instance.had_role.all()]

    def test_attribution_is_not_deleted_when_attributed_agent_is(self):
        obj = models.Person.objects.get(
            id=self.nested_models['attributed_agent'])
        obj.delete()
        try:
            initial_instance = models.Attribution.objects.get(pk=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_attribution_is_not_deleted_when_had_role_are(self):
        for pk in self.nested_models['had_role']:
            obj = models.Role.objects.get(id=pk)
            obj.delete()
        try:
            initial_instance = models.Attribution.objects.get(pk=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLanguageResource(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file_crp = open('registry/tests/fixtures/corpus.json', 'r')
        json_str_crp = json_file_crp.read()
        json_file_crp.close()
        json_data_crp = json.loads(json_str_crp)
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data_lr = json_data['described_entity']
        json_data_lr['has_metadata'] = [json_data_crp]
        serializer = serializers.LanguageResourceSerializer(data=json_data_lr)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        # instance = instance_md.described_entity

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['lr_identifier'] = [pid.pk for pid in
                                              instance.lr_identifier.all()]
        cls.nested_models['additional_info'] = [additional_info.pk for
                                                additional_info in
                                                instance.additional_info.all()]
        cls.nested_models['contact'] = [contact.pk for contact in
                                        instance.contact.all()]
        cls.nested_models['ipr_holder'] = [ipr_holder.pk for ipr_holder in
                                           instance.ipr_holder.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['subject'] = [subject.pk for subject in
                                        instance.subject.all()]
        cls.nested_models['resource_provider'] = [resource_provider.pk
                                                  for resource_provider
                                                  in
                                                  instance.resource_provider.all()]
        cls.nested_models['resource_creator'] = [resource_creator.pk for
                                                 resource_creator in
                                                 instance.resource_creator.all()]
        cls.nested_models['funding_project'] = [funding_project.pk for
                                                funding_project in
                                                instance.funding_project.all()]
        cls.nested_models['actual_use'] = [actual_use.pk for actual_use in
                                           instance.actual_use.all()]
        cls.nested_models['validation'] = [validation.pk for validation in
                                           instance.validation.all()]
        cls.nested_models['is_documented_by'] = [is_documented_by.pk for
                                                 is_documented_by in
                                                 instance.is_documented_by.all()]
        cls.nested_models['is_described_by'] = [is_described_by.pk for
                                                is_described_by in
                                                instance.is_described_by.all()]
        cls.nested_models['is_cited_by'] = [is_cited_by.pk for is_cited_by in
                                            instance.is_cited_by.all()]
        cls.nested_models['is_reviewed_by'] = [is_reviewed_by.pk for
                                               is_reviewed_by in
                                               instance.is_reviewed_by.all()]
        cls.nested_models['is_part_of'] = [is_part_of.pk for is_part_of in
                                           instance.is_part_of.all()]
        cls.nested_models['is_part_with'] = [is_part_with.pk for is_part_with in
                                             instance.is_part_with.all()]
        cls.nested_models['is_similar_to'] = [is_similar_to.pk for is_similar_to
                                              in
                                              instance.is_similar_to.all()]
        cls.nested_models['is_exact_match_with'] = [is_exact_match_with.pk for
                                                    is_exact_match_with in
                                                    instance.is_exact_match_with.all()]
        cls.nested_models['has_metadata'] = [has_metadata.pk for has_metadata in
                                             instance.has_metadata.all()]
        cls.nested_models['is_archived_by'] = [is_archived_by.pk for
                                               is_archived_by in
                                               instance.is_archived_by.all()]
        cls.nested_models['is_continuation_of'] = [is_continuation_of.pk for
                                                   is_continuation_of in
                                                   instance.is_continuation_of.all()]
        cls.nested_models['replaces'] = [replaces.pk for replaces in
                                         instance.replaces.all()]
        cls.nested_models['is_version_of'] = instance.is_version_of.pk
        cls.nested_models['relation'] = [relation.pk for relation in
                                         instance.relation.all()]
        cls.nested_models['has_original_source'] = [has_original_source.pk for
                                                    has_original_source in
                                                    instance.has_original_source.all()]
        cls.nested_models['is_created_by'] = [is_created_by.pk for
                                              is_created_by in
                                              instance.is_created_by.all()]
        cls.nested_models['lr_subclass'] = instance.lr_subclass.pk
        LOGGER.info(cls.nested_models)
        instance.delete()

    def test_lr_identifier_are_deleted(self):
        try:
            for pk in self.nested_models['lr_identifier']:
                obj = models.LRIdentifier.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_additional_info_are_deleted(self):
        try:
            for pk in self.nested_models['additional_info']:
                obj = models.additionalInfo.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_contact_are_not_deleted(self):
        try:
            for pk in self.nested_models['contact']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_ipr_holder_are_not_deleted(self):
        try:
            for pk in self.nested_models['ipr_holder']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_domain_are_not_deleted(self):
        try:
            for pk in self.nested_models['domain']:
                obj = models.Domain.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_subject_are_not_deleted(self):
        try:
            for pk in self.nested_models['subject']:
                obj = models.Subject.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_resource_provider_are_not_deleted(self):
        try:
            for pk in self.nested_models['resource_provider']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_resource_creator_are_not_deleted(self):
        try:
            for pk in self.nested_models['resource_creator']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_funding_project_are_not_deleted(self):
        try:
            for pk in self.nested_models['funding_project']:
                obj = models.Project.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_actual_use_are_deleted(self):
        try:
            for pk in self.nested_models['actual_use']:
                obj = models.ActualUse.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_validation_are_deleted(self):
        try:
            for pk in self.nested_models['validation']:
                obj = models.Validation.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_documented_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_documented_by']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_described_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_described_by']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_cited_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_cited_by']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_reviewed_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_reviewed_by']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_part_of_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_part_of']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_part_with_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_part_with']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_similar_to_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_similar_to']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_exact_match_with_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_exact_match_with']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_has_metadata_are_deleted(self):
        try:
            for pk in self.nested_models['has_metadata']:
                obj = models.GenericMetadataRecord.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_archived_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_archived_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_continuation_of_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_continuation_of']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_replaces_are_not_deleted(self):
        try:
            for pk in self.nested_models['replaces']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_version_of_is_not_deleted(self):
        try:
            obj = models.LanguageResource.objects.get(
                id=self.nested_models['is_version_of'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_relation_are_deleted(self):
        try:
            for pk in self.nested_models['relation']:
                obj = models.Relation.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_lr_subclass_is_deleted(self):
        try:
            obj = models.GenericMetadataRecord.objects.get(
                id=self.nested_models['lr_subclass'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_has_original_source_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_original_source']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_created_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_created_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lr_is_deleted(self):
        try:
            obj = models.LanguageResource.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityActualUse(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['actual_use']
        serializer = serializers.ActualUseSerializer(data=cls.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['has_outcome'] = [outcome.pk for outcome in
                                            instance.has_outcome.all()]
        cls.nested_models['usage_project'] = [project.pk for project in
                                              instance.usage_project.all()]
        cls.nested_models['usage_report'] = [document.pk for document in
                                             instance.usage_report.all()]

        instance.delete()

    def test_has_outcome_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_outcome']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_usage_project_are_not_deleted(self):
        try:
            for pk in self.nested_models['usage_project']:
                obj = models.Project.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_usage_report_are_not_deleted(self):
        try:
            for pk in self.nested_models['usage_report']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_actual_use_is_deleted(self):
        try:
            obj = models.ActualUse.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityValidation(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['validation']
        serializer = serializers.ValidationSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['is_validated_by'] = [lr.pk for lr in
                                                instance.is_validated_by.all()]
        cls.nested_models['validation_report'] = [document.pk for document in
                                                  instance.validation_report.all()]
        cls.nested_models['validator'] = [actor.pk for actor in
                                          instance.validator.all()]

        instance.delete()

    def test_is_validated_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_validated_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_validation_report_are_not_deleted(self):
        try:
            for pk in self.nested_models['validation_report']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_validator_are_not_deleted(self):
        try:
            for pk in self.nested_models['validator']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_validation_is_deleted(self):
        try:
            obj = models.Validation.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityRelation(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['relation']
        serializer = serializers.RelationSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['related_lr'] = instance.related_lr.pk

        instance.delete()

    def test_related_lr_is_not_deleted(self):
        try:
            obj = models.LanguageResource.objects.get(
                id=self.nested_models['related_lr'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_relation_is_deleted(self):
        try:
            obj = models.Relation.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityToolService(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']['lr_subclass']
        serializer = serializers.ToolServiceSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['software_distribution'] = [sd.pk for sd in
                                                      instance.software_distribution.all()]
        cls.nested_models['input_content_resource'] = [input_cr.pk for
                                                       input_cr in
                                                       instance.input_content_resource.all()]
        cls.nested_models['output_resource'] = [output.pk for output in
                                                instance.output_resource.all()]
        cls.nested_models['typesystem'] = instance.typesystem.pk
        cls.nested_models['annotation_schema'] =  [anno_schema.pk for anno_schema in
            instance.annotation_schema.all()]
        cls.nested_models['annotation_resource'] = [lr.pk for lr in
                                                    instance.annotation_resource.all()]
        cls.nested_models['ml_model'] = [lr.pk
                                         for lr
                                         in
                                         instance.ml_model.all()]
        cls.nested_models['evaluation'] = [evaluation.pk for
                                           evaluation in
                                           instance.evaluation.all()]
        cls.nested_models['parameter'] = [parameter.pk for parameter in
                                          instance.parameter.all()]
        instance.delete()

    def test_software_distribution_are_deleted(self):
        try:
            for pk in self.nested_models['software_distribution']:
                obj = models.SoftwareDistribution.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_input_content_resource_are_deleted(self):
        try:
            for pk in self.nested_models['input_content_resource']:
                obj = models.ProcessingResource.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_output_resource_are_deleted(self):
        try:
            for pk in self.nested_models['output_resource']:
                obj = models.ProcessingResource.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_typesystem_is_not_deleted(self):
        try:
            obj = models.LanguageResource.objects.get(
                id=self.nested_models['typesystem'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_schema_are_not_deleted(self):
        try:
            for pk in self.nested_models['annotation_schema']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_resource_are_not_deleted(self):
        try:
            for pk in self.nested_models['annotation_resource']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_ml_model_are_not_deleted(self):
        try:
            for pk in self.nested_models['ml_model']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_evaluation_are_deleted(self):
        try:
            for pk in self.nested_models['evaluation']:
                obj = models.Evaluation.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_parameter_are_deleted(self):
        try:
            for pk in self.nested_models['parameter']:
                obj = models.Parameter.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_tool_is_deleted(self):
        try:
            obj = models.ToolService.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalitySoftwareDistribution(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for SoftwareDistribution
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['software_distribution']
        serializer = serializers.SoftwareDistributionSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        c_f, _ = ContentFile.objects.get_or_create(file=f'asdasd/test_filename.zip')
        instance.package = c_f
        instance.save()
        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['is_described_by'] = [doc.pk for doc in
                                                instance.is_described_by.all()]
        cls.nested_models['licence_terms'] = [licence.pk for
                                              licence in
                                              instance.licence_terms.all()]
        cls.nested_models['distribution_rights_holder'] = [actor.pk for actor in
                                                           instance.distribution_rights_holder.all()]
        cls.nested_models['cost'] = instance.cost.pk
        cls.nested_models['access_rights'] = [access_rights.pk for access_rights
                                              in
                                              instance.access_rights.all()]
        cls.nested_models['package'] = instance.package.pk
        instance.delete()

    def test_is_described_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_described_by']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_licence_terms_are_not_deleted(self):
        try:
            for pk in self.nested_models['licence_terms']:
                obj = models.LicenceTerms.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_distribution_rights_holder_are_not_deleted(self):
        try:
            for pk in self.nested_models['distribution_rights_holder']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_cost_is_deleted(self):
        try:
            obj = models.Cost.objects.get(
                id=self.nested_models['cost'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_access_rights_are_not_deleted(self):
        try:
            for pk in self.nested_models['access_rights']:
                obj = models.AccessRightsStatement.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_package_is_deleted(self):
        try:
            obj = ContentFile.objects.get(
                id=self.nested_models['package'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_software_distribution_is_deleted(self):
        try:
            obj = models.SoftwareDistribution.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalitySoftwareDistributionFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['software_distribution']
        serializer = serializers.SoftwareDistributionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        c_f, _ = ContentFile.objects.get_or_create(file=f'fdashgdasda/test_filename.zip')
        instance.package = c_f
        instance.save()
        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['is_described_by'] = [
            lr.pk for lr in
            instance.is_described_by.all()]
        cls.nested_models['licence_terms'] = [licence.pk for
                                              licence in
                                              instance.licence_terms.all()]
        cls.nested_models['cost'] = instance.cost.pk
        cls.nested_models['distribution_rights_holder'] = [actor.pk for actor in
                                                           instance.distribution_rights_holder.all()]
        cls.nested_models['access_rights'] = [access_rights.pk for access_rights
                                              in
                                              instance.access_rights.all()]
        cls.nested_models['package'] = instance.package.pk

    def test_software_distribution_is_not_deleted_when_software_distribution_is_described_by_is(self):
        for pk in self.nested_models['is_described_by']:
            obj = models.Document.objects.get(id=pk)
            obj.delete()
        try:
            initial_instance = models.SoftwareDistribution.objects.get(id=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_software_distribution_is_not_deleted_when_package_is(self):
        obj = ContentFile.objects.get(id=self.nested_models['package'])
        obj.delete()
        try:
            initial_instance = models.SoftwareDistribution.objects.get(id=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_software_distribution_is_not_deleted_when_software_distribution_licence_terms_is(self):
        for pk in self.nested_models['licence_terms']:
            obj = models.LicenceTerms.objects.get(id=pk)
            obj.delete()
        try:
            initial_instance = models.SoftwareDistribution.objects.get(id=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_software_distribution_is_not_deleted_when_software_distribution_cost_is(self):
        obj = models.Cost.objects.get(id=self.nested_models['cost'])
        obj.delete()
        try:
            initial_instance = models.SoftwareDistribution.objects.get(id=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_software_distribution_is_not_deleted_when_software_distribution_distribution_rights_holder_is(self):
        for pk in self.nested_models['distribution_rights_holder']:
            obj = models.Actor.objects.get(id=pk)
            obj.delete()
        try:
            initial_instance = models.SoftwareDistribution.objects.get(id=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_software_distribution_is_not_deleted_when_software_distribution_access_right_is(self):
        for pk in self.nested_models['access_rights']:
            obj = models.AccessRightsStatement.objects.get(id=pk)
            obj.delete()
        try:
            initial_instance = models.SoftwareDistribution.objects.get(id=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityProcessingResource(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for ProcessingResource
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['processing_resource']
        serializer = serializers.ProcessingResourceSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [lang.pk for lang in
                                         instance.language.all()]
        cls.nested_models['typesystem'] = instance.typesystem.pk
        cls.nested_models['annotation_schema'] = [anno_schema.pk for anno_schema in instance.annotation_schema.all()]
        cls.nested_models[
            'annotation_resource'] = instance.annotation_resource.pk
        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_typesystem_is_not_deleted(self):
        try:
            obj = models.LanguageResource.objects.get(
                id=self.nested_models['typesystem'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_schema_are_not_deleted(self):
        try:
            for pk in self.nested_models['annotation_schema']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_resource_is_not_deleted(self):
        try:
            obj = models.LanguageResource.objects.get(
                id=self.nested_models['annotation_resource'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_processing_resource_is_deleted(self):
        try:
            obj = models.ProcessingResource.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityEvaluation(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['evaluation']
        serializer = serializers.EvaluationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['performance_indicator'] = [performance.pk for
                                                      performance in
                                                      instance.performance_indicator.all()]
        cls.nested_models['evaluation_report'] = [doc.pk for doc in
                                                  instance.evaluation_report.all()]
        cls.nested_models['is_evaluated_by'] = [lr.pk for lr in
                                                instance.is_evaluated_by.all()]
        cls.nested_models['evaluator'] = [actor.pk for actor in
                                          instance.evaluator.all()]

        instance.delete()

    def test_performance_indicator_are_deleted(self):
        try:
            for pk in self.nested_models['performance_indicator']:
                obj = models.PerformanceIndicator.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_evaluated_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_evaluated_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_evaluation_report_are_not_deleted(self):
        try:
            for pk in self.nested_models['evaluation_report']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_evaluator_are_not_deleted(self):
        try:
            for pk in self.nested_models['evaluator']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_evaluation_is_deleted(self):
        try:
            obj = models.Evaluation.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityCorpus(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        serializer = serializers.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['corpus_media_part'] = [corpus_media_part.pk for
                                                  corpus_media_part in
                                                  instance.corpus_media_part.all()]
        cls.nested_models['dataset_distribution'] = [dd.pk for dd in
                                                     instance.dataset_distribution.all()]
        cls.nested_models['is_analysed_by'] = [lr.pk for lr in
                                               instance.is_analysed_by.all()]
        cls.nested_models['is_edited_by'] = [lr.pk for lr in
                                             instance.is_edited_by.all()]
        cls.nested_models['is_elicited_by'] = [lr.pk for lr in
                                               instance.is_elicited_by.all()]
        cls.nested_models[
            'is_annotated_version_of'] = instance.is_annotated_version_of.pk
        cls.nested_models[
            'is_aligned_version_of'] = instance.is_aligned_version_of.pk
        cls.nested_models[
            'is_converted_version_of'] = [lr.pk for lr in
                                          instance.is_converted_version_of.all()]
        instance.delete()
        json_file_unsp = open('test_fixtures/dummy_data.json', 'r')
        json_str_unsp = json_file_unsp.read()
        json_file_unsp.close()
        raw_data_unsp = json.loads(json_str_unsp)['unspecified_part']
        dist_unsp_feature = json.loads(json_str_unsp)['distribution_unspecified_feature']
        raw_data['unspecified_part'] = raw_data_unsp
        raw_data['corpus_media_part'] = []
        raw_data['dataset_distribution'][0]['distribution_unspecified_feature'] = dist_unsp_feature
        raw_data['dataset_distribution'][0]['distribution_text_feature'] = []
        raw_data['dataset_distribution'][1]['distribution_unspecified_feature'] = dist_unsp_feature
        raw_data['dataset_distribution'][1]['distribution_text_feature'] = []
        serializer_unsp = serializers.CorpusSerializer(data=raw_data, context={'for_information_only': True})
        if serializer_unsp.is_valid():
            instance_unsp = serializer_unsp.save()
        else:
            LOGGER.info(serializer_unsp.errors)
        cls.instance_unsp_pk = instance_unsp.pk
        cls.nested_models[
            'unspecified_part'] = instance_unsp.unspecified_part.pk
        instance_unsp.delete()

    def test_corpus_media_part_are_deleted(self):
        try:
            for pk in self.nested_models['corpus_media_part']:
                obj = models.CorpusMediaPart.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_unspecified_part_is_deleted(self):
        try:
            obj = models.unspecifiedPart.objects.get(id=self.nested_models['unspecified_part'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_dataset_distribution_are_deleted(self):
        try:
            for pk in self.nested_models['dataset_distribution']:
                obj = models.DatasetDistribution.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_analysed_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_analysed_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_elicited_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_elicited_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_edited_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_edited_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_annotated_version_of_is_not_deleted(self):
        try:
            obj = models.LanguageResource.objects.get(
                id=self.nested_models['is_annotated_version_of'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_aligned_version_of_is_not_deleted(self):
        try:
            obj = models.LanguageResource.objects.get(
                id=self.nested_models['is_aligned_version_of'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_converted_version_of_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_converted_version_of']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_is_deleted(self):
        try:
            obj = models.Corpus.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityUnspecifiedPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for CorpusTextPart
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['unspecified_part']
        serializer = serializers.unspecifiedPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language.pk for
                                         language in
                                         instance.metalanguage.all()]
        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metalanguage_are_not_deleted(self):
        try:
            for pk in self.nested_models['metalanguage']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_unspecified_part_are_deleted(self):
        try:
            obj = models.unspecifiedPart.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityCorpusTextPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for CorpusTextPart
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['text_corpus_part']
        serializer = serializers.CorpusTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['text_type'] = [text_type.pk for
                                          text_type in
                                          instance.text_type.all()]
        cls.nested_models['text_genre'] = [text_genre.pk for
                                           text_genre in
                                           instance.text_genre.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_are_deleted(self):
        try:
            for pk in self.nested_models['annotation']:
                obj = models.Annotation.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_text_type_are_not_deleted(self):
        try:
            for pk in self.nested_models['text_type']:
                obj = models.TextType.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_text_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['text_genre']:
                obj = models.TextGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_created_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_created_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_has_original_source_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_original_source']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_link_to_other_media_are_deleted(self):
        try:
            for pk in self.nested_models['link_to_other_media']:
                obj = models.LinkToOtherMedia.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_corpus_text_part_are_deleted(self):
        try:
            obj = models.CorpusTextPart.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityCorpusAudioPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus_audio_part']
        serializer = serializers.CorpusAudioPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['speech_genre'] = [speech_genre.pk for
                                             speech_genre in
                                             instance.speech_genre.all()]
        cls.nested_models['audio_genre'] = [audio_genre.pk for
                                            audio_genre in
                                            instance.audio_genre.all()]
        cls.nested_models['recorder'] = [actor.pk for
                                         actor in
                                         instance.recorder.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_is_deleted(self):
        try:
            for pk in self.nested_models['annotation']:
                obj = models.Annotation.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_speech_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['speech_genre']:
                obj = models.SpeechGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_audio_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['audio_genre']:
                obj = models.AudioGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_recorder_are_not_deleted(self):
        try:
            for pk in self.nested_models['recorder']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_created_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_created_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_has_original_source_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_original_source']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_link_to_other_media_are_deleted(self):
        try:
            for pk in self.nested_models['link_to_other_media']:
                obj = models.LinkToOtherMedia.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_corpus_audio_part_are_deleted(self):
        try:
            obj = models.CorpusAudioPart.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityCorpusVideoPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus_video_part']
        serializer = serializers.CorpusVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['video_genre'] = [video_genre.pk for
                                            video_genre in
                                            instance.video_genre.all()]
        cls.nested_models['dynamic_element'] = [dynamic_element.pk for
                                                dynamic_element in
                                                instance.dynamic_element.all()]
        cls.nested_models['recorder'] = [actor.pk for
                                         actor in
                                         instance.recorder.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_are_deleted(self):
        try:
            for pk in self.nested_models['annotation']:
                obj = models.Annotation.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_video_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['video_genre']:
                obj = models.VideoGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dynamic_element_are_deleted(self):
        try:
            for pk in self.nested_models['dynamic_element']:
                obj = models.DynamicElement.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_recorder_are_not_deleted(self):
        try:
            for pk in self.nested_models['recorder']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_created_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_created_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_has_original_source_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_original_source']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_link_to_other_media_are_deleted(self):
        try:
            for pk in self.nested_models['link_to_other_media']:
                obj = models.LinkToOtherMedia.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_corpus_video_part_are_deleted(self):
        try:
            obj = models.CorpusVideoPart.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityCorpusImagePart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus_image_part']
        serializer = serializers.CorpusImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['image_genre'] = [image_genre.pk for
                                            image_genre in
                                            instance.image_genre.all()]
        cls.nested_models['static_element'] = [static_element.pk for
                                               static_element in
                                               instance.static_element.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_are_deleted(self):
        try:
            for pk in self.nested_models['annotation']:
                obj = models.Annotation.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_image_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['image_genre']:
                obj = models.ImageGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_static_element_are_deleted(self):
        try:
            for pk in self.nested_models['static_element']:
                obj = models.StaticElement.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_created_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_created_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_has_original_source_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_original_source']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_link_to_other_media_are_deleted(self):
        try:
            for pk in self.nested_models['link_to_other_media']:
                obj = models.LinkToOtherMedia.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_corpus_image_part_are_deleted(self):
        try:
            obj = models.CorpusImagePart.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityCorpusTextNumericalPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus_text_numerical_part']
        serializer = serializers.CorpusTextNumericalPartSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['recorder'] = [actor.pk for
                                         actor in
                                         instance.recorder.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

        instance.delete()

    def test_recorder_are_not_deleted(self):
        try:
            for pk in self.nested_models['recorder']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_are_deleted(self):
        try:
            for pk in self.nested_models['recorder']:
                obj = models.Annotation.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_created_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_created_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_has_original_source_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_original_source']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_link_to_other_media_are_deleted(self):
        try:
            for pk in self.nested_models['link_to_other_media']:
                obj = models.LinkToOtherMedia.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_corpus_text_numerical_part_are_deleted(self):
        try:
            obj = models.CorpusTextNumericalPart.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityTextType(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for TextType
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['text_type']
        serializer = serializers.TextTypeSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'text_type_identifier'] = instance.text_type_identifier.pk

        instance.delete()

    def test_text_type_identifier_is_deleted(self):
        try:
            obj = models.TextTypeIdentifier.objects.get(
                id=self.nested_models['text_type_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_text_type_is_deleted(self):
        try:
            obj = models.TextType.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityTextGenre(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for TextGenre
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['text_genre']
        serializer = serializers.TextGenreSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'text_genre_identifier'] = instance.text_genre_identifier.pk

        instance.delete()

    def test_text_genre_identifier_is_deleted(self):
        try:
            obj = models.TextGenreIdentifier.objects.get(
                id=self.nested_models['text_genre_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_text_genre_is_deleted(self):
        try:
            obj = models.TextGenre.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityAudioGenre(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for AudioGenre
        cls.raw_data = {
            "category_label": {
                "en": "broadcast news"
            },
            "audio_genre_identifier": {
                "audio_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                "value": "String"
            }
        }
        serializer = serializers.AudioGenreSerializer(data=cls.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'audio_genre_identifier'] = instance.audio_genre_identifier.pk

        instance.delete()

    def test_audio_genre_identifier_is_deleted(self):
        try:
            obj = models.AudioGenreIdentifier.objects.get(
                id=self.nested_models['audio_genre_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_audio_genre_is_deleted(self):
        try:
            obj = models.AudioGenre.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalitySpeechGenre(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for SpeechGenre
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['speech_genre']
        serializer = serializers.SpeechGenreSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'speech_genre_identifier'] = instance.speech_genre_identifier.pk

        instance.delete()

    def test_speech_genre_identifier_is_deleted(self):
        try:
            obj = models.SpeechGenreIdentifier.objects.get(
                id=self.nested_models['speech_genre_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_speech_genre_is_deleted(self):
        try:
            obj = models.SpeechGenre.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityVideoGenre(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for VideoGenre
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['video_genre']
        serializer = serializers.VideoGenreSerializer(
            data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'video_genre_identifier'] = instance.video_genre_identifier.pk

        instance.delete()

    def test_video_genre_identifier_is_deleted(self):
        try:
            obj = models.VideoGenreIdentifier.objects.get(
                id=self.nested_models['video_genre_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_video_genre_is_deleted(self):
        try:
            obj = models.VideoGenre.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityImageGenre(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for ImageGenre
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['image_genre']
        serializer = serializers.ImageGenreSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'image_genre_identifier'] = instance.image_genre_identifier.pk

        instance.delete()

    def test_image_genre_identifier_is_deleted(self):
        try:
            obj = models.ImageGenreIdentifier.objects.get(
                id=self.nested_models['image_genre_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_image_genre_is_deleted(self):
        try:
            obj = models.ImageGenre.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityDatasetDistribution(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['dataset_distribution']
        serializer = serializers.DatasetDistributionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        c_f, _ = ContentFile.objects.get_or_create(file=f'fdasdasda/test_filename.zip')
        instance.dataset = c_f
        instance.save()
        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['is_accessed_by'] = [
            lr.pk for lr in
            instance.is_accessed_by.all()]
        cls.nested_models['is_displayed_by'] = [
            lr.pk for lr in
            instance.is_displayed_by.all()]
        cls.nested_models['is_queried_by'] = [
            lr.pk for lr in
            instance.is_queried_by.all()]
        cls.nested_models['distribution_text_feature'] = [
            distribution_text_feature.pk for
            distribution_text_feature in
            instance.distribution_text_feature.all()]
        cls.nested_models['distribution_text_numerical_feature'] = [
            distribution_text_numerical_feature.pk for
            distribution_text_numerical_feature in
            instance.distribution_text_numerical_feature.all()]
        cls.nested_models['distribution_audio_feature'] = [
            distribution_audio_feature.pk for
            distribution_audio_feature in
            instance.distribution_audio_feature.all()]
        cls.nested_models['distribution_image_feature'] = [
            distribution_image_feature.pk for
            distribution_image_feature in
            instance.distribution_image_feature.all()]
        cls.nested_models['distribution_video_feature'] = [
            distribution_video_feature.pk for
            distribution_video_feature in
            instance.distribution_video_feature.all()]
        cls.nested_models['licence_terms'] = [licence.pk for
                                              licence in
                                              instance.licence_terms.all()]
        cls.nested_models['cost'] = instance.cost.pk
        cls.nested_models['distribution_rights_holder'] = [actor.pk for actor in
                                                           instance.distribution_rights_holder.all()]
        cls.nested_models['access_rights'] = [access_rights.pk for access_rights
                                              in
                                              instance.access_rights.all()]
        cls.nested_models['dataset'] = instance.dataset.pk
        instance.delete()
        json_file_fi = open('test_fixtures/dummy_data.json', 'r')
        json_str_fi = json_file_fi.read()
        json_file_fi.close()
        raw_data_fi = json.loads(json_str_fi)['dataset_distribution']
        raw_data_fi['distribution_unspecified_feature'] = {
            "size": [
              {
                "amount": 10.0,
                "size_unit": "http://w3id.org/meta-share/meta-share/T-HPair"
              }
            ],
            "data_format": [
              "http://w3id.org/meta-share/omtd-share/Json"
            ]
        }
        serializer_fi = serializers.DatasetDistributionSerializer(data=raw_data_fi)
        if serializer_fi.is_valid():
            instance_fi = serializer_fi.save()
        else:
            LOGGER.info(serializer_fi.errors)
        cls.instance_fi_pk = instance_fi.pk
        cls.nested_models['distribution_unspecified_feature'] = instance_fi.distribution_unspecified_feature.pk
        instance_fi.delete()


    def test_is_accessed_by_is_not_deleted(self):
        try:
            for pk in self.nested_models['is_accessed_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_displayed_by_is_not_deleted(self):
        try:
            for pk in self.nested_models['is_displayed_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_queried_by_is_not_deleted(self):
        try:
            for pk in self.nested_models['is_queried_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_distribution_text_feature_are_deleted(self):
        try:
            for pk in self.nested_models['distribution_text_feature']:
                obj = models.DistributionTextFeature.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_text_numerical_feature_are_deleted(self):
        try:
            for pk in self.nested_models['distribution_text_numerical_feature']:
                obj = models.DistributionTextNumericalFeature.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_audio_feature_are_deleted(self):
        try:
            for pk in self.nested_models['distribution_audio_feature']:
                obj = models.DistributionAudioFeature.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_image_feature_are_deleted(self):
        try:
            for pk in self.nested_models['distribution_image_feature']:
                obj = models.DistributionAudioFeature.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_video_feature_are_deleted(self):
        try:
            for pk in self.nested_models['distribution_video_feature']:
                obj = models.DistributionVideoFeature.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_unspecified_feature_is_deleted(self):
        try:
            obj = models.Cost.objects.get(
                id=self.nested_models['distribution_unspecified_feature'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_licence_terms_are_not_deleted(self):
        try:
            for pk in self.nested_models['licence_terms']:
                obj = models.LicenceTerms.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_cost_is_deleted(self):
        try:
            obj = models.Cost.objects.get(
                id=self.nested_models['cost'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_rights_holder_are_not_deleted(self):
        try:
            for pk in self.nested_models['distribution_rights_holder']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_access_rights_are_not_deleted(self):
        try:
            for pk in self.nested_models['access_rights']:
                obj = models.AccessRightsStatement.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_is_deleted(self):
        try:
            obj = ContentFile.objects.get(
                id=self.nested_models['dataset'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_distribution_are_deleted(self):
        try:
            obj = models.DatasetDistribution.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityDistributionUnspecifiedFeature(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_unspecified_feature']
        serializer = serializers.distributionUnspecifiedFeatureSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]
        instance.delete()

    def test_size_are_deleted(self):
        try:
            for pk in self.nested_models['size']:
                obj = models.Size.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_unspecified_feature_are_deleted(self):
        try:
            obj = models.DistributionTextFeature.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityDistributionTextFeature(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_text_feature']
        serializer = serializers.DistributionTextFeatureSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]
        instance.delete()

    def test_size_are_deleted(self):
        try:
            for pk in self.nested_models['size']:
                obj = models.Size.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_text_feature_are_deleted(self):
        try:
            obj = models.DistributionTextFeature.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityDistributionTextNumericalFeature(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_text_numerical_feature']
        serializer = serializers.DistributionTextNumericalFeatureSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]
        instance.delete()

    def test_size_are_deleted(self):
        try:
            for pk in self.nested_models['size']:
                obj = models.Size.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_text_numerical_feature_are_deleted(self):
        try:
            obj = models.DistributionTextNumericalFeature.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityDistributionAudioFeature(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for DistributionAudioFeature
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_audio_feature_serializer']
        serializer = serializers.DistributionAudioFeatureSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]

        cls.nested_models['duration_of_audio'] = [duration.pk for duration in
                                                  instance.duration_of_audio.all()]
        cls.nested_models['duration_of_effective_speech'] = [duration.pk for
                                                             duration in
                                                             instance.duration_of_effective_speech.all()]
        cls.nested_models['audio_format'] = [audio_format.pk for audio_format in
                                             instance.audio_format.all()]
        instance.delete()

    def test_size_are_deleted(self):
        try:
            for pk in self.nested_models['size']:
                obj = models.Size.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_duration_of_audio_are_deleted(self):
        try:
            for pk in self.nested_models['duration_of_audio']:
                obj = models.Duration.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_duration_of_effective_speech_are_deleted(self):
        try:
            for pk in self.nested_models['duration_of_effective_speech']:
                obj = models.Duration.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_audio_format_are_deleted(self):
        try:
            for pk in self.nested_models['audio_format']:
                obj = models.AudioFormat.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_audio_feature_are_deleted(self):
        try:
            obj = models.DistributionAudioFeature.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityDistributionImageFeature(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_image_feature']
        serializer = serializers.DistributionImageFeatureSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]
        cls.nested_models['image_format'] = [image_format.pk for image_format in
                                             instance.image_format.all()]
        instance.delete()

    def test_size_are_deleted(self):
        try:
            for pk in self.nested_models['size']:
                obj = models.Size.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_image_format_are_deleted(self):
        try:
            for pk in self.nested_models['image_format']:
                obj = models.ImageFormat.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_image_feature_are_deleted(self):
        try:
            obj = models.DistributionImageFeature.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityDistributionVideoFeature(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_video_feature']
        serializer = serializers.DistributionVideoFeatureSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]
        cls.nested_models['video_format'] = [video_format.pk for video_format in
                                             instance.video_format.all()]
        instance.delete()

    def test_size_are_deleted(self):
        try:
            for pk in self.nested_models['size']:
                obj = models.Size.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_video_format_are_deleted(self):
        try:
            for pk in self.nested_models['video_format']:
                obj = models.VideoFormat.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_distribution_video_feature_are_deleted(self):
        try:
            obj = models.DistributionVideoFeature.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityImageFormat(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for ImageFormat
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['image_format']
        serializer = serializers.ImageFormatSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['resolution'] = [resolution.pk for resolution in
                                           instance.resolution.all()]
        instance.delete()

    def test_resolution_is_not_deleted(self):
        try:
            for pk in self.nested_models['resolution']:
                obj = models.Resolution.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_image_format_is_deleted(self):
        try:
            obj = models.ImageFormat.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityVideoFormat(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for VideoFormat
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['video_format']
        serializer = serializers.VideoFormatSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['resolution'] = [resolution.pk for resolution in
                                           instance.resolution.all()]
        instance.delete()

    def test_resolution_are_not_deleted(self):
        try:
            for pk in self.nested_models['resolution']:
                obj = models.Resolution.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_video_format_is_deleted(self):
        try:
            obj = models.VideoFormat.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityAnnotation(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['annotation']
        serializer = serializers.AnnotationSerializer(data=cls.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['guidelines'] = [doc.pk for
                                           doc in
                                           instance.guidelines.all()]
        cls.nested_models['typesystem'] = [lr.pk for lr in
                                       instance.typesystem.all()]
        cls.nested_models['annotation_resource'] = [lr.pk for lr in
                                       instance.annotation_resource.all()]
        cls.nested_models[
            'is_annotated_by'] = [lr.pk for lr in
                                  instance.is_annotated_by.all()]
        cls.nested_models['annotator'] = [actor.pk for actor in
                                          instance.annotator.all()]
        cls.nested_models['annotation_report'] = [doc.pk for
                                                  doc in
                                                  instance.annotation_report.all()]
        cls.nested_models['is_annotated_by'] = [lr.pk for lr in
                                                instance.is_annotated_by.all()]
        instance.delete()

    def test_guidelines_are_not_deleted(self):
        try:
            for pk in self.nested_models['guidelines']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_typesystem_are_not_deleted(self):
        try:
            for pk in self.nested_models['typesystem']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_resource_are_not_deleted(self):
        try:
            for pk in self.nested_models['annotation_resource']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotator_are_not_deleted(self):
        try:
            for pk in self.nested_models['annotator']:
                obj = models.Actor.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_report_are_not_deleted(self):
        try:
            for pk in self.nested_models['annotation_report']:
                obj = models.Document.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_annotated_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_annotated_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_is_deleted(self):
        try:
            obj = models.Annotation.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLanguageDescription(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/ld_grammar.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        instance = instance_md.described_entity.lr_subclass

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'language_description_subclass'] = instance.language_description_subclass.pk
        cls.nested_models['language_description_media_part'] = [
            language_description_media_part.pk for
            language_description_media_part in
            instance.language_description_media_part.all()]
        cls.nested_models['dataset_distribution'] = [dd.pk for dd in
                                                     instance.dataset_distribution.all()]
        cls.nested_models['is_analysed_by'] = [lr.pk for lr in
                                               instance.is_analysed_by.all()]
        cls.nested_models['is_edited_by'] = [lr.pk for lr in
                                             instance.is_edited_by.all()]
        cls.nested_models['is_elicited_by'] = [lr.pk for lr in
                                               instance.is_elicited_by.all()]
        cls.nested_models[
            'is_converted_version_of'] = [lr.pk for lr in
                                          instance.is_converted_version_of.all()]
        instance.delete()
        json_file_model = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str_model = json_file_model.read()
        json_file_model.close()
        json_data_model = json.loads(json_str_model)
        json_data_model['described_entity']['resource_name'] = {'en': 'other model f'}
        json_data_model['described_entity']['resource_short_name'] = {'en': 'other model ff'}
        json_data_model['described_entity']['version'] = 14
        json_data_model['described_entity']['lr_identifier'] = []
        serializer_model = serializers.MetadataRecordSerializer(data=json_data_model)
        if serializer_model.is_valid():
            instance_model_md = serializer_model.save()
        else:
            LOGGER.info(serializer_model.errors)
        instance_model = instance_model_md.described_entity.lr_subclass
        cls.nested_models[
            'unspecified_part'] = instance_model.unspecified_part.pk
        instance_model.delete()

    def test_language_description_subclass_is_deleted(self):
        try:
            obj = models.LanguageDescriptionSubclass.objects.get(
                id=self.nested_models['language_description_subclass'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_language_description_media_part_are_deleted(self):
        try:
            for pk in self.nested_models['language_description_media_part']:
                obj = models.LanguageDescriptionMediaPart.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_dataset_distribution_are_deleted(self):
        try:
            for pk in self.nested_models['dataset_distribution']:
                obj = models.DatasetDistribution.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_unspecified_part_is_deleted(self):
        try:
            obj = models.unspecifiedPart.objects.get(id=self.nested_models['unspecified_part'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_analysed_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_analysed_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_elicited_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_elicited_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_edited_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_edited_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_converted_version_of_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_converted_version_of']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_language_description_are_deleted(self):
        try:
            obj = models.LanguageDescription.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLanguageDescriptionTextPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['ld_text_part']
        serializer = serializers.LanguageDescriptionTextPartSerializer(
            data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language_variety.pk for
                                             language_variety in
                                             instance.metalanguage.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metalanguage_are_not_deleted(self):
        try:
            for pk in self.nested_models['metalanguage']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_created_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_created_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_has_original_source_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_original_source']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_link_to_other_media_are_deleted(self):
        try:
            for pk in self.nested_models['link_to_other_media']:
                obj = models.LinkToOtherMedia.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_ld_text_part_are_deleted(self):
        try:
            obj = models.LanguageDescriptionTextPart.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLanguageDescriptionImagePart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['ld_image_part']
        serializer = serializers.LanguageDescriptionImagePartSerializer(
            data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language.pk for
                                             language in
                                             instance.metalanguage.all()]
        cls.nested_models['static_element'] = [static_element.pk for
                                               static_element in
                                               instance.static_element.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metalanguage_are_not_deleted(self):
        try:
            for pk in self.nested_models['metalanguage']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_static_element_are_deleted(self):
        try:
            for pk in self.nested_models['static_element']:
                obj = models.StaticElement.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_created_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_created_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_has_original_source_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_original_source']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_link_to_other_media_are_deleted(self):
        try:
            for pk in self.nested_models['link_to_other_media']:
                obj = models.LinkToOtherMedia.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_ld_image_part_are_deleted(self):
        try:
            obj = models.LanguageDescriptionImagePart.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLanguageDescriptionVideoPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['ld_video_part']
        serializer = serializers.LanguageDescriptionVideoPartSerializer(
            data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language.pk for
                                             language in
                                             instance.metalanguage.all()]
        cls.nested_models['dynamic_element'] = [dynamic_element.pk for
                                                dynamic_element in
                                                instance.dynamic_element.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metalanguage_are_not_deleted(self):
        try:
            for pk in self.nested_models['metalanguage']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dynamic_element_are_deleted(self):
        try:
            for pk in self.nested_models['dynamic_element']:
                obj = models.DynamicElement.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_created_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_created_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_has_original_source_are_not_deleted(self):
        try:
            for pk in self.nested_models['has_original_source']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_link_to_other_media_are_deleted(self):
        try:
            for pk in self.nested_models['link_to_other_media']:
                obj = models.LinkToOtherMedia.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_ld_video_part_are_deleted(self):
        try:
            obj = models.LanguageDescriptionVideoPart.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityNGramModel(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['ngram_model']
        serializer = serializers.NGramModelSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        instance.delete()


    def test_ngram_model_deleted(self):
        try:
            obj = models.NGramModel.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityModel(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data = json_data['described_entity']['lr_subclass'][
            'language_description_subclass']
        serializer = serializers.ModelSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        ngram_json_file = open('test_fixtures/dummy_data.json', 'r')
        ngram_json_str = ngram_json_file.read()
        ngram_json_file.close()
        ngram_raw_data = json.loads(ngram_json_str)['ngram_model']
        ngram_serializer = serializers.NGramModelSerializer(data=ngram_raw_data)
        if ngram_serializer.is_valid():
            ngram_instance = ngram_serializer.save()
        else:
            LOGGER.info(ngram_serializer.errors)
        # Retrieve pk from nested models
        instance.n_gram_model = ngram_instance
        instance.save()
        cls.instance_pk = instance.pk
        cls.nested_models = dict()

        cls.nested_models['typesystem'] = [lr.pk for lr in
                                           instance.typesystem.all()]
        cls.nested_models['annotation_schema'] = [lr.pk for lr in
                                                  instance.annotation_schema.all()]
        cls.nested_models['annotation_resource'] = [lr.pk for lr in
                                                    instance.annotation_resource.all()]
        cls.nested_models['tagset'] = [lr.pk
                                       for lr
                                       in
                                       instance.tagset.all()]
        cls.nested_models['n_gram_model'] = instance.n_gram_model.pk
        instance.delete()

    def test_typesystem_are_not_deleted(self):
        try:
            for pk in self.nested_models['typesystem']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_schema_are_not_deleted(self):
        try:
            for pk in self.nested_models['annotation_schema']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_annotation_resource_are_not_deleted(self):
        try:
            for pk in self.nested_models['annotation_resource']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_tagset_are_not_deleted(self):
        try:
            for pk in self.nested_models['tagset']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_n_gram_model_is_not_deleted(self):
        try:
            obj = models.NGramModel.objects.get(id=self.nested_models['n_gram_model'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_model_deleted(self):
        try:
            obj = models.Model.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityGrammar(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['grammar']
        serializer = serializers.GrammarSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()

        cls.nested_models['requires_lr'] = [lr.pk for lr in
                                            instance.requires_lr.all()]
        instance.delete()

    def test_requires_lr_are_not_deleted(self):
        try:
            for pk in self.nested_models['requires_lr']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_grammar_is_not_deleted(self):
        try:
            obj = models.Grammar.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLexicalConceptualResource(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_data = {
            "metadata_record_identifier": {
                "metadata_record_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                "value": "ELG-MDR-LRS-260320-00000046"
            },
            "metadata_creation_date": "2020-03-26",
            "metadata_last_date_updated": "2020-04-12",
            "metadata_curator": [
                {
                    "actor_type": "Person",
                    "surname": {
                        "en": "Smith"
                    },
                    "given_name": {
                        "en": "John"
                    },
                    "personal_identifier": [
                        {
                            "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                            "value": "0000-0000-0000-0000"
                        }
                    ]
                }
            ],
            "complies_with": "http://w3id.org/meta-share/meta-share/ELG-SHARE",
            "metadata_creator": {
                "actor_type": "Person",
                "surname": {
                    "en": "Smith"
                },
                "given_name": {
                    "en": "John"
                },
                "personal_identifier": [
                    {
                        "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                        "value": "0000-0000-0000-0000"
                    }
                ]
            },
            "source_of_metadata_record": {
                "repository_name": {
                    "en": "Repo name"
                },
                "repository_url": "http://www.repo.com",
                "repository_identifier": [
                    {
                        "repository_identifier_scheme": "http://purl.org/spar/datacite/handle",
                        "value": "repository test value"
                    }
                ]
            },
            "source_metadata_record": None,
            "revision": None,
            "described_entity": {
                "entity_type": "LanguageResource",
                "physical_resource": [
                    "placeholder for data resources",
                    "text"
                ],
                "resource_name": {
                    "en": "full title of a tool/service"
                },
                "resource_short_name": {
                    "en": "short name"
                },
                "description": {
                    "en": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue. Lobortis scelerisque fermentum dui faucibus in ornare quam viverra. Tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero. Ullamcorper velit sed ullamcorper morbi tincidunt ornare. Diam sollicitudin tempor id eu nisl nunc mi. Risus in hendrerit gravida rutrum quisque. Fusce ut placerat orci nulla pellentesque. Id neque aliquam vestibulum morbi blandit cursus risus at ultrices. Id venenatis a condimentum vitae. Malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi. Imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada. Auctor neque vitae tempus quam. Hac habitasse platea dictumst quisque sagittis purus sit amet volutpat. Aliquam id diam maecenas ultricies mi eget mauris pharetra. Tempor orci eu lobortis elementum nibh tellus. Faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis."
                },
                "lr_identifier": [
                    {
                        "lr_identifier_scheme": "http://purl.org/spar/datacite/handle",
                        "value": "http://hdl.grnet.gr/11500/ATHENA-0000-0000-23F2-7"
                    }
                ],
                "logo": "https://cdn.vox-cdn.com/thumbor/uploads/chorus_image/image/65613211/microsoftedgenewlogo.5.jpg",
                "version":  "1.0.0",
                "version_date": "1967-08-13",
                "update_frequency": {
                    "en": "rarely"
                },
                "revision": {
                    "en": "some revision text"
                },
                "additional_info": [
                    {
                        "landing_page": "http://www.exampleTool.com"
                    }
                ],
                "contact": [
                    {
                        "actor_type": "Person",
                        "surname": {
                            "en": "Smith"
                        },
                        "given_name": {
                            "en": "John"
                        },
                        "personal_identifier": [
                            {
                                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                "value": "0000-0000-0000-0000"
                            }
                        ]
                    }
                ],
                "mailing_list_name": [
                    "name"
                ],
                "discussion_url": [
                    "http://www.discussionForum.com"
                ],
                "citation_text": {
                    "en": "citation text"
                },
                "ipr_holder": [
                    {
                        "actor_type": "Person",
                        "surname": {
                            "en": "Smith"
                        },
                        "given_name": {
                            "en": "John"
                        },
                        "personal_identifier": [
                            {
                                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                "value": "0000-0000-0000-0000"
                            }
                        ]
                    }
                ],
                "keyword": [
                    {
                        "en": "keyword1"
                    },
                    {
                        "en": "keyword2"
                    }
                ],
                "domain": [
                    {
                        "category_label": {
                            "en": "domain"
                        },
                        "domain_identifier": {
                            "domain_classification_scheme": "http://w3id.org/meta-share/meta-share/ELG_domainClassification",
                            "value": "xxx"
                        }
                    },
                    {
                        "category_label": {
                            "en": "domain2"
                        },
                        "domain_identifier": {
                            "domain_classification_scheme": "http://w3id.org/meta-share/meta-share/ANC_domainClassification",
                            "value": "String"
                        }
                    }
                ],
                "subject": [
                    {
                        "category_label": {
                            "en": "subject1"
                        },
                        "subject_identifier": {
                            "subject_classification_scheme": "http://w3id.org/meta-share/meta-share/PAROLE_topicClassification",
                            "value": "String"
                        }
                    },
                    {
                        "category_label": {
                            "en": "subject2"
                        },
                        "subject_identifier": {
                            "subject_classification_scheme": "http://w3id.org/meta-share/meta-share/PAROLE_topicClassification",
                            "value": "String"
                        }
                    }
                ],
                "resource_provider": [
                    {
                        "actor_type": "Person",
                        "surname": {
                            "en": "Smith"
                        },
                        "given_name": {
                            "en": "John"
                        },
                        "personal_identifier": [
                            {
                                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                "value": "0000-0000-0000-0000"
                            }
                        ]
                    },
                    {
                        "actor_type": "Organization",
                        "organization_name": {
                            "en": "Distribution holder"
                        },
                        "organization_identifier": [
                            {
                                "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "DOI"
                            }
                        ],
                        "website": [
                            "http://www.organization.com"
                        ]
                    }
                ],
                "publication_date": "1967-08-13",
                "resource_creator": [
                    {
                        "actor_type": "Person",
                        "surname": {
                            "en": "Smith"
                        },
                        "given_name": {
                            "en": "John"
                        },
                        "personal_identifier": [
                            {
                                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                "value": "0000-0000-0000-0000"
                            }
                        ]
                    },
                    {
                        "actor_type": "Organization",
                        "organization_name": {
                            "en": "Distribution holder"
                        },
                        "organization_identifier": [
                            {
                                "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "DOI"
                            }
                        ],
                        "website": [
                            "http://www.organization.com"
                        ]
                    }
                ],
                "creation_start_date": "1967-08-13",
                "creation_end_date": "1969-08-13",
                "creation_mode": "http://w3id.org/meta-share/meta-share/interactive",
                "creation_details": {
                    "en": "Lorem ipsum"
                },
                "has_original_source": [
                    {
                        "resource_name": {
                            "en": "another lexicon"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/islrn",
                                "value": "pid2"
                            },
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/purl",
                                "value": "pid1"
                            }
                        ],
                        "version":  "unspecified"
                    },
                    {
                        "resource_name": {
                            "en": "new lexicon"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/metaShare",
                                "value": "noIid"
                            },
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/url",
                                "value": "urlid"
                            }
                        ],
                        "version":  "13"
                    }
                ],
                "original_source_description": {
                    "en": "a lexicon of 12 words"
                },
                "is_created_by": [
                    {
                        "resource_name": {
                            "en": "creation tool"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/isbn",
                                "value": "isbn13"
                            },
                            {
                                "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                "value": "docker3"
                            }
                        ],
                        "version":  "44"
                    },
                    {
                        "resource_name": {
                            "en": "another creation tool"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/purl",
                                "value": "purlid"
                            },
                            {
                                "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/maven",
                                "value": "mavenid"
                            }
                        ],
                        "version":  "4.0.7"
                    }
                ],
                "funding_project": [
                    {
                        "project_name": {
                            "en": "project 2"
                        },
                        "project_identifier": [
                            {
                                "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                "value": "String"
                            }
                        ],
                        "website": [
                            "http://www.project2.com"
                        ]
                    },
                    {
                        "project_name": {
                            "en": "full title of a project"
                        },
                        "website": [
                            "http://www.project.com"
                        ]
                    }
                ],
                "intended_application": [
                    "http://w3id.org/meta-share/omtd-share/NegationDetection",
                    "http://w3id.org/meta-share/omtd-share/AnnotationOfCompoundingFeatures"
                ],
                "actual_use": [
                    {
                        "used_in_application": [
                            "http://w3id.org/meta-share/omtd-share/AuthorClassification",
                            "http://w3id.org/meta-share/omtd-share/QuestionAnswering",
                            "Free text used in Application"
                        ],
                        "has_outcome": [
                            {
                                "resource_name": {
                                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                },
                                "version":  "1.0.0 (automatically assigned)"
                            },
                            {
                                "resource_name": {
                                    "en": "name of a resource"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/metaShare",
                                        "value": "String"
                                    }
                                ],
                                "version":  "1.0.0"
                            },
                            {
                                "resource_name": {
                                    "en": "resource2"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtd",
                                        "value": "String"
                                    }
                                ],
                                "version":  "1.0.3"
                            }
                        ],
                        "usage_project": [
                            {
                                "project_name": {
                                    "en": "project 2"
                                },
                                "project_identifier": [
                                    {
                                        "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                        "value": "String"
                                    }
                                ],
                                "website": [
                                    "http://www.project2.com"
                                ]
                            }
                        ],
                        "usage_report": [
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            },
                            {
                                "title": {
                                    "en": "report2"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/pmcid",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            }
                        ],
                        "actual_use_details": {
                            "en": "details on use of a tool"
                        }
                    },
                    {
                        "used_in_application": [
                            "http://w3id.org/meta-share/omtd-share/EvaluationOfBroadCoverageNaturalLanguageParsers",
                            "http://w3id.org/meta-share/omtd-share/DeepParserPerformanceEvaluation",
                            "Free text LT Area"
                        ],
                        "has_outcome": [
                            {
                                "resource_name": {
                                    "en": "resource4"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/islrn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "13"
                            },
                            {
                                "resource_name": {
                                    "en": "resource5"
                                },
                                "version":  "1"
                            }
                        ],
                        "usage_project": [
                            {
                                "project_name": {
                                    "en": "project 2"
                                },
                                "project_identifier": [
                                    {
                                        "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                        "value": "String"
                                    }
                                ],
                                "website": [
                                    "http://www.project2.com"
                                ]
                            }
                        ],
                        "usage_report": [
                            {
                                "title": {
                                    "en": "report2"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/pmcid",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            },
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            }
                        ],
                        "actual_use_details": {
                            "en": "details on use of a tool"
                        }
                    }
                ],
                "validated": True,
                "validation": [
                    {
                        "validation_type": "http://w3id.org/meta-share/meta-share/formal",
                        "validation_mode": "http://w3id.org/meta-share/meta-share/interactive",
                        "validation_details": {
                            "en": "detailsof validation"
                        },
                        "validation_extent": "http://w3id.org/meta-share/meta-share/full",
                        "is_validated_by": [
                            {
                                "resource_name": {
                                    "en": "validation tool"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "12"
                            },
                            {
                                "resource_name": {
                                    "en": "validation tool2"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "14"
                            }
                        ],
                        "validation_report": [
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            },
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            }
                        ],
                        "validator": [
                            {
                                "actor_type": "Person",
                                "surname": {
                                    "en": "Smith"
                                },
                                "given_name": {
                                    "en": "John"
                                },
                                "personal_identifier": [
                                    {
                                        "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                        "value": "0000-0000-0000-0000"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "validation_type": "http://w3id.org/meta-share/meta-share/content",
                        "validation_mode": "http://w3id.org/meta-share/meta-share/manual",
                        "validation_details": {
                            "en": "free text for validation"
                        },
                        "validation_extent": "http://w3id.org/meta-share/meta-share/full",
                        "is_validated_by": [
                            {
                                "resource_name": {
                                    "en": "validation tool"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "12"
                            },
                            {
                                "resource_name": {
                                    "en": "validation tool2"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "14"
                            }
                        ],
                        "validation_report": [
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            },
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            }
                        ],
                        "validator": [
                            {
                                "actor_type": "Person",
                                "surname": {
                                    "en": "Smith"
                                },
                                "given_name": {
                                    "en": "John"
                                },
                                "personal_identifier": [
                                    {
                                        "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                        "value": "0000-0000-0000-0000"
                                    }
                                ]
                            }
                        ]
                    }
                ],
                "is_documented_by": [
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://purl.org/spar/datacite/ark",
                                "value": "https://research.google/tools/datasets/discofuse/"
                            }
                        ]
                    }
                ],
                "is_described_by": [
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://purl.org/spar/datacite/ark",
                                "value": "https://research.google/tools/datasets/discofuse/"
                            }
                        ]
                    },
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://purl.org/spar/datacite/ark",
                                "value": "https://research.google/tools/datasets/discofuse/"
                            }
                        ]
                    }
                ],
                "is_cited_by": [
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://purl.org/spar/datacite/ark",
                                "value": "https://research.google/tools/datasets/discofuse/"
                            }
                        ]
                    },
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                                "value": "ELG-ENT-DOC-260320-00000576"
                            }
                        ]
                    }
                ],
                "is_reviewed_by": [
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                                "value": "ELG-ENT-DOC-260320-00000577"
                            }
                        ]
                    },
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                                "value": "ELG-ENT-DOC-260320-00000578"
                            }
                        ]
                    }
                ],
                "is_part_of": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    }
                ],
                "is_part_with": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    }
                ],
                "is_similar_to": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    }
                ],
                "is_exact_match_with": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    }
                ],
                "has_metadata": [
                    {
                        "metadata_record_identifier": {
                            "metadata_record_identifier_scheme": "http://w3id.org/meta-share/meta-share/CORE",
                            "value": "String"
                        }
                    }
                ],
                "is_archived_by": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    }
                ],
                "is_continuation_of": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    }
                ],
                "replaces": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }],
                        "version":  "unspecified"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    }
                ],
                "is_version_of": {
                    "resource_name": {
                        "en": "another resource"
                    },
                    "lr_identifier": [
                        {
                            "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "https://doi.org/10.5281/zenodo.3525366"
                        }
                    ],
                    "version":  "unspecified"
                },
                "relation": [
                    {
                        "relation_type": {
                            "en": "relation type"
                        },
                        "related_lr": {
                            "resource_name": {
                                "en": "another resource"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                    "value": "https://doi.org/10.5281/zenodo.3525366"
                                }
                            ],
                            "version":  "unspecified"
                        }
                    },
                    {
                        "relation_type": {
                            "en": "relation type2"
                        },
                        "related_lr": {
                            "resource_name": {
                                "en": "another resource"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                    "value": "https://doi.org/10.5281/zenodo.3525366"
                                }
                            ],
                            "version":  "unspecified"
                        }
                    }
                ],
                "lr_subclass": {
                    "lr_type": "LexicalConceptualResource",
                    "lcr_subclass": "http://w3id.org/meta-share/meta-share/computationalLexicon",
                    "encoding_level": [
                        "http://w3id.org/meta-share/meta-share/phonology",
                        "http://w3id.org/meta-share/meta-share/morphology"
                    ],
                    "content_type": [
                        "http://w3id.org/meta-share/meta-share/collocation",
                        "http://w3id.org/meta-share/meta-share/definition"
                    ],
                    "complies_with": [
                        "http://w3id.org/meta-share/meta-share/EAGLES",
                        "http://w3id.org/meta-share/meta-share/SemAF-SR"
                    ],
                    "external_reference": [
                        {
                            "resource_name": {
                                "en": "French wordnet"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                    "value": "randomId2"
                                },
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtd",
                                    "value": "id2"
                                }
                            ],
                            "version":  "1.0"
                        },
                        {
                            "resource_name": {
                                "en": "English wordnet"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                    "value": "randomId"
                                },
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtd",
                                    "value": "id"
                                }
                            ],
                            "version":  "1.0"
                        }
                    ],
                    "theoretic_model": [
                        {
                            "en": "Generative lexicon"
                        }
                    ],
                    "extratextual_information": [
                        "http://w3id.org/meta-share/meta-share/soundRecording",
                        "http://w3id.org/meta-share/meta-share/video1"
                    ],
                    "extratextual_information_unit": [
                        "http://w3id.org/meta-share/meta-share/word2",
                        "http://w3id.org/meta-share/meta-share/syntacticUnit"
                    ],
                    "lexical_conceptual_resource_media_part": [
                        {
                            "lcr_media_type": "LexicalConceptualResourceImagePart",
                            "media_type": "http://w3id.org/meta-share/meta-share/image",
                            "linguality_type": "http://w3id.org/meta-share/meta-share/bilingual",
                            "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
                            "multilinguality_type_details": {
                                "en": "lemma and translation"
                            },
                            "language": [
                                {
                                    "language_tag": "tgd-Bugi-MAh-e-p-b-u-r-n",
                                    "language_id": "tgd",
                                    "script_id": "Bugi",
                                    "region_id": "MA",
                                    "variant_id": ["hepburn"]
                                }
                            ],
                            "language_variety": [
                                {
                                    "language_variety_type": "http://w3id.org/meta-share/meta-share/other",
                                    "language_variety_name": {
                                        "en": "french dialect"
                                    }
                                },
                                {
                                    "language_variety_type": "http://w3id.org/meta-share/meta-share/jargon",
                                    "language_variety_name": {
                                        "en": "village dialect"
                                    }
                                }
                            ],
                            "metalanguage": [
                                {
                                    "language_tag": "okb-Grek-VCa-r-e-v-e-l-a",
                                    "language_id": "okb",
                                    "script_id": "Grek",
                                    "region_id": "VC",
                                    "variant_id": ["arevela"]
                                },
                                {
                                    "language_tag": "nfr-Nkoo-UGt-a-r-a-s-k",
                                    "language_id": "nfr",
                                    "script_id": "Nkoo",
                                    "region_id": "UG",
                                    "variant_id": ["tarask"]
                                }
                            ],
                            "modality_type": [
                                "http://w3id.org/meta-share/meta-share/voice",
                                "http://w3id.org/meta-share/meta-share/writtenLanguage"
                            ],
                            "type_of_image_content": [{
                                "en": "various vehicles"
                            }],
                            "text_included_in_image": [
                                "http://w3id.org/meta-share/meta-share/none2",
                                "http://w3id.org/meta-share/meta-share/caption"
                            ],
                            "static_element": [
                                {
                                    "type_of_element": [{
                                        "en": "car"
                                    }],
                                    "body_part": [
                                        "http://w3id.org/meta-share/meta-share/foot",
                                        "http://w3id.org/meta-share/meta-share/face"
                                    ],
                                    "face_view": [{
                                        "en": "profile"
                                    }],
                                    "face_expression": [{
                                        "en": "happy"
                                    }],
                                    "artifact_part": [{
                                        "en": "tyre"
                                    }],
                                    "landscape_part": [{
                                        "en": "tree"
                                    }],
                                    "person_description": {
                                        "en": "fat"
                                    },
                                    "thing_description": {
                                        "en": "wheel"
                                    },
                                    "organization_description": {
                                        "en": "institution"
                                    },
                                    "event_description": {
                                        "en": "birth"
                                    }
                                },
                                {
                                    "type_of_element": [{
                                        "en": "car"
                                    }],
                                    "body_part": [
                                        "http://w3id.org/meta-share/meta-share/foot",
                                        "http://w3id.org/meta-share/meta-share/face"
                                    ],
                                    "face_view": [{
                                        "en": "profile"
                                    }],
                                    "face_expression": [{
                                        "en": "happy"
                                    }],
                                    "artifact_part": [{
                                        "en": "tyre"
                                    }],
                                    "landscape_part": [{
                                        "en": "countryside"
                                    }],
                                    "person_description": {
                                        "en": "tall"
                                    },
                                    "thing_description": {
                                        "en": "car"
                                    },
                                    "organization_description": {
                                        "en": "institution"
                                    },
                                    "event_description": {
                                        "en": "wedding"
                                    }
                                }
                            ]
                        }
                    ],
                    "dataset_distribution": [
                        {
                            "dataset_distribution_form": "http://w3id.org/meta-share/meta-share/accessibleThroughInterface",
                            "download_location": "http://www.download.com",
                            "access_location": "http://www.access.com",
                            "is_accessed_by": [
                                {
                                    "resource_name": {
                                        "en": "access tool"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://purl.org/spar/datacite/handle",
                                            "value": "pid1"
                                        }
                                    ],
                                    "version":  "13"
                                }
                            ],
                            "is_displayed_by": [
                                {
                                    "resource_name": {
                                        "en": "display tool"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                            "value": "id43"
                                        }
                                    ],
                                    "version":  "14"
                                }
                            ],
                            "is_queried_by": [
                                {
                                    "resource_name": {
                                        "en": "query tool"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/metaShare",
                                            "value": "msId"
                                        }
                                    ],
                                    "version":  "33"
                                }
                            ],
                            "samples_location": [
                                "ftp://www.samples.com"
                            ],
                            "distribution_image_feature": [
                                {
                                    "size": [
                                        {
                                            "amount": 5.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/question"
                                        },
                                        {
                                            "amount": 12.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/second"
                                        }
                                    ],
                                    "data_format": [
                                        "http://w3id.org/meta-share/omtd-share/BionlpSt2013A1_a2",
                                        "http://w3id.org/meta-share/omtd-share/LD_json"
                                    ],
                                    "image_format": [
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/BionlpSt2013A1_a2",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/CMYK"
                                            ],
                                            "colour_depth": [
                                                10,
                                                4
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 50,
                                                    "size_height": 50,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/mov",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                            "quality": "http://w3id.org/meta-share/meta-share/high1"
                                        },
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/LD_json",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/RGB"
                                            ],
                                            "colour_depth": [
                                                10,
                                                10
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 24,
                                                    "size_height": 24,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/TwoD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/oggVorbis",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/vector",
                                            "quality": "http://w3id.org/meta-share/meta-share/medium1"
                                        }
                                    ]
                                },
                                {
                                    "size": [
                                        {
                                            "amount": 3.14159,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/gb"
                                        },
                                        {
                                            "amount": 3.14159,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/shot1"
                                        }
                                    ],
                                    "data_format": [
                                        "http://w3id.org/meta-share/omtd-share/Xces",
                                        "http://w3id.org/meta-share/omtd-share/LD_json"
                                    ],
                                    "image_format": [
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/Xces",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/CMYK",
                                                "http://w3id.org/meta-share/meta-share/RGB"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/TwoD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/shorten",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/vector",
                                            "quality": "http://w3id.org/meta-share/meta-share/medium1"
                                        },
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/LD_json",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/CMYK",
                                                "http://w3id.org/meta-share/meta-share/RGB"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/TwoD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/mp3",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/vector",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryLow1"
                                        }
                                    ]
                                }
                            ],
                            "licence_terms": [
                                {
                                    "licence_terms_name": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "licence_terms_url": [
                                        "http://www.licecen3.com",
                                        "http://www.licece44.com"
                                    ],
                                    "condition_of_use": [
                                        "http://w3id.org/meta-share/meta-share/unspecified"
                                    ],
                                    "licence_identifier": [
                                        {
                                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                            "value": "String"
                                        }
                                    ]
                                },
                                {
                                    "licence_terms_name": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "licence_terms_url": [
                                        "http://www.licecen1.com",
                                        "http://www.licecen2.com"
                                    ],
                                    "condition_of_use": [
                                        "http://w3id.org/meta-share/meta-share/unspecified"
                                    ],
                                    "licence_identifier": [
                                        {
                                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX",
                                            "value": "String"
                                        }
                                    ]
                                }
                            ],
                            "attribution_text": {
                                "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                            },
                            "cost": {
                                "amount": 3.14159,
                                "currency": "http://w3id.org/meta-share/meta-share/euro"
                            },
                            "membership_institution": [
                                "http://w3id.org/meta-share/meta-share/other",
                                "http://w3id.org/meta-share/meta-share/ELRA"
                            ],
                            "copyright_statement": {
                                "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                            },
                            "availability_start_date": "1967-08-13",
                            "availability_end_date": "1967-08-13",
                            "distribution_rights_holder": [
                                {
                                    "actor_type": "Person",
                                    "surname": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "given_name": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "personal_identifier": [
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/openid",
                                            "value": "String"
                                        },
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/researcherid",
                                            "value": "String"
                                        }
                                    ]
                                },
                                {
                                    "actor_type": "Person",
                                    "surname": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "given_name": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "personal_identifier": [
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                            "value": "String"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "dataset_distribution_form": "http://w3id.org/meta-share/meta-share/accessibleThroughInterface",
                            "download_location": "http://www.download.location.com",
                            "access_location": "http://www.accessLocation.com",
                            "is_accessed_by": [
                                {
                                    "resource_name": {
                                        "en": "acess tool for lexica"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                            "value": "String"
                                        },
                                        {
                                            "lr_identifier_scheme": "http://purl.org/spar/datacite/bibcode",
                                            "value": "String"
                                        }
                                    ],
                                    "version":  "15"
                                }
                            ],
                            "is_displayed_by": [
                                {
                                    "resource_name": {
                                        "en": "display tool for lexica"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                            "value": "String"
                                        },
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/islrn",
                                            "value": "String"
                                        }
                                    ],
                                    "version":  "43"
                                }
                            ],
                            "is_queried_by": [
                                {
                                    "resource_name": {
                                        "en": "query tool for lexica"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/maven",
                                            "value": "String"
                                        },
                                        {
                                            "lr_identifier_scheme": "http://purl.org/spar/datacite/bibcode",
                                            "value": "String"
                                        }
                                    ],
                                    "version":  "1"
                                }
                            ],
                            "samples_location": [
                                "ftp://samples.com"
                            ],
                            "distribution_image_feature": [
                                {
                                    "size": [
                                        {
                                            "amount": 85.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/byte"
                                        },
                                        {
                                            "amount": 150.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/semanticUnit1"
                                        }
                                    ],
                                    "data_format": [
                                        "http://w3id.org/meta-share/omtd-share/OpenOfficeDocument",
                                        "http://w3id.org/meta-share/omtd-share/CorpusFormat",
                                    ],
                                    "image_format": [
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/OpenOfficeDocument",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/YUV"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 15,
                                                    "size_height": 5,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/TwoD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/oggVorbis",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryHigh1"
                                        },
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/CorpusFormat",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/CMYK"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryLow1"
                                        }
                                    ]
                                },
                                {
                                    "size": [
                                        {
                                            "amount": 52.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/utterance1"
                                        },
                                        {
                                            "amount": 150.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/article"
                                        }
                                    ],
                                    "data_format": [
                                        "http://w3id.org/meta-share/omtd-share/Kaf",
                                        "http://w3id.org/meta-share/omtd-share/ConllFormat",
                                    ],
                                    "image_format": [
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/Kaf",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/CMYK"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/other",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryHigh1"
                                        },
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/ConllFormat",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/CMYK"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/other",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/vector",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryHigh1"
                                        }
                                    ]
                                }
                            ],
                            "licence_terms": [
                                {
                                    "licence_terms_name": {
                                        "en": "Creative Commons By 4.0"
                                    },
                                    "licence_terms_url": [
                                        "http://www.cc-by.com"
                                    ],
                                    "condition_of_use": [
                                        "http://w3id.org/meta-share/meta-share/unspecified"
                                    ],
                                    "licence_identifier": [
                                        {
                                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX",
                                            "value": "cc-by-4.0"
                                        }
                                    ]
                                },
                                {
                                    "licence_terms_name": {
                                        "en": "Creative Commons By 3.0"
                                    },
                                    "licence_terms_url": [
                                        "http://www.cc-by3.com"
                                    ],
                                    "condition_of_use": [
                                        "http://w3id.org/meta-share/meta-share/unspecified"
                                    ],
                                    "licence_identifier": [
                                        {
                                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX",
                                            "value": "cc-by-3.0"
                                        }
                                    ]
                                }
                            ],
                            "attribution_text": {
                                "en": "LCR x licensed under cc licence"
                            },
                            "cost": {
                                "amount": 53000.0,
                                "currency": "http://w3id.org/meta-share/meta-share/euro"
                            },
                            "membership_institution": [
                                "http://w3id.org/meta-share/meta-share/ELRA",
                                "http://w3id.org/meta-share/meta-share/LDC"
                            ],
                            "copyright_statement": {
                                "en": "copyrighted by noone"
                            },
                            "availability_start_date": "2010-08-13",
                            "availability_end_date": "2040-08-13",
                            "distribution_rights_holder": [
                                {
                                    "actor_type": "Organization",
                                    "organization_name": {
                                        "en": "Organization official title"
                                    },
                                    "website": [
                                        "http://www.exampleNew.com"
                                    ]
                                },
                                {
                                    "actor_type": "Person",
                                    "surname": {
                                        "en": "Smith"
                                    },
                                    "given_name": {
                                        "en": "Mary"
                                    },
                                    "personal_identifier": [
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                            "value": "0000-0000-0000-0001"
                                        }
                                    ]
                                },
                                {
                                    "actor_type": "Person",
                                    "surname": {
                                        "en": "Smith"
                                    },
                                    "given_name": {
                                        "en": "John"
                                    },
                                    "personal_identifier": [
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                            "value": "0000-0000-0000-0000"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "personal_data_included": "http://w3id.org/meta-share/meta-share/yesP",
                    "personal_data_details": {
                        "en": "identity, sex, name"
                    },
                    "sensitive_data_included": "http://w3id.org/meta-share/meta-share/yesS",
                    "sensitive_data_details": {
                        "en": "tax number"
                    },
                    "anonymized": "http://w3id.org/meta-share/meta-share/yesA",
                    "anonymization_details": {
                        "en": "method of anonymization"
                    },
                    "is_analysed_by": [
                        {
                            "resource_name": {
                                "en": "analysis tool 2"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                    "value": "String"
                                }
                            ],
                            "version":  "1"
                        },
                        {
                            "resource_name": {
                                "en": "analysis tool"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                    "value": "String"
                                }
                            ],
                            "version":  "1"
                        }
                    ],
                    "is_edited_by": [
                        {
                            "resource_name": {
                                "en": "editing tool"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/metaShare",
                                    "value": "String"
                                }
                            ],
                            "version":  "2.1.1"
                        },
                        {
                            "resource_name": {
                                "en": "editing tool2"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                    "value": "String"
                                }
                            ],
                            "version":  "2.2"
                        }
                    ],
                    "is_elicited_by": [
                        {
                            "resource_name": {
                                "en": "editing tool12"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/islrn",
                                    "value": "String"
                                }
                            ],
                            "version":  "13"
                        },
                        {
                            "resource_name": {
                                "en": "elicitation tool"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                    "value": "String"
                                }
                            ],
                            "version":  "1"
                        }
                    ],
                    "is_converted_version_of": [
                        {
                            "resource_name": {
                                "en": "raw corpus"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/isbn",
                                    "value": "String"
                                }
                            ],
                            "version":  "12"
                        },
                        {
                            "resource_name": {
                                "en": "converted corpus"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                    "value": "String"
                                },
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                    "value": "String"
                                }
                            ],
                            "version":  "0.0.8"
                        }
                    ],
                    "time_coverage": [
                        {
                            "en": "16th century"
                        }
                    ],
                    "geographic_coverage": [
                        {
                            "en": "all over the world"
                        }
                    ]
                }
            },
            "management_object": {}
        }
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        LOGGER.info(serializer.errors)
        instance_md = serializer.save()
        instance = instance_md.described_entity.lr_subclass

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['external_reference'] = [lr.pk for lr in
                                                   instance.external_reference.all()]
        cls.nested_models['lexical_conceptual_resource_media_part'] = [
            lexical_conceptual_resource_media_part.pk for
            lexical_conceptual_resource_media_part in
            instance.lexical_conceptual_resource_media_part.all()]
        cls.nested_models['dataset_distribution'] = [dd.pk for dd in
                                                     instance.dataset_distribution.all()]
        cls.nested_models['is_analysed_by'] = [lr.pk for lr in
                                               instance.is_analysed_by.all()]
        cls.nested_models['is_edited_by'] = [lr.pk for lr in
                                             instance.is_edited_by.all()]
        cls.nested_models['is_elicited_by'] = [lr.pk for lr in
                                               instance.is_elicited_by.all()]
        cls.nested_models['is_converted_version_of'] = [lr.pk for lr in
                                                        instance.is_converted_version_of.all()]
        instance.delete()
        json_file_unsp = open('test_fixtures/dummy_data.json', 'r')
        json_str_unsp = json_file_unsp.read()
        json_file_unsp.close()
        raw_data_unsp = json.loads(json_str_unsp)['unspecified_part']
        dist_unsp_feature = json.loads(json_str_unsp)['distribution_unspecified_feature']
        json_data['described_entity']['resource_name'] = {'en': 'other lcr'}
        json_data['described_entity']['resource_short_name'] = {'en': 'other ahort lcr'}
        json_data['described_entity']['version'] = 30
        json_data['described_entity']['lr_identifier'] = []
        json_data['described_entity']['lr_subclass']['unspecified_part'] = raw_data_unsp
        json_data['described_entity']['lr_subclass']['lexical_conceptual_resource_media_part'] = []
        json_data['described_entity']['lr_subclass']['dataset_distribution'][0]['distribution_unspecified_feature'] = dist_unsp_feature
        json_data['described_entity']['lr_subclass']['dataset_distribution'][0]['distribution_image_feature'] = []
        json_data['described_entity']['lr_subclass']['dataset_distribution'][1]['distribution_unspecified_feature'] = dist_unsp_feature
        json_data['described_entity']['lr_subclass']['dataset_distribution'][1]['distribution_image_feature'] = []
        serializer_unsp = serializers.MetadataRecordSerializer(data=json_data, context={'for_information_only': True})
        if serializer_unsp.is_valid():
            instance_unsp = serializer_unsp.save()
        else:
            LOGGER.info(serializer_unsp.errors)
        instance_unsp_lcr = instance_unsp.described_entity.lr_subclass
        cls.instance_unsp_pk = instance_unsp_lcr.pk
        cls.nested_models[
            'unspecified_part'] = instance_unsp_lcr.unspecified_part.pk
        instance_unsp_lcr.delete()


    def test_external_reference_are_not_deleted(self):
        try:
            for pk in self.nested_models['external_reference']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lexical_conceptual_resource_media_part_are_deleted(self):
        try:
            for pk in self.nested_models[
                'lexical_conceptual_resource_media_part']:
                obj = models.LexicalConceptualResourceMediaPart.objects.get(
                    id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_unspecified_part_is_deleted(self):
        try:
            obj = models.unspecifiedPart.objects.get(id=self.nested_models['unspecified_part'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_dataset_distribution_are_deleted(self):
        try:
            for pk in self.nested_models['dataset_distribution']:
                obj = models.DatasetDistribution.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_is_analysed_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_analysed_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_elicited_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_elicited_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_edited_by_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_edited_by']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_is_converted_version_of_are_not_deleted(self):
        try:
            for pk in self.nested_models['is_converted_version_of']:
                obj = models.LanguageResource.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lcr_are_deleted(self):
        try:
            obj = models.LexicalConceptualResource.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLexicalConceptualResourceTextPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['lcr_text_part']
        serializer = serializers.LexicalConceptualResourceTextPartSerializer(
            data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language_variety.pk for
                                             language_variety in
                                             instance.metalanguage.all()]
        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metalanguage_are_not_deleted(self):
        try:
            for pk in self.nested_models['metalanguage']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lcr_text_part_is_deleted(self):
        try:
            obj = models.LexicalConceptualResourceTextPart.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLexicalConceptualResourceAudioPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['lcr_audio_part']
        serializer = serializers.LexicalConceptualResourceAudioPartSerializer(
            data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language_variety.pk for
                                             language_variety in
                                             instance.metalanguage.all()]
        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metalanguage_are_not_deleted(self):
        try:
            for pk in self.nested_models['metalanguage']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lcr_audio_part_are_deleted(self):
        try:
            obj = models.LexicalConceptualResourceAudioPart.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLexicalConceptualResourceVideoPart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['lcr_video_part']
        serializer = serializers.LexicalConceptualResourceVideoPartSerializer(
            data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language.pk for
                                             language in
                                             instance.metalanguage.all()]
        cls.nested_models['dynamic_element'] = [dynamic_element.pk for
                                                dynamic_element in
                                                instance.dynamic_element.all()]

        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metalanguage_are_not_deleted(self):
        try:
            for pk in self.nested_models['metalanguage']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dynamic_element_are_deleted(self):
        try:
            for pk in self.nested_models['dynamic_element']:
                obj = models.DynamicElement.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_lcr_video_part_are_deleted(self):
        try:
            obj = models.LexicalConceptualResourceVideoPart.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityLexicalConceptualResourceImagePart(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['lcr_image_part']
        serializer = serializers.LexicalConceptualResourceImagePartSerializer(
            data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language.pk for
                                             language in
                                             instance.metalanguage.all()]
        cls.nested_models['static_element'] = [static_element.pk for
                                               static_element in
                                               instance.static_element.all()]

        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metalanguage_are_not_deleted(self):
        try:
            for pk in self.nested_models['metalanguage']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_static_element_are_deleted(self):
        try:
            for pk in self.nested_models['static_element']:
                obj = models.StaticElement.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_lcr_image_part_are_deleted(self):
        try:
            obj = models.LexicalConceptualResourceImagePart.objects.get(
                id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityGroup(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/group.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        instance = instance_md.described_entity

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['group_identifier'] = [pid.pk for pid in
                                                 instance.group_identifier.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['address_set'] = [address_set.pk for address_set in
                                            instance.address_set.all()]
        cls.nested_models['social_media_occupational_account'] = [sm_account.pk
                                                                  for sm_account
                                                                  in
                                                                  instance.social_media_occupational_account.all()]
        instance_md.delete()

    def test_group_identifier_is_deleted(self):
        try:
            for pk in self.nested_models['group_identifier']:
                obj = models.GroupIdentifier.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_domain_are_not_deleted(self):
        try:
            for pk in self.nested_models['domain']:
                obj = models.Domain.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_address_set_are_deleted(self):
        try:
            for pk in self.nested_models['address_set']:
                obj = models.AddressSet.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_social_media_occupational_account_are_deleted(self):
        try:
            for pk in self.nested_models['social_media_occupational_account']:
                obj = models.SocialMediaOccupationalAccount.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_group_is_deleted(self):
        try:
            obj = models.Group.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityAccessRightsStatement(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for VideoFormat
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['access_rights_statement']
        serializer = serializers.AccessRightsStatementSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'access_rights_statement_identifier'] = instance.access_rights_statement_identifier.pk
        instance.delete()

    def test_access_rights_statement_identifier_is_deleted(self):
        try:
            obj = models.AccessRightsStatementIdentifier.objects.get(
                id=self.nested_models['access_rights_statement_identifier'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_access_rights_statement_is_deleted(self):
        try:
            obj = models.AccessRightsStatement.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityProvenance(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for VideoFormat
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['provenance']
        serializer = serializers.provenanceSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['originating_repository'] = [repository.pk
                                                       for repository
                                                       in
                                                       instance.originating_repository.all()]
        instance.delete()

    def test_originating_repository_are_not_deleted(self):
        try:
            for pk in self.nested_models['originating_repository']:
                obj = models.Repository.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_provenance_is_deleted(self):
        try:
            obj = models.provenance.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityRepository(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for Group
        json_file = open('registry/tests/fixtures/repository.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']
        serializer = serializers.RepositorySerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['repository_identifier'] = [rid.pk for rid in
                                                      instance.repository_identifier.all()]
        cls.nested_models['repository_language'] = [language.pk for language in
                                                    instance.repository_language.all()]
        cls.nested_models[
            'repository_institution'] = instance.repository_institution.pk
        cls.nested_models['repository_scientific_leader'] = [leader.pk
                                                             for leader
                                                             in
                                                             instance.repository_scientific_leader.all()]
        cls.nested_models['repository_technical_responsible'] = [technical.pk
                                                                 for technical
                                                                 in
                                                                 instance.repository_technical_responsible.all()]
        cls.nested_models['provenance'] = [provenance.pk
                                           for provenance
                                           in
                                           instance.provenance.all()]
        instance.delete()

    def test_repository_identifier_is_deleted(self):
        try:
            for pk in self.nested_models['repository_identifier']:
                obj = models.RepositoryIdentifier.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_repository_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['repository_language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_repository_institution_are_not_deleted(self):
        try:
            obj = models.Organization.objects.get(
                id=self.nested_models['repository_institution'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_repository_scientific_leader_are_not_deleted(self):
        try:
            for pk in self.nested_models['repository_scientific_leader']:
                obj = models.Person.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_repository_technical_responsible_are_not_deleted(self):
        try:
            for pk in self.nested_models['repository_technical_responsible']:
                obj = models.Person.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_provenance_is_deleted(self):
        try:
            for pk in self.nested_models['provenance']:
                obj = models.provenance.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_repository_is_deleted(self):
        try:
            obj = models.Repository.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityParameter(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['parameter']
        serializer = serializers.ParameterSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['enumeration_value'] = [enum_val.pk
                                            for enum_val
                                            in
                                            instance.enumeration_value.all()]
        instance.delete()
    def test_enumeration_value_is_deleted(self):
        try:
            for pk in self.nested_models['enumeration_value']:
                obj = models.EnumerationValue.objects.get(id=pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_parameter_is_deleted(self):
        try:
            obj = models.Parameter.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityParameterFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['parameter']
        serializer = serializers.ParameterSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['enumeration_value'] = [enum_val.pk
                                            for enum_val
                                            in
                                            instance.enumeration_value.all()]

    def test_parameter_is_not_deleted_when_enumeration_value_is(self):
        for pk in self.nested_models['enumeration_value']:
            obj = models.EnumerationValue.objects.get(id=pk)
            obj.delete()
        try:
            initial_instance = models.Parameter.objects.get(pk=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityParameterFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['parameter']
        serializer = serializers.ParameterSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['enumeration_value'] = [enum_val.pk
                                            for enum_val
                                            in
                                            instance.enumeration_value.all()]

    def test_parameter_is_not_deleted_when_enumeration_value_is(self):
        for pk in self.nested_models['enumeration_value']:
            obj = models.EnumerationValue.objects.get(id=pk)
            obj.delete()
        try:
            initial_instance = models.Parameter.objects.get(id=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalitySize(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['size']
        serializer = serializers.SizeSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for language in
                                         instance.language.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['text_genre'] = [text_genre.pk
                                           for text_genre
                                           in instance.text_genre.all()]
        cls.nested_models['audio_genre'] = [audio_genre.pk
                                            for audio_genre
                                            in instance.audio_genre.all()]
        cls.nested_models['speech_genre'] = [speech_genre.pk
                                             for speech_genre
                                             in
                                             instance.speech_genre.all()]
        cls.nested_models['video_genre'] = [video_genre.pk
                                            for video_genre
                                            in
                                            instance.video_genre.all()]
        cls.nested_models['image_genre'] = [image_genre.pk
                                            for image_genre
                                            in
                                            instance.image_genre.all()]
        instance.delete()

    def test_language_are_not_deleted(self):
        try:
            for pk in self.nested_models['language']:
                obj = models.Language.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_domain_are_not_deleted(self):
        try:
            for pk in self.nested_models['domain']:
                obj = models.Domain.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_text_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['text_genre']:
                obj = models.TextGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_audio_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['audio_genre']:
                obj = models.AudioGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_speech_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['speech_genre']:
                obj = models.SpeechGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_video_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['video_genre']:
                obj = models.VideoGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_image_genre_are_not_deleted(self):
        try:
            for pk in self.nested_models['image_genre']:
                obj = models.ImageGenre.objects.get(id=pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_size_is_deleted(self):
        try:
            obj = models.Size.objects.get(id=self.instance_pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityRepositoryFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/repository.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']
        serializer = serializers.RepositorySerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['repository_identifier'] = [rid.pk for rid in
                                                      instance.repository_identifier.all()]
        cls.nested_models['repository_language'] = [language.pk for language in
                                                    instance.repository_language.all()]
        cls.nested_models['repository_institution'] = instance.repository_institution.pk
        cls.nested_models['repository_scientific_leader'] = [leader.pk
                                                             for leader
                                                             in instance.repository_scientific_leader.all()]
        cls.nested_models['repository_technical_responsible'] = [technical.pk
                                                                 for technical
                                                                 in
                                                                 instance.repository_technical_responsible.all()]
        cls.nested_models['provenance'] = [provenance.pk
                                           for provenance
                                           in
                                           instance.provenance.all()]

    def test_repository_is_not_deleted_when_repository_identifier_is(self):
        for pk in self.nested_models['repository_identifier']:
            obj = models.RepositoryIdentifier.objects.get(id=pk)
            obj.delete()
        try:
            initial_instance = models.Repository.objects.get(id=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_repository_is_not_deleted_when_repository_provenance_is(self):
        for pk in self.nested_models['provenance']:
            obj = models.provenance.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Repository.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityDistributionAudioFeatureFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_audio_feature_serializer']
        serializer = serializers.DistributionAudioFeatureSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]

        cls.nested_models['duration_of_audio'] = [duration.pk for duration in
                                                  instance.duration_of_audio.all()]
        cls.nested_models['duration_of_effective_speech'] = [duration.pk for
                                                             duration in
                                                             instance.duration_of_effective_speech.all()]
        cls.nested_models['audio_format'] = [audio_format.pk for audio_format in
                                             instance.audio_format.all()]

    def test_distribution_audio_feature_is_not_deleted_when_duration_of_audio_is(self):
        for pk in self.nested_models['duration_of_audio']:
            obj = models.Duration.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionAudioFeature.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_distribution_audio_feature_is_not_deleted_when_duration_of_effective_speech_is(self):
        for pk in self.nested_models['duration_of_effective_speech']:
            obj = models.Duration.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionAudioFeature.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_distribution_audio_feature_is_not_deleted_when_audio_format_is(self):
        for pk in self.nested_models['audio_format']:
            obj = models.AudioFormat.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionAudioFeature.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_distribution_audio_feature_is_not_deleted_when_size_is(self):
        for pk in self.nested_models['size']:
            obj = models.Size.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionAudioFeature.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityPersonFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/person.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']
        json_data['social_media_occupational_account'] = [{
            'social_media_occupational_account_type': 'http://w3id.org/meta-share/meta-share/facebook',
            'value': 'value',
        }]
        json_data['address_set'] = [{
            "address": {
                "en": "address"
            },
            "region": {
                "en": "region"
            },
            "zip_code": "14671",
            "city": {
                "en": "Athens"
            },
            "country": "GR"
        }]
        json_data['affiliation'] = [{
            "position": {
                "en": "position"
            },
            "affiliated_organization": {
                "organization_name": {
                    "en": "random_org"
                },
                "organization_identifier": [
                    {
                        "organization_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                        "value": "doi_value_id"
                    }
                ],
                "website": [
                    "http://www.orgwebsite.com"
                ]
            },
            "email": [
                "www.affiliation@email.com"
            ],
            "homepage": [],
            "address_set": [
                {
                    "address": {
                        "en": "address"
                    },
                    "region": {
                        "en": "region"
                    },
                    "zip_code": "14671",
                    "city": {
                        "en": "Athens"
                    },
                    "country": "GR"
                }
            ],
            "telephone_number": [],
            "fax_number": []
        }]
        serializer = serializers.PersonSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['personal_identifier'] = [pid.pk for pid in
                                                    instance.personal_identifier.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['member_of_association'] = [member_of_association.pk
                                                      for member_of_association
                                                      in
                                                      instance.member_of_association.all()]
        cls.nested_models['address_set'] = [address_set.pk for address_set in
                                            instance.address_set.all()]
        cls.nested_models['social_media_occupational_account'] = [sm_account.pk
                                                                  for sm_account
                                                                  in
                                                                  instance.social_media_occupational_account.all()]
        cls.nested_models['affiliation'] = [affiliation.pk for affiliation in
                                            instance.affiliation.all()]
        LOGGER.info(cls.nested_models)

    def test_person_is_not_deleted_when_social_media_occupational_account_is(self):
        for pk in self.nested_models['social_media_occupational_account']:
            obj = models.SocialMediaOccupationalAccount.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Person.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_person_is_not_deleted_when_personal_identifier_is(self):
        for pk in self.nested_models['personal_identifier']:
            obj = models.PersonalIdentifier.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Person.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_person_is_not_deleted_when_affiliation_is(self):
        for pk in self.nested_models['affiliation']:
            obj = models.Affiliation.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Person.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_person_is_not_deleted_when_address_set_is(self):
        for pk in self.nested_models['address_set']:
            obj = models.AddressSet.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Person.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityOrganizationFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']
        serializer = serializers.OrganizationSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['organization_identifier'] = [pid.pk for pid in
                                                        instance.organization_identifier.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['member_of_association'] = [member_of_association.pk
                                                      for member_of_association
                                                      in
                                                      instance.member_of_association.all()]
        cls.nested_models['other_office_address'] = [other_office_address.pk
                                                     for other_office_address
                                                     in
                                                     instance.other_office_address.all()]
        cls.nested_models['head_office_address'] = instance.head_office_address.pk
        cls.nested_models['social_media_occupational_account'] = [sm_account.pk
                                                                  for sm_account
                                                                  in
                                                                  instance.social_media_occupational_account.all()]
        cls.nested_models['is_division_of'] = instance.is_division_of.all().first().pk
        LOGGER.info(cls.nested_models)

    def test_organization_is_not_deleted_when_social_media_occupational_account_is(self):
        for pk in self.nested_models['social_media_occupational_account']:
            obj = models.SocialMediaOccupationalAccount.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Organization.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_organization_is_not_deleted_when_organization_identifier_is(self):
        for pk in self.nested_models['organization_identifier']:
            obj = models.OrganizationIdentifier.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Organization.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_organization_is_not_deleted_when_other_address_set_is(self):
        for pk in self.nested_models['other_office_address']:
            obj = models.AddressSet.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Organization.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_organization_is_not_deleted_when_head_address_set_is(self):
        obj = models.AddressSet.objects.get(id=self.nested_models['head_office_address'])
        obj.delete()
        try:

            initial_instance = models.Organization.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_organization_is_not_deleted_when_is_division_of_is(self):
        obj = models.Organization.objects.get(id=self.nested_models['is_division_of'])
        obj.delete()
        try:
            initial_instance = models.Organization.objects.get(id=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityGroupFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/group.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        instance = instance_md.described_entity

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['group_identifier'] = [pid.pk for pid in
                                                 instance.group_identifier.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['address_set'] = [address_set.pk for address_set in
                                            instance.address_set.all()]
        cls.nested_models['social_media_occupational_account'] = [sm_account.pk
                                                                  for sm_account
                                                                  in
                                                                  instance.social_media_occupational_account.all()]

    def test_group_is_not_deleted_when_social_media_occupational_account_is(self):
        for pk in self.nested_models['social_media_occupational_account']:
            obj = models.SocialMediaOccupationalAccount.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Group.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_group_is_not_deleted_when_group_identifier_is(self):
        for pk in self.nested_models['group_identifier']:
            obj = models.GroupIdentifier.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Group.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_group_is_not_deleted_when_address_set_is(self):
        for pk in self.nested_models['address_set']:
            obj = models.AddressSet.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Group.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityProjectFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)['described_entity']
        serializer = serializers.ProjectSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['project_identifier'] = [pid.pk for pid in
                                                   instance.project_identifier.all()]
        cls.nested_models['funder'] = [funder.pk for funder in
                                       instance.funder.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['subject'] = [subject.pk for subject in
                                        instance.subject.all()]
        cls.nested_models['social_media_occupational_account'] = [sm_account.pk
                                                                  for sm_account
                                                                  in
                                                                  instance.social_media_occupational_account.all()]
        cls.nested_models['cost'] = instance.cost.pk
        cls.nested_models[
            'ec_max_contribution'] = instance.ec_max_contribution.pk
        cls.nested_models[
            'national_max_contribution'] = instance.national_max_contribution.pk
        cls.nested_models['coordinator'] = instance.coordinator.pk
        cls.nested_models['participating_organization'] = [org.pk for org in
                                                           instance.participating_organization.all()]
        cls.nested_models['is_related_to_document'] = [doc.pk for doc in
                                                       instance.is_related_to_document.all()]
        LOGGER.info(cls.nested_models)

    def test_project_is_not_deleted_when_social_media_occupational_account_is(self):
        for pk in self.nested_models['social_media_occupational_account']:
            obj = models.SocialMediaOccupationalAccount.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Project.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_project_is_not_deleted_when_project_identifier_is(self):
        for pk in self.nested_models['project_identifier']:
            obj = models.ProjectIdentifier.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Project.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLanguageResourceFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file_crp = open('registry/tests/fixtures/corpus.json', 'r')
        json_str_crp = json_file_crp.read()
        json_file_crp.close()
        json_data_crp = json.loads(json_str_crp)
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data_lr = json_data['described_entity']
        json_data_lr['has_metadata'] = [json_data_crp]
        serializer = serializers.LanguageResourceSerializer(data=json_data_lr)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        # instance = instance_md.described_entity

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['lr_identifier'] = [pid.pk for pid in
                                              instance.lr_identifier.all()]
        cls.nested_models['additional_info'] = [additional_info.pk for
                                                additional_info in
                                                instance.additional_info.all()]
        cls.nested_models['contact'] = [contact.pk for contact in
                                        instance.contact.all()]
        cls.nested_models['ipr_holder'] = [ipr_holder.pk for ipr_holder in
                                           instance.ipr_holder.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['subject'] = [subject.pk for subject in
                                        instance.subject.all()]
        cls.nested_models['resource_provider'] = [resource_provider.pk
                                                  for resource_provider
                                                  in
                                                  instance.resource_provider.all()]
        cls.nested_models['resource_creator'] = [resource_creator.pk for
                                                 resource_creator in
                                                 instance.resource_creator.all()]
        cls.nested_models['funding_project'] = [funding_project.pk for
                                                funding_project in
                                                instance.funding_project.all()]
        cls.nested_models['actual_use'] = [actual_use.pk for actual_use in
                                           instance.actual_use.all()]
        cls.nested_models['validation'] = [validation.pk for validation in
                                           instance.validation.all()]
        cls.nested_models['is_documented_by'] = [is_documented_by.pk for
                                                 is_documented_by in
                                                 instance.is_documented_by.all()]
        cls.nested_models['is_described_by'] = [is_described_by.pk for
                                                is_described_by in
                                                instance.is_described_by.all()]
        cls.nested_models['is_cited_by'] = [is_cited_by.pk for is_cited_by in
                                            instance.is_cited_by.all()]
        cls.nested_models['is_reviewed_by'] = [is_reviewed_by.pk for
                                               is_reviewed_by in
                                               instance.is_reviewed_by.all()]
        cls.nested_models['is_part_of'] = [is_part_of.pk for is_part_of in
                                           instance.is_part_of.all()]
        cls.nested_models['is_part_with'] = [is_part_with.pk for is_part_with in
                                             instance.is_part_with.all()]
        cls.nested_models['is_similar_to'] = [is_similar_to.pk for is_similar_to
                                              in
                                              instance.is_similar_to.all()]
        cls.nested_models['is_exact_match_with'] = [is_exact_match_with.pk for
                                                    is_exact_match_with in
                                                    instance.is_exact_match_with.all()]
        cls.nested_models['has_metadata'] = [has_metadata.pk for has_metadata in
                                             instance.has_metadata.all()]
        cls.nested_models['is_archived_by'] = [is_archived_by.pk for
                                               is_archived_by in
                                               instance.is_archived_by.all()]
        cls.nested_models['is_continuation_of'] = [is_continuation_of.pk for
                                                   is_continuation_of in
                                                   instance.is_continuation_of.all()]
        cls.nested_models['replaces'] = [replaces.pk for replaces in
                                         instance.replaces.all()]
        cls.nested_models['is_version_of'] = instance.is_version_of.pk
        cls.nested_models['relation'] = [relation.pk for relation in
                                         instance.relation.all()]
        cls.nested_models['has_original_source'] = [has_original_source.pk for
                                                    has_original_source in
                                                    instance.has_original_source.all()]
        cls.nested_models['is_created_by'] = [is_created_by.pk for
                                              is_created_by in
                                              instance.is_created_by.all()]
        cls.nested_models['lr_subclass'] = instance.lr_subclass.pk
        LOGGER.info(cls.nested_models)

    def test_lr_is_not_deleted_when_relation_is(self):
        for pk in self.nested_models['relation']:
            obj = models.Relation.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageResource.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lr_is_not_deleted_when_actual_use_is(self):
        for pk in self.nested_models['actual_use']:
            obj = models.ActualUse.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageResource.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lr_is_not_deleted_when_validation_is(self):
        for pk in self.nested_models['validation']:
            obj = models.Validation.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageResource.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lr_is_not_deleted_when_lr_identifier_is(self):
        for pk in self.nested_models['lr_identifier']:
            obj = models.LRIdentifier.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageResource.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lr_is_not_deleted_when_mdr_is(self):
        for pk in self.nested_models['has_metadata']:
            obj = models.GenericMetadataRecord.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageResource.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityCorpusFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        serializer = serializers.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['corpus_media_part'] = [corpus_media_part.pk for
                                                  corpus_media_part in
                                                  instance.corpus_media_part.all()]
        cls.nested_models['dataset_distribution'] = [dd.pk for dd in
                                                     instance.dataset_distribution.all()]
        cls.nested_models['is_analysed_by'] = [lr.pk for lr in
                                               instance.is_analysed_by.all()]
        cls.nested_models['is_edited_by'] = [lr.pk for lr in
                                             instance.is_edited_by.all()]
        cls.nested_models['is_elicited_by'] = [lr.pk for lr in
                                               instance.is_elicited_by.all()]
        cls.nested_models[
            'is_annotated_version_of'] = instance.is_annotated_version_of.pk
        cls.nested_models[
            'is_aligned_version_of'] = instance.is_aligned_version_of.pk
        cls.nested_models[
            'is_converted_version_of'] = [lr.pk for lr in
                                          instance.is_converted_version_of.all()]
        json_file_unsp = open('test_fixtures/dummy_data.json', 'r')
        json_str_unsp = json_file_unsp.read()
        json_file_unsp.close()
        raw_data_unsp = json.loads(json_str_unsp)['unspecified_part']
        dist_unsp_feature = json.loads(json_str_unsp)['distribution_unspecified_feature']
        raw_data['unspecified_part'] = raw_data_unsp
        raw_data['corpus_media_part'] = []
        raw_data['dataset_distribution'][0]['distribution_unspecified_feature'] = dist_unsp_feature
        raw_data['dataset_distribution'][0]['distribution_text_feature'] = []
        raw_data['dataset_distribution'][1]['distribution_unspecified_feature'] = dist_unsp_feature
        raw_data['dataset_distribution'][1]['distribution_text_feature'] = []
        serializer_unsp = serializers.CorpusSerializer(data=raw_data, context={'for_information_only': True})
        if serializer_unsp.is_valid():
            instance_unsp = serializer_unsp.save()
        else:
            LOGGER.info(serializer_unsp.errors)
        cls.instance_unsp_pk = instance_unsp.pk
        cls.nested_models[
            'unspecified_part'] = instance_unsp.unspecified_part.pk

    def test_corpus_is_not_deleted_when_corpus_media_part_is(self):
        for pk in self.nested_models['corpus_media_part']:
            obj = models.CorpusMediaPart.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Corpus.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_is_not_deleted_when_unspecified_part_is(self):
        obj = models.unspecifiedPart.objects.get(id=self.nested_models['unspecified_part'])
        obj.delete()
        try:

            initial_instance = models.Corpus.objects.get(id=self.instance_unsp_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_is_not_deleted_when_dataset_distribution_is(self):
        for pk in self.nested_models['dataset_distribution']:
            obj = models.DatasetDistribution.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Corpus.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityCorpusTextPartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['text_corpus_part']
        serializer = serializers.CorpusTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['text_type'] = [text_type.pk for
                                          text_type in
                                          instance.text_type.all()]
        cls.nested_models['text_genre'] = [text_genre.pk for
                                           text_genre in
                                           instance.text_genre.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

    def test_corpus_text_part_is_not_deleted_when_annotation_is(self):
        for pk in self.nested_models['annotation']:
            obj = models.Annotation.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusTextPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_text_part_is_not_deleted_when_link_to_other_media_is(self):
        for pk in self.nested_models['link_to_other_media']:
            obj = models.LinkToOtherMedia.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusTextPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityCorpusAudioPartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus_audio_part']
        serializer = serializers.CorpusAudioPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['speech_genre'] = [speech_genre.pk for
                                             speech_genre in
                                             instance.speech_genre.all()]
        cls.nested_models['audio_genre'] = [audio_genre.pk for
                                            audio_genre in
                                            instance.audio_genre.all()]
        cls.nested_models['recorder'] = [actor.pk for
                                         actor in
                                         instance.recorder.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

    def test_corpus_audio_part_is_not_deleted_when_annotation_is(self):
        for pk in self.nested_models['annotation']:
            obj = models.Annotation.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusAudioPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_audio_part_is_not_deleted_when_link_to_other_media_is(self):
        for pk in self.nested_models['link_to_other_media']:
            obj = models.LinkToOtherMedia.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusAudioPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityCorpusVideoPartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus_video_part']
        serializer = serializers.CorpusVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['video_genre'] = [video_genre.pk for
                                            video_genre in
                                            instance.video_genre.all()]
        cls.nested_models['dynamic_element'] = [dynamic_element.pk for
                                                dynamic_element in
                                                instance.dynamic_element.all()]
        cls.nested_models['recorder'] = [actor.pk for
                                         actor in
                                         instance.recorder.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

    def test_corpus_video_part_is_not_deleted_when_annotation_is(self):
        for pk in self.nested_models['annotation']:
            obj = models.Annotation.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusVideoPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_video_part_is_not_deleted_when_dynamic_element_is(self):
        for pk in self.nested_models['dynamic_element']:
            obj = models.DynamicElement.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusVideoPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_video_part_is_not_deleted_when_link_to_other_media_is(self):
        for pk in self.nested_models['link_to_other_media']:
            obj = models.LinkToOtherMedia.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusVideoPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityCorpusImagePartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus_image_part']
        serializer = serializers.CorpusImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['image_genre'] = [image_genre.pk for
                                            image_genre in
                                            instance.image_genre.all()]
        cls.nested_models['static_element'] = [static_element.pk for
                                               static_element in
                                               instance.static_element.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

    def test_corpus_image_part_is_not_deleted_when_annotation_is(self):
        for pk in self.nested_models['annotation']:
            obj = models.Annotation.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusImagePart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_image_part_is_not_deleted_when_static_element_is(self):
        for pk in self.nested_models['static_element']:
            obj = models.StaticElement.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusImagePart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_image_part_is_not_deleted_when_link_to_other_media_is(self):
        for pk in self.nested_models['link_to_other_media']:
            obj = models.LinkToOtherMedia.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusImagePart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityCorpusTextNumericalPartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus_text_numerical_part']
        serializer = serializers.CorpusTextNumericalPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['recorder'] = [actor.pk for
                                         actor in
                                         instance.recorder.all()]
        cls.nested_models['annotation'] = [annotation.pk for
                                           annotation in
                                           instance.annotation.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

    def test_corpus_text_numerical_part_is_not_deleted_when_annotation_is(self):
        for pk in self.nested_models['annotation']:
            obj = models.Annotation.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusTextNumericalPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_corpus_text__numerical_part_is_not_deleted_when_link_to_other_media_is(self):
        for pk in self.nested_models['link_to_other_media']:
            obj = models.LinkToOtherMedia.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.CorpusTextNumericalPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityToolServiceFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']['lr_subclass']
        serializer = serializers.ToolServiceSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['software_distribution'] = [sd.pk for sd in
                                                      instance.software_distribution.all()]
        cls.nested_models['input_content_resource'] = [input_cr.pk for
                                                       input_cr in
                                                       instance.input_content_resource.all()]
        cls.nested_models['output_resource'] = [output.pk for output in
                                                instance.output_resource.all()]
        cls.nested_models['typesystem'] = instance.typesystem.pk
        cls.nested_models['annotation_schema'] = [anno_schema.pk for anno_schema in
            instance.annotation_schema.all()]
        cls.nested_models['annotation_resource'] = [lr.pk for lr in
                                                    instance.annotation_resource.all()]
        cls.nested_models['ml_model'] = [lr.pk
                                         for lr
                                         in
                                         instance.ml_model.all()]
        cls.nested_models['evaluation'] = [evaluation.pk for
                                           evaluation in
                                           instance.evaluation.all()]
        cls.nested_models['parameter'] = [parameter.pk for parameter in
                                          instance.parameter.all()]

    def test_tool_service_is_not_deleted_when_evaluation_is(self):
        for pk in self.nested_models['evaluation']:
            obj = models.Evaluation.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.ToolService.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_tool_service_is_not_deleted_when_input_content_resource_is(self):
        for pk in self.nested_models['input_content_resource']:
            obj = models.ProcessingResource.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.ToolService.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_tool_service_is_not_deleted_when_output_resource_is(self):
        for pk in self.nested_models['output_resource']:
            obj = models.ProcessingResource.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.ToolService.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_tool_service_is_not_deleted_when_parameter_is(self):
        for pk in self.nested_models['parameter']:
            obj = models.Parameter.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.ToolService.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_tool_service_is_not_deleted_when_software_distribution_is(self):
        for pk in self.nested_models['software_distribution']:
            obj = models.SoftwareDistribution.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.ToolService.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLexicalConceptualResourceVideoPartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['lcr_video_part']
        serializer = serializers.LexicalConceptualResourceVideoPartSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language.pk for
                                             language in
                                             instance.metalanguage.all()]
        cls.nested_models['dynamic_element'] = [dynamic_element.pk for
                                                dynamic_element in
                                                instance.dynamic_element.all()]

    def test_lcr_video_part_is_not_deleted_when_dynamic_element_is(self):
        for pk in self.nested_models['dynamic_element']:
            obj = models.DynamicElement.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LexicalConceptualResourceVideoPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLanguageDescriptionVideoPartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['ld_video_part']
        serializer = serializers.LanguageDescriptionVideoPartSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language.pk for
                                             language in
                                             instance.metalanguage.all()]
        cls.nested_models['dynamic_element'] = [dynamic_element.pk for
                                                dynamic_element in
                                                instance.dynamic_element.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

    def test_ld_video_part_is_not_deleted_when_dynamic_element_is(self):
        for pk in self.nested_models['dynamic_element']:
            obj = models.DynamicElement.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageDescriptionVideoPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_ld_video_part_is_not_deleted_when_link_to_other_media_is(self):
        for pk in self.nested_models['link_to_other_media']:
            obj = models.LinkToOtherMedia.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageDescriptionVideoPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLexicalConceptualResourceImagePartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['lcr_image_part']
        serializer = serializers.LexicalConceptualResourceImagePartSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language.pk for
                                             language in
                                             instance.metalanguage.all()]
        cls.nested_models['static_element'] = [static_element.pk for
                                               static_element in
                                               instance.static_element.all()]

    def test_lcr_image_part_is_not_deleted_when_static_element_is(self):
        for pk in self.nested_models['static_element']:
            obj = models.StaticElement.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LexicalConceptualResourceImagePart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLanguageDescriptionImagePartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['ld_image_part']
        serializer = serializers.LanguageDescriptionImagePartSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language.pk for
                                             language in
                                             instance.metalanguage.all()]
        cls.nested_models['static_element'] = [static_element.pk for
                                               static_element in
                                               instance.static_element.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

    def test_ld_image_part_is_not_deleted_when_static_element_is(self):
        for pk in self.nested_models['static_element']:
            obj = models.StaticElement.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageDescriptionImagePart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_ld_image_part_is_not_deleted_when_link_to_other_media_is(self):
        for pk in self.nested_models['link_to_other_media']:
            obj = models.LinkToOtherMedia.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageDescriptionImagePart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityEvaluationFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['evaluation']
        serializer = serializers.EvaluationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['performance_indicator'] = [performance.pk for
                                                      performance in
                                                      instance.performance_indicator.all()]
        cls.nested_models['evaluation_report'] = [doc.pk for doc in
                                                  instance.evaluation_report.all()]
        cls.nested_models['is_evaluated_by'] = [lr.pk for lr in
                                                instance.is_evaluated_by.all()]
        cls.nested_models['evaluator'] = [actor.pk for actor in
                                          instance.evaluator.all()]

    def test_evaluation_is_not_deleted_when_performance_indicator_is(self):
        for pk in self.nested_models['performance_indicator']:
            obj = models.PerformanceIndicator.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Evaluation.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLanguageDescriptionTextPartFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['ld_text_part']
        serializer = serializers.LanguageDescriptionTextPartSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for
                                         language in
                                         instance.language.all()]
        cls.nested_models['metalanguage'] = [language_variety.pk for
                                             language_variety in
                                             instance.metalanguage.all()]
        cls.nested_models['is_created_by'] = [lr.pk for
                                              lr in
                                              instance.is_created_by.all()]
        cls.nested_models['has_original_source'] = [lr.pk for
                                                    lr in
                                                    instance.has_original_source.all()]
        cls.nested_models['link_to_other_media'] = [link_to_other_media.pk for
                                                    link_to_other_media in
                                                    instance.link_to_other_media.all()]

    def test_ld_text_part_is_not_deleted_when_link_to_other_media_is(self):
        for pk in self.nested_models['link_to_other_media']:
            obj = models.LinkToOtherMedia.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageDescriptionTextPart.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityDistributionVideoFeatureFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_video_feature']
        serializer = serializers.DistributionVideoFeatureSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]
        cls.nested_models['video_format'] = [video_format.pk for video_format in
                                             instance.video_format.all()]

    def test_distribution_video_feature_is_not_deleted_when_video_format_is(self):
        for pk in self.nested_models['video_format']:
            obj = models.VideoFormat.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionVideoFeature.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_distribution_video_feature_is_not_deleted_when_size_is(self):
        for pk in self.nested_models['size']:
            obj = models.Size.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionVideoFeature.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityDistributionImageFeatureFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_image_feature']
        serializer = serializers.DistributionImageFeatureSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]
        cls.nested_models['image_format'] = [image_format.pk for image_format in
                                             instance.image_format.all()]

    def test_distribution_image_feature_is_not_deleted_when_image_format_is(self):
        for pk in self.nested_models['image_format']:
            obj = models.ImageFormat.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionImageFeature.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_distribution_image_feature_is_not_deleted_when_size_is(self):
        for pk in self.nested_models['size']:
            obj = models.Size.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionImageFeature.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalitySizeFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['size']
        serializer = serializers.SizeSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['language'] = [language.pk for language in
                                         instance.language.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['text_genre'] = [text_genre.pk
                                           for text_genre
                                           in instance.text_genre.all()]
        cls.nested_models['audio_genre'] = [audio_genre.pk
                                            for audio_genre
                                            in instance.audio_genre.all()]
        cls.nested_models['speech_genre'] = [speech_genre.pk
                                             for speech_genre
                                             in
                                             instance.speech_genre.all()]
        cls.nested_models['video_genre'] = [video_genre.pk
                                            for video_genre
                                            in
                                            instance.video_genre.all()]
        cls.nested_models['image_genre'] = [image_genre.pk
                                            for image_genre
                                            in
                                            instance.image_genre.all()]

    def test_size_is_not_deleted_when_domain_is(self):
        for pk in self.nested_models['domain']:
            obj = models.Domain.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Size.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_size_is_not_deleted_when_text_genre_is(self):
        for pk in self.nested_models['text_genre']:
            obj = models.TextGenre.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Size.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_size_is_not_deleted_when_speech_genre_is(self):
        for pk in self.nested_models['speech_genre']:
            obj = models.SpeechGenre.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Size.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_size_is_not_deleted_when_audio_genre_is(self):
        for pk in self.nested_models['audio_genre']:
            obj = models.AudioGenre.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Size.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_size_is_not_deleted_when_image_genre_is(self):
        for pk in self.nested_models['image_genre']:
            obj = models.ImageGenre.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Size.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_size_is_not_deleted_when_video_genre_is(self):
        for pk in self.nested_models['video_genre']:
            obj = models.VideoGenre.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Size.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_size_is_not_deleted_when_language_is(self):
        for pk in self.nested_models['language']:
            obj = models.Language.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Size.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLicenceTermsFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)['described_entity']
        serializer = serializers.LicenceTermsSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['licence_identifier'] = [pid.pk for pid in
                                                   instance.licence_identifier.all()]

    def test_licence_terms_are_not_deleted_when_licence_identifier_is(self):
        for pk in self.nested_models['licence_identifier']:
            obj = models.LicenceIdentifier.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LicenceTerms.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityDocumentFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/document.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        serializer.is_valid()
        instance = instance_md.described_entity

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['document_identifier'] = [pid.pk for pid in
                                                    instance.document_identifier.all()]
        cls.nested_models['author'] = [author.pk for author in
                                       instance.author.all()]
        cls.nested_models['editor'] = [editor.pk for editor in
                                       instance.editor.all()]
        cls.nested_models['contributor'] = [contributor.pk for contributor in
                                            instance.contributor.all()]
        cls.nested_models['domain'] = [domain.pk for domain in
                                       instance.domain.all()]
        cls.nested_models['subject'] = [subject.pk for subject in
                                        instance.subject.all()]
        LOGGER.info(cls.nested_models)

    def test_document_is_not_deleted_when_document_identifier_is(self):
        for pk in self.nested_models['document_identifier']:
            obj = models.DocumentIdentifier.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Document.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityAffiliationFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['affiliation']
        serializer = serializers.AffiliationSerializer(data=cls.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'affiliated_organization'] = instance.affiliated_organization.pk
        cls.nested_models['address_set'] = [address_set.pk for address_set in
                                            instance.address_set.all()]

    def test_affiliation_is_not_deleted_when_address_set_is(self):
        for pk in self.nested_models['address_set']:
            obj = models.AddressSet.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.Affiliation.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLexicalConceptualResourceFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_data = {
            "metadata_record_identifier": {
                "metadata_record_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                "value": "ELG-MDR-LRS-260320-00000046"
            },
            "metadata_creation_date": "2020-03-26",
            "metadata_last_date_updated": "2020-04-12",
            "metadata_curator": [
                {
                    "actor_type": "Person",
                    "surname": {
                        "en": "Smith"
                    },
                    "given_name": {
                        "en": "John"
                    },
                    "personal_identifier": [
                        {
                            "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                            "value": "0000-0000-0000-0000"
                        }
                    ]
                }
            ],
            "complies_with": "http://w3id.org/meta-share/meta-share/ELG-SHARE",
            "metadata_creator": {
                "actor_type": "Person",
                "surname": {
                    "en": "Smith"
                },
                "given_name": {
                    "en": "John"
                },
                "personal_identifier": [
                    {
                        "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                        "value": "0000-0000-0000-0000"
                    }
                ]
            },
            "source_of_metadata_record": {
                "repository_name": {
                    "en": "Repo name"
                },
                "repository_url": "http://www.repo.com",
                "repository_identifier": [
                    {
                        "repository_identifier_scheme": "http://purl.org/spar/datacite/handle",
                        "value": "repository test value"
                    }
                ]
            },
            "source_metadata_record": None,
            "revision": None,
            "described_entity": {
                "entity_type": "LanguageResource",
                "physical_resource": [
                    "placeholder for data resources",
                    "text"
                ],
                "resource_name": {
                    "en": "full title of a tool/service"
                },
                "resource_short_name": {
                    "en": "short name"
                },
                "description": {
                    "en": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue. Lobortis scelerisque fermentum dui faucibus in ornare quam viverra. Tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero. Ullamcorper velit sed ullamcorper morbi tincidunt ornare. Diam sollicitudin tempor id eu nisl nunc mi. Risus in hendrerit gravida rutrum quisque. Fusce ut placerat orci nulla pellentesque. Id neque aliquam vestibulum morbi blandit cursus risus at ultrices. Id venenatis a condimentum vitae. Malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi. Imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada. Auctor neque vitae tempus quam. Hac habitasse platea dictumst quisque sagittis purus sit amet volutpat. Aliquam id diam maecenas ultricies mi eget mauris pharetra. Tempor orci eu lobortis elementum nibh tellus. Faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis."
                },
                "lr_identifier": [
                    {
                        "lr_identifier_scheme": "http://purl.org/spar/datacite/handle",
                        "value": "http://hdl.grnet.gr/11500/ATHENA-0000-0000-23F2-7"
                    }
                ],
                "logo": "https://cdn.vox-cdn.com/thumbor/uploads/chorus_image/image/65613211/microsoftedgenewlogo.5.jpg",
                "version":  "1.0.0",
                "version_date": "1967-08-13",
                "update_frequency": {
                    "en": "rarely"
                },
                "revision": {
                    "en": "some revision text"
                },
                "additional_info": [
                    {
                        "landing_page": "http://www.exampleTool.com"
                    }
                ],
                "contact": [
                    {
                        "actor_type": "Person",
                        "surname": {
                            "en": "Smith"
                        },
                        "given_name": {
                            "en": "John"
                        },
                        "personal_identifier": [
                            {
                                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                "value": "0000-0000-0000-0000"
                            }
                        ]
                    }
                ],
                "mailing_list_name": [
                    "name"
                ],
                "discussion_url": [
                    "http://www.discussionForum.com"
                ],
                "citation_text": {
                    "en": "citation text"
                },
                "ipr_holder": [
                    {
                        "actor_type": "Person",
                        "surname": {
                            "en": "Smith"
                        },
                        "given_name": {
                            "en": "John"
                        },
                        "personal_identifier": [
                            {
                                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                "value": "0000-0000-0000-0000"
                            }
                        ]
                    }
                ],
                "keyword": [
                    {
                        "en": "keyword1"
                    },
                    {
                        "en": "keyword2"
                    }
                ],
                "domain": [
                    {
                        "category_label": {
                            "en": "domain"
                        },
                        "domain_identifier": {
                            "domain_classification_scheme": "http://w3id.org/meta-share/meta-share/ELG_domainClassification",
                            "value": "xxx"
                        }
                    },
                    {
                        "category_label": {
                            "en": "domain2"
                        },
                        "domain_identifier": {
                            "domain_classification_scheme": "http://w3id.org/meta-share/meta-share/ANC_domainClassification",
                            "value": "String"
                        }
                    }
                ],
                "subject": [
                    {
                        "category_label": {
                            "en": "subject1"
                        },
                        "subject_identifier": {
                            "subject_classification_scheme": "http://w3id.org/meta-share/meta-share/PAROLE_topicClassification",
                            "value": "String"
                        }
                    },
                    {
                        "category_label": {
                            "en": "subject2"
                        },
                        "subject_identifier": {
                            "subject_classification_scheme": "http://w3id.org/meta-share/meta-share/PAROLE_topicClassification",
                            "value": "String"
                        }
                    }
                ],
                "resource_provider": [
                    {
                        "actor_type": "Person",
                        "surname": {
                            "en": "Smith"
                        },
                        "given_name": {
                            "en": "John"
                        },
                        "personal_identifier": [
                            {
                                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                "value": "0000-0000-0000-0000"
                            }
                        ]
                    },
                    {
                        "actor_type": "Organization",
                        "organization_name": {
                            "en": "Distribution holder"
                        },
                        "organization_identifier": [
                            {
                                "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "DOI"
                            }
                        ],
                        "website": [
                            "http://www.organization.com"
                        ]
                    }
                ],
                "publication_date": "1967-08-13",
                "resource_creator": [
                    {
                        "actor_type": "Person",
                        "surname": {
                            "en": "Smith"
                        },
                        "given_name": {
                            "en": "John"
                        },
                        "personal_identifier": [
                            {
                                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                "value": "0000-0000-0000-0000"
                            }
                        ]
                    },
                    {
                        "actor_type": "Organization",
                        "organization_name": {
                            "en": "Distribution holder"
                        },
                        "organization_identifier": [
                            {
                                "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "DOI"
                            }
                        ],
                        "website": [
                            "http://www.organization.com"
                        ]
                    }
                ],
                "creation_start_date": "1967-08-13",
                "creation_end_date": "1969-08-13",
                "creation_mode": "http://w3id.org/meta-share/meta-share/interactive",
                "creation_details": {
                    "en": "Lorem ipsum"
                },
                "has_original_source": [
                    {
                        "resource_name": {
                            "en": "another lexicon"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/islrn",
                                "value": "pid2"
                            },
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/purl",
                                "value": "pid1"
                            }
                        ],
                        "version":  "unspecified"
                    },
                    {
                        "resource_name": {
                            "en": "new lexicon"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/metaShare",
                                "value": "noIid"
                            },
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/url",
                                "value": "urlid"
                            }
                        ],
                        "version":  "13"
                    }
                ],
                "original_source_description": {
                    "en": "a lexicon of 12 words"
                },
                "is_created_by": [
                    {
                        "resource_name": {
                            "en": "creation tool"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/isbn",
                                "value": "isbn13"
                            },
                            {
                                "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                "value": "docker3"
                            }
                        ],
                        "version":  "44"
                    },
                    {
                        "resource_name": {
                            "en": "another creation tool"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/purl",
                                "value": "purlid"
                            },
                            {
                                "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/maven",
                                "value": "mavenid"
                            }
                        ],
                        "version":  "4.0.7"
                    }
                ],
                "funding_project": [
                    {
                        "project_name": {
                            "en": "project 2"
                        },
                        "project_identifier": [
                            {
                                "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                "value": "String"
                            }
                        ],
                        "website": [
                            "http://www.project2.com"
                        ]
                    },
                    {
                        "project_name": {
                            "en": "full title of a project"
                        },
                        "website": [
                            "http://www.project.com"
                        ]
                    }
                ],
                "intended_application": [
                    "http://w3id.org/meta-share/omtd-share/NegationDetection",
                    "http://w3id.org/meta-share/omtd-share/AnnotationOfCompoundingFeatures"
                ],
                "actual_use": [
                    {
                        "used_in_application": [
                            "http://w3id.org/meta-share/omtd-share/AuthorClassification",
                            "http://w3id.org/meta-share/omtd-share/QuestionAnswering",
                            "Free text used in Application"
                        ],
                        "has_outcome": [
                            {
                                "resource_name": {
                                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                },
                                "version":  "1.0.0 (automatically assigned)"
                            },
                            {
                                "resource_name": {
                                    "en": "name of a resource"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/metaShare",
                                        "value": "String"
                                    }
                                ],
                                "version":  "1.0.0"
                            },
                            {
                                "resource_name": {
                                    "en": "resource2"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtd",
                                        "value": "String"
                                    }
                                ],
                                "version":  "1.0.3"
                            }
                        ],
                        "usage_project": [
                            {
                                "project_name": {
                                    "en": "project 2"
                                },
                                "project_identifier": [
                                    {
                                        "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                        "value": "String"
                                    }
                                ],
                                "website": [
                                    "http://www.project2.com"
                                ]
                            }
                        ],
                        "usage_report": [
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            },
                            {
                                "title": {
                                    "en": "report2"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/pmcid",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            }
                        ],
                        "actual_use_details": {
                            "en": "details on use of a tool"
                        }
                    },
                    {
                        "used_in_application": [
                            "http://w3id.org/meta-share/omtd-share/EvaluationOfBroadCoverageNaturalLanguageParsers",
                            "http://w3id.org/meta-share/omtd-share/DeepParserPerformanceEvaluation",
                            "Free text LT Area"
                        ],
                        "has_outcome": [
                            {
                                "resource_name": {
                                    "en": "resource4"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/islrn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "13"
                            },
                            {
                                "resource_name": {
                                    "en": "resource5"
                                },
                                "version":  "1"
                            }
                        ],
                        "usage_project": [
                            {
                                "project_name": {
                                    "en": "project 2"
                                },
                                "project_identifier": [
                                    {
                                        "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                        "value": "String"
                                    }
                                ],
                                "website": [
                                    "http://www.project2.com"
                                ]
                            }
                        ],
                        "usage_report": [
                            {
                                "title": {
                                    "en": "report2"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/pmcid",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            },
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            }
                        ],
                        "actual_use_details": {
                            "en": "details on use of a tool"
                        }
                    }
                ],
                "validated": True,
                "validation": [
                    {
                        "validation_type": "http://w3id.org/meta-share/meta-share/formal",
                        "validation_mode": "http://w3id.org/meta-share/meta-share/interactive",
                        "validation_details": {
                            "en": "detailsof validation"
                        },
                        "validation_extent": "http://w3id.org/meta-share/meta-share/full",
                        "is_validated_by": [
                            {
                                "resource_name": {
                                    "en": "validation tool"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "12"
                            },
                            {
                                "resource_name": {
                                    "en": "validation tool2"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "14"
                            }
                        ],
                        "validation_report": [
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            },
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            }
                        ],
                        "validator": [
                            {
                                "actor_type": "Person",
                                "surname": {
                                    "en": "Smith"
                                },
                                "given_name": {
                                    "en": "John"
                                },
                                "personal_identifier": [
                                    {
                                        "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                        "value": "0000-0000-0000-0000"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "validation_type": "http://w3id.org/meta-share/meta-share/content",
                        "validation_mode": "http://w3id.org/meta-share/meta-share/manual",
                        "validation_details": {
                            "en": "free text for validation"
                        },
                        "validation_extent": "http://w3id.org/meta-share/meta-share/full",
                        "is_validated_by": [
                            {
                                "resource_name": {
                                    "en": "validation tool"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "12"
                            },
                            {
                                "resource_name": {
                                    "en": "validation tool2"
                                },
                                "lr_identifier": [
                                    {
                                        "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                        "value": "String"
                                    }
                                ],
                                "version":  "14"
                            }
                        ],
                        "validation_report": [
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            },
                            {
                                "title": {
                                    "en": "title of a report"
                                },
                                "document_identifier": [
                                    {
                                        "document_identifier_scheme": "http://purl.org/spar/datacite/url",
                                        "value": "http://www.document.com"
                                    }
                                ]
                            }
                        ],
                        "validator": [
                            {
                                "actor_type": "Person",
                                "surname": {
                                    "en": "Smith"
                                },
                                "given_name": {
                                    "en": "John"
                                },
                                "personal_identifier": [
                                    {
                                        "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                        "value": "0000-0000-0000-0000"
                                    }
                                ]
                            }
                        ]
                    }
                ],
                "is_documented_by": [
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://purl.org/spar/datacite/ark",
                                "value": "https://research.google/tools/datasets/discofuse/"
                            }
                        ]
                    }
                ],
                "is_described_by": [
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://purl.org/spar/datacite/ark",
                                "value": "https://research.google/tools/datasets/discofuse/"
                            }
                        ]
                    },
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://purl.org/spar/datacite/ark",
                                "value": "https://research.google/tools/datasets/discofuse/"
                            }
                        ]
                    }
                ],
                "is_cited_by": [
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://purl.org/spar/datacite/ark",
                                "value": "https://research.google/tools/datasets/discofuse/"
                            }
                        ]
                    },
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                                "value": "ELG-ENT-DOC-260320-00000576"
                            }
                        ]
                    }
                ],
                "is_reviewed_by": [
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                                "value": "ELG-ENT-DOC-260320-00000577"
                            }
                        ]
                    },
                    {
                        "title": {
                            "en": "title"
                        },
                        "document_identifier": [
                            {
                                "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                                "value": "ELG-ENT-DOC-260320-00000578"
                            }
                        ]
                    }
                ],
                "is_part_of": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    }
                ],
                "is_part_with": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    }
                ],
                "is_similar_to": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    }
                ],
                "is_exact_match_with": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "14"
                    }
                ],
                "has_metadata": [
                    {
                        "metadata_record_identifier": {
                            "metadata_record_identifier_scheme": "http://w3id.org/meta-share/meta-share/CORE",
                            "value": "String"
                        }
                    }
                ],
                "is_archived_by": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    }
                ],
                "is_continuation_of": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    }
                ],
                "replaces": [
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }],
                        "version":  "unspecified"
                    },
                    {
                        "resource_name": {
                            "en": "another resource"
                        },
                        "lr_identifier": [
                            {
                                "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                "value": "https://doi.org/10.5281/zenodo.3525366"
                            }
                        ],
                        "version":  "unspecified"
                    }
                ],
                "is_version_of": {
                    "resource_name": {
                        "en": "another resource"
                    },
                    "lr_identifier": [
                        {
                            "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "https://doi.org/10.5281/zenodo.3525366"
                        }
                    ],
                    "version":  "unspecified"
                },
                "relation": [
                    {
                        "relation_type": {
                            "en": "relation type"
                        },
                        "related_lr": {
                            "resource_name": {
                                "en": "another resource"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                    "value": "https://doi.org/10.5281/zenodo.3525366"
                                }
                            ],
                            "version":  "unspecified"
                        }
                    },
                    {
                        "relation_type": {
                            "en": "relation type2"
                        },
                        "related_lr": {
                            "resource_name": {
                                "en": "another resource"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                    "value": "https://doi.org/10.5281/zenodo.3525366"
                                }
                            ],
                            "version":  "unspecified"
                        }
                    }
                ],
                "lr_subclass": {
                    "lr_type": "LexicalConceptualResource",
                    "lcr_subclass": "http://w3id.org/meta-share/meta-share/computationalLexicon",
                    "encoding_level": [
                        "http://w3id.org/meta-share/meta-share/phonology",
                        "http://w3id.org/meta-share/meta-share/morphology"
                    ],
                    "content_type": [
                        "http://w3id.org/meta-share/meta-share/collocation",
                        "http://w3id.org/meta-share/meta-share/definition"
                    ],
                    "complies_with": [
                        "http://w3id.org/meta-share/meta-share/EAGLES",
                        "http://w3id.org/meta-share/meta-share/SemAF-SR"
                    ],
                    "external_reference": [
                        {
                            "resource_name": {
                                "en": "French wordnet"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                    "value": "randomId2"
                                },
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtd",
                                    "value": "id2"
                                }
                            ],
                            "version":  "1.0"
                        },
                        {
                            "resource_name": {
                                "en": "English wordnet"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                    "value": "randomId"
                                },
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtd",
                                    "value": "id"
                                }
                            ],
                            "version":  "1.0"
                        }
                    ],
                    "theoretic_model": [
                        {
                            "en": "Generative lexicon"
                        }
                    ],
                    "extratextual_information": [
                        "http://w3id.org/meta-share/meta-share/soundRecording",
                        "http://w3id.org/meta-share/meta-share/video1"
                    ],
                    "extratextual_information_unit": [
                        "http://w3id.org/meta-share/meta-share/word2",
                        "http://w3id.org/meta-share/meta-share/syntacticUnit"
                    ],
                    "lexical_conceptual_resource_media_part": [
                        {
                            "lcr_media_type": "LexicalConceptualResourceImagePart",
                            "media_type": "http://w3id.org/meta-share/meta-share/image",
                            "linguality_type": "http://w3id.org/meta-share/meta-share/bilingual",
                            "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
                            "multilinguality_type_details": {
                                "en": "lemma and translation"
                            },
                            "language": [
                                {
                                    "language_tag": "tgd-Bugi-MAh-e-p-b-u-r-n",
                                    "language_id": "tgd",
                                    "script_id": "Bugi",
                                    "region_id": "MA",
                                    "variant_id": ["hepburn"]
                                }
                            ],
                            "language_variety": [
                                {
                                    "language_variety_type": "http://w3id.org/meta-share/meta-share/other",
                                    "language_variety_name": {
                                        "en": "french dialect"
                                    }
                                },
                                {
                                    "language_variety_type": "http://w3id.org/meta-share/meta-share/jargon",
                                    "language_variety_name": {
                                        "en": "village dialect"
                                    }
                                }
                            ],
                            "metalanguage": [
                                {
                                    "language_tag": "okb-Grek-VCa-r-e-v-e-l-a",
                                    "language_id": "okb",
                                    "script_id": "Grek",
                                    "region_id": "VC",
                                    "variant_id": ["arevela"]
                                },
                                {
                                    "language_tag": "nfr-Nkoo-UGt-a-r-a-s-k",
                                    "language_id": "nfr",
                                    "script_id": "Nkoo",
                                    "region_id": "UG",
                                    "variant_id": ["tarask"]
                                }
                            ],
                            "modality_type": [
                                "http://w3id.org/meta-share/meta-share/voice",
                                "http://w3id.org/meta-share/meta-share/writtenLanguage"
                            ],
                            "type_of_image_content": [{
                                "en": "various vehicles"
                            }],
                            "text_included_in_image": [
                                "http://w3id.org/meta-share/meta-share/none2",
                                "http://w3id.org/meta-share/meta-share/caption"
                            ],
                            "static_element": [
                                {
                                    "type_of_element": [{
                                        "en": "car"
                                    }],
                                    "body_part": [
                                        "http://w3id.org/meta-share/meta-share/foot",
                                        "http://w3id.org/meta-share/meta-share/face"
                                    ],
                                    "face_view": [{
                                        "en": "profile"
                                    }],
                                    "face_expression": [{
                                        "en": "happy"
                                    }],
                                    "artifact_part": [{
                                        "en": "tyre"
                                    }],
                                    "landscape_part": [{
                                        "en": "tree"
                                    }],
                                    "person_description": {
                                        "en": "fat"
                                    },
                                    "thing_description": {
                                        "en": "wheel"
                                    },
                                    "organization_description": {
                                        "en": "institution"
                                    },
                                    "event_description": {
                                        "en": "birth"
                                    }
                                },
                                {
                                    "type_of_element": [{
                                        "en": "car"
                                    }],
                                    "body_part": [
                                        "http://w3id.org/meta-share/meta-share/foot",
                                        "http://w3id.org/meta-share/meta-share/face"
                                    ],
                                    "face_view": [{
                                        "en": "profile"
                                    }],
                                    "face_expression": [{
                                        "en": "happy"
                                    }],
                                    "artifact_part": [{
                                        "en": "tyre"
                                    }],
                                    "landscape_part": [{
                                        "en": "countryside"
                                    }],
                                    "person_description": {
                                        "en": "tall"
                                    },
                                    "thing_description": {
                                        "en": "car"
                                    },
                                    "organization_description": {
                                        "en": "institution"
                                    },
                                    "event_description": {
                                        "en": "wedding"
                                    }
                                }
                            ]
                        }
                    ],
                    "dataset_distribution": [
                        {
                            "dataset_distribution_form": "http://w3id.org/meta-share/meta-share/accessibleThroughInterface",
                            "download_location": "http://www.download.com",
                            "access_location": "http://www.access.com",
                            "is_accessed_by": [
                                {
                                    "resource_name": {
                                        "en": "access tool"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://purl.org/spar/datacite/handle",
                                            "value": "pid1"
                                        }
                                    ],
                                    "version":  "13"
                                }
                            ],
                            "is_displayed_by": [
                                {
                                    "resource_name": {
                                        "en": "display tool"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                            "value": "id43"
                                        }
                                    ],
                                    "version":  "14"
                                }
                            ],
                            "is_queried_by": [
                                {
                                    "resource_name": {
                                        "en": "query tool"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/metaShare",
                                            "value": "msId"
                                        }
                                    ],
                                    "version":  "33"
                                }
                            ],
                            "samples_location": [
                                "ftp://www.samples.com"
                            ],
                            "distribution_image_feature": [
                                {
                                    "size": [
                                        {
                                            "amount": 5.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/question"
                                        },
                                        {
                                            "amount": 12.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/second"
                                        }
                                    ],
                                    "data_format": [
                                        "http://w3id.org/meta-share/omtd-share/BionlpSt2013A1_a2",
                                        "http://w3id.org/meta-share/omtd-share/LD_json"
                                    ],
                                    "image_format": [
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/BionlpSt2013A1_a2",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/CMYK"
                                            ],
                                            "colour_depth": [
                                                10,
                                                4
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 50,
                                                    "size_height": 50,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/mov",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                            "quality": "http://w3id.org/meta-share/meta-share/high1"
                                        },
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/LD_json",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/RGB"
                                            ],
                                            "colour_depth": [
                                                10,
                                                10
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 24,
                                                    "size_height": 24,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/TwoD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/oggVorbis",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/vector",
                                            "quality": "http://w3id.org/meta-share/meta-share/medium1"
                                        }
                                    ]
                                },
                                {
                                    "size": [
                                        {
                                            "amount": 3.14159,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/gb"
                                        },
                                        {
                                            "amount": 3.14159,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/shot1"
                                        }
                                    ],
                                    "data_format": [
                                        "http://w3id.org/meta-share/omtd-share/Xces",
                                        "http://w3id.org/meta-share/omtd-share/LD_json"
                                    ],
                                    "image_format": [
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/Xces",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/CMYK",
                                                "http://w3id.org/meta-share/meta-share/RGB"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/TwoD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/shorten",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/vector",
                                            "quality": "http://w3id.org/meta-share/meta-share/medium1"
                                        },
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/LD_json",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/CMYK",
                                                "http://w3id.org/meta-share/meta-share/RGB"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/TwoD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/mp3",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/vector",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryLow1"
                                        }
                                    ]
                                }
                            ],
                            "licence_terms": [
                                {
                                    "licence_terms_name": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "licence_terms_url": [
                                        "http://www.licecen3.com",
                                        "http://www.licece44.com"
                                    ],
                                    "condition_of_use": [
                                        "http://w3id.org/meta-share/meta-share/unspecified"
                                    ],
                                    "licence_identifier": [
                                        {
                                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                            "value": "String"
                                        }
                                    ]
                                },
                                {
                                    "licence_terms_name": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "licence_terms_url": [
                                        "http://www.licecen1.com",
                                        "http://www.licecen2.com"
                                    ],
                                    "condition_of_use": [
                                        "http://w3id.org/meta-share/meta-share/unspecified"
                                    ],
                                    "licence_identifier": [
                                        {
                                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX",
                                            "value": "String"
                                        }
                                    ]
                                }
                            ],
                            "attribution_text": {
                                "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                            },
                            "cost": {
                                "amount": 3.14159,
                                "currency": "http://w3id.org/meta-share/meta-share/euro"
                            },
                            "membership_institution": [
                                "http://w3id.org/meta-share/meta-share/other",
                                "http://w3id.org/meta-share/meta-share/ELRA"
                            ],
                            "copyright_statement": {
                                "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                            },
                            "availability_start_date": "1967-08-13",
                            "availability_end_date": "1967-08-13",
                            "distribution_rights_holder": [
                                {
                                    "actor_type": "Person",
                                    "surname": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "given_name": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "personal_identifier": [
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/openid",
                                            "value": "String"
                                        },
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/researcherid",
                                            "value": "String"
                                        }
                                    ]
                                },
                                {
                                    "actor_type": "Person",
                                    "surname": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "given_name": {
                                        "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                    },
                                    "personal_identifier": [
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                            "value": "String"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "dataset_distribution_form": "http://w3id.org/meta-share/meta-share/accessibleThroughInterface",
                            "download_location": "http://www.download.location.com",
                            "access_location": "http://www.accessLocation.com",
                            "is_accessed_by": [
                                {
                                    "resource_name": {
                                        "en": "acess tool for lexica"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                                            "value": "String"
                                        },
                                        {
                                            "lr_identifier_scheme": "http://purl.org/spar/datacite/bibcode",
                                            "value": "String"
                                        }
                                    ],
                                    "version":  "15"
                                }
                            ],
                            "is_displayed_by": [
                                {
                                    "resource_name": {
                                        "en": "display tool for lexica"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                            "value": "String"
                                        },
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/islrn",
                                            "value": "String"
                                        }
                                    ],
                                    "version":  "43"
                                }
                            ],
                            "is_queried_by": [
                                {
                                    "resource_name": {
                                        "en": "query tool for lexica"
                                    },
                                    "lr_identifier": [
                                        {
                                            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/maven",
                                            "value": "String"
                                        },
                                        {
                                            "lr_identifier_scheme": "http://purl.org/spar/datacite/bibcode",
                                            "value": "String"
                                        }
                                    ],
                                    "version":  "1"
                                }
                            ],
                            "samples_location": [
                                "ftp://samples.com"
                            ],
                            "distribution_image_feature": [
                                {
                                    "size": [
                                        {
                                            "amount": 85.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/byte"
                                        },
                                        {
                                            "amount": 150.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/semanticUnit1"
                                        }
                                    ],
                                    "data_format": [
                                        "http://w3id.org/meta-share/omtd-share/OpenOfficeDocument",
                                        "http://w3id.org/meta-share/omtd-share/CorpusFormat"
                                    ],
                                    "image_format": [
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/OpenOfficeDocument",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/YUV"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 15,
                                                    "size_height": 5,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/TwoD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/oggVorbis",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryHigh1"
                                        },
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/CorpusFormat",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/CMYK"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryLow1"
                                        }
                                    ]
                                },
                                {
                                    "size": [
                                        {
                                            "amount": 52.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/utterance1"
                                        },
                                        {
                                            "amount": 150.0,
                                            "size_unit": "http://w3id.org/meta-share/meta-share/article"
                                        }
                                    ],
                                    "data_format": [
                                        "http://w3id.org/meta-share/omtd-share/Kaf",
                                        "http://w3id.org/meta-share/omtd-share/ConllFormat"
                                    ],
                                    "image_format": [
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/Kaf",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/CMYK"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.720"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/other",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryHigh1"
                                        },
                                        {
                                            "data_format": "http://w3id.org/meta-share/omtd-share/ConllFormat",
                                            "colour_space": [
                                                "http://w3id.org/meta-share/meta-share/YUV",
                                                "http://w3id.org/meta-share/meta-share/CMYK"
                                            ],
                                            "colour_depth": [
                                                0,
                                                0
                                            ],
                                            "resolution": [
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                },
                                                {
                                                    "size_width": 0,
                                                    "size_height": 0,
                                                    "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                                }
                                            ],
                                            "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                            "compressed": True,
                                            "compression_name": "http://w3id.org/meta-share/meta-share/other",
                                            "compression_loss": True,
                                            "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/vector",
                                            "quality": "http://w3id.org/meta-share/meta-share/veryHigh1"
                                        }
                                    ]
                                }
                            ],
                            "licence_terms": [
                                {
                                    "licence_terms_name": {
                                        "en": "Creative Commons By 4.0"
                                    },
                                    "licence_terms_url": [
                                        "http://www.cc-by.com"
                                    ],
                                    "condition_of_use": [
                                        "http://w3id.org/meta-share/meta-share/unspecified"
                                    ],
                                    "licence_identifier": [
                                        {
                                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX",
                                            "value": "cc-by-4.0"
                                        }
                                    ]
                                },
                                {
                                    "licence_terms_name": {
                                        "en": "Creative Commons By 3.0"
                                    },
                                    "licence_terms_url": [
                                        "http://www.cc-by3.com"
                                    ],
                                    "condition_of_use": [
                                        "http://w3id.org/meta-share/meta-share/unspecified"
                                    ],
                                    "licence_identifier": [
                                        {
                                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX",
                                            "value": "cc-by-3.0"
                                        }
                                    ]
                                }
                            ],
                            "attribution_text": {
                                "en": "LCR x licensed under cc licence"
                            },
                            "cost": {
                                "amount": 53000.0,
                                "currency": "http://w3id.org/meta-share/meta-share/euro"
                            },
                            "membership_institution": [
                                "http://w3id.org/meta-share/meta-share/ELRA",
                                "http://w3id.org/meta-share/meta-share/LDC"
                            ],
                            "copyright_statement": {
                                "en": "copyrighted by noone"
                            },
                            "availability_start_date": "2010-08-13",
                            "availability_end_date": "2040-08-13",
                            "distribution_rights_holder": [
                                {
                                    "actor_type": "Organization",
                                    "organization_name": {
                                        "en": "Organization official title"
                                    },
                                    "website": [
                                        "http://www.exampleNew.com"
                                    ]
                                },
                                {
                                    "actor_type": "Person",
                                    "surname": {
                                        "en": "Smith"
                                    },
                                    "given_name": {
                                        "en": "Mary"
                                    },
                                    "personal_identifier": [
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                            "value": "0000-0000-0000-0001"
                                        }
                                    ]
                                },
                                {
                                    "actor_type": "Person",
                                    "surname": {
                                        "en": "Smith"
                                    },
                                    "given_name": {
                                        "en": "John"
                                    },
                                    "personal_identifier": [
                                        {
                                            "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                            "value": "0000-0000-0000-0000"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "personal_data_included": "http://w3id.org/meta-share/meta-share/yesP",
                    "personal_data_details": {
                        "en": "identity, sex, name"
                    },
                    "sensitive_data_included": "http://w3id.org/meta-share/meta-share/yesS",
                    "sensitive_data_details": {
                        "en": "tax number"
                    },
                    "anonymized": "http://w3id.org/meta-share/meta-share/yesA",
                    "anonymization_details": {
                        "en": "method of anonymization"
                    },
                    "is_analysed_by": [
                        {
                            "resource_name": {
                                "en": "analysis tool 2"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                    "value": "String"
                                }
                            ],
                            "version":  "1"
                        },
                        {
                            "resource_name": {
                                "en": "analysis tool"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                    "value": "String"
                                }
                            ],
                            "version":  "1"
                        }
                    ],
                    "is_edited_by": [
                        {
                            "resource_name": {
                                "en": "editing tool"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/metaShare",
                                    "value": "String"
                                }
                            ],
                            "version":  "2.1.1"
                        },
                        {
                            "resource_name": {
                                "en": "editing tool2"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                    "value": "String"
                                }
                            ],
                            "version":  "2.2"
                        }
                    ],
                    "is_elicited_by": [
                        {
                            "resource_name": {
                                "en": "editing tool12"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/islrn",
                                    "value": "String"
                                }
                            ],
                            "version":  "13"
                        },
                        {
                            "resource_name": {
                                "en": "elicitation tool"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                    "value": "String"
                                }
                            ],
                            "version":  "1"
                        }
                    ],
                    "is_converted_version_of": [
                        {
                            "resource_name": {
                                "en": "raw corpus"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/isbn",
                                    "value": "String"
                                }
                            ],
                            "version":  "12"
                        },
                        {
                            "resource_name": {
                                "en": "converted corpus"
                            },
                            "lr_identifier": [
                                {
                                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/omtdDocker",
                                    "value": "String"
                                },
                                {
                                    "lr_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                    "value": "String"
                                }
                            ],
                            "version":  "0.0.8"
                        }
                    ],
                    "time_coverage": [
                        {
                            "en": "16th century"
                        }
                    ],
                    "geographic_coverage": [
                        {
                            "en": "all over the world"
                        }
                    ]
                }
            },
            "management_object": {}
        }
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        LOGGER.info(serializer.errors)
        instance_md = serializer.save()
        instance = instance_md.described_entity.lr_subclass

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['external_reference'] = [lr.pk for lr in
                                                   instance.external_reference.all()]
        cls.nested_models['lexical_conceptual_resource_media_part'] = [
            lexical_conceptual_resource_media_part.pk for
            lexical_conceptual_resource_media_part in
            instance.lexical_conceptual_resource_media_part.all()]
        cls.nested_models['dataset_distribution'] = [dd.pk for dd in
                                                     instance.dataset_distribution.all()]
        cls.nested_models['is_analysed_by'] = [lr.pk for lr in
                                               instance.is_analysed_by.all()]
        cls.nested_models['is_edited_by'] = [lr.pk for lr in
                                             instance.is_edited_by.all()]
        cls.nested_models['is_elicited_by'] = [lr.pk for lr in
                                               instance.is_elicited_by.all()]
        cls.nested_models['is_converted_version_of'] = [lr.pk for lr in
                                                        instance.is_converted_version_of.all()]
        json_file_unsp = open('test_fixtures/dummy_data.json', 'r')
        json_str_unsp = json_file_unsp.read()
        json_file_unsp.close()
        raw_data_unsp = json.loads(json_str_unsp)['unspecified_part']
        dist_unsp_feature = json.loads(json_str_unsp)['distribution_unspecified_feature']
        json_data['described_entity']['resource_name'] = {'en': 'other lcr'}
        json_data['described_entity']['resource_short_name'] = {'en': 'other ahort lcr'}
        json_data['described_entity']['version'] = 30
        json_data['described_entity']['lr_identifier'] = []
        json_data['described_entity']['lr_subclass']['unspecified_part'] = raw_data_unsp
        json_data['described_entity']['lr_subclass']['lexical_conceptual_resource_media_part'] = []
        json_data['described_entity']['lr_subclass']['dataset_distribution'][0][
            'distribution_unspecified_feature'] = dist_unsp_feature
        json_data['described_entity']['lr_subclass']['dataset_distribution'][0]['distribution_image_feature'] = []
        json_data['described_entity']['lr_subclass']['dataset_distribution'][1][
            'distribution_unspecified_feature'] = dist_unsp_feature
        json_data['described_entity']['lr_subclass']['dataset_distribution'][1]['distribution_image_feature'] = []
        serializer_unsp = serializers.MetadataRecordSerializer(data=json_data, context={'for_information_only': True})
        if serializer_unsp.is_valid():
            instance_unsp = serializer_unsp.save()
        else:
            LOGGER.info(serializer_unsp.errors)
        instance_unsp_lcr = instance_unsp.described_entity.lr_subclass
        cls.instance_unsp_pk = instance_unsp_lcr.pk
        cls.nested_models[
            'unspecified_part'] = instance_unsp_lcr.unspecified_part.pk

    def test_lcr_is_not_deleted_when_lcr_media_part_is(self):
        for pk in self.nested_models['lexical_conceptual_resource_media_part']:
            obj = models.LexicalConceptualResourceMediaPart.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LexicalConceptualResource.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lcr_is_not_deleted_when_unspecified_part_is(self):
        obj = models.unspecifiedPart.objects.get(id=self.nested_models['unspecified_part'])
        obj.delete()
        try:

            initial_instance = models.LexicalConceptualResource.objects.get(id=self.instance_unsp_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lcr_is_not_deleted_when_dataset_distribution_is(self):
        for pk in self.nested_models['dataset_distribution']:
            obj = models.DatasetDistribution.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LexicalConceptualResource.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityLanguageDescriptionFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/ld_grammar.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        instance = instance_md.described_entity.lr_subclass

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models[
            'language_description_subclass'] = instance.language_description_subclass.pk
        cls.nested_models['language_description_media_part'] = [
            language_description_media_part.pk for
            language_description_media_part in
            instance.language_description_media_part.all()]
        cls.nested_models['dataset_distribution'] = [dd.pk for dd in
                                                     instance.dataset_distribution.all()]
        cls.nested_models['is_analysed_by'] = [lr.pk for lr in
                                               instance.is_analysed_by.all()]
        cls.nested_models['is_edited_by'] = [lr.pk for lr in
                                             instance.is_edited_by.all()]
        cls.nested_models['is_elicited_by'] = [lr.pk for lr in
                                               instance.is_elicited_by.all()]
        cls.nested_models[
            'is_converted_version_of'] = [lr.pk for lr in
                                          instance.is_converted_version_of.all()]
        json_file_model = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str_model = json_file_model.read()
        json_file_model.close()
        json_data_model = json.loads(json_str_model)
        json_data_model['described_entity']['resource_name'] = {'en': 'other model fk'}
        json_data_model['described_entity']['resource_short_name'] = {'en': 'other model ffk'}
        json_data_model['described_entity']['version'] = 15
        json_data_model['described_entity']['lr_identifier'] = []
        serializer_model = serializers.MetadataRecordSerializer(data=json_data_model)
        if serializer_model.is_valid():
            instance_model_md = serializer_model.save()
        else:
            LOGGER.info(serializer_model.errors)
        instance_model = instance_model_md.described_entity.lr_subclass
        cls.instance_model_pk = instance_model.pk
        cls.nested_models[
            'unspecified_part'] = instance_model.unspecified_part.pk

    def test_ld_is_not_deleted_when_ld_media_part_is(self):
        for pk in self.nested_models['language_description_media_part']:
            obj = models.LanguageDescriptionMediaPart.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageDescription.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_ld_is_not_deleted_when_unspecified_part_is(self):
        obj = models.unspecifiedPart.objects.get(id=self.nested_models['unspecified_part'])
        obj.delete()
        try:

            initial_instance = models.LanguageDescription.objects.get(id=self.instance_model_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_ld_is_not_deleted_when_dataset_distribution_is(self):
        for pk in self.nested_models['dataset_distribution']:
            obj = models.DatasetDistribution.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.LanguageDescription.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityModelFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['resource_name'] = {'en': 'mmmodel fk'}
        json_data['described_entity']['resource_short_name'] = {'en': 'mmmmodel fk short'}
        json_data['described_entity']['version'] = 99
        json_data['described_entity']['lr_identifier'] = []
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        instance = instance_md.described_entity.lr_subclass
        ngram_json_file = open('test_fixtures/dummy_data.json', 'r')
        ngram_json_str = ngram_json_file.read()
        ngram_json_file.close()
        ngram_raw_data = json.loads(ngram_json_str)['ngram_model']
        ngram_serializer = serializers.NGramModelSerializer(data=ngram_raw_data)
        if ngram_serializer.is_valid():
            ngram_instance = ngram_serializer.save()
        else:
            LOGGER.info(ngram_serializer.errors)
        # Retrieve pk from nested models
        instance.n_gram_model = ngram_instance
        instance.save()
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['n_gram_model'] = instance.n_gram_model.pk

    def test_model_is_not_deleted_when_n_gram_model_is(self):
        obj = models.NGramModel.objects.get(id=self.nested_models['n_gram_model'])
        obj.delete()
        try:
            initial_instance = models.LRSubclass.objects.get(pk=self.instance_pk)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)



class TestDeletionFunctionalityDatasetDistributionFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['dataset_distribution']
        serializer = serializers.DatasetDistributionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        c_f, _ = ContentFile.objects.get_or_create(file=f'fdasdasdasda/test_filename.zip')
        instance.dataset = c_f
        instance.save()
        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['is_accessed_by'] = [
            lr.pk for lr in
            instance.is_accessed_by.all()]
        cls.nested_models['is_displayed_by'] = [
            lr.pk for lr in
            instance.is_displayed_by.all()]
        cls.nested_models['is_queried_by'] = [
            lr.pk for lr in
            instance.is_queried_by.all()]
        cls.nested_models['distribution_text_feature'] = [
            distribution_text_feature.pk for
            distribution_text_feature in
            instance.distribution_text_feature.all()]
        cls.nested_models['distribution_text_numerical_feature'] = [
            distribution_text_numerical_feature.pk for
            distribution_text_numerical_feature in
            instance.distribution_text_numerical_feature.all()]
        cls.nested_models['distribution_audio_feature'] = [
            distribution_audio_feature.pk for
            distribution_audio_feature in
            instance.distribution_audio_feature.all()]
        cls.nested_models['distribution_image_feature'] = [
            distribution_image_feature.pk for
            distribution_image_feature in
            instance.distribution_image_feature.all()]
        cls.nested_models['distribution_video_feature'] = [
            distribution_video_feature.pk for
            distribution_video_feature in
            instance.distribution_video_feature.all()]
        cls.nested_models['licence_terms'] = [licence.pk for
                                              licence in
                                              instance.licence_terms.all()]
        cls.nested_models['cost'] = instance.cost.pk
        cls.nested_models['distribution_rights_holder'] = [actor.pk for actor in
                                                           instance.distribution_rights_holder.all()]
        cls.nested_models['access_rights'] = [access_rights.pk for access_rights
                                              in
                                              instance.access_rights.all()]
        cls.nested_models['dataset'] = instance.dataset.pk
        json_file_unsp = open('test_fixtures/dummy_data.json', 'r')
        json_str_unsp = json_file_unsp.read()
        json_file_unsp.close()
        raw_data_unsp = json.loads(json_str_unsp)['unspecified_part']
        dist_unsp_feature = json.loads(json_str_unsp)['distribution_unspecified_feature']
        raw_data_fi = json.loads(json_str)['dataset_distribution']
        raw_data_fi['distribution_unspecified_feature'] = dist_unsp_feature
        raw_data_fi['distribution_text_feature'] = []
        raw_data_fi['distribution_text_numerical_feature'] = []
        raw_data_fi['distribution_image_feature'] = []
        raw_data_fi['distribution_video_feature'] = []
        raw_data_fi['distribution_audio_feature'] = []
        serializer_fi = serializers.DatasetDistributionSerializer(data=raw_data_fi, context={'for_information_only': True})
        if serializer_fi.is_valid():
            instance_unsp = serializer_fi.save()
        else:
            LOGGER.info(serializer.errors)
        cls.instance_unsp_pk = instance_unsp.pk
        cls.nested_models[
            'distribution_unspecified_feature'] = instance_unsp.distribution_unspecified_feature.pk

    def test_dataset_distribution_is_not_deleted_when_dataset_distribution_text_feature_is(self):
        for pk in self.nested_models['distribution_text_feature']:
            obj = models.DistributionTextFeature.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DatasetDistribution.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_distribution_is_not_deleted_when_dataset_distribution_unspecified_feature_is(self):
        obj = models.distributionUnspecifiedFeature.objects.get(id=self.nested_models['distribution_unspecified_feature'])
        obj.delete()
        try:

            initial_instance = models.DatasetDistribution.objects.get(id=self.instance_unsp_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_distribution_is_not_deleted_when_dataset_is(self):
        obj = ContentFile.objects.get(id=self.nested_models['dataset'])
        obj.delete()
        try:

            initial_instance = models.DatasetDistribution.objects.get(id=self.instance_unsp_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_distribution_is_not_deleted_when_dataset_distribution_audio_feature_is(self):
        for pk in self.nested_models['distribution_audio_feature']:
            obj = models.DistributionAudioFeature.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DatasetDistribution.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_distribution_is_not_deleted_when_dataset_distribution_video_feature_is(self):
        for pk in self.nested_models['distribution_video_feature']:
            obj = models.DistributionVideoFeature.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DatasetDistribution.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_distribution_is_not_deleted_when_dataset_distribution_image_feature_is(self):
        for pk in self.nested_models['distribution_image_feature']:
            obj = models.DistributionImageFeature.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DatasetDistribution.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_distribution_is_not_deleted_when_dataset_distribution_text_numerical_feature_is(self):
        for pk in self.nested_models['distribution_text_numerical_feature']:
            obj = models.DistributionTextNumericalFeature.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DatasetDistribution.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_distribution_is_not_deleted_when_dataset_distribution_access_right_is(self):
        for pk in self.nested_models['access_rights']:
            obj = models.AccessRightsStatement.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DatasetDistribution.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityDistributionTextFeatureFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_text_feature']
        serializer = serializers.DistributionTextFeatureSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]

    def test_distribution_text_feature_is_not_deleted_when_size_is(self):
        for pk in self.nested_models['size']:
            obj = models.Size.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionTextFeature.objects.get(id=self.instance_pk)

            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityDistributionTextNumericalFeatureFKRelations(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['distribution_text_numerical_feature']
        serializer = serializers.DistributionTextNumericalFeatureSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        # Retrieve pk from nested models
        cls.instance_pk = instance.pk
        cls.nested_models = dict()
        cls.nested_models['size'] = [size.pk for size in
                                     instance.size.all()]

    def test_distribution_text_numerical_feature_is_not_deleted_when_size_is(self):
        for pk in self.nested_models['size']:
            obj = models.Size.objects.get(id=pk)
            obj.delete()
        try:

            initial_instance = models.DistributionTextNumericalFeature.objects.get(id=self.instance_pk)
            
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)
