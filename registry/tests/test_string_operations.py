import json
import logging

from test_utils import SerializerTestCase

from registry import serializers
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer
from registry.utils.string_utils import sanitize_field, ALLOWED_TAGS, ALLOWED_ATTRS, ALLOWED_PROTOCOLS
from test_utils import import_metadata

LOGGER = logging.getLogger(__name__)


class TestFieldSanitization(SerializerTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json',
                 'test_fixtures/md-organization.json']

    @classmethod
    def setUpTestData(cls):
        cls.metadata_records = import_metadata(cls.test_data)

    def _call_assertions_create(self, field):
        if isinstance(field, dict):
            self.assertTrue('<b>' in field['en'])
            self.assertTrue('<code>' in field['en'])
            self.assertTrue('<p>' in field['en'])
            self.assertFalse('<script>' in field['en'])
        else:
            self.assertTrue('<b>' in field)
            self.assertTrue('<code>' in field)
            self.assertTrue('<p>' in field['en'])
            self.assertFalse('<script>' in field)

    def _call_assertions_update(self, field):
        if isinstance(field, dict):
            self.assertTrue('<b>' in field['en'])
            self.assertTrue('<code>' in field['en'])
            self.assertTrue('<p>' in field['en'])
            self.assertTrue('<i>' in field['en'])
            self.assertFalse('<script>' in field['en'])
        else:
            self.assertTrue('<b>' in field)
            self.assertTrue('<code>' in field)
            self.assertTrue('<p>' in field)
            self.assertTrue('<i>' in field)
            self.assertFalse('<script>' in field)

    def test_sanitized_text(self):
        test_string = '<p><strong>Strong</strong></p><p><i><strong>strong italics&nbsp;</strong></i></p>' \
                      '<p><i><strong><u>underline&nbsp;</u></strong></i></p>' \
                      '<p><a href="www.in.gr"><i><strong><u>link</u></strong></i></a>' \
                      '<i><strong><u>&nbsp;</u></strong></i></p>' \
                      '<pre><code class="language-plaintext">import html def my_var;</code></pre>' \
                      '<p>&nbsp;</p><ul><li>list1</li><li>list2</li>' \
                      '<li>list3</li><li>list4</li></ul><ol><li>listA</li>' \
                      '<li>listB</li><li>listC</li></ol>' \
                      '<script src="js/scripts.js">' \
                      '</script><script src="js/scripts.js"/><table><tr><th>head</th></tr>' \
                      '<tr><td>row</td></tr></table>'

        partial = sanitize_field(test_string, tags=ALLOWED_TAGS, attrs=ALLOWED_ATTRS, protocols=ALLOWED_PROTOCOLS,
                                 keep_new_lines=True)
        partial_result = '<p><strong>Strong</strong></p><p><i><strong>strong italics&nbsp;</strong></i></p><p><i>' \
                         '<strong><u>underline&nbsp;</u></strong></i></p><p><a href="www.in.gr"><i>' \
                         '<strong><u>link</u></strong></i></a><i><strong><u>&nbsp;</u></strong></i></p><pre>' \
                         '<code>import html def my_var;</code></pre><p>&nbsp;</p><ul><li>list1</li><li>list2</li>' \
                         '<li>list3</li><li>list4</li></ul><ol><li>listA</li><li>listB</li><li>listC</li></ol>' \
                         '<table><tbody><tr><th>head</th></tr><tr><td>row</td></tr></tbody></table>'

        full = sanitize_field(test_string, handle_nbsp=True)
        full_result = 'Strong strong italics underline link import html def my_var; list1 list2 list3 ' \
                      'list4 listA listB listC head row'

        self.assertEquals(partial, partial_result)
        self.assertEquals(full, full_result)

    def test_sanitized_text_on_lr_create(self):
        mdr = MetadataRecord.objects.get(described_entity__entity_type='LanguageResource')
        self._call_assertions_create(mdr.described_entity.description)

    def test_sanitized_text_on_lr_update(self):
        mdr = MetadataRecord.objects.get(described_entity__entity_type='LanguageResource')
        mdrs = MetadataRecordSerializer(mdr)
        data = mdrs.data
        data['described_entity']['description']['en'] = '<b>Named entity recognition</b> ' \
                                                        '<code>pipeline that identifies</code> basic entity types, ' \
                                                        '<script>such</script> as Person, <i>Location</i>, ' \
                                                        '<table>Organization</table>, <p>Money amounts<p>, Time and Date ' \
                                                        'expressions'
        serializer = MetadataRecordSerializer(mdrs.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            self._call_assertions_update(instance.described_entity.description)

    def test_sanitized_text_on_org_create(self):
        mdr = MetadataRecord.objects.get(described_entity__entity_type='Organization')
        self._call_assertions_create(mdr.described_entity.organization_bio)

    def test_sanitized_text_on_org_update(self):
        mdr = MetadataRecord.objects.get(described_entity__entity_type='Organization')
        mdrs = MetadataRecordSerializer(mdr)
        data = mdrs.data
        data['described_entity']['organization_bio']['en'] = '<b>Athena Research</b> and ' \
                                                             '<code>Innovation Centre (ARC)</code> is a scientific ' \
                                                             '<script>research</script> and ' \
                                                             '<i>technological organisation</i>, ' \
                                                             '<table>functioning</table> under the auspices of the ' \
                                                             'General Secretariat for <p>Research and Technology</p>'
        serializer = MetadataRecordSerializer(mdrs.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            self._call_assertions_update(instance.described_entity.organization_bio)

    def test_sanitized_text_on_project_create(self):
        mdr = MetadataRecord.objects.get(described_entity__entity_type='Project')
        self._call_assertions_create(mdr.described_entity.project_summary)

    def test_sanitized_text_on_project_update(self):
        mdr = MetadataRecord.objects.get(described_entity__entity_type='Project')
        mdrs = MetadataRecordSerializer(mdr)
        data = mdrs.data
        data['described_entity']['project_summary']['en'] = 'The goal of the <b>ELITR</b> project is to ' \
                                                            '<code>remove</code> the language barrier in comm ' \
                                                            '<script>within</script> and among European citizens, ' \
                                                            '<i>companies</i>, institutes and organizations at ' \
                                                            '<table>large</table> <p>assemblies like conferences, in ' \
                                                            'smaller live discussions.</p>'
        serializer = MetadataRecordSerializer(mdrs.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            self._call_assertions_update(instance.described_entity.project_summary)
