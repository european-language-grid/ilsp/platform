import json
import logging

from datacite import schema43

from django.conf import settings

from registry import choices
from registry.models import MetadataRecord
from registry.serializers_datacite import (
    DataCiteMetadataRecordSerializer, DATACITE_IDENTIFIER_CHOICES
)
from test_utils import SerializerTestCase, import_metadata

LOGGER = logging.getLogger(__name__)


class TestDataciteRecordSerializer(SerializerTestCase):
    test_data = ['test_fixtures/md-2-tool.json', 'registry/tests/fixtures/corpus.json']
    tool_data = json.loads(open(test_data[0], encoding='utf-8').read())
    corpus_data = json.loads(open(test_data[1], encoding='utf-8').read())

    @classmethod
    def setUpTestData(cls):
        cls.tool_record, cls.corpus_record = import_metadata(cls.test_data)

        cls.datacite_tool_serializer = DataCiteMetadataRecordSerializer(instance=cls.tool_record)
        cls.datacite_tool_data = cls.datacite_tool_serializer.data

        # validate datacite schema 4.3
        assert schema43.validate(cls.datacite_tool_data)

        cls.datacite_corpus_serializer = DataCiteMetadataRecordSerializer(instance=cls.corpus_record)
        cls.datacite_corpus_data = cls.datacite_corpus_serializer.data

        # validate datacite schema 4.3
        assert schema43.validate(cls.datacite_corpus_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def test_titles(self):
        # title == resource_name
        self.assertEqual(
            self.datacite_corpus_data['titles'][0]['title'],
            self.corpus_data['described_entity']['resource_name']['en']
        )
        # title alternative == resource_short_name
        self.assertEqual(
            self.datacite_corpus_data['titles'][1]['titleType'],
            'AlternativeTitle'
        )
        self.assertEqual(
            self.datacite_corpus_data['titles'][1]['title'],
            self.corpus_data['described_entity']['resource_short_name']['en']
        )

    def test_descriptions(self):
        self.assertEqual(
            self.datacite_corpus_data['descriptions'][0]['description'],
            self.corpus_data['described_entity']['description']['en']
        )

    def test_subjects(self):
        # subjects == subject-->category_label, keyword, function
        self.assertEqual(
            self.datacite_tool_data['subjects'][0]['subject'],
            self.tool_data['described_entity']['subject'][0]['category_label']['en']
        )
        self.assertEqual(
            self.datacite_tool_data['subjects'][1]['subject'],
            self.tool_data['described_entity']['keyword'][0]['en']
        )
        self.assertEqual(
            self.datacite_tool_data['subjects'][2]['subject'],
            self.tool_data['described_entity']['keyword'][1]['en']
        )
        self.assertEqual(
            self.datacite_tool_data['subjects'][3]['subject'],
            self.tool_data['described_entity']['keyword'][2]['en']
        )
        self.assertEqual(
            self.datacite_tool_data['subjects'][4]['subject'],
            choices.LT_CLASS_RECOMMENDED_CHOICES[self.tool_data['described_entity']['lr_subclass']['function'][0]]
        )
        self.assertEqual(
            self.datacite_tool_data['subjects'][5]['subject'],
            choices.LT_CLASS_RECOMMENDED_CHOICES[self.tool_data['described_entity']['lr_subclass']['function'][1]]
        )

    def test_publication_year(self):
        # publicationYear == publication_date.year | management_object.publication_date.year
        self.assertEqual(
            self.datacite_corpus_data['publicationYear'],
            str(self.corpus_record.described_entity.publication_date.year)
        )

    def test_creators(self):
        # creators == resourceCreator
        self.assertEqual(
            self.datacite_corpus_data['creators'][0]['name'],
            f"{self.corpus_data['described_entity']['resource_creator'][0]['surname']['en']}, "
            f"{self.corpus_data['described_entity']['resource_creator'][0]['given_name']['en']}"
        )
        self.assertEqual(
            self.datacite_corpus_data['creators'][0]['nameType'],
            'Personal'
        )
        self.assertEqual(
            self.datacite_corpus_data['creators'][0]['givenName'],
            self.corpus_data['described_entity']['resource_creator'][0]['given_name']['en']
        )
        self.assertEqual(
            self.datacite_corpus_data['creators'][0]['familyName'],
            self.corpus_data['described_entity']['resource_creator'][0]['surname']['en']
        )
        self.assertEqual(
            self.datacite_corpus_data['creators'][0]['nameIdentifiers'][0]['nameIdentifier'],
            self.corpus_data['described_entity']['resource_creator'][0]['personal_identifier'][0]['value']
        )
        self.assertEqual(
            self.datacite_corpus_data['creators'][0]['nameIdentifiers'][0]['nameIdentifier'],
            self.corpus_data['described_entity']['resource_creator'][0]['personal_identifier'][0]['value']
        )
        self.assertEqual(
            self.datacite_corpus_data['creators'][0]['nameIdentifiers'][0]['nameIdentifierScheme'],
            choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES[
                self.corpus_data['described_entity']['resource_creator'][0]['personal_identifier'][0]['personal_identifier_scheme']
            ]
        )
        self.assertEqual(
            self.datacite_corpus_data['creators'][0]['nameIdentifiers'][0]['schemeURI'],
            self.corpus_data['described_entity']['resource_creator'][0]['personal_identifier'][0]['personal_identifier_scheme']
        )

    def test_version(self):
        self.assertEqual(
            self.datacite_corpus_data['version'],
            self.corpus_data['described_entity']['version']
        )

    def test_dates(self):
        # dates == creationEndDate, versionDate
        self.assertEqual(
            self.datacite_corpus_data['dates'][0]['date'],
            self.corpus_data['described_entity']['creation_end_date'].replace('-', '/')
        )
        self.assertEqual(
            self.datacite_corpus_data['dates'][0]['dateType'],
            'Created'
        )
        self.assertEqual(
            self.datacite_corpus_data['dates'][1]['date'],
            self.corpus_data['described_entity']['version_date'].replace('-', '/')
        )
        self.assertEqual(
            self.datacite_corpus_data['dates'][1]['dateType'],
            'Updated'
        )
        self.assertEqual(
            self.datacite_corpus_data['dates'][1]['dateInformation'],
            self.corpus_data['described_entity']['version']
        )

    def test_rights_list(self):
        # for Corpus
        self.assertEqual(
            self.datacite_corpus_data['rightsList'][0]['rights'],
            self.corpus_data['described_entity']['lr_subclass']['dataset_distribution'][0]['licence_terms'][0]['licence_terms_name']['en']
        )
        self.assertEqual(
            self.datacite_corpus_data['rightsList'][0]['rightsUri'],
            self.corpus_data['described_entity']['lr_subclass']['dataset_distribution'][0]['licence_terms'][0][
                'licence_terms_url'][0]
        )

        # for Tool/Service
        self.assertEqual(
            self.datacite_tool_data['rightsList'][0]['rights'],
            self.tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0][
                'licence_terms_name']['en']
        )
        self.assertEqual(
            self.datacite_tool_data['rightsList'][0]['rightsUri'],
            self.tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0][
                'licence_terms_url'][0]
        )

    def test_contributors(self):
        # contributors == contact, ipr_holer, distribution_rights_holder
        for c in self.datacite_corpus_data['contributors']:
            if c['contributorType'] == 'ContactPerson':
                if c['nameType'] == 'Personal':
                    given_name = self.corpus_data['described_entity']['contact'][0]['given_name']['en']
                    surname = self.corpus_data['described_entity']['contact'][0]['surname']['en']
                    personal_identifier = self.corpus_data['described_entity']['contact'][0]['personal_identifier'][0]
                    self.assertEqual(
                        c['name'],
                        f"{surname}, {given_name}"
                    )
                    self.assertEqual(
                        c['givenName'],
                        given_name
                    )
                    self.assertEqual(
                        c['familyName'],
                        surname
                    )
                    self.assertEqual(
                        c['nameIdentifiers'][0]['nameIdentifierScheme'],
                        choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES[personal_identifier['personal_identifier_scheme']]
                    )
                    self.assertEqual(
                        c['nameIdentifiers'][0]['schemeURI'],
                        personal_identifier['personal_identifier_scheme']
                    )
                    self.assertEqual(
                        c['nameIdentifiers'][0]['nameIdentifier'],
                        personal_identifier['value']
                    )
                if c['nameType'] == 'Organizational':
                    organization_identifier = self.corpus_data['described_entity']['contact'][1]['organization_identifier'][0]
                    self.assertEqual(
                        c['name'],
                        self.corpus_data['described_entity']['contact'][1]['organization_name']['en']
                    )
                    self.assertEqual(
                        c['nameIdentifiers'][0]['nameIdentifierScheme'],
                        choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES[organization_identifier['organization_identifier_scheme']]
                    )
                    self.assertEqual(
                        c['nameIdentifiers'][0]['schemeURI'],
                        organization_identifier['organization_identifier_scheme']
                    )
                    self.assertEqual(
                        c['nameIdentifiers'][0]['nameIdentifier'],
                        organization_identifier['value']
                    )
            if c['contributorType'] == 'RightsHolder':
                group_identifier = self.corpus_data['described_entity']['ipr_holder'][0]['group_identifier'][0]
                self.assertEqual(
                    c['name'],
                    self.corpus_data['described_entity']['ipr_holder'][0]['organization_name']['en']
                )
                self.assertEqual(
                    c['nameIdentifiers'][0]['nameIdentifierScheme'],
                    choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES[
                        group_identifier['organization_identifier_scheme']]
                )
                self.assertEqual(
                    c['nameIdentifiers'][0]['schemeURI'],
                    group_identifier['organization_identifier_scheme']
                )
                self.assertEqual(
                    c['nameIdentifiers'][0]['nameIdentifier'],
                    group_identifier['value']
                )
            if c['contributorType'] in ['HostingInstitution', 'Distributor']:
                self.assertEqual(
                    c['name'],
                    'ELG'
                )
                self.assertEqual(
                    c['nameType'],
                    'Organizational'
                )

    def test_identifiers(self):
        self.assertEqual(
            self.datacite_corpus_data['identifiers'][0]['identifierType'],
            'DOI'
        )
        self.assertEqual(
            self.datacite_corpus_data['identifiers'][0]['identifier'],
            settings.DATACITE_PREFIX
        )

        # a DOI and the URL alternateIdentifiers added
        self.assertEqual(
            len(self.datacite_corpus_data['identifiers']), 2
        )

    def test_types(self):
        # For corpus
        self.assertEqual(
            self.datacite_corpus_data['types']['resourceTypeGeneral'],
            'Dataset'
        )
        self.assertIn(
            'Corpus',
            self.datacite_corpus_data['types']['resourceType']
        )
        self.assertIn(
            'Numerical text',
            self.datacite_corpus_data['types']['resourceType']
        )
        self.assertIn(
            'Sound',
            self.datacite_corpus_data['types']['resourceType']
        )
        self.assertIn(
            'Image',
            self.datacite_corpus_data['types']['resourceType']
        )
        self.assertIn(
            'Text',
            self.datacite_corpus_data['types']['resourceType']
        )
        self.assertIn(
            'Audiovisual',
            self.datacite_corpus_data['types']['resourceType']
        )

        # For tool/service
        self.assertEqual(
            self.datacite_tool_data['types']['resourceTypeGeneral'],
            'Software'
        )
        self.assertEqual(
            self.datacite_tool_data['types']['resourceType'],
            'Software'
        )

    def test_relation_identifiers(self):
        for r in self.datacite_tool_data['relatedIdentifiers']:
            if r['relationType'] == 'IsCitedBy':
                # is cited by
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[self.tool_data['described_entity']['is_cited_by'][0]['document_identifier'][0]['document_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['is_cited_by'][0]['document_identifier'][0]['value']
                )
            if r['relationType'] == 'IsDescribedBy':
                # is described by
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[self.tool_data['described_entity']['is_described_by'][0][
                        'document_identifier'][0]['document_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['is_described_by'][0]['document_identifier'][0]['value']
                )
            if r['relationType'] == 'IsDocumentedBy':
                # is documented by
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[
                        self.tool_data['described_entity']['is_documented_by'][0]['document_identifier'][0][
                            'document_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['is_documented_by'][0]['document_identifier'][0][
                            'value']
                )
            if r['relationType'] == 'IsReviewedBy':
                # is reviewed by
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[
                        self.tool_data['described_entity']['is_reviewed_by'][0]['document_identifier'][0][
                            'document_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['is_reviewed_by'][0]['document_identifier'][0][
                            'value']
                )
            if r['relationType'] == 'Continues':
                # continues
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[
                        self.tool_data['described_entity']['is_continuation_of'][0]['lr_identifier'][0][
                            'lr_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['is_continuation_of'][0]['lr_identifier'][0][
                            'value']
                )
            if r['relationType'] == 'IsVersionOf':
                # is version of
                self.assertEqual(
                        r['relatedIdentifierType'],
                        DATACITE_IDENTIFIER_CHOICES[
                            self.tool_data['described_entity']['is_version_of']['lr_identifier'][0][
                                'lr_identifier_scheme']]
                    )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['is_version_of']['lr_identifier'][0][
                            'value']
                )
            if r['relationType'] == 'IsNewVersionOf':
                # replaces
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[
                        self.tool_data['described_entity']['replaces'][0]['lr_identifier'][0][
                            'lr_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['replaces'][0]['lr_identifier'][0][
                            'value']
                )
            if r['relationType'] == 'IsPartOf':
                # is_part_of
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[
                        self.tool_data['described_entity']['is_part_of'][0]['lr_identifier'][0][
                            'lr_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['is_part_of'][0]['lr_identifier'][0][
                            'value']
                )
            if r['relationType'] == 'IsIdenticalTo':
                # is_exact_match_with
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[
                        self.tool_data['described_entity']['is_exact_match_with'][0]['lr_identifier'][0][
                            'lr_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['is_exact_match_with'][0]['lr_identifier'][0][
                            'value']
                )
            if r['relationType'] == 'IsSourceOf':
                # has_outcome
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[
                        self.tool_data['described_entity']['actual_use'][0]['has_outcome'][0]['lr_identifier'][0][
                            'lr_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['actual_use'][0]['has_outcome'][0]['lr_identifier'][0][
                            'value']
                )
            if r['relationType'] == 'Requires':
                # requires LR
                self.assertEqual(
                    r['relatedIdentifierType'],
                    DATACITE_IDENTIFIER_CHOICES[
                        self.tool_data['described_entity']['lr_subclass']['requires_lr'][0]['lr_identifier'][0][
                            'lr_identifier_scheme']]
                )
                self.assertEqual(
                    r['relatedIdentifier'],
                    self.tool_data['described_entity']['lr_subclass']['requires_lr'][0]['lr_identifier'][0][
                            'value']
                )

    def test_publisher(self):
        # has only person resource_provider, thus ELG
        self.assertEqual(
            self.datacite_tool_data['publisher'],
            'ELG'
        )
        # publisher == resource_provider 2 orgs
        provider_1 = self.corpus_data['described_entity']['resource_provider'][1]['organization_name']['en']
        provider_2 = self.corpus_data['described_entity']['resource_provider'][2]['organization_name']['en']

        self.assertEqual(
            self.datacite_corpus_data['publisher'],
            f'{provider_1} and {provider_2}'
        )
