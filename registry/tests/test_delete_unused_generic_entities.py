import json
import logging

from django.core.exceptions import ObjectDoesNotExist

from registry import serializers
from registry.models import Document, Repository
from test_utils import SerializerTestCase
from io import StringIO

from django.core.management import call_command

from registry.management.commands.delete_unused_generic_entities import Command

LOGGER = logging.getLogger(__name__)

class TestFindUnusedDescribedEntities(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.serializer = serializers.MetadataRecordSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)
        LOGGER.info("Set up has finished")

    def test_find_unused_generic_lr(self):
        json_gde = {
            "resource_name" : { "en": "Generic language resource unused" },
        }
        LOGGER.info("Create generic lr")
        serializer_gde = serializers.GenericLanguageResourceSerializer(data=json_gde)
        self.assertTrue(serializer_gde.is_valid())
        instance_gde = serializer_gde.save()
        pk = instance_gde.pk
        LOGGER.info("Trigger to find unused described entities")
        result = Command().find_unused_described_entities()
        LOGGER.info(f"{result}")
        self.assertTrue(pk in result)

    def test_find_unused_generic_licence_terms(self):
        json_gde = {
            'licence_terms_name' : { 'en': 'Generic unused licence term' },
            'licence_terms_url' : ['http://www.licenceTermsURL.com'],
            'condition_of_use': [
                'http://w3id.org/meta-share/meta-share/attribution'
            ]
        }
        LOGGER.info("Create generic lr")
        serializer_gde = serializers.GenericLicenceTermsSerializer(data=json_gde)
        self.assertTrue(serializer_gde.is_valid())
        instance_gde = serializer_gde.save()
        pk = instance_gde.pk
        LOGGER.info("Trigger to find unused described entities")
        result = Command().find_unused_described_entities()
        LOGGER.info(f"{result}")
        self.assertTrue(pk in result)
        self.assertEqual(len(result), 1)

    def test_find_unused_generic_project(self):
        json_gde = {
            'project_name' : { 'en': 'Generic Unused Project' }
        }
        LOGGER.info("Create generic lr")
        serializer_gde = serializers.GenericProjectSerializer(data=json_gde)
        self.assertTrue(serializer_gde.is_valid())
        instance_gde = serializer_gde.save()
        pk = instance_gde.pk
        LOGGER.info("Trigger to find unused described entities")
        result = Command().find_unused_described_entities()
        LOGGER.info(f"{result}")
        self.assertTrue(pk in result)
        self.assertEqual(len(result), 1)

    def test_find_unused_generic_organization(self):
        json_gde = {
            'organization_name' : { 'en': 'Generic Unused Oranization' }

        }
        LOGGER.info("Create generic lr")
        serializer_gde = serializers.GenericOrganizationSerializer(data=json_gde)
        self.assertTrue(serializer_gde.is_valid())
        instance_gde = serializer_gde.save()
        pk = instance_gde.pk
        LOGGER.info("Trigger to find unused described entities")
        result = Command().find_unused_described_entities()
        LOGGER.info(f"{result}")
        self.assertTrue(pk in result)
        self.assertEqual(len(result), 1)

    def test_find_unused_generic_organization(self):
        json_gde = {
            'organization_name' : { 'en': 'Generic Unused Oranization' }

        }
        LOGGER.info("Create generic lr")
        serializer_gde = serializers.GenericOrganizationSerializer(data=json_gde)
        self.assertTrue(serializer_gde.is_valid())
        instance_gde = serializer_gde.save()
        pk = instance_gde.pk
        LOGGER.info("Trigger to find unused described entities")
        result = Command().find_unused_described_entities()
        LOGGER.info(f"{result}")
        self.assertTrue(pk in result)
        self.assertEqual(len(result), 1)

    def test_find_unused_generic_person(self):
        json_gde = {
            'surname' : { 'en': 'Generic Unused Person Surname' },
            'given_name': { 'en': 'Generic Unused Person Surname' },
        }
        LOGGER.info("Create generic lr")
        serializer_gde = serializers.GenericPersonSerializer(data=json_gde)
        self.assertTrue(serializer_gde.is_valid())
        instance_gde = serializer_gde.save()
        pk = instance_gde.pk
        LOGGER.info("Trigger to find unused described entities")
        result = Command().find_unused_described_entities()
        LOGGER.info(f"{result}")
        self.assertTrue(pk in result)
        self.assertEqual(len(result), 1)

    def test_find_unused_generic_group(self):
        json_gde = {
            'organization_name' : { 'en': 'Generic Unused Group' }
        }
        LOGGER.info("Create generic lr")
        serializer_gde = serializers.GenericGroupSerializer(data=json_gde)
        self.assertTrue(serializer_gde.is_valid())
        instance_gde = serializer_gde.save()
        pk = instance_gde.pk
        LOGGER.info("Trigger to find unused described entities")
        result = Command().find_unused_described_entities()
        LOGGER.info(f"{result}")
        self.assertTrue(pk in result)
        self.assertEqual(len(result), 1)

    def test_find_unused_generic_repository(self):
        json_gde = {
            'repository_name' : { 'en': 'Generic Unused Repository' }
        }
        LOGGER.info("Create generic lr")
        serializer_gde = serializers.GenericRepositorySerializer(data=json_gde)
        self.assertTrue(serializer_gde.is_valid())
        instance_gde = serializer_gde.save()
        pk = instance_gde.pk
        LOGGER.info("Trigger to find unused described entities")
        result = Command().find_unused_described_entities()
        LOGGER.info(f"{result}")
        self.assertTrue(pk in result)
        self.assertEqual(len(result), 1)

    def test_find_unused_generic_document(self):
        json_gde = {
            'title': { 'en': 'Generic Unused Document' }
        }
        LOGGER.info("Create generic lr")
        serializer_gde = serializers.GenericDocumentSerializer(data=json_gde)
        self.assertTrue(serializer_gde.is_valid())
        instance_gde = serializer_gde.save()
        pk = instance_gde.pk
        LOGGER.info("Trigger to find unused described entities")
        result = Command().find_unused_described_entities()
        LOGGER.info(f"{result}")
        self.assertTrue(pk in result)
        self.assertEqual(len(result), 1)




class TestCommandDeleteUnusedGenericDescribedEntities(SerializerTestCase):

     def call_command(self, *args, **kwargs):
         call_command(
             "delete_unused_generic_entities",
             *args,
             stdout=StringIO(),
             stderr=StringIO(),
             **kwargs,
         )

     def test_dry_run(self):
         json_gde = {
             'title': {'en': 'Generic Unused Document'}
         }
         LOGGER.info("Create generic lr")
         serializer_gde = serializers.GenericDocumentSerializer(data=json_gde)
         self.assertTrue(serializer_gde.is_valid())
         instance_gde = serializer_gde.save()
         pk = instance_gde.pk

         self.call_command()

         try:
             instance_gde = Document.objects.get(pk=pk)
         except ObjectDoesNotExist:
             instance_gde = None
         self.assertTrue(instance_gde is not None)

     def test_write(self):
         json_gde = {
             'repository_name': {'en': 'Generic Unused Repository'}
         }
         LOGGER.info("Create generic lr")
         serializer_gde = serializers.GenericRepositorySerializer(data=json_gde)
         self.assertTrue(serializer_gde.is_valid())
         instance_gde = serializer_gde.save()
         pk = instance_gde.pk

         self.call_command("--write")

         try:
             instance_gde = Repository.objects.get(pk=pk)
         except ObjectDoesNotExist:
             instance_gde = None
         self.assertTrue(instance_gde is None)
