import copy
import json
import logging
from unittest import skip

import xmltodict
from django.conf import settings
from django.contrib.admin.sites import AdminSite
from django.contrib.auth import get_user_model
from django.contrib.messages.api import get_messages
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import IntegrityError

from management.models import Manager
from processing.models import RegisteredLTService
from processing.serializers import RegisteredLTServiceSerializer
from registry.admin import (
    StatusFilter, MetadataRecordAdmin, TypeFilter,
    FunctionalServiceFilter
)
from registry.models import MetadataRecord, SoftwareDistribution
from registry.serializers import MetadataRecordSerializer
from test_utils import (
    import_metadata, get_test_user, EndpointTestCase
)

LOGGER = logging.getLogger(__name__)

ADMIN_ENDPOINT = f'/{settings.HOME_PREFIX}/admin/'


class TestMetadataRecordAdmin(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json', 'registry/tests/fixtures/tool.json',
                 'registry/tests/fixtures/organization.json', 'registry/tests/fixtures/document.json',
                 'registry/tests/fixtures/person.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.user, cls.token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.service_stats = {
            'user': 'preferred_username',
            'start_at': '2020-01-01',
            'end_at': '2020-02-02',
            'lt_service': 'x0f2143120x',
            'bytes': 12,
            'elg_resource': None,
            'status': 'succeeded',
            'call_type': 'PermissionsRequest'
        }

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()
    
    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_mdr_admin_model_get_str(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertEquals(mdr_admin.get_str(self.metadata_records[1]), 'ANNIE English Named Entity Recognizer')

    def test_mdr_admin_model_get_type_for_lr(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertEquals(mdr_admin.get_type(self.metadata_records[1]), 'ToolService')

    def test_mdr_admin_model_get_type_not_lr(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertEquals(mdr_admin.get_type(self.metadata_records[0]), 'Project')

    def test_mdr_admin_model_lt_service_link(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertTrue(mdr_admin.lt_service_link(self.metadata_records[1]))

    def test_mdr_admin_model_get_status(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertEquals(mdr_admin.get_status(self.metadata_records[1]), 'INT')

    def test_mdr_admin_model_is_functional_service(self):
        self.metadata_records[1].management_object.functional_service = True
        self.metadata_records[1].management_object.save()
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertTrue(mdr_admin.is_functional_service(self.metadata_records[1]))
        self.metadata_records[1].management_object.functional_service = False
        self.metadata_records[1].management_object.save()

    def test_mdr_admin_model_is_functional_service_exception_handling(self):
        mdr_instance = copy.deepcopy(self.metadata_records[0])
        mdr_admin_false = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertFalse(mdr_admin_false.is_functional_service(mdr_instance))

    def test_mdr_admin_model_review_status(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertEquals(mdr_admin.review_status(self.metadata_records[1]), 'PENDING')

    def test_mdr_admin_model_review_status_exception_handling(self):
        mdr_instance = copy.deepcopy(self.metadata_records[2])

        ltd_data = {
            'metadata_record': mdr_instance.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        serializer = RegisteredLTServiceSerializer(data=ltd_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        mdr_instance.delete()
        mdr_admin_false = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertFalse(mdr_admin_false.review_status(mdr_instance))

    def test_mdr_admin_md_curator(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        self.assertEquals(mdr_admin.md_curator(self.metadata_records[1]),
                          f"{self.metadata_records[1].management_object.curator.username}")

    def test_internalize_action_when_ingested(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[0]
        instance.management_object.status = 'g'
        instance.management_object.save()
        queryset = MetadataRecord.objects.filter(pk=instance.pk)
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.internalize_action(response.wsgi_request, queryset)
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'i')

    def test_internalize_action_when_unpublished(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[0]
        instance.management_object.status = 'u'
        instance.management_object.save()
        queryset = MetadataRecord.objects.filter(pk=instance.pk)
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.internalize_action(response.wsgi_request, queryset)
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'i')

    def test_ingest_action_when_non_tool_service(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[0]
        queryset = MetadataRecord.objects.filter(pk=instance.pk)
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.ingest_action(response.wsgi_request, queryset)
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'g')
        instance.management_object.status = 'i'
        instance.management_object.save()

    def test_ingest_action_when_functional_tool_service(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[2]
        instance.management_object.functional_service = True
        instance.management_object.save()
        distributions = instance.described_entity.lr_subclass.software_distribution.all()
        for dist in distributions:
            if dist.software_distribution_form == 'http://w3id.org/meta-share/meta-share/dockerImage' \
                    and dist.execution_location:
                dist.elg_compatible_distribution = True
                dist.save()
                break
        queryset = MetadataRecord.objects.filter(pk=instance.pk)
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.ingest_action(response.wsgi_request, queryset)
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'g')
        self.assertTrue(instance.management_object.functional_service)
        try:
            lt_service = RegisteredLTService.objects.get(pk=instance_after.lt_service.pk)
            self.assertTrue(lt_service)
        except ObjectDoesNotExist:
            self.assertTrue(False)
        instance.management_object.status = 'i'
        instance.management_object.functional_service = False
        instance.management_object.save()

    @skip('works standalone')
    def test_ingest_action_when_functional_tool_service_fails_with_no_elg_comp_dist(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[2]
        instance.management_object.status = 'i'
        instance.management_object.functional_service = True
        instance.management_object.save()
        queryset = MetadataRecord.objects.filter(pk=instance.pk)
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.ingest_action(response.wsgi_request, queryset)
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'i')
        self.assertTrue(instance.management_object.functional_service)
        try:
            lt_service = RegisteredLTService.objects.get(pk=instance_after.lt_service.pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)
        instance.management_object.status = 'i'
        instance.management_object.functional_service = False
        instance.management_object.save()

    @skip('works standalone')
    def test_ingest_action_when_functional_tool_service_fails_with_more_than_one_elg_comp_dist(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[2]
        instance.management_object.status = 'i'
        instance.management_object.functional_service = True
        instance.management_object.save()
        distributions = instance.described_entity.lr_subclass.software_distribution
        extra_dist = SoftwareDistribution.objects.create(
            software_distribution_form='http://w3id.org/meta-share/meta-share/dockerImage',
            execution_location="https://registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:0-0-43",
            elg_compatible_distribution=True)
        for dist in distributions.all():
            if dist.software_distribution_form == 'http://w3id.org/meta-share/meta-share/dockerImage' \
                    and dist.execution_location:
                dist.elg_compatible_distribution = True
                dist.save()
        distributions.add(extra_dist)
        queryset = MetadataRecord.objects.filter(pk=instance.pk)
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.ingest_action(response.wsgi_request, queryset)
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'i')
        self.assertTrue(instance.management_object.functional_service)
        try:
            lt_service = RegisteredLTService.objects.get(pk=instance_after.lt_service.pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)
        instance.management_object.status = 'i'
        instance.management_object.functional_service = False
        instance.management_object.save()

    @skip('works standalone')
    def test_ingest_action_when_functional_tool_service_fails_with_no_exec_loc(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[2]
        instance.management_object.status = 'i'
        instance.management_object.functional_service = True
        instance.management_object.save()
        distributions = instance.described_entity.lr_subclass.software_distribution
        for dist in distributions.all():
            if dist.software_distribution_form == 'http://w3id.org/meta-share/meta-share/dockerImage' \
                    and dist.execution_location:
                dist.elg_compatible_distribution = True
                dist.execution_location = ''
                dist.save()
        queryset = MetadataRecord.objects.filter(pk=instance.pk)
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.ingest_action(response.wsgi_request, queryset)
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'i')
        self.assertTrue(instance.management_object.functional_service)
        try:
            lt_service = RegisteredLTService.objects.get(pk=instance_after.lt_service.pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)
        instance.management_object.status = 'i'
        instance.management_object.functional_service = False
        instance.management_object.save()

    def test_ingest_action_when_functional_tool_service_and_under_construction(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[2]
        instance.management_object.functional_service = True
        instance.management_object.under_construction = True
        instance.management_object.save()
        queryset = MetadataRecord.objects.filter(pk=instance.pk)
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.ingest_action(response.wsgi_request, queryset)
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'g')
        self.assertTrue(instance.management_object.functional_service)
        self.assertTrue(instance.management_object.under_construction)
        try:
            lt_service = RegisteredLTService.objects.get(pk=instance_after.lt_service.pk)
            self.assertTrue(lt_service)
        except ObjectDoesNotExist:
            self.assertTrue(True)
        instance.management_object.status = 'i'
        instance.management_object.functional_service = False
        instance.management_object.save()

    def test_ingest_action_when_non_functional_tool_service(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[2]
        instance.management_object.functional_service = False
        instance.management_object.save()
        queryset = MetadataRecord.objects.filter(pk=instance.pk)
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.ingest_action(response.wsgi_request, queryset)
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'g')
        self.assertFalse(instance.management_object.functional_service)
        try:
            lt_service = RegisteredLTService.objects.get(pk=instance_after.lt_service.pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)
        instance.management_object.status = 'i'
        instance.management_object.save()

    def test_publish_action_when_tool_service_not_completed(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[1]
        instance.management_object.functional_service = True
        instance.management_object.ingest()
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.publish_action(response.wsgi_request, MetadataRecord.objects.all())
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertEquals(instance_after.management_object.status, 'g')
        instance.management_object.functional_service = False
        instance.management_object.save()

    def test_publish_action_when_tool_service_completed(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        test_data = ['registry/tests/fixtures/tool_version.json']
        raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            raw_instance = serializer.save()
            manager = Manager.objects.get(id=raw_instance.management_object.id)
            try:
                manager.curator = self.user
            except get_user_model().DoesNotExist:
                manager.curator = self.user
            manager.save()
            raw_instance.management_object = manager
            raw_instance.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        not_approved_ltd_data = {
            'metadata_record': raw_instance.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120asdx'
        }
        serializer = RegisteredLTServiceSerializer(data=not_approved_ltd_data)
        if serializer.is_valid():
            instance = serializer.save()
            lt_raw_data = copy.deepcopy(not_approved_ltd_data)
            lt_raw_data['elg_execution_location'] = 'http://www.executionlocation.com/differentlink'
            lt_raw_data['status'] = 'COMPLETED'
            lt_raw_data['elg_hosted'] = True
            update_serializer = RegisteredLTServiceSerializer(instance, data=lt_raw_data)
            try:
                update_serializer.is_valid()
                update_instance = update_serializer.save()
            except ValidationError:
                LOGGER.info(ValidationError)
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        response = self.client.post(ADMIN_ENDPOINT)
        raw_instance.management_object.ingest()
        mdr_admin.publish_action(response.wsgi_request, MetadataRecord.objects.filter(id=raw_instance.id))
        new_instance = MetadataRecord.objects.get(id=raw_instance.id)
        self.assertTrue(new_instance.management_object.status == 'p')

    def test_publish_action_when_not_tool_service(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[3]
        instance.management_object.ingest()
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.publish_action(response.wsgi_request, MetadataRecord.objects.all())
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'p')

    def test_publish_action_when_unpublished(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[3]
        instance.management_object.status = 'u'
        instance.management_object.save()
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.publish_action(response.wsgi_request, MetadataRecord.objects.all())
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'p')
        instance.management_object.status = 'i'
        instance.management_object.save()

    def test_publish_action_when_all_pass(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        instance = self.metadata_records[5]
        instance.management_object.ingest()
        response = self.client.post(ADMIN_ENDPOINT)
        queryset = MetadataRecord.objects.filter(id=instance.id)
        mdr_admin.publish_action(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message,
                          '1/1 metadata records have been successfully published')
        instance_after = MetadataRecord.objects.get(id=instance.pk)
        self.assertTrue(instance_after.management_object.status == 'p')

    def test_unpublish_action_cancel(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        request_data = {'cancel': True}
        response = self.client.post(ADMIN_ENDPOINT, data=request_data)
        mdr_admin.unpublish_action(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message, 'Action cancelled.')

    def test_mark_as_deleted_action_cancel(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        request_data = {'cancel': True}
        response = self.client.post(ADMIN_ENDPOINT, data=request_data)
        mdr_admin.mark_as_deleted_action(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message, 'Action cancelled.')

    def test_restore_action_deleted(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        instance = queryset.last()
        instance.management_object.deleted = True
        instance.management_object.save()
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.restore_action(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message,
                          f'Successfully restored 1 of the selected resources. '
                          f'{queryset.count() - 1} records failed due to deletion status. '
                             f'Only deleted records can be restored.')
        instance_after = MetadataRecord.objects.all().last()
        self.assertFalse(instance_after.management_object.deleted)

    @skip('works in transaction context')
    def test_register_service_action_fails_when_already_registered_or_not_an_lr(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        response = self.client.post(ADMIN_ENDPOINT)
        self.assertRaises(IntegrityError,
                                  mdr_admin.register_service_action(response.wsgi_request, MetadataRecord.objects.all()))

    @skip('works in transaction context')
    def test_register_service_action_fails_for_registered_or_not_an_lr_but_passes_for_not_registered_with_loc(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        xml_data = xmltodict.parse(xml_input, xml_attribs=True)['ms:MetadataRecord']
        serializer = MetadataRecordSerializer(xml_data=xml_data)
        serializer.initial_data['described_entity']['lr_subclass']['software_distribution'][0]['execution_location'] \
            = 'http://www.executionlocation.com'
        if serializer.is_valid():
            xml_instance = serializer.save()
            manager = Manager.objects.get(id=xml_instance.management_object.id)
            try:
                manager.curator = self.user
            except get_user_model().DoesNotExist:
                manager.curator = self.user
            manager.save()
            xml_instance.management_object = manager
            xml_instance.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        queryset = [self.metadata_records[1], self.metadata_records[0], xml_instance]
        response = self.client.post(ADMIN_ENDPOINT)
        self.assertRaises(IntegrityError,
                                  mdr_admin.register_service_action(response.wsgi_request, queryset))

    def test_register_service_action_tool_no_exec_location(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        test_data = ['registry/tests/fixtures/tool_version.json']
        raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
            manager = Manager.objects.get(id=instance.management_object.id)
            try:
                manager.curator = self.user
            except get_user_model().DoesNotExist:
                manager.curator = self.user
            manager.save()
            instance.management_object = manager
            instance.save()
            distributions = instance.described_entity.lr_subclass.software_distribution.all()
            for dist in distributions:
                if dist.software_distribution_form == 'http://w3id.org/meta-share/meta-share/dockerImage' \
                        and dist.execution_location:
                    dist.execution_location = ''
                    dist.save()
                    break
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        response = self.client.post(ADMIN_ENDPOINT)
        queryset = [instance]
        mdr_admin.register_service_action(response.wsgi_request, queryset)
        service = RegisteredLTService.objects.all().first()
        self.assertTrue(service)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message,
                          '[ToolService] ANNIE 2 English Named Entity Recognizer does not have a distribution tagged as elg compatible.')

    def test_register_service_action_tool_with_exec_location(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        test_data = ['registry/tests/fixtures/tool_version.json']
        raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
        serializer = MetadataRecordSerializer(data=raw_data)
        serializer.initial_data['described_entity']['lr_subclass']['software_distribution'][0]['execution_location'] \
            = 'http://www.executionlocation.com'
        if serializer.is_valid():
            instance = serializer.save()
            manager = Manager.objects.get(id=instance.management_object.id)
            try:
                manager.curator = self.user
            except get_user_model().DoesNotExist:
                manager.curator = self.user
            manager.save()
            instance.management_object = manager
            instance.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        response = self.client.post(ADMIN_ENDPOINT)
        queryset = [instance]
        mdr_admin.register_service_action(response.wsgi_request, queryset)
        service = RegisteredLTService.objects.all().first()
        self.assertTrue(service)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertTrue(all_messages[0].message,
                        'Succesfully registered [ToolService] Travel English grammar')

    def test_register_service_action_tool_lr_but_not_tool_service(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        test_data = ['registry/tests/fixtures/lcr.json']
        lcr_metadata_record = import_metadata(test_data)

        response = self.client.post(ADMIN_ENDPOINT)
        instance = lcr_metadata_record[0]
        queryset = [instance]
        mdr_admin.register_service_action(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message, f'{instance} has been bypassed because it is not a Tool/Service')

    def test_get_search_results_no_matches_found(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        response = self.client.post(ADMIN_ENDPOINT)
        self.assertFalse(mdr_admin.get_search_results(response.wsgi_request, queryset, 'random words')[0])

    def test_get_search_results_matches_found(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        response = self.client.post(ADMIN_ENDPOINT)
        self.assertTrue(self.metadata_records[1]
                        in mdr_admin.get_search_results(response.wsgi_request, queryset, 'Recognizer')[0])

    def test_get_search_results_no_query(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        response = self.client.post(ADMIN_ENDPOINT)
        self.assertTrue(mdr_admin.get_search_results(response.wsgi_request, queryset, ''))

    def test_resolve_claim_cancel(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        request_data = {'cancel': True}
        response = self.client.post(ADMIN_ENDPOINT, data=request_data)
        mdr_admin.resolve_claim(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message, 'Action cancelled.')

    def test_resolve_claim_not_claimed(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        response = self.client.post(ADMIN_ENDPOINT)
        mdr_admin.resolve_claim(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message, 'Records have not been claimed.')

    def test_assign_legal_validator_cancel(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        request_data = {'cancel': True}
        response = self.client.post(ADMIN_ENDPOINT, data=request_data)
        mdr_admin.assign_legal_validator(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message, 'Action cancelled.')

    def test_assign_metadata_validator_cancel(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        request_data = {'cancel': True}
        response = self.client.post(ADMIN_ENDPOINT, data=request_data)
        mdr_admin.assign_metadata_validator(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message, 'Action cancelled.')

    def test_assign_technical_validator_cancel(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        request_data = {'cancel': True}
        response = self.client.post(ADMIN_ENDPOINT, data=request_data)
        mdr_admin.assign_technical_validator(response.wsgi_request, queryset)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertEquals(all_messages[0].message, 'Action cancelled.')


class TestStatusFilter(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.user, cls.token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.service_stats = {
            'user': 'preferred_username',
            'start_at': '2020-01-01',
            'end_at': '2020-02-02',
            'lt_service': 'x0f2143120x',
            'bytes': 12,
            'elg_resource': None,
            'status': 'succeeded',
            'call_type': 'PermissionsRequest'
        }

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()
    
    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_lookups(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        response = self.client.post(ADMIN_ENDPOINT)
        status = [
            ('p', 'Published'),
            ('u', 'Unpublished'),
            ('g', 'Ingested'),
            ('i', 'Internal'),
            ('d', 'Draft')
        ]
        status_filter = StatusFilter(request=response.wsgi_request,
                                     params=status,
                                     model=MetadataRecord,
                                     model_admin=mdr_admin)
        lookup_list = status_filter.lookups(response.wsgi_request, mdr_admin)
        self.assertTrue(lookup_list == [
            ('p', 'Published'),
            ('u', 'Unpublished'),
            ('g', 'Ingested'),
            ('i', 'Internal'),
            ('d', 'Draft')
        ])

    def test_queryset(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        response = self.client.post(ADMIN_ENDPOINT)
        status = [
            ('p', 'Published'),
            ('u', 'Unpublished'),
            ('g', 'Ingested'),
            ('i', 'Internal'),
            ('d', 'Draft')
        ]
        status_filter = StatusFilter(request=response.wsgi_request,
                                     params=status,
                                     model=MetadataRecord,
                                     model_admin=mdr_admin)
        queryset = status_filter.queryset(response.wsgi_request, MetadataRecord.objects.all())
        
        self.assertTrue(queryset)

    def test_queryset_with_value(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        value_dict = {'status': 'p'}
        response = self.client.post(ADMIN_ENDPOINT, data=value_dict)
        status_filter = StatusFilter(request=response.wsgi_request,
                                     params=value_dict,
                                     model=MetadataRecord,
                                     model_admin=mdr_admin)
        queryset = status_filter.queryset(response.wsgi_request, MetadataRecord.objects.all())
        self.assertFalse(queryset)


class TestTypeFilter(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.user, cls.token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.service_stats = {
            'user': 'preferred_username',
            'start_at': '2020-01-01',
            'end_at': '2020-02-02',
            'lt_service': 'x0f2143120x',
            'bytes': 12,
            'elg_resource': None,
            'status': 'succeeded',
            'call_type': 'PermissionsRequest'
        }

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()
    
    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_lookups(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        response = self.client.post(ADMIN_ENDPOINT)
        entity_type = [
            ('Project', 'Project'),
            ('Organization', 'Organization'),
            ('LicenceTerms', 'Licence'),
            ('Person', 'Person'),
            ('Group', 'Group'),
            ('Document', 'Document'),
            ('ToolService', 'Tool/Service'),
            ('Corpus', 'Corpus'),
            ('LexicalConceptualResource', 'Lexical Conceptual Resource'),
            ('LanguageDescription', 'Language Description'),
            ('Repository', 'Repository')
        ]
        type_filter = TypeFilter(request=response.wsgi_request,
                                 params=entity_type,
                                 model=MetadataRecord,
                                 model_admin=mdr_admin)
        lookup_list = type_filter.lookups(response.wsgi_request, mdr_admin)
        self.assertTrue(lookup_list == [
            ('Project', 'Project'),
            ('Organization', 'Organization'),
            ('LicenceTerms', 'Licence'),
            ('Person', 'Person'),
            ('Group', 'Group'),
            ('Document', 'Document'),
            ('ToolService', 'Tool/Service'),
            ('Corpus', 'Corpus'),
            ('LexicalConceptualResource', 'Lexical Conceptual Resource'),
            ('LanguageDescription', 'Language Description'),
            ('Repository', 'Repository')
        ])

    def test_queryset(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        response = self.client.post(ADMIN_ENDPOINT)
        entity_type = [
            ('Project', 'Project'),
            ('Organization', 'Organization'),
            ('LicenceTerms', 'Licence'),
            ('Person', 'Person'),
            ('Group', 'Group'),
            ('Document', 'Document'),
            ('ToolService', 'Tool/Service'),
            ('Corpus', 'Corpus'),
            ('LexicalConceptualResource', 'Lexical Conceptual Resource'),
            ('LanguageDescription', 'Language Description'),
            ('Repository', 'Repository')
        ]
        type_filter = TypeFilter(request=response.wsgi_request,
                                 params=entity_type,
                                 model=MetadataRecord,
                                 model_admin=mdr_admin)
        queryset = type_filter.queryset(response.wsgi_request, MetadataRecord.objects.all())
        
        self.assertTrue(queryset)

    def test_queryset_with_value_lr(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        value_dict = {'type': 'ToolService'}
        response = self.client.post(ADMIN_ENDPOINT, data=value_dict)
        type_filter = TypeFilter(request=response.wsgi_request,
                                 params=value_dict,
                                 model=MetadataRecord,
                                 model_admin=mdr_admin)
        queryset = type_filter.queryset(response.wsgi_request, MetadataRecord.objects.all())
        
        self.assertTrue(queryset)

    def test_queryset_with_value_not_lr(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        value_dict = {'type': 'Project'}
        response = self.client.post(ADMIN_ENDPOINT, data=value_dict)
        type_filter = TypeFilter(request=response.wsgi_request,
                                 params=value_dict,
                                 model=MetadataRecord,
                                 model_admin=mdr_admin)
        queryset = type_filter.queryset(response.wsgi_request, MetadataRecord.objects.all())
        
        self.assertTrue(queryset)

    def test_queryset_with_value_repo(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        value_dict = {'type': 'Repository'}
        response = self.client.post(ADMIN_ENDPOINT, data=value_dict)
        type_filter = TypeFilter(request=response.wsgi_request,
                                 params=value_dict,
                                 model=MetadataRecord,
                                 model_admin=mdr_admin)
        queryset = type_filter.queryset(response.wsgi_request, MetadataRecord.objects.all())
        
        self.assertFalse(queryset)


class TestFunctionalServiceFilter(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.user, cls.token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.service_stats = {
            'user': 'preferred_username',
            'start_at': '2020-01-01',
            'end_at': '2020-02-02',
            'lt_service': 'x0f2143120x',
            'bytes': 12,
            'elg_resource': None,
            'status': 'succeeded',
            'call_type': 'PermissionsRequest'
        }

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()
    
    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_lookups(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        response = self.client.post(ADMIN_ENDPOINT)
        functional_service = [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]
        functional_service_filter = FunctionalServiceFilter(request=response.wsgi_request,
                                                            params=functional_service,
                                                            model=MetadataRecord,
                                                            model_admin=mdr_admin)
        lookup_list = functional_service_filter.lookups(response.wsgi_request, mdr_admin)
        self.assertTrue(lookup_list == [
            ('Yes', 'Yes'),
            ('No', 'No')
        ])

    def test_queryset(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        response = self.client.post(ADMIN_ENDPOINT)
        functional_service = [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]
        functional_service_filter = FunctionalServiceFilter(request=response.wsgi_request,
                                                            params=functional_service,
                                                            model=MetadataRecord,
                                                            model_admin=mdr_admin)
        queryset = functional_service_filter.queryset(response.wsgi_request, MetadataRecord.objects.all())
        self.assertTrue(queryset)

    def test_queryset_with_value_yes(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        value_dict = {'functional': 'yes'}
        response = self.client.post(ADMIN_ENDPOINT, data=value_dict)
        functional_service_filter = FunctionalServiceFilter(request=response.wsgi_request,
                                                            params=value_dict,
                                                            model=MetadataRecord,
                                                            model_admin=mdr_admin)
        queryset = functional_service_filter.queryset(response.wsgi_request, MetadataRecord.objects.all())
        
        self.assertTrue(queryset)

    def test_queryset_with_value_no(self):
        mdr_admin = MetadataRecordAdmin(model=MetadataRecord, admin_site=AdminSite())
        value_dict = {'functional': 'no'}
        response = self.client.post(ADMIN_ENDPOINT, data=value_dict)
        functional_service_filter = FunctionalServiceFilter(request=response.wsgi_request,
                                                            params=value_dict,
                                                            model=MetadataRecord,
                                                            model_admin=mdr_admin)
        queryset = functional_service_filter.queryset(response.wsgi_request, MetadataRecord.objects.all())
        
        self.assertTrue(queryset)
