import json
import logging

from django.conf import settings
from django.urls import reverse

from management.models import Manager
from registry.models import MetadataRecord
from registry.serializers import (
    MetadataRecordSerializer, GenericOrganizationSerializer,
    GenericPersonSerializer, DomainSerializer, GenericProjectSerializer,
    GenericDocumentSerializer,
    GenericLanguageResourceSerializer, GenericLicenceTermsSerializer,
    GenericRepositorySerializer,
    GenericGroupSerializer
)
from test_utils import (
    api_call, get_test_user, EndpointTestCase
)

LOGGER = logging.getLogger(__name__)


ORG_MATCH_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/match/organization/'
PROJ_MATCH_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/match/project/'
PERSON_MATCH_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/match/person/'
GROUP_MATCH_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/match/group/'
DOC_MATCH_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/match/document/'
LICENCE_MATCH_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/match/licence_terms/'
REPO_MATCH_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/match/repository/'
LR_MATCH_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/match/lr/'

ORG_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/organization/'
PERSON_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/person/'
GROUP_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/group/'
ACTOR_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/actor/'
DOMAIN_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/domain/'
TEXTTYPE_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/text_type/'
AUDIOGENRE_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/audio_genre/'
SPEECHGENRE_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/speech_genre/'
IMAGEGENRE_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/image_genre/'
VIDEOGENRE_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/video_genre/'
TEXTGENRE_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/text_genre/'
SUBJECT_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/subject/'
ASR_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/access_rights_statement/'
LANGUAGE_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/language/'
COUNTRY_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/country/'
CVRECOMMENDED_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/cv_recommended/'
PROJ_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/project/'
LR_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/lr/'
DOC_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/document/'
LICENCE_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/licence_terms/'
REPO_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/repository/'
KW_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/keyword/'
SO_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/service_offered/'
CR_LOOKUP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/lookup/corpus_register/'


class TestOrganizationMatchEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['lt_area'] = []
        cls.serializer_deleted = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_deleted.is_valid():
            cls.instance_deleted = cls.serializer_deleted.save()
            manager = Manager.objects.get(id=cls.instance_deleted.management_object.id)
            manager.curator = cls.admin
            manager.deleted = True
            manager.save()
            cls.instance_deleted.management_object = manager
            cls.instance_deleted.save()
        else:
            LOGGER.info(cls.serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            manager = Manager.objects.get(id=cls.instance.management_object.id)
            manager.curator = cls.admin
            manager.under_construction = True
            manager.save()
            cls.instance.management_object = manager
            cls.instance.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            'actor_type': 'Organization',
            'organization_name': {
                "en": "Other org"
            },
            'organization_identifier': [],
            'website': []
        }
        gen_serializer = GenericOrganizationSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_org_match_superuser_full(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertTrue(response.get('json_content')[0]['under_construction'])
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_org_match_superuser_full_with_parent_exact_false_with_same_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath',
                 'parent': 'true'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_org_match_superuser_full_with_parent_exact_false_with_same_div_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Athena Research Center',
                 'parent': 'true'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_org_match_superuser_full_with_short_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'arc'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_org_match_superuser_gen(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Other'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], None)
        self.assertEquals(response.get('json_content')[0]['curator'], None)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(
            response.get('json_content')[0]['full_metadata_record'])

    def test_org_match_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_org_match_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_org_match_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_org_match_superuser_exact_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_org_match_superuser_exact_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Other org'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['exact'])

    def test_org_match_superuser_locked_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Other org'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['locked'])

    def test_org_match_superuser_locked_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['locked'])

    def test_cannot_org_match_anonymous(self):
        query = {'query': 'ath'}
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_org_match_consumer(self):
        query = {'query': 'ath'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_org_match_validator(self):
        query = {'query': 'ath'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_org_match_content_manager(self):
        query = {'query': 'ath'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_org_match_provider(self):
        query = {'query': 'ath'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', ORG_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestProjectMatchEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['lt_area'] = []
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            manager = Manager.objects.get(id=cls.instance.management_object.id)
            manager.curator = cls.admin
            manager.under_construction = True
            manager.save()
            cls.instance.management_object = manager
            cls.instance.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            'project_name': {
                "en": "Other proj"
            },
            'project_identifier': [
                {
                    "project_identifier_scheme": 'http://w3id.org/meta-share/meta-share/OpenAIRE',
                    "value": "0000-0001-23423-634"
                }
            ],
            'website': [],
            'grant_number': ''
        }
        gen_serializer = GenericProjectSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_project_match_superuser_full(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'European'}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertTrue(response.get('json_content')[0]['under_construction'])
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_project_match_superuser_full_with_short_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'eli'}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_project_match_superuser_gen(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Other'}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], None)
        self.assertEquals(response.get('json_content')[0]['curator'], None)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(
            response.get('json_content')[0]['full_metadata_record'])

    def test_project_match_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_project_match_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_project_match_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_project_match_superuser_exact_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'European'}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_project_match_superuser_exact_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Other proj'}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['exact'])

    def test_project_match_superuser_locked_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Other proj'}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['locked'])

    def test_project_match_superuser_locked_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'European'}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['locked'])

    def test_cannot_project_match_anonymous(self):
        query = {'query': 'European'}
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_project_match_consumer(self):
        query = {'query': 'European'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_project_match_validator(self):
        query = {'query': 'European'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_project_match_content_manager(self):
        query = {'query': 'European'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_project_match_provider(self):
        query = {'query': 'European'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', PROJ_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestPersonMatchEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/person.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['lt_area'] = []
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            manager = Manager.objects.get(id=cls.instance.management_object.id)
            manager.curator = cls.admin
            manager.under_construction = True
            manager.save()
            cls.instance.management_object = manager
            cls.instance.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            "actor_type": "Person",
            "surname": {
                "en": "Voukoutis"
            },
            "given_name": {
                "en": "Leon"
            },
            "personal_identifier": [
                {
                    "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                    "value": "0000-0001-25235-6423124"
                }
            ],
            "email": [
                "leon_v@arc.gr"
            ]
        }
        gen_serializer = GenericPersonSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_person_match_superuser_only_given_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_match_superuser_given_name_query(self):
        user, token = self.admin, self.admin_token
        query = {'given_name': 'leon'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_match_superuser_surname_query(self):
        user, token = self.admin, self.admin_token
        query = {'surname': 'vouk'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_match_superuser_given_name_and_surname(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon voukoutis'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_match_superuser_full(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'penny'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertTrue(response.get('json_content')[0]['under_construction'])
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_person_match_superuser_gen(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], None)
        self.assertEquals(response.get('json_content')[0]['curator'], None)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(
            response.get('json_content')[0]['full_metadata_record'])

    def test_person_match_superuser_exact_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_person_match_superuser_locked_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['locked'])

    def test_person_match_superuser_locked_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'penny'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['locked'])

    def test_person_match_superuser_given_name_and_middle_and_surname(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon middlename voukoutis'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_match_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_person_match_superuser_full_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon voukoutis'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_person_match_superuser_with_middle(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon middlename voukoutis'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_person_match_superuser_with_multiple_middle(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon middleone middlename voukoutis'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_person_match_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_person_match_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_person_match_anonymous(self):
        query = {'query': 'leon'}
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_person_match_consumer(self):
        query = {'query': 'leon'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_person_match_validator(self):
        query = {'query': 'leon'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_person_match_content_manager(self):
        query = {'query': 'leon'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_person_match_provider(self):
        query = {'query': 'leon'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', PERSON_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestGroupMatchEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/group.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['lt_area'] = []
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            manager = Manager.objects.get(id=cls.instance.management_object.id)
            manager.curator = cls.admin
            manager.under_construction = True
            manager.save()
            cls.instance.management_object = manager
            cls.instance.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            'actor_type': 'Group',
            'organization_name': {
                "en": "Other org"
            },
            'group_identifier': [
                {
                    "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                    "value": "0000-0001-23423-634"
                }
            ],
            'website': [
                "https://www.other-org.gr"
            ]
        }
        gen_serializer = GenericGroupSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_group_match_superuser_full(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertTrue(response.get('json_content')[0]['under_construction'])
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_group_match_superuser_gen(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Other'}
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], None)
        self.assertEquals(response.get('json_content')[0]['curator'], None)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(
            response.get('json_content')[0]['full_metadata_record'])

    def test_group_match_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_group_match_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_group_match_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_group_match_superuser_exact_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_group_match_superuser_exact_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Other org'}
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['exact'])

    def test_group_match_superuser_locked_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['locked'])

    def test_group_match_superuser_locked_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'Other org'}
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['locked'])

    def test_cannot_group_match_anonymous(self):
        query = {'query': 'ath'}
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_group_match_consumer(self):
        query = {'query': 'ath'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_group_match_validator(self):
        query = {'query': 'ath'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_group_match_content_manager(self):
        query = {'query': 'ath'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_group_match_provider(self):
        query = {'query': 'ath'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', GROUP_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestDocumentMatchEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/document.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['lt_area'] = []
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            manager = Manager.objects.get(id=cls.instance.management_object.id)
            manager.curator = cls.admin
            manager.under_construction = True
            manager.save()
            cls.instance.management_object = manager
            cls.instance.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            "document_identifier": [{
                "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                "value": "https://doi.org/10.5281/zenodo.3467639.1241241"
            }],
            "title": {
                "en": "totally not the same doc"
            }
        }
        gen_serializer = GenericDocumentSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_document_match_superuser_full(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'pypdd'}
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertTrue(response.get('json_content')[0]['under_construction'])
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_document_match_superuser_gen(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'totally'}
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], None)
        self.assertEquals(response.get('json_content')[0]['curator'], None)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(
            response.get('json_content')[0]['full_metadata_record'])

    def test_document_match_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_document_match_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_document_match_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_document_match_superuser_exact_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'pypdd'}
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_document_match_superuser_exact_true(self):
        user, token = self.admin, self.admin_token
        query = {
            'query': "PyPDD: a positive degree day model for glacier surface mass balance"}
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['exact'])

    def test_document_match_superuser_locked_true(self):
        user, token = self.admin, self.admin_token
        query = {
            'query': "PyPDD: a positive degree day model for glacier surface mass balance"}
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['locked'])

    def test_document_match_superuser_locked_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': "totally"}
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['locked'])

    def test_cannot_document_match_anonymous(self):
        query = {'query': 'pypdd'}
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_document_match_consumer(self):
        query = {'query': 'pypdd'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_document_match_validator(self):
        query = {'query': 'pypdd'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_document_match_content_manager(self):
        query = {'query': 'pypdd'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_document_match_provider(self):
        query = {'query': 'pypdd'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', DOC_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestLicenceTermsMatchEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            manager = Manager.objects.get(id=cls.instance.management_object.id)
            manager.curator = cls.admin
            manager.under_construction = True
            manager.save()
            cls.instance.management_object = manager
            cls.instance.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        gen_data = {'licence_terms_name': {'en': 'licence term name'},
                    'licence_terms_url': ['http://www.licencetermurll.com'],
                    "condition_of_use": [
                        "http://w3id.org/meta-share/meta-share/unspecified"
                    ],
                    'licence_identifier': [
                        {
                            "value": "0BSSFasasddfd",
                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX"
                        }
                    ]}
        gen_serializer = GenericLicenceTermsSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_licence_terms_match_superuser_full(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'bsd'}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertTrue(response.get('json_content')[0]['under_construction'])
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_licence_terms_match_superuser_full_with_short_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'bsd'}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get(
            'json_content')[0]['metadata_record'],
                          f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_licence_terms_match_superuser_gen(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'licence'}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], None)
        self.assertEquals(response.get('json_content')[0]['curator'], None)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(
            response.get('json_content')[0]['full_metadata_record'])

    def test_licence_terms_match_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_licence_terms_match_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_licence_terms_match_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_licence_terms_match_superuser_exact_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'bsd'}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_licence_terms_match_superuser_exact_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': "BSD Zero Clause License"}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['exact'])

    def test_licence_terms_match_superuser_locked_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': "BSD Zero Clause License"}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['locked'])

    def test_licence_terms_match_superuser_locked_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': "name"}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['locked'])

    def test_cannot_licence_terms_match_anonymous(self):
        query = {'query': 'bsd'}
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_licence_terms_match_consumer(self):
        query = {'query': 'bsd'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_licence_terms_match_validator(self):
        query = {'query': 'bsd'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_licence_terms_match_content_manager(self):
        query = {'query': 'bsd'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_licence_terms_match_provider(self):
        query = {'query': 'bsd'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', LICENCE_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestRepositoryMatchEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/repository.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            manager = Manager.objects.get(id=cls.instance.management_object.id)
            manager.curator = cls.admin
            manager.under_construction = True
            manager.save()
            cls.instance.management_object = manager
            cls.instance.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            'repository_name': {'en': 'different name'},
            'repository_url': "http://www.repo2dif.com",
            'repository_identifier': [
                {
                    "repository_identifier_scheme": "http://purl.org/spar/datacite/handle",
                    "value": "0000-0034-234-734-1523"
                }]
        }
        gen_serializer = GenericRepositorySerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_repository_match_superuser_full(self):
        user, token = self.admin, self.admin_token
        query = {'query': "Repo name"}
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 2)
        self.assertTrue(response.get('json_content')[1]['under_construction'])
        self.assertTrue(
            f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} "
            in [
                response.get('json_content')[0]['metadata_record'],
                response.get('json_content')[1]['metadata_record']
            ]
        )

    def test_repository_match_superuser_gen(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'different'}
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], None)
        self.assertEquals(response.get('json_content')[0]['curator'], None)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(
            response.get('json_content')[0]['full_metadata_record'])

    def test_repository_match_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_repository_match_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_repository_match_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_repository_match_superuser_exact_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'repo'}
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 3)
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_repository_match_superuser_exact_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': "Other repo name"}
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['exact'])

    def test_repository_match_superuser_locked_true(self):
        user, token = self.admin, self.admin_token
        query = {'query': "Repo name"}
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 2)
        self.assertTrue(True in [response.get('json_content')[0]['locked'],
                                 response.get('json_content')[1]['locked']])

    def test_repository_match_superuser_locked_false(self):
        user, token = self.admin, self.admin_token
        query = {'query': "Other repo name"}
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertFalse(response.get('json_content')[0]['locked'])

    def test_cannot_repository_match_anonymous(self):
        query = {'query': 'repo'}
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_repository_match_consumer(self):
        query = {'query': 'repo'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_repository_match_validator(self):
        query = {'query': 'repo'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_repository_match_content_manager(self):
        query = {'query': 'repo'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_repository_match_provider(self):
        query = {'query': 'repo'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', REPO_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestLanguageResourceMatchEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            manager = Manager.objects.get(id=cls.instance.management_object.id)
            manager.curator = cls.admin
            manager.under_construction = True
            manager.save()
            cls.instance.management_object = manager
            cls.instance.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            'resource_name': {
                'en': 'Resource'
            },
            'resource_Short_name': {
                'en': 're'
            },
            'lr_identifier': [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_value_id"
            }],
            'version': '1.0.1'
        }
        gen_serializer = GenericLanguageResourceSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_lr_match_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'res'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 4)

    def test_lr_match_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'nomatch'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_lr_matchsuperuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_lr_match_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': ''}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_lr_match_superuser_full(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'ANNIE 2 English Named Entity Recognizer'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], self.instance.management_object.status)
        self.assertEquals(response.get('json_content')[0]['version'], self.instance.described_entity.version)
        self.assertEquals(response.get('json_content')[0]['curator'], self.instance.management_object.curator.username)
        self.assertTrue(response.get('json_content')[0]['under_construction'])
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(
            response.get('json_content')[0]['metadata_record'],
            f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.instance.id})} ")

    def test_lr_match_superuser_gen(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'Resource'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('json_content')[0]['status'], None)
        self.assertEquals(response.get('json_content')[0]['curator'], None)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 4)
        self.assertFalse(
            response.get('json_content')[0]['full_metadata_record'])

    def test_lr_match_superuser_exact_false(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'ann'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 4)
        self.assertFalse(response.get('json_content')[0]['exact'])

    def test_lr_match_superuser_exact_true(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'ANNIE 2 English Named Entity Recognizer'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['exact'])

    def test_lr_match_superuser_locked_true(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'ANNIE 2 English Named Entity Recognizer'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertTrue(response.get('json_content')[0]['locked'])

    def test_lr_match_superuser_locked_false(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'Resource'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 4)
        self.assertFalse(response.get('json_content')[0]['locked'])

    def test_lr_match_superuser_corpus_full(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
            manager = Manager.objects.get(id=instance.management_object.id)
            manager.curator = user
            manager.save()
            instance.management_object = manager
            instance.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        query = {'corpus': 'Full corpus'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_lr_match_superuser_corpus_generic(self):
        user, token = self.admin, self.admin_token
        query = {'corpus': 'res'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 4)

    def test_lr_match_superuser_tool_full(self):
        user, token = self.admin, self.admin_token
        query = {'tool_service': 'ANNIE'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_lr_match_superuser_tool_generic(self):
        user, token = self.admin, self.admin_token
        query = {'tool_service': 'res'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 4)

    def test_lr_match_superuser_lcr_full(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
            manager = Manager.objects.get(id=instance.management_object.id)
            manager.curator = user
            manager.save()
            instance.management_object = manager
            instance.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        query = {'lcr': 'full title'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_lr_match_superuser_lcr_generic(self):
        user, token = self.admin, self.admin_token
        query = {'lcr': 'res'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 4)

    def test_lr_match_superuser_ld_full(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
            manager = Manager.objects.get(id=instance.management_object.id)
            manager.curator = user
            manager.save()
            instance.management_object = manager
            instance.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        query = {'ld': 'full title'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_lr_match_superuser_ld_generic(self):
        user, token = self.admin, self.admin_token
        query = {'ld': 'res'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 4)

    def test_cannot_lr_match_anonymous(self):
        query = {'query_all': 'res'}
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_lr_match_consumer(self):
        query = {'query_all': 'res'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_lr_match_validator(self):
        query = {'query_all': 'res'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_lr_match_content_manager(self):
        query = {'query_all': 'res'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_lr_match_provider(self):
        query = {'query_all': 'res'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', LR_MATCH_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestOrganizationLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['lt_area'] = []
        cls.serializer_deleted = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_deleted.is_valid():
            cls.instance_deleted = cls.serializer_deleted.save()
            cls.instance_deleted.management_object.status = 'p'
            cls.instance_deleted.management_object.deleted = True
            cls.instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_published = json.loads(cls.json_str)
        cls.raw_data_published['described_entity']['organization_name'] = {
            "en": "Athena Research Center 2"
        }
        cls.raw_data_published['described_entity']['organization_short_name'] = []
        cls.raw_data_published['described_entity']['organization_identifier'] = []
        cls.raw_data_published['described_entity']['website'] = []
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_np = json.loads(cls.json_str)
        cls.raw_data_np['described_entity']['organization_name'] = {
            "en": "np org"
        }
        cls.raw_data_np['described_entity']['organization_short_name'] = []
        cls.raw_data_np['described_entity']['organization_identifier'] = []
        cls.raw_data_np['described_entity']['website'] = []
        cls.serializer_np = MetadataRecordSerializer(data=cls.raw_data_np)
        if cls.serializer_np.is_valid():
            cls.instance_np = cls.serializer_np.save()
            cls.instance_np.management_object.curator = cls.admin
            cls.instance_np.management_object.save()
        else:
            LOGGER.info(cls.serializer_np.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            'actor_type': 'Organization',
            'organization_name': {
                "en": "Other ath"
            },
            'organization_identifier': [
                {
                    "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                    "value": "0000-0001-2342dsa3-634"
                }
            ],
            'website': []
        }
        gen_serializer = GenericOrganizationSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)
        gen_data_2 = {
            'actor_type': 'Organization',
            'organization_name': {
                "en": "Other org"
            },
            'organization_identifier': [
                {
                    "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                    "value": "0000-00dsa01-23423-6344"
                }
            ],
            'website': []
        }
        gen_serializer_2 = GenericOrganizationSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_org_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 3)

    def test_org_lookup_superuser_np(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'np org'}
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'i')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_org_lookup_superuser_np_fails_with_different_user(self):
        user, token = self.provider, self.provider_token
        query = {'query': 'np org'}
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_org_lookup_superuser_with_short_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'arc'}
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 2)

    def test_org_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_org_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_org_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_org_lookup_anonymous(self):
        query = {'query': 'ath'}
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_org_lookup_consumer(self):
        query = {'query': 'ath'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_org_lookup_validator(self):
        query = {'query': 'ath'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_org_lookup_content_manager(self):
        query = {'query': 'ath'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_org_lookup_provider(self):
        query = {'query': 'ath'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', ORG_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestPersonLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/person.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['lt_area'] = []
        cls.serializer_deleted = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_deleted.is_valid():
            cls.instance_deleted = cls.serializer_deleted.save()
            cls.instance_deleted.management_object.status = 'p'
            cls.instance_deleted.management_object.deleted = True
            cls.instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_published = json.loads(cls.json_str)
        cls.raw_data_published['described_entity']['surname'] = {
            "en": "labropppp"
        }
        cls.raw_data_published['described_entity']['given_name'] = {
            "en": "pennnn"
        }
        cls.raw_data_published['described_entity']['personal_identifier'] = []
        cls.raw_data_published['described_entity']['email'] = []
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_np = json.loads(cls.json_str)
        cls.raw_data_np['described_entity']['surname'] = {
            "en": "np pers"
        }
        cls.raw_data_np['described_entity']['given_name'] = {
            "en": "np pers gn"
        }
        cls.raw_data_np['described_entity']['personal_identifier'] = []
        cls.raw_data_np['described_entity']['email'] = []
        cls.serializer_np = MetadataRecordSerializer(data=cls.raw_data_np)
        if cls.serializer_np.is_valid():
            cls.instance_np = cls.serializer_np.save()
            cls.instance_np.management_object.curator = cls.admin
            cls.instance_np.management_object.save()
        else:
            LOGGER.info(cls.serializer_np.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            "actor_type": "Person",
            "surname": {
                "en": "Labropoul"
            },
            "given_name": {
                "en": "Peny"
            },
            "personal_identifier": [
                {
                    "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                    "value": "0000-0001-25235-642312424"
                }
            ],
            "email": []
        }
        gen_serializer = GenericPersonSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)
        gen_data_2 = {
            "actor_type": "Person",
            "surname": {
                "en": "Voukoutis"
            },
            "given_name": {
                "en": "Leon"
            },
            "personal_identifier": [
                {
                    "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                    "value": "0000-0001-25235-6423523422323124"
                }
            ],
            "email": [
                "leon.voukoutissss@athenarc.gr"
            ]
        }
        gen_serializer_2 = GenericPersonSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_person_lookup_superuser_query_test_return(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'pen'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 3)

    def test_person_lookup_superuser_query_test_return_np(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'np per'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'i')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_lookup_superuser_query_test_return_np_with_different_user(self):
        user, token = self.provider, self.provider_token
        query = {'query': 'np per'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_person_lookup_superuser_only_given_name_test_return(self):
        user, token = self.admin, self.admin_token
        query = {'given_name': 'pen'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 3)

    def test_person_lookup_superuser_only_surname_test_return(self):
        user, token = self.admin, self.admin_token
        query = {'surname': 'Labr'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'p')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 3)

    def test_person_lookup_superuser_only_given_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_lookup_superuser_given_name_query(self):
        user, token = self.admin, self.admin_token
        query = {'given_name': 'leon'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_lookup_superuser_surname_query(self):
        user, token = self.admin, self.admin_token
        query = {'surname': 'vouk'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_lookup_superuser_given_name_and_surname(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon voukoutis'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_lookup_superuser_given_name_and_middle_and_surname(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon middlename voukoutis'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_person_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_person_lookup_superuser_full_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon voukoutis'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_person_lookup_superuser_with_middle(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon middlename voukoutis'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_person_lookup_superuser_with_multiple_middle(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon middleone middlename voukoutis'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_person_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_person_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_person_lookup_anonymous(self):
        query = {'query': 'leon'}
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_person_lookup_consumer(self):
        query = {'query': 'leon'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_person_lookup_validator(self):
        query = {'query': 'leon'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_person_lookup_content_manager(self):
        query = {'query': 'leon'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_person_lookup_provider(self):
        query = {'query': 'leon'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', PERSON_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestGroupLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/group.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['lt_area'] = []
        cls.serializer_deleted = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_deleted.is_valid():
            cls.instance_deleted = cls.serializer_deleted.save()
            cls.instance_deleted.management_object.status = 'p'
            cls.instance_deleted.management_object.deleted = True
            cls.instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_published = json.loads(cls.json_str)
        cls.raw_data_published['described_entity']['organization_name'] = {
            "en": "Athena Research Center 2"
        }
        cls.raw_data_published['described_entity']['organization_short_name'] = []
        cls.raw_data_published['described_entity']['group_identifier'] = []
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_np = json.loads(cls.json_str)
        cls.raw_data_np['described_entity']['organization_name'] = {
            "en": "np grp"
        }
        cls.raw_data_np['described_entity']['organization_short_name'] = []
        cls.raw_data_np['described_entity']['group_identifier'] = []
        cls.serializer_np = MetadataRecordSerializer(data=cls.raw_data_np)
        if cls.serializer_np.is_valid():
            cls.instance_np = cls.serializer_np.save()
            cls.instance_np.management_object.curator = cls.admin
            cls.instance_np.management_object.save()
        else:
            LOGGER.info(cls.serializer_np.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            'actor_type': 'Group',
            'organization_name': {
                "en": "other group"
            },
            'group_identifier': [
                {
                    "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                    "value": "0000-0001-5123-7890"
                }
            ],
            'website': [
                "https://www.athena-innovation.gr"
            ]
        }
        gen_serializer = GenericGroupSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)
        gen_data_2 = {
            'actor_type': 'Group',
            'organization_name': {
                "en": "Other ath"
            },
            'group_identifier': [
                {
                    "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                    "value": "0000-0001-5123-784290"
                }
            ],
            'website': [
                "https://www.athena-innovgsdation.gr"
            ]
        }
        gen_serializer_2 = GenericGroupSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_group_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 3)

    def test_group_lookup_superuser_np(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'np grp'}
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_group_lookup_superuser_np_with_different_user(self):
        user, token = self.provider, self.provider_token
        query = {'query': 'np grp'}
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_group_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_group_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_group_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_group_lookup_anonymous(self):
        query = {'query': 'ath'}
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_group_lookup_consumer(self):
        query = {'query': 'ath'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_group_lookup_validator(self):
        query = {'query': 'ath'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_group_lookup_content_manager(self):
        query = {'query': 'ath'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_group_lookup_provider(self):
        query = {'query': 'ath'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', GROUP_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestActorLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.per_json_file = open('registry/tests/fixtures/person.json', 'r')
        cls.per_json_str = cls.per_json_file.read()
        cls.per_json_file.close()
        cls.per_raw_data = json.loads(cls.per_json_str)
        cls.per_raw_data['lt_area'] = []
        cls.per_serializer_deleted = MetadataRecordSerializer(data=cls.per_raw_data)
        if cls.per_serializer_deleted.is_valid():
            cls.per_instance_deleted = cls.per_serializer_deleted.save()
            cls.per_instance_deleted.management_object.status = 'p'
            cls.per_instance_deleted.management_object.deleted = True
            cls.per_instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.per_serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.per_serializer = MetadataRecordSerializer(data=cls.per_raw_data)
        if cls.per_serializer.is_valid():
            cls.per_instance = cls.per_serializer.save()
        else:
            LOGGER.info(cls.per_serializer.errors)
        LOGGER.info('Setup has finished')
        cls.per_raw_data_published = json.loads(cls.per_json_str)
        cls.per_raw_data_published['described_entity']['personal_identifier'] = []
        cls.per_raw_data_published['described_entity']['email'] = []
        cls.per_serializer_published = MetadataRecordSerializer(data=cls.per_raw_data_published)
        if cls.per_serializer_published.is_valid():
            cls.per_instance_published = cls.per_serializer_published.save()
            cls.per_instance_published.management_object.status = 'p'
            cls.per_instance_published.management_object.save()
        else:
            LOGGER.info(cls.per_serializer_published.errors)
        LOGGER.info('Setup has finished')
        cls.org_json_file = open('registry/tests/fixtures/organization.json',
                                 'r')
        cls.org_json_str = cls.org_json_file.read()
        cls.org_json_file.close()
        cls.org_raw_data = json.loads(cls.org_json_str)
        cls.org_raw_data['lt_area'] = []
        cls.org_serializer_deleted = MetadataRecordSerializer(data=cls.org_raw_data)
        if cls.org_serializer_deleted.is_valid():
            cls.org_instance_deleted = cls.org_serializer_deleted.save()
            cls.org_instance_deleted.management_object.status = 'p'
            cls.org_instance_deleted.management_object.deleted = True
            cls.org_instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.org_serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.org_serializer = MetadataRecordSerializer(data=cls.org_raw_data)
        if cls.org_serializer.is_valid():
            cls.org_instance = cls.org_serializer.save()
        else:
            LOGGER.info(cls.org_serializer.errors)
        LOGGER.info('Setup has finished')
        cls.org_raw_data_published = json.loads(cls.org_json_str)
        cls.org_raw_data_published['described_entity']['organization_name'] = {
            "en": "Athena Research Center 2"
        }
        cls.org_raw_data_published['described_entity']['organization_short_name'] = []
        cls.org_raw_data_published['described_entity']['organization_identifier'] = []
        cls.org_raw_data_published['described_entity']['website'] = []
        cls.org_serializer_published = MetadataRecordSerializer(data=cls.org_raw_data_published)
        if cls.org_serializer_published.is_valid():
            cls.org_instance_published = cls.org_serializer_published.save()
            cls.org_instance_published.management_object.status = 'p'
            cls.org_instance_published.management_object.save()
        else:
            LOGGER.info(cls.org_instance_published.errors)
        LOGGER.info('Setup has finished')
        cls.grp_json_file = open('registry/tests/fixtures/group.json', 'r')
        cls.grp_json_str = cls.grp_json_file.read()
        cls.grp_json_file.close()
        cls.grp_raw_data = json.loads(cls.grp_json_str)
        cls.grp_raw_data['lt_area'] = []
        cls.grp_serializer_deleted = MetadataRecordSerializer(data=cls.grp_raw_data)
        if cls.grp_serializer_deleted.is_valid():
            cls.grp_instance_deleted = cls.grp_serializer_deleted.save()
            cls.grp_instance_deleted.management_object.status = 'p'
            cls.grp_instance_deleted.management_object.deleted = True
            cls.grp_instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.grp_serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.grp_serializer = MetadataRecordSerializer(data=cls.grp_raw_data)
        if cls.grp_serializer.is_valid():
            cls.grp_instance = cls.grp_serializer.save()
        else:
            LOGGER.info(cls.org_serializer.errors)
        LOGGER.info('Setup has finished')
        cls.grp_raw_data_published = json.loads(cls.grp_json_str)
        cls.grp_raw_data_published['described_entity']['organization_name'] = {
            "en": "Athena Research Center 2"
        }
        cls.grp_raw_data_published['described_entity']['organization_short_name'] = []
        cls.grp_raw_data_published['described_entity']['group_identifier'] = []
        cls.grp_serializer_published = MetadataRecordSerializer(data=cls.grp_raw_data_published)
        if cls.grp_serializer_published.is_valid():
            cls.grp_instance_published = cls.grp_serializer_published.save()
            cls.grp_instance_published.management_object.status = 'p'
            cls.grp_instance_published.management_object.save()
        else:
            LOGGER.info(cls.grp_serializer_published.errors)
        LOGGER.info('Setup has finished')
        cls.actor_raw_data_np = json.loads(cls.grp_json_str)
        cls.actor_raw_data_np['described_entity']['organization_name'] = {
            "en": "np actor"
        }
        cls.actor_raw_data_np['described_entity']['organization_short_name'] = []
        cls.actor_raw_data_np['described_entity']['group_identifier'] = []
        cls.actor_serializer_np = MetadataRecordSerializer(data=cls.actor_raw_data_np)
        if cls.actor_serializer_np.is_valid():
            cls.actor_instance_np = cls.actor_serializer_np.save()
            cls.actor_instance_np.management_object.curator = cls.admin
            cls.actor_instance_np.management_object.save()
        else:
            LOGGER.info(cls.actor_serializer_np.errors)
        LOGGER.info('Setup has finished')
        gen_per_data = {
            "actor_type": "Person",
            "surname": {
                "en": "Voukoutis"
            },
            "given_name": {
                "en": "Leon"
            },
            "personal_identifier": [
                {
                    "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                    "value": "000asd0-0001-25235-6423124"
                }
            ],
            "email": []
        }
        gen_per_serializer = GenericPersonSerializer(data=gen_per_data)
        if gen_per_serializer.is_valid():
            gen_per_instance = gen_per_serializer.save()
        else:
            LOGGER.info(gen_per_serializer.errors)
        gen_per_data_2 = {
            "actor_type": "Person",
            "surname": {
                "en": "Labropou"
            },
            "given_name": {
                "en": "Peny"
            },
            "personal_identifier": [
                {
                    "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                    "value": "0000-0ads001-25235-6422523124"
                }
            ],
            "email": []
        }
        gen_per_serializer_2 = GenericPersonSerializer(data=gen_per_data_2)
        if gen_per_serializer_2.is_valid():
            gen_per_instance_2 = gen_per_serializer_2.save()
        else:
            LOGGER.info(gen_per_serializer_2.errors)
        gen_org_data = {
            'actor_type': 'Organization',
            'organization_name': {
                "en": "Other org"
            },
            'organization_identifier': [
                {
                    "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                    "value": "0000asd-0001-23423-634"
                }
            ],
            'website': []
        }
        gen_org_serializer = GenericOrganizationSerializer(data=gen_org_data)
        if gen_org_serializer.is_valid():
            gen_org_instance = gen_org_serializer.save()
        else:
            LOGGER.info(gen_org_serializer.errors)
        gen_org_data_2 = {
            'actor_type': 'Organization',
            'organization_name': {
                "en": "Other ath"
            },
            'organization_identifier': [
                {
                    "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                    "value": "0000-0001-23423-623434"
                }
            ],
            'website': [
                "https://www.otheadr-org.gr"
            ]
        }
        gen_org_serializer_2 = GenericOrganizationSerializer(data=gen_org_data_2)
        if gen_org_serializer_2.is_valid():
            gen_org_instance = gen_org_serializer_2.save()
        else:
            LOGGER.info(gen_org_serializer_2.errors)
        gen_grp_data = {
            'actor_type': 'Group',
            'organization_name': {
                "en": "other grp"
            },
            'group_identifier': [
                {
                    "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                    "value": "0000-0001-5123-7890"
                }
            ],
            'website': [
                "https://www.athena-innovation.gr"
            ]
        }
        gen_grp_serializer = GenericGroupSerializer(data=gen_grp_data)
        if gen_grp_serializer.is_valid():
            gen_grp_instance = gen_grp_serializer.save()
        else:
            LOGGER.info(gen_grp_serializer.errors)
        gen_grp_data_2 = {
            'actor_type': 'Group',
            'organization_name': {
                "en": "other ath"
            },
            'group_identifier': [
                {
                    "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                    "value": "0000-0001-5123-7354890"
                }
            ],
            'website': [
                "https://www.athena354-innovation.gr"
            ]
        }
        gen_grp_serializer_2 = GenericGroupSerializer(data=gen_grp_data_2)
        if gen_grp_serializer_2.is_valid():
            gen_grp_instance_2 = gen_grp_serializer_2.save()
        else:
            LOGGER.info(gen_grp_serializer_2.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_actor_lookup_superuser_person_test_return(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'pen'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 3)
        self.assertEquals(response.get('json_content')[0]['actor_type'],
                          'Person')
        print(response.get('json_content'))
        md_id = response.get('json_content')[1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)

    def test_actor_lookup_superuser_actor_test_return_np(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'np actor'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'i')

    def test_actor_lookup_superuser_actor_test_return_np_with_different_user(self):
        user, token = self.provider, self.provider_token
        query = {'query': 'np actor'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_actor_lookup_superuser_org_group_test_return(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 6)
        self.assertTrue(
            response.get('json_content')[2]['actor_type'] in ['Group',
                                                              'Organization'])
        self.assertTrue(
            response.get('json_content')[3]['actor_type'] in ['Group',
                                                              'Organization'])
        
        md_id = response.get('json_content')[2].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        md_id_2 = response.get('json_content')[3].get('pk')
        mdr_2 = MetadataRecord.objects.get(described_entity__pk=md_id_2)

    def test_actor_lookup_superuser_person(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get('json_content')[0]['actor_type'],
                          'Person')

    def test_actor_lookup_superuser_org_and_group(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'ath'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 6)
        self.assertTrue(
            response.get('json_content')[1]['actor_type'] in ['Group',
                                                              'Organization'])
        self.assertTrue(
            response.get('json_content')[0]['actor_type'] in ['Group',
                                                              'Organization'])

    def test_actor_lookup_superuser_given_name_and_middle_and_surname(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon middlename voukoutis'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get('json_content')[0]['actor_type'],
                          'Person')

    def test_actor_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_actor_lookup_superuser_full_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon voukoutis'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get('json_content')[0]['actor_type'],
                          'Person')

    def test_actor_lookup_superuser_with_middle(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon middlename voukoutis'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get('json_content')[0]['actor_type'],
                          'Person')

    def test_actor_lookup_superuser_with_multiple_middle(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'leon middleone middlename voukoutis'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get('json_content')[0]['actor_type'],
                          'Person')

    def test_actor_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_actor_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_actor_lookup_anonymous(self):
        query = {'query': 'leon'}
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_actor_lookup_consumer(self):
        query = {'query': 'leon'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_actor_lookup_validator(self):
        query = {'query': 'leon'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_actor_lookup_content_manager(self):
        query = {'query': 'leon'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_actor_lookup_provider(self):
        query = {'query': 'leon'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', ACTOR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)
        self.assertEquals(response.get('json_content')[0]['actor_type'],
                          'Person')


class TestDomainLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['domain']
        cls.serializer = DomainSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_domain_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'lab'}
        response = api_call('GET', DOMAIN_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_domain_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', DOMAIN_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_domain_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', DOMAIN_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_domain_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', DOMAIN_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_domain_lookup_anonymous(self):
        query = {'query': 'lab'}
        response = api_call('GET', DOMAIN_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_domain_lookup_consumer(self):
        query = {'query': 'lab'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', DOMAIN_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_domain_lookup_validator(self):
        query = {'query': 'lab'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', DOMAIN_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_domain_lookup_content_manager(self):
        query = {'query': 'lab'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', DOMAIN_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_domain_lookup_provider(self):
        query = {'query': 'lab'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', DOMAIN_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestTextTypeLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/corpus.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_text_type_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'art'}
        response = api_call('GET', TEXTTYPE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_text_type_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', TEXTTYPE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_text_type_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', TEXTTYPE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_text_type_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', TEXTTYPE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_text_type_lookup_anonymous(self):
        query = {'query': 'art'}
        response = api_call('GET', TEXTTYPE_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_text_type_lookup_consumer(self):
        query = {'query': 'art'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', TEXTTYPE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_text_type_lookup_validator(self):
        query = {'query': 'art'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', TEXTTYPE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_text_type_lookup_content_manager(self):
        query = {'query': 'art'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', TEXTTYPE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_text_type_lookup_provider(self):
        query = {'query': 'art'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', TEXTTYPE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestAudioGenreLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/corpus.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_audio_genre_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'broadcast'}
        response = api_call('GET', AUDIOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_audio_genre_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', AUDIOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_audio_genre_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', AUDIOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_audio_genre_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', AUDIOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_audio_genre_lookup_anonymous(self):
        query = {'query': 'broadcast'}
        response = api_call('GET', AUDIOGENRE_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_audio_genre_lookup_consumer(self):
        query = {'query': 'broadcast'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', AUDIOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_audio_genre_lookup_validator(self):
        query = {'query': 'broadcast'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', AUDIOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_audio_genre_lookup_content_manager(self):
        query = {'query': 'broadcast'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', AUDIOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_audio_genre_lookup_provider(self):
        query = {'query': 'broadcast'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', AUDIOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestSpeechGenreLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/corpus.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_speech_genre_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'spee'}
        response = api_call('GET', SPEECHGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_speech_genre_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', SPEECHGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_speech_genre_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', SPEECHGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_speech_genre_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', SPEECHGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_speech_genre_lookup_anonymous(self):
        query = {'query': 'spee'}
        response = api_call('GET', SPEECHGENRE_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_speech_genre_lookup_consumer(self):
        query = {'query': 'spee'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', SPEECHGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_speech_genre_lookup_validator(self):
        query = {'query': 'spee'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', SPEECHGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_speech_genre_lookup_content_manager(self):
        query = {'query': 'spee'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', SPEECHGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_speech_genre_lookup_provider(self):
        query = {'query': 'spee'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', SPEECHGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestImageGenreLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/corpus.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_image_genre_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'doc'}
        response = api_call('GET', IMAGEGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_image_genre_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', IMAGEGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_image_genre_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', IMAGEGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_image_genre_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', IMAGEGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_image_genre_lookup_anonymous(self):
        query = {'query': 'doc'}
        response = api_call('GET', IMAGEGENRE_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_image_genre_lookup_consumer(self):
        query = {'query': 'doc'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', IMAGEGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_image_genre_lookup_validator(self):
        query = {'query': 'doc'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', IMAGEGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_image_genre_lookup_content_manager(self):
        query = {'query': 'doc'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', IMAGEGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_image_genre_lookup_provider(self):
        query = {'query': 'doc'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', IMAGEGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestVideoGenreLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/corpus.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_video_genre_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'doc'}
        response = api_call('GET', VIDEOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_video_genre_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', VIDEOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_video_genre_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', VIDEOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_video_genre_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', VIDEOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_video_genre_lookup_anonymous(self):
        query = {'query': 'doc'}
        response = api_call('GET', VIDEOGENRE_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_video_genre_lookup_consumer(self):
        query = {'query': 'doc'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', VIDEOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_video_genre_lookup_validator(self):
        query = {'query': 'doc'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', VIDEOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_video_genre_lookup_content_manager(self):
        query = {'query': 'doc'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', VIDEOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_video_genre_lookup_provider(self):
        query = {'query': 'doc'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', VIDEOGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestTextGenreLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/corpus.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_text_genre_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'lit'}
        response = api_call('GET', TEXTGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_text_genre_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', TEXTGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_text_genre_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', TEXTGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_text_genre_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', TEXTGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_text_genre_lookup_anonymous(self):
        query = {'query': 'lit'}
        response = api_call('GET', TEXTGENRE_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_text_genre_lookup_consumer(self):
        query = {'query': 'lit'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', TEXTGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_text_genre_lookup_validator(self):
        query = {'query': 'lit'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', TEXTGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_text_genre_lookup_content_manager(self):
        query = {'query': 'lit'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', TEXTGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_text_genre_lookup_provider(self):
        query = {'query': 'lit'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', TEXTGENRE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestSubjectLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/document.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_subject_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'model'}
        response = api_call('GET', SUBJECT_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 2)

    def test_subject_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', SUBJECT_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_subject_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', SUBJECT_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_subject_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', SUBJECT_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_subject_lookup_anonymous(self):
        query = {'query': 'model'}
        response = api_call('GET', SUBJECT_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_subject_lookup_consumer(self):
        query = {'query': 'model'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', SUBJECT_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_subject_lookup_validator(self):
        query = {'query': 'model'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', SUBJECT_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_subject_lookup_content_manager(self):
        query = {'query': 'model'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', SUBJECT_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_subject_lookup_provider(self):
        query = {'query': 'model'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', SUBJECT_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestAccessRightsStatementLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/corpus.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_access_rights_statement_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'lab'}
        response = api_call('GET', ASR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_access_rights_statement_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', ASR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_access_rights_statement_lookup_superuser_fails_with_no_query_keyword(
            self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', ASR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_access_rights_statement_lookup_superuser_fails_with_empty_query(
            self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', ASR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_access_rights_statement_lookup_anonymous(self):
        query = {'query': 'lab'}
        response = api_call('GET', ASR_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_access_rights_statement_lookup_consumer(self):
        query = {'query': 'lab'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', ASR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_access_rights_statement_lookup_validator(self):
        query = {'query': 'lab'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', ASR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_access_rights_statement_lookup_content_manager(self):
        query = {'query': 'lab'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', ASR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_access_rights_statement_lookup_provider(self):
        query = {'query': 'lab'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', ASR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestLanguageLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_language_lookup_superuser_all(self):
        user, token = self.admin, self.admin_token
        query = {'all': ''}
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['all'])

    def test_language_lookup_superuser_id(self):
        user, token = self.admin, self.admin_token
        query = {'language_id': 'en'}
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['language_id'])

    def test_language_lookup_superuser_label(self):
        user, token = self.admin, self.admin_token
        query = {'language_label': 'en'}
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['language_label'])

    def test_language_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'language_id': 'nomatch'}
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_language_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_language_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'language_label': ''}
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_language_lookup_anonymous(self):
        query = {'language_label': 'english'}
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_language_lookup_consumer(self):
        query = {'language_label': 'english'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_language_lookup_validator(self):
        query = {'language_label': 'english'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_language_lookup_content_manager(self):
        query = {'language_label': 'english'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_language_lookup_provider(self):
        query = {'language_label': 'english'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', LANGUAGE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token),
                            data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestCountryLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_country_lookup_superuser_all(self):
        user, token = self.admin, self.admin_token
        query = {'all': ''}
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['all'])

    def test_country_lookup_superuser_id(self):
        user, token = self.admin, self.admin_token
        query = {'country_id': '039'}
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['country_id'])

    def test_country_lookup_superuser_label(self):
        user, token = self.admin, self.admin_token
        query = {'country_label': 'south'}
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['country_label'])

    def test_country_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'country_id': 'nomatch'}
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_country_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_country_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'country_label': ''}
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_country_lookup_anonymous(self):
        query = {'country_label': 'south'}
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_country_lookup_consumer(self):
        query = {'country_label': 'south'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_country_lookup_validator(self):
        query = {'country_label': 'south'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_country_lookup_content_manager(self):
        query = {'country_label': 'south'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_country_lookup_provider(self):
        query = {'country_label': 'south'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', COUNTRY_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token),
                            data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestCVRecommendedLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_lt_area_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'lt_area': 'transl'}
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content')[0],
                          [
                              'http://w3id.org/meta-share/omtd-share/ComputerAidedTranslation',
                              'Computer-aided translation'], )

    def test_model_type_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'model_type': 'transf'}
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content')[0],
                          [
                              'http://w3id.org/meta-share/meta-share/transformer1',
                              'transformer'], )

    def test_model_function_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'model_function': 'transl'}
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content')[0],
                          [
                              'http://w3id.org/meta-share/omtd-share/MachineTranslation',
                              'Machine Translation'], )

    def test_data_format_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'data_format': 'alt'}
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content')[0],
                          [
                              'http://w3id.org/meta-share/omtd-share/Alto',
                              'ALTO'], )

    def test_size_unit_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'size_unit': 'entr'}
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content')[0],
                          [
                              'http://w3id.org/meta-share/meta-share/entry',
                              'entry'], )

    def test_dev_framework_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'development_framework': 'caff'}
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content')[0],
                          [
                              'http://w3id.org/meta-share/meta-share/Caffe',
                              'Caffe'], )

    def test_annotation_type_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'annotation_type': 'align'}
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content')[0],
                          [
                              'http://w3id.org/meta-share/omtd-share/Alignment1',
                              'Alignment'], )

    def test_cv_recommended_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_cv_recommended_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'lt_area': ''}
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_cv_recommended_lookup_anonymous(self):
        query = {'lt_area': 'transl'}
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_cv_recommended_lookup_consumer(self):
        query = {'lt_area': 'transl'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_cv_recommended_lookup_validator(self):
        query = {'lt_area': 'transl'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_cv_recommended_lookup_content_manager(self):
        query = {'lt_area': 'transl'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_cv_recommended_lookup_provider(self):
        query = {'lt_area': 'transl'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', CVRECOMMENDED_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestProjectLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['lt_area'] = []
        cls.serializer_deleted = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_deleted.is_valid():
            cls.instance_deleted = cls.serializer_deleted.save()
            cls.instance_deleted.management_object.status = 'p'
            cls.instance_deleted.management_object.deleted = True
            cls.instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        # cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        # if cls.serializer.is_valid():
        #     cls.instance = cls.serializer.save()
        # else:
        #     LOGGER.info(cls.serializer.errors)
        # LOGGER.info('Setup has finished')
        cls.raw_data_published = json.loads(cls.json_str)
        cls.raw_data_published['described_entity']['project_name'] = {
            "en": "European Live Translator 2"
        }
        cls.raw_data_published['described_entity']['project_short_name'] = {"en": "eli"}
        cls.raw_data_published['described_entity']['project_identifier'] = []
        cls.raw_data_published['described_entity']['website'] = []
        cls.raw_data_published['described_entity']['grant_number'] = ''
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Published Setup has finished')
        cls.raw_data_np = json.loads(cls.json_str)
        cls.raw_data_np['described_entity']['project_name'] = {
            "en": "np proj"
        }
        cls.raw_data_np['described_entity']['project_short_name'] = {"en": "np proj short"}
        cls.raw_data_np['described_entity']['project_identifier'] = []
        cls.raw_data_np['described_entity']['website'] = []
        cls.raw_data_np['described_entity']['grant_number'] = ''
        cls.serializer_np = MetadataRecordSerializer(data=cls.raw_data_np)
        if cls.serializer_np.is_valid():
            cls.instance_published = cls.serializer_np.save()
            cls.instance_published.management_object.curator = cls.admin
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_np.errors)
        LOGGER.info('NP Setup has finished')
        gen_data = {
            "project_name": {
                "en": "Other proj"
            },
            "project_identifier": [
                {
                    "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                    "value": "this_is_elg_vsdalue_id_number_two"
                },
                {
                    "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                    "value": "doi_valusde_id_two"
                }
            ],
            "website": [],
            "grant_number": "",
            "funding_type": [
                "http://w3id.org/meta-share/meta-share/euFunds"
            ],
            "funder": [
                {
                    "actor_type": "Organization",
                    "organization_name": {
                        "en": "European Commission"
                    },
                    "organization_identifier": [
                        {
                            "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "doi_value"
                        }
                    ],
                    "website": [
                        "http://www.european-commission.eu"
                    ]
                },
                {
                    "actor_type": "Person",
                    "surname": {
                        "en": "Giagkou"
                    },
                    "given_name": {
                        "en": "Maria"
                    },
                    "email": [
                        "mgiagkou@athenarc.gr"
                    ]
                },
                {
                    "actor_type": "Group",
                    "organization_name": {
                        "en": "Institute Rockerfeller"
                    },
                    "group_identifier": [
                        {
                            "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "doi_value"
                        }
                    ],
                    "website": [
                        "http://www.institute-rockerfeller.org"
                    ]
                }
            ]
        }
        gen_serializer = GenericProjectSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)
        gen_data_2 = {
            "project_name": {
                "en": "Other transl"
            },
            "project_identifier": [
                {
                    "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                    "value": "this_is_elg_value_id_numbe34r_two"
                },
                {
                    "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                    "value": "doi_value_id234_two"
                }
            ],
            "website": [],
            "grant_number": "",
            "funding_type": [
                "http://w3id.org/meta-share/meta-share/euFunds"
            ],
            "funder": [
                {
                    "actor_type": "Organization",
                    "organization_name": {
                        "en": "European Commission"
                    },
                    "organization_identifier": [
                        {
                            "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "doi_value"
                        }
                    ],
                    "website": [
                        "http://www.european-commission.eu"
                    ]
                },
                {
                    "actor_type": "Person",
                    "surname": {
                        "en": "Giagkou"
                    },
                    "given_name": {
                        "en": "Maria"
                    },
                    "email": [
                        "mgiagkou@athenarc.gr"
                    ]
                },
                {
                    "actor_type": "Group",
                    "organization_name": {
                        "en": "Institute Rockerfeller"
                    },
                    "group_identifier": [
                        {
                            "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "doi_value"
                        }
                    ],
                    "website": [
                        "http://www.institute-rockerfeller.org"
                    ]
                }
            ]
        }
        gen_serializer_2 = GenericProjectSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_project_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'transl'}
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'p')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 2)

    def test_project_lookup_superuser_np(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'np proj'}
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        print(response.get('json_content'))
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'i')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_project_lookup_superuser_np_with_different_user(self):
        user, token = self.provider, self.provider_token
        query = {'query': 'np proj'}
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_project_lookup_superuser_short_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'eli'}
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'p')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_project_lookup_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_project_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_project_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_project_lookup_anonymous(self):
        query = {'query': 'transl'}
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_project_lookup_consumer(self):
        query = {'query': 'transl'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_project_lookup_validator(self):
        query = {'query': 'transl'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_project_lookup_content_manager(self):
        query = {'query': 'transl'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_project_lookup_provider(self):
        query = {'query': 'transl'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', PROJ_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestLanguageResourceLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer_deleted = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_deleted.is_valid():
            cls.instance_deleted = cls.serializer_deleted.save()
            cls.instance_deleted.management_object.status = 'p'
            cls.instance_deleted.management_object.deleted = True
            cls.instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_published = json.loads(cls.json_str)
        cls.raw_data_published['described_entity']['resource_name'] = {
            "en": "ANNIE 2 English Named Entity Recognizer 2"
        }
        cls.raw_data_published['described_entity']['resource_short_name'] = {"en": "annie 2 2 2 2 2"}
        cls.raw_data_published['described_entity']['lr_identifier'] = []
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_np = json.loads(cls.json_str)
        cls.raw_data_np['described_entity']['resource_name'] = {
            "en": "np lr"
        }
        cls.raw_data_np['described_entity']['resource_short_name'] = {"en": "np short lr"}
        cls.raw_data_np['described_entity']['lr_identifier'] = []
        cls.serializer_np = MetadataRecordSerializer(data=cls.raw_data_np)
        if cls.serializer_np.is_valid():
            cls.instance_np = cls.serializer_np.save()
            cls.instance_np.management_object.curator = cls.admin
            cls.instance_np.management_object.save()
        else:
            LOGGER.info(cls.serializer_np.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            'resource_name': {
                'en': 'Resource'
            },
            'resource_Short_name': {
                'en': 're'
            },
            'lr_identifier': [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_value_id"
            }],
            'version': '1.0.1'
        }
        gen_serializer = GenericLanguageResourceSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)
        gen_data_2 = {
            'resource_name': {
                'en': 'annie two'
            },
            'resource_Short_name': {
                'en': 're'
            },
            'lr_identifier': [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_valgjue_id"
            }],
            'version': '1.0.1'
        }
        gen_serializer_2 = GenericLanguageResourceSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_lr_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'res'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['all']), 4)

    def test_lr_lookup_superuser_np(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'np lr'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['all']), 1)

    def test_lr_lookup_superuser_np_with_different_user(self):
        user, token = self.provider, self.provider_token
        query = {'query_all': 'np lr'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['all']), 0)

    def test_lr_lookup_superuser_brings_published_when_full_mdr(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'annie'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')['all'][1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['all']), 3)

    def test_lr_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': 'nomatch'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['all']), 0)

    def test_lr_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_lr_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query_all': ''}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_lr_lookup_superuser_corpus_full(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        raw_data_published = json.loads(json_str)
        raw_data_published['described_entity']['resource_name'] = {
            "en": "Full corpus name 2"
        }
        raw_data_published['described_entity']['resource_short_name'] = {}
        raw_data_published['described_entity']['lr_identifier'] = []
        serializer_published = MetadataRecordSerializer(data=raw_data_published)
        if serializer_published.is_valid():
            instance_published = serializer_published.save()
            instance_published.management_object.status = 'p'
            instance_published.management_object.save()
        else:
            LOGGER.info(serializer_published.errors)
        LOGGER.info('Setup has finished')
        query = {'corpus': 'Full corpus'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')['corpus'][0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'p')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['corpus']), 1)

    def test_lr_lookup_superuser_corpus_both(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        raw_data['described_entity']['resource_name'] = {
            "en": "Full corpus name 2 2 2"
        }
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        raw_data_published = json.loads(json_str)
        raw_data_published['described_entity']['resource_name'] = {
            "en": "Full corpus name 2 2"
        }
        raw_data_published['described_entity']['resource_short_name'] = {}
        raw_data_published['described_entity']['lr_identifier'] = []
        serializer_published = MetadataRecordSerializer(data=raw_data_published)
        if serializer_published.is_valid():
            instance_published = serializer_published.save()
            instance_published.management_object.status = 'p'
            instance_published.management_object.save()
        else:
            LOGGER.info(serializer_published.errors)
        LOGGER.info('Setup has finished')
        gen_data_2 = {
            'resource_name': {
                'en': 'fullc'
            },
            'resource_Short_name': {
                'en': 're'
            },
            'lr_identifier': [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_valdgjue_id"
            }],
            'version': '1.0.1'
        }
        gen_serializer_2 = GenericLanguageResourceSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)
        query = {'corpus': 'Full'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')['corpus'][1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'p')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['corpus']), 2)

    def test_lr_lookup_superuser_corpus_generic(self):
        user, token = self.admin, self.admin_token
        query = {'corpus': 'res'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['corpus']), 4)

    def test_lr_lookup_superuser_tool_both(self):
        user, token = self.admin, self.admin_token
        query = {'tool_service': 'annie'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')['tool_service'][1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'p')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['tool_service']), 2)

    def test_lr_lookup_superuser_tool_generic(self):
        user, token = self.admin, self.admin_token
        query = {'tool_service': 'res'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['tool_service']), 4)

    def test_lr_lookup_superuser_lcr_full(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        raw_data_published = json.loads(json_str)
        raw_data_published['described_entity']['resource_name'] = {
            "en": "full title of a lcr 2"
        }
        raw_data_published['described_entity']['resource_short_name'] = {}
        raw_data_published['described_entity']['lr_identifier'] = []
        serializer_published = MetadataRecordSerializer(data=raw_data_published)
        if serializer_published.is_valid():
            instance_published = serializer_published.save()
            instance_published.management_object.status = 'p'
            instance_published.management_object.save()
        else:
            LOGGER.info(serializer_published.errors)
        LOGGER.info('Setup has finished')
        gen_data_2 = {
            'resource_name': {
                'en': 'fullt'
            },
            'resource_Short_name': {
                'en': 're'
            },
            'lr_identifier': [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_valhjdgjue_id"
            }],
            'version': '1.0.1'
        }
        gen_serializer_2 = GenericLanguageResourceSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)
        query = {'lcr': 'full'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')['lcr'][1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'p')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['lcr']), 2)

    def test_lr_lookup_superuser_lcr_generic(self):
        user, token = self.admin, self.admin_token
        query = {'lcr': 'res'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['lcr']), 4)

    def test_lr_lookup_superuser_ld_full(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        raw_data_published = json.loads(json_str)
        raw_data_published['described_entity']['resource_name'] = {
            "en": "full title of a ld 2"
        }
        raw_data_published['described_entity']['resource_short_name'] = {}
        raw_data_published['described_entity']['lr_identifier'] = []
        serializer_published = MetadataRecordSerializer(data=raw_data_published)
        if serializer_published.is_valid():
            instance_published = serializer_published.save()
            instance_published.management_object.status = 'p'
            instance_published.management_object.save()
        else:
            LOGGER.info(serializer_published.errors)
        LOGGER.info('Setup has finished')
        gen_data_2 = {
            'resource_name': {
                'en': 'fullt'
            },
            'resource_Short_name': {
                'en': 're'
            },
            'lr_identifier': [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_valhjdgfdsjue_id"
            }],
            'version': '1.0.1'
        }
        gen_serializer_2 = GenericLanguageResourceSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)
        query = {'ld': 'full'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')['ld'][1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'p')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['ld']), 2)

    def test_lr_lookup_superuser_ld_generic(self):
        user, token = self.admin, self.admin_token
        query = {'ld': 'res'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')['ld']), 4)

    def test_cannot_lr_lookup_anonymous(self):
        query = {'query_all': 'res'}
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_lr_lookup_consumer(self):
        query = {'query_all': 'res'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_lr_lookup_validator(self):
        query = {'query_all': 'res'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_lr_lookup_content_manager(self):
        query = {'query_all': 'res'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_lr_lookup_provider(self):
        query = {'query_all': 'res'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', LR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestDocumentLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/document.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer_deleted = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_deleted.is_valid():
            cls.instance_deleted = cls.serializer_deleted.save()
            cls.instance_deleted.management_object.status = 'p'
            cls.instance_deleted.management_object.deleted = True
            cls.instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_published = json.loads(cls.json_str)
        cls.raw_data_published['described_entity']['title'] = {
            "en": "PyPDD: a positive degree day model for glacier surface mass balance 2"
        }
        cls.raw_data_published['described_entity']['alternative_title'] = []
        cls.raw_data_published['described_entity']['document_identifier'] = []
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_np = json.loads(cls.json_str)
        cls.raw_data_np['described_entity']['title'] = {
            "en": "np doc"
        }
        cls.raw_data_np['described_entity']['alternative_title'] = []
        cls.raw_data_np['described_entity']['document_identifier'] = []
        cls.serializer_np = MetadataRecordSerializer(data=cls.raw_data_np)
        if cls.serializer_np.is_valid():
            cls.instance_np = cls.serializer_np.save()
            cls.instance_np.management_object.curator = cls.admin
            cls.instance_np.management_object.save()
        else:
            LOGGER.info(cls.serializer_np.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            "document_identifier": [{
                "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                "value": "https://doi.org/10.5281/zenodo.3467639.21351325"
            }],
            "title": {
                "en": "totally not the same doc"
            }
        }
        gen_serializer = GenericDocumentSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)
        gen_data_2 = {
            "document_identifier": [{
                "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                "value": "https://doi.org/10.5281/zenodo.3467639.21352341325"
            }],
            "title": {
                "en": "pypdd g"
            }
        }
        gen_serializer_2 = GenericDocumentSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_document_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'pypdd'}
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 3)

    def test_document_lookup_superuser_np(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'np doc'}
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'i')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_document_lookup_superuser_np_with_different_user(self):
        user, token = self.provider, self.provider_token
        query = {'query': 'np doc'}
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_document_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_document_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_document_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_document_lookup_anonymous(self):
        query = {'query': 'bibl'}
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_document_lookup_consumer(self):
        query = {'query': 'bibl'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_document_lookup_validator(self):
        query = {'query': 'bibl'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_document_lookup_content_manager(self):
        query = {'query': 'bibl'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_document_lookup_provider(self):
        query = {'query': 'bibl'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', DOC_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestLicenceTermsLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer_deleted = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_deleted.is_valid():
            cls.instance_deleted = cls.serializer_deleted.save()
            cls.instance_deleted.management_object.status = 'p'
            cls.instance_deleted.management_object.deleted = True
            cls.instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_published = json.loads(cls.json_str)
        cls.raw_data_published['described_entity']['licence_terms_name'] = {
            "en": "BSD Zero Clause License 2"
        }
        cls.raw_data_published['described_entity']['licence_terms_short_name'] = {
            "en": "0BSD ZCL 2"
        }
        cls.raw_data_published['described_entity']['licence_identifier'] = []
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_np = json.loads(cls.json_str)
        cls.raw_data_np['described_entity']['licence_terms_name'] = {
            "en": "np licence"
        }
        cls.raw_data_np['described_entity']['licence_terms_short_name'] = {
            "en": "np short licence"
        }
        cls.raw_data_np['described_entity']['licence_identifier'] = []
        cls.raw_data_np['described_entity']['licence_terms_url'] = ['http://www.licenadscetermurl.com']

        cls.serializer_np = MetadataRecordSerializer(data=cls.raw_data_np)
        if cls.serializer_np.is_valid():
            cls.instance_np = cls.serializer_np.save()
            cls.instance_np.management_object.curator = cls.admin
            cls.instance_np.management_object.save()
        else:
            LOGGER.info(cls.serializer_np.errors)
        LOGGER.info('Setup has finished')
        gen_data = {'licence_terms_name': {'en': 'licence term name'},
                    'licence_terms_url': ['http://www.licensdcetermurl.com'],
                    "condition_of_use": [
                        "http://w3id.org/meta-share/meta-share/unspecified"
                    ],
                    'licence_identifier': [
                        {
                            "value": "0BSSFasdsd",
                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX"
                        }
                    ]}
        gen_serializer = GenericLicenceTermsSerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)
        gen_data_2 = {'licence_terms_name': {'en': 'BSD l'},
                      'licence_terms_url': ['http://www.licenceteasdrmurl.com'],
                      "condition_of_use": [
                          "http://w3id.org/meta-share/meta-share/unspecified"
                      ],
                      'licence_identifier': [
                          {
                              "value": "0BSSasdFasd",
                              "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX"
                          }
                      ]}
        gen_serializer_2 = GenericLicenceTermsSerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_licence_terms_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'BSD'}
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[1].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 3)

    def test_licence_terms_lookup_superuser_np(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'np licence'}
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'i')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_licence_terms_lookup_superuser_np_with_different_user(self):
        user, token = self.provider, self.provider_token
        query = {'query': 'np licence'}
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_licence_terms_lookup_superuser_short_name(self):
        user, token = self.admin, self.admin_token
        query = {'query': '0BSD'}
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 2)

    def test_licence_terms_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_licence_terms_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_licence_terms_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_licence_terms_lookup_anonymous(self):
        query = {'query': 'BSD'}
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_licence_terms_lookup_consumer(self):
        query = {'query': 'BSD'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_licence_terms_lookup_validator(self):
        query = {'query': 'BSD'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_licence_terms_lookup_content_manager(self):
        query = {'query': 'BSD'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_licence_terms_lookup_provider(self):
        query = {'query': 'BSD'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', LICENCE_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestRepositoryLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/repository.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer_deleted = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_deleted.is_valid():
            cls.instance_deleted = cls.serializer_deleted.save()
            cls.instance_deleted.management_object.status = 'p'
            cls.instance_deleted.management_object.deleted = True
            cls.instance_deleted.management_object.save()
        else:
            LOGGER.info(cls.serializer_deleted.errors)
        LOGGER.info('Setup has finished')
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_published = json.loads(cls.json_str)
        cls.raw_data_published['described_entity']['repository_name'] = {
            "en": "Repo name 2"
        }
        cls.raw_data_published['described_entity']['repository_url'] = "http://www.repourlpublished.com"
        cls.raw_data_published['described_entity']['repository_additional_name'] = {}
        cls.raw_data_published['described_entity']['repository_identifier'] = []
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data_np = json.loads(cls.json_str)
        cls.raw_data_np['described_entity']['repository_name'] = {
            "en": "np r"
        }
        cls.raw_data_np['described_entity']['repository_url'] = "http://www.nprepourlpublished.com"
        cls.raw_data_np['described_entity']['repository_additional_name'] = {}
        cls.raw_data_np['described_entity']['repository_identifier'] = []
        cls.serializer_np = MetadataRecordSerializer(data=cls.raw_data_np)
        if cls.serializer_np.is_valid():
            cls.instance_np = cls.serializer_np.save()
            cls.instance_np.management_object.curator = cls.admin
            cls.instance_np.management_object.save()
        else:
            LOGGER.info(cls.serializer_np.errors)
        LOGGER.info('Setup has finished')
        gen_data = {
            'repository_name': {'en': 'different name'},
            'repository_url': "http://www.repo2dif.com",
            'repository_identifier': [
                {
                    "repository_identifier_scheme": "http://purl.org/spar/datacite/handle",
                    "value": "0000-0034-234-734-1523f"
                }]
        }
        gen_serializer = GenericRepositorySerializer(data=gen_data)
        if gen_serializer.is_valid():
            gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)
        gen_data_2 = {
            'repository_name': {'en': 'repo n'},
            'repository_url': "http://www.repods2difs.com",
            'repository_identifier': [
                {
                    "repository_identifier_scheme": "http://purl.org/spar/datacite/handle",
                    "value": "0000-0034-234-723434-1523s"
                }]
        }
        gen_serializer_2 = GenericRepositorySerializer(data=gen_data_2)
        if gen_serializer_2.is_valid():
            gen_instance_2 = gen_serializer_2.save()
        else:
            LOGGER.info(gen_serializer_2.errors)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_repository_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'repo'}
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        
        md_id = response.get('json_content')[2].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 5)

    def test_repository_lookup_superuser_np(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'np r'}
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        
        md_id = response.get('json_content')[0].get('pk')
        mdr = MetadataRecord.objects.get(described_entity__pk=md_id)
        self.assertEquals(mdr.management_object.status, 'i')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_repository_lookup_superuser_np_fails_with_different_user(self):
        user, token = self.provider, self.provider_token
        query = {'query': 'np r'}
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_repository_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch'}
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_repository_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_repository_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': ''}
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_repository_lookup_anonymous(self):
        query = {'query': 'repo'}
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_repository_lookup_consumer(self):
        query = {'query': 'repo'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_repository_lookup_validator(self):
        query = {'query': 'repo'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_repository_lookup_content_manager(self):
        query = {'query': 'repo'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_repository_lookup_provider(self):
        query = {'query': 'repo'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', REPO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestKeywordLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['described_entity']['keyword'] = [{'en': 'keyword1'}, {'en': 'keyword2'}, {'en': 'test'}]
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_kw_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'organization': 'key',
                 'lang': 'en'}
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        md_id = response.get('json_content')[1].get('pk')
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 2)

    def test_kw_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'organization': 'nomatch',
                 'lang': 'en'}
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_kw_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_kw_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'organization': '',
                 'lang': 'en'}
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_kw_lookup_superuser_fails_with_no_lang(self):
        user, token = self.admin, self.admin_token
        query = {'organization': 'key'}
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_kw_lookup_anonymous(self):
        query = {'organization': 'key',
                 'lang': 'en'}
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_kw_lookup_consumer(self):
        query = {'organization': 'key',
                 'lang': 'en'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_kw_lookup_validator(self):
        query = {'organization': 'key',
                 'lang': 'en'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_kw_lookup_content_manager(self):
        query = {'organization': 'key',
                 'lang': 'en'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_kw_lookup_provider(self):
        query = {'organization': 'key',
                 'lang': 'en'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', KW_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestServiceOfferedLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['described_entity']['service_offered'] = [{'en': 'software development'}, {'en': 'test'}]
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_so_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'sof',
                 'lang': 'en'}
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_so_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch',
                 'lang': 'en'}
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_so_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def test_so_lookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': '',
                 'lang': 'en'}
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_so_lookup_superuser_fails_with_no_lang(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'sof'}
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_so_lookup_anonymous(self):
        query = {'query': 'sof',
                 'lang': 'en'}
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_so_lookup_consumer(self):
        query = {'query': 'sof',
                 'lang': 'en'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_so_lookup_validator(self):
        query = {'query': 'sof',
                 'lang': 'en'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_so_lookup_content_manager(self):
        query = {'query': 'sof',
                 'lang': 'en'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_so_lookup_provider(self):
        query = {'query': 'sof',
                 'lang': 'en'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', SO_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)


class TestCorpusRegisterLookupEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.json_file = open('registry/tests/fixtures/corpus.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer_published = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info(cls.serializer_published.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_register_lookup_superuser(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'aca',
                 'lang': 'en'}
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 1)

    def test_register_lookup_superuser_no_match(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'nomatch',
                 'lang': 'en'}
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(len(response.get('json_content')), 0)

    def test_register_lookup_superuser_fails_with_no_query_keyword(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 400)

    def testregisterlookup_superuser_fails_with_empty_query(self):
        user, token = self.admin, self.admin_token
        query = {'query': '',
                 'lang': 'en'}
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def testregisterlookup_superuser_fails_with_no_lang(self):
        user, token = self.admin, self.admin_token
        query = {'query': 'aca'}
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_register_lookup_anonymous(self):
        query = {'query': 'aca',
                 'lang': 'en'}
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client, data=query)
        
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_register_lookup_consumer(self):
        query = {'query': 'aca',
                 'lang': 'en'}
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_register_lookup_validator(self):
        query = {'query': 'aca',
                 'lang': 'en'}
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_register_lookup_content_manager(self):
        query = {'query': 'aca',
                 'lang': 'en'}
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)

    def test_register_lookup_provider(self):
        query = {'query': 'aca',
                 'lang': 'en'}
        user, token = self.provider, self.provider_token
        response = api_call('GET', CR_LOOKUP_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=query)
        self.assertEquals(response.get('status_code'), 200)
