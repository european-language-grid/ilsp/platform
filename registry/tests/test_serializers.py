import copy
import json
import logging
from collections import OrderedDict

import xmltodict
from django.conf import settings
from django.contrib.auth import get_user_model

from processing.models import RegisteredLTService
from test_utils import SerializerTestCase
from nested_lookup import nested_delete
from rest_framework.exceptions import ValidationError

import registry.serializers as r_s
from management.models import (
    Manager, PUBLISHED, ContentFile
)
from management.serializers import ContentFileSerializer
from registry import choices
from registry.models import (
    MetadataRecordIdentifier, GenericMetadataRecord,
    SoftwareDistribution, DatasetDistribution, Language, Organization, Project, MetadataRecord
)
from registry.registry_identifier import get_registry_identifier
from registry.serializers_display import (
    meet_conditions_to_display_full_mdr_for_generic_entity,
    meet_conditions_to_display_full_mdr_for_generic_mdr
)

LOGGER = logging.getLogger(__name__)


class TestMetadataRecordIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.mdri = cls.json_data['metadata_record_identifier']
        cls.serializer = r_s.MetadataRecordIdentifierSerializer(data=cls.mdri)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.mdri.keys():
            if i not in r_s.MetadataRecordIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_mdri_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.MetadataRecordIdentifierSerializer.Meta.fields)

    def test_mdri_serializer_fails_with_bad_data(self):
        data = self.serializer.data
        data['metadata_record_identifier_scheme'] = 'fail'
        serializer = r_s.MetadataRecordIdentifierSerializer(self.instance,
                                                            data=data)
        self.assertFalse(serializer.is_valid())

    def test_scheme_no_update_when_already_registry_scheme(self):
        initial_scheme = self.instance.metadata_record_identifier_scheme
        data = self.serializer.data
        data[
            'metadata_record_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/OpenAIRE'
        serializer = r_s.MetadataRecordIdentifierSerializer(self.instance,
                                                            data=data)
        if serializer.is_valid():
            instance = serializer.save()
            scheme_after_update = instance.metadata_record_identifier_scheme
            self.assertEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_scheme_update_with_registry_scheme(self):
        json_data_not_registry = json.loads(self.json_str)
        data_not_registry = json_data_not_registry['metadata_record_identifier']
        data_not_registry[
            'metadata_record_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/OpenAIRE'
        serializer = r_s.MetadataRecordIdentifierSerializer(data=data_not_registry)
        if serializer.is_valid():
            instance = serializer.save()
        initial_scheme = instance.metadata_record_identifier_scheme
        data = serializer.data
        data[
            'metadata_record_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        serializer_registry = r_s.MetadataRecordIdentifierSerializer(instance,
                                                                     data=data)
        if serializer_registry.is_valid():
            instance_registry = serializer_registry.save()
            scheme_after_update = instance_registry.metadata_record_identifier_scheme
            self.assertNotEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestLanguageResourceIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.lri = {
            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
            'value': "doi_value_id"
        }
        cls.serializer = r_s.LRIdentifierSerializer(data=cls.lri)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.lri.keys():
            if i not in r_s.LRIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_lri_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LRIdentifierSerializer.Meta.fields)

    def test_lri_serializer_fails_with_bad_data(self):
        data_fail = {
            'lr_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.LRIdentifierSerializer(self.instance, data=data_fail)
        self.assertFalse(serializer.is_valid())

    def test_scheme_no_update_when_already_registry_scheme(self):
        initial_scheme = self.instance.lr_identifier_scheme
        data = self.serializer.data
        data[
            'lr_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/other'
        serializer = r_s.LRIdentifierSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            scheme_after_update = instance.lr_identifier_scheme
            self.assertEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_scheme_update_with_registry_scheme(self):
        data_not_registry = {
            'lr_identifier_scheme': 'http://w3id.org/meta-share/meta-share/other',
            'value': "doi_value_id"
        }
        serializer = r_s.LRIdentifierSerializer(data=data_not_registry)
        if serializer.is_valid():
            instance = serializer.save()
        initial_scheme = instance.lr_identifier_scheme
        data = serializer.data
        data[
            'lr_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        serializer_registry = r_s.LRIdentifierSerializer(instance, data=data)
        if serializer_registry.is_valid():
            instance_registry = serializer_registry.save()
            scheme_after_update = instance_registry.lr_identifier_scheme
            self.assertNotEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestGenericMetadataRecordSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.mdri = cls.json_data['metadata_record_identifier']
        cls.mdr_identifier = MetadataRecordIdentifier.objects.create(**cls.mdri)
        cls.gmdr = GenericMetadataRecord.objects.create(
            metadata_record_identifier=cls.mdr_identifier)
        cls.serializer = r_s.GenericMetadataRecordSerializer(instance=cls.gmdr)

    def test_gmdr_serializer_contains_expected_fields(self):
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericMetadataRecordSerializer.Meta.fields)

    def test_to_xml_representation_no_xml_representation(self):
        xml_representation = r_s.GenericMetadataRecordSerializer(instance=self.gmdr).to_xml_representation()
        self.assertTrue(r_s.GenericMetadataRecordSerializer.Meta.model.schema_fields[
                            'metadata_record_identifier'] not in xml_representation)


class TestPersonalIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.pi = {
            "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
            "value": "registry_value"
        }
        cls.serializer = r_s.PersonalIdentifierSerializer(data=cls.pi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.pi.keys():
            if i not in r_s.PersonalIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_pi_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.PersonalIdentifierSerializer.Meta.fields)

    def test_pi_serializer_fails_with_bad_data(self):
        data_fail = {
            'personal_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.PersonalIdentifierSerializer(self.instance,
                                                      data=data_fail)
        self.assertFalse(serializer.is_valid())

    def test_scheme_no_update_when_already_registry_scheme(self):
        initial_scheme = self.instance.personal_identifier_scheme
        data = self.serializer.data
        data[
            'personal_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/other'
        serializer = r_s.PersonalIdentifierSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            scheme_after_update = instance.personal_identifier_scheme
            self.assertEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_scheme_update_with_registry_scheme(self):
        data_not_registry = {
            'personal_identifier_scheme': 'http://w3id.org/meta-share/meta-share/other',
            'value': "doi_value_id"
        }
        serializer = r_s.PersonalIdentifierSerializer(data=data_not_registry)
        if serializer.is_valid():
            instance = serializer.save()
        initial_scheme = instance.personal_identifier_scheme
        data = serializer.data
        data[
            'personal_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        serializer_registry = r_s.PersonalIdentifierSerializer(instance, data=data)
        if serializer_registry.is_valid():
            instance_registry = serializer_registry.save()
            scheme_after_update = instance_registry.personal_identifier_scheme
            self.assertNotEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestGenericPersonSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/person.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.GenericPersonSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.gp_instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_generic_person_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericPersonSerializer.Meta.fields)

    def test_generic_person_serializer_does_not_update(self):
        self.assertTrue(self.serializer.is_valid())
        data = copy.deepcopy(self.raw_data)
        data['surname'] = {
            'en': 'failed update name'
        }
        gen_serializer = r_s.GenericPersonSerializer(self.gp_instance, data)
        gen_serializer.is_valid()
        gen_instance = gen_serializer.save()
        self.assertTrue(gen_instance == self.gp_instance)

    def test_generic_person_serializer_passes_when_no_email_given(self):
        no_email_data = copy.deepcopy(self.raw_data)
        no_email_data['email'] = None
        serializer = r_s.GenericPersonSerializer(data=no_email_data)
        if serializer.is_valid():
            gp_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericPersonSerializer.Meta.fields)

    def test_generic_person_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_identifier'] = [
            {
                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.GenericPersonSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generic_person_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_identifier'] = [{
            'personal_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }]
        serializer = r_s.GenericPersonSerializer(self.gp_instance,
                                                 data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_by_pi(self):
        self.assertTrue(self.gp_instance)

    def test_retrieve_instance_by_email(self):
        no_pi_data = copy.deepcopy(self.raw_data)
        no_pi_data['personal_identifier'] = []
        serializer = r_s.GenericPersonSerializer(data=no_pi_data)
        if serializer.is_valid():
            gp_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(gp_instance)

    def test_pi_created_and_instance_retrieved_when_none_given(self):
        no_pi_email_data = copy.deepcopy(self.raw_data)
        no_pi_email_data['personal_identifier'] = []
        no_pi_email_data['email'] = []
        serializer = r_s.GenericPersonSerializer(data=no_pi_email_data)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertFalse(data['personal_identifier'] == [])
        self.assertTrue(instance)

    def test_to_display_representation_works(self):
        d_repr = self.serializer.to_display_representation(self.gp_instance)
        self.assertEquals(tuple(d_repr['personal_identifier'].keys()),
                          tuple(['field_label', 'field_value']))
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_display_representation_works_when_no_email_is_given(self):
        no_email_data = copy.deepcopy(self.raw_data)
        del no_email_data['email']
        serializer = r_s.GenericPersonSerializer(data=no_email_data)
        if serializer.is_valid():
            gp_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        d_repr = serializer.to_display_representation(gp_instance)
        self.assertEquals(tuple(d_repr['personal_identifier'].keys()),
                          tuple(['field_label', 'field_value']))
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_display_representation_deletes_email(self):
        self.assertTrue(self.serializer.is_valid())
        d_repr = self.serializer.to_display_representation(self.gp_instance)
        self.assertEquals(tuple(d_repr['personal_identifier'].keys()),
                          tuple(['field_label', 'field_value']))
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.GenericPersonSerializer(instance=self.gp_instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.PersonalIdentifierSerializer.Meta.model.schema_fields[
                'personal_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.GenericPersonSerializer.Meta.model.schema_fields['personal_identifier']]
        ]))

    def test_to_xml_representation_no_email(self):
        xml_representation = r_s.GenericPersonSerializer(instance=self.gp_instance).to_xml_representation()
        self.assertTrue(r_s.GenericPersonSerializer.Meta.model.schema_fields['email'] not in xml_representation)

    def test_generic_person_serializer_fails_with_name_no_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['name'] = {
            'en': 'Mary Smith',
        }
        serializer = r_s.GenericPersonSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_generic_person_serializer_fails_with_name_for_info_context_false(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['name'] = {
            'en': 'Mary Smith',
        }
        context = {
            'for_information_only': False
        }
        serializer = r_s.GenericPersonSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())

    def test_generic_person_serializer_success_with_name_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['name'] = {
            'en': 'Mary Smith',
        }
        context = {
            'for_information_only': True
        }
        serializer = r_s.GenericPersonSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())


class TestRepositoryIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.ri = {
            'repository_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
            'value': 'repository test value',
        }
        cls.serializer = r_s.RepositoryIdentifierSerializer(data=cls.ri)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.ri.keys():
            if i not in r_s.RepositoryIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_ri_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.RepositoryIdentifierSerializer.Meta.fields)

    def test_ri_serializer_fails_with_bad_data(self):
        data_fail = {
            'repository_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.RepositoryIdentifierSerializer(self.instance,
                                                        data=data_fail)
        self.assertFalse(serializer.is_valid())

    def test_scheme_no_update_when_already_registry_scheme(self):
        initial_scheme = self.instance.repository_identifier_scheme
        data = self.serializer.data
        data[
            'repository_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/re3data'
        serializer = r_s.RepositoryIdentifierSerializer(self.instance,
                                                        data=data)
        if serializer.is_valid():
            instance = serializer.save()
            scheme_after_update = instance.repository_identifier_scheme
            self.assertEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestGenericRepositorySerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/repository.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.GenericRepositorySerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_generic_repository_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericRepositorySerializer.Meta.fields)

    def test_generic_repository_serializer_does_not_update(self):
        self.assertTrue(self.serializer.is_valid())
        data = copy.deepcopy(self.raw_data)
        data['repository_name'] = {
            'en': 'failed update name'
        }
        gen_serializer = r_s.GenericRepositorySerializer(self.instance, data)
        gen_serializer.is_valid()
        gen_instance = gen_serializer.save()
        self.assertTrue(gen_instance == self.instance)

    def test_generic_repository_serializer_fails_when_same_schemes_are_given(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['repository_identifier'] = [
            {
                "repository_identifier_scheme": 'http://purl.org/spar/datacite/handle',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "repository_identifier_scheme": 'http://purl.org/spar/datacite/handle',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.GenericRepositorySerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generic_repository_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['repository_identifier'] = [{
            'repository_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }]
        serializer = r_s.GenericRepositorySerializer(self.instance,
                                                     data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.GenericRepositorySerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.RepositoryIdentifierSerializer.Meta.model.schema_fields[
                'repository_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in
            xml_representation[r_s.GenericRepositorySerializer.Meta.model.schema_fields['repository_identifier']]
        ]))


class TestAdditionalInfoSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.raw_data = {
            'landing_page': 'http://www.landing.com',
        }
        cls.serializer = r_s.additionalInfoSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.additionalInfoSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_additional_info_serializer_passes_only_with_landing_page(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.additionalInfoSerializer.Meta.fields)

    def test_additional_info_serializer_passes_only_with_email(self):
        raw_data = {
            'email': 'lone@email.com'
        }
        serializer = r_s.additionalInfoSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.additionalInfoSerializer.Meta.fields)

    def test_additional_info_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['landing_page'] = 'not a url'
        serializer = r_s.additionalInfoSerializer(self.instance,
                                                  data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_additional_info_serializer_validation_fails_when_data_has_both(
            self):
        raw_data = {
            'landing_page': 'http://www.landing.com',
            'email': 'lone@email.com'
        }
        serializer = r_s.additionalInfoSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)

    def test_additional_info_serializer_validation_fails_when_data_has_none(
            self):
        raw_data = dict()
        serializer = r_s.additionalInfoSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)

    def test_additional_info_serializer_validation_fails_with_blank_fields(
            self):
        raw_data = {
            'landing_page': '',
            'email': ''
        }
        serializer = r_s.additionalInfoSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)

    def test_additional_info_serializer_validation_fails_with_one_blank_field(
            self):
        raw_data = {
            'landing_page': ''
        }
        serializer = r_s.additionalInfoSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)

    def test_to_display_shows_only_email(self):
        raw_data = {
            'email': 'www.mail@example.com'
        }
        serializer = r_s.additionalInfoSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        d_repr = r_s.additionalInfoSerializer(instance).to_display_representation(instance)
        self.assertTrue(d_repr['email'])

    def test_to_display_shows_only_landing_page(self):
        raw_data = {
            'landing_page': 'http://www.landing.com',
        }
        serializer = r_s.additionalInfoSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        d_repr = r_s.additionalInfoSerializer(instance).to_display_representation(instance)
        self.assertTrue(d_repr['landing_page'])


class TestOrganizationIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.orgi = {
            'organization_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
            'value': "doi_value_id"
        }
        cls.serializer = r_s.OrganizationIdentifierSerializer(data=cls.orgi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.orgi.keys():
            if i not in r_s.OrganizationIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_organization_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.OrganizationIdentifierSerializer.Meta.fields)

    def test_organization_identifier_serializer_fails_with_bad_data(self):
        data_fail = {
            'organization_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.OrganizationIdentifierSerializer(self.instance,
                                                          data=data_fail)
        self.assertFalse(serializer.is_valid())

    def test_scheme_no_update_when_already_registry_scheme(self):
        initial_scheme = self.instance.organization_identifier_scheme
        data = self.serializer.data
        data[
            'organization_identifier_scheme'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER
        serializer = r_s.OrganizationIdentifierSerializer(self.instance,
                                                          data=data)
        if serializer.is_valid():
            instance = serializer.save()
            scheme_after_update = instance.organization_identifier_scheme
            self.assertEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_scheme_update_with_registry_scheme(self):
        data_not_registry = {
            'organization_identifier_scheme': choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER,
            'value': "doi_value_id"
        }
        serializer = r_s.OrganizationIdentifierSerializer(data=data_not_registry)
        if serializer.is_valid():
            instance = serializer.save()
        initial_scheme = instance.organization_identifier_scheme
        data = serializer.data
        data[
            'organization_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        serializer_registry = r_s.OrganizationIdentifierSerializer(instance,
                                                                   data=data)
        if serializer_registry.is_valid():
            instance_registry = serializer_registry.save()
            scheme_after_update = instance_registry.organization_identifier_scheme
            self.assertNotEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestGenericOrganizationSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.GenericOrganizationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.go_instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)

    def test_generic_organization_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericOrganizationSerializer.Meta.fields)

    def test_generic_organization_serializer_does_not_update(self):
        self.assertTrue(self.serializer.is_valid())
        data = copy.deepcopy(self.raw_data)
        data['organization_name'] = {
            'en': 'failed update name'
        }
        gen_serializer = r_s.GenericOrganizationSerializer(self.go_instance, data)
        gen_serializer.is_valid()
        gen_instance = gen_serializer.save()
        self.assertTrue(gen_instance == self.go_instance)

    def test_generic_organization_serializer_fails_when_same_schemes_are_given(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['organization_identifier'] = [
            {
                "organization_identifier_scheme": 'http://purl.org/spar/datacite/doi',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "organization_identifier_scheme": 'http://purl.org/spar/datacite/doi',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.GenericOrganizationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_generic_organization_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['organization_identifier'] = [{
            'organization_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }]
        serializer = r_s.GenericOrganizationSerializer(self.go_instance,
                                                       data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_by_oi(self):
        self.assertTrue(self.go_instance)

    def test_retrieve_instance_by_website(self):
        no_oi_data = copy.deepcopy(self.raw_data)
        no_oi_data['organization_identifier'] = []
        serializer = r_s.GenericOrganizationSerializer(data=no_oi_data)
        if serializer.is_valid():
            go_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(go_instance)

    def test_oi_created_and_instance_retrieved_when_none_given(self):
        no_oi_website_data = copy.deepcopy(self.raw_data)
        no_oi_website_data['organization_identifier'] = []
        no_oi_website_data['website'] = []
        serializer = r_s.GenericOrganizationSerializer(data=no_oi_website_data)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertFalse(data['organization_identifier'] == [])
        self.assertTrue(instance)

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.GenericOrganizationSerializer(instance=self.go_instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.OrganizationIdentifierSerializer.Meta.model.schema_fields[
                'organization_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in
            xml_representation[r_s.GenericOrganizationSerializer.Meta.model.schema_fields['organization_identifier']]
        ]))


class TestGroupIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.orgi = {
            'organization_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
            'value': "doi_value_id"
        }
        cls.serializer = r_s.GroupIdentifierSerializer(data=cls.orgi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.orgi.keys():
            if i not in r_s.GroupIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_group_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GroupIdentifierSerializer.Meta.fields)

    def test_group_identifier_serializer_fails_with_bad_data(self):
        data_fail = {
            'organization_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.GroupIdentifierSerializer(self.instance,
                                                   data=data_fail)
        self.assertFalse(serializer.is_valid())

    def test_scheme_no_update_when_already_registry_scheme(self):
        initial_scheme = self.instance.organization_identifier_scheme
        data = self.serializer.data
        data[
            'organization_identifier_scheme'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER
        serializer = r_s.GroupIdentifierSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            scheme_after_update = instance.organization_identifier_scheme
            self.assertEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_scheme_update_with_registry_scheme(self):
        data_not_registry = {
            'organization_identifier_scheme': choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER,
            'value': "doi_value_id"
        }
        serializer = r_s.GroupIdentifierSerializer(data=data_not_registry)
        if serializer.is_valid():
            instance = serializer.save()
        initial_scheme = instance.organization_identifier_scheme
        data = serializer.data
        data[
            'organization_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        serializer_registry = r_s.GroupIdentifierSerializer(instance, data=data)
        if serializer_registry.is_valid():
            instance_registry = serializer_registry.save()
            scheme_after_update = instance_registry.organization_identifier_scheme
            self.assertNotEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestGenericGroupSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/group.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.GenericGroupSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.gg_instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_generic_group_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericGroupSerializer.Meta.fields)

    def test_generic_group_serializer_does_not_update(self):
        self.assertTrue(self.serializer.is_valid())
        data = copy.deepcopy(self.raw_data)
        data['organization_name'] = {
            'en': 'failed update name'
        }
        gen_serializer = r_s.GenericGroupSerializer(self.gg_instance, data)
        gen_serializer.is_valid()
        gen_instance = gen_serializer.save()
        self.assertTrue(gen_instance == self.gg_instance)

    def test_generic_group_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['group_identifier'] = [
            {
                "organization_identifier_scheme": 'http://purl.org/spar/datacite/doi',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "organization_identifier_scheme": 'http://purl.org/spar/datacite/doi',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.GenericGroupSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_generic_group_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['group_identifier'] = [{
            'organization_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }]
        serializer = r_s.GenericGroupSerializer(self.gg_instance,
                                                data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_by_gi(self):
        json_file = open('registry/tests/fixtures/group.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        group_data = json_data
        serializer = r_s.MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
            instance.management_object.status = 'p'
            instance.management_object.save()
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))

        registry_identifier_value = get_registry_identifier(instance)
        generic_group = dict()
        generic_group['actor_type'] = 'Group'
        generic_group['organization_name'] = {
            'en': 'group name'}
        generic_group['group_identifier'] = list()
        group_id = dict()
        group_id[
            'organization_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        group_id['value'] = registry_identifier_value
        generic_group['group_identifier'].append(group_id)
        generic_group['website'] = ['http://www.orgwebsite.com']
        group_serializer = r_s.GenericGroupSerializer(
            data=generic_group)
        if group_serializer.is_valid():
            gg_instance = group_serializer.save()
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))

        self.assertEqual(gg_instance.pk, instance.described_entity.pk)

    def test_retrieve_instance_by_website(self):
        no_gi_data = copy.deepcopy(self.raw_data)
        no_gi_data['group_identifier'] = []
        serializer = r_s.GenericGroupSerializer(data=no_gi_data)
        if serializer.is_valid():
            gg_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(gg_instance)

    def test_gi_created_and_instance_retrieved_when_none_given(self):
        no_gi_website_data = copy.deepcopy(self.raw_data)
        no_gi_website_data['group_identifier'] = []
        no_gi_website_data['website'] = []
        serializer = r_s.GenericGroupSerializer(data=no_gi_website_data)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertFalse(data['group_identifier'] == [])
        self.assertTrue(instance)

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.GenericGroupSerializer(instance=self.gg_instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.OrganizationIdentifierSerializer.Meta.model.schema_fields[
                'organization_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.GenericGroupSerializer.Meta.model.schema_fields['group_identifier']]
        ]))


class TestActorSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/person.json', 'r')
        cls.person_str = cls.json_file.read()
        cls.json_file.close()
        cls.person_data = json.loads(cls.person_str)['described_entity']
        cls.person_data['actor_type'] = 'Person'

        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.org_str = cls.json_file.read()
        cls.json_file.close()
        cls.org_data = json.loads(cls.org_str)['described_entity']
        cls.org_data['actor_type'] = 'Organization'

        cls.json_file = open('registry/tests/fixtures/group.json', 'r')
        cls.group_str = cls.json_file.read()
        cls.json_file.close()
        cls.group_data = json.loads(cls.group_str)['described_entity']
        cls.group_data['actor_type'] = 'Group'

    def test_actor_serializer_for_person_contains_expected_fields(self):
        serializer = r_s.ActorSerializer(data=self.person_data)
        if serializer.is_valid():
            gp_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertTrue(tuple(data.keys()), r_s.ActorSerializer.Meta.fields)

    def test_actor_serializer_for_person_fails_with_bad_input(self):
        fail_person_data = copy.deepcopy(self.person_data)
        fail_person_data['actor_type'] = ''
        serializer = r_s.ActorSerializer(data=fail_person_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertRaises(ValidationError)
        self.assertFalse(serializer.is_valid())

    def test_actor_serializer_for_person_validation_fails(self):
        fail_person_data = copy.deepcopy(self.person_data)
        fail_person_data['actor_type'] = None
        serializer = r_s.ActorSerializer(data=fail_person_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertRaises(ValidationError)
        self.assertFalse(serializer.is_valid())

    def test_actor_serializer_for_organization_has_expected_fields(self):
        serializer = r_s.ActorSerializer(data=self.org_data)
        if serializer.is_valid():
            go_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertTrue(tuple(data.keys()), r_s.ActorSerializer.Meta.fields)

    def test_actor_serializer_for_organization_fails_with_bad_input(self):
        fail_org_data = copy.deepcopy(self.org_data)
        fail_org_data['actor_type'] = 'fail'
        serializer = r_s.ActorSerializer(data=fail_org_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_actor_serializer_for_organization_validation_fails(self):
        fail_org_data = copy.deepcopy(self.org_data)
        del fail_org_data['actor_type']
        serializer = r_s.ActorSerializer(data=fail_org_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_actor_serializer_for_group_has_expected_fields(self):
        serializer = r_s.ActorSerializer(data=self.group_data)
        if serializer.is_valid():
            gg_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertTrue(tuple(data.keys()), r_s.ActorSerializer.Meta.fields)

    def test_actor_serializer_for_group_fails_with_fail_input(self):
        fail_group_data = copy.deepcopy(self.group_data)
        fail_group_data['actor_type'] = ''
        serializer = r_s.ActorSerializer(data=fail_group_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_actor_serializer_for_group_fails_with_bad_input(self):
        fail_group_data = copy.deepcopy(self.group_data)
        fail_group_data['actor_type'] = None
        serializer = r_s.ActorSerializer(data=fail_group_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_actor_serializer_fails_with_wrong_actor_type(self):
        person_data = copy.deepcopy(self.person_data)
        person_data['actor_type'] = 'Organization'
        serializer = r_s.ActorSerializer(data=person_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())


class TestDomainIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.di = {
            'domain_classification_scheme': 'http://w3id.org/meta-share/meta-share/ANC_domainClassification',
            'value': "doi_value_id"
        }
        cls.serializer = r_s.DomainIdentifierSerializer(data=cls.di)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.di.keys():
            if i not in r_s.DomainIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_domain_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DomainIdentifierSerializer.Meta.fields)

    def test_domain_identifier_serializer_fails_with_bad_data(self):
        data_fail = {
            'organization_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.DomainIdentifierSerializer(self.instance,
                                                    data=data_fail)
        self.assertFalse(serializer.is_valid())


class TestDomainSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['domain']
        cls.serializer = r_s.DomainSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DomainSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_domain_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()), r_s.DomainSerializer.Meta.fields)

    def test_domain_identifier_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['domain_identifier'] = {
            'domain_classification_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.DomainSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_without_label(self):
        no_label = copy.deepcopy(self.raw_data)
        no_label['category_label'] = None
        serializer = r_s.DomainSerializer(data=no_label)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_without_identifier(self):
        no_id = copy.deepcopy(self.raw_data)
        no_id['domain_identifier'] = None
        serializer = r_s.DomainSerializer(data=no_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertTrue(serializer.is_valid())

    def test_cannot_retrieve_instance_with_blank_id(self):
        blank_id = copy.deepcopy(self.raw_data)
        blank_id['domain_identifier'] = []
        serializer = r_s.DomainSerializer(data=blank_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_with_empty_id(self):
        empty_id = copy.deepcopy(self.raw_data)
        empty_id['domain_identifier'] = {
            'domain_classification_scheme': '',
            'value': ""
        }
        serializer = r_s.DomainSerializer(data=empty_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())


class TestSubjectIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.si = {
            'subject_classification_scheme': 'http://w3id.org/meta-share/meta-share/MeSH_classification',
            'value': "doi_value_id"
        }
        cls.serializer = r_s.SubjectIdentifierSerializer(data=cls.si)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.si.keys():
            if i not in r_s.SubjectIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_subject_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.SubjectIdentifierSerializer.Meta.fields)

    def test_subject_identifier_serializer_fails_with_bad_data(self):
        data_fail = {
            'subject_classification_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.SubjectIdentifierSerializer(self.instance,
                                                     data=data_fail)
        self.assertFalse(serializer.is_valid())


class TestSubjectSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['subject']
        cls.serializer = r_s.SubjectSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.SubjectSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_subject_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()), r_s.SubjectSerializer.Meta.fields)

    def test_subject_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['subject_identifier'] = {
            'subject_classification_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.SubjectSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestProjectIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.pi = {
            'project_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
            'value': "doi_value_id"
        }
        cls.serializer = r_s.ProjectIdentifierSerializer(data=cls.pi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.pi.keys():
            if i not in r_s.ProjectIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_project_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ProjectIdentifierSerializer.Meta.fields)

    def test_project_identifier_serializer_fails_with_bad_data(self):
        fail_data = {
            'project_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.ProjectIdentifierSerializer(self.instance,
                                                     data=fail_data)
        self.assertFalse(serializer.is_valid())

    def test_scheme_no_update_when_already_registry_scheme(self):
        initial_scheme = self.instance.project_identifier_scheme
        data = self.serializer.data
        data[
            'project_identifier_scheme'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER
        serializer = r_s.ProjectIdentifierSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            scheme_after_update = instance.project_identifier_scheme
            self.assertEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_scheme_update_with_registry_scheme(self):
        data_not_registry = {
            'project_identifier_scheme': choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER,
            'value': "doi_value_id"
        }
        serializer = r_s.ProjectIdentifierSerializer(data=data_not_registry)
        if serializer.is_valid():
            instance = serializer.save()
        initial_scheme = instance.project_identifier_scheme
        data = serializer.data
        data[
            'project_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        serializer_registry = r_s.ProjectIdentifierSerializer(instance, data=data)
        if serializer_registry.is_valid():
            instance_registry = serializer_registry.save()
            scheme_after_update = instance_registry.project_identifier_scheme
            self.assertNotEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestGenericProjectSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.GenericProjectSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.gp_instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_generic_project_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericProjectSerializer.Meta.fields)

    def test_generic_project_serializer_does_not_update(self):
        self.assertTrue(self.serializer.is_valid())
        data = copy.deepcopy(self.raw_data)
        data['project_name'] = {
            'en': 'failed update name'
        }
        gen_serializer = r_s.GenericProjectSerializer(self.gp_instance, data)
        gen_serializer.is_valid()
        gen_instance = gen_serializer.save()
        self.assertTrue(gen_instance == self.gp_instance)

    def test_generic_project_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['project_identifier'] = [
            {
                "project_identifier_scheme": 'http://w3id.org/meta-share/meta-share/OpenAIRE',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "project_identifier_scheme": 'http://w3id.org/meta-share/meta-share/OpenAIRE',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.GenericProjectSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_generic_project_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['project_identifier'] = [{
            'project_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }]
        serializer = r_s.GenericProjectSerializer(self.gp_instance,
                                                  data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_by_pi(self):
        self.assertTrue(self.gp_instance)

    def test_retrieve_instance_by_website(self):
        no_pi_data = copy.deepcopy(self.raw_data)
        no_pi_data['project_identifier'] = []
        serializer = r_s.GenericProjectSerializer(data=no_pi_data)
        if serializer.is_valid():
            gp_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(gp_instance)

    def test_pi_created_and_instance_retrieved_when_none_given(self):
        no_pi_or_website_data = copy.deepcopy(self.raw_data)
        no_pi_or_website_data['project_identifier'] = []
        no_pi_or_website_data['website'] = []
        serializer = r_s.GenericProjectSerializer(data=no_pi_or_website_data)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertTrue(instance)

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.GenericProjectSerializer(instance=self.gp_instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.ProjectIdentifierSerializer.Meta.model.schema_fields[
                'project_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.GenericProjectSerializer.Meta.model.schema_fields['project_identifier']]
        ]))


class TestGenericLanguageResourceSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.GenericLanguageResourceSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_generic_language_resource_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericLanguageResourceSerializer.Meta.fields)

    def test_generic_lr_serializer_does_not_update(self):
        self.assertTrue(self.serializer.is_valid())
        data = copy.deepcopy(self.raw_data)
        data['resource_name'] = {
            'en': 'failed update name'
        }
        gen_serializer = r_s.GenericLanguageResourceSerializer(self.instance, data)
        gen_serializer.is_valid()
        gen_instance = gen_serializer.save()
        self.assertTrue(gen_instance == self.instance)

    def test_generic_language_resource_serializer_fails_when_same_schemes_are_given(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lr_identifier'] = [
            {
                "lr_identifier_scheme": 'http://purl.org/spar/datacite/ark',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "lr_identifier_scheme": 'http://purl.org/spar/datacite/ark',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.GenericLanguageResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_generic_language_resource_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['resource_name'] = 'fail'
        serializer = r_s.GenericLanguageResourceSerializer(self.instance,
                                                           data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_by_lri(self):
        self.assertTrue(self.instance)

    def test_lri_created_and_instance_retrieved_when_none_given(self):
        no_lri = copy.deepcopy(self.raw_data)
        no_lri['lr_identifier'] = []
        serializer = r_s.GenericLanguageResourceSerializer(data=no_lri)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertFalse(data['lr_identifier'] == [])
        self.assertTrue(instance)

    def test_from_xml_representation(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = data['ms:MetadataRecord']['ms:DescribedEntity'][
            'ms:LanguageResource']
        xml_data['ms:version'] = ''
        from_xml = r_s.GenericLanguageResourceSerializer().from_xml_representation(
            xml_data=xml_data)
        self.assertEquals(tuple(from_xml.keys()),
                          tuple(['resource_name', 'lr_identifier', 'version']))
        self.assertEquals(from_xml['version'], 'unspecified')
        serializer = r_s.GenericLanguageResourceSerializer(data=from_xml)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())

    def test_from_xml_representation_fails_with_bad_data(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = data['ms:MetadataRecord']['ms:DescribedEntity'][
            'ms:LanguageResource']
        xml_data_fail = OrderedDict(
            [('resourceName', v) if k == 'ms:resourceName' else (k, v) for k, v
             in xml_data.items()])
        from_xml_fail = r_s.GenericLanguageResourceSerializer().from_xml_representation(
            xml_data=xml_data_fail)
        self.assertEquals(from_xml_fail['resource_name'], None)
        serializer = r_s.GenericLanguageResourceSerializer(data=from_xml_fail)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.GenericLanguageResourceSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.LRIdentifierSerializer.Meta.model.schema_fields[
                'lr_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in
            xml_representation[r_s.GenericLanguageResourceSerializer.Meta.model.schema_fields['lr_identifier']]
        ]))


class TestDocumentIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.di = {
            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
            'value': "doi_value_id"
        }
        cls.serializer = r_s.DocumentIdentifierSerializer(data=cls.di)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.di.keys():
            if i not in r_s.DocumentIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_document_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DocumentIdentifierSerializer.Meta.fields)

    def test_document_identifier_serializer_fails_with_bad_data(self):
        fail_data = {
            'document_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.DocumentIdentifierSerializer(self.instance,
                                                      data=fail_data)
        self.assertFalse(serializer.is_valid())

    def test_scheme_no_update_when_already_registry_scheme(self):
        initial_scheme = self.instance.document_identifier_scheme
        data = self.serializer.data
        data[
            'document_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/other'
        serializer = r_s.DocumentIdentifierSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            scheme_after_update = instance.document_identifier_scheme
            self.assertEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_scheme_update_with_registry_scheme(self):
        data_not_registry = {
            'document_identifier_scheme': 'http://w3id.org/meta-share/meta-share/other',
            'value': "doi_value_id"
        }
        serializer = r_s.DocumentIdentifierSerializer(data=data_not_registry)
        if serializer.is_valid():
            instance = serializer.save()
        initial_scheme = instance.document_identifier_scheme
        data = serializer.data
        data[
            'document_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        serializer_registry = r_s.DocumentIdentifierSerializer(instance, data=data)
        if serializer_registry.is_valid():
            instance_registry = serializer_registry.save()
            scheme_after_update = instance_registry.document_identifier_scheme
            self.assertNotEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestGenericDocumentSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/document.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.GenericDocumentSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_generic_document_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericDocumentSerializer.Meta.fields)

    def test_generic_document_serializer_does_not_update(self):
        self.assertTrue(self.serializer.is_valid())
        data = copy.deepcopy(self.raw_data)
        data['title'] = {
            'en': 'failed update name'
        }
        gen_serializer = r_s.GenericDocumentSerializer(self.instance, data)
        gen_serializer.is_valid()
        gen_instance = gen_serializer.save()
        self.assertTrue(gen_instance == self.instance)

    def test_generic_document_serializer_fails_when_same_schemes_are_given(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['document_identifier'] = [
            {
                "document_identifier_scheme": 'http://purl.org/spar/datacite/ark',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "document_identifier_scheme": 'http://purl.org/spar/datacite/ark',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.GenericDocumentSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_generic_document_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['title'] = 'failed title'
        serializer = r_s.GenericDocumentSerializer(self.instance,
                                                   data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_by_di(self):
        self.assertTrue(self.instance)

    def test_di_created_and_instance_retrieved_when_none_given(self):
        no_di = copy.deepcopy(self.raw_data)
        no_di['document_identifier'] = []
        serializer = r_s.GenericDocumentSerializer(data=no_di)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertFalse(data['document_identifier'] == [])
        self.assertTrue(instance)

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.GenericDocumentSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.DocumentIdentifierSerializer.Meta.model.schema_fields[
                'document_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in
            xml_representation[r_s.GenericDocumentSerializer.Meta.model.schema_fields['document_identifier']]
        ]))


class TestActualUseSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['actual_use']
        cls.serializer = r_s.ActualUseSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ActualUseSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_actual_use_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ActualUseSerializer.Meta.fields)

    def test_actual_use_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['used_in_application'] = 'fail'
        serializer = r_s.ActualUseSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_display_representation_works(self):
        dis_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(dis_repr['used_in_application'] == {
            'field_label': {
                'en': 'Used in application'
            },
            'field_value': [{
                'value': 'Translation'
            }]
        })

    def test_from_xml_representation(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource'][
                'ms:actualUse']
        from_xml = r_s.ActualUseSerializer().from_xml_representation(
            xml_data=xml_data)
        self.assertEquals(tuple(from_xml.keys()), tuple(
            ['used_in_application', 'has_outcome', 'usage_project',
             'usage_report', 'actual_use_details']))
        self.assertEquals(from_xml['used_in_application'],
                          ['http://w3id.org/meta-share/omtd-share/Translation'])
        serializer = r_s.ActualUseSerializer(data=from_xml)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())

    def test_from_xml_representation_fails_with_bad_data(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource'][
                'ms:actualUse']
        xml_data_fail = copy.deepcopy(xml_data)
        xml_data_fail['ms:hasOutcome'] = OrderedDict(
            [('resourceName', v) if k == 'ms:resourceName' else (k, v) for k, v
             in xml_data['ms:hasOutcome'].items()])
        from_xml_fail = r_s.ActualUseSerializer().from_xml_representation(
            xml_data=xml_data_fail)
        self.assertEquals(from_xml_fail['has_outcome'][0]['resource_name'],
                          None)
        serializer = r_s.ActualUseSerializer(data=from_xml_fail)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_lt_class(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['used_in_application'] = [
            choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT,
            'lt class non CV value'
        ]
        serializer = r_s.ActualUseSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        used_in_app_xml_field_name = r_s.ActualUseSerializer.Meta.model.schema_fields['used_in_application']
        self.assertEquals(list(xml_data[used_in_app_xml_field_name][0].keys()), ['ms:LTClassRecommended'])
        self.assertEquals(list(xml_data[used_in_app_xml_field_name][0].values()),
                          [choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT])
        self.assertEquals(list(xml_data[used_in_app_xml_field_name][1].keys()), ['ms:LTClassOther'])
        self.assertEquals(list(xml_data[used_in_app_xml_field_name][1].values()),
                          ['lt class non CV value'])


class TestValidationSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['validation']
        cls.serializer = r_s.ValidationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ValidationSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_validation_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ValidationSerializer.Meta.fields)

    def test_validation_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['validation_type'] = 'fail'
        serializer = r_s.ValidationSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestRelationSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['relation']
        cls.serializer = r_s.RelationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.RelationSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_relation_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.RelationSerializer.Meta.fields)

    def test_relation_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['relation_type'] = 'fail'
        serializer = r_s.RelationSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_relation_serializer_fails_without_lr(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['related_lr'] = ''
        serializer = r_s.RelationSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestLanguageSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['language']
        cls.serializer = r_s.LanguageSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LanguageSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_language_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LanguageSerializer.Meta.fields)

    def test_language_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language_id'] = ''
        serializer = r_s.LanguageSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_language_instance_creation_of_language_tag_works_correctly(self):
        serializer = r_s.LanguageSerializer(data=self.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(self.serializer.is_valid())
        self.assertEquals(str(instance.language_tag),
                          f'{instance.language_id}-{instance.script_id}-{instance.region_id}-' + "-".join(
                              instance.variant_id))

    def test_empty_string_save_null_script_id(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['script_id'] = ''
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEqual(instance.script_id, None)

    def test_empty_string_save_null_region_id(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['region_id'] = ''
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEqual(instance.region_id, None)

    def test_language_instance_creation_of_language_tag_with_suppressed_script(self):
        raw_data = copy.deepcopy(self.raw_data)
        # en, Latn --> en
        raw_data['language_id'] = 'en'
        raw_data['script_id'] = 'Latn'
        raw_data['region_id'] = None
        raw_data['variant_id'] = None
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEqual(instance.language_tag, 'en')
        # el, Grek --> el
        raw_data['language_id'] = 'el'
        raw_data['script_id'] = 'Grek'
        raw_data['region_id'] = None
        raw_data['variant_id'] = None
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEqual(instance.language_tag, 'el')
        # bg-Cyrl --> bg
        raw_data['language_id'] = 'bg'
        raw_data['script_id'] = 'Cyrl'
        raw_data['region_id'] = None
        raw_data['variant_id'] = None
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEqual(instance.language_tag, 'bg')

    def test_language_instance_creation_of_language_tag_without_suppressed_script(self):
        raw_data = copy.deepcopy(self.raw_data)
        # en, Grek --> en-Grek
        raw_data['language_id'] = 'en'
        raw_data['script_id'] = 'Grek'
        raw_data['region_id'] = None
        raw_data['variant_id'] = None
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEqual(instance.language_tag, 'en-Grek')
        # el, Latn --> el-Latn
        raw_data['language_id'] = 'el'
        raw_data['script_id'] = 'Latn'
        raw_data['region_id'] = None
        raw_data['variant_id'] = None
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEqual(instance.language_tag, 'el-Latn')
        # bg, Latn --> bg-Latn
        raw_data['language_id'] = 'bg'
        raw_data['script_id'] = 'Latn'
        raw_data['region_id'] = None
        raw_data['variant_id'] = None
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEqual(instance.language_tag, 'bg-Latn')

    def test_language_display_representation_with_suppressed_script(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language_id'] = 'en'
        raw_data['script_id'] = 'Latn'
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['script_id'], None)

    def test_language_display_representation_without_suppressed_script(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language_id'] = 'en'
        raw_data['script_id'] = 'Grek'
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['script_id'],
                          OrderedDict([('field_label', {'en': 'Script'}),
                                       ('field_value', 'Grek'),
                                       ('label', {'en': 'Greek'})]))

    def test_language_display_representation_glottolog_label(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['glottolog_code'] = 'bava1246'
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['glottolog_code'],
                          OrderedDict([('field_label', {'en': 'Glottolog code'}),
                                       ('field_value', 'bava1246'),
                                       ('label', {'en': 'Bavarian'})]))

    def test_language_valid_glottolog_code(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['glottolog_code'] = 'bava1246'
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_language_invalid_glottolog_code(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['glottolog_code'] = 'foo'
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(str(serializer.errors['glottolog_code']),
                          '[ErrorDetail(string=\'foo is not a valid option for Glottolog code.\', code=\'invalid\')]')

    def test_language_iso_generates_glottolog_code(self):
        raw_data = {
            "language_id": "en"
        }
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.glottolog_code, 'stan1293')

    def test_language_iso_generates_glottolog_code(self):
        raw_data = {
            "language_id": "en"
        }
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.glottolog_code, 'stan1293')

    def test_language_iso_does_not_generate_glottolog_code_if_it_does_not_match(self):
        raw_data = {
            "language_id": "et"
        }
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.glottolog_code, '')

    def test_language_if_iso_and_glottolog_passes_as_is(self):
        raw_data = {
            "language_id": "en",
            "glottolog_code": "stan1293"
        }
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.glottolog_code, 'stan1293')
        self.assertEquals(instance.language_id, 'en')

    def test_language_if_iso_mis_glottolog_passes_as_is_and_generates_tag(self):
        raw_data = {
            "language_id": "mis",
            "glottolog_code": "apul1236"
        }
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.glottolog_code, 'apul1236')
        self.assertEquals(instance.language_id, 'mis')
        self.assertEquals(instance.language_tag, 'mis-x-apul1236')

    def test_language_if_iso_and_glottolog_do_not_match_change_id_to_mis(self):
        raw_data = {
            "language_id": "en",
            "glottolog_code": "apul1236"
        }
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.glottolog_code, 'apul1236')
        self.assertEquals(instance.language_id, 'mis')
        self.assertEquals(instance.language_tag, 'mis-x-apul1236')

    def test_language_fails_with_mis_but_no_glottolog_code(self):
        raw_data = {
            "language_id": "mis",
        }
        serializer = r_s.LanguageSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(str(serializer.errors['glottolog_code']),
                          '[ErrorDetail(string=\'Glottolog code is required for Uncoded Languages.\', code=\'invalid\')]')


class TestTextTypeIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.tti = {
            'text_type_classification_scheme': 'http://w3id.org/meta-share/meta-share/BNC_textTypeClassification',
            'value': "doi_value_id"
        }
        cls.serializer = r_s.TextTypeIdentifierSerializer(data=cls.tti)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.tti.keys():
            if i not in r_s.TextTypeIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_text_type_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.TextTypeIdentifierSerializer.Meta.fields)

    def test_text_type_identifier_serializer_fails_with_bad_data(self):
        fail_data = {
            'text_type_classification_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.TextTypeIdentifierSerializer(self.instance,
                                                      data=fail_data)
        self.assertFalse(serializer.is_valid())


class TestTextTypeSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['text_type']
        cls.serializer = r_s.TextTypeSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.TextTypeSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_text_type_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.TextTypeSerializer.Meta.fields)

    def test_text_type_serializer_fails_with_bad_data(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['category_label'] = 'fail'
        serializer = r_s.TextTypeSerializer(data=fail_data)
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_without_label(self):
        no_label = copy.deepcopy(self.raw_data)
        no_label['category_label'] = None
        serializer = r_s.TextTypeSerializer(data=no_label)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_without_identifier(self):
        no_id = copy.deepcopy(self.raw_data)
        no_id['test_type_identifier'] = None
        serializer = r_s.TextTypeSerializer(data=no_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertTrue(serializer.is_valid())

    def test_cannot_retrieve_instance_with_blank_id(self):
        blank_id = copy.deepcopy(self.raw_data)
        blank_id['text_type_identifier'] = ''
        serializer = r_s.TextTypeSerializer(data=blank_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_with_empty_id(self):
        empty_id = copy.deepcopy(self.raw_data)
        empty_id['text_type_identifier'] = {
            'text_type_classification_scheme': '',
            'value': ''
        }
        serializer = r_s.TextTypeSerializer(data=empty_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())


class TestTextGenreIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.tgi = {
            'text_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/ANC_genreClassification',
            'value': "doi_value_id"
        }
        cls.serializer = r_s.TextGenreIdentifierSerializer(data=cls.tgi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.tgi.keys():
            if i not in r_s.TextGenreIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_text_genre_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.TextGenreIdentifierSerializer.Meta.fields)

    def test_text_genre_identifier_serializer_fails_with_bad_data(self):
        fail_data = {
            'text_genre_classification_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.TextGenreIdentifierSerializer(self.instance,
                                                       data=fail_data)
        self.assertFalse(serializer.is_valid())


class TestTextGenreSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['text_genre']
        cls.serializer = r_s.TextGenreSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.TextGenreSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_text_genre_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.TextGenreSerializer.Meta.fields)

    def test_text_genre_serializer_fails_with_bad_data(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['category_label'] = 'fail'
        serializer = r_s.TextGenreSerializer(data=fail_data)
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_without_label(self):
        no_label = copy.deepcopy(self.raw_data)
        no_label['category_label'] = None
        serializer = r_s.TextGenreSerializer(data=no_label)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_without_identifier(self):
        no_id = copy.deepcopy(self.raw_data)
        no_id['text_genre_identifier'] = None
        serializer = r_s.TextGenreSerializer(data=no_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertTrue(serializer.is_valid())

    def test_cannot_retrieve_instance_with_blank_id(self):
        blank_id = copy.deepcopy(self.raw_data)
        blank_id['text_genre_identifier'] = ''
        serializer = r_s.TextGenreSerializer(data=blank_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_with_empty_id(self):
        empty_id = copy.deepcopy(self.raw_data)
        empty_id['text_genre_identifier'] = {
            'text_genre_classification_scheme': '',
            'value': ''
        }
        serializer = r_s.TextGenreSerializer(data=empty_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())


class TestLinkToOtherMediaSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.raw_data = {
            'other_media': ['http://w3id.org/meta-share/meta-share/image'],
            'media_type_details': {
                'en': 'details'
            },
            'synchronized_with_text': None,
            'synchronized_with_audio': None,
            'synchronized_with_video': None,
            'synchronized_with_image': False,
            'synchronized_with_text_numerical': None
        }

        cls.serializer = r_s.LinkToOtherMediaSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LinkToOtherMediaSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_link_to_other_media_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LinkToOtherMediaSerializer.Meta.fields)

    def test_link_to_other_media_fails_with_bad_data(self):
        self.raw_data['other_media'] = 'fail'
        serializer = r_s.LinkToOtherMediaSerializer(data=self.raw_data)
        self.assertFalse(serializer.is_valid())
        self.raw_data['other_media'] = [
            'http://w3id.org/meta-share/meta-share/image']

    def test_link_to_other_media_serializer_fails_with_no_other_media(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = []
        serializer = r_s.LinkToOtherMediaSerializer(self.instance,
                                                    data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_fails_with_none_synchronized_with_text(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.TEXT]
        raw_data['synchronized_with_text'] = None
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_fails_with_missing_synchronized_with_text(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.TEXT]
        del raw_data['synchronized_with_text']
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_passes_with_false_synchronized_with_text(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.TEXT]
        raw_data['synchronized_with_text'] = False
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_link_to_other_media_passes_with_true_synchronized_with_text(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.TEXT]
        raw_data['synchronized_with_text'] = True
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_link_to_other_media_fails_with_none_synchronized_with_audio(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.AUDIO]
        raw_data['synchronized_with_audio'] = None
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_fails_with_missing_synchronized_with_audio(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.AUDIO]
        del raw_data['synchronized_with_audio']
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_passes_with_false_synchronized_with_audio(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.AUDIO]
        raw_data['synchronized_with_audio'] = False
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_link_to_other_media_passes_with_true_synchronized_with_audio(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.AUDIO]
        raw_data['synchronized_with_audio'] = True
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_link_to_other_media_fails_with_none_synchronized_with_image(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.IMAGE]
        raw_data['synchronized_with_image'] = None
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_fails_with_missing_synchronized_with_image(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.IMAGE]
        del raw_data['synchronized_with_image']
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_passes_with_false_synchronized_with_image(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.IMAGE]
        raw_data['synchronized_with_image'] = False
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_link_to_other_media_passes_with_true_synchronized_with_image(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.IMAGE]
        raw_data['synchronized_with_image'] = True
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_link_to_other_media_fails_with_none_synchronized_with_video(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.VIDEO]
        raw_data['synchronized_with_video'] = None
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_fails_with_missing_synchronized_with_video(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.VIDEO]
        del raw_data['synchronized_with_video']
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_passes_with_false_synchronized_with_video(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.VIDEO]
        raw_data['synchronized_with_video'] = False
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_link_to_other_media_passes_with_truesynchronized_with_video(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.VIDEO]
        raw_data['synchronized_with_video'] = True
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_link_to_other_media_fails_with_none_synchronized_with_text_numerical(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.TEXT_NUMERICAL]
        raw_data['synchronized_with_text_numerical'] = None
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_fails_with_missing_synchronized_with_text_numerical(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.TEXT_NUMERICAL]
        del raw_data['synchronized_with_text_numerical']
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_link_to_other_media_passes_with_false_synchronized_with_text_numerical(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.TEXT_NUMERICAL]
        raw_data['synchronized_with_text_numerical'] = False
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_link_to_other_media_passes_with_true_synchronized_with_text_numerical(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_media'] = [choices.OTHER_MEDIA_CHOICES.TEXT_NUMERICAL]
        raw_data['synchronized_with_text_numerical'] = True
        serializer = r_s.LinkToOtherMediaSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())


class TestCorpusTextPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['text_corpus_part']
        cls.serializer = r_s.CorpusTextPartSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.CorpusTextPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_corpus_text_part_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue(
            ['corpus_media_type', 'media_type'] not in tuple(data.keys()))

    def test_corpus_text_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.CorpusTextPartSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_text_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.CorpusTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_text_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.CorpusTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_multilignuality_obligatory_upon_condition_bilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.CorpusTextPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_multilignuality_obligatory_upon_condition_multilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.CorpusTextPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_multilignuality_not_obligatory_upon_condition_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        data['multilinguality_type'] = None
        serializer = r_s.CorpusTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_text_part_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = choices.MULTILINGUALITY_TYPE_CHOICES.COMPARABLE
        initial_multilinguality = self.instance.multilinguality_type
        serializer = r_s.CorpusTextPartSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertFalse(
            instance.multilinguality_type == initial_multilinguality)

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.CorpusTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.CorpusTextPartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.CorpusTextPartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)


class TestAudioGenreIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.agi = {
            'audio_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/MS_audioGenreClassification',
            'value': "doi_value_id"
        }
        cls.serializer = r_s.AudioGenreIdentifierSerializer(data=cls.agi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.agi.keys():
            if i not in r_s.AudioGenreIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_audio_genre_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.AudioGenreIdentifierSerializer.Meta.fields)

    def test_audio_genre_identifier_serializer_fails_with_bad_data(self):
        fail_data = {
            'audio_genre_classification_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.AudioGenreIdentifierSerializer(self.instance,
                                                        data=fail_data)
        self.assertFalse(serializer.is_valid())


class TestAudioGenreSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.raw_data = {
            'category_label': {
                'en': 'label'
            },
            'audio_genre_identifier': {
                'audio_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/MS_audioGenreClassification',
                'value': "doi_value_id"
            }
        }
        cls.serializer = r_s.AudioGenreSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.AudioGenreSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_audio_genre_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.AudioGenreSerializer.Meta.fields)

    def test_audio_genre_serializer_fails_with_bad_data(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['category_label'] = 'fail'
        serializer = r_s.AudioGenreSerializer(data=fail_data)
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_without_label(self):
        no_label = copy.deepcopy(self.raw_data)
        no_label['category_label'] = None
        serializer = r_s.AudioGenreSerializer(data=no_label)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_without_identifier(self):
        no_id = copy.deepcopy(self.raw_data)
        no_id['audio_genre_identifier'] = None
        serializer = r_s.AudioGenreSerializer(data=no_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertTrue(serializer.is_valid())

    def test_cannot_retrieve_instance_with_blank_id(self):
        blank_id = copy.deepcopy(self.raw_data)
        blank_id['audio_genre_identifier'] = ''
        serializer = r_s.AudioGenreSerializer(data=blank_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_with_empty_id(self):
        empty_id = copy.deepcopy(self.raw_data)
        empty_id['audio_genre_identifier'] = {
            'audio_genre_classification_scheme': '',
            'value': ''
        }
        serializer = r_s.AudioGenreSerializer(data=empty_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())


class TestSpeechGenreIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.sgi = {
            'speech_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/MS_SpeechGenreClassification',
            'value': "doi_value_id"
        }
        cls.serializer = r_s.SpeechGenreIdentifierSerializer(data=cls.sgi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.sgi.keys():
            if i not in r_s.SpeechGenreIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_speech_genre_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.SpeechGenreIdentifierSerializer.Meta.fields)

    def test_speech_genre_identifier_serializer_fails_with_bad_data(self):
        fail_data = {
            'speech_genre_classification_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.SpeechGenreIdentifierSerializer(self.instance,
                                                         data=fail_data)
        self.assertFalse(serializer.is_valid())


class TestSpeechGenreSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['speech_genre']
        cls.serializer = r_s.SpeechGenreSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.SpeechGenreSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_speech_genre_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.SpeechGenreSerializer.Meta.fields)

    def test_speech_genre_serializer_fails_with_bad_data(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['category_label'] = 'fail'
        serializer = r_s.SpeechGenreSerializer(data=fail_data)
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_without_label(self):
        no_label = copy.deepcopy(self.raw_data)
        no_label['category_label'] = None
        serializer = r_s.SpeechGenreSerializer(data=no_label)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_without_identifier(self):
        no_id = copy.deepcopy(self.raw_data)
        no_id['speech_genre_identifier'] = None
        serializer = r_s.SpeechGenreSerializer(data=no_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertTrue(serializer.is_valid())

    def test_cannot_retrieve_instance_with_blank_id(self):
        blank_id = copy.deepcopy(self.raw_data)
        blank_id['speech_genre_identifier'] = ''
        serializer = r_s.SpeechGenreSerializer(data=blank_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_with_empty_id(self):
        empty_id = copy.deepcopy(self.raw_data)
        empty_id['speech_genre_identifier'] = {
            'speech_genre_classification_scheme': '',
            'value': ''
        }
        serializer = r_s.SpeechGenreSerializer(data=empty_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())


class TestCorpusAudioPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['corpus_audio_part']
        cls.serializer = r_s.CorpusAudioPartSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.CorpusAudioPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_corpus_audio_part_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue(
            ['corpus_media_type', 'media_type'] not in tuple(data.keys()))

    def test_corpus_audio_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.CorpusAudioPartSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_audio_part_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = choices.MULTILINGUALITY_TYPE_CHOICES.COMPARABLE
        initial_multilinguality = self.instance.multilinguality_type
        serializer = r_s.CorpusAudioPartSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertFalse(
            instance.multilinguality_type == initial_multilinguality)

    def test_corpus_audio_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.CorpusAudioPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_audio_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.CorpusAudioPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.CorpusAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.CorpusAudioPartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.CorpusAudioPartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_multilinguality_obligatory_upon_condition_bilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.CorpusAudioPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_multilinguality_obligatory_upon_condition_multilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.CorpusAudioPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_multilinguality_not_obligatory_upon_condition_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        data['multilinguality_type'] = None
        serializer = r_s.CorpusAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())


class TestVideoGenreIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.vgi = {
            'video_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/other',
            'value': "doi_value_id"
        }
        cls.serializer = r_s.VideoGenreIdentifierSerializer(data=cls.vgi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.vgi.keys():
            if i not in r_s.VideoGenreIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_video_genre_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.VideoGenreIdentifierSerializer.Meta.fields)

    def test_video_genre_identifier_serializer_fails_with_bad_data(self):
        fail_data = {
            'video_genre_classification_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.VideoGenreIdentifierSerializer(self.instance,
                                                        data=fail_data)
        self.assertFalse(serializer.is_valid())


class TestVideoGenreSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['video_genre']
        cls.serializer = r_s.VideoGenreSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.VideoGenreSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_video_genre_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.VideoGenreSerializer.Meta.fields)

    def test_video_genre_serializer_fails_with_bad_data(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['category_label'] = 'fail'
        serializer = r_s.VideoGenreSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())


class TestDynamicElementSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['dynamic_element']
        cls.serializer = r_s.DynamicElementSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DynamicElementSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_dynamic_element_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DynamicElementSerializer.Meta.fields)

    def test_dynamic_element_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['body_part'] = ['my arm']
        serializer = r_s.DynamicElementSerializer(self.instance,
                                                  data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dynamic_element_serializer_fails_with_no_type_of_element(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_element'] = []
        serializer = r_s.DynamicElementSerializer(self.instance,
                                                  data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestCorpusVideoPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['corpus_video_part']
        cls.serializer = r_s.CorpusVideoPartSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.CorpusVideoPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_corpus_video_part_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue(
            ['corpus_media_type', 'media_type'] not in tuple(data.keys()))

    def test_corpus_video_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.CorpusVideoPartSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_video_part_serializer_fails_with_no_type_of_video_content(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_video_content'] = []
        serializer = r_s.CorpusVideoPartSerializer(self.instance,
                                                   data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_video_part_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = choices.MULTILINGUALITY_TYPE_CHOICES.COMPARABLE
        initial_multilinguality = self.instance.multilinguality_type
        serializer = r_s.CorpusVideoPartSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertFalse(
            instance.multilinguality_type == initial_multilinguality)

    def test_corpus_video_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.CorpusVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_video_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.CorpusVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.CorpusVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.CorpusVideoPartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.CorpusVideoPartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_multilinguality_obligatory_upon_condition_bilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.CorpusVideoPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_multilinguality_obligatory_upon_condition_multilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.CorpusVideoPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_multilinguality_not_obligatory_upon_condition_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        data['multilinguality_type'] = None
        serializer = r_s.CorpusVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())


class TestImageGenreIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.igi = {
            'image_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/other',
            'value': "doi_value_id"
        }
        cls.serializer = r_s.ImageGenreIdentifierSerializer(data=cls.igi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.igi.keys():
            if i not in r_s.ImageGenreIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_image_genre_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ImageGenreIdentifierSerializer.Meta.fields)

    def test_image_genre_identifier_serializer_fails_with_bad_data(self):
        fail_data = {
            'image_genre_classification_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.ImageGenreIdentifierSerializer(self.instance,
                                                        data=fail_data)
        self.assertFalse(serializer.is_valid())


class TestImageGenreSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['image_genre']
        cls.serializer = r_s.ImageGenreSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ImageGenreSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_image_genre_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ImageGenreSerializer.Meta.fields)

    def test_image_genre_serializer_fails_with_bad_data(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['category_label'] = 'fail'
        serializer = r_s.ImageGenreSerializer(data=fail_data)
        self.assertFalse(serializer.is_valid())


class TestStaticElementSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['static_element']
        cls.serializer = r_s.StaticElementSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.StaticElementSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_static_element_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.StaticElementSerializer.Meta.fields)

    def test_static_element_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['body_part'] = ['my arm']
        serializer = r_s.StaticElementSerializer(self.instance,
                                                 data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_static_element_serializer_fails_with_no_type_of_element(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_element'] = []
        serializer = r_s.StaticElementSerializer(self.instance,
                                                 data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestCorpusImagePartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['corpus_image_part']
        cls.serializer = r_s.CorpusImagePartSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.CorpusImagePartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_corpus_image_part_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue(
            ['corpus_media_type', 'media_type'] not in tuple(data.keys()))

    def test_corpus_image_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.CorpusImagePartSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_image_part_serializer_fails_with_no_type_of_image_content(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_image_content'] = []
        serializer = r_s.CorpusImagePartSerializer(self.instance,
                                                   data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_image_part_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = choices.MULTILINGUALITY_TYPE_CHOICES.COMPARABLE
        initial_multilinguality = self.instance.multilinguality_type
        serializer = r_s.CorpusImagePartSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertFalse(
            instance.multilinguality_type == initial_multilinguality)

    def test_corpus_image_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.CorpusImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_image_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.CorpusImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.CorpusImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_no_language(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = []
        serializer = r_s.CorpusImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, None)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.CorpusImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.CorpusImagePartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.CorpusImagePartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_multilinguality_obligatory_upon_condition_bilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.CorpusImagePartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_multilinguality_obligatory_upon_condition_multilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.CorpusImagePartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_multilinguality_not_obligatory_upon_condition_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        data['multilinguality_type'] = None
        serializer = r_s.CorpusImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())


class TestCorpusTextNumericalPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['corpus_text_numerical_part']
        cls.serializer = r_s.CorpusTextNumericalPartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.CorpusTextNumericalPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_corpus_text_numerical_part_serializer_contains_expected_fields(
            self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue(
            ['corpus_media_type', 'media_type'] not in tuple(data.keys()))

    def test_corpus_text_numerical_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_text_numerical_content'] = 'fail'
        serializer = r_s.CorpusTextNumericalPartSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_text_numerical_serializer_fails_with_no_type_of_text_numerical_content(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_text_numerical_content'] = []
        serializer = r_s.CorpusTextNumericalPartSerializer(self.instance,
                                                           data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_test_numerical_part_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus_text_numerical_part']
        raw_data['synthetic_data'] = True
        initial_synthetic_data = self.instance.synthetic_data
        serializer = r_s.CorpusTextNumericalPartSerializer(self.instance,
                                                           data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertFalse(instance.synthetic_data == initial_synthetic_data)


class TestCorpusMediaPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['text_corpus_part']
        cls.raw_data["corpus_media_type"] = 'CorpusTextPart'
        cls.serializer = r_s.CorpusMediaPartSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_corpus_media_part_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue('corpus_media_type' in tuple(data.keys()))

    def test_corpus_media_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['corpus_media_type'] = 'fail'
        serializer = r_s.CorpusMediaPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_corpus_media_part_serializer_requires_media_type(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['corpus_media_type'] = ''
        serializer = r_s.CorpusMediaPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_corpus_media_part_serializer_validation_fails_with_none(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['corpus_media_type'] = None
        serializer = r_s.CorpusMediaPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_corpus_media_part_serializer_validation_fails_with_no_field(self):
        raw_data = copy.deepcopy(self.raw_data)
        del raw_data['corpus_media_type']
        serializer = r_s.CorpusMediaPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_corpus_media_part_serializer_fails_with_wrong_corpus_media_type(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['corpus_media_type'] = 'CorpusImagePart'
        serializer = r_s.CorpusMediaPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())


class TestSizeSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['size']
        cls.serializer = r_s.SizeSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.SizeSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_size_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()), r_s.SizeSerializer.Meta.fields)

    def test_size_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['amount'] = 'fail'
        serializer = r_s.SizeSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_size_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.SizeSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_size_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.SizeSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_to_display_representation_for_size_unit_recommended_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size_unit'] = choices.SIZE_UNIT_RECOMMENDED_CHOICES.GB
        serializer = r_s.SizeSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['size_unit'],
                          {
                              'field_label': {'en': 'Size unit'},
                              'field_value':
                                  {
                                      'value': choices.SIZE_UNIT_RECOMMENDED_CHOICES.GB,
                                      'label': {
                                          'en': choices.SIZE_UNIT_RECOMMENDED_CHOICES[
                                              choices.SIZE_UNIT_RECOMMENDED_CHOICES.GB]}
                                  }
                          })

    def test_to_xml_representation_for_size_unit_recommended_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size_unit'] = choices.SIZE_UNIT_RECOMMENDED_CHOICES.GB
        serializer = r_s.SizeSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        size_unit_xml_field_name = r_s.SizeSerializer.Meta.model.schema_fields['size_unit']
        self.assertEquals(list(xml_data[size_unit_xml_field_name].keys()), ['ms:sizeUnitRecommended'])
        self.assertEquals(list(xml_data[size_unit_xml_field_name].values()),
                          [choices.SIZE_UNIT_RECOMMENDED_CHOICES.GB])

    def test_to_xml_representation_for_size_unit_not_recommended_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size_unit'] = 'size unit not cv'
        serializer = r_s.SizeSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        size_unit_xml_field_name = r_s.SizeSerializer.Meta.model.schema_fields['size_unit']
        self.assertEquals(list(xml_data[size_unit_xml_field_name].keys()), ['ms:sizeUnitOther'])
        self.assertEquals(list(xml_data[size_unit_xml_field_name].values()),
                          ['size unit not cv'])

    def test_from_xml_recommended_size_unit_recommended_works(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionTextFeature']['ms:size']
        from_xml = r_s.SizeSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['size_unit'], choices.SIZE_UNIT_RECOMMENDED_CHOICES.T_H_PAIR)

    def test_from_xml_recommended_size_unit_no_recommended_works(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionTextFeature']['ms:size']
        xml_data['ms:sizeUnit'] = {'ms:sizeUnitOther': 'size unit other'}
        from_xml = r_s.SizeSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['size_unit'], 'size unit other')

    def test_size_serializer_fails_with_sizetext_no_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size_text'] = [{
            'en': 'various size description',
        }]
        serializer = r_s.SizeSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_size_serializer_fails_with_sizetext_for_info_context_false(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size_text'] = [{
            'en': 'various size description',
        }]
        context = {
            'for_information_only': False
        }
        serializer = r_s.SizeSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())

    def test_size_serializer_success_with_sizetext_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size_text'] = [{
            'en': 'various size description',
        }]
        context = {
            'for_information_only': True
        }
        serializer = r_s.SizeSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())

    def test_size_serializer_success_with_no_sizetext_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size_text'] = None
        context = {
            'for_information_only': True
        }
        serializer = r_s.SizeSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())


class TestDistributionTextFeatureSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['distribution_text_feature']
        cls.serializer = r_s.DistributionTextFeatureSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DistributionTextFeatureSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_distribution_text_feature_serializer_contains_expected_fields(
            self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DistributionTextFeatureSerializer.Meta.fields)

    def test_distribution_text_feature_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['character_encoding'] = ['fail']
        serializer = r_s.DistributionTextFeatureSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_distribution_text_feature_serializer_fails_with_no_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = []
        serializer = r_s.DistributionTextFeatureSerializer(self.instance,
                                                           data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [
            choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
            'data format non CV value'
        ]
        serializer = r_s.DistributionTextFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.DistributionTextFeatureSerializer.Meta.model.schema_fields['data_format']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['data format non CV value'])

    def test_from_xml_data_format_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionTextFeature']
        from_xml = r_s.DistributionTextFeatureSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                           'data format non CV value'])

    def test_to_display_representation_for_data_format_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS, 'data format non CV value']
        serializer = r_s.DistributionTextFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value': [
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  },
                                  {
                                      'value': 'data format non CV value'
                                  }
                              ]
                          })


class TestDistributionTextNumericalFeatureSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)[
            'distribution_text_numerical_feature']
        cls.serializer = r_s.DistributionTextNumericalFeatureSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DistributionTextNumericalFeatureSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_distribution_text_numerical_feature_serializer_contains_expected_fields(
            self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DistributionTextNumericalFeatureSerializer.Meta.fields)

    def test_distriubtion_text_numerical_feature_serializer_fails_with_no_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = []
        serializer = r_s.DistributionTextNumericalFeatureSerializer(self.instance,
                                                                    data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_distribution_text_numerical_feature_serializer_fails_with_bad_data(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = None
        serializer = r_s.DistributionTextNumericalFeatureSerializer(
            data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [
            choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
            'data format non CV value'
        ]
        serializer = r_s.DistributionTextNumericalFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.DistributionTextNumericalFeatureSerializer.Meta.model.schema_fields[
            'data_format']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['data format non CV value'])

    def test_from_xml_data_format_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionTextNumericalFeature']
        from_xml = r_s.DistributionTextNumericalFeatureSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                           'data format non CV value'])

    def test_to_display_representation_for_data_format_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS, 'data format non CV value']
        serializer = r_s.DistributionTextNumericalFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value': [
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  },
                                  {
                                      'value': 'data format non CV value'
                                  }
                              ]
                          })


class TestDurationSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.raw_data = {
            'amount': 10.0,
            'duration_unit': 'http://w3id.org/meta-share/meta-share/minute1'
        }

        cls.serializer = r_s.DurationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DurationSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_duration_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DurationSerializer.Meta.fields)

    def test_duration_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['duration_unit'] = 'fail'
        serializer = r_s.DurationSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestAudioFormatSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['audio_format']
        cls.serializer = r_s.AudioFormatSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.AudioFormatSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_audio_format_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.AudioFormatSerializer.Meta.fields)

    def test_audio_format_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = None
        serializer = r_s.AudioFormatSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format_recommeded(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS
        serializer = r_s.AudioFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        data_format_xml_field_name = r_s.AudioFormatSerializer.Meta.model.schema_fields[
            'data_format']
        self.assertEquals(list(xml_data[data_format_xml_field_name].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[data_format_xml_field_name].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])

    def test_to_xml_representation_for_data_format_not_recommended_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = 'data format not cv'
        serializer = r_s.AudioFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        data_format_xml_field_name = r_s.AudioFormatSerializer.Meta.model.schema_fields['data_format']
        self.assertEquals(list(xml_data[data_format_xml_field_name].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[data_format_xml_field_name].values()),
                          ['data format not cv'])

    def test_from_xml_data_format_works_recommended(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionAudioFeature']['ms:audioFormat']
        from_xml = r_s.AudioFormatSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS)

    def test_from_xml_recommended_size_unit_no_recommended_works(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionAudioFeature']['ms:audioFormat']
        xml_data['ms:dataFormat'] = {'ms:dataFormatOther': 'data format other'}
        from_xml = r_s.AudioFormatSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'], 'data format other')

    def test_to_display_representation_for_data_format_works_recommended(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS
        serializer = r_s.AudioFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value':
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  }
                          })

    def test_to_display_representation_for_data_format_works_not_recommended(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = 'data format other'
        serializer = r_s.AudioFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value':
                                  {
                                      'value': 'data format other'
                                  }
                          })


class TestDistributionAudioFeatureSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)[
            'distribution_audio_feature_serializer']
        cls.serializer = r_s.DistributionAudioFeatureSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DistributionAudioFeatureSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_distribution_audio_feature_serializer_contains_expected_fields(
            self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DistributionAudioFeatureSerializer.Meta.fields)

    def test_distribution_audio_feature_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size'] = []
        serializer = r_s.DistributionAudioFeatureSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_distribution_audio_feature_serializer_fails_with_no_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = []
        serializer = r_s.DistributionAudioFeatureSerializer(self.instance,
                                                            data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [
            choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
            'data format non CV value'
        ]
        serializer = r_s.DistributionAudioFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.DistributionAudioFeatureSerializer.Meta.model.schema_fields[
            'data_format']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['data format non CV value'])

    def test_from_xml_data_format_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionAudioFeature']
        from_xml = r_s.DistributionAudioFeatureSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                           'data format non CV value'])

    def test_to_display_representation_for_data_format_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS, 'data format non CV value']
        serializer = r_s.DistributionAudioFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value': [
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  },
                                  {
                                      'value': 'data format non CV value'
                                  }
                              ]
                          })


class TestResolutionSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.raw_data = {
            'size_width': 10,
            'size_height': 10,
            'resolution_standard': 'http://w3id.org/meta-share/meta-share/HD.1080'
        }

        cls.serializer = r_s.ResolutionSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ResolutionSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_resolution_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ResolutionSerializer.Meta.fields)

    def test_resolution_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['resolution_standard'] = 'fail'
        serializer = r_s.ResolutionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestImageFormatSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['image_format']
        cls.serializer = r_s.ImageFormatSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ImageFormatSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_image_format_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ImageFormatSerializer.Meta.fields)

    def test_image_format_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = None
        serializer = r_s.ImageFormatSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format_recommeded(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS
        serializer = r_s.ImageFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        data_format_xml_field_name = r_s.ImageFormatSerializer.Meta.model.schema_fields[
            'data_format']
        self.assertEquals(list(xml_data[data_format_xml_field_name].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[data_format_xml_field_name].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])

    def test_to_xml_representation_for_data_format_not_recommended_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = 'data format not cv'
        serializer = r_s.ImageFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        data_format_xml_field_name = r_s.ImageFormatSerializer.Meta.model.schema_fields['data_format']
        self.assertEquals(list(xml_data[data_format_xml_field_name].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[data_format_xml_field_name].values()),
                          ['data format not cv'])

    def test_from_xml_data_format_works_recommended(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionImageFeature']['ms:imageFormat']
        from_xml = r_s.ImageFormatSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS)

    def test_from_xml_recommended_size_unit_no_recommended_works(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionImageFeature']['ms:imageFormat']
        xml_data['ms:dataFormat'] = {'ms:dataFormatOther': 'data format other'}
        from_xml = r_s.ImageFormatSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'], 'data format other')

    def test_to_display_representation_for_data_format_works_recommended(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS
        serializer = r_s.ImageFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value':
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  }
                          })

    def test_to_display_representation_for_data_format_works_not_recommended(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = 'data format other'
        serializer = r_s.ImageFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value':
                                  {
                                      'value': 'data format other'
                                  }
                          })


class TestDistributionImageFeatureSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['distribution_image_feature']
        cls.serializer = r_s.DistributionImageFeatureSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DistributionImageFeatureSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_distribution_image_feature_serializer_contains_expected_fields(
            self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DistributionImageFeatureSerializer.Meta.fields)

    def test_distribution_image_feature_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size'] = []
        serializer = r_s.DistributionImageFeatureSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_distribution_image_feature_serializer_fails_with_no_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = []
        serializer = r_s.DistributionImageFeatureSerializer(self.instance,
                                                            data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [
            choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
            'data format non CV value'
        ]
        serializer = r_s.DistributionImageFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.DistributionImageFeatureSerializer.Meta.model.schema_fields[
            'data_format']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['data format non CV value'])

    def test_from_xml_data_format_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionImageFeature']
        from_xml = r_s.DistributionImageFeatureSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                           'data format non CV value'])

    def test_to_display_representation_for_data_format_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS, 'data format non CV value']
        serializer = r_s.DistributionImageFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value': [
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  },
                                  {
                                      'value': 'data format non CV value'
                                  }
                              ]
                          })


class TestVideoFormatSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['video_format']
        cls.serializer = r_s.VideoFormatSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.VideoFormatSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_video_format_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.VideoFormatSerializer.Meta.fields)

    def test_video_format_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = None
        serializer = r_s.VideoFormatSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format_recommeded(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS
        serializer = r_s.VideoFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        data_format_xml_field_name = r_s.VideoFormatSerializer.Meta.model.schema_fields[
            'data_format']
        self.assertEquals(list(xml_data[data_format_xml_field_name].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[data_format_xml_field_name].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])

    def test_to_xml_representation_for_data_format_not_recommended_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = 'data format not cv'
        serializer = r_s.VideoFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        data_format_xml_field_name = r_s.VideoFormatSerializer.Meta.model.schema_fields['data_format']
        self.assertEquals(list(xml_data[data_format_xml_field_name].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[data_format_xml_field_name].values()),
                          ['data format not cv'])

    def test_from_xml_data_format_works_recommended(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionVideoFeature']['ms:videoFormat']
        from_xml = r_s.VideoFormatSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS)

    def test_from_xml_recommended_size_unit_no_recommended_works(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionVideoFeature']['ms:videoFormat']
        xml_data['ms:dataFormat'] = {'ms:dataFormatOther': 'data format other'}
        from_xml = r_s.VideoFormatSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'], 'data format other')

    def test_to_display_representation_for_data_format_works_recommended(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS
        serializer = r_s.VideoFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value':
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  }
                          })

    def test_to_display_representation_for_data_format_works_not_recommended(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = 'data format other'
        serializer = r_s.VideoFormatSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value':
                                  {
                                      'value': 'data format other'
                                  }
                          })


class TestDistributionVideoFeatureSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['distribution_video_feature']
        cls.serializer = r_s.DistributionVideoFeatureSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DistributionVideoFeatureSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_distribution_video_feature_serializer_contains_expected_fields(
            self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DistributionVideoFeatureSerializer.Meta.fields)

    def test_distribution_video_feature_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['size'] = []
        serializer = r_s.DistributionVideoFeatureSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_distribution_video_feature_serializer_fails_with_no_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = []
        serializer = r_s.DistributionVideoFeatureSerializer(self.instance,
                                                            data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [
            choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
            'data format non CV value'
        ]
        serializer = r_s.DistributionVideoFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.DistributionVideoFeatureSerializer.Meta.model.schema_fields[
            'data_format']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['data format non CV value'])

    def test_from_xml_data_format_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionVideoFeature']
        from_xml = r_s.DistributionVideoFeatureSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                           'data format non CV value'])

    def test_to_display_representation_for_data_format_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS, 'data format non CV value']
        serializer = r_s.DistributionVideoFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value': [
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  },
                                  {
                                      'value': 'data format non CV value'
                                  }
                              ]
                          })


class TestLicenceIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.li = {
            "licence_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
            "value": "registry_value"
        }
        cls.serializer = r_s.LicenceIdentifierSerializer(data=cls.li)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.li.keys():
            if i not in r_s.LicenceIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_li_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LicenceIdentifierSerializer.Meta.fields)

    def test_li_serializer_fails_with_bad_data(self):
        data_fail = {
            'licence_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.LicenceIdentifierSerializer(self.instance,
                                                     data=data_fail)
        self.assertFalse(serializer.is_valid())

    def test_scheme_no_update_when_already_registry_scheme(self):
        initial_scheme = self.instance.licence_identifier_scheme
        data = self.serializer.data
        data[
            'licence_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/other'
        serializer = r_s.LicenceIdentifierSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            scheme_after_update = instance.licence_identifier_scheme
            self.assertEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_scheme_update_with_registry_scheme(self):
        data_not_registry = {
            'licence_identifier_scheme': 'http://w3id.org/meta-share/meta-share/other',
            'value': "doi_value_id"
        }
        serializer = r_s.LicenceIdentifierSerializer(data=data_not_registry)
        if serializer.is_valid():
            instance = serializer.save()
        initial_scheme = instance.licence_identifier_scheme
        data = serializer.data
        data[
            'licence_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        serializer_registry = r_s.LicenceIdentifierSerializer(instance, data=data)
        if serializer_registry.is_valid():
            instance_registry = serializer_registry.save()
            scheme_after_update = instance_registry.licence_identifier_scheme
            self.assertNotEqual(scheme_after_update, initial_scheme)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestGenericLicenceTermsSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.GenericLicenceTermsSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.gl_instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)

    def test_generic_licence_terms_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GenericLicenceTermsSerializer.Meta.fields)

    def test_generic_licence_terms_serializer_does_not_update(self):
        self.assertTrue(self.serializer.is_valid())
        data = copy.deepcopy(self.raw_data)
        data['licence_terms_name'] = {
            'en': 'failed update name'
        }
        gen_serializer = r_s.GenericLicenceTermsSerializer(self.gl_instance, data)
        gen_serializer.is_valid()
        gen_instance = gen_serializer.save()
        self.assertTrue(gen_instance == self.gl_instance)

    def test_generic_licence_terms_serializer_fails_when_same_schemes_are_given(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['licence_identifier'] = [
            {
                "licence_identifier_scheme": 'http://w3id.org/meta-share/meta-share/SPDX',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "licence_identifier_scheme": 'http://w3id.org/meta-share/meta-share/SPDX',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.GenericLicenceTermsSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_generic_licence_terms_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['licence_identifier'] = [{
            'licence_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }]
        serializer = r_s.GenericLicenceTermsSerializer(self.gl_instance,
                                                       data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_generic_licence_terms_serializer_fails_with_no_licence_terms_url(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['licence_terms_url'] = []
        serializer = r_s.GenericLicenceTermsSerializer(self.gl_instance,
                                                       data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_generic_licence_terms_serializer_fails_with_no_condition_of_use(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['condition_of_use'] = []
        serializer = r_s.GenericLicenceTermsSerializer(self.gl_instance,
                                                       data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_by_gl(self):
        self.assertTrue(self.gl_instance)

    def test_retrieve_instance_by_terms_and_name(self):
        no_gl_data = copy.deepcopy(self.raw_data)
        no_gl_data['licence_identifier'] = []
        serializer = r_s.GenericLicenceTermsSerializer(data=no_gl_data)
        if serializer.is_valid():
            gl_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(gl_instance)

    def test_generic_licence_terms_serializer_fails_with_unspecified_condition_of_use_not_alone(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified",
            "http://w3id.org/meta-share/meta-share/noDerivatives"
        ]
        serializer = r_s.GenericLicenceTermsSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.GenericLicenceTermsSerializer(instance=self.gl_instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.LicenceIdentifierSerializer.Meta.model.schema_fields[
                'licence_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in
            xml_representation[r_s.GenericLicenceTermsSerializer.Meta.model.schema_fields['licence_identifier']]
        ]))


class TestCostSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.raw_data = {
            'amount': 10.0,
            'currency': 'http://w3id.org/meta-share/meta-share/euro',
        }

        cls.serializer = r_s.CostSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.CostSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_cost_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()), r_s.CostSerializer.Meta.fields)

    def test_cost_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['currency'] = 'fail'
        serializer = r_s.CostSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestDatasetDistributionSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['dataset_distribution']
        cls.serializer = r_s.DatasetDistributionSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)
        # distribution_unspecified_feature
        cls.distribution_unspecified_feature_data = json.loads(cls.json_str)['distribution_unspecified_feature']

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DatasetDistributionSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_dataset_distribution_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DatasetDistributionSerializer.Meta.fields)

    def test_dataset_distribution_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = 'fail'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_serializer_passes_when_accessible_through_query_and_only_one_lrs(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_serializer_passes_when_accessible_through_query_and_no_lrs(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        raw_data['is_queried_by'] = []
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_serializer_fails_when_accessible_through_query_and_more_than_one_lrs(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = 'fail'
        raw_data['is_queried_by'] = [self.raw_data['is_queried_by'][0], self.raw_data['is_queried_by'][0]]
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_fails_bluRay_no_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.BLU_RAY,
        raw_data['access_location'] = ''
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_fails_cdrom_no_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.CD_ROM,
        raw_data['access_location'] = ''
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_fails_harddisk_no_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.HARD_DISK,
        raw_data['access_location'] = ''
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_fails_dvdr_no_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.DVD_R,
        raw_data['access_location'] = ''
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_fails_other_no_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.OTHER,
        raw_data['access_location'] = ''
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_fails_unspecified_no_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.UNSPECIFIED
        raw_data['access_location'] = ''
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_passes_bluRay_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.BLU_RAY
        raw_data['access_location'] = 'http://www.location.com'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_passes_cdrom_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.CD_ROM
        raw_data['access_location'] = 'http://www.location.com'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_passes_harddisk_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.HARD_DISK
        raw_data['access_location'] = 'http://www.location.com'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_passes_dvdr_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.DVD_R
        raw_data['access_location'] = 'http://www.location.com'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_passes_other_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.OTHER
        raw_data['access_location'] = 'http://www.location.com'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_passes_unspecified_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.UNSPECIFIED
        raw_data['access_location'] = 'http://www.location.com'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_fails_downloadable_no_download_no_access_no_dataset(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.DOWNLOADABLE
        raw_data['download_location'] = ''
        raw_data['access_location'] = ''
        raw_data['dataset'] = None
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_passes_downloadable_access_no_download_no_dataset(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.DOWNLOADABLE
        raw_data['download_location'] = ''
        raw_data['access_location'] = 'http://www.location.com'
        raw_data['dataset'] = None
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_passes_downloadable_download_no_access_no_dataset(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.DOWNLOADABLE
        raw_data['download_location'] = 'http://www.location.com'
        raw_data['access_location'] = ''
        raw_data['dataset'] = None
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_passes_downloadable_dataset_no_access_no_download(self):
        # Create a metadata record with a dummy distribution
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        record_serializer = r_s.MetadataRecordSerializer(data=json.loads(json_str))
        self.assertTrue(record_serializer.is_valid())
        record_instance = record_serializer.save()
        # Create a ContentFile for the metadata record
        content_file_instance = ContentFile(
            file="ed46a01bde134f1e9ab6016aa744b2b8/3c7f037650914a839afb34ddddcdd766/apache-jena-fuseki-3.17.0.zip",
            record_manager=record_instance.management_object
        )
        content_file_instance.save()
        # Relate the content file with the given distribution of the record
        data = record_serializer.data['described_entity']['lr_subclass']['dataset_distribution'][0]
        data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.DOWNLOADABLE
        data['download_location'] = ''
        data['access_location'] = ''
        data['dataset'] = ContentFileSerializer(instance=content_file_instance).data
        dd_instance = DatasetDistribution.objects.get(pk=data['pk'])
        serializer = r_s.DatasetDistributionSerializer(instance=dd_instance, data=data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_fails_accessInter_no_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_INTERFACE
        raw_data['access_location'] = ''
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertRaises(ValidationError)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_fails_accessQuery_no_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_QUERY
        raw_data['access_location'] = ''
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_dataset_distribution_validation_passes_accessInter_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_INTERFACE
        raw_data['access_location'] = 'http://www.location.com'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_passes_accessQuery_access_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_QUERY
        raw_data['access_location'] = 'http://www.location.com'
        serializer = r_s.DatasetDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_successes_when_xml_upload_with_data_downloadable_no_download_no_access_no_dataset(
            self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['dataset_distribution_form'] = choices.DATASET_DISTRIBUTION_FORM_CHOICES.DOWNLOADABLE
        distribution_data['access_location'] = ''
        distribution_data['download_location'] = ''
        distribution_data['dataset'] = None
        serializer = r_s.DatasetDistributionSerializer(data=distribution_data, context={'xml_upload_with_data': True})
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation__licence_terms_is_missing_no_for_info(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['licence_terms'] = []
        serializer = r_s.DatasetDistributionSerializer(data=distribution_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(str(serializer.errors['licence_terms']),
                          '[ErrorDetail(string=\'Licence may not be empty.\', code=\'invalid\')]')

    def test_dataset_distribution_validation_success_licence_terms_is_missing_for_info(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['licence_terms'] = []
        serializer = r_s.DatasetDistributionSerializer(data=distribution_data, context={'for_information_only': True})
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_success_access_rights_is_missing_no_for_info(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['access_rights'] = []
        serializer = r_s.DatasetDistributionSerializer(data=distribution_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()

    def test_dataset_distribution_validation_passes_access_rights_is_missing_but_licence_terms_exist_for_info(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['access_rights'] = []
        serializer = r_s.DatasetDistributionSerializer(data=distribution_data, context={'for_information_only': True})
        self.assertTrue(serializer.is_valid())

    def test_dataset_distribution_validation_fails_access_rights_is_missing_and_no_licence_terms_for_info(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['access_rights'] = []
        distribution_data['licence_terms'] = []
        serializer = r_s.DatasetDistributionSerializer(data=distribution_data, context={'for_information_only': True})
        self.assertFalse(serializer.is_valid())
        self.assertEquals(str(serializer.errors['access_rights']),
                          '[ErrorDetail(string=\'Access rights may not be empty.\', code=\'invalid\')]')

    def test_dataset_distribution_xml_representation_hidden_values_dataset(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        dataset_distribution = raw_data['described_entity']['lr_subclass']['dataset_distribution'][0]
        dataset_distribution['dataset_distribution_form'] = \
            choices.DATASET_DISTRIBUTION_FORM_CHOICES.DOWNLOADABLE
        dataset_distribution['download_location'] = "http://www.dummylocation.com"
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Add data to the instance
        m = instance.management_object
        content_file = ContentFile.objects.create(file='test_general_purpose_annotated_text.zip', record_manager=m)
        # Add appropriate distribution
        distribution = instance.described_entity.lr_subclass.dataset_distribution.all()[0]
        content_file.dataset_distributions.add(distribution)
        content_file.save()
        # xml export - check that for download_location is hidden
        xml_dict = r_s.MetadataRecordSerializer(instance=instance,
                                                context={'record_id': instance.pk}).to_xml_representation()
        instance_dataset_distribution = \
            xml_dict['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass']['ms:Corpus'][
                'ms:DatasetDistribution'][0]
        self.assertTrue(instance_dataset_distribution['ms:DatasetDistributionForm'],
                        choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE)
        self.assertTrue(instance_dataset_distribution['ms:downloadLocation'], r_s.HIDDEN_VALUE_URL)


class TestAnnotationSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['annotation']
        cls.serializer = r_s.AnnotationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.AnnotationSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_annotation_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.AnnotationSerializer.Meta.fields)

    def test_annotation_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['annotation_type'] = 'fail'
        serializer = r_s.AnnotationSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_annotation_serializer_fails_with_no_annotation_type(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['annotation_type'] = []
        serializer = r_s.AnnotationSerializer(self.instance,
                                              data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_annotation_type(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['annotation_type'] = [
            choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK,
            'annotation type non CV value'
        ]
        serializer = r_s.AnnotationSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.AnnotationSerializer.Meta.model.schema_fields['annotation_type']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:annotationTypeRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:annotationTypeOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['annotation type non CV value'])

    def test_from_xml_annotation_type_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:CorpusMediaPart']['ms:CorpusTextPart']['ms:annotation']
        from_xml = r_s.AnnotationSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['annotation_type'],
                          [choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK,
                           'Annotation Type Other'])

    def test_to_display_representation_for_annotation_type_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['annotation_type'] = [choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK, 'annotation type other']
        serializer = r_s.AnnotationSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['annotation_type'],
                          {
                              'field_label': {'en': 'Annotation type'},
                              'field_value': [
                                  {
                                      'value': choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK,
                                      'label': {
                                          'en': choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES[
                                              choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK]}
                                  },
                                  {
                                      'value': 'annotation type other'
                                  }
                              ]
                          })


class TestCorpusSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['corpus']
        cls.serializer = r_s.CorpusSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)
        # unspecified_part
        cls.unspecified_json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.unspecified_json_str = cls.unspecified_json_file.read()
        cls.unspecified_json_file.close()
        cls.unspecified_part_data = json.loads(cls.unspecified_json_str)['unspecified_part']
        # tool_service
        cls.tool_json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.tool_json_str = cls.tool_json_file.read()
        cls.tool_json_file.close()
        cls.tool_data = json.loads(cls.tool_json_str)
        cls.tool_serializer = r_s.MetadataRecordSerializer(data=cls.tool_data)
        if cls.tool_serializer.is_valid():
            cls.tool_instance = cls.tool_serializer.save()
        else:
            LOGGER.debug(cls.tool_serializer.errors)
        # distribution unspecified feature
        cls.dummy_json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.dummy_json_str = cls.dummy_json_file.read()
        cls.dummy_json_file.close()
        # distribution_unspecified_feature
        cls.distribution_unspecified_feature_data = json.loads(cls.dummy_json_str)[
            'distribution_unspecified_feature']

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.CorpusSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_corpus_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()), r_s.CorpusSerializer.Meta.fields)

    def test_corpus_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['corpus_subclass'] = 'fail'
        serializer = r_s.CorpusSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_validation_fails_with_personalY_and_sensitiveY_but_no_anon(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.YES_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S
        raw_data['anonymized'] = None
        serializer = r_s.CorpusSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_validation_fails_without_personal_data_included(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = ''
        serializer = r_s.CorpusSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_validation_fails_without_sensitive_data_included(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['sensitive_data_included'] = None
        serializer = r_s.CorpusSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_validation_passes_without_personal_data_included_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = None
        serializer = r_s.CorpusSerializer(self.instance, data=raw_data, context={'for_information_only': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_validation_passes_without_sensitive_data_included_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['sensitive_data_included'] = None
        serializer = r_s.CorpusSerializer(self.instance, data=raw_data, context={'for_information_only': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_validation_fails_with_personalY_but_no_anon(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S
        raw_data['anonymized'] = None
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_validation_fails_with_sensitiveY_but_no_anon(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S
        raw_data['anonymized'] = None
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_validation_passes_with_personalY_and_anon(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.NO_S
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_validation_passes_with_sensitiveY_and_anon(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.NO_S
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_validation_passes_without_all_and_anon(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.NO_S
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        raw_data['corpus_media_part'][0]['multilinguality_type'] = None
        serializer = r_s.CorpusSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_fails_with_distribution_unspecified_feature_and_no_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data)
        for i in raw_data['dataset_distribution']:
            i['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        serializer = r_s.CorpusSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_passes_with_distribution_unspecified_feature_with_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data)
        for i in raw_data['dataset_distribution']:
            i['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.CorpusSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_text(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['dataset_distribution'][0]['distribution_text_feature'] = None
        raw_data['dataset_distribution'][1]['distribution_text_feature'] = None
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_text_part_does_not_exist(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        for n, part in enumerate(raw_data['corpus_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.TEXT:
                raw_data['corpus_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_s_c_d_passes(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['dataset_distribution'][0][
            'dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        functional_service_instance = self.tool_instance
        functional_service_instance.management_object.functional_service = True
        functional_service_instance.management_object.status = 'p'
        functional_service_instance.management_object.save()
        initial_serializer = r_s.GenericLanguageResourceSerializer(
            instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        serializer = r_s.CorpusSerializer(data=raw_data, context={'service_compliant_dataset': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_s_c_d_fails_with_different_form(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['dataset_distribution'][0][
            'dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughInterface'
        functional_service_instance = self.tool_instance
        functional_service_instance.management_object.functional_service = True
        functional_service_instance.management_object.status = 'p'
        functional_service_instance.management_object.save()
        initial_serializer = r_s.GenericLanguageResourceSerializer(
            instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        serializer = r_s.CorpusSerializer(data=raw_data, context={'service_compliant_dataset': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_s_c_d_fails_if_service_not_published(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['dataset_distribution'][0][
            'dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        functional_service_instance = self.tool_instance
        functional_service_instance.management_object.functional_service = True
        functional_service_instance.management_object.status = 'g'
        functional_service_instance.management_object.save()
        initial_serializer = r_s.GenericLanguageResourceSerializer(
            instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        serializer = r_s.CorpusSerializer(data=raw_data, context={'service_compliant_dataset': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_s_c_d_fails_if_service_not_functional(self):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['corpus']
        raw_data['dataset_distribution'][0][
            'dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        functional_service_instance = self.tool_instance
        functional_service_instance.management_object.functional_service = False
        functional_service_instance.management_object.status = 'p'
        functional_service_instance.management_object.save()
        initial_serializer = r_s.GenericLanguageResourceSerializer(
            instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        serializer = r_s.CorpusSerializer(data=raw_data, context={'service_compliant_dataset': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_passes_text_numerical(
            self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_text_numerical(
            self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for dist in raw_data['dataset_distribution']:
            dist['distribution_text_numerical_feature'] = None
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_text_numerical_part_does_not_exist(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['corpus_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.TEXT_NUMERICAL:
                raw_data['corpus_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_passes_audio(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_audio(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for dist in raw_data['dataset_distribution']:
            dist['distribution_audio_feature'] = None
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_audio_part_does_not_exist(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['corpus_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.AUDIO:
                raw_data['corpus_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_passes_video(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_video(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for dist in raw_data['dataset_distribution']:
            dist['distribution_video_feature'] = None
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_video_part_does_not_exist(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['corpus_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.IMAGE:
                raw_data['corpus_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_passes_image(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_image(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for dist in raw_data['dataset_distribution']:
            dist['distribution_image_feature'] = None
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_data_distribution_validation_fails_image_does_not_exist(self):
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['corpus_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.IMAGE:
                raw_data['corpus_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_fails_with_unspecified_part_no_for_info_context(self):
        c_data = copy.deepcopy(self.raw_data)
        c_data['unspecified_part'] = self.unspecified_part_data
        serializer = r_s.CorpusSerializer(data=c_data)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_fails_with_unspecified_part_for_info_context_false(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = self.unspecified_part_data
        context = {
            'for_information_only': False
        }
        serializer = r_s.CorpusSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())

    def test_corpus_serializer_success_with_unspecified_part_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = self.unspecified_part_data
        raw_data['corpus_media_part'] = []
        for d in raw_data['dataset_distribution']:
            d['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.CorpusSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_success_with_no_unspecified_part_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = None
        context = {
            'for_information_only': True
        }
        serializer = r_s.CorpusSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())

    def test_corpus_serializer_validation_fails_when_both_unspecified_and_mediapart_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = self.unspecified_part_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.CorpusSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)

    def test_corpus_serializer_validation_fails_when_none_unspecified_and_mediapart_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = None
        raw_data['corpus_media_part'] = []
        context = {
            'for_information_only': True
        }
        serializer = r_s.CorpusSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)

    def test_corpus_serializer_data_distribution_validation_fails_unspecified(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = self.unspecified_part_data
        raw_data['corpus_media_part'] = []
        for d in raw_data['dataset_distribution']:
            d['distribution_unspecified_feature'] = None
        serializer = r_s.CorpusSerializer(data=raw_data, context={'for_information_only': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())


class TestLexicalConceptualResourceTextPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['lcr_text_part']
        cls.serializer = r_s.LexicalConceptualResourceTextPartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LexicalConceptualResourceTextPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_lcr_text_part_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LexicalConceptualResourceTextPartSerializer.Meta.fields)

    def test_lcr_text_part_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(
            data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lcr_text_part_serializer_media_type_read_only(self):
        media_type_before_update = self.raw_data['media_type']
        self.raw_data[
            'media_type'] = 'http://w3id.org/meta-share/meta-share/textNumerical'
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(
            self.instance, data=self.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
            media_type_after_update = instance.media_type
            LOGGER.debug(self.raw_data['media_type'])
            LOGGER.debug(media_type_after_update)
            self.assertNotEqual(media_type_after_update,
                                self.raw_data['media_type'])
            self.assertEqual(media_type_after_update, media_type_before_update)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_lcr_text_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_text_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_text_part_serializer_passes_with_multiple_different_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_text_part_serializer_fails_with_multiple_same_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.LexicalConceptualResourceTextPartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.LexicalConceptualResourceTextPartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    # TODO add validation tests when validation is added


class TestLexicalConceptualResourceAudioPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['lcr_audio_part']
        cls.serializer = r_s.LexicalConceptualResourceAudioPartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LexicalConceptualResourceAudioPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_lcr_audio_part_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LexicalConceptualResourceAudioPartSerializer.Meta.fields)

    def test_lcr_audio_part_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(
            data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lcr_audio_part_serializer_media_type_read_only(self):
        media_type_before_update = self.raw_data['media_type']
        self.raw_data[
            'media_type'] = 'http://w3id.org/meta-share/meta-share/textNumerical'
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(
            self.instance, data=self.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
            media_type_after_update = instance.media_type
            LOGGER.debug(self.raw_data['media_type'])
            LOGGER.debug(media_type_after_update)
            self.assertNotEqual(media_type_after_update,
                                self.raw_data['media_type'])
            self.assertEqual(media_type_after_update, media_type_before_update)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_lcr_audio_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_audio_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_audio_part_serializer_passes_with_multiple_different_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_audio_part_serializer_fails_with_multiple_same_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.LexicalConceptualResourceAudioPartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.LexicalConceptualResourceAudioPartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    # TODO add validation tests when validation is added


class TestLexicalConceptualResourceVideoPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['lcr_video_part']
        cls.serializer = r_s.LexicalConceptualResourceVideoPartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LexicalConceptualResourceVideoPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_lcr_video_part_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LexicalConceptualResourceVideoPartSerializer.Meta.fields)

    def test_lcr_video_part_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(
            data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lcr_video_part_serializer_fails_with_no_type_of_video_content(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_video_content'] = []
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(self.instance,
                                                                      data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lcr_video_part_serializer_media_type_read_only(self):
        media_type_before_update = self.raw_data['media_type']
        self.raw_data[
            'media_type'] = 'http://w3id.org/meta-share/meta-share/textNumerical'
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(
            self.instance, data=self.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
            media_type_after_update = instance.media_type
            LOGGER.debug(self.raw_data['media_type'])
            LOGGER.debug(media_type_after_update)
            self.assertNotEqual(media_type_after_update,
                                self.raw_data['media_type'])
            self.assertEqual(media_type_after_update, media_type_before_update)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_lcr_video_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_video_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_video_part_serializer_passes_with_multiple_different_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_video_part_serializer_fails_with_multiple_same_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.LexicalConceptualResourceVideoPartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.LexicalConceptualResourceVideoPartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    # TODO add validation tests when validation is added


class TestLexicalConceptualResourceImagePartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['lcr_image_part']
        cls.serializer = r_s.LexicalConceptualResourceImagePartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LexicalConceptualResourceImagePartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_lcr_image_part_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LexicalConceptualResourceImagePartSerializer.Meta.fields)

    def test_lcr_image_part_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(
            data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lcr_image_part_serializer_fails_with_no_type_of_image_content(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_image_content'] = []
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(self.instance,
                                                                      data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lcr_image_part_serializer_media_type_read_only(self):
        media_type_before_update = self.raw_data['media_type']
        self.raw_data[
            'media_type'] = 'http://w3id.org/meta-share/meta-share/textNumerical'
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(
            self.instance, data=self.raw_data)
        if serializer.is_valid():
            instance = serializer.save()
            media_type_after_update = instance.media_type
            LOGGER.debug(self.raw_data['media_type'])
            LOGGER.debug(media_type_after_update)
            self.assertNotEqual(media_type_after_update,
                                self.raw_data['media_type'])
            self.assertEqual(media_type_after_update, media_type_before_update)
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_lcr_image_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_image_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_image_part_serializer_passes_with_multiple_different_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_image_part_serializer_fails_with_multiple_same_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_no_language(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = []
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, None)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.LexicalConceptualResourceImagePartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.LexicalConceptualResourceImagePartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    # TODO add validation tests when validation is added


class TestLexicalConceptualResourceMediaPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['lcr_image_part']
        cls.serializer = r_s.LexicalConceptualResourceMediaPartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_lcr_media_part_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue('lcr_media_type' in tuple(data.keys()))

    def test_lcr_media_part_serializer_fails_with_empty_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lcr_media_type'] = ''
        serializer = r_s.LexicalConceptualResourceMediaPartSerializer(
            self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_validation_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lcr_media_type'] = 'fail'
        serializer = r_s.LexicalConceptualResourceMediaPartSerializer(
            self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_validation_fails_with_none(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lcr_media_type'] = None
        serializer = r_s.LexicalConceptualResourceMediaPartSerializer(
            self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_validation_fails_with_no_field(self):
        raw_data = copy.deepcopy(self.raw_data)
        del raw_data['lcr_media_type']
        serializer = r_s.LexicalConceptualResourceMediaPartSerializer(
            self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_lcr_media_part_serializer_fails_with_wrong_lcr_media_type(self):
        audio_data = json.loads(self.json_str)['lcr_audio_part']
        audio_data['lcr_media_type'] = 'LexicalConceptualResourceImagePart'
        serializer = r_s.LexicalConceptualResourceMediaPartSerializer(
            data=audio_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())
        # media_type audio with image fields passes


class TestLexicalConceptualResourceSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/lcr.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity'][
            'lr_subclass']
        cls.serializer = r_s.LexicalConceptualResourceSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)
        # unspecified_part
        json_file2 = open('test_fixtures/dummy_data.json', 'r')
        json_str2 = json_file2.read()
        json_file2.close()
        cls.unspecified_part_data = json.loads(json_str2)['unspecified_part']
        # tool_service
        cls.tool_json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.tool_json_str = cls.tool_json_file.read()
        cls.tool_json_file.close()
        cls.tool_data = json.loads(cls.tool_json_str)
        cls.tool_serializer = r_s.MetadataRecordSerializer(data=cls.tool_data)
        if cls.tool_serializer.is_valid():
            cls.tool_instance = cls.tool_serializer.save()
        else:
            LOGGER.debug(cls.tool_serializer.errors)
        # distribution unspecified feature
        cls.dummy_json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.dummy_json_str = cls.dummy_json_file.read()
        cls.dummy_json_file.close()
        # distribution_unspecified_feature
        cls.distribution_unspecified_feature_data = json.loads(cls.dummy_json_str)[
            'distribution_unspecified_feature']

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LexicalConceptualResourceSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_lcr_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LexicalConceptualResourceSerializer.Meta.fields)

    def test_lcr_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['encoding_level'] = 'fail'
        serializer = r_s.LexicalConceptualResourceSerializer(self.instance,
                                                             data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_fails_with_no_encoding_level(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['encoding_level'] = []
        serializer = r_s.LexicalConceptualResourceSerializer(self.instance,
                                                             data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_s_c_d_passes(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution'][0][
            'dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        functional_service_instance = self.tool_instance
        functional_service_instance.management_object.functional_service = True
        functional_service_instance.management_object.status = 'p'
        functional_service_instance.management_object.save()
        initial_serializer = r_s.GenericLanguageResourceSerializer(
            instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context={'service_compliant_dataset': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_serializer_s_c_d_fails_with_wrong_form(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution'][0][
            'dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughInterface'
        functional_service_instance = self.tool_instance
        functional_service_instance.management_object.functional_service = True
        functional_service_instance.management_object.status = 'p'
        functional_service_instance.management_object.save()
        initial_serializer = r_s.GenericLanguageResourceSerializer(
            instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context={'service_compliant_dataset': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_s_c_d_fails_when_service_not_published(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution'][0][
            'dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        functional_service_instance = self.tool_instance
        functional_service_instance.management_object.functional_service = True
        functional_service_instance.management_object.status = 'g'
        functional_service_instance.management_object.save()
        initial_serializer = r_s.GenericLanguageResourceSerializer(
            instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context={'service_compliant_dataset': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_s_c_d_fails_when_service_not_functional(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['dataset_distribution'][0][
            'dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        functional_service_instance = self.tool_instance
        functional_service_instance.management_object.functional_service = False
        functional_service_instance.management_object.status = 'p'
        functional_service_instance.management_object.save()
        initial_serializer = r_s.GenericLanguageResourceSerializer(
            instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context={'service_compliant_dataset': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_cr_validation_fails_with_personalY_and_sensitiveY_but_no_anon(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.YES_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S
        raw_data['anonymized'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_validation_fails_with_personalY_but_no_anon(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S
        raw_data['anonymized'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_validation_fails_with_sensitiveY_but_no_anon(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S
        raw_data['anonymized'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_validation_passes_with_personalY_and_anon(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.NO_S
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_validation_passes_with_sensitiveY_and_anon(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.NO_S
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_validation_passes_without_all_and_anon(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.NO_S
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_validation_fails_without_personal_data_included(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_validation_fails_without_sensitive_data_included(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['sensitive_data_included'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_validation_passes_without_personal_data_included(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context={'for_information_only': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_validation_passes_without_sensitive_data_included_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['sensitive_data_included'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context={'for_information_only': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_lcr_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P
        initial_personal_data = self.instance.personal_data_included
        serializer = r_s.LexicalConceptualResourceSerializer(self.instance,
                                                             data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertFalse(instance.personal_data_included == initial_personal_data)
        self.assertEquals(instance.personal_data_included, choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P)

    def test_lcr_serializer_fails_with_distribution_unspecified_feature_and_no_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data)
        for i in raw_data['dataset_distribution']:
            i['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_passes_with_distribution_unspecified_feature_with_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data)
        for i in raw_data['dataset_distribution']:
            i['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())

    def test_lcr_serializer_data_distribution_validation_fails_text(self):
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        raw_data['dataset_distribution'][0]['distribution_text_feature'] = None
        raw_data['dataset_distribution'][1]['distribution_text_feature'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_data_distribution_validation_fails_text_does_not_exist(self):
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['lexical_conceptual_resource_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.IMAGE:
                raw_data['lexical_conceptual_resource_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_data_distribution_validation_fails_video(self):
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        raw_data['dataset_distribution'][0]['distribution_video_feature'] = None
        raw_data['dataset_distribution'][1]['distribution_video_feature'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_data_distribution_validation_fails_video_does_not_exist(self):
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['lexical_conceptual_resource_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.VIDEO:
                raw_data['lexical_conceptual_resource_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_data_distribution_validation_fails_image(self):
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        raw_data['dataset_distribution'][0]['distribution_image_feature'] = None
        raw_data['dataset_distribution'][1]['distribution_image_feature'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_data_distribution_validation_fails_image_does_not_exist(self):
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['lexical_conceptual_resource_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.IMAGE:
                raw_data['lexical_conceptual_resource_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_data_distribution_validation_fails_audio(self):
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        raw_data['dataset_distribution'][0]['distribution_audio_feature'] = None
        raw_data['dataset_distribution'][1]['distribution_audio_feature'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_data_distribution_validation_fails_audio_does_not_exist(self):
        json_file = open('registry/tests/fixtures/lcr.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['lexical_conceptual_resource_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.AUDIO:
                raw_data['lexical_conceptual_resource_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_fails_with_unspecified_part_no_for_info_context(self):
        c_data = copy.deepcopy(self.raw_data)
        c_data['unspecified_part'] = self.unspecified_part_data
        serializer = r_s.LexicalConceptualResourceSerializer(data=c_data)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_fails_with_unspecified_part_for_info_context_false(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = self.unspecified_part_data
        context = {
            'for_information_only': False
        }
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())

    def test_lcr_serializer_success_with_unspecified_part_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = self.unspecified_part_data
        for d in raw_data['dataset_distribution']:
            d['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        raw_data['lexical_conceptual_resource_media_part'] = []
        context = {
            'for_information_only': True
        }
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())

    def test_lcr_serializer_success_with_no_unspecified_part_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = None
        context = {
            'for_information_only': True
        }
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())

    def test_lcr_serializer_validation_fails_when_both_unspecified_and_mediapart_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = self.unspecified_part_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)

    def test_lcr_serializer_validation_fails_when_none_unspecified_and_mediapart_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = None
        raw_data['lexical_conceptual_resource_media_part'] = []
        context = {
            'for_information_only': True
        }
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)

    def test_lcr_serializer_data_distribution_validation_fails_unspecified(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['unspecified_part'] = self.unspecified_part_data
        raw_data['lexical_conceptual_resource_media_part'] = []
        for d in raw_data['dataset_distribution']:
            d['distribution_unspecified_feature'] = None
        serializer = r_s.LexicalConceptualResourceSerializer(data=raw_data, context={'for_information_only': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())


class TestModelSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = \
            json.loads(cls.json_str)['described_entity']['lr_subclass'][
                'language_description_subclass']
        cls.serializer = r_s.ModelSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ModelSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_model_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()), r_s.ModelSerializer.Meta.fields)

    def test_model_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['training_corpus_details'] = 'fail'
        serializer = r_s.ModelSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_model_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['algorithm_details'] = {}
        initial_algo = self.instance.algorithm_details
        serializer = r_s.ModelSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())
        self.assertFalse(instance.algorithm_details == initial_algo)

    def test_to_xml_representation_development_framework(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['development_framework'] = [
            choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE,
            'development framework non CV value'
        ]
        serializer = r_s.ModelSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.ModelSerializer.Meta.model.schema_fields['development_framework']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:DevelopmentFrameworkRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:DevelopmentFrameworkOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['development framework non CV value'])

    def test_to_xml_representation_model_type(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['model_type'] = [
            choices.MODEL_TYPE_RECOMMENDED_CHOICES.DEEP_LEARNING_MODEL,
            'model type non CV value'
        ]
        serializer = r_s.ModelSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        model_type_xml_field_name = r_s.ModelSerializer.Meta.model.schema_fields['model_type']
        self.assertEquals(list(xml_data[model_type_xml_field_name][0].keys()), ['ms:modelTypeRecommended'])
        self.assertEquals(list(xml_data[model_type_xml_field_name][0].values()),
                          [choices.MODEL_TYPE_RECOMMENDED_CHOICES.DEEP_LEARNING_MODEL])
        self.assertEquals(list(xml_data[model_type_xml_field_name][1].keys()), ['ms:modelTypeOther'])
        self.assertEquals(list(xml_data[model_type_xml_field_name][1].values()),
                          ['model type non CV value'])

    def test_to_xml_representation_model_function(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['model_function'] = [
            choices.MODEL_FUNCTION_RECOMMENDED_CHOICES.MACHINE_TRANSLATION,
            'model function non CV value'
        ]
        serializer = r_s.ModelSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        model_type_xml_field_name = r_s.ModelSerializer.Meta.model.schema_fields['model_function']
        self.assertEquals(list(xml_data[model_type_xml_field_name][0].keys()), ['ms:modelFunctionRecommended'])
        self.assertEquals(list(xml_data[model_type_xml_field_name][0].values()),
                          [choices.MODEL_FUNCTION_RECOMMENDED_CHOICES.MACHINE_TRANSLATION])
        self.assertEquals(list(xml_data[model_type_xml_field_name][1].keys()), ['ms:modelFunctionOther'])
        self.assertEquals(list(xml_data[model_type_xml_field_name][1].values()),
                          ['model function non CV value'])

    def test_from_xml_development_framework_works(self):
        xml_test_data = 'registry/tests/fixtures/ld_model.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:LanguageDescription']['ms:LanguageDescriptionSubclass']['ms:Model']
        from_xml = r_s.ModelSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['development_framework'],
                          [choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.TENSOR_FLOW,
                           'Other Development Framework'])

    def test_from_xml_model_type_works(self):
        xml_test_data = 'registry/tests/fixtures/ld_model.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:LanguageDescription']['ms:LanguageDescriptionSubclass']['ms:Model']
        from_xml = r_s.ModelSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['model_type'],
                          [choices.MODEL_TYPE_RECOMMENDED_CHOICES.DEEP_LEARNING_MODEL,
                           'Other Model Type'])

    def test_from_xml_model_funtion_works(self):
        xml_test_data = 'registry/tests/fixtures/ld_model.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:LanguageDescription']['ms:LanguageDescriptionSubclass']['ms:Model']
        from_xml = r_s.ModelSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['model_function'],
                          [choices.MODEL_FUNCTION_RECOMMENDED_CHOICES.MACHINE_TRANSLATION,
                           'Other Model function'])

    def test_to_display_representation_for_development_framework_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['development_framework'] = [choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE, 'dev frame other']
        serializer = r_s.ModelSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['development_framework'],
                          {
                              'field_label': {'en': 'Development framework'},
                              'field_value': [
                                  {
                                      'value': choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE,
                                      'label': {
                                          'en': choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES[
                                              choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE]}
                                  },
                                  {
                                      'value': 'dev frame other'
                                  }
                              ]
                          })

    def test_to_display_representation_for_model_type_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['model_type'] = [choices.MODEL_TYPE_RECOMMENDED_CHOICES.DEEP_LEARNING_MODEL, 'model type other']
        serializer = r_s.ModelSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['model_type'],
                          {
                              'field_label': {'en': 'Model type'},
                              'field_value': [
                                  {
                                      'value': choices.MODEL_TYPE_RECOMMENDED_CHOICES.DEEP_LEARNING_MODEL,
                                      'label': {
                                          'en': choices.MODEL_TYPE_RECOMMENDED_CHOICES[
                                              choices.MODEL_TYPE_RECOMMENDED_CHOICES.DEEP_LEARNING_MODEL]}
                                  },
                                  {
                                      'value': 'model type other'
                                  }
                              ]
                          })

    def test_to_display_representation_for_model_function_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['model_function'] = [choices.MODEL_FUNCTION_RECOMMENDED_CHOICES.MACHINE_TRANSLATION,
                                      'model function other']
        serializer = r_s.ModelSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['model_function'],
                          {
                              'field_label': {'en': 'Model function'},
                              'field_value': [
                                  {
                                      'value': choices.MODEL_FUNCTION_RECOMMENDED_CHOICES.MACHINE_TRANSLATION,
                                      'label': {
                                          'en': choices.MODEL_FUNCTION_RECOMMENDED_CHOICES[
                                              choices.MODEL_FUNCTION_RECOMMENDED_CHOICES.MACHINE_TRANSLATION]}
                                  },
                                  {
                                      'value': 'model function other'
                                  }
                              ]
                          })


class TestGrammarSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['grammar']
        cls.serializer = r_s.GrammarSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.GrammarSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_grammar_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()), r_s.GrammarSerializer.Meta.fields)

    def test_grammar_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['encoding_level'] = 'fail'
        serializer = r_s.GrammarSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_grammar_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['grammar']
        raw_data['robustness'] = {}
        initial_robustness = self.instance.robustness
        serializer = r_s.GrammarSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertFalse(instance.robustness == initial_robustness)


class TestNGramModelSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['ngram_model']
        cls.serializer = r_s.NGramModelSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.NGramModelSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_ngram_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.NGramModelSerializer.Meta.fields)

    def test_ngram_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['base_item'] = 'fail'
        serializer = r_s.NGramModelSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_ngram_serializer_fails_with_no_base_item(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['base_item'] = []
        serializer = r_s.NGramModelSerializer(self.instance,
                                              data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_ngram_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['factor'] = []
        initial_factor = self.instance.factor
        serializer = r_s.NGramModelSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertFalse(instance.factor == initial_factor)


class TestLanguageDescriptionSubclassSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['grammar']
        cls.raw_data['ld_subclass_type'] = 'Grammar'
        cls.serializer = r_s.LanguageDescriptionSubclassSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_ld_subclass_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue('ld_subclass_type' in tuple(data.keys()))

    def test_ld_subclass_serializer_fails_with_empty_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['ld_subclass_type'] = ''
        serializer = r_s.LanguageDescriptionSubclassSerializer(self.instance,
                                                               data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_subclass_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['ld_subclass_type'] = 'fail'
        serializer = r_s.LanguageDescriptionSubclassSerializer(self.instance,
                                                               data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_subclass_serializer_validation_fails_with_none(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['ld_subclass_type'] = None
        serializer = r_s.LanguageDescriptionSubclassSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_subclass_serializer_validation_fails_with_no_field(self):
        raw_data = copy.deepcopy(self.raw_data)
        del raw_data['ld_subclass_type']
        serializer = r_s.LanguageDescriptionSubclassSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_subclass_serializer_fails_with_wrong_lr_type(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['ld_subclass_type'] = 'failtype'
        serializer = r_s.LanguageDescriptionSubclassSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())


class TestLanguageDescriptionTextPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['ld_text_part']
        cls.serializer = r_s.LanguageDescriptionTextPartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LanguageDescriptionTextPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_ld_text_part_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LanguageDescriptionTextPartSerializer.Meta.fields)

    def test_ld_text_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.LanguageDescriptionTextPartSerializer(self.instance,
                                                               data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_text_part_serializer_media_type_read_only(self):
        media_type_before_update = self.raw_data['media_type']
        self.raw_data[
            'media_type'] = 'http://w3id.org/meta-share/meta-share/textNumerical'
        serializer = r_s.LanguageDescriptionTextPartSerializer(self.instance,
                                                               data=self.raw_data)
        serializer.is_valid()
        data = serializer.data
        self.assertTrue(serializer.is_valid())
        self.assertEqual(data['media_type'], media_type_before_update)

    def test_ld_text_part_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/monolingual'
        raw_data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionTextPartSerializer(self.instance,
                                                               data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_ld_text_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_text_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_text_part_serializer_passes_with_multiple_different_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'] = [
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_text_part_serializer_fails_with_multiple_same_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.LanguageDescriptionTextPartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.LanguageDescriptionTextPartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_multilinguality_obligatory_upon_condition_bilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_multilinguality_obligatory_upon_condition_multilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_multilinguality_not_obligatory_upon_condition_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionTextPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())


class TestLanguageDescriptionImagePartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['ld_image_part']
        cls.serializer = r_s.LanguageDescriptionImagePartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LanguageDescriptionImagePartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_ld_image_part_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LanguageDescriptionImagePartSerializer.Meta.fields)

    def test_ld_image_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.LanguageDescriptionImagePartSerializer(self.instance,
                                                                data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_image_part_serializer_fails_with_no_type_of_image_content(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_image_content'] = []
        serializer = r_s.LanguageDescriptionImagePartSerializer(self.instance,
                                                                data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_ld_image_part_serializer_media_type_read_only(self):
        media_type_before_update = self.raw_data['media_type']
        self.raw_data[
            'media_type'] = 'http://w3id.org/meta-share/meta-share/textNumerical'
        serializer = r_s.LanguageDescriptionImagePartSerializer(self.instance,
                                                                data=self.raw_data)
        serializer.is_valid()
        data = serializer.data
        self.assertTrue(serializer.is_valid())
        self.assertEqual(data['media_type'], media_type_before_update)

    def test_ld_image_part_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/monolingual'
        raw_data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionImagePartSerializer(self.instance,
                                                                data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_ld_image_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_image_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_image_part_serializer_passes_with_multiple_different_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'] = [
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_image_part_serializer_fails_with_multiple_same_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_no_language(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = []
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, None)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.LanguageDescriptionImagePartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.LanguageDescriptionImagePartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_multilinguality_obligatory_upon_condition_bilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_multilinguality_obligatory_upon_condition_multilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_multilinguality_not_obligatory_upon_condition_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionImagePartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())


class TestLanguageDescriptionVideoPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['ld_video_part']
        cls.serializer = r_s.LanguageDescriptionVideoPartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LanguageDescriptionVideoPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_ld_video_part_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LanguageDescriptionVideoPartSerializer.Meta.fields)

    def test_ld_video_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['multilinguality_type'] = 'fail'
        serializer = r_s.LanguageDescriptionVideoPartSerializer(self.instance,
                                                                data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_video_part_serializer_fails_with_no_type_of_video_content(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type_of_video_content'] = []
        serializer = r_s.LanguageDescriptionVideoPartSerializer(self.instance,
                                                                data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_ld_video_part_serializer_media_type_read_only(self):
        media_type_before_update = self.raw_data['media_type']
        self.raw_data[
            'media_type'] = 'http://w3id.org/meta-share/meta-share/textNumerical'
        serializer = r_s.LanguageDescriptionVideoPartSerializer(self.instance,
                                                                data=self.raw_data)
        serializer.is_valid()
        data = serializer.data
        self.assertTrue(serializer.is_valid())
        self.assertEqual(data['media_type'], media_type_before_update)

    def test_ld_video_part_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/monolingual'
        raw_data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionVideoPartSerializer(self.instance,
                                                                data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_ld_video_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_video_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_video_part_serializer_passes_with_multiple_different_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'] = [
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_video_part_serializer_fails_with_multiple_same_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.LanguageDescriptionVideoPartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.LanguageDescriptionVideoPartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_multilinguality_obligatory_upon_condition_bilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_multilinguality_obligatory_upon_condition_multilingual(self):
        fail_data = copy.deepcopy(self.raw_data)
        fail_data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        fail_data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=fail_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_multilinguality_not_obligatory_upon_condition_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        data['multilinguality_type'] = None
        serializer = r_s.LanguageDescriptionVideoPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())


class TestLanguageDescriptionMediaPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['ld_video_part']
        cls.raw_data['ld_media_type'] = 'LanguageDescriptionVideoPart'
        cls.serializer = r_s.LanguageDescriptionMediaPartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_ld_media_part_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue('ld_media_type' in tuple(data.keys()))

    def test_ld_media_part_serializer_fails_with_empty_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['ld_media_type'] = ''
        serializer = r_s.LanguageDescriptionMediaPartSerializer(self.instance,
                                                                data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_media_part_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['ld_media_type'] = 'fail'
        serializer = r_s.LanguageDescriptionMediaPartSerializer(self.instance,
                                                                data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_media_part_serializer_validation_fails_with_none(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['ld_media_type'] = None
        serializer = r_s.LanguageDescriptionMediaPartSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_media_part_serializer_validation_fails_with_no_field(self):
        raw_data = copy.deepcopy(self.raw_data)
        del raw_data['ld_media_type']
        serializer = r_s.LanguageDescriptionMediaPartSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_media_part_serializer_fails_with_wrong_type(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['ld_media_type'] = 'LanguageDescriptionImagePart'
        serializer = r_s.LanguageDescriptionMediaPartSerializer(
            data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())


class TestLanguageDescriptionSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # Grammar
        cls.json_file = open('registry/tests/fixtures/ld_grammar.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data_grammar = json.loads(cls.json_str)['described_entity'][
            'lr_subclass']
        cls.serializer_grammar = r_s.LanguageDescriptionSerializer(data=cls.raw_data_grammar)
        if cls.serializer_grammar.is_valid():
            cls.instance_grammar = cls.serializer_grammar.save()
        else:
            LOGGER.info(f'Grammar:: {cls.serializer_grammar.errors}')

        # Grammar
        cls.json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data_model = json.loads(cls.json_str)['described_entity'][
            'lr_subclass']
        cls.serializer_model = r_s.LanguageDescriptionSerializer(data=cls.raw_data_model)
        if cls.serializer_model.is_valid():
            cls.instance_model = cls.serializer_model.save()
        else:
            LOGGER.info(f'Grammar:: {cls.serializer_model.errors}')

        # unspecified_part
        json_file2 = open('test_fixtures/dummy_data.json', 'r')
        json_str2 = json_file2.read()
        json_file2.close()
        cls.unspecified_part_data = json.loads(json_str2)['unspecified_part']
        # distribution unspecified feature
        cls.dummy_json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.dummy_json_str = cls.dummy_json_file.read()
        cls.dummy_json_file.close()
        # distribution_unspecified_feature
        cls.distribution_unspecified_feature_data = json.loads(cls.dummy_json_str)['distribution_unspecified_feature']

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data_grammar.keys():
            if i not in r_s.LanguageDescriptionSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_ld_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer_grammar.is_valid())
        data = self.serializer_grammar.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LanguageDescriptionSerializer.Meta.fields)

    def test_ld_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data_model)
        raw_data['required_hardware'] = 'fail'
        serializer = r_s.LanguageDescriptionSerializer(self.instance_model,
                                                       data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_model_fails_with_non_model_ld_subclass_type(self):
        raw_data = copy.deepcopy(self.raw_data_model)
        raw_data['language_description_subclass']['ld_subclass_type'] = 'Grammar'
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_grammar_fails_with_non_grammar_ld_subclass_type(self):
        non_ml_raw_data = raw_data = copy.deepcopy(self.raw_data_grammar)
        non_ml_raw_data['ld_subclass'] = 'http://w3id.org/meta-share/meta-share/grammar'
        non_ml_raw_data['language_description_subclass']['ld_subclass_type'] = 'Model'
        serializer = r_s.LanguageDescriptionSerializer(data=non_ml_raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_other_fails_with_ld_subclass_type(self):
        non_ml_raw_data = copy.deepcopy(self.raw_data_grammar)
        non_ml_raw_data['ld_subclass'] = 'http://w3id.org/meta-share/meta-share/other'
        serializer = r_s.LanguageDescriptionSerializer(data=non_ml_raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_other_fails_with_ld_subclass_type(self):
        non_ml_raw_data = copy.deepcopy(self.raw_data_grammar)
        non_ml_raw_data['ld_subclass'] = 'http://w3id.org/meta-share/meta-share/other'
        serializer = r_s.LanguageDescriptionSerializer(data=non_ml_raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.errors['language_description_subclass'][0],
                         'Language description type must be empty.')

    def test_ld_validation_fails_with_personalY_and_sensitiveY_but_no_anon(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.YES_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S
        raw_data['anonymized'] = None
        serializer = r_s.LanguageDescriptionSerializer(self.instance_grammar, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_validation_fails_with_personalY_but_no_anon(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S
        raw_data['anonymized'] = None
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_validation_fails_with_sensitiveY_but_no_anon(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S
        raw_data['anonymized'] = None
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_validation_passes_with_personalY_and_anon(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P
        raw_data['sensitive_data_included'] = None
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_validation_passes_with_sensitiveY_and_anon(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['personal_data_included'] = None
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.NO_S
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_validation_passes_without_all_and_anon(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['personal_data_included'] = choices.PERSONAL_DATA_INCLUDED_CHOICES.NO_P
        raw_data['sensitive_data_included'] = choices.SENSITIVE_DATA_INCLUDED_CHOICES.NO_S
        raw_data['anonymized'] = choices.ANONYMIZED_CHOICES.NO_A
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_video_part_serializer_update(self):
        self.assertTrue(self.serializer_grammar.is_valid())
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['required_hardware'] = [
            'http://w3id.org/meta-share/meta-share/specialHardwareEquipment']
        serializer = r_s.LanguageDescriptionSerializer(self.instance_grammar,
                                                       data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_serializer_data_distribution_validation_fails_text(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['dataset_distribution'][0]['distribution_text_feature'] = None
        raw_data['dataset_distribution'][1]['distribution_text_feature'] = None
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_data_distribution_validation_fails_text_part_not_exist(self):
        json_file = open('registry/tests/fixtures/ld_grammar.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['language_description_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.TEXT:
                raw_data['language_description_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_data_distribution_validation_fails_video(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['dataset_distribution'][0]['distribution_video_feature'] = None
        raw_data['dataset_distribution'][1]['distribution_video_feature'] = None
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_data_distribution_validation_fails_audio_feature_not_exist(self):
        json_file = open('registry/tests/fixtures/ld_grammar.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['language_description_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.AUDIO:
                raw_data['language_description_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_data_distribution_validation_fails_image(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['dataset_distribution'][0]['distribution_image_feature'] = None
        raw_data['dataset_distribution'][1]['distribution_image_feature'] = None
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_data_distribution_validation_fails_image_feature_not_exist(self):
        json_file = open('registry/tests/fixtures/ld_grammar.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)['described_entity']['lr_subclass']
        for n, part in enumerate(raw_data['language_description_media_part']):
            if part['media_type'] == choices.MEDIA_TYPE_CHOICES.IMAGE:
                raw_data['language_description_media_part'][n] = dict()
        serializer = r_s.CorpusSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_data_distribution_validation_fails_unspecified(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['unspecified_part'] = self.unspecified_part_data
        raw_data['language_description_media_part'] = []
        for d in raw_data['dataset_distribution']:
            d['distribution_unspecified_feature'] = None
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data, context={'for_information_only': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_data_distribution_validation_fails_remove_media_part(
            self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['language_description_media_part'][0] = {}
        raw_data['language_description_media_part'][1] = {}
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_fails_with_distribution_unspecified_feature_non_model_no_for_info_context(self):
        non_ml_raw_data = copy.deepcopy(self.raw_data_grammar)
        non_ml_raw_data['unspecified_part'] = self.unspecified_part_data
        for d in non_ml_raw_data['dataset_distribution']:
            d['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        serializer = r_s.LanguageDescriptionSerializer(data=non_ml_raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_passes_with_distribution_unspecified_feature_non_model_with_for_info_context(self):
        non_ml_raw_data = copy.deepcopy(self.raw_data_grammar)
        non_ml_raw_data['unspecified_part'] = self.unspecified_part_data
        non_ml_raw_data['language_description_media_part'] = []
        for d in non_ml_raw_data['dataset_distribution']:
            d['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.LanguageDescriptionSerializer(data=non_ml_raw_data, context=context)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_serializer_passes_with_distribution_unspecified_feature_model_no_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data_model)
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_serializer_fails_without_distribution_unspecified_feature_model_no_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data_model)
        raw_data['language_description_media_part'] = None
        for i in raw_data['dataset_distribution']:
            i['distribution_unspecified_feature'] = None
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_passes_with_distribution_unspecified_feature_model_with_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data_model)
        raw_data['language_description_media_part'] = []
        for i in raw_data['dataset_distribution']:
            i['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data, context=context)
        if not serializer.is_valid():
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_serializer_fails_with_non_model_unspecified_part_no_for_info_context(self):
        non_ml_raw_data = copy.deepcopy(self.raw_data_grammar)
        non_ml_raw_data['unspecified_part'] = self.unspecified_part_data
        serializer = r_s.LanguageDescriptionSerializer(data=non_ml_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_passes_with_non_model_unspecified_part_with_for_info_context(self):
        non_ml_raw_data = copy.deepcopy(self.raw_data_grammar)
        non_ml_raw_data['unspecified_part'] = self.unspecified_part_data
        non_ml_raw_data['language_description_media_part'] = []
        for d in non_ml_raw_data['dataset_distribution']:
            d['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.LanguageDescriptionSerializer(data=non_ml_raw_data, context=context)
        if not serializer.is_valid():
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_serializer_passes_for_model_with_unspecified_part(self):
        raw_data = copy.deepcopy(self.raw_data_model)
        raw_data['unspecified_part'] = self.unspecified_part_data
        context = {
            'for_information_only': False
        }
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())

    def test_ld_serializer_fails_for_model_without_unspecified_part(self):
        raw_data = copy.deepcopy(self.raw_data_model)
        raw_data['unspecified_part'] = None
        context = {
            'for_information_only': False
        }
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_fails_with_unspecified_part_for_info_context_false(self):
        non_ml_raw_data = copy.deepcopy(self.raw_data_grammar)
        non_ml_raw_data['unspecified_part'] = self.unspecified_part_data
        context = {
            'for_information_only': False
        }
        serializer = r_s.LanguageDescriptionSerializer(data=non_ml_raw_data, context=context)
        self.assertFalse(serializer.is_valid())

    def test_ld_serializer_success_with_unspecified_part_for_info(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['unspecified_part'] = self.unspecified_part_data
        raw_data['language_description_media_part'] = []
        for d in raw_data['dataset_distribution']:
            d['distribution_unspecified_feature'] = self.distribution_unspecified_feature_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data, context=context)
        if not serializer.is_valid():
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_serializer_success_with_no_unspecified_part_for_info(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['unspecified_part'] = None
        context = {
            'for_information_only': True
        }
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data, context=context)
        if not serializer.is_valid():
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_ld_serializer_validation_fails_when_both_unspecified_and_mediapart_for_info(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['unspecified_part'] = self.unspecified_part_data
        context = {
            'for_information_only': True
        }
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)

    def test_ld_serializer_validation_fails_when_none_unspecified_and_mediapart_for_info(self):
        raw_data = copy.deepcopy(self.raw_data_grammar)
        raw_data['unspecified_part'] = None
        raw_data['language_description_media_part'] = []
        context = {
            'for_information_only': True
        }
        serializer = r_s.LanguageDescriptionSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())
        self.assertRaises(ValidationError)


class TestSoftwareDistributionSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['software_distribution']
        cls.serializer = r_s.SoftwareDistributionSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.SoftwareDistributionSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_software_distribution_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.SoftwareDistributionSerializer.Meta.fields)

    def test_software_distribution_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = 'fail'
        serializer = r_s.SoftwareDistributionSerializer(self.instance,
                                                        data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['software_distribution_form'])
        self.assertEquals(serializer.errors['software_distribution_form'][0],
                          '"fail" is not a valid choice.')

    def test_software_distribution_validation_fails_for_docker_without_docker_location(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        raw_data['docker_download_location'] = ''
        serializer = r_s.SoftwareDistributionSerializer(self.instance,
                                                        data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['docker_download_location'])
        self.assertEquals(serializer.errors['docker_download_location'][0],
                          'Docker download location cannot be blank.')

    def test_software_distribution_validation_success_for_docker_with_docker_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        raw_data['docker_download_location'] = 'redis:redis'
        serializer = r_s.SoftwareDistributionSerializer(self.instance,
                                                        data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_validation_fails_for_web_service_without_web_service_type(self):
        data = copy.deepcopy(self.raw_data)
        data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WEB_SERVICE
        data['web_service_type'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['web_service_type'])
        self.assertEquals(serializer.errors['web_service_type'][0],
                          'Web service type cannot be blank.')

    def test_software_distribution_fails_when_web_service_and_no_access_or_no_execution(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WEB_SERVICE
        raw_data['web_service_type'] = 'http://w3id.org/meta-share/meta-share/REST'
        raw_data['access_location'] = ''
        raw_data['execution_location'] = ''
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['non_field_error'])
        self.assertEquals(serializer.errors['non_field_error'][0],
                          'Execution location or Access location cannot be blank.')

    def test_software_distribution_passes_when_web_service_access_no_execution(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WEB_SERVICE
        raw_data[
            'access_location'] = "https://registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:0-0-43"
        raw_data['web_service_type'] = 'http://w3id.org/meta-share/meta-share/REST'
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_web_service_and_execution_no_access(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WEB_SERVICE
        raw_data['execution_location'] = \
            "https://registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:0-0-43"
        raw_data['web_service_type'] = 'http://w3id.org/meta-share/meta-share/REST'
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_other_and_access_no_download(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER
        raw_data['access_location'] = "https://www.accesslocation.com"
        raw_data['download_location'] = ''
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_other_and_download_no_access(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER
        raw_data['access_location'] = ''
        raw_data['download_location'] = "https://www.downloadlocation.com"
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_fails_when_other_and_no_download_no_access(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER
        raw_data['access_location'] = ''
        raw_data['download_location'] = ''
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['non_field_error'])
        self.assertEquals(serializer.errors['non_field_error'][0],
                          'Download location or Access location cannot be blank.')

    def test_software_distribution_passes_when_unspecified_and_access_no_download(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.UNSPECIFIED
        raw_data['access_location'] = "https://www.accesslocation.com"
        raw_data['download_location'] = ''
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_unspecified_and_download_no_access(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.UNSPECIFIED
        raw_data['access_location'] = ''
        raw_data['download_location'] = "https://www.downloadlocation.com"
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_fails_when_unspecified_and_no_download_no_access(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.UNSPECIFIED
        raw_data['access_location'] = ''
        raw_data['download_location'] = ''
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['non_field_error'])
        self.assertEquals(serializer.errors['non_field_error'][0],
                          'Download location or Access location cannot be blank.')

    def test_software_distribution_fails_when_executable_and_no_access_or_execution_or_download_or_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.EXECUTABLE_CODE
        raw_data['access_location'] = ''
        raw_data['execution_location'] = ''
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['non_field_error'])
        self.assertEquals(serializer.errors['non_field_error'][0],
                          'Download location, Access location, Execution location cannot be blank or data must have been related with this distribution.')

    def test_software_distribution_passes_when_executable_and_access_no_execution_no_download_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.EXECUTABLE_CODE
        raw_data['access_location'] = 'http://www.accesslocation.com'
        raw_data['execution_location'] = ''
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_executable_and_execution_no_access_no_download_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.EXECUTABLE_CODE
        raw_data['execution_location'] = "https://www.executionlocation.com"
        raw_data['access_location'] = ''
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_executable_and_download_no_access_no_execution_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.EXECUTABLE_CODE
        raw_data['download_location'] = "https://www.downloadlocation.com"
        raw_data['access_location'] = ''
        raw_data['execution_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_executable_and_package_no_access_no_execution_no_download(self):
        # Create a metadata record with a dummy distribution
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        record_serializer = r_s.MetadataRecordSerializer(data=json.loads(json_str))
        self.assertTrue(record_serializer.is_valid())
        record_instance = record_serializer.save()
        # Create a ContentFile for the metadata record
        content_file_instance = ContentFile(
            file="ed46a01bde134f1e9ab6016aa744b2b8/3c7f037650914a839afb34ddddcdd766/apache-jena-fuseki-3.17.0.zip",
            record_manager=record_instance.management_object
        )
        content_file_instance.save()
        # Relate the content file with the given software distribution of the record
        data = record_serializer.data['described_entity']['lr_subclass']['software_distribution'][0]
        data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.EXECUTABLE_CODE
        data['access_location'] = ''
        data['execution_location'] = ''
        data['download_location'] = ""
        data['package'] = ContentFileSerializer(instance=content_file_instance).data
        sf_instance = SoftwareDistribution.objects.get(pk=data['pk'])
        serializer = r_s.SoftwareDistributionSerializer(instance=sf_instance, data=data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_fails_when_library_and_no_access_or_download_or_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.LIBRARY
        raw_data['access_location'] = ''
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['non_field_error'])
        self.assertEquals(serializer.errors['non_field_error'][0],
                          'Download location, Access location cannot be blank or data must have been related with this distribution.')

    def test_software_distribution_passes_when_library_and_access_no_download_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.LIBRARY
        raw_data['access_location'] = 'http://www.accesslocation.com'
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_library_and_download_no_access_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.LIBRARY
        raw_data['download_location'] = 'http://www.accesslocation.com'
        raw_data['access_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_library_and_package_no_access_no_download(self):
        # Create a metadata record with a dummy distribution
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        record_serializer = r_s.MetadataRecordSerializer(data=json.loads(json_str))
        self.assertTrue(record_serializer.is_valid())
        record_instance = record_serializer.save()
        # Create a ContentFile for the metadata record
        content_file_instance = ContentFile(
            file="ed46a01bde134f1e9ab6016aa744b2b8/3c7f037650914a839afb34ddddcdd766/apache-jena-fuseki-3.17.0.zip",
            record_manager=record_instance.management_object
        )
        content_file_instance.save()
        # Relate the content file with the given software distribution of the record
        data = record_serializer.data['described_entity']['lr_subclass']['software_distribution'][0]
        data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.LIBRARY
        data['access_location'] = ''
        data['download_location'] = ""
        data['package'] = ContentFileSerializer(instance=content_file_instance).data
        sf_instance = SoftwareDistribution.objects.get(pk=data['pk'])
        serializer = r_s.SoftwareDistributionSerializer(instance=sf_instance, data=data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_fails_when_plugin_and_no_access_or_download_or_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.PLUGIN
        raw_data['access_location'] = ''
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['non_field_error'])
        self.assertEquals(serializer.errors['non_field_error'][0],
                          'Download location, Access location cannot be blank or data must have been related with this distribution.')

    def test_software_distribution_passes_when_plugin_and_access_no_download_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.PLUGIN
        raw_data['access_location'] = 'http://www.accesslocation.com'
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_plugin_and_download_no_access_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.PLUGIN
        raw_data['download_location'] = 'http://www.accesslocation.com'
        raw_data['access_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_plugin_and_package_no_access_no_download(self):
        # Create a metadata record with a dummy distribution
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        record_serializer = r_s.MetadataRecordSerializer(data=json.loads(json_str))
        self.assertTrue(record_serializer.is_valid())
        record_instance = record_serializer.save()
        # Create a ContentFile for the metadata record
        content_file_instance = ContentFile(
            file="ed46a01bde134f1e9ab6016aa744b2b8/3c7f037650914a839afb34ddddcdd766/apache-jena-fuseki-3.17.0.zip",
            record_manager=record_instance.management_object
        )
        content_file_instance.save()
        # Relate the content file with the given software distribution of the record
        data = record_serializer.data['described_entity']['lr_subclass']['software_distribution'][0]
        data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.PLUGIN
        data['access_location'] = ''
        data['download_location'] = ""
        data['package'] = ContentFileSerializer(instance=content_file_instance).data
        sf_instance = SoftwareDistribution.objects.get(pk=data['pk'])
        serializer = r_s.SoftwareDistributionSerializer(instance=sf_instance, data=data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_fails_when_sourceAndExecutableCode_and_no_access_or_download_or_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE
        raw_data['access_location'] = ''
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['non_field_error'])
        self.assertEquals(serializer.errors['non_field_error'][0],
                          'Download location, Access location cannot be blank or data must have been related with this distribution.')

    def test_software_distribution_passes_when_sourceAndExecutableCode_and_access_no_download_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE
        raw_data['access_location'] = 'http://www.accesslocation.com'
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_sourceAndExecutableCode_and_download_no_access_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE
        raw_data['download_location'] = 'http://www.accesslocation.com'
        raw_data['access_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_sourceAndExecutableCode_and_package_no_access_no_download(self):
        # Create a metadata record with a dummy distribution
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        record_serializer = r_s.MetadataRecordSerializer(data=json.loads(json_str))
        self.assertTrue(record_serializer.is_valid())
        record_instance = record_serializer.save()
        # Create a ContentFile for the metadata record
        content_file_instance = ContentFile(
            file="ed46a01bde134f1e9ab6016aa744b2b8/3c7f037650914a839afb34ddddcdd766/apache-jena-fuseki-3.17.0.zip",
            record_manager=record_instance.management_object
        )
        content_file_instance.save()
        # Relate the content file with the given software distribution of the record
        data = record_serializer.data['described_entity']['lr_subclass']['software_distribution'][0]
        data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE
        data['access_location'] = ''
        data['download_location'] = ""
        data['package'] = ContentFileSerializer(instance=content_file_instance).data
        sf_instance = SoftwareDistribution.objects.get(pk=data['pk'])
        serializer = r_s.SoftwareDistributionSerializer(instance=sf_instance, data=data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_fails_when_sourceCode_and_no_access_or_download_or_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_CODE
        raw_data['access_location'] = ''
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['non_field_error'])
        self.assertEquals(serializer.errors['non_field_error'][0],
                          'Download location, Access location cannot be blank or data must have been related with this distribution.')

    def test_software_distribution_passes_when_sourceCode_and_access_no_download_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_CODE
        raw_data['access_location'] = 'http://www.accesslocation.com'
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_sourceCode_and_download_no_access_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_CODE
        raw_data['download_location'] = 'http://www.accesslocation.com'
        raw_data['access_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_sourceCode_and_package_no_access_no_download(self):
        # Create a metadata record with a dummy distribution
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        record_serializer = r_s.MetadataRecordSerializer(data=json.loads(json_str))
        self.assertTrue(record_serializer.is_valid())
        record_instance = record_serializer.save()
        # Create a ContentFile for the metadata record
        content_file_instance = ContentFile(
            file="ed46a01bde134f1e9ab6016aa744b2b8/3c7f037650914a839afb34ddddcdd766/apache-jena-fuseki-3.17.0.zip",
            record_manager=record_instance.management_object
        )
        content_file_instance.save()
        # Relate the content file with the given software distribution of the record
        data = record_serializer.data['described_entity']['lr_subclass']['software_distribution'][0]
        data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_CODE
        data['access_location'] = ''
        data['download_location'] = ""
        data['package'] = ContentFileSerializer(instance=content_file_instance).data
        sf_instance = SoftwareDistribution.objects.get(pk=data['pk'])
        serializer = r_s.SoftwareDistributionSerializer(instance=sf_instance, data=data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_fails_when_workflowFile_and_no_access_or_download_or_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WORKFLOW_FILE
        raw_data['access_location'] = ''
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['non_field_error'])
        self.assertEquals(serializer.errors['non_field_error'][0],
                          'Download location, Access location cannot be blank or data must have been related with this distribution.')

    def test_software_distribution_passes_when_workflowFile_and_access_no_download_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WORKFLOW_FILE
        raw_data['access_location'] = 'http://www.accesslocation.com'
        raw_data['download_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_workflowFile_and_download_no_access_no_package(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WORKFLOW_FILE
        raw_data['download_location'] = 'http://www.accesslocation.com'
        raw_data['access_location'] = ''
        raw_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_passes_when_workflowFile_and_package_no_access_no_download(self):
        # Create a metadata record with a dummy distribution
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        record_serializer = r_s.MetadataRecordSerializer(data=json.loads(json_str))
        self.assertTrue(record_serializer.is_valid())
        record_instance = record_serializer.save()
        # Create a ContentFile for the metadata record
        content_file_instance = ContentFile(
            file="ed46a01bde134f1e9ab6016aa744b2b8/3c7f037650914a839afb34ddddcdd766/apache-jena-fuseki-3.17.0.zip",
            record_manager=record_instance.management_object
        )
        content_file_instance.save()
        # Relate the content file with the given software distribution of the record
        data = record_serializer.data['described_entity']['lr_subclass']['software_distribution'][0]
        data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WORKFLOW_FILE
        data['access_location'] = ''
        data['download_location'] = ""
        data['package'] = ContentFileSerializer(instance=content_file_instance).data
        sf_instance = SoftwareDistribution.objects.get(pk=data['pk'])
        serializer = r_s.SoftwareDistributionSerializer(instance=sf_instance, data=data)
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_validation_fails_licence_terms_is_missing_no_for_info(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['licence_terms'] = []
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(str(serializer.errors['licence_terms']),
                          '[ErrorDetail(string=\'Licence may not be empty.\', code=\'invalid\')]')

    def test_software_distribution_validation_success_licence_terms_is_missing_for_info(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['licence_terms'] = []
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'for_information_only': True})
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_validation_success_access_rights_is_missing_no_for_info(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['access_rights'] = []
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()

    def test_software_distribution_validation_passes_access_rights_is_missing_but_licence_terms_exist_for_info(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['access_rights'] = []
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'for_information_only': True})
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_validation_fails_access_rights_is_missing_and_licence_terms_are_missing_for_info(
            self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['licence_terms'] = []
        distribution_data['access_rights'] = []
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'for_information_only': True})
        self.assertFalse(serializer.is_valid())
        self.assertEquals(str(serializer.errors['access_rights']),
                          '[ErrorDetail(string=\'Access rights may not be empty.\', code=\'invalid\')]')

    def test_software_distribution_display_represenation_hidden_docker_download_location_private_resource_true(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['docker_download_location'] = 'redis'
        distribution_data['private_resource'] = True
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        display_data = serializer.to_display_representation(instance=instance)
        self.assertEqual(display_data['docker_download_location']['field_value'], r_s.HIDDEN_VALUE_STR)

    def test_software_distribution_display_representation_hidden_values_dockerImage_functional_private_resource_true(
            self):
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        software_distribution = raw_data['described_entity']['lr_subclass']['software_distribution'][0]
        software_distribution['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        software_distribution['docker_download_location'] = "registry.gitlab.com/a/b"
        software_distribution['service_adapter_download_location'] = "registry.gitlab.com/a/b:adapt"
        software_distribution['execution_location'] = "http://www.dummylocation.com"
        software_distribution['private_resource'] = True
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Make instance a functional service
        instance.management_object.functional_service = True
        instance.management_object.save()
        display_data = r_s.MetadataRecordSerializer(instance=instance).to_display_representation()
        # Check that for functional service some information is hidden
        instance_software_distribution = \
            display_data['described_entity']['field_value']['lr_subclass']['field_value']['software_distribution'][
                'field_value'][0]
        self.assertTrue(instance_software_distribution['software_distribution_form']['field_value'],
                        choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE)
        self.assertEquals(instance_software_distribution['docker_download_location']['field_value'],
                          r_s.HIDDEN_VALUE_STR)
        self.assertEquals(instance_software_distribution['service_adapter_download_location']['field_value'],
                          r_s.HIDDEN_VALUE_STR)
        self.assertEquals(instance_software_distribution['execution_location']['field_value'], r_s.HIDDEN_VALUE_URL)

    def test_software_distribution_display_representation_hidden_values_dockerImage_functional_private_resource_false(
            self):
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        software_distribution = raw_data['described_entity']['lr_subclass']['software_distribution'][0]
        software_distribution['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        software_distribution['docker_download_location'] = "registry.gitlab.com/a/b"
        software_distribution['service_adapter_download_location'] = "registry.gitlab.com/a/b:adapt"
        software_distribution['execution_location'] = "http://www.dummylocation.com"
        software_distribution['private_resource'] = False
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Make instance a functional service
        instance.management_object.functional_service = True
        instance.management_object.save()
        display_data = r_s.MetadataRecordSerializer(instance=instance).to_display_representation()
        # Check that for functional service some information is hidden
        instance_software_distribution = \
            display_data['described_entity']['field_value']['lr_subclass']['field_value']['software_distribution'][
                'field_value'][0]
        self.assertTrue(instance_software_distribution['software_distribution_form']['field_value'],
                        choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE)
        self.assertFalse(
            instance_software_distribution['docker_download_location']['field_value'] == r_s.HIDDEN_VALUE_STR)
        self.assertFalse(
            instance_software_distribution['service_adapter_download_location']['field_value'] == r_s.HIDDEN_VALUE_STR)
        self.assertFalse(instance_software_distribution['execution_location']['field_value'] == r_s.HIDDEN_VALUE_URL)

    def test_software_distribution_display_representation_hidden_values_private_resource_none(self):
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        software_distribution = raw_data['described_entity']['lr_subclass']['software_distribution'][0]
        software_distribution['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WORKFLOW_FILE
        software_distribution['docker_download_location'] = "registry.gitlab.com/a/b"
        software_distribution['service_adapter_download_location'] = "registry.gitlab.com/a/b:adapt"
        software_distribution['execution_location'] = "http://www.dummylocation.com"
        software_distribution['download_location'] = "http://www.dummylocation.com"
        software_distribution['access_location'] = "http://www.dummylocation.com"
        software_distribution['private_resource'] = None
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if not serializer.is_valid():
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        display_data = r_s.MetadataRecordSerializer(instance=instance).to_display_representation()
        # Check that for functional service some information is hidden
        instance_software_distribution = \
            display_data['described_entity']['field_value']['lr_subclass']['field_value']['software_distribution'][
                'field_value'][0]
        self.assertTrue(instance_software_distribution['software_distribution_form']['field_value'],
                        choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE)
        self.assertTrue(
            instance_software_distribution['docker_download_location']['field_value'] == r_s.HIDDEN_VALUE_STR)
        self.assertTrue(
            instance_software_distribution['service_adapter_download_location']['field_value'] == r_s.HIDDEN_VALUE_STR)
        self.assertTrue(instance_software_distribution['execution_location']['field_value'] == r_s.HIDDEN_VALUE_URL)

    def test_software_distribution_fails_when_docker_image_and_private_resource(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        del distribution_data['private_resource']
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['private_resource'])
        self.assertEquals(serializer.errors['private_resource'][0], 'Private cannot be blank.')

    def test_software_distribution_fails_when_functional_service_docker_and_no_docker_download_location(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        distribution_data['docker_download_location'] = ''
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'functional_service': True})
        self.assertFalse(serializer.is_valid())
        self.assertEquals(serializer.errors['docker_download_location'][0],
                          'Docker download location cannot be blank.')

    def test_software_distribution_successes_when_functional_service_docker_and_execution_location_docker_download_location(
            self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        distribution_data['docker_download_location'] = 'redis'
        distribution_data['execution_location'] = 'http://www.location.com'
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'functional_service': True})
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_successes_when_xml_upload_with_data_executable_no_download_no_access_no_execution_no_package(
            self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.EXECUTABLE_CODE
        distribution_data['access_location'] = ''
        distribution_data['execution_location'] = ''
        distribution_data['download_location'] = ''
        distribution_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'xml_upload_with_data': True})
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_successes_when_xml_upload_with_data_library_no_download_no_access_no_package(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.LIBRARY
        distribution_data['access_location'] = ''
        distribution_data['download_location'] = ''
        distribution_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'xml_upload_with_data': True})
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_successes_when_xml_upload_with_data_plugin_no_download_no_access_no_package(self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.PLUGIN
        distribution_data['access_location'] = ''
        distribution_data['download_location'] = ''
        distribution_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'xml_upload_with_data': True})
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_successes_when_xml_upload_with_data_sourceAndExecutableCode_no_download_no_access_no_package(
            self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data[
            'software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE
        distribution_data['access_location'] = ''
        distribution_data['download_location'] = ''
        distribution_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'xml_upload_with_data': True})
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_successes_when_xml_upload_with_data_sourceCode_no_download_no_access_no_package(
            self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_CODE
        distribution_data['access_location'] = ''
        distribution_data['download_location'] = ''
        distribution_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'xml_upload_with_data': True})
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_successes_when_xml_upload_with_data_workflowFile_no_download_no_access_no_package(
            self):
        distribution_data = copy.deepcopy(self.raw_data)
        distribution_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WORKFLOW_FILE
        distribution_data['access_location'] = ''
        distribution_data['download_location'] = ''
        distribution_data['package'] = None
        serializer = r_s.SoftwareDistributionSerializer(data=distribution_data, context={'xml_upload_with_data': True})
        self.assertTrue(serializer.is_valid())

    def test_software_distribution_xml_representation_hidden_values_dockerImage_functional(self):
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        software_distribution = raw_data['described_entity']['lr_subclass']['software_distribution'][0]
        software_distribution['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        software_distribution['docker_download_location'] = "registry.gitlab.com/a/b"
        software_distribution['service_adapter_download_location'] = "registry.gitlab.com/a/b:adapt"
        software_distribution['execution_location'] = "http://www.dummylocation.com"
        software_distribution['private_resource'] = False
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Make instance a functional service
        instance.management_object.functional_service = True
        instance.management_object.save()
        xml_dict = r_s.MetadataRecordSerializer(instance=instance).to_xml_representation()
        # Check that for functional service some information is hidden
        instance_software_distribution = \
            xml_dict['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass']['ms:ToolService'][
                'ms:SoftwareDistribution'][0]
        self.assertTrue(instance_software_distribution['ms:SoftwareDistributionForm'],
                        choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE)
        self.assertTrue(instance_software_distribution['ms:dockerDownloadLocation'], r_s.HIDDEN_VALUE_STR)
        self.assertTrue(instance_software_distribution['ms:serviceAdapterDownloadLocation'], r_s.HIDDEN_VALUE_STR)
        self.assertTrue(instance_software_distribution['ms:executionLocation'], r_s.HIDDEN_VALUE_URL)

    def test_software_distribution_xml_representation_hidden_values_package(self):
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        software_distribution = raw_data['described_entity']['lr_subclass']['software_distribution'][0]
        software_distribution['software_distribution_form'] = \
            choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE
        software_distribution['download_location'] = "http://www.dummylocation.com"
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        serializer.is_valid()
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Add data to the instance
        m = instance.management_object
        content_file = ContentFile.objects.create(file='test_general_purpose_annotated_text.zip',
                                                  record_manager=m)
        # Add appropriate distribution
        distribution = instance.described_entity.lr_subclass.software_distribution.all()[0]
        content_file.software_distributions.add(distribution)
        content_file.save()
        # xml export - check that for download_location is hidden
        xml_dict = r_s.MetadataRecordSerializer(instance=instance,
                                                context={'record_id': instance.pk}).to_xml_representation()
        instance_software_distribution = \
            xml_dict['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass']['ms:ToolService'][
                'ms:SoftwareDistribution'][0]
        self.assertTrue(instance_software_distribution['ms:SoftwareDistributionForm'],
                        choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE)
        self.assertTrue(instance_software_distribution['ms:downloadLocation'], r_s.HIDDEN_VALUE_STR)

    def test_software_distribution_xml_representation_hidden_values_private_resource(self):
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        software_distribution = raw_data['described_entity']['lr_subclass']['software_distribution'][0]
        software_distribution['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        software_distribution['docker_download_location'] = "registry.gitlab.com/a/b"
        software_distribution['private_resource'] = True
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_dict = r_s.MetadataRecordSerializer(instance=instance).to_xml_representation()
        # Check that for functional service some information is hidden
        instance_software_distribution = \
            xml_dict['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass']['ms:ToolService'][
                'ms:SoftwareDistribution'][0]
        self.assertTrue(instance_software_distribution['ms:SoftwareDistributionForm'],
                        choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE)
        self.assertTrue(instance_software_distribution['ms:dockerDownloadLocation'], r_s.HIDDEN_VALUE_STR)
        self.assertTrue(instance_software_distribution['ms:privateResource'], True)

    def test_software_distribution_validation_fails_for_docker_without_invalid_docker_location(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        invalid_docker_location = 'https://hub.docker.com/repository/docker/username/image'
        raw_data['docker_download_location'] = invalid_docker_location
        serializer = r_s.SoftwareDistributionSerializer(self.instance,
                                                        data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['docker_download_location'])
        self.assertEquals(serializer.errors['docker_download_location'][0],
                          f'"{invalid_docker_location}" does not comply with the expected input. Follow the template: [host.name/]image/name:version')

    def test_software_distribution_validation_fails_for_docker_without_invalid_service_adapter(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        invalid_service_adapter = 'https://hub.docker.com/repository/docker/username/image'
        raw_data['service_adapter_download_location'] = invalid_service_adapter
        serializer = r_s.SoftwareDistributionSerializer(self.instance,
                                                        data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertEquals(list(serializer.errors.keys()), ['service_adapter_download_location'])
        self.assertEquals(serializer.errors['service_adapter_download_location'][0],
                          f'"{invalid_service_adapter}" does not comply with the expected input. Follow the template: [host.name/]image/name:version')


class TestProcessingResourceSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['processing_resource']
        cls.serializer = r_s.ProcessingResourceSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ProcessingResourceSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_processing_resource_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ProcessingResourceSerializer.Meta.fields)

    def test_processing_resource_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "glottolog_code": 'bava1246',
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "glottolog_code": 'sind1281',
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.ProcessingResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_processing_resource_serializer_passes_with_multiple_different_languages_check_same_glottolog(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "glottolog_code": 'sind1281',
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "glottolog_code": 'bava1246',
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.ProcessingResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_processing_resource_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "glottolog_code": 'testcode',
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "glottolog_code": 'testcode',
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.ProcessingResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_processing_resource_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['processing_resource_type'] = 'fail'
        serializer = r_s.ProcessingResourceSerializer(self.instance,
                                                      data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_annotation_type(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['annotation_type'] = [
            choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK,
            'annotation type non CV value'
        ]
        serializer = r_s.ProcessingResourceSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.ProcessingResourceSerializer.Meta.model.schema_fields['annotation_type']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:annotationTypeRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:annotationTypeOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['annotation type non CV value'])

    def test_from_xml_annotation_type_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/tool.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:ToolService']['ms:inputContentResource']
        from_xml = r_s.ProcessingResourceSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['annotation_type'],
                          [choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK,
                           'Annotation Type Other'])

    def test_to_display_representation_for_annotation_type_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['annotation_type'] = [choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK, 'annotation type other']
        serializer = r_s.ProcessingResourceSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['annotation_type'],
                          {
                              'field_label': {'en': 'Annotation type'},
                              'field_value': [
                                  {
                                      'value': choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK,
                                      'label': {
                                          'en': choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES[
                                              choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES.CHUNK]}
                                  },
                                  {
                                      'value': 'annotation type other'
                                  }
                              ]
                          })

    def test_to_xml_representation_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [
            choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
            'data format non CV value'
        ]
        serializer = r_s.ProcessingResourceSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.ProcessingResourceSerializer.Meta.model.schema_fields[
            'data_format']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['data format non CV value'])

    def test_from_xml_data_format_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/tool.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:ToolService']['ms:inputContentResource']
        from_xml = r_s.ProcessingResourceSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                           'data format non CV value'])

    def test_to_display_representation_for_data_format_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS, 'data format non CV value']
        serializer = r_s.ProcessingResourceSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value': [
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  },
                                  {
                                      'value': 'data format non CV value'
                                  }
                              ]
                          })


class TestPerformanceIndicatorSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.raw_data = {
            'metric': 'http://w3id.org/meta-share/meta-share/ART',
            'measure': 10.0,
            'unit_of_measure_metric': 'unit of measure',
        }
        cls.serializer = r_s.PerformanceIndicatorSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.PerformanceIndicatorSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_performance_indicator_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.PerformanceIndicatorSerializer.Meta.fields)

    def test_performance_indicator_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metric'] = 'fail'
        serializer = r_s.PerformanceIndicatorSerializer(self.instance,
                                                        data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestEvaluationSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['evaluation']
        cls.serializer = r_s.EvaluationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.EvaluationSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_evaluation_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.EvaluationSerializer.Meta.fields)

    def test_evaluation_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['evaluation_level'] = 'fail'
        serializer = r_s.EvaluationSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestParameterSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['parameter']
        cls.serializer = r_s.ParameterSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ParameterSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_parameter_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ParameterSerializer.Meta.fields)

    def test_parameter_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['parameter_label'] = 'fail'
        serializer = r_s.ParameterSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [
            choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
            'data format non CV value'
        ]
        serializer = r_s.ParameterSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.ParameterSerializer.Meta.model.schema_fields[
            'data_format']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['data format non CV value'])

    def test_from_xml_data_format_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/tool.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:ToolService']['ms:parameter']
        from_xml = r_s.ParameterSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                           'data format non CV value'])

    def test_to_display_representation_for_data_format_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS, 'data format non CV value']
        serializer = r_s.ParameterSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value': [
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  },
                                  {
                                      'value': 'data format non CV value'
                                  }
                              ]
                          })


class TestToolServiceSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity'][
            'lr_subclass']
        cls.serializer = r_s.ToolServiceSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ToolServiceSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_tool_service_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.ToolServiceSerializer.Meta.fields)

    def test_tool_service_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['function'] = 'fail'
        serializer = r_s.ToolServiceSerializer(self.instance,
                                               data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_tool_service_fails_with_language_dependent_no_language_input(self):
        data = copy.deepcopy(self.raw_data)
        data['language_dependent'] = True
        for input in data['input_content_resource']:
            input['language'] = None
        serializer = r_s.ToolServiceSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_tool_service_serializer_fails_with_no_function(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['function'] = []
        serializer = r_s.ToolServiceSerializer(self.instance,
                                               data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_tool_service_fails_with_language_dependent_no_language_output(
            self):
        data = copy.deepcopy(self.raw_data)
        data['language_dependent'] = True
        for output in data['output_resource']:
            output['language'] = None
        serializer = r_s.ToolServiceSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_tool_service_success_with_language_dependent_no_output(self):
        data = copy.deepcopy(self.raw_data)
        data['language_dependent'] = True
        data['output_resource'] = None
        serializer = r_s.ToolServiceSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_from_xml_representation_fails_with_bad_data(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:LRSubclass']['ms:ToolService']
        xml_data_fail = copy.deepcopy(xml_data)
        xml_data_fail['ms:inputContentResource'] = OrderedDict(
            [('language', v) if k == 'ms:language' else (k, v) for k, v in
             xml_data['ms:inputContentResource'].items()])
        from_xml_fail = r_s.ToolServiceSerializer().from_xml_representation(
            xml_data=xml_data_fail)
        self.assertEquals(
            from_xml_fail['input_content_resource'][0]['language'], [])
        serializer = r_s.ToolServiceSerializer(data=from_xml_fail)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_from_xml_recommended_function_works(self):
        xml_test_data = 'test_fixtures/xml_serializer_test.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:ToolService']
        from_xml = r_s.ToolServiceSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['function'], [choices.LT_CLASS_RECOMMENDED_CHOICES.TRANSLATION, 'ltclass other'])

    def test_from_xml_for_development_framework_works(self):
        xml_test_data = 'test_fixtures/xml_serializer_test.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:ToolService']
        from_xml = r_s.ToolServiceSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['development_framework'],
                          [choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.TENSOR_FLOW,
                           'Other Development Framework'])

    def test_to_display_representation_for_function_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['function'] = [choices.LT_CLASS_RECOMMENDED_CHOICES.TRANSLATION, 'ltarea other']
        serializer = r_s.ToolServiceSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['function'],
                          {
                              'field_label': {'en': 'Function'},
                              'field_value': [
                                  {
                                      'value': choices.LT_CLASS_RECOMMENDED_CHOICES.TRANSLATION,
                                      'label': {
                                          'en': choices.LT_CLASS_RECOMMENDED_CHOICES[
                                              choices.LT_CLASS_RECOMMENDED_CHOICES.TRANSLATION]}
                                  },
                                  {
                                      'value': 'ltarea other'
                                  }
                              ]
                          })

    def test_to_display_representation_for_development_framework_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['development_framework'] = [choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE, 'dev frame other']
        serializer = r_s.ToolServiceSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['development_framework'],
                          {
                              'field_label': {'en': 'Development framework'},
                              'field_value': [
                                  {
                                      'value': choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE,
                                      'label': {
                                          'en': choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES[
                                              choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE]}
                                  },
                                  {
                                      'value': 'dev frame other'
                                  }
                              ]
                          })

    def test_to_xml_representation_function_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['function'] = [
            choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT,
            'lt class non CV value'
        ]
        serializer = r_s.ToolServiceSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        ltclass_xml_field_name = r_s.ToolServiceSerializer.Meta.model.schema_fields['function']
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].keys()), ['ms:LTClassRecommended'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].values()),
                          [choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].keys()), ['ms:LTClassOther'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].values()),
                          ['lt class non CV value'])

    def test_to_xml_representation_for_development_framework_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['development_framework'] = [
            choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE,
            'development framework non CV value'
        ]
        serializer = r_s.ToolServiceSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.ToolServiceSerializer.Meta.model.schema_fields['development_framework']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:DevelopmentFrameworkRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES.CAFFE])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:DevelopmentFrameworkOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['development framework non CV value'])

    def test_functional_toolservice_fails_when_no_distribution_with_dockerImage(self):
        data = copy.deepcopy(self.raw_data)
        # New distribution
        soft_dist = data['software_distribution'][0]
        soft_dist['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.LIBRARY
        soft_dist['access_location'] = 'http://www.location.com'
        data['software_distribution'] = [soft_dist]
        serializer = r_s.ToolServiceSerializer(data=data, context={'functional_service': True})
        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.errors['software_distribution'][0],
                         'There must be one Software distribution with Software'
                         ' distribution form=http://w3id.org/meta-share/meta-share/dockerImage')

    def test_functional_toolservice_success_when_distribution_with_dockerImage(self):
        data = copy.deepcopy(self.raw_data)
        # New distribution
        soft_dist = data['software_distribution'][0]
        soft_dist['software_distribution_form'] = choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE
        soft_dist['docker_download_location'] = 'image/name:version'
        soft_dist['execution_location'] = 'http://www.location.com'
        soft_dist['private_resource'] = False
        data['software_distribution'] = [soft_dist]
        serializer = r_s.ToolServiceSerializer(data=data, context={'functional_service': True})
        self.assertTrue(serializer.is_valid())


class TestLRSubclassSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['corpus']
        cls.serializer = r_s.LRSubclassSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_lr_subclass_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue('lr_type' in tuple(data.keys()))

    def test_lr_subclass_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lr_type'] = ''
        serializer = r_s.LRSubclassSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_lr_subclass_serializer_fails_with_fail_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lr_type'] = 'fail'
        serializer = r_s.LRSubclassSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_lr_subclass_serializer_validation_fails_with_none(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lr_type'] = None
        serializer = r_s.LRSubclassSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_lr_subclass_serializer_validation_fails_with_no_field(self):
        raw_data = copy.deepcopy(self.raw_data)
        del raw_data['lr_type']
        serializer = r_s.LRSubclassSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_lr_subclass_serializer_fails_with_wrong_lr_type(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lr_type'] = 'LexicalConceptualResource'
        serializer = r_s.LRSubclassSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())


class TestLanguageResourceSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.LanguageResourceSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LanguageResourceSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_lr_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LanguageResourceSerializer.Meta.fields)

    def test_language_resource_serializer_fails_when_same_schemes_are_given(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lr_identifier'] = [
            {
                "lr_identifier_scheme": 'http://purl.org/spar/datacite/ark',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "lr_identifier_scheme": 'http://purl.org/spar/datacite/ark',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.LanguageResourceSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_lr_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['description'] = 'fail'
        raw_data['keyword'] = ['fail', 'fail_again']
        serializer = r_s.LanguageResourceSerializer(self.instance,
                                                    data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lr_serializer_fails_with_no_keyword(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['keyword'] = []
        serializer = r_s.LanguageResourceSerializer(self.instance,
                                                    data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_lr_serializer_with_no_version_provided_gets_default_value(self):
        raw_data = copy.deepcopy(self.raw_data)
        del raw_data['version']
        serializer = r_s.LanguageResourceSerializer(self.instance,
                                                    data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        self.assertEqual(serializer.instance.version, serializer.fields.get('version').default)

    def test_lr_serializer_with_version_empty_string_gets_default_value(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['version'] = ''
        serializer = r_s.LanguageResourceSerializer(self.instance,
                                                    data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        self.assertEqual(serializer.instance.version, serializer.fields.get('version').default)

    def test_lr_serializer_can_create_own_identifier(self):
        data = {
            "entity_type": "",
            "physical_resource": [],
            "resource_name": {
                'en': 'lr_name'
            },
            "resource_short_name": {
                'en': 'lr_short_name'
            },
            "description": {
                'en': 'description'
            },
            "lr_identifier": [],
            "logo": "http://www.logo.com",
            "version": '1.1.0',
            "version_date": None,
            "update_frequency": None,
            "revision": None,
            "additional_info": [
                {
                    "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                    "email": ""
                }
            ],
            "contact": [{
                'actor_type': 'Person',
                'surname': {
                    'en': 'random_surname'
                },
                'given_name': {
                    'en': 'random_given_name'
                },
                'personal_identifier': [{
                    'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                    'value': "registry_value_rndm"
                }],
                'email': ['validated@email.com'],
            }],
            "mailing_list_name": [],
            "discussion_url": [],
            "citation_text": {
                'en': 'citation text'
            },
            "ipr_holder": [],
            "keyword": [
                {
                    "en": "GATE"
                },
                {
                    "en": "NER"
                },
                {
                    "en": "English"
                }
            ],
            "domain": [{
                'category_label': {
                    'en': 'label'
                },
                'domain_identifier': {
                    'domain_classification_scheme': 'http://w3id.org/meta-share/meta-share/ANC_domainClassification',
                    'value': "doi_value_id"
                }
            }],
            "subject": [{
                'category_label': {
                    'en': 'category'
                },
                'subject_identifier': {
                    'subject_classification_scheme': 'http://w3id.org/meta-share/meta-share/MeSH_classification',
                    'value': "doi_value_id"
                }
            }],
            "resource_provider": [],
            "publication_date": None,
            "resource_creator": [],
            "creation_start_date": None,
            "creation_end_date": None,
            "creation_mode": None,
            "is_created_by": [{
                'resource_name': {
                    'en': 'Resource'
                },
                'resource_Short_name': {
                    'en': 're'
                },
                'lr_identifier': [{
                    'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                    'value': "doi_value_id"
                }],
                'version': '1.0.1'
            }],
            "has_original_source": [{
                'resource_name': {
                    'en': 'Resource'
                },
                'resource_Short_name': {
                    'en': 're'
                },
                'lr_identifier': [{
                    'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                    'value': "doi_value_id"
                }],
                'version': '1.0.1'
            }],
            "original_source_description": None,
            "funding_project": [{
                'project_name': {
                    'en': 'Project'
                },
                'project_identifier': [{
                    'project_identifier_scheme': 'http://w3id.org/meta-share/meta-share/other',
                    'value': "doi_value_id"
                }],
                'website': ['http://www.orgwebsite.com'],
            }],
            "intended_application": [],
            "actual_use": [{
                'used_in_application': ['pyThia'],
                'has_outcome': [{
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                }],
                'usage_project': [{
                    'project_name': {
                        'en': 'Project'
                    },
                    'project_identifier': [{
                        'project_identifier_scheme': 'http://w3id.org/meta-share/meta-share/other',
                        'value': "doi_value_id"
                    }],
                    'website': ['http://www.orgwebsite.com'],
                }],
                'usage_report': [{
                    'title': {
                        'en': 'doc title'
                    },
                    'author': {
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "registry_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    },
                    'document_identifier': [{
                        'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }]
                }],
                'actual_use_details': {
                    'en': 'text classification training set'
                }
            }],
            "validated": True,
            "validation": [{
                'validation_type': 'http://w3id.org/meta-share/meta-share/content',
                'validation_mode': 'http://w3id.org/meta-share/meta-share/automatic',
                'validation_details':
                    {
                        'en': 'validation details'
                    },
                'validation_extent': 'http://w3id.org/meta-share/meta-share/full',
                'is_validated_by': [{
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                }],
                'validation_report': [{
                    'title': {
                        'en': 'doc title'
                    },
                    'author': {
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "registry_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    },
                    'document_identifier': [{
                        'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }]
                }],
                'validator': [{
                    'actor_type': 'Person',
                    'surname': {
                        'en': 'random_surname'
                    },
                    'given_name': {
                        'en': 'random_given_name'
                    },
                    'personal_identifier': [{
                        'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "registry_value_rndm"
                    }],
                    'email': ['validated@email.com'],
                }],
                'language_resource_of_validation': {
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                }
            }],
            "is_documented_by": [{
                'title': {
                    'en': 'doc title'
                },
                'author': {
                    'actor_type': 'Person',
                    'surname': {
                        'en': 'random_surname'
                    },
                    'given_name': {
                        'en': 'random_given_name'
                    },
                    'personal_identifier': [{
                        'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "registry_value_rndm"
                    }],
                    'email': ['validated@email.com'],
                },
                'document_identifier': [{
                    'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                    'value': "doi_value_id"
                }]
            }],
            "is_described_by": [],
            "is_cited_by": [],
            "is_reviewed_by": [],
            "is_part_of": [{
                'resource_name': {
                    'en': 'Resource'
                },
                'resource_Short_name': {
                    'en': 're'
                },
                'lr_identifier': [{
                    'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                    'value': "doi_value_id"
                }],
                'version': '1.0.1'
            }],
            "is_part_with": [],
            "is_similar_to": [],
            "is_exact_match_with": [],
            "has_metadata": [self.json_data],
            "is_archived_by": [],
            "is_continuation_of": [],
            "replaces": [],
            "is_version_of": None,
            "relation": [{
                'relation_type': {
                    'en': 'relation type'
                },
                'related_lr': {
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                },
            }],
            "support_type": "http://w3id.org/meta-share/meta-share/commercialSupport",
            "lr_subclass": {
                'lr_type': 'Corpus',
                'corpus_subclass': 'http://w3id.org/meta-share/meta-share/annotatedCorpus',
                'corpus_media_part': [{
                    "corpus_media_type": 'CorpusTextPart',
                    'media_type': 'http://w3id.org/meta-share/meta-share/text',
                    "linguality_type": 'http://w3id.org/meta-share/meta-share/bilingual',
                    "multilinguality_type": 'http://w3id.org/meta-share/meta-share/comparable',
                    "multilinguality_type_details": None,
                    "language": [
                        {
                            "language_tag": "en",
                            "language_id": "en",
                            "script_id": None,
                            "region_id": None,
                            "variant_id": None,
                            'language_variety_name': {
                                'en': 'variety name'
                            }
                        }
                    ],
                    "modality_type": [],
                    "text_type": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_type_identifier': {
                            'text_type_classification_scheme': 'http://w3id.org/meta-share/meta-share/BNC_textTypeClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "text_genre": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_genre_identifier': {
                            'text_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/ANC_genreClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "synthetic_data": False,
                    "creation_details": None,
                    "link_to_other_media": [{
                        'other_media': [
                            'http://w3id.org/meta-share/meta-share/image'],
                        'media_type_details': {
                            'en': 'details'
                        },
                        'synchronized_with_text': True,
                        'synchronized_with_audio': True,
                        'synchronized_with_video': True,
                        'synchronized_with_image': True,
                        'synchronized_with_text_numerical': True
                    }]
                }],
                'dataset_distribution': [{
                    'dataset_distribution_form': 'http://w3id.org/meta-share/meta-share/downloadable',
                    'download_location': 'http://downloadfromhere.com',
                    'access_location': 'http://downloadfromhere.com',
                    'is_accessed_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'is_displayed_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'is_queried_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'samples_location': ['http://downloadfromhere.com'],
                    'distribution_text_feature': [{
                        'size': [{
                            'amount': 10.0,
                            'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                        }],
                        'data_format': [
                            'http://w3id.org/meta-share/omtd-share/Json'],
                        'character_encoding': [
                            'http://w3id.org/meta-share/meta-share/Big5']
                    }],
                    'distribution_audio_feature': [],
                    'distribution_image_feature': [],
                    'distribution_video_feature': [],
                    'licence_terms': [{
                        'licence_terms_name': {
                            'en': 'term_name'
                        },
                        'licence_terms_url': ['http://www.termurl.com'],
                        "condition_of_use": [
                            "http://w3id.org/meta-share/meta-share/unspecified"
                        ],
                        'licence_identifier': [{
                            "licence_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                            "value": "registry_value"
                        }]
                    }],
                    'attribution_text': '',
                    'cost': {
                        'amount': 10.0,
                        'currency': 'http://w3id.org/meta-share/meta-share/euro',
                    },
                    'membership_institution': [],
                    'copyright_statement': '',
                    'availability_start_date': '2020-01-01',
                    'availability_end_date': '2020-01-01',
                    'distribution_rights_holder': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "registry_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }]
                }],
                'personal_data_included': 'http://w3id.org/meta-share/meta-share/yesP',
                'personal_data_details': '',
                'sensitive_data_included': 'http://w3id.org/meta-share/meta-share/yesS',
                'sensitive_data_details': '',
                'anonymized': 'http://w3id.org/meta-share/meta-share/yesA',
                'anonymization_details': '',
                'is_analysed_by': [{
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                }],
                'is_edited_by': [{
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                }],
                'is_elicited_by': [],
                'is_annotated_version_of': {
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                },
                'is_aligned_version_of': {
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                },
                'is_converted_version_of': [],
                'time_coverage': [],
                'geographic_coverage': [],
                'register': [{
                    'en': 'register'
                }],
                'user_query': '',
                'annotation': [{
                    'annotation_type': [
                        'http://w3id.org/meta-share/omtd-share/StructuralAnnotationType'],
                    'annotated_element': [],
                    'segmentation_level': [],
                    'annotation_standoff': True,
                    'guidelines': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                                'value': "registry_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                    "typesystem": [
                        {
                            "resource_name": {
                                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                            },
                            "lr_identifier": [
                            ],
                            "version": "1.0.0 (automatically assigned)"
                        }
                    ],
                    "annotation_resource": [
                        {
                            "resource_name": {
                                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                            },
                            "lr_identifier": [
                            ],
                            "version": "1.0.0 (automatically assigned)"
                        }
                    ],
                    'theoretic_model': {
                        'en': 'model'
                    },
                    'annotation_mode': 'http://w3id.org/meta-share/meta-share/automatic',
                    'annotation_mode_details': {
                        'en': 'details'
                    },
                    'is_annotated_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'annotator': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "registry_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }],
                    'annotation_start_date': '2020-01-01',
                    'annotation_end_date': '2020-02-02',
                    'interannotator_agreement': None,
                    'intraannotator_agreement': None,
                    'annotation_report': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                                'value': "registry_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                }]
            }
        }
        serializer = r_s.LanguageResourceSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())
        self.assertTrue(instance)

    def test_from_xml_representation(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = data['ms:MetadataRecord']['ms:DescribedEntity'][
            'ms:LanguageResource']
        from_xml = r_s.LanguageResourceSerializer().from_xml_representation(
            xml_data=xml_data)
        self.assertTrue('resource_name' in tuple(from_xml.keys()))
        self.assertEquals(from_xml['intended_application'],
                          ['http://w3id.org/meta-share/omtd-share/Translation'])
        serializer = r_s.LanguageResourceSerializer(data=from_xml)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)

        self.assertTrue(serializer.is_valid())

    def test_from_xml_representation_fails_with_bad_data(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = data['ms:MetadataRecord']['ms:DescribedEntity'][
            'ms:LanguageResource']
        xml_data_fail = OrderedDict(
            [('resourceName', v) if k == 'ms:resourceName' else (k, v) for k, v
             in xml_data.items()])
        from_xml_fail = r_s.LanguageResourceSerializer().from_xml_representation(
            xml_data=xml_data_fail)
        self.assertEquals(from_xml_fail['resource_name'], None)
        serializer = r_s.LanguageResourceSerializer(data=from_xml_fail)
        self.assertFalse(serializer.is_valid())

    def test_lr_serializer_to_display_representation_works(self):
        dis_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(dis_repr['intended_application'] == {
            'field_label': {'en': 'Intended application'},
            'field_value': [{
                'value': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
                'label': {
                    'en': 'Machine Translation'}},
                {
                    'value': 'http://w3id.org/meta-share/omtd-share/LanguageTechnology',
                    'label': {
                        'en': 'Language Technology'}},
                {
                    'value': 'http://w3id.org/meta-share/omtd-share/SpeechRecognition',
                    'label': {'en': 'Speech Recognition'}},
                {
                    'value': "Free text describing project's lt_area"}]
        })

    def test_lr_serializer_to_display_representation_works_is_exact_match_with(self):
        md_json_file = open('registry/tests/fixtures/corpus.json', 'r')
        md_json_str = md_json_file.read()
        md_json_file.close()
        md_json_data = json.loads(md_json_str)
        f_c_data = copy.deepcopy(md_json_data)
        f_c_data['described_entity']['version'] = '2.0'
        f_c_data['described_entity']['is_exact_match_with'] = []
        f_c_data['described_entity']['lr_identifier'] = []
        intera_data = copy.deepcopy(md_json_data)
        intera_data['described_entity']['resource_name'] = {'en': 'INTERA Corpus - the English structurally annotated part of the BG-EN pair'}
        intera_data['described_entity']['resource_short_name'] = {'en': 'intera sintera short name'}
        intera_data['described_entity']['version'] = '6.0'
        intera_data['described_entity']['is_exact_match_with'] = []
        intera_data['described_entity']['lr_identifier'] = [{
            "lr_identifier_scheme": "http://purl.org/spar/datacite/handle",
            "value": "http://hdl.grnet.gr/11500/AasdadsaTHENA-000sdas0-0asdasda000-23F2-7c"
        }]
        f_c_serializer = r_s.MetadataRecordSerializer(data=f_c_data)
        if f_c_serializer.is_valid():
            f_c_instance = f_c_serializer.save()
        else:
            LOGGER.debug(f_c_serializer.errors)
        intera_serializer = r_s.MetadataRecordSerializer(data=intera_data)
        if intera_serializer.is_valid():
            intera_instance = intera_serializer.save()
        else:
            LOGGER.debug(intera_serializer.errors)
        f_c_instance.described_entity.is_exact_match_with.add(intera_instance.described_entity)
        f_c_instance.management_object.publish(force=True)
        f_c_instance = MetadataRecord.objects.get(pk=f_c_instance.pk)
        intera_instance.described_entity.is_exact_match_with.add(f_c_instance.described_entity)
        intera_instance.management_object.publish(force=True)
        intera_instance = MetadataRecord.objects.get(pk=intera_instance.pk)
        f_c_repr = f_c_serializer.to_display_representation(f_c_instance)
        intera_repr = intera_serializer.to_display_representation(intera_instance)
        f_c_exact = None
        intera_exact = None
        for i in f_c_repr['described_entity']['field_value']['reverse_relations']['field_value']:
            if i['field_label']['en'] == 'Exact match with':
                f_c_exact = i['field_value']
        for i in intera_repr['described_entity']['field_value']['reverse_relations']['field_value']:
            if i['field_label']['en'] == 'Exact match with':
                intera_exact = i['field_value']
        self.assertTrue(len(f_c_exact) == len(intera_exact) == 1)
        self.assertFalse(f_c_repr['described_entity']['field_value'].get('is_exact_match_with'))
        self.assertFalse(intera_repr['described_entity']['field_value'].get('is_exact_match_with'))

    def test_lr_serializer_to_display_representation_works_is_potential_duplicate_with(self):
        md_json_file = open('registry/tests/fixtures/corpus.json', 'r')
        md_json_str = md_json_file.read()
        md_json_file.close()
        md_json_data = json.loads(md_json_str)
        f_c_data = copy.deepcopy(md_json_data)
        f_c_data['described_entity']['version'] = '2.0'
        f_c_data['described_entity']['is_potential_duplicate_with'] = []
        f_c_data['described_entity']['lr_identifier'] = []
        intera_data = copy.deepcopy(md_json_data)
        intera_data['described_entity']['resource_name'] = {
            'en': 'INTERA Corpus - the English structurally annotated part of the BG-EN pair'}
        intera_data['described_entity']['resource_short_name'] = {'en': 'intera sintera short name'}
        intera_data['described_entity']['version'] = '6.0'
        intera_data['described_entity']['is_potential_duplicate_with'] = []
        intera_data['described_entity']['lr_identifier'] = [{
            "lr_identifier_scheme": "http://purl.org/spar/datacite/handle",
            "value": "http://hdl.grnet.gr/11500/AasdadsaTHENA-000sdas0-0asdasda000-23F2-7c"
        }]
        f_c_serializer = r_s.MetadataRecordSerializer(data=f_c_data)
        if f_c_serializer.is_valid():
            f_c_instance = f_c_serializer.save()
        else:
            LOGGER.debug(f_c_serializer.errors)
        intera_serializer = r_s.MetadataRecordSerializer(data=intera_data)
        if intera_serializer.is_valid():
            intera_instance = intera_serializer.save()
        else:
            LOGGER.debug(intera_serializer.errors)
        f_c_instance.described_entity.is_potential_duplicate_with.add(intera_instance.described_entity)
        f_c_instance.management_object.publish(force=True)
        f_c_instance = MetadataRecord.objects.get(pk=f_c_instance.pk)
        intera_instance.described_entity.is_potential_duplicate_with.add(f_c_instance.described_entity)
        intera_instance.management_object.publish(force=True)
        intera_instance = MetadataRecord.objects.get(pk=intera_instance.pk)
        f_c_repr = f_c_serializer.to_display_representation(f_c_instance)
        intera_repr = intera_serializer.to_display_representation(intera_instance)
        f_c_exact = None
        intera_exact = None
        for i in f_c_repr['described_entity']['field_value']['reverse_relations']['field_value']:
            if i['field_label']['en'] == 'Potential duplicate with':
                f_c_exact = i['field_value']
        for i in intera_repr['described_entity']['field_value']['reverse_relations']['field_value']:
            if i['field_label']['en'] == 'Potential duplicate with':
                intera_exact = i['field_value']
        self.assertTrue(len(f_c_exact) == len(intera_exact) == 1)
        self.assertFalse(f_c_repr['described_entity']['field_value'].get('is_potential_duplicate_with'))
        self.assertFalse(intera_repr['described_entity']['field_value'].get('is_potential_duplicate_with'))

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.LanguageResourceSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.LRIdentifierSerializer.Meta.model.schema_fields[
                'lr_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.LanguageResourceSerializer.Meta.model.schema_fields['lr_identifier']]
        ]))

    def test_to_xml_representation_lt_class(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['intended_application'] = [
            choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT,
            'lt class non CV value'
        ]
        serializer = r_s.LanguageResourceSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        ltclass_xml_field_name = r_s.LanguageResourceSerializer.Meta.model.schema_fields['intended_application']
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].keys()), ['ms:LTClassRecommended'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].values()),
                          [choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].keys()), ['ms:LTClassOther'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].values()),
                          ['lt class non CV value'])

    def test_to_display_representation_functional_model_ml_model_with_functional_service(self):
        # Create "functional" Model
        json_file_model = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str_model = json_file_model.read()
        json_file_model.close()
        json_data_model = json.loads(json_str_model)
        json_data_model['described_entity']['resource_name']['en'] = 'Functional model'
        serializer_model = r_s.MetadataRecordSerializer(data=json_data_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Create functional service and make it point to model
        json_data_tool = copy.deepcopy(self.json_data)
        json_data_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_data_tool['described_entity']['lr_subclass']['ml_model'] = [
            r_s.GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = r_s.MetadataRecordSerializer(data=json_data_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Emmulate create view functionality
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.save()

        # Get Display Representation of model
        display_serialization = r_s.MetadataRecordSerializer(instance=instance_model,
                                                             context={'uncensored': True}).to_display_representation()

        # Check that relation is_ml_model_of_functional relation exists
        self.assertTrue('is_ml_model_of_functional' in display_serialization['described_entity']['field_value'].keys())

        # Check that relation is_ml_model_of_functional relation contains something in display serialization structure
        self.assertTrue(display_serialization['described_entity']['field_value']['is_ml_model_of_functional'] != None)
        self.assertTrue('field_value' in display_serialization['described_entity']['field_value']['is_ml_model_of_functional'].keys())

        # Check that is_ml_model_of_functional relation contains only the declared functional Tool
        self.assertEquals(len(display_serialization['described_entity']['field_value']['is_ml_model_of_functional']['field_value']), 1)
        self.assertEquals(
            display_serialization['described_entity']['field_value']['is_ml_model_of_functional']['field_value'][0]['resource_name']['field_value'],
            {'en': 'Functional tool'}
        )

    def test_to_display_representation_functional_model_ml_model_with_non_functional_service(self):
        # Create "functional" Model
        json_file_model = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str_model = json_file_model.read()
        json_file_model.close()
        json_data_model = json.loads(json_str_model)
        json_data_model['described_entity']['resource_name']['en'] = 'Functional model'
        serializer_model = r_s.MetadataRecordSerializer(data=json_data_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Create a non functional service and make it point to model
        json_data_tool = copy.deepcopy(self.json_data)
        json_data_tool['described_entity']['resource_name']['en'] = 'Non Functional tool'
        json_data_tool['described_entity']['lr_subclass']['ml_model'] = [
            r_s.GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = r_s.MetadataRecordSerializer(data=json_data_tool)
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()

        # Get Display Representation of model
        display_serialization = r_s.MetadataRecordSerializer(instance=instance_model,
                                                             context={'uncensored': True}).to_display_representation()

        # Check that relation is_ml_model_of_functional relation does not exist
        self.assertTrue('is_ml_model_of_functional' not in display_serialization['described_entity']['field_value'].keys())

        # Check that Reverse Relations contains exactly one
        reverse_relations = display_serialization['described_entity']['field_value']['reverse_relations']['field_value']
        self.assertEquals(len(reverse_relations), 4)

        # Check that the contained relation is isMLModelFor
        self.assertEquals(reverse_relations[0]['field_label'], {'en': 'ML model for'})

        # Check that isMLModelFor contains only the non functional Tool
        self.assertEquals(len(reverse_relations[0]['field_value']), 1)
        self.assertEquals(reverse_relations[0]['field_value'][0]['resource_name']['field_value'], {'en': 'Non Functional tool'})

    def test_to_display_representation_functional_model_ml_model_with_isMLModelOf_direct_functional_tool(self):
        # Create a functional service to be used in isMLModelFor of model
        json_data_tool_direct = copy.deepcopy(self.json_data)
        json_data_tool_direct['described_entity']['resource_name']['en'] = 'Functional tool direct'
        serializer_tool_direct = r_s.MetadataRecordSerializer(data=json_data_tool_direct, context={'functional_service': True})
        self.assertTrue(serializer_tool_direct.is_valid())
        instance_tool_direct = serializer_tool_direct.save()
        instance_tool_direct.management_object.functional_service = True
        instance_tool_direct.management_object.save()

        # Create "functional" Model
        json_file_model = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str_model = json_file_model.read()
        json_file_model.close()
        json_data_model = json.loads(json_str_model)
        json_data_model['described_entity']['resource_name']['en'] = 'Functional model'
        json_data_model['described_entity']['is_ml_model_of'] = [
            r_s.GenericLanguageResourceSerializer(instance=instance_tool_direct.described_entity).data
        ]
        serializer_model = r_s.MetadataRecordSerializer(data=json_data_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Create functional service and make it point to model
        json_data_tool_fun = copy.deepcopy(self.json_data)
        json_data_tool_fun['described_entity']['resource_name']['en'] = 'Functional tool indirect'
        # Remove id and change short name to avoid duplication errors
        json_data_tool_fun['described_entity']['lr_identifier'] = []
        json_data_tool_fun['described_entity']['resource_short_name']['en'] = 'Functional model short name'
        # Point to model
        json_data_tool_fun['described_entity']['lr_subclass']['ml_model'] = [
            r_s.GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool_fun = r_s.MetadataRecordSerializer(data=json_data_tool_fun, context={'functional_service': True})
        self.assertTrue(serializer_tool_fun.is_valid())
        instance_tool = serializer_tool_fun.save()
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.save()

        # Get Display Representation of model
        display_serialization = r_s.MetadataRecordSerializer(instance=instance_model,
                                                             context={'uncensored': True}).to_display_representation()

        # Check that relation is_ml_model_of_functional relation exists
        self.assertTrue('is_ml_model_of_functional' in display_serialization['described_entity']['field_value'].keys())

        # Check that relation is_ml_model_of_functional relation contains something in display serialization structure
        self.assertTrue(display_serialization['described_entity']['field_value']['is_ml_model_of_functional'] != None)
        self.assertTrue('field_value' in display_serialization['described_entity']['field_value'][
            'is_ml_model_of_functional'].keys())

        # Check that is_ml_model_of_functional relation contains only the declared functional Tool
        self.assertEquals(
            len(display_serialization['described_entity']['field_value']['is_ml_model_of_functional']['field_value']),
            1)
        self.assertEquals(
            display_serialization['described_entity']['field_value']['is_ml_model_of_functional']['field_value'][0][
                'resource_name']['field_value'],
            {'en': 'Functional tool indirect'}
        )

        # Check that Reverse Relations contains exactly one
        reverse_relations = display_serialization['described_entity']['field_value']['reverse_relations']['field_value']
        self.assertEquals(len(reverse_relations), 4)

        # Check that the contained relation is isMLModelFor
        self.assertEquals(reverse_relations[0]['field_label'], {'en': 'ML model for'})

        # Check that isMLModelFor contains only the functional Tool
        self.assertEquals(len(reverse_relations[0]['field_value']), 1)
        self.assertEquals(reverse_relations[0]['field_value'][0]['resource_name']['field_value'],
                          {'en': 'Functional tool direct'})

    def test_to_display_representation_model_only_isMLModelOf_direct_functional_tool(self):
        # This is not a functional model, aka contains no is_ml_model_for_functional relation
        # Create a functional service to be used in isMLModelFor of model
        json_data_tool_direct = copy.deepcopy(self.json_data)
        json_data_tool_direct['described_entity']['resource_name']['en'] = 'Functional tool direct'
        serializer_tool_direct = r_s.MetadataRecordSerializer(data=json_data_tool_direct, context={'functional_service': True})
        self.assertTrue(serializer_tool_direct.is_valid())
        instance_tool_direct = serializer_tool_direct.save()
        instance_tool_direct.management_object.functional_service = True
        instance_tool_direct.management_object.save()

        # Create "functional" Model
        json_file_model = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str_model = json_file_model.read()
        json_file_model.close()
        json_data_model = json.loads(json_str_model)
        json_data_model['described_entity']['resource_name']['en'] = 'Functional model'
        json_data_model['described_entity']['is_ml_model_of'] = [
            r_s.GenericLanguageResourceSerializer(instance=instance_tool_direct.described_entity).data
        ]
        serializer_model = r_s.MetadataRecordSerializer(data=json_data_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Get Display Representation of model
        display_serialization = r_s.MetadataRecordSerializer(instance=instance_model,
                                                             context={'uncensored': True}).to_display_representation()

        # Check that relation is_ml_model_of_functional relation does not exist
        self.assertTrue('is_ml_model_of_functional' not in display_serialization['described_entity']['field_value'].keys())

        # Check that Reverse Relations contains exactly one
        reverse_relations = display_serialization['described_entity']['field_value']['reverse_relations']['field_value']
        self.assertEquals(len(reverse_relations), 4)

        # Check that the contained relation is isMLModelFor
        self.assertEquals(reverse_relations[0]['field_label'], {'en': 'ML model for'})

        # Check that isMLModelFor contains only the functional Tool
        self.assertEquals(len(reverse_relations[0]['field_value']), 1)
        self.assertEquals(reverse_relations[0]['field_value'][0]['resource_name']['field_value'],
                          {'en': 'Functional tool direct'})

    def test_to_display_representation_functional_model_ml_model_with_isMLModelOf_direct_nonfunctional_tool(self):
        # Create a non functional service to be used in isMLModelFor of model
        json_data_tool = copy.deepcopy(self.json_data)
        json_data_tool['described_entity']['resource_name']['en'] = 'Non Functional tool direct'
        serializer_tool = r_s.MetadataRecordSerializer(data=json_data_tool)
        self.assertTrue(serializer_tool.is_valid())
        instance_tool_non = serializer_tool.save()

        # Create "functional" Model
        json_file_model = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str_model = json_file_model.read()
        json_file_model.close()
        json_data_model = json.loads(json_str_model)
        json_data_model['described_entity']['resource_name']['en'] = 'Functional model'
        json_data_model['described_entity']['is_ml_model_of'] = [
            r_s.GenericLanguageResourceSerializer(instance=instance_tool_non.described_entity).data
        ]
        serializer_model = r_s.MetadataRecordSerializer(data=json_data_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Create functional service and make it point to model
        json_data_tool_fun = copy.deepcopy(self.json_data)
        json_data_tool_fun['described_entity']['resource_name']['en'] = 'Functional tool direct'
        # Remove id and change short name to avoid duplication errors
        json_data_tool_fun['described_entity']['lr_identifier'] = []
        json_data_tool_fun['described_entity']['resource_short_name']['en'] = 'Functional model short name'
        # Point to model
        json_data_tool_fun['described_entity']['lr_subclass']['ml_model'] = [
            r_s.GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool_fun = r_s.MetadataRecordSerializer(data=json_data_tool_fun, context={'functional_service': True})
        self.assertTrue(serializer_tool_fun.is_valid())
        instance_tool = serializer_tool_fun.save()
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.save()

        # Get Display Representation of model
        display_serialization = r_s.MetadataRecordSerializer(instance=instance_model,
                                                             context={'uncensored': True}).to_display_representation()

        # Check that relation is_ml_model_of_functional relation exists
        self.assertTrue('is_ml_model_of_functional' in display_serialization['described_entity']['field_value'].keys())

        # Check that relation is_ml_model_of_functional relation contains something in display serialization structure
        self.assertTrue(display_serialization['described_entity']['field_value']['is_ml_model_of_functional'] != None)
        self.assertTrue('field_value' in display_serialization['described_entity']['field_value'][
        'is_ml_model_of_functional'].keys())

        # Check that is_ml_model_of_functional relation contains only the declared functional Tool
        self.assertEquals(
        len(display_serialization['described_entity']['field_value']['is_ml_model_of_functional']['field_value']),
        1)
        self.assertEquals(
            display_serialization['described_entity']['field_value']['is_ml_model_of_functional']['field_value'][0][
                'resource_name']['field_value'],
                {'en': 'Functional tool direct'}
        )

        # Check that Reverse Relations contains exactly one
        reverse_relations = display_serialization['described_entity']['field_value']['reverse_relations']['field_value']
        self.assertEquals(len(reverse_relations), 4)

        # Check that the contained relation is isMLModelFor
        self.assertEquals(reverse_relations[0]['field_label'], {'en': 'ML model for'})

        # Check that isMLModelFor contains only the non functional Tool
        self.assertEquals(len(reverse_relations[0]['field_value']), 1)
        self.assertEquals(reverse_relations[0]['field_value'][0]['resource_name']['field_value'],
        {'en': 'Non Functional tool direct'})

    def test_to_display_representation_functional_model_ml_model_with_isMLModelOf_direct_data_lr(self):
        # Create a lr to be used in isMLModelFor of model
        json_file_corpus = open('registry/tests/fixtures/corpus.json', 'r')
        json_str_corpus = json_file_corpus.read()
        json_file_corpus.close()
        json_data_corpus = json.loads(json_str_corpus)
        json_data_corpus['described_entity']['resource_name']['en'] = 'Corpus in isMLModelOf'
        serializer_corpus= r_s.MetadataRecordSerializer(data=json_data_corpus)
        self.assertTrue(serializer_corpus.is_valid())
        instance_corpus = serializer_corpus.save()

        # Create "functional" Model
        json_file_model = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str_model = json_file_model.read()
        json_file_model.close()
        json_data_model = json.loads(json_str_model)
        json_data_model['described_entity']['resource_name']['en'] = 'Functional model'
        json_data_model['described_entity']['is_ml_model_of'] = [
            r_s.GenericLanguageResourceSerializer(instance=instance_corpus.described_entity).data
        ]
        serializer_model = r_s.MetadataRecordSerializer(data=json_data_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Create functional service and make it point to model
        json_data_tool_fun = copy.deepcopy(self.json_data)
        json_data_tool_fun['described_entity']['resource_name']['en'] = 'Functional tool'
        # Point to model
        json_data_tool_fun['described_entity']['lr_subclass']['ml_model'] = [
            r_s.GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool_fun = r_s.MetadataRecordSerializer(data=json_data_tool_fun,
                                                           context={'functional_service': True})
        self.assertTrue(serializer_tool_fun.is_valid())
        instance_tool = serializer_tool_fun.save()
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.save()

        # Get Display Representation of model
        display_serialization = r_s.MetadataRecordSerializer(instance=instance_model,
                                                             context={'uncensored': True}).to_display_representation()

        # Check that relation is_ml_model_of_functional relation exists
        self.assertTrue('is_ml_model_of_functional' in display_serialization['described_entity']['field_value'].keys())

        # Check that relation is_ml_model_of_functional relation contains something in display serialization structure
        self.assertTrue(display_serialization['described_entity']['field_value']['is_ml_model_of_functional'] != None)
        self.assertTrue('field_value' in display_serialization['described_entity']['field_value'][
            'is_ml_model_of_functional'].keys())

        # Check that is_ml_model_of_functional relation contains only the declared functional Tool
        self.assertEquals(
            len(display_serialization['described_entity']['field_value']['is_ml_model_of_functional']['field_value']),
            1)
        self.assertEquals(
            display_serialization['described_entity']['field_value']['is_ml_model_of_functional']['field_value'][0][
                'resource_name']['field_value'],
            {'en': 'Functional tool'}
        )

        # Check that Reverse Relations contains exactly one
        reverse_relations = display_serialization['described_entity']['field_value']['reverse_relations']['field_value']
        self.assertEquals(len(reverse_relations), 4)

        # Check that the contained relation is isMLModelFor
        self.assertEquals(reverse_relations[0]['field_label'], {'en': 'ML model for'})

        # Check that isMLModelFor contains only the non functional Tool
        self.assertEquals(len(reverse_relations[0]['field_value']), 1)
        self.assertEquals(reverse_relations[0]['field_value'][0]['resource_name']['field_value'],
                          {'en': 'Corpus in isMLModelOf'})


class TestAccessRightsStatementIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.ari = {
            'access_rights_statement_scheme': 'http://w3id.org/meta-share/meta-share/COARightsStatementScheme',
            'value': "doi_value_id",
        }

        cls.serializer = r_s.AccessRightsStatementIdentifierSerializer(
            data=cls.ari)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.ari.keys():
            if i not in r_s.AccessRightsStatementIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_li_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.AccessRightsStatementIdentifierSerializer.Meta.fields)

    def test_ari_serializer_fails_with_bad_data(self):
        data_fail = {
            'access_rights_statement_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.AccessRightsStatementIdentifierSerializer(
            self.instance, data=data_fail)
        self.assertFalse(serializer.is_valid())


class TestAccessRightsStatementSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['access_rights_statement']
        cls.serializer = r_s.AccessRightsStatementSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.AccessRightsStatementSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_access_rights_statement_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.AccessRightsStatementSerializer.Meta.fields)

    def test_access_rights_statement_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['access_rights_statement_identifier'][
            'access_rights_statement_scheme'] = 'fail'
        serializer = r_s.AccessRightsStatementSerializer(self.instance,
                                                         data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_without_label(self):
        no_label = copy.deepcopy(self.raw_data)
        no_label['category_label'] = None
        serializer = r_s.AccessRightsStatementSerializer(data=no_label)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_without_identifier(self):
        no_id = copy.deepcopy(self.raw_data)
        no_id['access_rights_statement_identifier'] = None
        serializer = r_s.AccessRightsStatementSerializer(data=no_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertTrue(serializer.is_valid())

    def test_cannot_retrieve_instance_with_blank_id(self):
        blank_id = copy.deepcopy(self.raw_data)
        blank_id['access_rights_statement_identifier'] = ''
        serializer = r_s.AccessRightsStatementSerializer(data=blank_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())

    def test_cannot_retrieve_instance_with_empty_id(self):
        empty_id = copy.deepcopy(self.raw_data)
        empty_id['access_rights_statement_identifier'] = {
            'access_rights_statement_scheme': '',
            'value': ''
        }
        serializer = r_s.AccessRightsStatementSerializer(data=empty_id)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertFalse(serializer.is_valid())


class TestLicenceTermsSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.LicenceTermsSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.LicenceTermsSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_licence_terms_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.LicenceTermsSerializer.Meta.fields)

    def test_licence_terms_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['licence_identifier'] = [
            {
                "licence_identifier_scheme": 'http://w3id.org/meta-share/meta-share/SPDX',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "licence_identifier_scheme": 'http://w3id.org/meta-share/meta-share/SPDX',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.LicenceTermsSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_licence_terms_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['licence_identifier'][0]['licence_identifier_scheme'] = 'fail'
        serializer = r_s.LicenceTermsSerializer(self.instance,
                                                data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_licence_terms_serializer_fails_with_no_licence_terms_url(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['licence_terms_url'] = []
        serializer = r_s.LicenceTermsSerializer(self.instance,
                                                data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_licence_terms_serializer_fails_with_no_condition_of_use(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['condition_of_use'] = []
        serializer = r_s.LicenceTermsSerializer(self.instance,
                                                data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_retrieve_instance_without_li(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['licence_identifier'] = []
        serializer = r_s.LicenceTermsSerializer(self.instance,
                                                data=raw_data)
        self.assertTrue(serializer.is_valid())

    def test_licence_terms_serializer_fails_with_unspecified_condition_of_use_not_alone(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified",
            "http://w3id.org/meta-share/meta-share/noDerivatives"
        ]
        serializer = r_s.LicenceTermsSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.LicenceTermsSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.LicenceIdentifierSerializer.Meta.model.schema_fields[
                'licence_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.LicenceTermsSerializer.Meta.model.schema_fields['licence_identifier']]
        ]))


class TestDocumentSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/document.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.raw_data['lt_area'] = []
        cls.serializer = r_s.DocumentSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.DocumentSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_document_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.DocumentSerializer.Meta.fields)

    def test_document_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['document_identifier'] = [
            {
                "document_identifier_scheme": 'http://purl.org/spar/datacite/ark',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "document_identifier_scheme": 'http://purl.org/spar/datacite/ark',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.DocumentSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_document_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['documentation_type'] = 'fail'
        serializer = r_s.DocumentSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_di_created_and_instance_retrieved_when_none_given(self):
        no_di = copy.deepcopy(self.raw_data)
        no_di['document_identifier'] = []
        serializer = r_s.DocumentSerializer(data=no_di)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertFalse(data['document_identifier'] == [])
        self.assertTrue(instance)

    def test_from_xml_representation(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:LRSubclass']['ms:ToolService'] \
                ['ms:SoftwareDistribution']['ms:isDescribedBy']
        from_xml = r_s.DocumentSerializer().from_xml_representation(
            xml_data=xml_data)
        self.assertTrue('title' in tuple(from_xml.keys()))
        self.assertEquals(from_xml['lt_area'],
                          ['http://w3id.org/meta-share/omtd-share/Translation'])
        serializer = r_s.DocumentSerializer(data=from_xml)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())

    def test_from_xml_representation_fails_with_bad_data(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:LRSubclass']['ms:ToolService'] \
                ['ms:SoftwareDistribution']['ms:isDescribedBy']
        xml_data_fail = OrderedDict(
            [('title', v) if k == 'ms:title' else (k, v) for k, v in
             xml_data.items()])
        from_xml_fail = r_s.DocumentSerializer().from_xml_representation(
            xml_data=xml_data_fail)
        self.assertEquals(from_xml_fail['title'], None)
        serializer = r_s.DocumentSerializer(data=from_xml_fail)
        self.assertFalse(serializer.is_valid())

    def test_to_display_representation_works_without_lt(self):
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(d_repr['lt_area'] is None)

    def test_to_display_representation_works_with_lt(self):
        lt_data = {
            "entity_type": "",
            "documentation_type": 'http://w3id.org/meta-share/meta-share/apiSpecification',
            "document_identifier": [{
                'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_value_id2"
            }],
            "bibliographic_record": "",
            "document_type": 'http://w3id.org/meta-share/meta-share/article1',
            "title": {
                'en': 'Document_title'
            },
            "alternative_title": [{
                'en': 'alt title'
            }],
            "subtitle": [],
            "author": [{
                'surname': {
                    'en': 'random_surname'
                },
                'given_name': {
                    'en': 'random_given_name'
                },
                'personal_identifier': [{
                    'personal_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                    'value': "registry_value_rndm"
                }],
                'email': ['validated@email.com'],
            }],
            "editor": [],
            "contributor": [],
            "publication_date": '2020-01-01',
            "publisher": '',
            "version_type": None,
            "abstract": '',
            "journal": '',
            "book_title": None,
            "volume": "",
            "series": None,
            "pages": "",
            "conference": None,
            "lt_area": ['Translation'],
            "domain": [{
                'category_label': {
                    'en': 'label'
                },
                'domain_identifier': {
                    'domain_classification_scheme': 'http://w3id.org/meta-share/meta-share/ANC_domainClassification',
                    'value': "doi_value_id"
                }
            }],
            "subject": [{
                'category_label': {
                    'en': 'category'
                },
                'subject_identifier': {
                    'subject_classification_scheme': 'http://w3id.org/meta-share/meta-share/MeSH_classification',
                    'value': "doi_value_id"
                }
            }],
            "keyword": [
                {
                    "en": "GATE"
                },
                {
                    "en": "NER"
                },
                {
                    "en": "English"
                }
            ]
        }
        serializer = r_s.DocumentSerializer(data=lt_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        d_repr = serializer.to_display_representation(instance)
        self.assertEquals(tuple(d_repr['lt_area'].keys()),
                          tuple(['field_label', 'field_value']))
        self.raw_data['lt_area'] = []

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.DocumentSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.DocumentIdentifierSerializer.Meta.model.schema_fields[
                'document_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.DocumentSerializer.Meta.model.schema_fields['document_identifier']]
        ]))

    def test_to_xml_representation_lt_class(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = [
            choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT,
            'lt class non CV value'
        ]
        serializer = r_s.DocumentSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        ltclass_xml_field_name = r_s.DocumentSerializer.Meta.model.schema_fields['lt_area']
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].keys()), ['ms:LTClassRecommended'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].values()),
                          [choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].keys()), ['ms:LTClassOther'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].values()),
                          ['lt class non CV value'])


class TestAddressSetSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['address_set']
        cls.serializer = r_s.AddressSetSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.AddressSetSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_address_set_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.AddressSetSerializer.Meta.fields)

    def test_address_set_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['country'] = 'fail'
        serializer = r_s.AddressSetSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestSocialMediaOccupationalAccountSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.raw_data = {
            'social_media_occupational_account_type': 'http://w3id.org/meta-share/meta-share/facebook',
            'value': 'value',
        }
        cls.serializer = r_s.SocialMediaOccupationalAccountSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.SocialMediaOccupationalAccountSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_social_media_occupational_account_serializer_has_expected_fields(
            self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.SocialMediaOccupationalAccountSerializer.Meta.fields)

    def test_social_media_occupational_account_serializer_fails_with_bad_input(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['social_media_occupational_account_type'] = 'fail'
        serializer = r_s.SocialMediaOccupationalAccountSerializer(self.instance,
                                                                  data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestOrganizationSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.raw_data['lt_area'] = []
        cls.serializer = r_s.OrganizationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.OrganizationSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_organization_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.OrganizationSerializer.Meta.fields)

    def test_organization_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['organization_identifier'] = [
            {
                "organization_identifier_scheme": 'http://purl.org/spar/datacite/doi',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "organization_identifier_scheme": 'http://purl.org/spar/datacite/doi',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.OrganizationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_organization_serializer_fails_when_other_and_not_head(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['head_office_address'] = ''
        serializer = r_s.OrganizationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_organization_serializer_fails_when_other_and_head_is_none(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['head_office_address'] = None
        serializer = r_s.OrganizationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_organization_serializer_fails_when_head_and_not_other(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['other_office_address'] = []
        serializer = r_s.OrganizationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_organization_serializer_fails_when_head_blank_but_other_filled(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['head_office_address'] = ''
        serializer = r_s.OrganizationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_organization_serializer_fails_when_startup_and_not_sme_or_large(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['startup'] = True
        serializer = r_s.OrganizationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_organization_serializer_passes_when_startup_and_sme_only(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['startup'] = True
        raw_data[
            'organization_legal_status'] = 'http://w3id.org/meta-share/meta-share/sme'
        serializer = r_s.OrganizationSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_organization_serializer_fails_when_startup_and_not_sme(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['startup'] = True
        raw_data[
            'organization_legal_status'] = 'http://w3id.org/meta-share/meta-share/largeCompany'
        serializer = r_s.OrganizationSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_organization_serializer_passes_when_no_email_given(self):
        no_email_data = copy.deepcopy(self.raw_data)
        no_email_data['email'] = None
        serializer = r_s.OrganizationSerializer(data=no_email_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        self.assertEquals(tuple(data.keys()),
                          r_s.OrganizationSerializer.Meta.fields)

    def test_organization_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['organization_legal_status'] = 'fail'
        serializer = r_s.OrganizationSerializer(self.instance,
                                                data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_oi_created_and_instance_retrieved_when_none_given(self):
        no_oi = copy.deepcopy(self.raw_data)
        no_oi['organization_identifier'] = []
        serializer = r_s.OrganizationSerializer(data=no_oi)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertFalse(data['organization_identifier'] == [])
        self.assertTrue(instance)

    def test_from_xml_representation_fails_with_bad_data(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:contact']['ms:Organization']
        xml_data_fail = OrderedDict(
            [('organizationName', v) if k == 'ms:organizationName' else (k, v)
             for k, v in xml_data.items()])
        from_xml_fail = r_s.OrganizationSerializer().from_xml_representation(
            xml_data=xml_data_fail)
        self.assertEquals(from_xml_fail['organization_name'], None)
        serializer = r_s.OrganizationSerializer(data=from_xml_fail)
        self.assertFalse(serializer.is_valid())

    def test_to_display_representation_works_without_lt_area(self):
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(d_repr['lt_area'] is None)

    def test_to_display_representation_for_lt_class_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = [choices.LT_CLASS_RECOMMENDED_CHOICES.TRANSLATION, 'ltarea other']
        serializer = r_s.OrganizationSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['lt_area'],
                          {
                              'field_label': {'en': 'LT area'},
                              'field_value': [
                                  {
                                      'value': choices.LT_CLASS_RECOMMENDED_CHOICES.TRANSLATION,
                                      'label': {
                                          'en': choices.LT_CLASS_RECOMMENDED_CHOICES[
                                              choices.LT_CLASS_RECOMMENDED_CHOICES.TRANSLATION]}
                                  },
                                  {
                                      'value': 'ltarea other'
                                  }
                              ]
                          })

    def test_to_display_representation_works_when_no_email_is_given(self):
        no_email_data = copy.deepcopy(self.raw_data)
        del no_email_data['email']
        serializer = r_s.OrganizationSerializer(data=no_email_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        d_repr = serializer.to_display_representation(instance)
        self.assertEquals(tuple(d_repr['organization_name'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_to_display_representation_deletes_email(self):
        self.assertTrue(self.serializer.is_valid())
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertEquals(tuple(d_repr['organization_name'].keys()),
                          tuple(['field_label', 'field_value']))
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_validate_division_category_branch_requires_is_division_of(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.BRANCH
        del new_raw_data['is_division_of']
        serializer = r_s.OrganizationSerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_validate_division_category_department_requires_is_division_of(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.DEPARTMENT
        new_raw_data['is_division_of'] = []
        serializer = r_s.OrganizationSerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_validate_division_category_faculty_requires_is_division_of(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.FACULTY
        del new_raw_data['is_division_of']
        serializer = r_s.OrganizationSerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_validate_division_category_group_requires_is_division_of(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.GROUP
        del new_raw_data['is_division_of']
        serializer = r_s.OrganizationSerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_validate_division_category_institute_requires_is_division_of(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.INSTITUTE
        del new_raw_data['is_division_of']
        serializer = r_s.OrganizationSerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_validate_division_category_laboratory_requires_is_division_of(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.LABORATORY
        del new_raw_data['is_division_of']
        serializer = r_s.OrganizationSerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_validate_division_category_school_requires_is_division_of(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.SCHOOL
        del new_raw_data['is_division_of']
        serializer = r_s.OrganizationSerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_validate_division_category_subsidiary_requires_is_division_of(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.SUBSIDIARY
        del new_raw_data['is_division_of']
        serializer = r_s.OrganizationSerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_validate_division_category_unit_requires_is_division_of(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.UNIT1
        del new_raw_data['is_division_of']
        serializer = r_s.OrganizationSerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.OrganizationSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.OrganizationIdentifierSerializer.Meta.model.schema_fields[
                'organization_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in
            xml_representation[r_s.OrganizationSerializer.Meta.model.schema_fields['organization_identifier']]
        ]))

    def test_to_xml_representation_lt_class(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = [
            choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT,
            'lt class non CV value'
        ]
        serializer = r_s.OrganizationSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        ltclass_xml_field_name = r_s.OrganizationSerializer.Meta.model.schema_fields['lt_area']
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].keys()), ['ms:LTClassRecommended'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].values()),
                          [choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].keys()), ['ms:LTClassOther'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].values()),
                          ['lt class non CV value'])

    def test_from_xml_lt_class_function_works(self):
        xml_test_data = 'test_fixtures/xml_serializer_test.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:contact'][
            'ms:Organization']
        from_xml = r_s.OrganizationSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['lt_area'], [choices.LT_CLASS_RECOMMENDED_CHOICES.TRANSLATION, 'ltclass other'])


class TestGroupSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/group.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.raw_data['lt_area'] = []
        cls.serializer = r_s.GroupSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.GroupSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_group_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.GroupSerializer.Meta.fields)

    def test_group_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['group_identifier'] = [
            {
                "organization_identifier_scheme": 'http://purl.org/spar/datacite/doi',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "organization_identifier_scheme": 'http://purl.org/spar/datacite/doi',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.GroupSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_group_serializer_passes_when_no_email_given(self):
        no_email_data = copy.deepcopy(self.raw_data)
        no_email_data['email'] = None
        serializer = r_s.GroupSerializer(data=no_email_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        self.assertEquals(tuple(data.keys()),
                          r_s.GroupSerializer.Meta.fields)

    def test_group_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['organization_role'] = 'fail'
        serializer = r_s.GroupSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_gi_created_and_instance_retrieved_when_none_given(self):
        no_gi = copy.deepcopy(self.raw_data)
        no_gi['group_identifier'] = []
        serializer = r_s.GroupSerializer(data=no_gi)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertFalse(data['group_identifier'] == [])
        self.assertTrue(instance)

    def test_group_serializer_update(self):
        self.assertTrue(self.serializer.is_valid())
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['organization_name'] = {
            'en': 'other name'
        }
        initial_name = self.instance.organization_name
        serializer = r_s.GroupSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertFalse(instance.organization_name == initial_name)

    def test_from_xml_representation(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:contact']['ms:Group']
        from_xml = r_s.GroupSerializer().from_xml_representation(
            xml_data=xml_data)
        self.assertTrue('organization_name' in tuple(from_xml.keys()))
        self.assertEquals(from_xml['lt_area'],
                          ['http://w3id.org/meta-share/omtd-share/Translation'])
        serializer = r_s.GroupSerializer(data=from_xml)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())

    def test_from_xml_representation_fails_with_bad_data(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:contact']['ms:Group']
        xml_data_fail = OrderedDict(
            [('organizationName', v) if k == 'ms:organizationName' else (k, v)
             for k, v in xml_data.items()])
        from_xml_fail = r_s.GroupSerializer().from_xml_representation(
            xml_data=xml_data_fail)
        self.assertEquals(from_xml_fail['organization_name'], None)
        serializer = r_s.GroupSerializer(data=from_xml_fail)
        self.assertFalse(serializer.is_valid())

    def test_to_display_representation_works_without_lr(self):
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(d_repr['lt_area'] is None)
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_display_representation_works_with_lr(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = ['Translation']
        serializer = r_s.GroupSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        d_repr = serializer.to_display_representation(instance)
        self.assertEquals(tuple(d_repr['lt_area'].keys()),
                          tuple(['field_label', 'field_value']))
        rev = []
        for i in raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_display_representation_works_when_no_email_is_given(self):
        no_email_data = copy.deepcopy(self.raw_data)
        del no_email_data['email']
        serializer = r_s.GroupSerializer(data=no_email_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        d_repr = serializer.to_display_representation(instance)
        self.assertEquals(tuple(d_repr['organization_name'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_to_display_representation_deletes_email(self):
        self.assertTrue(self.serializer.is_valid())
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertEquals(tuple(d_repr['organization_name'].keys()),
                          tuple(['field_label', 'field_value']))
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.GenericGroupSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.OrganizationIdentifierSerializer.Meta.model.schema_fields[
                'organization_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.GenericGroupSerializer.Meta.model.schema_fields['group_identifier']]
        ]))

    def test_to_xml_representation_lt_class(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = [
            choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT,
            'lt class non CV value'
        ]
        serializer = r_s.GroupSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        ltclass_xml_field_name = r_s.GroupSerializer.Meta.model.schema_fields['lt_area']
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].keys()), ['ms:LTClassRecommended'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].values()),
                          [choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].keys()), ['ms:LTClassOther'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].values()),
                          ['lt class non CV value'])


class TestAffiliationSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['affiliation']
        cls.serializer = r_s.AffiliationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.AffiliationSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_affiliation_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.AffiliationSerializer.Meta.fields)

    def test_affiliation_serializer_passes_when_no_email_given(self):
        no_email_data = {
            'position': {
                'en': 'position'
            },
            'affiliated_organization': {
                'organization_name': {
                    'en': 'random_org'
                },
                'organization_identifier': [{
                    'organization_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                    'value': "doi_value_id"
                }],
                'website': ['http://www.orgwebsite.com'],
            },
            'email': None,
            'homepage': [],
            'address_set': [{
                'address': {
                    'en': 'address'
                },
                'region': {
                    'en': 'region'
                },
                'zip_code': '14671',
                'city': {
                    'en': 'Athens'
                },
                'country': 'GR'
            }],
            'telephone_number': [],
            'fax_number': [],
            'social_media_occupational_account': [{
                'social_media_occupational_account_type': 'http://w3id.org/meta-share/meta-share/facebook',
                'value': 'value',
            }],
        }
        serializer = r_s.AffiliationSerializer(data=no_email_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        self.assertEquals(tuple(data.keys()),
                          r_s.AffiliationSerializer.Meta.fields)

    def test_affiliation_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['position'] = 'fail'
        serializer = r_s.AffiliationSerializer(self.instance,
                                               data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_display_representation_works(self):
        d_repr = self.serializer.to_display_representation(self.instance)
        rev = []
        for i in self.instance.email:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_display_representation_works_when_no_email_is_given(self):
        no_email_data = {
            'position': {
                'en': 'position'
            },
            'affiliated_organization': {
                'organization_name': {
                    'en': 'random_org'
                },
                'organization_identifier': [{
                    'organization_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                    'value': "doi_value_id"
                }],
                'website': ['http://www.orgwebsite.com'],
            },
            'homepage': [],
            'address_set': [{
                'address': {
                    'en': 'address'
                },
                'region': {
                    'en': 'region'
                },
                'zip_code': '14671',
                'city': {
                    'en': 'Athens'
                },
                'country': 'GR'
            }],
            'telephone_number': [],
            'fax_number': [],
            'social_media_occupational_account': [{
                'social_media_occupational_account_type': 'http://w3id.org/meta-share/meta-share/facebook',
                'value': 'value',
            }],
        }
        serializer = r_s.AffiliationSerializer(data=no_email_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        d_repr = serializer.to_display_representation(instance)
        self.assertEquals(tuple(d_repr['affiliated_organization'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_to_display_representation_deletes_email(self):
        self.assertTrue(self.serializer.is_valid())
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertEquals(tuple(d_repr['affiliated_organization'].keys()),
                          tuple(['field_label', 'field_value']))
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)


class TestPersonSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/person.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.raw_data['lt_area'] = []
        cls.serializer = r_s.PersonSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.PersonSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_person_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.PersonSerializer.Meta.fields)

    def test_person_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['personal_identifier'] = [
            {
                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.PersonSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_person_instance_name(self):
        self.assertEquals(str(self.instance),
                          f'{self.instance.given_name["en"]} '
                          f'{self.instance.surname["en"]} ({self.instance.id})')

    def test_person_serializer_passes_when_no_email_given(self):
        no_email_data = copy.deepcopy(self.raw_data)
        no_email_data['email'] = None
        serializer = r_s.PersonSerializer(data=no_email_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        self.assertEquals(tuple(data.keys()), r_s.PersonSerializer.Meta.fields)

    def test_person_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['surname'] = 'fail'
        serializer = r_s.PersonSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_personal_identifier_created_and_instance_retrieved_when_none_given(
            self):
        no_personal = copy.deepcopy(self.raw_data)
        no_personal['personal_identifier'] = []
        serializer = r_s.PersonSerializer(data=no_personal)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(self.serializer.is_valid())
        data = serializer.data
        self.assertFalse(data['personal_identifier'] == [])
        self.assertTrue(instance)

    def test_from_xml_representation(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:contact']['ms:Person']
        from_xml = r_s.PersonSerializer().from_xml_representation(
            xml_data=xml_data)
        self.assertTrue('surname' in tuple(from_xml.keys()))
        self.assertEquals(from_xml['lt_area'],
                          ['http://w3id.org/meta-share/omtd-share/Translation'])
        serializer = r_s.PersonSerializer(data=from_xml)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())

    def test_from_xml_representation_fails_with_bad_data(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:contact']['ms:Person']
        xml_data_fail = OrderedDict(
            [('surname', v) if k == 'ms:surname' else (k, v) for k, v in
             xml_data.items()])
        from_xml_fail = r_s.PersonSerializer().from_xml_representation(
            xml_data=xml_data_fail)
        self.assertEquals(from_xml_fail['surname'], None)
        serializer = r_s.PersonSerializer(data=from_xml_fail)
        self.assertFalse(serializer.is_valid())

    def test_to_display_representation_works_without_lr(self):
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(d_repr['lt_area'] is None)
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_display_representation_works_with_lr(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = ['Translation']
        serializer = r_s.PersonSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        d_repr = serializer.to_display_representation(instance)
        self.assertEquals(tuple(d_repr['lt_area'].keys()),
                          tuple(['field_label', 'field_value']))
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_display_representation_works_when_no_email_is_given(self):
        no_email_data = copy.deepcopy(self.raw_data)
        del no_email_data['email']
        serializer = r_s.PersonSerializer(data=no_email_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        d_repr = serializer.to_display_representation(instance)
        self.assertEquals(tuple(d_repr['personal_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_to_display_representation_deletes_email(self):
        self.assertTrue(self.serializer.is_valid())
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertEquals(tuple(d_repr['personal_identifier'].keys()),
                          tuple(['field_label', 'field_value']))
        rev = []
        for i in self.raw_data['email']:
            rev.append(i[::-1])
        self.assertEquals(d_repr['email']['field_value'], rev)

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.PersonSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.PersonalIdentifierSerializer.Meta.model.schema_fields[
                'personal_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.PersonSerializer.Meta.model.schema_fields['personal_identifier']]
        ]))

    def test_to_xml_representation_lt_class(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = [
            choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT,
            'lt class non CV value'
        ]
        serializer = r_s.PersonSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        ltclass_xml_field_name = r_s.PersonSerializer.Meta.model.schema_fields['lt_area']
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].keys()), ['ms:LTClassRecommended'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].values()),
                          [choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].keys()), ['ms:LTClassOther'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].values()),
                          ['lt class non CV value'])

    def test_to_xml_representation_no_email(self):
        xml_representation = r_s.PersonSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(r_s.PersonSerializer.Meta.model.schema_fields['email'] not in xml_representation)

    def test_person_serializer_fails_with_name_no_for_info_context(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['name'] = {
            'en': 'Mary Smith',
        }
        serializer = r_s.PersonSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_person_serializer_fails_with_name_for_info_context_false(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['name'] = {
            'en': 'Mary Smith',
        }
        context = {
            'for_information_only': False
        }
        serializer = r_s.PersonSerializer(data=raw_data, context=context)
        self.assertFalse(serializer.is_valid())

    def test_person_serializer_success_with_name_for_info(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['name'] = {
            'en': 'Mary Smith',
        }
        context = {
            'for_information_only': True
        }
        serializer = r_s.PersonSerializer(data=raw_data, context=context)
        self.assertTrue(serializer.is_valid())


class TestProjectSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.raw_data['lt_area'] = []
        cls.serializer = r_s.ProjectSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.ProjectSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_project_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()), r_s.ProjectSerializer.Meta.fields)

    def test_project_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['project_identifier'] = [
            {
                "project_identifier_scheme": 'http://w3id.org/meta-share/meta-share/OpenAIRE',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "project_identifier_scheme": 'http://w3id.org/meta-share/meta-share/OpenAIRE',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.ProjectSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_generic_proj_serializer_name_strip(
            self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['project_name'] = {'en': ' strip name please '}
        raw_data['project_short_name'] = {'en': ' strip '}
        raw_data['website'] = []
        raw_data['project_identifier'] = []
        raw_data['grant_number'] = ' 12345 '
        serializer = r_s.ProjectSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        proj = Project.objects.get(id=instance.pk)
        self.assertFalse(instance.project_name['en'] == raw_data['project_name']['en'])
        self.assertFalse(instance.grant_number == raw_data['grant_number'])

    def test_project_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['project_name'] = 'fail'
        serializer = r_s.ProjectSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_pri_created_and_instance_retrieved_when_none_given(self):
        no_pri = copy.deepcopy(self.raw_data)
        no_pri['project_identifier'] = []
        serializer = r_s.ProjectSerializer(data=no_pri)
        if serializer.is_valid():
            instance = serializer.save()
        data = serializer.data
        self.assertFalse(data['project_identifier'] == [])
        self.assertTrue(instance)

    def test_from_xml_representation(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:actualUse']['ms:usageProject']
        from_xml = r_s.ProjectSerializer().from_xml_representation(
            xml_data=xml_data)
        self.assertTrue('lt_area' in tuple(from_xml.keys()))
        self.assertEquals(from_xml['lt_area'],
                          ['http://w3id.org/meta-share/omtd-share/Translation'])
        serializer = r_s.ProjectSerializer(data=from_xml)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertTrue(serializer.is_valid())

    def test_from_xml_representation_fails_with_bad_data(self):
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            data['ms:MetadataRecord']['ms:DescribedEntity'][
                'ms:LanguageResource']['ms:actualUse']['ms:usageProject']
        xml_data_fail = OrderedDict(
            [('projectName', v) if k == 'ms:projectName' else (k, v) for k, v in
             xml_data.items()])
        from_xml_fail = r_s.ProjectSerializer().from_xml_representation(
            xml_data=xml_data_fail)
        self.assertEquals(from_xml_fail['project_name'], None)
        serializer = r_s.ProjectSerializer(data=from_xml_fail)
        self.assertFalse(serializer.is_valid())

    def test_to_display_representation_works_without_lt(self):
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(d_repr['lt_area'] is None)
        self.assertFalse('email' not in d_repr.keys())

    def test_to_display_representation_works_with_lt(self):
        lt_data = copy.deepcopy(self.raw_data)
        lt_data['lt_area'] = ['Translation']
        serializer = r_s.ProjectSerializer(data=lt_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        d_repr = serializer.to_display_representation(instance)
        self.assertEquals(tuple(d_repr['lt_area'].keys()),
                          tuple(['field_label', 'field_value']))
        self.raw_data['lt_area'] = []

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.ProjectSerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.ProjectIdentifierSerializer.Meta.model.schema_fields[
                'project_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.ProjectSerializer.Meta.model.schema_fields['project_identifier']]
        ]))

    def test_to_xml_representation_lt_class(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = [
            choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT,
            'lt class non CV value'
        ]
        serializer = r_s.ProjectSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        xml_data = serializer.to_xml_representation()
        ltclass_xml_field_name = r_s.ProjectSerializer.Meta.model.schema_fields['lt_area']
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].keys()), ['ms:LTClassRecommended'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][0].values()),
                          [choices.LT_CLASS_RECOMMENDED_CHOICES.ALIGNMENT])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].keys()), ['ms:LTClassOther'])
        self.assertEquals(list(xml_data[ltclass_xml_field_name][1].values()),
                          ['lt class non CV value'])


class TestProvenanceSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['provenance']
        cls.serializer = r_s.provenanceSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.provenanceSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_repository_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.provenanceSerializer.Meta.fields)

    def test_repository_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data["originating_repository"][0]['repository_identifier'] = [{
            'repository_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }]
        serializer = r_s.provenanceSerializer(self.instance,
                                              data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestRepositorySerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/repository.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.RepositorySerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.RepositorySerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_repository_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.RepositorySerializer.Meta.fields)

    def test_repository_serializer_fails_when_same_schemes_are_given(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['repository_identifier'] = [
            {
                "repository_identifier_scheme": 'http://purl.org/spar/datacite/handle',
                "value": "https://orcid.org/0000-0002-4725-3094"
            },
            {
                "repository_identifier_scheme": 'http://purl.org/spar/datacite/handle',
                "value": "https://orcid.org/0000-0002-4725-3094-004"
            }
        ]
        serializer = r_s.RepositorySerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_repository_serializer_update_passes(self):
        self.assertTrue(self.serializer.is_valid())
        data_new = copy.deepcopy(self.serializer.data)
        data_new[
            "mission_statement_url"] = "http://www.missionstatementurl_new.com"
        serializer = r_s.GenericRepositorySerializer(self.instance,
                                                     data=data_new)
        if serializer.is_valid():
            new_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())
        self.assertEquals(tuple(data_new.keys()),
                          r_s.RepositorySerializer.Meta.fields)

    def test_repository_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['repository_identifier'] = [{
            'repository_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }]
        serializer = r_s.RepositorySerializer(self.instance,
                                              data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_empty_string_save_null_repository_country(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['repository_country'] = ''
        serializer = r_s.RepositorySerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEqual(instance.repository_country, None)

    def test_default_value_mission_statement_url(self):
        data = copy.deepcopy(self.raw_data)
        del data['mission_statement_url']
        serializer = r_s.RepositorySerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
            self.assertEquals(instance.mission_statement_url, 'https://www.clarin.gr/en/about/what-is-clarin')
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(serializer.is_valid())

    def test_validate_repository_type_aggregating_requires_provenance(self):
        new_raw_data = copy.deepcopy(self.raw_data)
        new_raw_data['repository_type'] = choices.REPOSITORY_TYPE_CHOICES.AGGREGATING
        del new_raw_data['provenance']
        serializer = r_s.RepositorySerializer(data=new_raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_no_registry_identifier(self):
        xml_representation = r_s.RepositorySerializer(instance=self.instance).to_xml_representation()
        self.assertTrue(all([
            lr_id[r_s.RepositoryIdentifierSerializer.Meta.model.schema_fields[
                'repository_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            for lr_id in xml_representation[r_s.RepositorySerializer.Meta.model.schema_fields['repository_identifier']]
        ]))


class TestDescribedEntitySerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.DescribedEntitySerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_described_entity_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertTrue('entity_type' in tuple(data.keys()))

    def test_described_entity_serializer_fails_with_bad_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['entity_type'] = 'fail'
        serializer = r_s.DescribedEntitySerializer(self.instance,
                                                   data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_described_entity_serializer_fails_with_no_input(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['entity_type'] = ''
        serializer = r_s.DescribedEntitySerializer(self.instance,
                                                   data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_described_entity_validation_fails_with_none(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['entity_type'] = None
        serializer = r_s.DescribedEntitySerializer(self.instance,
                                                   data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_described_entity_validation_fails_with_no_field(self):
        raw_data = copy.deepcopy(self.raw_data)
        del raw_data['entity_type']
        serializer = r_s.DescribedEntitySerializer(self.instance,
                                                   data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())

    def test_described_entity_validation_fails_with_wrong_entity_type(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['entity_type'] = 'Person'
        serializer = r_s.DescribedEntitySerializer(self.instance,
                                                   data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)

        self.assertFalse(serializer.is_valid())


class TestMetadataRecordSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for project
        json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        cls.json_data = json.loads(cls.json_str)
        cls.serializer = r_s.MetadataRecordSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)
        # From JSON data, create a metadata record for org
        org_json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.org_json_str = org_json_file.read()
        org_json_file.close()
        cls.org_json_data = json.loads(cls.org_json_str)
        cls.org_json_data['described_entity']['is_division_of'] = [{
            "actor_type": "Organization",
            "organization_name": {
                "en": "ILSP"
            },
            "website": [
                "https://www.ilsp.gr"
            ],
            "is_division_of": [{
                "actor_type": "Organization",
                "organization_name": {
                    "en": "evenhigher"
                },
                "website": [
                    "https://www.evenhigher.gr"
                ]
            }]
        },
            {
                "actor_type": "Organization",
                "organization_name": {
                    "en": "ILSP 3"
                },
                "website": [
                    "https://www.ilsp3.gr"
                ]
            }
        ]
        cls.org_serializer = r_s.MetadataRecordSerializer(data=cls.org_json_data)
        if cls.org_serializer.is_valid():
            cls.org_instance = cls.org_serializer.save()
        else:
            LOGGER.debug(cls.org_serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.json_data.keys():
            if i not in r_s.MetadataRecordSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_duplicate_validation_error(self):
        serializer = r_s.MetadataRecordSerializer(data=self.json_data)
        if not serializer.is_valid():
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_duplicate_validation_error_when_first_record_is_under_construction(self):
        raw_data = copy.deepcopy(self.json_data)
        raw_data['described_entity']['project_name'] = {"en": "duplicate u c"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
            instance.management_object.under_construction = True
            instance.management_object.save()
        serializer_1 = r_s.MetadataRecordSerializer(data=raw_data)
        if not serializer_1.is_valid():
            LOGGER.debug(serializer_1.errors)
        self.assertFalse(serializer_1.is_valid())

    def test_duplicate_validation_org_same_name_same_parents(self):
        raw_data = copy.deepcopy(self.org_json_data)
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if not serializer.is_valid():
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_duplicate_validation_org_same_name_one_same_parent(self):
        raw_data = copy.deepcopy(self.org_json_data)
        raw_data['described_entity']['is_division_of'] = [{
            "actor_type": "Organization",
            "organization_name": {
                "en": "ILSP"
            },
            "website": [
                "https://www.ilsp.gr"
            ],
            "is_division_of": [{
                "actor_type": "Organization",
                "organization_name": {
                    "en": "evenhigher"
                },
                "website": [
                    "https://www.evenhigher.gr"
                ]
            }]
        }]
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if not serializer.is_valid():
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_duplicate_validation_org_same_name_no_parent(self):
        raw_data = copy.deepcopy(self.org_json_data)
        raw_data['described_entity']['organization_name'] = {"en": "duplicate n p"}
        raw_data['described_entity']['organization_short_name'] = []
        raw_data['described_entity']['organization_identifier'] = []
        raw_data['described_entity']['is_division_of'] = None
        raw_data['described_entity']['division_category'] = None
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        serializer_1 = r_s.MetadataRecordSerializer(data=raw_data)
        if not serializer_1.is_valid():
            LOGGER.debug(serializer_1.errors)
        self.assertFalse(serializer_1.is_valid())

    def test_duplicate_validation_org_dif_name_no_parent(self):
        raw_data = copy.deepcopy(self.org_json_data)
        raw_data['described_entity']['organization_name'] = {"en": "duplicate n p d"}
        raw_data['described_entity']['organization_short_name'] = []
        raw_data['described_entity']['organization_identifier'] = []
        raw_data['described_entity']['website'] = []
        raw_data['described_entity']['is_division_of'] = None
        raw_data['described_entity']['division_category'] = None
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        n_raw_data = copy.deepcopy(raw_data)
        n_raw_data['described_entity']['organization_name'] = {"en": "duplicate n p not d"}
        serializer_1 = r_s.MetadataRecordSerializer(data=n_raw_data)
        if not serializer_1.is_valid():
            LOGGER.debug(serializer_1.errors)
        self.assertTrue(serializer_1.is_valid())

    def test_duplicate_validation_org_same_name_no_parent_with_org_that_has_parent(self):
        raw_data = copy.deepcopy(self.org_json_data)
        raw_data['described_entity']['organization_identifier'] = []
        raw_data['described_entity']['is_division_of'] = None
        raw_data['described_entity']['division_category'] = None
        raw_data['described_entity']['website'] = []
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        if not serializer.is_valid():
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_duplicate_validation_org_dif_name_with_all_same_parents(self):
        raw_data = copy.deepcopy(self.org_json_data)
        raw_data['described_entity']['organization_name'] = {"en": "duplicate p d"}
        raw_data['described_entity']['organization_short_name'] = []
        raw_data['described_entity']['organization_identifier'] = []
        raw_data['described_entity']['website'] = []
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertTrue(serializer.is_valid())

    def test_duplicate_validation_org_same_name_with_parent_with_org_without_parent(self):
        p_raw_data = copy.deepcopy(self.org_json_data)
        p_raw_data['described_entity']['organization_name'] = {"en": "n duplicate p d"}
        p_raw_data['described_entity']['organization_short_name'] = []
        p_raw_data['described_entity']['organization_identifier'] = []
        p_raw_data['described_entity']['website'] = []
        p_raw_data['described_entity']['is_division_of'] = None
        p_raw_data['described_entity']['division_category'] = None
        p_serializer = r_s.MetadataRecordSerializer(data=p_raw_data)
        if p_serializer.is_valid():
            instance = p_serializer.save()
        raw_data = copy.deepcopy(self.org_json_data)
        raw_data['described_entity']['organization_name'] = {"en": "n duplicate p d"}
        raw_data['described_entity']['organization_short_name'] = []
        raw_data['described_entity']['organization_identifier'] = []
        raw_data['described_entity']['website'] = []
        p_raw_data['described_entity']['is_division_of'] = [{
            "actor_type": "Organization",
            "organization_name": {
                "en": "ILSP"
            },
            "website": [
                "https://www.ilsp.gr"
            ],
            "is_division_of": [{
                "actor_type": "Organization",
                "organization_name": {
                    "en": "evenhigher"
                },
                "website": [
                    "https://www.evenhigher.gr"
                ]
            }]
        }]
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertTrue(serializer.is_valid())

    def test_duplicate_validation_org_dif_name_with_one_same_parent(self):
        raw_data = copy.deepcopy(self.org_json_data)
        raw_data['described_entity']['organization_name'] = {"en": "duplicate p d"}
        raw_data['described_entity']['organization_short_name'] = []
        raw_data['described_entity']['organization_identifier'] = []
        raw_data['described_entity']['website'] = []
        raw_data['described_entity']['is_division_of'] = [{
            "actor_type": "Organization",
            "organization_name": {
                "en": "ILSP"
            },
            "website": [
                "https://www.ilsp.gr"
            ],
            "is_division_of": [{
                "actor_type": "Organization",
                "organization_name": {
                    "en": "evenhigher"
                },
                "website": [
                    "https://www.evenhigher.gr"
                ]
            }]
        }]
        serializer = r_s.MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        self.assertTrue(serializer.is_valid())

    def test_mdr_identifier_gets_removed_and_replaced(self):
        json_file_rem = open('registry/tests/fixtures/project.json', 'r')
        json_str_rem = json_file_rem.read()
        json_file_rem.close()
        json_data_rem = json.loads(json_str_rem)
        json_data_rem['described_entity']['project_name'] = {"en": "not a duplicate"}
        json_data_rem['described_entity']['project_short_name'] = {}
        json_data_rem['described_entity']['project_identifier'] = []
        json_data_rem['described_entity']['grant_number'] = ''
        json_data_rem['described_entity']['website'] = []
        json_data_rem['metadatarecord_identifier'] = {
            "value": "user_provider_registry_id",
            "metadata_record_identifier_scheme": 'http://purl.org/spar/datacite/handle'
        }
        serializer = r_s.MetadataRecordSerializer(data=json_data_rem)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertEquals(data['metadata_record_identifier'][
                              'metadata_record_identifier_scheme'],
                          settings.REGISTRY_IDENTIFIER_SCHEMA)

    def test_get_detail_url(self):
        json_file_rem = open('registry/tests/fixtures/project.json', 'r')
        json_str_rem = json_file_rem.read()
        json_file_rem.close()
        json_data_rem = json.loads(json_str_rem)
        json_data_rem['described_entity']['project_name'] = {"en": "not a duplicate 1"}
        json_data_rem['described_entity']['project_short_name'] = {}
        json_data_rem['described_entity']['project_identifier'] = []
        json_data_rem['described_entity']['grant_number'] = ''
        json_data_rem['described_entity']['website'] = []
        serializer = r_s.MetadataRecordSerializer(data=json_data_rem)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertEquals(instance.get_detail_url(),
                          f'{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/{instance.pk}/')

    def test_return_group_instance_name(self):
        json_file_group = open('registry/tests/fixtures/organization.json', 'r')
        json_str_group = json_file_group.read()
        json_file_group.close()
        json_data_rem = json.loads(json_str_group)
        json_data_rem['described_entity']['entity_type'] = 'Group'
        serializer = r_s.MetadataRecordSerializer(data=json_data_rem)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertEquals(str(instance),
                          f'[Group] {instance.described_entity.actor.group.organization_name["en"]}')

    def test_return_organization_instance_name(self):
        self.assertEquals(str(self.org_instance),
                          f'[Organization] {self.org_instance.described_entity.actor.organization.organization_name["en"]}')

    def test_return_document_instance_name(self):
        json_file_group = open('registry/tests/fixtures/document.json', 'r')
        json_str_group = json_file_group.read()
        json_file_group.close()
        json_data_rem = json.loads(json_str_group)
        serializer = r_s.MetadataRecordSerializer(data=json_data_rem)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertEquals(str(instance),
                          f'[Document] {instance.described_entity.document.title["en"]}')

    def test_return_repository_instance_name(self):
        json_file_group = open('registry/tests/fixtures/repository.json', 'r')
        json_str_group = json_file_group.read()
        json_file_group.close()
        json_data_rem = json.loads(json_str_group)
        serializer = r_s.MetadataRecordSerializer(data=json_data_rem)
        if serializer.is_valid():
            instance = serializer.save()
            self.assertEquals(str(instance),
                              f'[Repository] {instance.described_entity.repository.repository_name["en"]}')
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
            self.assertTrue(serializer.is_valid())

    def test_return_licence_terms_instance_name(self):
        json_file_group = open('registry/tests/fixtures/licence_terms.json',
                               'r')
        json_str_group = json_file_group.read()
        json_file_group.close()
        json_data_rem = json.loads(json_str_group)
        serializer = r_s.MetadataRecordSerializer(data=json_data_rem)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertEquals(str(instance),
                          f'[LicenceTerms] {instance.described_entity.licenceterms.licence_terms_name["en"]}')

    def test_return_person_instance_name(self):
        json_file_group = open('registry/tests/fixtures/person.json', 'r')
        json_str_group = json_file_group.read()
        json_file_group.close()
        json_data_rem = json.loads(json_str_group)
        serializer = r_s.MetadataRecordSerializer(data=json_data_rem)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertEquals(str(instance),
                          f'[Person] {instance.described_entity.actor.person.surname["en"]} '
                          f'{instance.described_entity.actor.person.given_name["en"]}')

    def test_return_language_resource_instance_name(self):
        json_file_group = open('registry/tests/fixtures/tool.json', 'r')
        json_str_group = json_file_group.read()
        json_file_group.close()
        json_data_rem = json.loads(json_str_group)
        serializer = r_s.MetadataRecordSerializer(data=json_data_rem)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertEquals(str(instance),
                          f'[{instance.described_entity.languageresource.lr_subclass.lr_type}] '
                          f'{instance.described_entity.languageresource.resource_name["en"]}')

    def test_to_xml_representation_no_system_identifier(self):
        xml_representation = r_s.MetadataRecordSerializer(instance=self.instance).to_xml_representation()
        if r_s.MetadataRecordSerializer.Meta.model.schema_fields['metadata_record_identifier'] in xml_representation:
            print(r_s.MetadataRecordSerializer.Meta.model.schema_fields['metadata_record_identifier'])
            self.assertTrue(
                xml_representation[r_s.MetadataRecordSerializer.Meta.model.schema_fields['metadata_record_identifier']]
                [r_s.MetadataRecordIdentifierSerializer.Meta.model.schema_fields[
                    'metadata_record_identifier_scheme']] != settings.REGISTRY_IDENTIFIER_SCHEMA
            )
        self.assertTrue(True)


class TestFunderIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.fi = {
            'funder_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
            'value': "doi_value_id"
        }
        cls.serializer = r_s.FunderIdentifierSerializer(data=cls.fi)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.fi.keys():
            if i not in r_s.FunderIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_funder_identifier_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.FunderIdentifierSerializer.Meta.fields)

    def test_funder_identifier_serializer_fails_with_bad_data(self):
        fail_data = {
            'funder_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.FunderIdentifierSerializer(self.instance,
                                                    data=fail_data)
        self.assertFalse(serializer.is_valid())


class TestCreateReverseRelationLrFromLrs(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = r_s.LanguageResourceSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

        cls.xml_tool_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(cls.xml_tool_test_data[0], encoding='utf-8') as xml_file:
            cls.xml_tool_input = xml_file.read()
        cls.tool_data = xmltodict.parse(cls.xml_tool_input, xml_attribs=True)
        cls.xml_tool_data = cls.tool_data['ms:MetadataRecord']

    def test_isRelatedToLRTs_reverse_relation(self):
        from_xml = r_s.MetadataRecordSerializer(xml_data=self.xml_tool_data).initial_data
        from_xml['described_entity']['is_related_to_lr'] = [self.serializer.data]
        from_xml['described_entity']['resource_name'] = {"en": "not a duplicate tool related to lr"}
        from_xml['described_entity']['resource_short_name'] = {}
        from_xml['described_entity']['lr_identifier'] = []
        lr_serializer = r_s.MetadataRecordSerializer(data=from_xml)
        if lr_serializer.is_valid():
            lr_instance = lr_serializer.save()
            lr_instance.management_object.status = PUBLISHED
            user, _ = get_user_model().objects.get_or_create(username='curatorA1')
            lr_instance.management_object.curator = user
            lr_instance.management_object.save()
        else:
            LOGGER.info(lr_serializer.errors)
            self.assertTrue(lr_serializer.is_valid())

        self.assertTrue(meet_conditions_to_display_full_mdr_for_generic_entity(lr_instance.described_entity))
        d_repr = self.serializer.to_display_representation(self.instance)
        g_repr = r_s.GenericLanguageResourceSerializer().to_display_representation(
            lr_instance.described_entity
        )
        self.assertEqual(
            d_repr['is_related_to_lr']['field_value'][0],
            g_repr
        )

    def test_ArchiveLRTs_reverse_relation(self):
        from_xml = r_s.MetadataRecordSerializer(xml_data=self.xml_tool_data).initial_data
        from_xml['described_entity']['is_archived_by'] = [self.serializer.data]
        from_xml['described_entity']['resource_name'] = {"en": "not a duplicate tool archive"}
        from_xml['described_entity']['resource_short_name'] = {}
        from_xml['described_entity']['lr_identifier'] = []
        lr_serializer = r_s.MetadataRecordSerializer(data=from_xml)
        if lr_serializer.is_valid():
            lr_instance = lr_serializer.save()
            lr_instance.management_object.status = PUBLISHED
            user, _ = get_user_model().objects.get_or_create(username='curatorA1')
            lr_instance.management_object.curator = user
            lr_instance.management_object.save()
        else:
            LOGGER.info(lr_serializer.errors)
            self.assertTrue(lr_serializer.is_valid())

        g_repr = r_s.GenericLanguageResourceSerializer().to_display_representation(
            lr_instance.described_entity)
        self.assertTrue(meet_conditions_to_display_full_mdr_for_generic_entity(
            lr_instance.described_entity))

        # Get display representation of LR of interest
        display_repr = self.serializer.to_display_representation(self.instance)
        # Has reverse relations
        self.assertTrue('reverse_relations' in display_repr.keys())
        self.assertTrue(display_repr['reverse_relations'] is not None)

        # Check that the hasArchived part of reverse relations
        inverse_relation_of_interest_label = 'Archived Language Resources and Technologies'
        inverse_relations_labels = list()
        inverse_relation_list = list()
        for i in range(0, len(display_repr['reverse_relations']['field_value'])):
            label = display_repr['reverse_relations']['field_value'][i]['field_label']['en']
            inverse_relations_labels.append(label)
            if label == inverse_relation_of_interest_label:
                inverse_relation_list = display_repr['reverse_relations']['field_value'][i]['field_value']
        self.assertTrue(inverse_relation_of_interest_label in inverse_relations_labels)
        # Check that corpus, model, tool  is in the inverse relation of interest
        self.assertTrue(g_repr in inverse_relation_list)

    def test_create_reverse_relation_LrFromLrs_fails(self):
        from_xml = r_s.MetadataRecordSerializer(xml_data=self.xml_tool_data).initial_data
        from_xml['described_entity']['is_archived_by'] = [self.serializer.data]
        serializer_lr = r_s.MetadataRecordSerializer(data=from_xml)
        if serializer_lr.is_valid():
            instance_lr = serializer_lr.save()
        else:
            LOGGER.debug(serializer_lr.errors)

        g_repr = r_s.GenericLanguageResourceSerializer().to_display_representation(
            instance_lr.described_entity)
        self.assertFalse(meet_conditions_to_display_full_mdr_for_generic_entity(
            instance_lr.described_entity))

        # Get display representation of LR of interest
        display_repr = self.serializer.to_display_representation(self.instance)
        # Has reverse relations
        self.assertTrue('reverse_relations' in display_repr.keys())
        self.assertTrue(display_repr['reverse_relations'] is not None)

        # Check that the hasArchived part of reverse relations
        inverse_relation_of_interest_label = 'Archived Language Resources and Technologies'
        inverse_relations_labels = list()
        inverse_relation_list = list()
        for i in range(0, len(display_repr['reverse_relations']['field_value'])):
            label = display_repr['reverse_relations']['field_value'][i]['field_label']['en']
            inverse_relations_labels.append(label)
            if label == inverse_relation_of_interest_label:
                inverse_relation_list = display_repr['reverse_relations']['field_value'][i]['field_value']
        self.assertTrue(inverse_relation_of_interest_label not in inverse_relations_labels)
        # Check that corpus, model, tool  is in the inverse relation of interest
        self.assertTrue(g_repr not in inverse_relation_list)


class TestCreateReverseRelationLrFromLrsNotPublished(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.raw_data = json.loads(cls.json_str)
        cls.user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        cls.serializer = r_s.MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.instance.management_object.curator = cls.user
            cls.instance.management_object.save()
        else:
            LOGGER.debug(cls.serializer.errors)

        cls.xml_tool_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(cls.xml_tool_test_data[0], encoding='utf-8') as xml_file:
            cls.xml_tool_input = xml_file.read()
        cls.tool_data = xmltodict.parse(cls.xml_tool_input, xml_attribs=True)
        cls.xml_tool_data = cls.tool_data['ms:MetadataRecord']

    def test_isRelatedToLRTs_reverse_relation_np(self):
        from_xml = r_s.MetadataRecordSerializer(xml_data=self.xml_tool_data).initial_data
        from_xml['described_entity']['is_related_to_lr'] = [
            r_s.GenericLanguageResourceSerializer(self.instance.described_entity).data]
        from_xml['described_entity']['resource_name'] = {"en": "not a duplicate tool related to lr"}
        from_xml['described_entity']['resource_short_name'] = {}
        from_xml['described_entity']['lr_identifier'] = []
        lr_serializer = r_s.MetadataRecordSerializer(data=from_xml)
        if lr_serializer.is_valid():
            lr_instance = lr_serializer.save()
            lr_instance.management_object.curator = self.user
            lr_instance.management_object.save()
        else:
            LOGGER.info(lr_serializer.errors)
            self.assertTrue(lr_serializer.is_valid())

        self.assertTrue(meet_conditions_to_display_full_mdr_for_generic_entity(lr_instance.described_entity, True))
        d_repr = r_s.MetadataRecordSerializer(data=self.instance,
                                              context={'uncensored': True}).to_display_representation(self.instance)
        g_repr = r_s.GenericLanguageResourceSerializer(context={'uncensored': True}).to_display_representation(
            lr_instance.described_entity
        )
        self.assertEqual(
            d_repr['described_entity']['field_value']['is_related_to_lr']['field_value'][0],
            g_repr
        )

    def test_isRelatedToLRTs_reverse_relation_np_passes_without_context(self):
        from_xml = r_s.MetadataRecordSerializer(xml_data=self.xml_tool_data).initial_data
        from_xml['described_entity']['is_related_to_lr'] = [
            r_s.GenericLanguageResourceSerializer(self.instance.described_entity).data]
        from_xml['described_entity']['resource_name'] = {"en": "not a duplicate tool related to lr"}
        from_xml['described_entity']['resource_short_name'] = {}
        from_xml['described_entity']['lr_identifier'] = []
        lr_serializer = r_s.MetadataRecordSerializer(data=from_xml)
        if lr_serializer.is_valid():
            lr_instance = lr_serializer.save()
            lr_instance.management_object.curator = self.user
            lr_instance.management_object.save()
        else:
            LOGGER.info(lr_serializer.errors)
            self.assertTrue(lr_serializer.is_valid())

        d_repr = r_s.MetadataRecordSerializer(data=self.instance).to_display_representation(self.instance)
        g_repr = r_s.GenericLanguageResourceSerializer().to_display_representation(
            lr_instance.described_entity
        )
        self.assertTrue(
            d_repr['described_entity']['field_value'].get('is_related_to_lr'))

    def test_ArchiveLRTs_reverse_relation_np(self):
        from_xml = r_s.MetadataRecordSerializer(xml_data=self.xml_tool_data).initial_data
        from_xml['described_entity']['is_archived_by'] = [
            r_s.GenericLanguageResourceSerializer(self.instance.described_entity).data]
        from_xml['described_entity']['resource_name'] = {"en": "not a duplicate tool archive"}
        from_xml['described_entity']['resource_short_name'] = {}
        from_xml['described_entity']['lr_identifier'] = []
        lr_serializer = r_s.MetadataRecordSerializer(data=from_xml)
        if lr_serializer.is_valid():
            lr_instance = lr_serializer.save()
            lr_instance.management_object.curator = self.user
            lr_instance.management_object.save()
        else:
            LOGGER.info(lr_serializer.errors)
        self.assertTrue(lr_serializer.is_valid())

        self.assertTrue(meet_conditions_to_display_full_mdr_for_generic_entity(lr_instance.described_entity, True))
        g_repr = r_s.GenericLanguageResourceSerializer(context={'uncensored': True}).to_display_representation(
            lr_instance.described_entity
        )

        # Get display representation of LR of interest
        display_repr = r_s.MetadataRecordSerializer(context={'uncensored': True}).to_display_representation(
            self.instance)
        # Has reverse relations
        self.assertTrue('reverse_relations' in display_repr['described_entity']['field_value'].keys())
        self.assertTrue(display_repr['described_entity']['field_value']['reverse_relations'] is not None)

        # Check that the hasArchived part of reverse relations
        inverse_relation_of_interest_label = 'Archived Language Resources and Technologies'
        inverse_relations_labels = list()
        inverse_relation_list = list()
        for i in range(0, len(display_repr['described_entity']['field_value']['reverse_relations']['field_value'])):
            label = \
            display_repr['described_entity']['field_value']['reverse_relations']['field_value'][i]['field_label']['en']
            inverse_relations_labels.append(label)
            if label == inverse_relation_of_interest_label:
                inverse_relation_list = \
                display_repr['described_entity']['field_value']['reverse_relations']['field_value'][i]['field_value']
        self.assertTrue(inverse_relation_of_interest_label in inverse_relations_labels)
        # Check that corpus, model, tool  is in the inverse relation of interest
        self.assertTrue(g_repr in inverse_relation_list)

    def test_ArchiveLRTs_reverse_relation_np_fails(self):
        from_xml = r_s.MetadataRecordSerializer(xml_data=self.xml_tool_data).initial_data
        from_xml['described_entity']['is_archived_by'] = [
            r_s.GenericLanguageResourceSerializer(self.instance.described_entity).data]
        from_xml['described_entity']['resource_name'] = {"en": "not a duplicate tool archive"}
        from_xml['described_entity']['resource_short_name'] = {}
        from_xml['described_entity']['lr_identifier'] = []
        lr_serializer = r_s.MetadataRecordSerializer(data=from_xml)
        if lr_serializer.is_valid():
            lr_instance = lr_serializer.save()
            lr_instance.management_object.curator = self.user
            lr_instance.management_object.save()
        else:
            LOGGER.info(lr_serializer.errors)
        self.assertTrue(lr_serializer.is_valid())

        self.assertFalse(meet_conditions_to_display_full_mdr_for_generic_entity(lr_instance.described_entity))
        g_repr = r_s.GenericLanguageResourceSerializer().to_display_representation(
            lr_instance.described_entity
        )

        # Get display representation of LR of interest
        display_repr = r_s.MetadataRecordSerializer().to_display_representation(
            self.instance)
        # Has reverse relations
        self.assertTrue('reverse_relations' in display_repr['described_entity']['field_value'].keys())
        self.assertTrue(display_repr['described_entity']['field_value']['reverse_relations'] is not None)

        # Check that the hasArchived part of reverse relations
        inverse_relation_of_interest_label = 'Archived Language Resources and Technologies'
        inverse_relations_labels = list()
        inverse_relation_list = list()
        for i in range(0, len(display_repr['described_entity']['field_value']['reverse_relations']['field_value'])):
            label = \
                display_repr['described_entity']['field_value']['reverse_relations']['field_value'][i]['field_label'][
                    'en']
            inverse_relations_labels.append(label)
            if label == inverse_relation_of_interest_label:
                inverse_relation_list = \
                    display_repr['described_entity']['field_value']['reverse_relations']['field_value'][i][
                        'field_value']
        self.assertTrue(inverse_relation_of_interest_label not in inverse_relations_labels)
        # Check that corpus, model, tool  is in the inverse relation of interest
        self.assertTrue(g_repr not in inverse_relation_list)


class TestunspecifiedPartSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['unspecified_part']
        cls.serializer = r_s.unspecifiedPartSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.unspecifiedPartSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_unspecified_part_serializer_passes_with_multiple_different_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_id": "en",
                "script_id": "Adlm",
                "region_id": "150",
                "variant_id": [
                ],
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "en",
                "script_id": "Adlm",
                "region_id": "150",
                "variant_id": [
                ],
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.unspecifiedPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_unspecified_part_serializer_fails_with_multiple_same_languages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['language'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.unspecifiedPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_unspecified_part_serializer_passes_with_multiple_different_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": None
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.unspecifiedPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())

    def test_unspecified_part_serializer_fails_with_multiple_same_metalanguages(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metalanguage'].extend([
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            },
            {
                "language_tag": "en",
                "language_id": "en",
                "script_id": "",
                "region_id": "",
                "variant_id": None,
                "language_variety_name": {
                    "en": "var"
                }
            }
        ])
        serializer = r_s.unspecifiedPartSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertFalse(serializer.is_valid())

    def test_generated_linguality_type_monolingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.unspecifiedPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)

    def test_generated_linguality_type_bilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.unspecifiedPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)

    def test_generated_linguality_type_multilingual(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "de",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.unspecifiedPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_one_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
        ]
        serializer = r_s.unspecifiedPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_multilingual_collective_two_lang(self):
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "aav",
                "language_variety_name": {
                    "en": "variety name"
                }
            },
            {
                "language_id": "afa",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.unspecifiedPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL)

    def test_generated_linguality_type_monolingual_on_update(self):
        # Create unspecified part
        data = copy.deepcopy(self.raw_data)
        data['language'] = [
            {
                "language_id": "en",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        ]
        serializer = r_s.unspecifiedPartSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL)
        # Update unspecified part
        data = copy.deepcopy(r_s.unspecifiedPartSerializer(instance).data)
        data['language'].append(
            {
                "language_id": "fr",
                "language_variety_name": {
                    "en": "variety name"
                }
            }
        )
        serializer = r_s.unspecifiedPartSerializer(instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertEquals(instance.linguality_type, choices.LINGUALITY_TYPE_CHOICES.BILINGUAL)


class TestDistributionUnspecifiedFeatureSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/dummy_data.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['distribution_unspecified_feature']
        cls.serializer = r_s.distributionUnspecifiedFeatureSerializer(
            data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.distributionUnspecifiedFeatureSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_distribution_unspecified_feature_serializer_contains_expected_fields(
            self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.distributionUnspecifiedFeatureSerializer.Meta.fields)

    def test_distribution_unspecified_feature_serializer_fails_with_no_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = []
        serializer = r_s.distributionUnspecifiedFeatureSerializer(self.instance,
                                                                  data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_to_xml_representation_data_format(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [
            choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
            'data format non CV value'
        ]
        serializer = r_s.distributionUnspecifiedFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        xml_data = serializer.to_xml_representation()
        dev_frame_xml_field_name = r_s.distributionUnspecifiedFeatureSerializer.Meta.model.schema_fields['data_format']
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].keys()), ['ms:dataFormatRecommended'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][0].values()),
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].keys()), ['ms:dataFormatOther'])
        self.assertEquals(list(xml_data[dev_frame_xml_field_name][1].values()),
                          ['data format non CV value'])

    def test_from_xml_data_format_works_tool(self):
        xml_test_data = 'registry/tests/fixtures/corpus.xml'
        with open(xml_test_data, encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        md_xml_data = xmltodict.parse(xml_input, xml_attribs=True)
        xml_data = \
            md_xml_data['ms:MetadataRecord']['ms:DescribedEntity']['ms:LanguageResource']['ms:LRSubclass'][
                'ms:Corpus']['ms:DatasetDistribution']['ms:distributionUnspecifiedFeature']
        from_xml = r_s.distributionUnspecifiedFeatureSerializer().from_xml_representation(xml_data=xml_data)
        self.assertEquals(from_xml['data_format'],
                          [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                           'data format non CV value'])

    def test_to_display_representation_for_data_format_works(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['data_format'] = [choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS, 'data format non CV value']
        serializer = r_s.distributionUnspecifiedFeatureSerializer(data=raw_data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        dis_repr = serializer.to_display_representation(instance)
        self.assertEquals(dis_repr['data_format'],
                          {
                              'field_label': {'en': 'Data format'},
                              'field_value': [
                                  {
                                      'value': choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS,
                                      'label': {
                                          'en': choices.DATA_FORMAT_RECOMMENDED_CHOICES[
                                              choices.DATA_FORMAT_RECOMMENDED_CHOICES.ANNIS]}
                                  },
                                  {
                                      'value': 'data format non CV value'
                                  }
                              ]
                          })


class TestPeriodOfTimeSerializerSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.raw_data = json.loads(json_str)['period_of_time']
        cls.serializer = r_s.PeriodOfTimeSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.PeriodOfTimeSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_lcr_serializer_has_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.PeriodOfTimeSerializer.Meta.fields)

    def test_lcr_serializer_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['start_date'] = 'fail'
        serializer = r_s.PeriodOfTimeSerializer(self.instance, data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        self.assertFalse(serializer.is_valid())


class TestAttributionSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.raw_data = json.loads(json_str)['attribution']
        cls.serializer = r_s.AttributionSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.AttributionSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)


class TestRoleSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        json_file = open('test_fixtures/dummy_data.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.raw_data = json.loads(json_str)['role']
        cls.serializer = r_s.RoleSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.raw_data.keys():
            if i not in r_s.RoleSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)


class TestRoleIdentifierSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.ari = {
            'role_identifier_scheme': 'http://w3id.org/meta-share/meta-share/DataCite',
            'value': "doi_value_id",
        }

        cls.serializer = r_s.RoleIdentifierSerializer(
            data=cls.ari)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_fixture_fields_in_serializer_fields(self):
        extra_fields = list()
        for i in self.ari.keys():
            if i not in r_s.RoleIdentifierSerializer.Meta.fields:
                extra_fields.append(i)
        message = f'The fields {extra_fields} are not recognized by the serializer'
        self.assertEquals(extra_fields, [], message)

    def test_serializer_contains_expected_fields(self):
        self.assertTrue(self.serializer.is_valid())
        data = self.serializer.data
        self.assertEquals(tuple(data.keys()),
                          r_s.RoleIdentifierSerializer.Meta.fields)

    def test_serializer_fails_with_bad_data(self):
        data_fail = {
            'role_identifier_scheme': 'fail',
            'value': "doi_value_id"
        }
        serializer = r_s.RoleIdentifierSerializer(
            self.instance, data=data_fail)
        self.assertFalse(serializer.is_valid())
