import copy
import json
import logging

from registry.serializer_tasks import GetRelatedRecords
from registry.serializers import MetadataRecordSerializer, LicenceTermsSerializer, DatasetDistributionSerializer
from registry.models import MetadataRecord, LicenceTerms, DatasetDistribution, Corpus
from registry.view_tasks import batch_xml_retrieve
from test_utils import (
    import_metadata, get_test_user, EndpointTestCase, SerializerTestCase
)

LOGGER = logging.getLogger(__name__)


class BatchXMLRetrieveTaskTest(EndpointTestCase):
    json_test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.json_test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def test_retrieve_all_requested(self):
        request_user_id = 1
        requested_ids = [md.pk for md in self.metadata_records]
        permitted_ids = requested_ids
        errors = None
        retrieved_records = batch_xml_retrieve(request_user_id, requested_ids, permitted_ids, errors)
        self.assertEquals(requested_ids, permitted_ids)
        self.assertEquals(len(permitted_ids), len(retrieved_records))

    def test_retrieve_all_permitted(self):
        request_user_id = 1
        requested_ids = [md.pk for md in self.metadata_records]
        permitted_ids = copy.deepcopy(requested_ids)
        permitted_ids.remove(self.metadata_records[0].pk)
        errors = None
        retrieved_records = batch_xml_retrieve(request_user_id, requested_ids, permitted_ids, errors)
        self.assertEquals(len(permitted_ids), len(retrieved_records))


class TestRecognitionOfRelatedEntities(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/corpus.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.raw_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_lt_relations(self):
        l_t = self.instance.described_entity.lr_subclass.dataset_distribution.first().licence_terms.all()[0]
        get_related_instances = GetRelatedRecords(l_t)
        relations = get_related_instances.get_all_fields()
        self.assertTrue(self.instance.described_entity in relations)

    def test_lr_relations(self):
        l_r = self.instance.described_entity.is_created_by.all()[0]
        get_related_instances = GetRelatedRecords(l_r)
        relations = get_related_instances.get_all_fields()
        self.assertTrue(self.instance.described_entity in relations)

    def test_lr_relations_deep(self):
        l_r = self.instance.described_entity.lr_subclass.dataset_distribution.first().is_accessed_by.all()[0]
        get_related_instances = GetRelatedRecords(l_r)
        relations = get_related_instances.get_all_fields()
        self.assertTrue(self.instance.described_entity in relations)

    def test_project_relations(self):
        prj = self.instance.described_entity.funding_project.all()[0]
        get_related_instances = GetRelatedRecords(prj)
        relations = get_related_instances.get_all_fields()
        self.assertTrue(self.instance.described_entity in relations)

    def test_org_relations(self):
        org = self.instance.described_entity.resource_provider.all()[1]
        get_related_instances = GetRelatedRecords(org)
        relations = get_related_instances.get_all_fields()
        self.assertTrue(self.instance.described_entity in relations)

    def test_person_relations(self):
        per = self.instance.described_entity.resource_provider.all()[0]
        get_related_instances = GetRelatedRecords(per)
        relations = get_related_instances.get_all_fields()
        self.assertTrue(self.instance.described_entity in relations)

    def test_document_relations(self):
        doc = self.instance.described_entity.actual_use.first().usage_report.all()[0]
        get_related_instances = GetRelatedRecords(doc)
        relations = get_related_instances.get_all_fields()
        self.assertTrue(self.instance.described_entity in relations)
