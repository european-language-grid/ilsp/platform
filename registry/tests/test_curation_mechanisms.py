import copy
import json
import logging

import registry.models as r_m
import registry.serializers as r_s
from registry import choices
from registry.choices import CURRENCY_CHOICES
from registry.curation_mechanisms import curate_model
from test_utils import SerializerTestCase

LOGGER = logging.getLogger(__name__)


class TestCurationOfFields(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # Load project
        cls.json_file_project = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str_project = cls.json_file_project.read()
        cls.json_file_project.close()
        cls.json_data_project = json.loads(cls.json_str_project)['described_entity']
        # Load organization
        cls.json_file_organization = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str_organization = cls.json_file_organization.read()
        cls.json_file_organization.close()
        cls.json_data_organization = json.loads(cls.json_str_organization)['described_entity']

    def test_update_multilingual_obligatory_en(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_name'] = {
            'en': 'Initial project name',
            'el': 'Αρχικό όνομα προτζεκτ'
        }
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.project_name['en'], data['project_name']['en'])
        self.assertEquals(instance.project_name['el'], data['project_name']['el'])
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_name': 'Updated project name'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichment
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.project_name['en'], update_info['project_name'])
        self.assertEquals(instance.project_name['el'], data['project_name']['el'])
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_update_multilingual_optional_en_exists(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_short_name'] = {
            'en': 'Initial project short name',
            'el': 'Αρχικό συντομο όνομα προτζεκτ'
        }
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.project_short_name['en'], data['project_short_name']['en'])
        self.assertEquals(instance.project_short_name['el'], data['project_short_name']['el'])
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_short_name': 'Updated project short name'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichment
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.project_short_name['en'], update_info['project_short_name'])
        self.assertEquals(instance.project_short_name['el'], data['project_short_name']['el'])
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_update_multilingual_optional_en_does_not_exist(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_short_name'] = None
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.project_short_name, None)
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_short_name': 'Updated project short name'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichment
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.project_short_name['en'], update_info['project_short_name'])
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_multiple_multilingual_exists(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_alternative_name'] = [{
            'en': 'Alternative project name',
            'el': 'Εναλλακτικό όνομα προτζεκτ'
        }
        ]
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.project_alternative_name) == 1)
        self.assertEquals(instance.project_alternative_name[0]['en'], data['project_alternative_name'][0]['en'])
        self.assertEquals(instance.project_alternative_name[0]['el'], data['project_alternative_name'][0]['el'])
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_alternative_name': 'New alternative project name'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.project_alternative_name) == 2)
        if len(instance.project_alternative_name[0]) == 2:
            self.assertEquals(instance.project_alternative_name[0]['en'], data['project_alternative_name'][0]['en'])
            self.assertEquals(instance.project_alternative_name[0]['el'], data['project_alternative_name'][0]['el'])
            self.assertEquals(instance.project_alternative_name[1]['en'], update_info['project_alternative_name'])
        else:
            self.assertEquals(instance.project_alternative_name[1]['en'], data['project_alternative_name'][0]['en'])
            self.assertEquals(instance.project_alternative_name[1]['el'], data['project_alternative_name'][0]['el'])
            self.assertEquals(instance.project_alternative_name[0]['en'], update_info['project_alternative_name'])
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_multiple_multilingual_does_not_exist(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_alternative_name'] = None
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(instance.project_alternative_name == None)
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_alternative_name': 'New alternative project name'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.project_alternative_name) == 1)
        self.assertEquals(instance.project_alternative_name[0]['en'], update_info['project_alternative_name'])
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_multiple_url_field_with_previous_values(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_type'] = [
            choices.FUNDING_TYPE_CHOICES.EU_FUNDS
        ]
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.funding_type) == 1)
        self.assertEquals(instance.funding_type[0], choices.FUNDING_TYPE_CHOICES.EU_FUNDS)
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_type': choices.FUNDING_TYPE_CHOICES.NATIONAL_FUNDS + "|" + choices.FUNDING_TYPE_CHOICES.OTHER
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.funding_type) == 3)
        self.assertEquals(
            sorted(instance.funding_type),
            sorted([
                choices.FUNDING_TYPE_CHOICES.EU_FUNDS,
                choices.FUNDING_TYPE_CHOICES.NATIONAL_FUNDS,
                choices.FUNDING_TYPE_CHOICES.OTHER
            ])
        )
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_multiple_url_field_with_no_previous_values(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_type'] = None
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_type': choices.FUNDING_TYPE_CHOICES.NATIONAL_FUNDS + " | " + choices.FUNDING_TYPE_CHOICES.OTHER
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.funding_type) == 2)
        self.assertEquals(
            sorted(instance.funding_type),
            sorted([
                choices.FUNDING_TYPE_CHOICES.NATIONAL_FUNDS,
                choices.FUNDING_TYPE_CHOICES.OTHER
            ])
        )
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_multiple_url_field_with_wrong_cv_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_type'] = None
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_type': choices.FUNDING_TYPE_CHOICES.NATIONAL_FUNDS + " | Not a valid CV value "
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(instance.funding_type is None)
        # Check report
        self.assertEquals(str(report[2]['funding_type'][1]),
                          "[ErrorDetail(string='\"Not a valid CV value\" is not a valid choice.', code='invalid_choice')]"
                          )

    def test_add_multiple_url_field_with_wrong_url_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['website'] = ['http://www.google.com']
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.website) == 1)
        self.assertEquals(instance.website[0], 'http://www.google.com')
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'website': "www.novalidurl.com"
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.website) == 1)
        self.assertEquals(
            sorted(instance.website),
            sorted(['http://www.google.com'])
        )
        # Check report
        self.assertEquals(str(report[2]['website'][1]),
                          "[ErrorDetail(string='Enter a valid URL.', code='invalid')]"
                          )

    def test_add_multiple_char_field_with_previous_values(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_country'] = ['FR']
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.funding_country) == 1)
        self.assertEquals(instance.funding_country[0], 'FR')
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_country': 'GB | GR'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.funding_country) == 3)
        self.assertEquals(
            sorted(instance.funding_country),
            sorted(['FR', 'GB', 'GR'])
        )
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_multiple_char_field_with_no_previous_values(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_country'] = None
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(instance.funding_country is None)
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_country': 'GB | GR'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.funding_country) == 2)
        self.assertEquals(
            sorted(instance.funding_country),
            sorted(['GB', 'GR'])
        )
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_multiple_char_field_with_wrong_cv_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_country'] = None
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(instance.funding_country is None)
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_country': 'Not a country code | GR'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(instance.funding_country is None)
        # Check report
        self.assertEquals(str(report[2]['funding_country'][0]),
                          "[ErrorDetail(string='\"Not a country code\" is not a valid choice.', code='invalid_choice')]"
                          )

    def test_replace_url_field_with_no_previous_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['logo'] = ''
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'logo': 'http://www.google.com\t'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(
            instance.logo, 'http://www.google.com'
        )
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_replace_url_field_with_previous_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['logo'] = 'http://www.initialvalue.com'
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'logo': 'http://www.newvalue.com'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.logo, 'http://www.newvalue.com')
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_replace_url_field_with_wrong_url_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['logo'] = 'http://www.initialvalue.com'
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.logo, 'http://www.initialvalue.com')
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'logo': 'www.newvalue.com'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.logo, 'http://www.initialvalue.com')
        # Check report
        self.assertEquals(report[2], {'logo': ['Enter a valid URL.']})

    def test_header_entity_pk_not_found_message(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'pk': instance.pk,
            'logo': 'www.newvalue.com'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check report
        self.assertEquals(report[2], "No entity pk header was found in line 2")

    def test_entity_id_not_int_message(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': 'lala',
            'pk': instance.pk,
            'logo': 'www.newvalue.com'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check report
        self.assertEquals(report[2], f"Field 'id' expected a number but got \'{update_info['entity_pk']}\'.")

    def test_described_entity_not_found_message(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': 10000,
            'pk': instance.pk,
            'logo': 'www.newvalue.com'
        }
        report = curate_model([update_info], 'registry.Project')
        self.assertEquals(report[2],
        f"{r_m.DescribedEntity._meta.verbose_name} with pk {update_info['entity_pk']} does not exist")

    def test_header_id_not_found_message(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'logo': 'www.newvalue.com'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check report
        self.assertEquals(report[2], "No pk header was found in line 2")

    def test_id_not_int_message(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': 'lala',
            'logo': 'www.newvalue.com'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check report
        self.assertEquals(report[2], f"Field 'id' expected a number but got \'{update_info['pk']}\'.")

    def test_model_not_found_message(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': 10000,
            'logo': 'www.newvalue.com'
        }
        report = curate_model([update_info], 'registry.Project')
        self.assertEquals(report[2], f"{r_m.Project._meta.verbose_name} with pk {update_info['pk']} does not exist")

    def test_field_name_not_exist_message(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'field_name_that_does_not_exists': 'www.newvalue.com'
        }
        report = curate_model([update_info], 'registry.Project')
        self.assertEquals(report[2], {
            'field_name_that_does_not_exists': 'Field with name field_name_that_does_not_exists does not exist in Project'}
                          )

    def test_add_many_to_many_field_with_string_pk(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funder': 'Not a country codeR'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check report
        self.assertEquals(report[2],
                          {
                              'funder': f"Field 'id' expected a number but got '{update_info['funder']}'."
                          }
                          )

    def test_add_many_to_many_field_with_valid_pk(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        init_funders_len = len(instance.funder.all())
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funder': str(instance.coordinator.pk)
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(len(instance.funder.all()), init_funders_len + 1)
        self.assertTrue(instance.coordinator in instance.funder.all())
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_many_to_many_field_with_invalid_pk(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        init_funders_len = len(instance.funder.all())
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funder': str(10000)
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(len(instance.funder.all()), init_funders_len)
        # Check report
        self.assertEquals(report[2]['funder'],
                          f"Not all provided ids ['10000'] where retrieved. Verify that ids exist for Actor"
                          )

    def test_add_one_to_one_field_with_valid_pk(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        new_cost_data = {
            "amount": "1000",
            "currency": CURRENCY_CHOICES.OTHER
        }
        new_cost_serializer = r_s.CostSerializer(data=new_cost_data)
        self.assertTrue(new_cost_serializer.is_valid())
        new_cost_instance = new_cost_serializer.save()
        old_cost_instance = instance.cost.pk
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'cost': str(new_cost_instance.pk)
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.cost, new_cost_instance)
        self.assertNotEquals(instance.cost, old_cost_instance)
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_one_to_one_field_with_invalid_pk(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        old_cost_instance = instance.cost.pk
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'cost': str(10000)
        }
        report = curate_model([update_info], 'registry.Project')
        # Check enrichments
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertNotEquals(instance.cost.pk, update_info['cost'])
        self.assertEquals(instance.cost.pk, old_cost_instance)
        # Check report
        self.assertEquals(report[2]['cost'],
                          f"{r_m.Cost._meta.verbose_name} with pk {update_info['cost']} does not exist")

    def test_add_one_to_many_field_with_string_pk(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'cost': 'Not a country codeR'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check report
        self.assertEquals(report[2]['cost'],
                          f"Field 'id' expected a number but got '{update_info['cost']}'."
                          )

    def test_add_boolean_field_valid(self):
        data = copy.deepcopy(self.json_data_organization)
        serializer = r_s.OrganizationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'startup': 'false\t'
        }
        report = curate_model([update_info], 'registry.Organization')
        # Check enrichments
        instance = r_m.Organization.objects.get(pk=instance.pk)
        self.assertEquals(
            instance.startup, False
        )
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Organization._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_boolean_field_invalid(self):
        data = copy.deepcopy(self.json_data_organization)
        serializer = r_s.OrganizationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'startup': 'my false\t'
        }
        report = curate_model([update_info], 'registry.Organization')
        # Check enrichments
        instance = r_m.Organization.objects.get(pk=instance.pk)
        self.assertEquals(
            instance.startup, None
        )
        # Check report
        self.assertEquals(str(report[2]['startup']),
                          "[ErrorDetail(string='Must be a valid boolean.', code='invalid')]"
                          )

    def test_add_date_field_correct_value(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_start_date': '2022-10-10'
        }
        report = curate_model([update_info], 'registry.Project')
        instance = r_m.Project.objects.get(pk=instance.pk)
        # Check report
        self.assertEquals(str(instance.project_start_date), update_info['project_start_date'])
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_date_field_wrong_value(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_start_date': '10-10-2022'
        }
        report = curate_model([update_info], 'registry.Project')
        # Check report
        self.assertEquals(str(report[2]['project_start_date']),
                          "[ErrorDetail(string='Date has wrong format. Use one of these formats instead: YYYY-MM-DD.', code='invalid')]"
                          )

    def test_add_choice_field_correct_cv_value(self):
        data = copy.deepcopy(self.json_data_organization)
        serializer = r_s.OrganizationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'division_category': choices.DIVISION_CATEGORY_CHOICES.OTHER
        }
        report = curate_model([update_info], 'registry.Organization')
        # Check enrichments
        instance = r_m.Organization.objects.get(pk=instance.pk)
        self.assertEquals(instance.division_category, update_info['division_category'])
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Organization._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_add_choice_field_wrong_cv_value(self):
        data = copy.deepcopy(self.json_data_organization)
        serializer = r_s.OrganizationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'division_category': 'invalid choice'
        }
        report = curate_model([update_info], 'registry.Organization')
        # Check enrichments
        instance = r_m.Organization.objects.get(pk=instance.pk)
        # Check enrichments
        self.assertNotEquals(instance.division_category, update_info['division_category'])
        # Check report
        self.assertEquals(str(report[2]['division_category']),
                          "[ErrorDetail(string='\"invalid choice\" is not a valid choice.', code='invalid_choice')]"
                          )

    def test_remove_boolean_field_valid(self):
        data = copy.deepcopy(self.json_data_organization)
        data['startup'] = False
        serializer = r_s.OrganizationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'startup': str(data['startup']) + '\t'
        }
        report = curate_model([update_info], 'registry.Organization', remove=True)
        # Check curation
        instance = r_m.Organization.objects.get(pk=instance.pk)
        self.assertNotEquals(
            instance.startup, data['startup']
        )
        self.assertEquals(
            instance.startup, None
        )
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Organization._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_boolean_field_invalid(self):
        data = copy.deepcopy(self.json_data_organization)
        data['startup'] = False
        serializer = r_s.OrganizationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'startup': str(not data['startup']) + '\t'
        }
        report = curate_model([update_info], 'registry.Organization', remove=True)
        # Check curation
        instance = r_m.Organization.objects.get(pk=instance.pk)
        self.assertEquals(
            instance.startup, data['startup']
        )
        self.assertNotEquals(
            instance.startup, update_info['startup']
        )
        # Check report
        self.assertEquals(report[2]['startup'],
                          f"Request to delete {update_info['startup']} from field with name startup, "
                          f"but that value does not exist. Field has value {instance.startup}")

    def test_remove_choice_field_correct_cv_value(self):
        data = copy.deepcopy(self.json_data_organization)
        data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.OTHER
        serializer = r_s.OrganizationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'division_category': data['division_category']
        }
        report = curate_model([update_info], 'registry.Organization', remove=True)
        # Check curation
        instance = r_m.Organization.objects.get(pk=instance.pk)
        self.assertNotEquals(instance.division_category, update_info['division_category'])
        self.assertEquals(instance.division_category, None)
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Organization._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_choice_field_incorrect_cv_value(self):
        data = copy.deepcopy(self.json_data_organization)
        data['division_category'] = choices.DIVISION_CATEGORY_CHOICES.OTHER
        serializer = r_s.OrganizationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'division_category': choices.DIVISION_CATEGORY_CHOICES.UNSPECIFIED
        }
        report = curate_model([update_info], 'registry.Organization', remove=True)
        # Check curation
        instance = r_m.Organization.objects.get(pk=instance.pk)
        self.assertEquals(instance.division_category, data['division_category'])
        self.assertNotEquals(instance.division_category, update_info['division_category'])
        self.assertNotEquals(instance.division_category, None)
        # Check report
        self.assertEquals(report[2]['division_category'],
                          f"Request to delete {update_info['division_category']} from field with name division_category, "
                          f"but that value does not exist. Field has value {instance.division_category}")

    def test_remove_date_field_correct_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_start_date'] = '2022-10-10'
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_start_date': data['project_start_date']
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        instance = r_m.Project.objects.get(pk=instance.pk)
        # Check report
        self.assertNotEquals(str(instance.project_start_date), update_info['project_start_date'])
        self.assertEquals(instance.project_start_date, None)
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_date_field_incorrect_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_start_date'] = '2022-10-10'
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_start_date': '2022-10-30'
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        instance = r_m.Project.objects.get(pk=instance.pk)
        # Check report
        self.assertEquals(str(instance.project_start_date), data['project_start_date'])
        self.assertNotEquals(str(instance.project_start_date), update_info['project_start_date'])
        self.assertNotEquals(instance.project_start_date, None)
        self.assertEquals(report[2]['project_start_date'],
                          f"Request to delete {update_info['project_start_date']} from field with name project_start_date, "
                          f"but that value does not exist. Field has value {instance.project_start_date}")

    def test_remove_url_field(self):
        data = copy.deepcopy(self.json_data_project)
        data['logo'] = 'http://www.initialvalue.com'
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'logo': data['logo']
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check value
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertNotEquals(instance.logo, update_info['logo'])
        self.assertEquals(instance.logo, '')
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_url_field_incorrect_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['logo'] = 'http://www.initialvalue.com'
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'logo': 'http://www.initialvalue.com2'
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check value
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertNotEquals(instance.logo, update_info['logo'])
        self.assertEquals(instance.logo, data['logo'])
        self.assertNotEquals(instance.logo, '')
        # Check report
        self.assertEquals(report[2]['logo'],
                          f"Request to delete {update_info['logo']} from field with name logo, "
                          f"but that value does not exist. Field has value {instance.logo}")

    def test_remove_char_field(self):
        data = copy.deepcopy(self.json_data_project)
        data['grant_number'] = 'Grant number'
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'grant_number': data['grant_number']
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check value
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertNotEquals(instance.grant_number, update_info['grant_number'])
        self.assertEquals(instance.grant_number, '')
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_char_field_incorrect_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['grant_number'] = 'Grant number'
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'grant_number': data['grant_number'] + 'fsfws'
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check value
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertNotEquals(instance.grant_number, update_info['grant_number'])
        self.assertNotEquals(instance.grant_number, '')
        self.assertEquals(instance.grant_number, data['grant_number'])
        # Check report
        self.assertEquals(report[2]['grant_number'],
                          f"Request to delete {update_info['grant_number']} from field with name grant_number, "
                          f"but that value does not exist. Field has value {instance.grant_number}")

    def test_remove_multilingual_obligatory_en(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_name'] = {
            'en': 'Initial project name',
            'el': 'Αρχικό όνομα προτζεκτ'
        }
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.project_name['en'], data['project_name']['en'])
        self.assertEquals(instance.project_name['el'], data['project_name']['el'])
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_name': data['project_name']['en']
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.project_name['en'], data['project_name']['en'])
        self.assertEquals(instance.project_name['el'], data['project_name']['el'])
        # Check report
        self.assertEquals(str(report[2]['project_name']),
                          "[ErrorDetail(string='This field may not be null.', code='null')]")

    def test_remove_multilingual_optional_correct_en(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_short_name'] = {
            'en': 'Project short name',
            'el': 'Σύντομο όνομα έργου',
        }
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_short_name': data['project_short_name']['en']
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.project_short_name, None)
        self.assertNotEquals(instance.project_short_name, data['project_short_name'])
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_multilingual_optional_incorrect_en(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_short_name'] = {
            'en': 'Project short name',
            'el': 'Σύντομο όνομα έργου',
        }
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_short_name': data['project_short_name']['el']
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertNotEquals(instance.project_short_name, None)
        self.assertEquals(instance.project_short_name, data['project_short_name'])
        self.assertNotEquals(instance.project_short_name['en'], update_info['project_short_name'])
        # Check report
        self.assertEquals(report[2]['project_short_name'],
                          f"Request to delete {update_info['project_short_name']} from field with name project_short_name, "
                          f"but that value with en language does not exist. Field has value {instance.project_short_name}")

    def test_remove_multilingual_optional_none_incorrect_en(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_short_name'] = None
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_short_name': 'Project short name'
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.project_short_name, None)
        self.assertEquals(instance.project_short_name, data['project_short_name'])
        self.assertNotEquals(instance.project_short_name, update_info['project_short_name'])
        # Check report
        self.assertEquals(report[2]['project_short_name'],
                          f"Request to delete {update_info['project_short_name']} from field with name project_short_name, "
                          f"but that value with en language does not exist. Field has value {instance.project_short_name}")

    def test_remove_multiple_char_field_with_correct_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_country'] = ['FR', 'GR']
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.funding_country) == 2)
        self.assertEquals(
            sorted(instance.funding_country),
            sorted(data['funding_country'])
        )
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_country': data['funding_country'][1]
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.funding_country) == 1)
        self.assertEquals(
            instance.funding_country,
            [data['funding_country'][0]]
        )
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_multiple_char_field_with_all_correct_values(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_country'] = ['FR', 'GR']
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.funding_country) == 2)
        self.assertEquals(
            sorted(instance.funding_country),
            sorted(data['funding_country'])
        )
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_country': data['funding_country'][0] + "|\t" + data['funding_country'][1]
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.funding_country, None)
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_multiple_char_field_with_incorrect_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_country'] = ['FR', 'GR']
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.funding_country) == 2)
        self.assertEquals(
            sorted(instance.funding_country),
            sorted(data['funding_country'])
        )
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_country': 'IT'
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.funding_country) == 2)
        self.assertEquals(
            sorted(instance.funding_country),
            sorted(data['funding_country'])
        )
        # Check report
        self.assertEquals(report[2]['funding_country'],
                          f"[\"Request to delete {update_info['funding_country']} from field with name funding_country, "
                          f"but that value does not exist. Field has value {data['funding_country']}\"]"
                          f""
                          )

    def test_remove_multiple_url_field_with_correct_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_type'] = [
            choices.FUNDING_TYPE_CHOICES.NATIONAL_FUNDS,
            choices.FUNDING_TYPE_CHOICES.OTHER
        ]
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.funding_type) == 2)
        self.assertEquals(
            sorted(instance.funding_type),
            sorted(data['funding_type'])
        )
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_type': choices.FUNDING_TYPE_CHOICES.OTHER
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.funding_type) == 1)
        self.assertEquals(
            instance.funding_type,
            [data['funding_type'][0]]
        )
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_multiple_url_field_with_all_correct_values(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_type'] = [
            choices.FUNDING_TYPE_CHOICES.NATIONAL_FUNDS,
            choices.FUNDING_TYPE_CHOICES.OTHER
        ]
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.funding_type) == 2)
        self.assertEquals(
            sorted(instance.funding_type),
            sorted(data['funding_type'])
        )
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_type': data['funding_type'][0] + "|\t" + data['funding_type'][1]
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.funding_type, None)
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_multiple_url_field_with_incorrect_value(self):
        data = copy.deepcopy(self.json_data_project)
        data['funding_type'] = [
            choices.FUNDING_TYPE_CHOICES.NATIONAL_FUNDS,
            choices.FUNDING_TYPE_CHOICES.OTHER
        ]
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.funding_type) == 2)
        self.assertEquals(
            sorted(instance.funding_type),
            sorted(data['funding_type'])
        )
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funding_type': choices.FUNDING_TYPE_CHOICES.OWN_FUNDS
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.funding_type) == 2)
        self.assertEquals(
            sorted(instance.funding_type),
            sorted(data['funding_type'])
        )
        # Check report
        self.assertEquals(report[2]['funding_type'],
                          f"[\"Request to delete {update_info['funding_type']} from field with name funding_type, "
                          f"but that value does not exist. Field has value {data['funding_type']}\"]"
                          f""
                          )

    def test_remove_multiple_multilingual_optional_correct_en_remove_one(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_alternative_name'] = [{
            'en': 'Alternative project name',
            'el': 'Εναλλακτικό όνομα έργου'
        },
            {
                'en': 'Second alternative project name',
                'el': 'Δεύτερο εναλλακτικό όνομα έργου'
            }
        ]
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.project_alternative_name) == 2)
        self.assertEquals(instance.project_alternative_name[0]['en'], data['project_alternative_name'][0]['en'])
        self.assertEquals(instance.project_alternative_name[0]['el'], data['project_alternative_name'][0]['el'])
        self.assertEquals(instance.project_alternative_name[1]['en'], data['project_alternative_name'][1]['en'])
        self.assertEquals(instance.project_alternative_name[1]['el'], data['project_alternative_name'][1]['el'])
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_alternative_name': data['project_alternative_name'][0]['en']
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.project_alternative_name) == 1)
        self.assertEquals(instance.project_alternative_name[0]['en'], data['project_alternative_name'][1]['en'])
        self.assertEquals(instance.project_alternative_name[0]['el'], data['project_alternative_name'][1]['el'])

        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_multiple_multilingual_optional_correct_en_remove_all(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_alternative_name'] = [{
            'en': 'Alternative project name',
            'el': 'Εναλλακτικό όνομα έργου'
        },
            {
                'en': 'Second alternative project name',
                'el': 'Δεύτερο εναλλακτικό όνομα έργου'
            }
        ]
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.project_alternative_name) == 2)
        self.assertEquals(instance.project_alternative_name[0]['en'], data['project_alternative_name'][0]['en'])
        self.assertEquals(instance.project_alternative_name[0]['el'], data['project_alternative_name'][0]['el'])
        self.assertEquals(instance.project_alternative_name[1]['en'], data['project_alternative_name'][1]['en'])
        self.assertEquals(instance.project_alternative_name[1]['el'], data['project_alternative_name'][1]['el'])
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_alternative_name': data['project_alternative_name'][0]['en'] +
                                        '|' + data['project_alternative_name'][1]['en']
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(instance.project_alternative_name is None)

        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_multiple_multilingual_optional_incorrect_en_remove(self):
        data = copy.deepcopy(self.json_data_project)
        data['project_alternative_name'] = [{
            'en': 'Alternative project name',
            'el': 'Εναλλακτικό όνομα έργου'
        },
            {
                'en': 'Second alternative project name',
                'el': 'Δεύτερο εναλλακτικό όνομα έργου'
            }
        ]
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(len(instance.project_alternative_name) == 2)
        self.assertEquals(instance.project_alternative_name[0]['en'], data['project_alternative_name'][0]['en'])
        self.assertEquals(instance.project_alternative_name[0]['el'], data['project_alternative_name'][0]['el'])
        self.assertEquals(instance.project_alternative_name[1]['en'], data['project_alternative_name'][1]['en'])
        self.assertEquals(instance.project_alternative_name[1]['el'], data['project_alternative_name'][1]['el'])
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'project_alternative_name': data['project_alternative_name'][0]['en'] + "foo"
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertTrue(len(instance.project_alternative_name) == 2)
        self.assertEquals(instance.project_alternative_name[0]['en'], data['project_alternative_name'][0]['en'])
        self.assertEquals(instance.project_alternative_name[0]['el'], data['project_alternative_name'][0]['el'])
        self.assertEquals(instance.project_alternative_name[1]['en'], data['project_alternative_name'][1]['en'])
        self.assertEquals(instance.project_alternative_name[1]['el'], data['project_alternative_name'][1]['el'])
        # Check report
        self.assertEquals(report[2]['project_alternative_name'],
                          f"[\"Request to delete {update_info['project_alternative_name']} from field with name "
                          f"project_alternative_name, but that value with en language does not exist. "
                          f"Field has value {instance.project_alternative_name}\"]"
                          )

    def test_remove_one_to_one_field_with_valid_pk(self):
        data = copy.deepcopy(self.json_data_project)
        data['cost'] = {
            "amount": "1000",
            "currency": CURRENCY_CHOICES.OTHER
        }
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'cost': str(instance.cost.pk)
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.cost, None)
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_one_to_one_field_with_invalid_pk(self):
        data = copy.deepcopy(self.json_data_project)
        data['cost'] = {
            "amount": "1000",
            "currency": CURRENCY_CHOICES.OTHER
        }
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        instance_cost = instance.cost
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'cost': str(1000000)
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.cost, instance_cost)
        # Check report
        self.assertEquals(report[2]['cost'],
                          f"{r_m.Cost._meta.verbose_name} with pk {update_info['cost']} does not exist"
                          )

    def test_remove_one_to_one_field_with_none(self):
        data = copy.deepcopy(self.json_data_project)
        data['cost'] = None
        cost_data = {
            "amount": "1000",
            "currency": CURRENCY_CHOICES.OTHER
        }
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        cost_serializer = r_s.CostSerializer(data=cost_data)
        self.assertTrue(cost_serializer.is_valid())
        cost_instance = cost_serializer.save()
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'cost': cost_instance.pk
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.cost, None)
        # Check report
        self.assertEquals(report[2]['cost'],
                          f"Request to delete {update_info['cost']} from field with name cost, "
                          f"but that value does not exist. Field has value None")

    def test_remove_one_to_one_field_with_string(self):
        data = copy.deepcopy(self.json_data_project)
        data['cost'] = {
            "amount": "1000",
            "currency": CURRENCY_CHOICES.OTHER
        }
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        cost_instance = instance.cost
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'cost': str(instance.cost.pk) + 'fsfs'
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(instance.cost, cost_instance)
        # Check report
        self.assertEquals(report[2]['cost'],
                          f"Request to delete {update_info['cost']} from field with name cost, "
                          f'but that value is not an integer.')

    def test_remove_many_to_many_field_with_valid_pk(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        init_funders = [funder.pk for funder in instance.funder.all()]
        funder_to_remove = r_m.Actor.objects.get(pk=init_funders[0])
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funder': str(funder_to_remove.pk)
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(len(instance.funder.all()), len(init_funders) - 1)
        self.assertTrue(funder_to_remove not in instance.funder.all())
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_many_to_many_field_with_valid_pk_all(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        init_funders = [funder.pk for funder in instance.funder.all()]
        funders_to_remove = []
        for pk in init_funders:
            funders_to_remove.append(r_m.Actor.objects.get(pk=init_funders[0]))
        remove_funders_pk = ''
        for i, pk in enumerate(init_funders):
            remove_funders_pk += str(pk)
            if i < len(init_funders) - 1:
                remove_funders_pk += '|'

        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funder': remove_funders_pk
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(len(instance.funder.all()), 0)
        for funder in funders_to_remove:
            self.assertTrue(funder not in instance.funder.all())
        # Check report
        self.assertEquals(report[2],
                          f"Curation of {r_m.Project._meta.verbose_name} with pk {update_info['pk']} was successful")

    def test_remove_many_to_many_field_with_string_pk(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        init_funders = [funder.pk for funder in instance.funder.all()]
        funder_to_remove = r_m.Actor.objects.get(pk=init_funders[0])
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funder': str(funder_to_remove.pk) + 'foo'
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(len(instance.funder.all()), len(init_funders))
        self.assertTrue(funder_to_remove in instance.funder.all())
        # Check report
        self.assertEquals(report[2]['funder'],
                          f"['Request to delete {update_info['funder']} from field with name funder, "
                          f"but that value is not an integer.']")

    def test_remove_many_to_many_field_with_invalid_pk(self):
        data = copy.deepcopy(self.json_data_project)
        serializer = r_s.ProjectSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        init_funders = [funder.pk for funder in instance.funder.all()]
        # Create info for update
        update_info = {
            'entity_pk': instance.pk,
            'pk': instance.pk,
            'funder': str(100000)
        }
        report = curate_model([update_info], 'registry.Project', remove=True)
        # Check curation
        instance = r_m.Project.objects.get(pk=instance.pk)
        self.assertEquals(len(instance.funder.all()), len(init_funders))
        # Check report
        funders = r_m.Actor.objects.filter(pk__in=init_funders)
        funders_serializer = r_s.ActorSerializer(instance=funders, many=True)
        self.assertEquals(report[2]['funder'],
                          f"[\"Request to delete {update_info['funder']} from field with name funder, "
                          f"but that value does not exist. Field has value {funders_serializer.data}\"]")
