import copy
import json
import logging

from django.conf import settings
from django.contrib.auth import get_user_model

from test_utils import SerializerTestCase

from registry.models import Project
from registry.registry_identifier import get_registry_identifier
from registry.retrieval_mechanisms import (
    retrieve_by_non_registry_identifier, retrieve_document_by_title,
    retrieve_person_by_full_name, retrieve_model_by_organization_name,
    retrieve_lr_by_resource_name,
    retrieve_project_by_grant_number_or_name,
    retrieve_licence_by_licence_terms_and_name,
    retrieve_licence_terms_given_only_name,
    meet_conditions_to_retrieve_single_entity,
    meet_conditions_to_retrieve_pk_of_model
)
from registry.serializers import (
    MetadataRecordSerializer,
    GenericProjectSerializer, GenericPersonSerializer,
    GenericLicenceTermsSerializer, GenericOrganizationSerializer,
    GenericLanguageResourceSerializer, GenericDocumentSerializer,
    DomainSerializer, TextTypeSerializer, AudioGenreSerializer,
    SpeechGenreSerializer, TextGenreSerializer, AccessRightsStatementSerializer,
    GenericGroupSerializer,
    GenericRepositorySerializer, SubjectSerializer, VideoGenreSerializer,
    ImageGenreSerializer
)

LOGGER = logging.getLogger(__name__)


class TestProjectRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for Project
        user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        cls.serializer_context = {'request_user': user}
        json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.instance.management_object.status = 'p'
            cls.instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        non_p_data = copy.deepcopy(json_data)
        non_p_data['described_entity']['project_name'] = {'en': 'non published'}
        non_p_data['described_entity']['project_short_name'] = {}
        non_p_data['described_entity']['project_identifier'] = [
          {
            "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
            "value": "this_is_elg_value_idnp"
          },
          {
            "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
            "value": "doi_value_idnp"
          }
        ]
        non_p_data['described_entity']['website'] = [
          "https://elitr.eu/long/slug/testnp/",
          "http://www.secondurlnp.eu/"
        ]
        non_p_data['described_entity']['grant_number'] = "82546023"
        cls.non_p_serializer = MetadataRecordSerializer(data=non_p_data)
        if cls.non_p_serializer.is_valid():
            cls.non_p_instance = cls.non_p_serializer.save()
            cls.non_p_instance.management_object.curator = user
            cls.non_p_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.non_p_serializer.errors))
        LOGGER.info('Setup has finished')

    def test_project_retrieve_by_registry_identifier_p(self):
        registry_identifier_value = get_registry_identifier(self.instance)
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project['project_identifier'] = list()
        project_id = dict()
        project_id['project_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        project_id['value'] = registry_identifier_value
        json_data_generic_project['project_identifier'].append(project_id)
        json_data_generic_project['website'] = ['http://www.projet-website.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_registry_identifier_non_p(self):
        registry_identifier_value = get_registry_identifier(self.non_p_instance)
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored again'}
        json_data_generic_project['project_identifier'] = list()
        project_id = dict()
        project_id['project_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        project_id['value'] = registry_identifier_value
        json_data_generic_project['project_identifier'].append(project_id)
        json_data_generic_project['website'] = ['http://www.projet-website.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_non_registry_identifier_p(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project['project_identifier'] = list()
        project_id = dict()
        project_id['project_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/other"
        project_id['value'] = "doi_value_id"
        project_id_2 = dict()
        project_id_2['project_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/cordis'
        project_id_2['value'] = "doi_value_id_cord"
        json_data_generic_project['project_identifier'].append(project_id_2)
        json_data_generic_project['project_identifier'].append(project_id)
        json_data_generic_project['website'] = ['http://www.projet-website.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_non_registry_identifier_non_p(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored again again'}
        json_data_generic_project['project_identifier'] = list()
        project_id = dict()
        project_id['project_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/other"
        project_id['value'] = "doi_value_idnp"
        project_id_2 = dict()
        project_id_2['project_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/cordis'
        project_id_2['value'] = "doi_value_id_cordnp"
        json_data_generic_project['project_identifier'].append(project_id_2)
        json_data_generic_project['project_identifier'].append(project_id)
        json_data_generic_project['website'] = ['http://www.projet-website.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieval_by_non_elg_identifier_fails(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project['project_identifier'] = list()
        project_id = dict()
        project_id['project_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/cordis'
        project_id['value'] = "doi_value_id_cord"
        json_data_generic_project['project_identifier'].append(project_id)
        json_data_generic_project['website'] = ['http://www.projet-website.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertFalse(project_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_any_identifier_fails_when_identifier_is_None(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project['project_identifier'] = None
        json_data_generic_project['website'] = ['http://www.projet-website.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertFalse(project_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_website_no_slash_p(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "https://elitr.eu/long/slug/test"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)
    
    def test_project_retrieve_by_website_no_slash_n_p(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "https://elitr.eu/long/slug/testnp"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_website_http_when_retrieved_instance_https(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "http://elitr.eu/long/slug/test"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_website_https_when_retrieved_instance_https(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "https://elitr.eu/long/slug/test"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_website_http_when_retrieved_instance_http(self):
        self.instance.described_entity.website = [
            "http://rando.rando/rando",
            "http://elitr.eu/long/slug/test"
        ]
        self.instance.described_entity.save()
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "http://elitr.eu/long/slug/test"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)
        self.instance.described_entity.website = [
            "https://elitr.eu/long/slug/test/",
            "http://www.secondurl.eu/"
        ]
        self.instance.described_entity.save()

    def test_project_retrieve_by_website_https_when_retrieved_instance_http(self):
        self.instance.described_entity.website = [
            "http://rando.rando/rando",
            "http://elitr.eu/long/slug/test"
        ]
        self.instance.described_entity.save()
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "https://elitr.eu/long/slug/test"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)
        self.instance.described_entity.website = [
            "https://elitr.eu/long/slug/test/",
            "http://www.secondurl.eu/"
        ]
        self.instance.described_entity.save()

    def test_project_retrieve_by_pk(self):
        json_data_generic_project = dict()
        json_data_generic_project['pk'] = self.instance.described_entity.pk
        json_data_generic_project['project_name'] = {"en": "rando project name"}
        json_data_generic_project['website'] = ["http://randomproj.com"]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
        project_retrieved_by_pk = GenericProjectSerializer(data=project_retrieved.data)
        if project_retrieved_by_pk.is_valid():
            project_retrieved_by_pk.save()
            self.assertEqual(project_retrieved_by_pk['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved_by_pk.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_pk_np(self):
        json_data_generic_project = dict()
        json_data_generic_project['pk'] = self.non_p_instance.described_entity.pk
        json_data_generic_project['project_name'] = {"en": "rando project name"}
        json_data_generic_project['website'] = ["http://randomproj.com"]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
        project_retrieved_by_pk = GenericProjectSerializer(data=project_retrieved.data)
        if project_retrieved_by_pk.is_valid():
            project_retrieved_by_pk.save()
            self.assertEqual(project_retrieved_by_pk['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved_by_pk.errors)
            self.assertTrue(False)
            
    def test_project_retrieve_by_website_with_slash(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "https://elitr.eu/long/slug/test/"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_website_with_multiple(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "https://elitr.eu/long/slug/test",
            "https://random.com"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_website_with_multiple_existing(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "https://elitr.eu/long/slug/test",
            "http://www.secondurl.eu/"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_website_when_both_exist_in_different_projects(self):
        json_file_new = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file_new.read()
        json_file_new.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['website'] = ["http://www.newurl.com"]
        json_data['described_entity']['project_name'] = {
            'en': 'new name one'}
        json_data['described_entity']['project_short_name'] = {
            'en': 'new short name'}
        json_data['described_entity']['grant_number'] = "12412124142"
        json_data['described_entity']['project_identifier'] = list()
        project_id = dict()
        project_id['project_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/cordis'
        project_id['value'] = "doi_value_id_cord_not_the_one_below"
        json_data['described_entity']['project_identifier'].append(project_id)
        serializer = MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
            instance.management_object.status = 'p'
            instance.management_object.save()
        else:
            LOGGER.info('first Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        json_file_new_2 = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file_new_2.read()
        json_file_new_2.close()
        json_data_2 = json.loads(json_str)
        json_data_2['described_entity']['website'] = ["https://elitr.eu/random",
                                                      "http://www.newurl.com",
                                                      "http://www.new.com",
                                                      "http://valid.com"]
        json_data_2['described_entity']['project_name'] = {
            'en': 'new proj nammme one hundred percent sure'}
        json_data_2['described_entity']['project_short_name'] = {
            'en': 'new short nammmme new new new'}
        json_data_2['described_entity']['grant_number'] = "124456506812"
        json_data_2['described_entity']['project_identifier'] = list()
        project_id_2 = dict()
        project_id_2['project_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/cordis'
        project_id_2['value'] = "doi_value_id_20cord_2_2_2_2"
        json_data_2['described_entity']['project_identifier'].append(project_id_2)
        serializer_2 = GenericProjectSerializer(data=json_data_2['described_entity'])
        if serializer_2.is_valid():
            instance_2 = serializer_2.save()
        else:
            LOGGER.info('second Errors {}'.format(serializer_2.errors))
        LOGGER.info('Setup has finished')
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = [
            "http://www.xrandomx.eu/",
            "http://www.newurl.com/",
            "http://www.newurl2.com/"
        ]
        json_data_generic_project['grant_number'] = ''
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertTrue(project_retrieved['pk'].value in [instance.described_entity.pk,
                                                              instance_2.pk])
        else:
            LOGGER.info('third errors {}'.format(project_retrieved.errors))
            self.assertTrue(False)

    def test_project_retrieve_by_website_only_retrieves_exact(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}

        json_data_generic_project[
            'website'] = [
            "https://elitr.eu/long/slug/"
        ]
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertFalse(project_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_website_when_website_is_None(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project[
            'website'] = None
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertFalse(project_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_grant_number(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        json_data_generic_project['grant_number'] = self.instance.described_entity.grant_number
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_grant_number_np(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.non_p_instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        json_data_generic_project['grant_number'] = self.non_p_instance.described_entity.grant_number
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_grant_number_fails_when_only_non_published_and_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        json_data_generic_project['grant_number'] = self.instance.described_entity.grant_number
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertFalse(project_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_project_retrieve_by_grant_number_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_proj_data = dict()
        gen_proj_data['project_name'] = self.instance.described_entity.project_name
        gen_proj_data['website'] = ['http://www.notimportant.org']
        gen_proj_data['grant_number'] = self.instance.described_entity.grant_number
        gen_proj = GenericProjectSerializer(
            data=gen_proj_data)
        gen_proj.is_valid()
        gen_proj_instance = gen_proj.save()
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        json_data_generic_project['grant_number'] = self.instance.described_entity.grant_number
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             gen_proj_instance.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_project_retrieve_by_grant_number_when_generic_and_published_prefers_published(self):
        gen_proj_data = dict()
        gen_proj_data['project_name'] = self.instance.described_entity.project_name
        gen_proj_data['website'] = ['http://www.notimportant.org']
        gen_proj_data['grant_number'] = self.instance.described_entity.grant_number
        gen_proj = GenericProjectSerializer(
            data=gen_proj_data)
        gen_proj.is_valid()
        gen_proj_instance = gen_proj.save()
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        json_data_generic_project['grant_number'] = self.instance.described_entity.grant_number
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_project_name(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_project_name_np(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.non_p_instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_name_fails_when_only_non_published_and_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertFalse(project_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_project_retrieve_by_name_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_proj_data = dict()
        gen_proj_data['project_name'] = self.instance.described_entity.project_name
        gen_proj_data['website'] = ['http://www.notimportant.org']
        gen_proj = GenericProjectSerializer(
            data=gen_proj_data)
        gen_proj.is_valid()
        gen_proj_instance = gen_proj.save()
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             gen_proj_instance.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_project_retrieve_by_name_when_generic_and_published_prefers_published(self):
        gen_proj_data = dict()
        gen_proj_data['project_name'] = self.instance.described_entity.project_name
        gen_proj_data['website'] = ['http://www.notimportant.org']
        gen_proj = GenericProjectSerializer(
            data=gen_proj_data)
        gen_proj.is_valid()
        gen_proj_instance = gen_proj.save()
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_retrieve_by_project_name_when_wrong_number(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = self.instance.described_entity.project_name
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        json_data_generic_project['grant_number'] = 'not a grant number for sure'
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertEqual(project_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_does_not_retrieve_by_grant_number_or_project_name_with_wrong_name_and_no_number(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {'en': 'title not in db for sure'}
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertFalse(project_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)

    def test_project_does_not_retrieve_by_grant_number_or_project_name_with_wrong_number_and_wrong_name(self):
        json_data_generic_project = dict()
        json_data_generic_project['grant_number'] = '825'
        json_data_generic_project['project_name'] = {'en': 'title not in db for sure'}
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        project_retrieved = GenericProjectSerializer(
            data=json_data_generic_project, context=self.serializer_context)
        if project_retrieved.is_valid():
            project_retrieved.save()
            self.assertFalse(project_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(project_retrieved.errors)
            self.assertTrue(False)


class TestPersonRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        cls.serializer_context = {'request_user': user}
        # From JSON data, create a metadata record for Person
        json_file = open('registry/tests/fixtures/person.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.instance.management_object.status = 'p'
            cls.instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')
        non_p_data = copy.deepcopy(json_data)
        non_p_data['described_entity']['surname'] = {'en': 'non published'}
        non_p_data['described_entity']['given_name'] = {'en': 'non published at all'}
        non_p_data['described_entity']['personal_identifier'] = [
          {
            "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
            "value": "https://orcid.org/0000-0002-4725-3094np"
          }
        ]
        non_p_data['described_entity']['email'] = [
          "np@email.com"
        ]
        cls.non_p_serializer = MetadataRecordSerializer(data=non_p_data)
        if cls.non_p_serializer.is_valid():
            cls.non_p_instance = cls.non_p_serializer.save()
            cls.non_p_instance.management_object.curator = user
            cls.non_p_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.non_p_serializer.errors))
        LOGGER.info('Setup has finished')

    def test_person_retrieve_by_registry_identifier(self):
        registry_identifier_value = get_registry_identifier(self.instance)
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['personal_identifier'] = list()
        person_id = dict()
        person_id['personal_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        person_id['value'] = registry_identifier_value
        json_data_generic_person['personal_identifier'].append(person_id)
        json_data_generic_person['email'] = ['person@email.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_registry_identifier_np(self):
        registry_identifier_value = get_registry_identifier(self.non_p_instance)
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['personal_identifier'] = list()
        person_id = dict()
        person_id['personal_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        person_id['value'] = registry_identifier_value
        json_data_generic_person['personal_identifier'].append(person_id)
        json_data_generic_person['email'] = ['person@email.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_non_registry_identifier(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['personal_identifier'] = list()
        person_id = dict()
        person_id['personal_identifier_scheme'] = "http://purl.org/spar/datacite/orcid"
        person_id['value'] = "0000-0001-5123-7890"
        person_id_2 = dict()
        person_id_2['personal_identifier_scheme'] = 'http://purl.org/spar/datacite/openid'
        person_id_2['value'] = "open_id_registry_value"
        json_data_generic_person['personal_identifier'].append(person_id)
        json_data_generic_person['personal_identifier'].append(person_id_2)
        json_data_generic_person['email'] = ['person@email.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_non_registry_identifier_np(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['personal_identifier'] = list()
        person_id = dict()
        person_id['personal_identifier_scheme'] = "http://purl.org/spar/datacite/orcid"
        person_id['value'] = "https://orcid.org/0000-0002-4725-3094np"
        person_id_2 = dict()
        person_id_2['personal_identifier_scheme'] = 'http://purl.org/spar/datacite/openid'
        person_id_2['value'] = "open_id_registry_value"
        json_data_generic_person['personal_identifier'].append(person_id)
        json_data_generic_person['personal_identifier'].append(person_id_2)
        json_data_generic_person['email'] = ['person@email.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_non_registry_identifier_fails(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['personal_identifier'] = list()
        person_id_2 = dict()
        person_id_2['personal_identifier_scheme'] = 'http://purl.org/spar/datacite/openid'
        person_id_2['value'] = "open_id_registry_value"
        json_data_generic_person['personal_identifier'].append(person_id_2)
        json_data_generic_person['email'] = ['person@email.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertFalse(person_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_any_identifier_fails_when_identifier_is_None(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['personal_identifier'] = None
        json_data_generic_person['email'] = ['person@email.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertFalse(person_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_email(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['email'] = self.instance.described_entity.email
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_email_np(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['email'] = self.non_p_instance.described_entity.email
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_email_fails_when_only_non_published_and_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['email'] = self.instance.described_entity.email
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertFalse(person_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_person_retrieve_by_email_passes_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_person_data = dict()
        gen_person_data['actor_type'] = 'Person'
        gen_person_data['surname'] = self.instance.described_entity.surname
        gen_person_data['given_name'] = self.instance.described_entity.given_name
        gen_person_data['email'] = self.instance.described_entity.email
        gen_person = GenericPersonSerializer(
            data=gen_person_data)
        gen_person.is_valid()
        gen_person_instance = gen_person.save()
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['email'] = self.instance.described_entity.email
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             gen_person_instance.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_person_retrieve_by_email_passes_when_generic_and_published_prefers_published(self):
        gen_person_data = dict()
        gen_person_data['actor_type'] = 'Person'
        gen_person_data['surname'] = self.instance.described_entity.surname
        gen_person_data['given_name'] = self.instance.described_entity.given_name
        gen_person_data['email'] = self.instance.described_entity.email
        gen_person = GenericPersonSerializer(
            data=gen_person_data)
        gen_person.is_valid()
        gen_person_instance = gen_person.save()
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['email'] = self.instance.described_entity.email
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_pk(self):
        json_data_generic_person = dict()
        json_data_generic_person['pk'] = self.instance.described_entity.pk
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['email'] = ['rando@emial.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
        person_retrieved_by_pk = GenericPersonSerializer(
            data=person_retrieved.data)
        if person_retrieved_by_pk.is_valid():
            person_retrieved_by_pk.save()
            self.assertEqual(person_retrieved_by_pk['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved_by_pk.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_pk_np(self):
        json_data_generic_person = dict()
        json_data_generic_person['pk'] = self.non_p_instance.described_entity.pk
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['email'] = ['rando@emiall.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
        person_retrieved_by_pk = GenericPersonSerializer(
            data=person_retrieved.data)
        if person_retrieved_by_pk.is_valid():
            person_retrieved_by_pk.save()
            self.assertEqual(person_retrieved_by_pk['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved_by_pk.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_email_fails_when_email_is_None(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['given_name'] = {
            'en': 'Person given name to be ignored'}
        json_data_generic_person['email'] = None
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertFalse(person_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_full_name(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = self.instance.described_entity.surname
        json_data_generic_person['given_name'] = self.instance.described_entity.given_name
        json_data_generic_person['email'] = ['tobe@ignored.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)
    
    def test_person_retrieve_by_full_name_np(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = self.non_p_instance.described_entity.surname
        json_data_generic_person['given_name'] = self.non_p_instance.described_entity.given_name
        json_data_generic_person['email'] = ['tobe@ignored.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_retrieve_by_full_name_fails_when_only_non_published_with_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = self.instance.described_entity.surname
        json_data_generic_person['given_name'] = self.instance.described_entity.given_name
        json_data_generic_person['email'] = ['tobe@ignored.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertFalse(person_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_person_retrieve_by_full_name_passes_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_person_data = dict()
        gen_person_data['actor_type'] = 'Person'
        gen_person_data['surname'] = self.instance.described_entity.surname
        gen_person_data['given_name'] = self.instance.described_entity.given_name
        gen_person_data['email'] = self.instance.described_entity.email
        gen_person = GenericPersonSerializer(
            data=gen_person_data)
        gen_person.is_valid()
        gen_person_instance = gen_person.save()
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = self.instance.described_entity.surname
        json_data_generic_person['given_name'] = self.instance.described_entity.given_name
        json_data_generic_person['email'] = ['tobe@ignored.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             gen_person_instance.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_person_retrieve_by_full_name_passes_when_generic_and_published_prefers_published(self):
        gen_person_data = dict()
        gen_person_data['actor_type'] = 'Person'
        gen_person_data['surname'] = self.instance.described_entity.surname
        gen_person_data['given_name'] = self.instance.described_entity.given_name
        gen_person_data['email'] = self.instance.described_entity.email
        gen_person = GenericPersonSerializer(
            data=gen_person_data)
        gen_person.is_valid()
        gen_person_instance = gen_person.save()
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = self.instance.described_entity.surname
        json_data_generic_person['given_name'] = self.instance.described_entity.given_name
        json_data_generic_person['email'] = ['tobe@ignored.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertEqual(person_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_does_not_retrieve_by_full_name_with_wrong_surname(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {'en': 'Voukoutis'}
        json_data_generic_person['given_name'] = self.instance.described_entity.given_name
        json_data_generic_person['email'] = ['tobe@ignored.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertFalse(person_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_does_not_retrieve_by_full_name_with_wrong_given_name(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = self.instance.described_entity.surname
        json_data_generic_person['given_name'] = {'en': 'Leon'}
        json_data_generic_person['email'] = ['tobe@ignored.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertFalse(person_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)

    def test_person_does_not_retrieve_by_full_name_with_both_wrong_names(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {'en': 'Insert Random Surname'}
        json_data_generic_person['given_name'] = {'en': 'Leon'}
        json_data_generic_person['email'] = ['tobe@ignored.com']
        person_retrieved = GenericPersonSerializer(
            data=json_data_generic_person, context=self.serializer_context)
        if person_retrieved.is_valid():
            person_retrieved.save()
            self.assertFalse(person_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(person_retrieved.errors)
            self.assertTrue(False)


class TestLicenceTermsRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        cls.serializer_context = {'request_user': user}
        # From JSON data, create a metadata record for LicenceTerms
        json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()
        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.instance.management_object.status = 'p'
            cls.instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')
        non_p_data = copy.deepcopy(json_data)
        non_p_data['described_entity']['licence_terms_name'] = {
          "en": "BSD Zero Clause License np"
        }
        non_p_data['described_entity']['licence_terms_short_name'] = {
          "en": "0BSD np"
        }
        non_p_data['described_entity']['licence_terms_url'] = \
            ['http://landley.net/toybox/licensenp.html']
        non_p_data['described_entity']['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        non_p_data['described_entity']['licence_identifier'] = [
          {
            "value": "0BSDnp",
            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX"
          },
          {
            "value": "ELG-ENT-LIC-270220-00000002np",
            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg"
          }
        ]
        cls.non_p_serializer = MetadataRecordSerializer(data=non_p_data)
        if cls.non_p_serializer.is_valid():
            cls.non_p_instance = cls.non_p_serializer.save()
            cls.non_p_instance.management_object.curator = user
            cls.non_p_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.non_p_serializer.errors))
        LOGGER.info('Setup has finished')

    def test_licence_terms_retrieve_by_registry_identifier(self):
        registry_identifier_value = get_registry_identifier(self.instance)
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {
            'en': 'LicenceTerms name to be ignored'}
        json_data_generic_licence_terms['licence_terms_url'] = \
            ['http://landley.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data_generic_licence_terms['licence_identifier'] = list()
        licence_terms_id = dict()
        licence_terms_id[
            'licence_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        licence_terms_id['value'] = registry_identifier_value
        json_data_generic_licence_terms['licence_identifier'].append(
            licence_terms_id)

        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_registry_identifier_np(self):
        registry_identifier_value = get_registry_identifier(self.non_p_instance)
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {
            'en': 'LicenceTerms name to be ignored'}
        json_data_generic_licence_terms['licence_terms_url'] = \
            ['http://landley.net/toybox/rando.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data_generic_licence_terms['licence_identifier'] = list()
        licence_terms_id = dict()
        licence_terms_id[
            'licence_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        licence_terms_id['value'] = registry_identifier_value
        json_data_generic_licence_terms['licence_identifier'].append(
            licence_terms_id)

        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_non_registry_identifier(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {
            'en': 'LicenceTerms name to be ignored'}
        json_data_generic_licence_terms['licence_terms_url'] = \
            ['http://landley.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data_generic_licence_terms['licence_identifier'] = list()
        licence_terms_id = dict()
        licence_terms_id[
            'licence_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/SPDX"
        licence_terms_id['value'] = "0BSD"
        licence_terms_id_2 = dict()
        licence_terms_id_2[
            'licence_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/other'
        licence_terms_id_2['value'] = "other_value"
        json_data_generic_licence_terms['licence_identifier'].append(
            licence_terms_id_2)
        json_data_generic_licence_terms['licence_identifier'].append(
            licence_terms_id)
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_non_registry_identifier_np(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {
            'en': 'LicenceTerms name to be ignored'}
        json_data_generic_licence_terms['licence_terms_url'] = \
            ['http://landley.net/toybox/rando.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data_generic_licence_terms['licence_identifier'] = list()
        licence_terms_id = dict()
        licence_terms_id[
            'licence_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/SPDX"
        licence_terms_id['value'] = "0BSDnp"
        licence_terms_id_2 = dict()
        licence_terms_id_2[
            'licence_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/other'
        licence_terms_id_2['value'] = "other_valuenp"
        json_data_generic_licence_terms['licence_identifier'].append(
            licence_terms_id_2)
        json_data_generic_licence_terms['licence_identifier'].append(
            licence_terms_id)
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_non_registry_identifier_fails(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {
            'en': 'LicenceTerms name to be ignored'}
        json_data_generic_licence_terms['licence_terms_url'] = \
            ['http://randomurl.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data_generic_licence_terms['licence_identifier'] = list()
        licence_terms_id_2 = dict()
        licence_terms_id_2[
            'licence_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/other'
        licence_terms_id_2['value'] = "other_value"
        json_data_generic_licence_terms['licence_identifier'].append(
            licence_terms_id_2)
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertFalse(licence_terms_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_any_identifier_fails_when_identifier_is_None(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {
            'en': 'LicenceTerms name to be ignored'}
        json_data_generic_licence_terms['licence_terms_url'] = \
            ['http://randomlicence.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data_generic_licence_terms['licence_identifier'] = None
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertFalse(licence_terms_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_name(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = self.instance.described_entity.licence_terms_url
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_only(self):
        only_url_json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        only_url_json_str = only_url_json_file.read()
        only_url_json_file.close()
        only_url_json_data = json.loads(only_url_json_str)
        only_url_json_data['described_entity']['licence_terms_name'] = {
            "en": "BSD Zero Clause License only url"
        }
        only_url_json_data['described_entity']['licence_terms_short_name'] = {
            "en": "0BSD ou"
        }
        only_url_json_data['described_entity']['licence_terms_url'] = \
            ['http://rando.net/randbox/ou/licensenp.html', 'http://landley.net/toybox/ou/licensenp.html']
        only_url_json_data['described_entity']['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        only_url_json_data['described_entity']['licence_identifier'] = [
            {
                "value": "0BSDou",
                "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX"
            },
            {
                "value": "ELG-ENT-LIC-270220-00000002ou",
                "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg"
            }
        ]
        ou_serializer = MetadataRecordSerializer(data=only_url_json_data)
        if ou_serializer.is_valid():
            ou_instance = ou_serializer.save()
            ou_instance.management_object.status = 'p'
            ou_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(ou_serializer.errors))
        LOGGER.info('Setup has finished')


        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = {'en': 'ignore me'}
        json_data_generic_licence_terms[
            'licence_terms_url'] = ['http://landley.net/toybox/ou/licensenp.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             ou_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_name_np(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.non_p_instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = self.non_p_instance.described_entity.licence_terms_url
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_retrieve_by_url_name_fails_when_only_non_published_and_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = self.instance.described_entity.licence_terms_url
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertFalse(licence_terms_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_retrieve_by_url_name_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_lt_data = dict()
        gen_lt_data[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        gen_lt_data[
            'licence_terms_url'] = self.instance.described_entity.licence_terms_url
        gen_lt_data['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        gen_lt = GenericLicenceTermsSerializer(
            data=gen_lt_data)
        gen_lt.is_valid()
        gen_lt_instance = gen_lt.save()
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = self.instance.described_entity.licence_terms_url
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEquals(licence_terms_retrieved['pk'].value,
                              gen_lt_instance.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_retrieve_by_url_name_when_generic_and_published_prefers_published(self):
        gen_lt_data = dict()
        gen_lt_data[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        gen_lt_data[
            'licence_terms_url'] = self.instance.described_entity.licence_terms_url
        gen_lt_data['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        gen_lt = GenericLicenceTermsSerializer(
            data=gen_lt_data)
        gen_lt.is_valid()
        gen_lt_instance = gen_lt.save()
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms['licence_terms_url'] = ['http://notimportant.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEquals(licence_terms_retrieved['pk'].value,
                              self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_pk(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['pk'] = self.instance.described_entity.pk
        json_data_generic_licence_terms[
            'licence_terms_name'] = {"en": "rando licence name"}
        json_data_generic_licence_terms[
            'licence_terms_url'] = ['http://randourl.com']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
        licence_terms_retrieved_by_pk = GenericLicenceTermsSerializer(
            data=licence_terms_retrieved.data)
        if licence_terms_retrieved_by_pk.is_valid():
            licence_terms_retrieved_by_pk.save()
            self.assertEqual(licence_terms_retrieved_by_pk['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_pk_np(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['pk'] = self.non_p_instance.described_entity.pk
        json_data_generic_licence_terms[
            'licence_terms_name'] = {"en": "rando licence name"}
        json_data_generic_licence_terms[
            'licence_terms_url'] = ['http://randourl.com']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
        licence_terms_retrieved_by_pk = GenericLicenceTermsSerializer(
            data=licence_terms_retrieved.data)
        if licence_terms_retrieved_by_pk.is_valid():
            licence_terms_retrieved_by_pk.save()
            self.assertEqual(licence_terms_retrieved_by_pk['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_name_when_multiple(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms['licence_terms_url'] = [
            "http://landley.net/toybox/license.html",
            "http://anotherurl.com/terms.html"
        ]
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_name_when_multiple_np(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.non_p_instance.described_entity.licence_terms_name
        json_data_generic_licence_terms['licence_terms_url'] = [
            "http://landley.net/toybox/licensenp.html",
            "http://anotherurl.com/terms.html"
        ]
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_name_when_same_url_in_different_licence_terms(self):
        json_file_new = open('registry/tests/fixtures/licence_terms.json', 'r')
        json_str = json_file_new.read()
        json_file_new.close()
        json_data = json.loads(json_str)
        json_data['described_entity'][
            'licence_terms_name'] = {'en': 'random name'}
        json_data['described_entity'][
            'licence_terms_url'] = self.instance.described_entity.licence_terms_url
        json_data['described_entity']['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data['described_entity']['licence_identifier'] = list()
        licence_terms_id_2 = dict()
        licence_terms_id_2[
            'licence_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/other'
        licence_terms_id_2['value'] = "other_value"
        json_data['described_entity']['licence_identifier'].append(
            licence_terms_id_2)
        serializer = MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = self.instance.described_entity.licence_terms_url
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_name_when_same_url_in_different_licence_terms_np(self):
        json_file_new = open('registry/tests/fixtures/licence_terms.json', 'r')
        json_str = json_file_new.read()
        json_file_new.close()
        json_data = json.loads(json_str)
        json_data['described_entity'][
            'licence_terms_name'] = {'en': 'random name'}
        json_data['described_entity'][
            'licence_terms_url'] = self.non_p_instance.described_entity.licence_terms_url
        json_data['described_entity']['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data['described_entity']['licence_identifier'] = list()
        licence_terms_id_2 = dict()
        licence_terms_id_2[
            'licence_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/other'
        licence_terms_id_2['value'] = "other_value"
        json_data['described_entity']['licence_identifier'].append(
            licence_terms_id_2)
        serializer = MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.non_p_instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = self.non_p_instance.described_entity.licence_terms_url
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_by_url_name_retrieve_exact(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = {'en': 'random name'}
        json_data_generic_licence_terms[
            'licence_terms_url'] = [
            "http://landley.net/"
        ]
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertFalse(licence_terms_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_with_slash(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = [
            "http://landley.net/toybox/license.html/"
        ]
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_with_http_when_retrieved_instance_https(self):
        self.instance.described_entity.licence_terms_url = [
            "https://landley.net/toybox/license.html",
            "http://randomlicencetermsurl.com/terms.html"
        ]
        self.instance.described_entity.save()
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = [
            "http://landley.net/toybox/license.html/"
        ]
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.instance.described_entity.licence_terms_url = [
            "http://landley.net/toybox/license.html",
            "http://randomlicencetermsurl.com/terms.html"
        ]
        self.instance.described_entity.save()

    def test_licence_terms_retrieve_by_url_with_https_when_retrieved_instance_https(self):
        self.instance.described_entity.licence_terms_url = [
            "https://landley.net/toybox/license.html",
            "http://randomlicencetermsurl.com/terms.html"
        ]
        self.instance.described_entity.save()
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = [
            "https://landley.net/toybox/license.html/"
        ]
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.instance.described_entity.licence_terms_url = [
            "http://landley.net/toybox/license.html",
            "http://randomlicencetermsurl.com/terms.html"
        ]
        self.instance.described_entity.save()

    def test_licence_terms_retrieve_by_url_with_http_when_retrieved_instance_http(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = [
            "http://landley.net/toybox/license.html/"
        ]
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieve_by_url_with_https_when_retrieved_instance_http(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = [
            "https://landley.net/toybox/license.html/"
        ]
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieval_only_name(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms['licence_terms_url'] = ['http://notimportant.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieval_only_name_np(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = self.non_p_instance.described_entity.licence_terms_name
        json_data_generic_licence_terms['licence_terms_url'] = ['http://notimportant.net/toybox/licensenp.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieval_only_name_fails_when_only_non_published_when_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms['licence_terms_url'] = ['http://notimportant.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertFalse(licence_terms_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_licence_terms_retrieval_only_name_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_lt_data = dict()
        gen_lt_data['licence_terms_name'] = self.instance.described_entity.licence_terms_name
        gen_lt_data['licence_terms_url'] = ['http://notimportant.net/toybox/license.html']
        gen_lt_data['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        gen_lt = GenericLicenceTermsSerializer(
            data=gen_lt_data)
        gen_lt.is_valid()
        gen_lt_instance = gen_lt.save()
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms['licence_terms_url'] = ['http://notimportant.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEquals(licence_terms_retrieved['pk'].value,
                              gen_lt_instance.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_licence_terms_retrieval_only_name_when_generic_and_published_prefers_published(self):
        gen_lt_data = dict()
        gen_lt_data['licence_terms_name'] = self.instance.described_entity.licence_terms_name
        gen_lt_data['licence_terms_url'] = ['http://notimportant.net/toybox/license.html']
        gen_lt_data['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        gen_lt = GenericLicenceTermsSerializer(
            data=gen_lt_data)
        gen_lt.is_valid()
        gen_lt_instance = gen_lt.save()
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms['licence_terms_url'] = ['http://notimportant.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEquals(licence_terms_retrieved['pk'].value,
                              self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_does_not_retrieve_by_name_when_wrong_name(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {'en': 'insert random licence terms name here please'}
        json_data_generic_licence_terms['licence_terms_url'] = ['http://notimportant.net/toybox/license.html']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertFalse(licence_terms_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_licence_terms_retrieval_when_more_than_one_licence_with_same_url(self):
        licence_2 = open('registry/tests/fixtures/licence_terms.json', 'r')
        licence_2_str = licence_2.read()
        licence_2.close()
        licence_2_data = json.loads(licence_2_str)
        licence_2_data['described_entity']['licence_terms_name'] = {'en': 'differendft name'}
        licence_2_data['described_entity']['licence_terms_short_name'] = {'en': 'differdfent different name'}
        licence_2_data['described_entity']['licence_identifier'] = []
        licence_2_data['described_entity']['licence_terms_url'] = self.instance.described_entity.licence_terms_url
        licence_2_serializer = MetadataRecordSerializer(data=licence_2_data)
        if licence_2_serializer.is_valid():
            licence_2_instance = licence_2_serializer.save()
        else:
            LOGGER.info('Errors {}'.format(licence_2_serializer.errors))
        LOGGER.info('Setup has finished')
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = self.instance.described_entity.licence_terms_url
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.assertFalse(licence_terms_retrieved['pk'].value == licence_2_instance.described_entity.pk)

    def test_licence_terms_retrieval_when_more_than_one_licence_with_same_url_np(self):
        licence_2 = open('registry/tests/fixtures/licence_terms.json', 'r')
        licence_2_str = licence_2.read()
        licence_2.close()
        licence_2_data = json.loads(licence_2_str)
        licence_2_data['described_entity']['licence_terms_name'] = {'en': 'ddfifferentdf name'}
        licence_2_data['described_entity']['licence_terms_short_name'] = {'en': 'ddfifferentdf name'}
        licence_2_data['described_entity']['licence_identifier'] = []
        licence_2_data['described_entity']['licence_terms_url'] = self.non_p_instance.described_entity.licence_terms_url
        licence_2_serializer = MetadataRecordSerializer(data=licence_2_data)
        if licence_2_serializer.is_valid():
            licence_2_instance = licence_2_serializer.save()
        else:
            LOGGER.info('Errors {}'.format(licence_2_serializer.errors))
        LOGGER.info('Setup has finished')
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = self.non_p_instance.described_entity.licence_terms_name
        json_data_generic_licence_terms[
            'licence_terms_url'] = self.non_p_instance.described_entity.licence_terms_url
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.assertFalse(licence_terms_retrieved['pk'].value == licence_2_instance.described_entity.pk)

    def test_licence_terms_retrieval_when_more_than_two_licences_with_same_url_p(self):
        licence_2 = open('registry/tests/fixtures/licence_terms.json', 'r')
        licence_2_str = licence_2.read()
        licence_2.close()
        licence_1_data = json.loads(licence_2_str)
        licence_1_data['described_entity']['licence_terms_name'] = {'en': 'ddfifferentdf1 name'}
        licence_1_data['described_entity']['licence_terms_short_name'] = {'en': 'ddfiffer1entdf name'}
        licence_1_data['described_entity']['licence_identifier'] = []
        licence_1_data['described_entity']['licence_terms_url'] = ['https://cctest.com']
        licence_1_serializer = MetadataRecordSerializer(data=licence_1_data)
        if licence_1_serializer.is_valid():
            licence_1_instance = licence_1_serializer.save()
            licence_1_instance.management_object.status = 'p'
            licence_1_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(licence_1_serializer.errors))
        licence_2_data = json.loads(licence_2_str)
        licence_2_data['described_entity']['licence_terms_name'] = {'en': 'ddfifferentdf name'}
        licence_2_data['described_entity']['licence_terms_short_name'] = {'en': 'ddfifferentdf name'}
        licence_2_data['described_entity']['licence_identifier'] = []
        licence_2_data['described_entity']['licence_terms_url'] = ['https://cctest.com']
        licence_2_serializer = MetadataRecordSerializer(data=licence_2_data)
        if licence_2_serializer.is_valid():
            licence_2_instance = licence_2_serializer.save()
            licence_2_instance.management_object.status = 'p'
            licence_2_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(licence_2_serializer.errors))
        LOGGER.info('Setup has finished')
        licence_3_data = json.loads(licence_2_str)
        licence_3_data['described_entity']['licence_terms_name'] = {'en': 'ddfifferentdsf name'}
        licence_3_data['described_entity']['licence_terms_short_name'] = {'en': 'ddfifferentdsf name'}
        licence_3_data['described_entity']['licence_identifier'] = []
        licence_3_data['described_entity']['licence_terms_url'] = ['https://cctest.com']
        licence_3_serializer = MetadataRecordSerializer(data=licence_3_data)
        if licence_3_serializer.is_valid():
            licence_3_instance = licence_3_serializer.save()
            licence_3_instance.management_object.status = 'p'
            licence_3_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(licence_3_serializer.errors))
        LOGGER.info('Setup has finished')
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms[
            'licence_terms_name'] = {'en': 'name number 3'}
        json_data_generic_licence_terms[
            'licence_terms_url'] = ['https://cctest.com']
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             licence_3_instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.assertFalse(licence_terms_retrieved['pk'].value == licence_2_instance.described_entity.pk)

    def test_licence_terms_retrieval_when_creative_commons_terms_np_fails_when_not_lang(self):
        licence_2 = open('registry/tests/fixtures/licence_terms.json', 'r')
        licence_2_str = licence_2.read()
        licence_2.close()
        licence_2_data = json.loads(licence_2_str)
        licence_2_data['described_entity']['licence_terms_name'] = {'en': 'ddfcciffcerentdf name'}
        licence_2_data['described_entity']['licence_terms_short_name'] = {'en': 'ddficccfferentdf name'}
        licence_2_data['described_entity']['licence_identifier'] = []
        licence_2_data['described_entity']['licence_terms_url'] = ['https://creativecommons.org/licenses/test/test/by-sa/3.0/4.0/']
        licence_2_serializer = MetadataRecordSerializer(data=licence_2_data)
        if licence_2_serializer.is_valid():
            licence_2_instance = licence_2_serializer.save()
            licence_2_instance.management_object.status = 'p'
            licence_2_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(licence_2_serializer.errors))
        LOGGER.info('Setup has finished')
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {'en': 'cc name'}
        json_data_generic_licence_terms['licence_terms_short_name'] = {'en': 'cc name'}
        json_data_generic_licence_terms['licence_identifier'] = []
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data_generic_licence_terms['licence_terms_url'] = [
            'https://creativecommons.org/licenses/test/test/by-sa/3.0/']
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertFalse(licence_terms_retrieved['pk'].value ==
                             licence_2_instance.described_entity.pk)
        else:
            LOGGER.info('Errors {}'.format(licence_terms_retrieved.errors))
            self.assertTrue(False)
        self.assertFalse(licence_terms_retrieved['pk'].value == self.non_p_instance.described_entity.pk)

    def test_licence_terms_retrieval_when_creative_commons_terms_np(self):
        licence_2 = open('registry/tests/fixtures/licence_terms.json', 'r')
        licence_2_str = licence_2.read()
        licence_2.close()
        licence_2_data = json.loads(licence_2_str)
        licence_2_data['described_entity']['licence_terms_name'] = {'en': 'ddfccifferentdf name'}
        licence_2_data['described_entity']['licence_terms_short_name'] = {'en': 'ddficcfferentdf name'}
        licence_2_data['described_entity']['licence_identifier'] = []
        licence_2_data['described_entity']['licence_terms_url'] = ['https://creativecommons.org/licenses/test/test/by-sa/3.0/']
        licence_2_serializer = MetadataRecordSerializer(data=licence_2_data)
        if licence_2_serializer.is_valid():
            licence_2_instance = licence_2_serializer.save()
            licence_2_instance.management_object.status = 'p'
            licence_2_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(licence_2_serializer.errors))
        LOGGER.info('Setup has finished')
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {'en': 'cc name'}
        json_data_generic_licence_terms['licence_terms_short_name'] = {'en': 'cc name'}
        json_data_generic_licence_terms['licence_identifier'] = []
        json_data_generic_licence_terms['condition_of_use'] = [
            "http://w3id.org/meta-share/meta-share/unspecified"
        ]
        json_data_generic_licence_terms['licence_terms_url'] = [
            'https://creativecommons.org/licenses/test/test/by-sa/3.0/de']
        licence_terms_retrieved = GenericLicenceTermsSerializer(
            data=json_data_generic_licence_terms, context=self.serializer_context)
        if licence_terms_retrieved.is_valid():
            licence_terms_retrieved.save()
            self.assertEqual(licence_terms_retrieved['pk'].value,
                             licence_2_instance.described_entity.pk)
        else:
            LOGGER.info('Errors {}'.format(licence_terms_retrieved.errors))
            self.assertTrue(False)
        self.assertFalse(licence_terms_retrieved['pk'].value == self.non_p_instance.described_entity.pk)



class TestOrganizationRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        cls.serializer_context = {'request_user': user}
        # From JSON data, create a metadata record for Organization
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()
        json_data = json.loads(cls.json_str)
        json_data['described_entity']['organization_name'].update({'el': 'greek name'})
        json_data['described_entity']['division_category'] = None
        json_data['described_entity']['is_division_of'] = None
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.instance.management_object.status = 'p'
            cls.instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')
        non_p_data = copy.deepcopy(json_data)
        non_p_data['described_entity']['organization_name'] = {
            "en": "Org name np"
        }
        non_p_data['described_entity']['organization_short_name'] = [{
            "en": "Org short name np"
        }]
        non_p_data['described_entity']['organization_identifier'] = [
          {
            "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
            "value": "0000-0001-5123-7890np"
          },
          {
            "organization_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
            "value": "ELG-ENT-ORG-270220-00000002np"
          }
        ]
        non_p_data['described_entity']['website'] = [
          "https://www.athena-innovation-np.gr"
        ]
        cls.non_p_serializer = MetadataRecordSerializer(data=non_p_data)
        if cls.non_p_serializer.is_valid():
            cls.non_p_instance = cls.non_p_serializer.save()
            cls.non_p_instance.management_object.curator = user
            cls.non_p_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.non_p_serializer.errors))
        LOGGER.info('Setup has finished')

    def test_organization_retrieve_by_registry_identifier(self):
        registry_identifier_value = get_registry_identifier(self.instance)
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization['organization_identifier'] = list()
        organization_id = dict()
        organization_id[
            'organization_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        organization_id['value'] = registry_identifier_value
        json_data_generic_organization['organization_identifier'].append(
            organization_id)
        json_data_generic_organization['website'] = [
            'http://www.organization.org']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_registry_identifier_np(self):
        registry_identifier_value = get_registry_identifier(self.non_p_instance)
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization['organization_identifier'] = list()
        organization_id = dict()
        organization_id[
            'organization_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        organization_id['value'] = registry_identifier_value
        json_data_generic_organization['organization_identifier'].append(
            organization_id)
        json_data_generic_organization['website'] = [
            'http://www.organization.org']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)
            
    def test_organization_retrieve_by_non_registry_identifier(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization['organization_identifier'] = list()
        organization_id = dict()
        organization_id[
            'organization_identifier_scheme'] = "http://purl.org/spar/datacite/doi"
        organization_id['value'] = "0000-0001-5123-7890"
        organization_id_2 = dict()
        organization_id_2['organization_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/grid'
        organization_id_2['value'] = "identifier_value_grid"
        json_data_generic_organization['organization_identifier'].append(
            organization_id_2)
        json_data_generic_organization['organization_identifier'].append(
            organization_id)
        json_data_generic_organization['website'] = [
            'http://www.organization.org']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_non_registry_identifier_np(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization['organization_identifier'] = list()
        organization_id = dict()
        organization_id[
            'organization_identifier_scheme'] = "http://purl.org/spar/datacite/doi"
        organization_id['value'] = "0000-0001-5123-7890np"
        organization_id_2 = dict()
        organization_id_2['organization_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/grid'
        organization_id_2['value'] = "identifier_value_gridnp"
        json_data_generic_organization['organization_identifier'].append(
            organization_id_2)
        json_data_generic_organization['organization_identifier'].append(
            organization_id)
        json_data_generic_organization['website'] = [
            'http://www.organization.org']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_non_registry_identifier_fails(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization['organization_identifier'] = list()
        organization_id_2 = dict()
        organization_id_2['organization_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/grid'
        organization_id_2['value'] = "identifier_value_grid"
        json_data_generic_organization['organization_identifier'].append(
            organization_id_2)
        json_data_generic_organization['website'] = [
            'http://www.organization.org']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertFalse(organization_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_any_identifier_fails_when_identifier_is_None(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization['organization_identifier'] = None
        json_data_generic_organization['website'] = [
            'http://www.organization.org']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertFalse(organization_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_website(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = self.instance.described_entity.website
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_website_np(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = self.non_p_instance.described_entity.website
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_website_fails_when_only_non_published_with_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = self.instance.described_entity.website
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertFalse(organization_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_organization_retrieve_by_website_passes_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_org_data = dict()
        gen_org_data['actor_type'] = 'Organization'
        gen_org_data['organization_name'] = {
            'en': 'Organization name to be ignored'}
        gen_org_data[
            'website'] = self.instance.described_entity.website
        gen_org = GenericOrganizationSerializer(
            data=gen_org_data)
        gen_org.is_valid()
        gen_org_instance = gen_org.save()
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = self.instance.described_entity.website
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             gen_org_instance.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_organization_retrieve_by_website_passes_when_generic_and_published_prefers_published(self):
        gen_org_data = dict()
        gen_org_data['actor_type'] = 'Organization'
        gen_org_data['organization_name'] = {
            'en': 'Organization name to be ignored'}
        gen_org_data[
            'website'] = self.instance.described_entity.website
        gen_org = GenericOrganizationSerializer(
            data=gen_org_data)
        gen_org.is_valid()
        gen_org_instance = gen_org.save()
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = self.instance.described_entity.website
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_pk(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['pk'] = self.instance.described_entity.pk
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = ["http://randomweb.com"]
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
        organization_retrieved_by_pk = GenericOrganizationSerializer(
            data=organization_retrieved.data)
        if organization_retrieved_by_pk.is_valid():
            organization_retrieved_by_pk.save()
            self.assertEqual(organization_retrieved_by_pk['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_pk_np(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['pk'] = self.non_p_instance.described_entity.pk
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = ["http://randomweb.com"]
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
        organization_retrieved_by_pk = GenericOrganizationSerializer(
            data=organization_retrieved.data)
        if organization_retrieved_by_pk.is_valid():
            organization_retrieved_by_pk.save()
            self.assertEqual(organization_retrieved_by_pk['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_website_fails_when_website_is_None(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = None
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertFalse(organization_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_website_with_slash(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = ["https://www.athena-innovation.gr/"]
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_full_by_organization_name(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = self.instance.described_entity.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_full_by_organization_name_different_lang(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {'en': 'greek name'}
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_full_by_organization_name_np(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = self.non_p_instance.described_entity.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_full_by_organization_name_single_vs_no_division_fails(self):
        # create a retrievable organization with single is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['organization_identifier'] = []
        serializer = MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_with_division = serializer.save()
            instance_with_division.management_object.status = 'p'
            instance_with_division.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same name as the retrievable, but no is_division_of
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.described_entity.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = None
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertNotEqual(organization_retrieved['pk'].value, instance_with_division.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_full_by_organization_name_and_single_is_division_of_success(self):
        # create a retrievable organization with a single is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['organization_identifier'] = []
        serializer = MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_with_division = serializer.save()
            instance_with_division.management_object.status = 'p'
            instance_with_division.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same organization name and single is_division_of as the retrievable
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.described_entity.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = list()
        for division_instance in  instance_with_division.described_entity.is_division_of.all():
            is_division_of_data = dict()
            is_division_of_data['actor_type'] = 'Organization'
            is_division_of_data['organization_name'] = division_instance.organization_name
            json_data_generic_organization['is_division_of'].append(is_division_of_data)
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value, instance_with_division.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_full_by_organization_name_and_multiple_is_division_of_success(self):
        # create a retrievable organization with multiple is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['organization_identifier'] = []
        is_division_of_second = dict()
        is_division_of_second['actor_type'] = 'Organization'
        is_division_of_second['organization_name'] = {'en': 'Second is division of'}
        json_data['described_entity']['is_division_of'].append(is_division_of_second)
        serializer = MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_with_division = serializer.save()
            instance_with_division.management_object.status = 'p'
            instance_with_division.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same organization name and multiple is_division_of as the retrievable
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.described_entity.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = list()
        for division_instance in  instance_with_division.described_entity.is_division_of.all():
            is_division_of_data = dict()
            is_division_of_data['actor_type'] = 'Organization'
            is_division_of_data['organization_name'] = division_instance.organization_name
            json_data_generic_organization['is_division_of'].append(is_division_of_data)
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value, instance_with_division.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_full_by_organization_name_and_single_vs_multi_is_division_of_fails(self):
        # create a retrievable organization with single is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['organization_identifier'] = []
        serializer = MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_with_division = serializer.save()
            instance_with_division.management_object.status = 'p'
            instance_with_division.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same organization name and
        # multiple is_division_of, one common with single as the retrievable
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.described_entity.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = list()
        for division_instance in instance_with_division.described_entity.is_division_of.all():
            is_division_of_data = dict()
            is_division_of_data['actor_type'] = 'Organization'
            is_division_of_data['organization_name'] = division_instance.organization_name
            json_data_generic_organization['is_division_of'].append(is_division_of_data)
        is_division_of_second = dict()
        is_division_of_second['actor_type'] = 'Organization'
        is_division_of_second['organization_name'] = {'en': 'Second is division of'}
        json_data_generic_organization['is_division_of'].append(is_division_of_second)
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertNotEqual(organization_retrieved['pk'].value, instance_with_division.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_full_by_organization_name_and_multi_vs_single_is_division_of_fails(self):
        # create a retrievable organization with multi is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['organization_identifier'] = []
        is_division_of_second = dict()
        is_division_of_second['actor_type'] = 'Organization'
        is_division_of_second['organization_name'] = {'en': 'Second is division of'}
        json_data['described_entity']['is_division_of'].append(is_division_of_second)
        serializer = MetadataRecordSerializer(data=json_data)
        if serializer.is_valid():
            instance_with_division = serializer.save()
            instance_with_division.management_object.status = 'p'
            instance_with_division.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same organization name and
        # single is_division_of, one common with multipple of the retrievable
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.described_entity.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = list()
        division_instance = instance_with_division.described_entity.is_division_of.all()[0]
        is_division_of_data = dict()
        is_division_of_data['actor_type'] = 'Organization'
        is_division_of_data['organization_name'] = division_instance.organization_name
        json_data_generic_organization['is_division_of'].append(is_division_of_data)

        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertNotEqual(organization_retrieved['pk'].value, instance_with_division.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_generic_by_organization_name(self):
        # Create retrievable generic organization, only name
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {'en': 'Retrievable generic org'}
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        organization_retrievable = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        self.assertTrue(organization_retrievable.is_valid())
        organization_retrievable_instance = organization_retrievable.save()
        # Create retrieved generic organization, only name
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             organization_retrievable_instance.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_generic_by_organization_name_single_vs_no_division_fails(self):
        # create a generic retrievable organization with single is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['website'] = ['http://www.athena.gr']
        json_data['described_entity']['organization_identifier'] = []

        serializer = GenericOrganizationSerializer(data=json_data['described_entity'])
        if serializer.is_valid():
            instance_with_division = serializer.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same name as the retrievable, but no is_division_of
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = None
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertNotEqual(organization_retrieved['pk'].value, instance_with_division.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_generic_by_organization_name_and_single_is_division_of_success(self):
        # create a retrievable organization with a single is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['organization_identifier'] = []
        json_data['described_entity']['website'] = ['http://www.athena2.gr']
        serializer = GenericOrganizationSerializer(data=json_data['described_entity'])
        if serializer.is_valid():
            instance_with_division = serializer.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same organization name and single is_division_of as the retrievable
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = list()
        for division_instance in instance_with_division.is_division_of.all():
            is_division_of_data = dict()
            is_division_of_data['actor_type'] = 'Organization'
            is_division_of_data['organization_name'] = division_instance.organization_name
            json_data_generic_organization['is_division_of'].append(is_division_of_data)
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value, instance_with_division.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_generic_by_organization_name_and_multiple_is_division_of_success(self):
        # create a generic retrievable organization with multiple is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['organization_identifier'] = []
        json_data['described_entity']['website'] = ['http://www.athena3.gr']
        is_division_of_second = dict()
        is_division_of_second['actor_type'] = 'Organization'
        is_division_of_second['organization_name'] = {'en': 'Second is division of'}
        json_data['described_entity']['is_division_of'].append(is_division_of_second)
        serializer = GenericOrganizationSerializer(data=json_data['described_entity'])
        if serializer.is_valid():
            instance_with_division = serializer.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same organization name and multiple is_division_of as the retrievable
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = list()
        for division_instance in instance_with_division.is_division_of.all():
            is_division_of_data = dict()
            is_division_of_data['actor_type'] = 'Organization'
            is_division_of_data['organization_name'] = division_instance.organization_name
            json_data_generic_organization['is_division_of'].append(is_division_of_data)
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value, instance_with_division.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_generic_by_organization_name_and_single_vs_multi_is_division_of_fails(self):
        # create a retrievable organization with single is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['organization_identifier'] = []
        json_data['described_entity']['website'] = ['http://www.athena4.gr']
        serializer = GenericOrganizationSerializer(data=json_data['described_entity'])
        if serializer.is_valid():
            instance_with_division = serializer.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same organization name and
        # multiple is_division_of, one common with single as the retrievable
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = list()
        for division_instance in instance_with_division.is_division_of.all():
            is_division_of_data = dict()
            is_division_of_data['actor_type'] = 'Organization'
            is_division_of_data['organization_name'] = division_instance.organization_name
            json_data_generic_organization['is_division_of'].append(is_division_of_data)
        is_division_of_second = dict()
        is_division_of_second['actor_type'] = 'Organization'
        is_division_of_second['organization_name'] = {'en': 'Second is division of'}
        json_data_generic_organization['is_division_of'].append(is_division_of_second)
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertNotEqual(organization_retrieved['pk'].value, instance_with_division.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_generic_by_organization_name_and_multi_vs_single_is_division_of_fails(self):
        # create a retrievable organization with multi is_division_of
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {'en': 'Athena'}
        json_data['described_entity']['organization_short_name'] = [{'en': 'Athena short'}]
        json_data['described_entity']['organization_identifier'] = []
        json_data['described_entity']['website'] = ['http://www.athena6.gr']
        is_division_of_second = dict()
        is_division_of_second['actor_type'] = 'Organization'
        is_division_of_second['organization_name'] = {'en': 'Second is division of'}
        json_data['described_entity']['is_division_of'].append(is_division_of_second)
        serializer = GenericOrganizationSerializer(data=json_data['described_entity'])
        if serializer.is_valid():
            instance_with_division = serializer.save()
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        LOGGER.info('Setup has finished')
        # create a generic organization with the same organization name and
        # single is_division_of, one common with multipple of the retrievable
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = instance_with_division.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        json_data_generic_organization['is_division_of'] = list()
        division_instance = instance_with_division.is_division_of.all()[0]
        is_division_of_data = dict()
        is_division_of_data['actor_type'] = 'Organization'
        is_division_of_data['organization_name'] = division_instance.organization_name
        json_data_generic_organization['is_division_of'].append(is_division_of_data)

        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertNotEqual(organization_retrieved['pk'].value, instance_with_division.pk)
        else:
            self.assertTrue(False)

    def test_organization_retrieve_by_name_fails_when_only_non_published_and_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = self.instance.described_entity.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertFalse(organization_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_organization_retrieve_by_name_passes_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_org_data = dict()
        gen_org_data['actor_type'] = 'Organization'
        gen_org_data['organization_name'] = self.instance.described_entity.organization_name
        gen_org_data['website'] = ['http://www.tobeignored.com']
        gen_org = GenericOrganizationSerializer(
            data=gen_org_data)
        gen_org.is_valid()
        gen_org_instance = gen_org.save()
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = self.instance.described_entity.organization_name
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             gen_org_instance.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'

    def test_organization_retrieve_by_name_passes_when_generic_and_published_prefers_published(self):
        gen_org_data = dict()
        gen_org_data['actor_type'] = 'Organization'
        gen_org_data['organization_name'] = self.instance.described_entity.organization_name
        gen_org_data['website'] = ['http://www.tobeignored.com']
        gen_org = GenericOrganizationSerializer(
            data=gen_org_data)
        gen_org.is_valid()
        gen_org_instance = gen_org.save()
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_organization[
            'website'] = self.instance.described_entity.website
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertEqual(organization_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_organization_does_not_retrieve_by_organization_name_when_name_is_wrong(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = {'en': 'Athena'}
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        organization_retrieved = GenericOrganizationSerializer(
            data=json_data_generic_organization, context=self.serializer_context)
        if organization_retrieved.is_valid():
            organization_retrieved.save()
            self.assertFalse(organization_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)


class TestGroupRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        cls.serializer_context = {'request_user': user}
        # From JSON data, create a metadata record for Group
        json_file = open('registry/tests/fixtures/group.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.instance.management_object.status = 'p'
            cls.instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')
        non_p_data = copy.deepcopy(json_data)
        non_p_data['described_entity']['organization_name'] = {
            "en": "Grp name np"
        }
        non_p_data['described_entity']['organization_short_name'] = [{
            "en": "Grp short name np"
        }]
        non_p_data['described_entity']['group_identifier'] = [
            {
                "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                "value": "0000-0001-5123-7890np"
            },
            {
                "organization_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                "value": "ELG-ENT-ORG-270220-00000002np"
            }
        ]
        non_p_data['described_entity']['website'] = [
            "https://www.athena-innovation-np.gr"
        ]
        cls.non_p_serializer = MetadataRecordSerializer(data=non_p_data)
        if cls.non_p_serializer.is_valid():
            cls.non_p_instance = cls.non_p_serializer.save()
            cls.non_p_instance.management_object.curator = user
            cls.non_p_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.non_p_serializer.errors))
        LOGGER.info('Setup has finished')

    def test_group_retrieve_by_registry_identifier(self):
        registry_identifier_value = get_registry_identifier(self.instance)
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group['group_identifier'] = list()
        group_id = dict()
        group_id[
            'organization_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        group_id['value'] = registry_identifier_value
        json_data_generic_group['group_identifier'].append(
            group_id)
        json_data_generic_group['website'] = [
            'http://www.organization.org']
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertEqual(group_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_registry_identifier_np(self):
        registry_identifier_value = get_registry_identifier(self.non_p_instance)
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group['group_identifier'] = list()
        group_id = dict()
        group_id[
            'organization_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        group_id['value'] = registry_identifier_value
        json_data_generic_group['group_identifier'].append(
            group_id)
        json_data_generic_group['website'] = [
            'http://www.organization.org']
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertEqual(group_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_non_registry_identifier(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group['group_identifier'] = list()
        group_id = dict()
        group_id[
            'organization_identifier_scheme'] = "http://purl.org/spar/datacite/doi"
        group_id['value'] = "0000-0001-5123-7890"
        group_id_2 = dict()
        group_id_2[
            'organization_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/grid'
        group_id_2['value'] = "grid_value"
        json_data_generic_group['group_identifier'].append(
            group_id)
        json_data_generic_group['website'] = [
            'http://www.organization.org']
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertEqual(group_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_non_registry_identifier_np(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group['group_identifier'] = list()
        group_id = dict()
        group_id[
            'organization_identifier_scheme'] = "http://purl.org/spar/datacite/doi"
        group_id['value'] = "0000-0001-5123-7890np"
        group_id_2 = dict()
        group_id_2[
            'organization_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/grid'
        group_id_2['value'] = "grid_valuenp"
        json_data_generic_group['group_identifier'].append(
            group_id)
        json_data_generic_group['website'] = [
            'http://www.organization.org']
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertEqual(group_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_non_registry_identifier_fails(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group['group_identifier'] = list()
        group_id_2 = dict()
        group_id_2[
            'organization_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/grid'
        group_id_2['value'] = "grid_value"
        json_data_generic_group['website'] = [
            'http://www.organization.org']
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertFalse(group_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_any_identifier_fails_when_identifier_is_None(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group['group_identifier'] = None
        json_data_generic_group['website'] = [
            'http://www.organization.org']
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertFalse(group_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_website(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group[
            'website'] = self.instance.described_entity.website
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertEqual(group_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_website_np(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group[
            'website'] = self.non_p_instance.described_entity.website
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertEqual(group_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_pk(self):
        json_data_generic_group = dict()
        json_data_generic_group['pk'] = self.instance.described_entity.pk
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group[
            'website'] = ["http://randoweb.com"]
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
        group_retrieved_by_pk = GenericGroupSerializer(
            data=group_retrieved.data)
        if group_retrieved_by_pk.is_valid():
            group_retrieved_by_pk.save()
            self.assertEqual(group_retrieved_by_pk['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_pk_np(self):
        json_data_generic_group = dict()
        json_data_generic_group['pk'] = self.non_p_instance.described_entity.pk
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group[
            'website'] = ["http://randoweb.com"]
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
        group_retrieved_by_pk = GenericGroupSerializer(
            data=group_retrieved.data)
        if group_retrieved_by_pk.is_valid():
            group_retrieved_by_pk.save()
            self.assertEqual(group_retrieved_by_pk['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_website_when_website_is_None(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {
            'en': 'Organization name to be ignored'}
        json_data_generic_group[
            'website'] = None
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertFalse(group_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_organization_name(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = self.instance.described_entity.organization_name
        json_data_generic_group['website'] = ['http://www.tobeignored.com']
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertEqual(group_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_retrieve_by_organization_name_np(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = self.non_p_instance.described_entity.organization_name
        json_data_generic_group['website'] = ['http://www.tobeignored.com']
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertEqual(group_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_group_does_not_retrieve_by_organization_name_when_name_is_wrong(self):
        json_data_generic_group = dict()
        json_data_generic_group['actor_type'] = 'Group'
        json_data_generic_group['organization_name'] = {'en': 'Center'}
        json_data_generic_group['website'] = ['http://www.tobeignored.com']
        group_retrieved = GenericGroupSerializer(
            data=json_data_generic_group, context=self.serializer_context)
        if group_retrieved.is_valid():
            group_retrieved.save()
            self.assertFalse(group_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)


class TestLanguageResourceRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        cls.serializer_context = {'request_user': user}
        # From JSON data, create a metadata record for LanguageResource
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.instance.management_object.status = 'p'
            cls.instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')
        non_p_data = copy.deepcopy(json_data)
        non_p_data['described_entity']['resource_name'] = {
            "en": "Resource name np"
        }
        non_p_data['described_entity']['resource_short_name'] = {
            "en": "Resource short name np"
        }
        non_p_data['described_entity']['lr_identifier'] = [
          {
            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
            "value": "doi_value_id_2np"
          },
          {
            "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
            "value": "ELG-ENT-LRS-030220-00000052tnp"
          }
        ]
        cls.non_p_serializer = MetadataRecordSerializer(data=non_p_data)
        if cls.non_p_serializer.is_valid():
            cls.non_p_instance = cls.non_p_serializer.save()
            cls.non_p_instance.management_object.curator = user
            cls.non_p_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.non_p_serializer.errors))
        LOGGER.info('Setup has finished')

    def test_language_resource_retrieve_by_registry_identifier(self):
        registry_identifier_value = get_registry_identifier(self.instance)
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = {
            'en': 'LanguageResource name to be ignored'}
        json_data_generic_lr['lr_identifier'] = list()
        lr_id = dict()
        lr_id['lr_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        lr_id['value'] = registry_identifier_value
        json_data_generic_lr['lr_identifier'].append(lr_id)
        json_data_generic_lr['version'] = 'unspecified'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEqual(lr_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_retrieve_by_registry_identifier_np(self):
        registry_identifier_value = get_registry_identifier(self.non_p_instance)
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = {
            'en': 'LanguageResource name to be ignored'}
        json_data_generic_lr['lr_identifier'] = list()
        lr_id = dict()
        lr_id['lr_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        lr_id['value'] = registry_identifier_value
        json_data_generic_lr['lr_identifier'].append(lr_id)
        json_data_generic_lr['version'] = 'unspecified'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEqual(lr_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_retrieve_by_non_registry_identifier(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = {
            'en': 'LanguageResource name to be ignored'}
        json_data_generic_lr['lr_identifier'] = list()
        lr_id = dict()
        lr_id['lr_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/other"
        lr_id['value'] = "doi_value_id_2"
        lr_id_2 = dict()
        lr_id_2['lr_identifier_scheme'] = 'http://purl.org/spar/datacite/ark'
        lr_id_2['value'] = "doi_value_id_ark"
        json_data_generic_lr['lr_identifier'].append(lr_id_2)
        json_data_generic_lr['lr_identifier'].append(lr_id)
        json_data_generic_lr['version'] = 'unspecified'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEqual(lr_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(lr_retrieved.errors)
            self.assertTrue(False)

    def test_language_resource_retrieve_by_non_registry_identifier_np(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = {
            'en': 'LanguageResource name to be ignored'}
        json_data_generic_lr['lr_identifier'] = list()
        lr_id = dict()
        lr_id['lr_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/other"
        lr_id['value'] = "doi_value_id_2np"
        lr_id_2 = dict()
        lr_id_2['lr_identifier_scheme'] = 'http://purl.org/spar/datacite/ark'
        lr_id_2['value'] = "doi_value_id_ark"
        json_data_generic_lr['lr_identifier'].append(lr_id_2)
        json_data_generic_lr['lr_identifier'].append(lr_id)
        json_data_generic_lr['version'] = 'unspecified'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEqual(lr_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(lr_retrieved.errors)
            self.assertTrue(False)

    def test_lr_retrieve_by_non_registry_identifier_fails(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = {
            'en': 'Resource name to be ignored'}
        json_data_generic_lr['lr_identifier'] = list()
        lr_id_2 = dict()
        lr_id_2['lr_identifier_scheme'] = 'http://purl.org/spar/datacite/ark'
        lr_id_2['value'] = "doi_value_id_ark"
        json_data_generic_lr['lr_identifier'].append(lr_id_2)
        json_data_generic_lr['version'] = 'unspecified'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertFalse(lr_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(lr_retrieved.errors)
            self.assertTrue(False)

    def test_language_resource_retrieve_by_any_identifier_fails_when_identifier_is_None(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = {
            'en': 'LanguageResource name to be ignored'}
        json_data_generic_lr['lr_identifier'] = None
        json_data_generic_lr['version'] = 'unspecified'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertFalse(lr_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_retrieve_by_resource_name_and_version(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.instance.described_entity.resource_name
        json_data_generic_lr['version'] = self.instance.described_entity.version
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEqual(lr_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_retrieve_by_resource_name_and_version_np(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.non_p_instance.described_entity.resource_name
        json_data_generic_lr['version'] = self.non_p_instance.described_entity.version
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEqual(lr_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_retrieve_by_resource_name_and_version_fails_when_no_version_vs_version(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.instance.described_entity.resource_name
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertFalse(lr_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_retrieve_by_resource_name_and_version_fails_when_dif_version(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.instance.described_entity.resource_name
        json_data_generic_lr['version'] = '22.0'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertFalse(lr_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_retrieve_by_resource_name_and_version_passes_when_no_version_and_default(self):
        previous_version = copy.deepcopy(self.instance.described_entity.version)
        self.instance.described_entity.version = '1.0.0 (automatically assigned)'
        self.instance.described_entity.save()
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.instance.described_entity.resource_name
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEquals(lr_retrieved['pk'].value,
                              self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.instance.described_entity.version = previous_version
        self.instance.described_entity.save()

    def test_lr_retrieve_by_name_fails_when_only_non_published_and_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.instance.described_entity.resource_name
        json_data_generic_lr['version'] = self.instance.described_entity.version
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertFalse(lr_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_lr_retrieve_by_name_passes_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_lr_data = dict()
        gen_lr_data['resource_name'] = self.instance.described_entity.resource_name
        gen_lr_data['version'] = '10.0'
        gen_lr = GenericLanguageResourceSerializer(
            data=gen_lr_data)
        gen_lr.is_valid()
        gen_lr_instance = gen_lr.save()
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.instance.described_entity.resource_name
        json_data_generic_lr['version'] = '10.0'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEqual(lr_retrieved['pk'].value,
                             gen_lr_instance.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_lr_retrieve_by_name_passes_when_generic_and_non_published_fails_version_dif(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_lr_data = dict()
        gen_lr_data['resource_name'] = self.instance.described_entity.resource_name
        gen_lr_data['version'] = '9.0'
        gen_lr = GenericLanguageResourceSerializer(
            data=gen_lr_data)
        gen_lr.is_valid()
        gen_lr_instance = gen_lr.save()
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.instance.described_entity.resource_name
        json_data_generic_lr['version'] = '10.0'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertFalse(lr_retrieved['pk'].value == gen_lr_instance.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_lr_retrieve_by_name_passes_when_generic_and_non_published_no_version(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_lr_data = dict()
        gen_lr_data['resource_name'] = self.instance.described_entity.resource_name
        gen_lr = GenericLanguageResourceSerializer(
            data=gen_lr_data)
        gen_lr.is_valid()
        gen_lr_instance = gen_lr.save()
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.instance.described_entity.resource_name
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEqual(lr_retrieved['pk'].value,
                             gen_lr_instance.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_lr_retrieve_by_name_passes_when_generic_and_published_prefers_published(self):
        gen_lr_data = dict()
        gen_lr_data['resource_name'] = self.instance.described_entity.resource_name
        gen_lr_data['version'] = self.instance.described_entity.version
        gen_lr = GenericLanguageResourceSerializer(
            data=gen_lr_data)
        gen_lr.is_valid()
        gen_lr_instance = gen_lr.save()
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = self.instance.described_entity.resource_name
        json_data_generic_lr['version'] = self.instance.described_entity.version
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertEqual(lr_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_retrieve_by_pk(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['pk'] = self.instance.described_entity.pk
        json_data_generic_lr['resource_name'] = {"en": "random name"}
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
        lr_retrieved_by_pk = GenericLanguageResourceSerializer(
            data=lr_retrieved.data)
        if lr_retrieved_by_pk.is_valid():
            lr_retrieved_by_pk.save()
            self.assertEqual(lr_retrieved_by_pk['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_retrieve_by_pk_np(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['pk'] = self.non_p_instance.described_entity.pk
        json_data_generic_lr['resource_name'] = {"en": "random name"}
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
        lr_retrieved_by_pk = GenericLanguageResourceSerializer(
            data=lr_retrieved.data)
        if lr_retrieved_by_pk.is_valid():
            lr_retrieved_by_pk.save()
            self.assertEqual(lr_retrieved_by_pk['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_language_resource_does_not_retrieve_by_resource_name_when_resource_name_is_wrong(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = {'en': 'unimportant resource name not in the database'}
        json_data_generic_lr['version'] = 'unspecified'
        lr_retrieved = GenericLanguageResourceSerializer(
            data=json_data_generic_lr, context=self.serializer_context)
        if lr_retrieved.is_valid():
            lr_retrieved.save()
            self.assertFalse(lr_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)


class TestDocumentRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        cls.serializer_context = {'request_user': user}
        # From JSON data, create a metadata record for Document
        json_file = open('registry/tests/fixtures/document.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.instance.management_object.status = 'p'
            cls.instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')
        non_p_data = copy.deepcopy(json_data)
        non_p_data['described_entity']['title'] = {
            "en": "Document name np"
        }
        non_p_data['described_entity']['alternative_title'] = [{
            "en": "Document short name np"
        }]
        non_p_data['described_entity']['document_identifier'] = [
            {
                "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                "value": "https://doi.org/10.5281np/zenodo.3467639"
            },
            {
                "document_identifier_scheme": "http://purl.org/spar/datacite/isbn",
                "value": "978-1491514985np"
            },
            {
                "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                "value": "978-1491514985np"
            }
        ]
        cls.non_p_serializer = MetadataRecordSerializer(data=non_p_data)
        if cls.non_p_serializer.is_valid():
            cls.non_p_instance = cls.non_p_serializer.save()
            cls.non_p_instance.management_object.curator = user
            cls.non_p_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.non_p_serializer.errors))
        LOGGER.info('Setup has finished')

    def test_document_retrieve_by_registry_identifier(self):
        registry_identifier_value = get_registry_identifier(self.instance)
        json_data_generic_document = dict()
        json_data_generic_document['title'] = {
            'en': 'Document title to be ignored'}
        json_data_generic_document['document_identifier'] = list()
        document_id = dict()
        document_id[
            'document_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        document_id['value'] = registry_identifier_value
        json_data_generic_document['document_identifier'].append(
            document_id)
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertEqual(document_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_retrieve_by_registry_identifier_np(self):
        registry_identifier_value = get_registry_identifier(self.non_p_instance)
        json_data_generic_document = dict()
        json_data_generic_document['title'] = {
            'en': 'Document title to be ignored'}
        json_data_generic_document['document_identifier'] = list()
        document_id = dict()
        document_id[
            'document_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        document_id['value'] = registry_identifier_value
        json_data_generic_document['document_identifier'].append(
            document_id)
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertEqual(document_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_retrieve_by_non_registry_identifier(self):
        json_data_generic_document = dict()
        json_data_generic_document['title'] = {
            'en': 'Document title to be ignored'}
        json_data_generic_document['document_identifier'] = list()
        document_id = dict()
        document_id[
            'document_identifier_scheme'] = "http://purl.org/spar/datacite/doi"
        document_id['value'] = "https://doi.org/10.5281/zenodo.3467639"
        document_id_2 = dict()
        document_id_2[
            'document_identifier_scheme'] = "http://purl.org/spar/datacite/isbn"
        document_id_2['value'] = "978-1491514985"
        document_id_3 = dict()
        document_id_3[
            'document_identifier_scheme'] = 'http://purl.org/spar/datacite/urn'
        document_id_3['value'] = "urn_value"
        json_data_generic_document['document_identifier'].append(
            document_id_3)
        json_data_generic_document['document_identifier'].append(
            document_id_2)
        json_data_generic_document['document_identifier'].append(
            document_id)
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertEqual(document_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_retrieve_by_non_registry_identifier_np(self):
        json_data_generic_document = dict()
        json_data_generic_document['title'] = {
            'en': 'Document title to be ignored'}
        json_data_generic_document['document_identifier'] = list()
        document_id = dict()
        document_id[
            'document_identifier_scheme'] = "http://purl.org/spar/datacite/doi"
        document_id['value'] = "https://doi.org/10.5281np/zenodo.3467639"
        document_id_2 = dict()
        document_id_2[
            'document_identifier_scheme'] = "http://purl.org/spar/datacite/isbn"
        document_id_2['value'] = "978-1491514985np"
        document_id_3 = dict()
        document_id_3[
            'document_identifier_scheme'] = 'http://purl.org/spar/datacite/urn'
        document_id_3['value'] = "urn_valuenp"
        json_data_generic_document['document_identifier'].append(
            document_id_3)
        json_data_generic_document['document_identifier'].append(
            document_id_2)
        json_data_generic_document['document_identifier'].append(
            document_id)
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertEqual(document_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_retrieve_by_non_registry_identifier_fails(self):
        json_data_generic_document = dict()
        json_data_generic_document['title'] = {
            'en': 'Document title to be ignored'}
        json_data_generic_document['document_identifier'] = list()
        document_id_3 = dict()
        document_id_3[
            'document_identifier_scheme'] = 'http://purl.org/spar/datacite/urn'
        document_id_3['value'] = "urn_value"
        json_data_generic_document['document_identifier'].append(
            document_id_3)
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertFalse(document_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_retrieve_by_any_identifier_fails_when_identifier_is_None(self):
        json_data_generic_document = dict()
        json_data_generic_document['title'] = {
            'en': 'Document title to be ignored'}
        json_data_generic_document['document_identifier'] = None
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertFalse(document_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_retrieve_by_title(self):
        json_data_generic_document = dict()
        json_data_generic_document['title'] = self.instance.described_entity.title
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertEqual(document_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_retrieve_by_title_np(self):
        json_data_generic_document = dict()
        json_data_generic_document['title'] = self.non_p_instance.described_entity.title
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertEqual(document_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_retrieve_by_title_fails_when_only_non_published_and_no_context(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        json_data_generic_document = dict()
        json_data_generic_document['title'] = self.instance.described_entity.title
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertFalse(document_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_document_retrieve_by_title_passes_when_generic_and_non_published(self):
        self.instance.management_object.status = 'i'
        self.instance.management_object.save()
        gen_doc_data = dict()
        gen_doc_data['title'] = self.instance.described_entity.title
        gen_doc = GenericDocumentSerializer(
            data=gen_doc_data)
        gen_doc.is_valid()
        gen_doc_instance = gen_doc.save()
        json_data_generic_document = dict()
        json_data_generic_document['title'] = self.instance.described_entity.title
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertEqual(document_retrieved['pk'].value,
                             gen_doc_instance.pk)
        else:
            self.assertTrue(False)
        self.instance.management_object.status = 'p'
        self.instance.management_object.save()

    def test_document_retrieve_by_title_passes_when_generic_and_published_prefers_published(self):
        gen_doc_data = dict()
        gen_doc_data['title'] = self.instance.described_entity.title
        gen_doc = GenericDocumentSerializer(
            data=gen_doc_data)
        gen_doc.is_valid()
        gen_doc_instance = gen_doc.save()
        json_data_generic_document = dict()
        json_data_generic_document['title'] = self.instance.described_entity.title
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertEqual(document_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_retrieve_by_pk(self):
        json_data_generic_document = dict()
        json_data_generic_document['pk'] = self.instance.described_entity.pk
        json_data_generic_document['title'] = {"en": "rando doc title"}
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
        document_retrieved_by_pk = GenericDocumentSerializer(
            data=document_retrieved.data)
        if document_retrieved_by_pk.is_valid():
            document_retrieved_by_pk.save()
            self.assertEqual(document_retrieved_by_pk['pk'].value,
                             self.instance.described_entity.pk)
        else:
            self.assertTrue(False)
    
    def test_document_retrieve_by_pk_np(self):
        json_data_generic_document = dict()
        json_data_generic_document['pk'] = self.non_p_instance.described_entity.pk
        json_data_generic_document['title'] = {"en": "rando doc title"}
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
        document_retrieved_by_pk = GenericDocumentSerializer(
            data=document_retrieved.data)
        if document_retrieved_by_pk.is_valid():
            document_retrieved_by_pk.save()
            self.assertEqual(document_retrieved_by_pk['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            self.assertTrue(False)

    def test_document_does_not_retrieve_by_title_when_title_is_wrong(self):
        json_data_generic_document = dict()
        json_data_generic_document['title'] = {'en': 'PyPDD'}
        document_retrieved = GenericDocumentSerializer(
            data=json_data_generic_document, context=self.serializer_context)
        if document_retrieved.is_valid():
            document_retrieved.save()
            self.assertFalse(document_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            self.assertTrue(False)


class TestDomainRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_data = {
            'category_label': {'en': 'telecommunications'},
            'domain_identifier': {
                'domain_classification_scheme': 'http://w3id.org/meta-share/meta-share/ELG_domainClassification',
                'value': 'ELG_Domain004',
            }
        }

        cls.serializer = DomainSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_domain_retrieve_by_all_elements(self):
        domain_retrieved = DomainSerializer(
            data=self.json_data)
        if domain_retrieved.is_valid():
            domain_retrieved.save()
            self.assertEqual(domain_retrieved['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_domain_retrieve_by_pk(self):
        domain_retrieved = DomainSerializer(
            data=self.json_data)
        if domain_retrieved.is_valid():
            domain_retrieved.save()
        domain_retrieved_by_pk = DomainSerializer(
            data=domain_retrieved.data)
        if domain_retrieved_by_pk.is_valid():
            domain_retrieved_by_pk.save()
            self.assertEqual(domain_retrieved_by_pk['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_domain_retrieve_by_all_elements_is_case_insensitive(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'Telecommunications'}
        domain_retrieved = DomainSerializer(
            data=json_data)
        if domain_retrieved.is_valid():
            domain_retrieved.save()
            self.assertEquals(domain_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)

    def test_domain_retrieve_by_all_elements_ignores_space_before(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': ' Telecommunications'}
        domain_retrieved = DomainSerializer(
            data=json_data)
        if domain_retrieved.is_valid():
            domain_retrieved.save()
            self.assertEquals(domain_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)

    def test_domain_retrieve_by_all_elements_ignores_space_after(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'Telecommunications '}
        domain_retrieved = DomainSerializer(
            data=json_data)
        if domain_retrieved.is_valid():
            domain_retrieved.save()
            self.assertEquals(domain_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)

    def test_domain_retrieve_by_all_elements_fail_category_label(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'other domain'}
        domain_retrieved = DomainSerializer(
            data=json_data)
        if domain_retrieved.is_valid():
            domain_retrieved.save()
            self.assertNotEquals(domain_retrieved['pk'].value,
                                 self.instance.pk)
        else:
            self.assertTrue(False)

    def test_domain_retrieve_by_all_elements_passes_without_id(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['domain_identifier'] = None
        domain_retrieved = DomainSerializer(
            data=json_data)
        if domain_retrieved.is_valid():
            domain_retrieved.save()
            self.assertEquals(domain_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)


class TestTextTypeRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_data = {
            'category_label': {'en': 'academic text'},
            'text_type_identifier': {
                'text_type_classification_scheme': 'http://w3id.org/meta-share/meta-share/ClarinEL_TextTypeClassification',
                'value': 'ClarinEl_TT001',
            }
        }

        cls.serializer = TextTypeSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_text_type_retrieve_by_all_elements(self):
        text_type_retrieved = TextTypeSerializer(
            data=self.json_data)
        if text_type_retrieved.is_valid():
            text_type_retrieved.save()
            self.assertEqual(text_type_retrieved['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_text_type_retrieve_by_pk(self):
        text_type_retrieved = TextTypeSerializer(
            data=self.json_data)
        if text_type_retrieved.is_valid():
            text_type_retrieved.save()
        text_type_retrieved_by_pk = TextTypeSerializer(
            data=text_type_retrieved.data)
        if text_type_retrieved_by_pk.is_valid():
            text_type_retrieved_by_pk.save()
            self.assertEqual(text_type_retrieved_by_pk['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_text_type_retrieve_by_all_elements_fail_category_label(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'other text type'}
        text_type_retrieved = TextTypeSerializer(
            data=json_data)
        if text_type_retrieved.is_valid():
            text_type_retrieved.save()
            self.assertNotEquals(text_type_retrieved['pk'].value,
                                 self.instance.pk)
        else:
            self.assertTrue(False)

    def test_text_type_retrieve_by_all_elements_passes_without_id(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['text_type_identifier'] = None
        text_type_retrieved = TextTypeSerializer(
            data=json_data)
        if text_type_retrieved.is_valid():
            text_type_retrieved.save()
            self.assertEquals(text_type_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)


class TestSubjectRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_data = {
            "category_label": {
                "en": "category"
            },
            "subject_identifier": {
                "subject_classification_scheme": "http://w3id.org/meta-share/meta-share/MeSH_classification",
                "value": "doi_value_id"
            }
        }
        cls.serializer = SubjectSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_subject_retrieve_by_all_elements(self):
        subject_retrieved = SubjectSerializer(
            data=self.json_data)
        if subject_retrieved.is_valid():
            subject_retrieved.save()
            self.assertEqual(subject_retrieved['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_subject_retrieve_by_pk(self):
        subject_retrieved = SubjectSerializer(
            data=self.json_data)
        if subject_retrieved.is_valid():
            subject_retrieved.save()
        subject_retrieved_by_pk = SubjectSerializer(
            data=subject_retrieved.data)
        if subject_retrieved_by_pk.is_valid():
            subject_retrieved_by_pk.save()
            self.assertEqual(subject_retrieved_by_pk['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_subject_retrieve_by_all_elements_fail_category_label(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'other domain'}
        subject_retrieved = SubjectSerializer(
            data=json_data)
        if subject_retrieved.is_valid():
            subject_retrieved.save()
            self.assertNotEquals(subject_retrieved['pk'].value,
                                 self.instance.pk)
        else:
            self.assertTrue(False)

    def test_subject_retrieve_by_all_elements_passes_without_id(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['subject_identifier'] = None
        subject_retrieved = SubjectSerializer(
            data=json_data)
        if subject_retrieved.is_valid():
            subject_retrieved.save()
            self.assertEquals(subject_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)


class TestAudioGenreRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_data = {
            'category_label': {'en': 'human non speech'},
            'audio_genre_identifier': {
                'audio_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/MS_audioGenreClassification',
                'value': 'MS_AG001',
            }
        }

        cls.serializer = AudioGenreSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_audio_genre_retrieve_by_all_elements(self):
        audio_genre_retrieved = AudioGenreSerializer(
            data=self.json_data)
        if audio_genre_retrieved.is_valid():
            audio_genre_retrieved.save()
            self.assertEqual(audio_genre_retrieved['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_audio_genre_retrieve_by_pk(self):
        audio_genre_retrieved = AudioGenreSerializer(
            data=self.json_data)
        if audio_genre_retrieved.is_valid():
            audio_genre_retrieved.save()
        audio_genre_retrieved_by_pk = AudioGenreSerializer(
            data=audio_genre_retrieved.data)
        if audio_genre_retrieved_by_pk.is_valid():
            audio_genre_retrieved_by_pk.save()
            self.assertEqual(audio_genre_retrieved_by_pk['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_audio_genre_retrieve_by_all_elements_fail_category_label(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'other audio genre'}
        audio_genre_retrieved = AudioGenreSerializer(
            data=json_data)
        if audio_genre_retrieved.is_valid():
            audio_genre_retrieved.save()
            self.assertNotEquals(audio_genre_retrieved['pk'].value,
                                 self.instance.pk)
        else:
            self.assertTrue(False)

    def test_audio_genre_retrieve_by_all_elements_passes_without_id(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['audio_genre_identifier'] = None
        audio_genre_retrieved = AudioGenreSerializer(
            data=json_data)
        if audio_genre_retrieved.is_valid():
            audio_genre_retrieved.save()
            self.assertEquals(audio_genre_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)


class TestSpeechGenreRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a SpeechGenre
        cls.json_data = {
            'category_label': {'en': 'air traffic control'},
            'speech_genre_identifier': {
                'speech_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/MS_SpeechGenreClassification',
                'value': 'MS_SG001',
            }
        }

        cls.serializer = SpeechGenreSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_speech_genre_retrieve_by_all_elements(self):
        speech_genre_retrieved = SpeechGenreSerializer(
            data=self.json_data)
        if speech_genre_retrieved.is_valid():
            speech_genre_retrieved.save()
            self.assertEqual(speech_genre_retrieved['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_speech_genre_retrieve_by_pk(self):
        speech_genre_retrieved = SpeechGenreSerializer(
            data=self.json_data)
        if speech_genre_retrieved.is_valid():
            speech_genre_retrieved.save()
        speech_genre_retrieved_by_pk = SpeechGenreSerializer(
            data=speech_genre_retrieved.data)
        if speech_genre_retrieved_by_pk.is_valid():
            speech_genre_retrieved_by_pk.save()
            self.assertEqual(speech_genre_retrieved_by_pk['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_speech_genre_retrieve_by_all_elements_fail_category_label(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'other speech genre'}
        speech_genre_retrieved = SpeechGenreSerializer(
            data=json_data)
        if speech_genre_retrieved.is_valid():
            speech_genre_retrieved.save()
            self.assertNotEquals(speech_genre_retrieved['pk'].value,
                                 self.instance.pk)
        else:
            self.assertTrue(False)

    def test_speech_genre_retrieve_by_all_elements_passes_without_id(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['speech_genre_identifier'] = None
        speech_genre_retrieved = SpeechGenreSerializer(
            data=json_data)
        if speech_genre_retrieved.is_valid():
            speech_genre_retrieved.save()
            self.assertEquals(speech_genre_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)


class TestTextGenreRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_data = {
            'category_label': {'en': 'advertising'},
            'text_genre_identifier': {
                'text_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/PAROLE_genreClassification',
                'value': 'PAROLE_G002',
            }
        }

        cls.serializer = TextGenreSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_text_genre_retrieve_by_all_elements(self):
        text_genre_retrieved = TextGenreSerializer(
            data=self.json_data)
        if text_genre_retrieved.is_valid():
            text_genre_retrieved.save()
            self.assertEqual(text_genre_retrieved['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_text_genre_retrieve_by_pk(self):
        text_genre_retrieved = TextGenreSerializer(
            data=self.json_data)
        if text_genre_retrieved.is_valid():
            text_genre_retrieved.save()
        text_genre_retrieved_by_pk = TextGenreSerializer(
            data=text_genre_retrieved.data)
        if text_genre_retrieved_by_pk.is_valid():
            text_genre_retrieved_by_pk.save()
            self.assertEqual(text_genre_retrieved_by_pk['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_text_genre_retrieve_by_all_elements_fail_category_label(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'other text genre'}
        text_genre_retrieved = TextGenreSerializer(
            data=json_data)
        if text_genre_retrieved.is_valid():
            text_genre_retrieved.save()
            self.assertNotEquals(text_genre_retrieved['pk'].value,
                                 self.instance.pk)
        else:
            self.assertTrue(False)

    def test_text_genre_retrieve_by_all_elements_passes_without_id(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['text_genre_identifier'] = None
        text_genre_retrieved = TextGenreSerializer(
            data=json_data)
        if text_genre_retrieved.is_valid():
            text_genre_retrieved.save()
            self.assertEquals(text_genre_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)


class TestVideoGenreRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_data = {
            "category_label": {
                "en": "label"
            },
            "video_genre_identifier": {
                "video_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                "value": "doi_value_id"
            }
        }

        cls.serializer = VideoGenreSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_video_genre_retrieve_by_all_elements(self):
        video_genre_retrieved = VideoGenreSerializer(
            data=self.json_data)
        if video_genre_retrieved.is_valid():
            video_genre_retrieved.save()
            self.assertEqual(video_genre_retrieved['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_video_genre_retrieve_by_pk(self):
        video_genre_retrieved = VideoGenreSerializer(
            data=self.json_data)
        if video_genre_retrieved.is_valid():
            video_genre_retrieved.save()
        video_genre_retrieved_by_pk = VideoGenreSerializer(
            data=video_genre_retrieved.data)
        if video_genre_retrieved_by_pk.is_valid():
            video_genre_retrieved_by_pk.save()
            self.assertEqual(video_genre_retrieved_by_pk['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_video_genre_retrieve_by_all_elements_fail_category_label(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'other video genre'}
        video_genre_retrieved = VideoGenreSerializer(
            data=json_data)
        if video_genre_retrieved.is_valid():
            video_genre_retrieved.save()
            self.assertNotEquals(video_genre_retrieved['pk'].value,
                                 self.instance.pk)
        else:
            self.assertTrue(False)

    def test_video_genre_retrieve_by_all_elements_passes_without_id(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['video_genre_identifier'] = None
        video_genre_retrieved = VideoGenreSerializer(
            data=json_data)
        if video_genre_retrieved.is_valid():
            video_genre_retrieved.save()
            self.assertEquals(video_genre_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)


class TestImageGenreRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_data = {
            "category_label": {
                "en": "label"
            },
            "image_genre_identifier": {
                "image_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                "value": "doi_value_id"
            }
        }

        cls.serializer = ImageGenreSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_image_genre_retrieve_by_all_elements(self):
        image_genre_retrieved = ImageGenreSerializer(
            data=self.json_data)
        if image_genre_retrieved.is_valid():
            image_genre_retrieved.save()
            self.assertEqual(image_genre_retrieved['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_image_genre_retrieve_by_pk(self):
        image_genre_retrieved = ImageGenreSerializer(
            data=self.json_data)
        if image_genre_retrieved.is_valid():
            image_genre_retrieved.save()
        image_genre_retrieved_by_pk = ImageGenreSerializer(
            data=image_genre_retrieved.data)
        if image_genre_retrieved_by_pk.is_valid():
            image_genre_retrieved_by_pk.save()
            self.assertEqual(image_genre_retrieved_by_pk['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_image_genre_retrieve_by_all_elements_fail_category_label(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'other image genre'}
        image_genre_retrieved = ImageGenreSerializer(
            data=json_data)
        if image_genre_retrieved.is_valid():
            image_genre_retrieved.save()
            self.assertNotEquals(image_genre_retrieved['pk'].value,
                                 self.instance.pk)
        else:
            self.assertTrue(False)

    def test_image_genre_retrieve_by_all_elements_passes_without_id(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['image_genre_identifier'] = None
        image_genre_retrieved = ImageGenreSerializer(
            data=json_data)
        if image_genre_retrieved.is_valid():
            image_genre_retrieved.save()
            self.assertEquals(image_genre_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)


class TestAccessRightsStatementRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a AccessRightsStatement
        cls.json_data = {
            'category_label': {'en': 'restricted access'},
            'access_rights_statement_identifier': {
                'access_rights_statement_scheme': 'http://w3id.org/meta-share/meta-share/COARightsStatementScheme',
                'value': 'c_16ec',
            }
        }

        cls.serializer = AccessRightsStatementSerializer(data=cls.json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_access_rights_statement_retrieve_by_all_elements(self):
        access_rights_statement_retrieved = AccessRightsStatementSerializer(
            data=self.json_data)
        if access_rights_statement_retrieved.is_valid():
            access_rights_statement_retrieved.save()
            self.assertEqual(access_rights_statement_retrieved['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_access_rights_statement_retrieve_by_pk(self):
        access_rights_statement_retrieved = AccessRightsStatementSerializer(
            data=self.json_data)
        if access_rights_statement_retrieved.is_valid():
            access_rights_statement_retrieved.save()
        access_rights_statement_retrieved_by_pk = AccessRightsStatementSerializer(
            data=access_rights_statement_retrieved.data)
        if access_rights_statement_retrieved_by_pk.is_valid():
            access_rights_statement_retrieved_by_pk.save()
            self.assertEqual(access_rights_statement_retrieved_by_pk['pk'].value,
                             self.instance.pk)
        else:
            self.assertTrue(False)

    def test_access_rights_statement_retrieve_by_all_elements_fail_category_label(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['category_label'] = {'en': 'other text genre'}
        access_rights_statement_retrieved = AccessRightsStatementSerializer(
            data=json_data)
        if access_rights_statement_retrieved.is_valid():
            access_rights_statement_retrieved.save()
            self.assertNotEquals(access_rights_statement_retrieved['pk'].value,
                                 self.instance.pk)
        else:
            self.assertTrue(False)

    def test_access_rights_statement_retrieve_by_all_elements_passes_without_id(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['access_rights_statement_identifier'] = None
        access_rights_statement_retrieved = AccessRightsStatementSerializer(
            data=json_data)
        if access_rights_statement_retrieved.is_valid():
            access_rights_statement_retrieved.save()
            self.assertEquals(access_rights_statement_retrieved['pk'].value,
                              self.instance.pk)
        else:
            self.assertTrue(False)


class TestRepositoryRetrievalMechanisms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        cls.serializer_context = {'request_user': user}
        json_file = open('registry/tests/fixtures/repository.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()
        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.instance.management_object.status = 'p'
            cls.instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')
        non_p_data = copy.deepcopy(json_data)
        non_p_data['described_entity']['repository_name'] = {
            "en": "Repo name np"
        }
        non_p_data['described_entity']['repository_additional_name'] = {
            "en": "Repo short name np"
        }
        non_p_data['described_entity']['repository_url'] = "http://www.reponp.com"
        non_p_data['described_entity']['repository_identifier'] = [
          {
            "repository_identifier_scheme": "http://purl.org/spar/datacite/handle",
            "value": "0000-0001-5122-7990np"
          },
          {
            "repository_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
            "value": "ELG-ENT-PER-270220-00000002np"
          }
        ]
        cls.non_p_serializer = MetadataRecordSerializer(data=non_p_data)
        if cls.non_p_serializer.is_valid():
            cls.non_p_instance = cls.non_p_serializer.save()
            cls.non_p_instance.management_object.curator = user
            cls.non_p_instance.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.non_p_serializer.errors))
        LOGGER.info('Setup has finished')

    def test_repository_retrieve_by_url_and_name(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = self.instance.described_entity.repository_name
        json_data_generic_repository['repository_url'] = self.instance.described_entity.repository_url
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/re3data"
        repo_id['value'] = "repository test value r3a"
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertEqual(repo_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_url_and_name_np(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = self.non_p_instance.described_entity.repository_name
        json_data_generic_repository['repository_url'] = self.non_p_instance.described_entity.repository_url
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/re3data"
        repo_id['value'] = "repository test value r3a"
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertEqual(repo_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_url_and_name_fails_correctly(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = {"en": "randonamef"}
        json_data_generic_repository['repository_url'] = "https://www.randourl.com"
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/re3data"
        repo_id['value'] = "repository test value r3af"
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertFalse(repo_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_url_only_fails_if_multiple(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = {"en": "randonamer"}
        json_data_generic_repository['repository_url'] = self.instance.described_entity.repository_url
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/re3data"
        repo_id['value'] = "repository test value r3ar"
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertFalse(repo_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_name_only(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = self.instance.described_entity.repository_name
        json_data_generic_repository['repository_url'] = "http://www.randoesturl.com"
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/re3data"
        repo_id['value'] = "repository test value r3arr"
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertEqual(repo_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_name_only_np(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = self.non_p_instance.described_entity.repository_name
        json_data_generic_repository['repository_url'] = "http://www.randoesturl.com"
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/re3data"
        repo_id['value'] = "repository test value r3arr"
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertEqual(repo_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_registry_identifier(self):
        registry_identifier_value = get_registry_identifier(self.instance)
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = {
            'en': 'Repo name to be ignored'}
        json_data_generic_repository['repository_url'] = 'http://www.tobeignored.com'
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        repo_id['value'] = registry_identifier_value
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertEqual(repo_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_registry_identifier_np(self):
        registry_identifier_value = get_registry_identifier(self.non_p_instance)
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = {
            'en': 'Repo name to be ignored'}
        json_data_generic_repository['repository_url'] = 'http://www.tobeignored.com'
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        repo_id['value'] = registry_identifier_value
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertEqual(repo_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_non_registry_identifier(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = {
            'en': 'Repo name to be ignored'}
        json_data_generic_repository['repository_url'] = 'http://www.tobeignored.com'
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = "http://purl.org/spar/datacite/handle"
        repo_id['value'] = "0000-0001-5122-7990"
        repo_id_2 = dict()
        repo_id_2[
            'repository_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/re3data'
        repo_id_2['value'] = "repository test value r3a"
        json_data_generic_repository['repository_identifier'].append(
            repo_id_2)
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertEqual(repo_retrieved['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_non_registry_identifier_np(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = {
            'en': 'Repo name to be ignored'}
        json_data_generic_repository['repository_url'] = 'http://www.tobeignored.com'
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = "http://purl.org/spar/datacite/handle"
        repo_id['value'] = "0000-0001-5122-7990np"
        repo_id_2 = dict()
        repo_id_2[
            'repository_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/re3data'
        repo_id_2['value'] = "repository test value r3anp"
        json_data_generic_repository['repository_identifier'].append(
            repo_id_2)
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertEqual(repo_retrieved['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_non_registry_identifier_fails(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = {
            'en': 'Repo name to be ignored'}
        json_data_generic_repository['repository_url'] = 'http://www.tobeignored.com'
        json_data_generic_repository['repository_identifier'] = list()
        repo_id = dict()
        repo_id['repository_identifier_scheme'] = "http://w3id.org/meta-share/meta-share/re3data"
        repo_id['value'] = "repository test value r3a"
        json_data_generic_repository['repository_identifier'].append(repo_id)
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertFalse(repo_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_any_identifier_fails_when_identifier_is_None(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['repository_name'] = {
            'en': 'Repo name to be ignored'}
        json_data_generic_repository['repository_url'] = 'http://www.tobeignored.com'
        json_data_generic_repository['repository_identifier'] = None
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
            self.assertFalse(repo_retrieved['pk'].value == self.instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_pk(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['pk'] = self.instance.described_entity.pk
        json_data_generic_repository['repository_name'] = {
            'en': 'Repo name to be ignored'}
        json_data_generic_repository['repository_url'] = 'http://www.tobeignored.com'
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
        repo_retrieved_by_pk = GenericRepositorySerializer(
            data=repo_retrieved.data)
        if repo_retrieved_by_pk.is_valid():
            repo_retrieved_by_pk.save()
            self.assertEqual(repo_retrieved_by_pk['pk'].value,
                             self.instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)

    def test_repository_retrieve_by_pk_np(self):
        json_data_generic_repository = dict()
        json_data_generic_repository['pk'] = self.non_p_instance.described_entity.pk
        json_data_generic_repository['repository_name'] = {
            'en': 'Repo name to be ignored'}
        json_data_generic_repository['repository_url'] = 'http://www.tobeignored.com'
        repo_retrieved = GenericRepositorySerializer(
            data=json_data_generic_repository)
        if repo_retrieved.is_valid():
            repo_retrieved.save()
        repo_retrieved_by_pk = GenericRepositorySerializer(
            data=repo_retrieved.data)
        if repo_retrieved_by_pk.is_valid():
            repo_retrieved_by_pk.save()
            self.assertEqual(repo_retrieved_by_pk['pk'].value,
                             self.non_p_instance.described_entity.pk)
        else:
            LOGGER.info(repo_retrieved.errors)
            self.assertTrue(False)


class TestRetrieveByNonElgIdentifierExceptions(SerializerTestCase):

    def test_retrieve_by_non_registry_identifier_returns_none_when_given_elg_identifier(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project['project_identifier'] = list()
        project_id = dict()
        project_id['project_identifier_scheme'] = settings.REGISTRY_IDENTIFIER_SCHEMA
        project_id['value'] = "this_is_elg_value_id"
        json_data_generic_project['project_identifier'].append(project_id)
        json_data_generic_project['website'] = ['http://www.random-website.org']
        self.assertTrue(retrieve_by_non_registry_identifier(Project, json_data_generic_project,
                                                            get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_by_non_registry_identifier_returns_none_when_given_invalid_identifier(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project['project_identifier'] = list()
        project_id = dict()
        project_id['project_identifier_scheme'] = 'http://www.invalid_identifier.com/invalid'
        project_id['value'] = "this_is_a_value_of_an_invalid_identifier"
        json_data_generic_project['project_identifier'].append(project_id)
        json_data_generic_project['website'] = ['http://www.random-website.org']
        self.assertTrue(retrieve_by_non_registry_identifier(Project, json_data_generic_project,
                                                            get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_by_non_registry_identifier_returns_none_when_value_is_none(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = {
            'en': 'Project name to be ignored'}
        json_data_generic_project['project_identifier'] = list()
        project_id = dict()
        project_id['project_identifier_scheme'] = 'http://www.invalid_identifier.com/invalid'
        project_id['value'] = None
        json_data_generic_project['project_identifier'].append(project_id)
        json_data_generic_project['website'] = ['http://www.random-website.org']
        self.assertTrue(retrieve_by_non_registry_identifier(Project, json_data_generic_project,
                                                            get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)


class TestRetrieveDocumentByTitleExceptions(SerializerTestCase):

    def test_retrieve_document_by_title_returns_none_with_no_title(self):
        json_data_generic_document = dict()
        json_data_generic_document['document_identifier'] = list()
        document_id_3 = dict()
        document_id_3[
            'document_identifier_scheme'] = 'http://purl.org/spar/datacite/urn'
        document_id_3['value'] = "urn_value"
        json_data_generic_document['document_identifier'].append(
            document_id_3)
        self.assertTrue(
            retrieve_document_by_title(json_data_generic_document,
                                       get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_document_by_title_returns_none_with_empty_title(self):
        json_data_generic_document = dict()
        json_data_generic_document['title'] = None
        self.assertTrue(
            retrieve_document_by_title(json_data_generic_document,
                                       get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)


class TestRetrieveModelByOrganizationNameExceptions(SerializerTestCase):

    def test_retrieve_model_by_organization_name_returns_none_with_no_name(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        self.assertTrue(retrieve_model_by_organization_name(
            GenericOrganizationSerializer.Meta.model, json_data_generic_organization,
        get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_model_by_organization_name_returns_none_with_empty_name(self):
        json_data_generic_organization = dict()
        json_data_generic_organization['actor_type'] = 'Organization'
        json_data_generic_organization['organization_name'] = None
        json_data_generic_organization['website'] = ['http://www.tobeignored.com']
        self.assertTrue(retrieve_model_by_organization_name(
            GenericOrganizationSerializer.Meta.model, json_data_generic_organization,
        get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)


class TestRetrievePersonByFullNameExceptions(SerializerTestCase):

    def test_retrieve_person_by_full_name_returns_none_with_neither_of_the_names(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['email'] = ['tobe@ignored.com']
        self.assertTrue(retrieve_person_by_full_name(json_data_generic_person,
                                                     get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_person_by_full_name_returns_none_with_only_with_one_name(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = {
            'en': 'Person surname to be ignored'}
        json_data_generic_person['email'] = ['tobe@ignored.com']
        self.assertTrue(retrieve_person_by_full_name(json_data_generic_person,
                                                     get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_person_by_full_name_returns_none_with_empty_names(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = None
        json_data_generic_person['given_name'] = None
        json_data_generic_person['email'] = ['tobe@ignored.com']
        self.assertTrue(retrieve_person_by_full_name(json_data_generic_person,
                                                     get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_person_by_full_name_returns_none_with_only_with_one_empty_name(self):
        json_data_generic_person = dict()
        json_data_generic_person['actor_type'] = 'Person'
        json_data_generic_person['surname'] = None
        json_data_generic_person['email'] = ['tobe@ignored.com']
        self.assertTrue(retrieve_person_by_full_name(json_data_generic_person,
                                                     get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)


class TestRetrieveLRByResourceNameExceptions(SerializerTestCase):

    def test_retrieve_lr_by_resource_name_returns_none_with_no_resource_name(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['version'] = 'unspecified'
        self.assertTrue(retrieve_lr_by_resource_name(json_data_generic_lr,
                                                     get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_lr_by_resource_name_returns_none_with_empty_resource_name(self):
        json_data_generic_lr = dict()
        json_data_generic_lr['resource_name'] = None
        json_data_generic_lr['version'] = 'unspecified'
        self.assertTrue(retrieve_lr_by_resource_name(json_data_generic_lr,
                                                     get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)


class TestRetrieveProjectByGrantNumberOrNameExceptions(SerializerTestCase):

    def test_retrieve_project_by_grant_number_or_name_returns_none_with_no_grant_number_or_project_name(self):
        json_data_generic_project = dict()
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        self.assertTrue(retrieve_project_by_grant_number_or_name(json_data_generic_project,
                                                                 get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_project_by_grant_number_or_name_returns_none_with_empty_project_name_and_no_grant_number(self):
        json_data_generic_project = dict()
        json_data_generic_project['project_name'] = None
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        self.assertTrue(retrieve_project_by_grant_number_or_name(json_data_generic_project,
                                                                 get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_project_by_grant_number_or_name_returns_none_with_empty_grant_number(self):
        json_data_generic_project = dict()
        json_data_generic_project['grant_number'] = None
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        self.assertTrue(retrieve_project_by_grant_number_or_name(json_data_generic_project,
                                                                 get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_project_by_grant_number_or_name_returns_none_with_empty_name_and_grant_number(self):
        json_data_generic_project = dict()
        json_data_generic_project['grant_number'] = None
        json_data_generic_project['project_name'] = None
        json_data_generic_project['website'] = ['http://www.notimportant.org']
        self.assertTrue(retrieve_project_by_grant_number_or_name(json_data_generic_project,
                                                                 get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)


class TestRetrieveLicenceByLicenceTermsAndNameExceptions(SerializerTestCase):

    def test_retrieve_licence_by_licence_terms_and_name_with_no_url(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {'en': 'licence term name'}
        self.assertTrue(retrieve_licence_by_licence_terms_and_name(json_data_generic_licence_terms,
                                                                   get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_licence_by_licence_terms_and_name_with_empty_url(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = {'en': 'licence term name'}
        json_data_generic_licence_terms['licence_terms_url'] = None
        self.assertTrue(retrieve_licence_by_licence_terms_and_name(json_data_generic_licence_terms,
                                                                   get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)


class TestRetrieveLicenceTermsGivenOnlyNameExceptions(SerializerTestCase):

    def test_retrieve_licence_terms_only_by_name_with_no_name(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_url'] = ['http://www.random.url']
        self.assertTrue(retrieve_licence_terms_given_only_name(json_data_generic_licence_terms,
                                                               get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)

    def test_retrieve_licence_terms_only_by_name_with_empty_name(self):
        json_data_generic_licence_terms = dict()
        json_data_generic_licence_terms['licence_terms_name'] = None
        json_data_generic_licence_terms['licence_terms_url'] = ['http://www.random.url']
        self.assertTrue(retrieve_licence_terms_given_only_name(json_data_generic_licence_terms,
                                                               get_user_model().objects.get(username=settings.SYSTEM_USER)) is None)


class TestMeetSingleEntityRetrievalConditions(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for Project
        json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()
        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('first Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')
        json_data_published = json.loads(cls.json_str)
        json_data_published['described_entity']['project_name'] = {
            "en": "European Live Translator 2"
        }
        json_data_published['described_entity']['project_short_name'] = {"en": "eli"}
        json_data_published['described_entity']['project_identifier'] = []
        json_data_published['described_entity']['website'] = []
        json_data_published['described_entity']['grant_number'] = ''
        cls.serializer_published = MetadataRecordSerializer(data=json_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info('published Errors {}'.format(cls.serializer_published.errors))
        LOGGER.info('Setup has finished')
        gen_data = {
            "project_name": {
                "en": "Other proj"
            },
            "project_identifier": [
                {
                    "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                    "value": "this_is_elg_value_id_number_two"
                },
                {
                    "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                    "value": "doi_value_id_two"
                }
            ],
            "website": [
                "https://elitr2.eu/long/slug/test/",
                "http://www.secondurl2.eu/"
            ],
            "grant_number": "82546032",
            "funding_type": [
                "http://w3id.org/meta-share/meta-share/euFunds"
            ],
            "funder": [
                {
                    "actor_type": "Organization",
                    "organization_name": {
                        "en": "European Commission"
                    },
                    "organization_identifier": [
                        {
                            "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "doi_value"
                        }
                    ],
                    "website": [
                        "http://www.european-commission.eu"
                    ]
                },
                {
                    "actor_type": "Person",
                    "surname": {
                        "en": "Giagkou"
                    },
                    "given_name": {
                        "en": "Maria"
                    },
                    "email": [
                        "mgiagkou@athenarc.gr"
                    ]
                },
                {
                    "actor_type": "Group",
                    "organization_name": {
                        "en": "Institute Rockerfeller"
                    },
                    "group_identifier": [
                        {
                            "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "doi_value"
                        }
                    ],
                    "website": [
                        "http://www.institute-rockerfeller.org"
                    ]
                }
            ]
        }
        gen_serializer = GenericProjectSerializer(data=gen_data)
        if gen_serializer.is_valid():
            cls.gen_instance = gen_serializer.save()
        else:
            LOGGER.info('get errors {}'.format(gen_serializer.errors))

    def test_published_gets_retrieved(self):
        self.assertTrue(meet_conditions_to_retrieve_single_entity(self.instance_published.described_entity,
                                                                  get_user_model().objects.get(username=settings.SYSTEM_USER)))

    def test_non_published_does_not_get_retrieved(self):
        self.assertFalse(meet_conditions_to_retrieve_single_entity(self.instance.described_entity,
                                                                   get_user_model().objects.get(username=settings.SYSTEM_USER)))

    def test_generic_gets_retrieved(self):
        self.assertTrue(meet_conditions_to_retrieve_single_entity(self.gen_instance,
                                                                  get_user_model().objects.get(username=settings.SYSTEM_USER)))


class TestMeetModelPKRetrievalConditions(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for Project
        json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()
        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')
        json_data_published = json.loads(cls.json_str)
        json_data_published['described_entity']['project_name'] = {
            "en": "European Live Translator 2"
        }
        json_data_published['described_entity']['project_short_name'] = {"en": "eli"}
        json_data_published['described_entity']['project_identifier'] = []
        json_data_published['described_entity']['website'] = []
        json_data_published['described_entity']['grant_number'] = ''
        cls.serializer_published = MetadataRecordSerializer(data=json_data_published)
        if cls.serializer_published.is_valid():
            cls.instance_published = cls.serializer_published.save()
            cls.instance_published.management_object.status = 'p'
            cls.instance_published.management_object.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer_published.errors))
        LOGGER.info('Setup has finished')
        gen_data = {
            "project_name": {
                "en": "Other proj"
            },
            "project_identifier": [
                {
                    "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                    "value": "this_is_elg_value_id_number_two"
                },
                {
                    "project_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                    "value": "doi_value_id_two"
                }
            ],
            "website": [],
            "grant_number": "",
            "funding_type": [
                "http://w3id.org/meta-share/meta-share/euFunds"
            ],
            "funder": [
                {
                    "actor_type": "Organization",
                    "organization_name": {
                        "en": "European Commission"
                    },
                    "organization_identifier": [
                        {
                            "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "doi_value"
                        }
                    ],
                    "website": [
                        "http://www.european-commission.eu"
                    ]
                },
                {
                    "actor_type": "Person",
                    "surname": {
                        "en": "Giagkou"
                    },
                    "given_name": {
                        "en": "Maria"
                    },
                    "email": [
                        "mgiagkou@athenarc.gr"
                    ]
                },
                {
                    "actor_type": "Group",
                    "organization_name": {
                        "en": "Institute Rockerfeller"
                    },
                    "group_identifier": [
                        {
                            "organization_identifier_scheme": "http://purl.org/spar/datacite/doi",
                            "value": "doi_value"
                        }
                    ],
                    "website": [
                        "http://www.institute-rockerfeller.org"
                    ]
                }
            ]
        }
        gen_serializer = GenericProjectSerializer(data=gen_data)
        if gen_serializer.is_valid():
            cls.gen_instance = gen_serializer.save()
        else:
            LOGGER.info(gen_serializer.errors)
        cls.json_data = {
            'category_label': {'en': 'telecommunications'},
            'domain_identifier': {
                'domain_classification_scheme': 'http://w3id.org/meta-share/meta-share/ELG_domainClassification',
                'value': 'ELG_Domain004',
            }
        }

        cls.domain_serializer = DomainSerializer(data=cls.json_data)
        if cls.domain_serializer.is_valid():
            cls.domain_instance = cls.domain_serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.domain_serializer.errors))
        LOGGER.info('Setup has finished')

    def test_published_gets_retrieved(self):
        self.assertTrue(meet_conditions_to_retrieve_pk_of_model(self.instance_published.described_entity, 'Project',
                                                                get_user_model().objects.get(
                                                                    username=settings.SYSTEM_USER)))

    def test_non_published_does_not_get_retrieved(self):
        self.assertFalse(meet_conditions_to_retrieve_pk_of_model(self.instance.described_entity, 'Project',
                                                                 get_user_model().objects.get(
                                                                     username=settings.SYSTEM_USER)))

    def test_generic_gets_retrieved(self):
        self.assertTrue(meet_conditions_to_retrieve_pk_of_model(self.gen_instance, 'Project',
                                                                get_user_model().objects.get(
                                                                    username=settings.SYSTEM_USER)                                                                ))

    def test_domain_like_gets_retrieved(self):
        self.assertTrue(meet_conditions_to_retrieve_pk_of_model(self.domain_instance, 'Domain',
                                                                get_user_model().objects.get(
                                                                    username=settings.SYSTEM_USER)))
