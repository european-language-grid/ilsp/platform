from django.contrib.auth import get_user_model
from django.test import RequestFactory

from management.models import (
    INTERNAL, DRAFT, INGESTED, PUBLISHED, UNPUBLISHED
)
from registry.models import MetadataRecord
from registry.permissions import BatchExportXMLRecordPermission
from test_utils import get_test_user, EndpointTestCase, import_metadata


class BatchExportXMLRecordPermissionTest(EndpointTestCase):
    json_test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.json_test_data)
        cls.factory = RequestFactory()

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def test_xml_retrieve_admin_not_permitted_draft_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = DRAFT
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.admin
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_admin_permitted_internal_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.admin
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_admin_permitted_ingested_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INGESTED
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.admin
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_admin_permitted_published_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = PUBLISHED
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.admin
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_admin_permitted_unpublished_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = UNPUBLISHED
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.admin
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_admin_permitted_deleted_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.deleted = True
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.admin
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))
        obj.management_object.deleted = False
        obj.management_object.save()

    def test_xml_retrieve_provider_not_permitted_own_draft_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = DRAFT
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.provider
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_provider_permitted_own_internal_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.provider
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_provider_permitted_own_ingested_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INGESTED
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.provider
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_provider_permitted_own_published_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = PUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.provider
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_provider_permitted_own_unpublished_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = UNPUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.provider
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_provider_not_permitted_own_deleted_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.deleted = True
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.provider
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))
        obj.management_object.deleted = False
        obj.management_object.save()

    def test_xml_retrieve_content_manager_not_permitted_draft_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = DRAFT
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.c_m
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_content_manager_permitted_internal_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.c_m
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_content_manager_permitted_ingested_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INGESTED
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.c_m
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_content_manager_permitted_published_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = PUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.c_m
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_content_manager_permitted_unpublished_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = UNPUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.c_m
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_content_manager_permitted_deleted_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.deleted = True
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.c_m
        permission_check = BatchExportXMLRecordPermission()
        self.assertTrue(permission_check.has_object_permission(request, None, obj))
        obj.management_object.deleted = False
        obj.management_object.save()

    def test_xml_retrieve_consumer_not_permitted_draft_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = DRAFT
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.consumer
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_consumer_not_permitted_internal_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.consumer
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_consumer_not_permitted_ingested_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INGESTED
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.consumer
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_consumer_not_permitted_published_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = PUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.consumer
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_consumer_not_permitted_unpublished_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = UNPUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.consumer
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_consumer_not_permitted_deleted_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.deleted = True
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.consumer
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))
        obj.management_object.deleted = False
        obj.management_object.save()

    def test_xml_retrieve_legal_validator_not_permitted_draft_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = DRAFT
        obj.management_object.curator = self.provider
        obj.management_object.legal_validator = self.legal_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.legal_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_legal_validator_not_permitted_internal_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.legal_validator = self.legal_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.legal_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_legal_validator_not_permitted_ingested_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INGESTED
        obj.management_object.curator = self.provider
        obj.management_object.legal_validator = self.legal_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.legal_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_legal_validator_not_permitted_published_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = PUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.legal_validator = self.legal_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.legal_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_legal_validator_not_permitted_unpublished_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = UNPUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.legal_validator = self.legal_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.legal_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_legal_validator_not_permitted_deleted_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.legal_validator = self.legal_val
        obj.management_object.deleted = True
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.legal_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))
        obj.management_object.deleted = False
        obj.management_object.save()

    def test_xml_retrieve_meta_validator_not_permitted_draft_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = DRAFT
        obj.management_object.curator = self.provider
        obj.management_object.metadata_validator = self.meta_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.meta_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_meta_validator_not_permitted_internal_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.metadata_validator = self.meta_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.meta_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_meta_validator_not_permitted_ingested_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INGESTED
        obj.management_object.curator = self.provider
        obj.management_object.metadata_validator = self.meta_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.meta_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_meta_validator_not_permitted_published_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = PUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.metadata_validator = self.meta_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.meta_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_meta_validator_not_permitted_unpublished_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = UNPUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.metadata_validator = self.meta_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.meta_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_meta_validator_not_permitted_deleted_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.metadata_validator = self.meta_val
        obj.management_object.deleted = True
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.meta_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))
        obj.management_object.deleted = False
        obj.management_object.save()

    def test_xml_retrieve_tech_validator_not_permitted_draft_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = DRAFT
        obj.management_object.curator = self.provider
        obj.management_object.technical_validator = self.tech_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.tech_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_tech_validator_not_permitted_internal_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.technical_validator = self.tech_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.tech_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_tech_validator_not_permitted_ingested_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INGESTED
        obj.management_object.curator = self.provider
        obj.management_object.technical_validator = self.tech_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.tech_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_tech_validator_not_permitted_published_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = PUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.technical_validator = self.tech_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.tech_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_tech_validator_not_permitted_unpublished_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = UNPUBLISHED
        obj.management_object.curator = self.provider
        obj.management_object.technical_validator = self.tech_val
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.tech_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))

    def test_xml_retrieve_tech_validator_not_permitted_deleted_record(self):
        obj = self.metadata_records[0]
        obj.management_object.status = INTERNAL
        obj.management_object.curator = self.provider
        obj.management_object.technical_validator = self.tech_val
        obj.management_object.deleted = True
        obj.management_object.save()
        request = self.factory.get(f'/{obj.pk}/')
        request.user = self.tech_val
        permission_check = BatchExportXMLRecordPermission()
        self.assertFalse(permission_check.has_object_permission(request, None, obj))
        obj.management_object.deleted = False
        obj.management_object.save()

