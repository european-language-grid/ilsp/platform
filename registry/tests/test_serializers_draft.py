import copy
import json
import logging

from django.conf import settings
from test_utils import SerializerTestCase
from rest_framework.exceptions import ValidationError

import registry.serializers as r_s

LOGGER = logging.getLogger(__name__)


class TestDraftSerializerCreation(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.org_json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.org_json_str = cls.org_json_file.read()
        cls.org_json_file.close()
        cls.org_raw_data = json.loads(cls.org_json_str)['described_entity']
        cls.group_json_file = open('registry/tests/fixtures/group.json', 'r')
        cls.group_json_str = cls.group_json_file.read()
        cls.group_json_file.close()
        cls.group_raw_data = json.loads(cls.group_json_str)['described_entity']
        cls.per_json_file = open('registry/tests/fixtures/person.json', 'r')
        cls.per_json_str = cls.per_json_file.read()
        cls.per_json_file.close()
        cls.per_raw_data = json.loads(cls.per_json_str)['described_entity']
        cls.prj_json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.prj_json_str = cls.prj_json_file.read()
        cls.prj_json_file.close()
        cls.prj_raw_data = json.loads(cls.prj_json_str)['described_entity']
        cls.lt_json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        cls.lt_json_str = cls.lt_json_file.read()
        cls.lt_json_file.close()
        cls.lt_raw_data = json.loads(cls.lt_json_str)['described_entity']
        cls.doc_json_file = open('registry/tests/fixtures/document.json', 'r')
        cls.doc_json_str = cls.doc_json_file.read()
        cls.doc_json_file.close()
        cls.doc_raw_data = json.loads(cls.doc_json_str)['described_entity']
        cls.lr_json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.lr_json_str = cls.lr_json_file.read()
        cls.lr_json_file.close()
        cls.lr_raw_data = json.loads(cls.lr_json_str)['described_entity']
        cls.repo_json_file = open('registry/tests/fixtures/repository.json', 'r')
        cls.repo_json_str = cls.repo_json_file.read()
        cls.repo_json_file.close()
        cls.repo_raw_data = json.loads(cls.repo_json_str)['described_entity']

    def test_draft_serializer_fails_when_no_name_org(self):
        raw_data = copy.deepcopy(self.org_raw_data)
        raw_data['organization_name'] = None
        serializer = r_s.OrganizationSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_name_gen_org(self):
        raw_data = copy.deepcopy(self.org_raw_data)
        raw_data['organization_name'] = None
        serializer = r_s.GenericOrganizationSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_name_group(self):
        raw_data = copy.deepcopy(self.group_raw_data)
        raw_data['organization_name'] = None
        serializer = r_s.GroupSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_name_gen_group(self):
        raw_data = copy.deepcopy(self.group_raw_data)
        raw_data['organization_name'] = None
        serializer = r_s.GenericGroupSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_given_name_person(self):
        raw_data = copy.deepcopy(self.per_raw_data)
        raw_data['given_name'] = None
        serializer = r_s.PersonSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_surname_person(self):
        raw_data = copy.deepcopy(self.per_raw_data)
        raw_data['surname'] = None
        serializer = r_s.PersonSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_surname_or_given_name_person(self):
        raw_data = copy.deepcopy(self.per_raw_data)
        raw_data['given_name'] = None
        raw_data['surname'] = None
        serializer = r_s.PersonSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_given_name_gen_person(self):
        raw_data = copy.deepcopy(self.per_raw_data)
        raw_data['given_name'] = None
        serializer = r_s.GenericPersonSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_surname_gen_person(self):
        raw_data = copy.deepcopy(self.per_raw_data)
        raw_data['surname'] = None
        serializer = r_s.GenericPersonSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_surname_or_given_name_gen_person(self):
        raw_data = copy.deepcopy(self.per_raw_data)
        raw_data['given_name'] = None
        raw_data['surname'] = None
        serializer = r_s.GenericPersonSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_name_project(self):
        raw_data = copy.deepcopy(self.prj_raw_data)
        raw_data['project_name'] = None
        serializer = r_s.ProjectSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_project_name_gen_project(self):
        raw_data = copy.deepcopy(self.prj_raw_data)
        raw_data['project_name'] = None
        serializer = r_s.GenericProjectSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_name_licence_terms(self):
        raw_data = copy.deepcopy(self.lt_raw_data)
        raw_data['licence_terms_name'] = None
        serializer = r_s.LicenceTermsSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_project_name_gen_licence_terms(self):
        raw_data = copy.deepcopy(self.lt_raw_data)
        raw_data['licence_terms_name'] = None
        serializer = r_s.GenericLicenceTermsSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_title_document(self):
        raw_data = copy.deepcopy(self.doc_raw_data)
        raw_data['title'] = None
        serializer = r_s.DocumentSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_title_gen_document(self):
        raw_data = copy.deepcopy(self.doc_raw_data)
        raw_data['title'] = None
        serializer = r_s.GenericDocumentSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_name_lr(self):
        raw_data = copy.deepcopy(self.lr_raw_data)
        raw_data['resource_name'] = None
        serializer = r_s.LanguageResourceSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_name_gen_lr(self):
        raw_data = copy.deepcopy(self.lr_raw_data)
        raw_data['resource_name'] = None
        serializer = r_s.GenericLanguageResourceSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_name_repository(self):
        raw_data = copy.deepcopy(self.repo_raw_data)
        raw_data['repository_name'] = None
        serializer = r_s.RepositorySerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_name_gen_repository(self):
        raw_data = copy.deepcopy(self.repo_raw_data)
        raw_data['repository_name'] = None
        serializer = r_s.GenericRepositorySerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_lr_subclass_described_entity(self):
        raw_data = copy.deepcopy(self.lr_raw_data)
        raw_data['lr_subclass'] = {}
        serializer = r_s.DescribedEntitySerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_described_entity_metadata_record(self):
        raw_data = json.loads(self.lr_json_str)
        raw_data['described_entity'] = {}
        serializer = r_s.MetadataRecordSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_passes_when_not_syntactically_valid(self):
        raw_data = copy.deepcopy(self.lr_raw_data)
        raw_data['lr_subclass']['software_distribution'][0]['software_distribution_form'] = None
        serializer = r_s.LanguageResourceSerializer(data=raw_data, context={'draft': True})
        if serializer.is_valid():
            new_instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        self.assertTrue(serializer.is_valid())


class TestDraftSerializerUpdate(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.lr_json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.lr_json_str = cls.lr_json_file.read()
        cls.lr_json_file.close()
        cls.lr_raw_data = json.loads(cls.lr_json_str)['described_entity']
        draft_serializer = r_s.LanguageResourceSerializer(data=cls.lr_raw_data, context={'draft': True})
        if draft_serializer.is_valid():
            cls.draft_instance = draft_serializer.save()
        else:
            LOGGER.info(draft_serializer.errors)
        LOGGER.info('Setup has finished')

    def test_update_draft_serializer_fails_when_no_entity_name(self):
        raw_data = copy.deepcopy(self.lr_raw_data)
        raw_data['resource_name'] = {}
        serializer = r_s.LanguageResourceSerializer(self.draft_instance, data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_update_draft_serializer_fails_when_no_lr_subclass_described_entity(self):
        raw_data = copy.deepcopy(self.lr_raw_data)
        raw_data['lr_subclass'] = {}
        serializer = r_s.DescribedEntitySerializer(self.draft_instance, data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_draft_serializer_fails_when_no_described_entity_metadata_record(self):
        raw_data = json.loads(self.lr_json_str)
        mdr_serializer = r_s.MetadataRecordSerializer(data=raw_data, context={'draft': True})
        if mdr_serializer.is_valid():
            mdr_instance = mdr_serializer.save()
        else:
            LOGGER.info(mdr_serializer.errors)
        LOGGER.info('Setup has finished')
        raw_data['described_entity'] = {}
        serializer = r_s.MetadataRecordSerializer(mdr_instance, data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_update_draft_serializer_passes_when_not_syntactically_valid(self):
        raw_data = copy.deepcopy(self.lr_raw_data)
        raw_data['lr_subclass']['software_distribution'][0]['software_distribution_form'] = None
        serializer = r_s.LanguageResourceSerializer(self.draft_instance, data=raw_data, context={'draft': True})
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertTrue(serializer.is_valid())
