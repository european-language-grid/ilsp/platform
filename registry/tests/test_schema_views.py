import json
import logging

from django.conf import settings
from test_utils import api_call, get_test_user, EndpointTestCase

LOGGER = logging.getLogger(__name__)


PER_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/described_entity/person/'
ORG_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/described_entity/organization/'
GROUP_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/described_entity/group/'
PRJ_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/described_entity/project/'
LC_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/described_entity/licence/'
DOC_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/described_entity/document/'
LR_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/described_entity/lr/'
GP_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/actor/generic_person/'
GO_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/actor/generic_organization/'
GG_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/actor/generic_group/'
CORPUS_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/lr_subclass/corpus/'
GRAMMAR_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/ld_subclass/grammar/'
NGRAM_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/ld_subclass/ngram_model/'
MLMODEL_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/ld_subclass/model/'
LDIMG_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/language_description_image_part/'
LDVID_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/language_description_video_part/'
LDTXT_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/language_description_text_part/'
LCRIMG_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/lcr_image_part/'
LCRVID_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/lcr_video_part/'
LCRAUDIO_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/lcr_audio_part/'
LCRTXT_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/lcr_text_part/'
CRPNUM_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/corpus_text_numerical_part/'
CRPIMG_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/corpus_image_part/'
CRPVID_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/corpus_video_part/'
CRPAUDIO_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/corpus_audio_part/'
CRPTXT_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/media_part/corpus_text_part/'
LRS_TOOL_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/lr_subclass/tool_service/'
LRS_LANG_DESC_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/lr_subclass/language_description/'
LRS_LCR_SCHEMA_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema/lr_subclass/lcr/'
SCHEMA_2_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/schema2/'


class TestLRSLanguageDescriptionSchemaEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/ld_grammar.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']['lr_subclass']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_lang_desc_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LRS_LANG_DESC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token),
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(
            response.get('json_content')['language_description_subclass'][
                'encoding_level'],
            self.raw_data['language_description_subclass']['encoding_level'])

    def test_create_lang_desc_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['language_description_media_part'][0][
            'ld_media_type'] = 'invalid'
        response = api_call('POST', LRS_LANG_DESC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token),
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['language_description_media_part'][0][
            'ld_media_type'] = 'LanguageDescriptionTextPart'

    def test_cannot_create_lang_desc_schema_anonymous(self):
        response = api_call('POST', LRS_LANG_DESC_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lang_desc_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LRS_LANG_DESC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token),
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lang_desc_schema_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LRS_LANG_DESC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token),
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lang_desc_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LRS_LANG_DESC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token),
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(
            response.get('json_content')['language_description_subclass'][
                'encoding_level'],
            self.raw_data['language_description_subclass']['encoding_level'])

    def test_create_lang_desc_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LRS_LANG_DESC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token),
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(
            response.get('json_content')['language_description_subclass'][
                'encoding_level'],
            self.raw_data['language_description_subclass']['encoding_level'])


class TestLCRSchemaEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/lcr.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']['lr_subclass']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_lcr_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LRS_LCR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['lcr_subclass'],
                          self.raw_data['lcr_subclass'])

    def test_create_lcr_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['lcr_subclass'] = 'subclass'
        response = api_call('POST', LRS_LCR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'lcr_subclass'] = 'http://w3id.org/meta-share/meta-share/annotationScheme'

    def test_cannot_create_lcr_schema_anonymous(self):
        response = api_call('POST', LRS_LCR_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LRS_LCR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LRS_LCR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LRS_LCR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)

    def test_create_lcr_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LRS_LCR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['lcr_subclass'],
                          self.raw_data['lcr_subclass'])


class TestSchemaPersonEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/person.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_person_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', PER_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['surname'],
                          self.raw_data['surname'])

    def test_options_person_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', PER_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['fields'])

    def test_create_person_schema_fails_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['surname'] = 'Voukoutis'
        response = api_call('POST', PER_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['surname'] = {
            'en': 'Voukoutis'
        }

    def test_cannot_create_person_schema_anonymous(self):
        response = api_call('POST', PER_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_person_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', PER_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_person_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', PER_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_person_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', PER_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['surname'],
                          self.raw_data['surname'])

    def test_create_person_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', PER_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['surname'],
                          self.raw_data['surname'])


class TestSchemaOrganizationEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/organization.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_organization_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', ORG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])

    def test_options_organization_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', ORG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['fields'])

    def test_create_organization_schema_fails_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['organization_name'] = 'org'
        response = api_call('POST', ORG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['organization_name'] = {
            'en': 'Organization'
        }

    def test_cannot_create_organization_schema_anonymous(self):
        response = api_call('POST', ORG_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_organization_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', ORG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_organization_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', ORG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_organization_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', ORG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])

    def test_create_organization_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', ORG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])


class TestSchemaGroupEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/group.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_group_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', GROUP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])

    def test_options_group_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', GROUP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['fields'])

    def test_create_group_schema_fails_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['organization_name'] = 'org'
        response = api_call('POST', GROUP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['organization_name'] = {
            'en': 'Organization'
        }

    def test_cannot_create_group_schema_anonymous(self):
        response = api_call('POST', GROUP_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_group_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', GROUP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_group_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', GROUP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_group_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', GROUP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])

    def test_create_group_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', GROUP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])


class TestSchemaProjectEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/project.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_project_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', PRJ_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['project_name'],
                          self.raw_data['project_name'])

    def test_options_project_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', PRJ_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['fields'])

    def test_create_project_schema_fails_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['project_name'] = 'proj'
        response = api_call('POST', PRJ_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['project_name'] = {
            'en': 'Project'
        }

    def test_cannot_create_project_schema_anonymous(self):
        response = api_call('POST', PRJ_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_project_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', PRJ_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_project_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', PRJ_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_project_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', PRJ_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['project_name'],
                          self.raw_data['project_name'])

    def test_create_project_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', PRJ_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['project_name'],
                          self.raw_data['project_name'])


class TestSchemaLicenceEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_licence_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['licence_terms_name'],
                          self.raw_data['licence_terms_name'])

    def test_options_licence_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', LC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['fields'])

    def test_create_licence_schema_fails_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['licence_terms_name'] = 'licence'
        response = api_call('POST', LC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['licence_terms_name'] = {
            'en': 'Licence_name'
        }

    def test_cannot_create_licence_schema_anonymous(self):
        response = api_call('POST', LC_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_licence_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_licence_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_licence_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['licence_terms_name'],
                          self.raw_data['licence_terms_name'])

    def test_create_licence_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['licence_terms_name'],
                          self.raw_data['licence_terms_name'])


class TestSchemaDocumentEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/document.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_document_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', DOC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['title'],
                          self.raw_data['title'])

    def test_options_licence_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', DOC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['fields'])

    def test_create_document_schema_fails_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['title'] = 'title'
        response = api_call('POST', DOC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['title'] = {
            'en': 'Document_title'
        }

    def test_cannot_create_document_schema_anonymous(self):
        response = api_call('POST', DOC_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_document_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', DOC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_document_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', DOC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_document_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', DOC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['title'],
                          self.raw_data['title'])

    def test_create_document_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', DOC_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['title'],
                          self.raw_data['title'])


class TestSchemaLanguageResourceEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/tool.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_lr_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['resource_name'],
                          self.raw_data['resource_name'])

    def test_options_lr_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', LR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('json_content')['fields'])

    def test_create_lr_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['resource_name'] = 'resource name'
        response = api_call('POST', LR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['resource_name'] = {
            'en': 'lr_name'
        }

    def test_cannot_create_lr_schema_anonymous(self):
        response = api_call('POST', LR_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lr_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lr_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lr_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['resource_name'],
                          self.raw_data['resource_name'])

    def test_create_lr_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['resource_name'],
                          self.raw_data['resource_name'])


class TestGenericPersonSchemaEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/person.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_generic_person_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', GP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['surname'],
                          self.raw_data['surname'])

    def test_create_generic_person_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['surname'] = 'voukoutis'
        response = api_call('POST', GP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['surname'] = {
            'en': 'Voukoutis'
        }

    def test_cannot_create_generic_person_schema_anonymous(self):
        response = api_call('POST', GP_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_generic_person_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', GP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_generic_person_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', GP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_generic_person_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', GP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['surname'],
                          self.raw_data['surname'])

    def test_create_generic_person_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', GP_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['surname'],
                          self.raw_data['surname'])


class TestGenericOrganizationSchemaEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/organization.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_generic_organization_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', GO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])

    def test_options_generic_organization_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', GO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)

    def test_create_generic_organization_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['organization_name'] = 'org'
        response = api_call('POST', GO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['organization_name'] = {
            'en': 'Organization'
        }

    def test_cannot_create_generic_organization_schema_anonymous(self):
        response = api_call('POST', GO_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_generic_organization_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', GO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_generic_organization_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', GO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_generic_organization_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', GO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])

    def test_create_generic_organization_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', GO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])


class TestGenericGroupSchemaEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/group.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_generic_group_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', GG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])

    def test_create_generic_group_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['organization_name'] = 'group'
        response = api_call('POST', GG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['organization_name'] = {
            'en': 'Group'
        }

    def test_cannot_create_generic_group_schema_anonymous(self):
        response = api_call('POST', GG_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_generic_group_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', GG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_generic_group_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', GG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_generic_group_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', GG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])

    def test_create_generic_group_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', GG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['organization_name'],
                          self.raw_data['organization_name'])


class TestCorpusSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['corpus']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_corpus_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', CORPUS_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['corpus_subclass'],
                          self.raw_data['corpus_subclass'])

    def test_create_corpus_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['corpus_subclass'] = 'subclass'
        response = api_call('POST', CORPUS_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'corpus_subclass'] = 'http://w3id.org/meta-share/meta-share/annotatedCorpus'

    def test_cannot_create_corpus_schema_anonymous(self):
        response = api_call('POST', CORPUS_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', CORPUS_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', CORPUS_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', CORPUS_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['corpus_subclass'],
                          self.raw_data['corpus_subclass'])

    def test_create_corpus_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', CORPUS_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['corpus_subclass'],
                          self.raw_data['corpus_subclass'])


class TestGrammarSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['grammar']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_grammar_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', GRAMMAR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['encoding_level'],
                          self.raw_data['encoding_level'])

    def test_create_grammar_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['encoding_level'] = 'encoding'
        response = api_call('POST', GRAMMAR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['encoding_level'] = [
            'http://w3id.org/meta-share/meta-share/morphology',
            'http://w3id.org/meta-share/meta-share/other'
        ]

    def test_cannot_create_grammar_schema_anonymous(self):
        response = api_call('POST', GRAMMAR_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_grammar_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', GRAMMAR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_grammar_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', GRAMMAR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_grammar_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', GRAMMAR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['encoding_level'],
                          self.raw_data['encoding_level'])

    def test_create_grammar_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', GRAMMAR_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['encoding_level'],
                          self.raw_data['encoding_level'])


class TestMLModelSchemaEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/ld_model.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']['lr_subclass'][
        'language_description_subclass']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_ml_model_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', MLMODEL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(
            response.get('json_content')['training_corpus_details'],
            self.raw_data['training_corpus_details'])

    def test_create_ml_model_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['training_corpus_details'] = 'details'
        response = api_call('POST', MLMODEL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['training_corpus_details'] = {
            'en': 'model details'
        }

    def test_cannot_create_ml_model_schema_anonymous(self):
        response = api_call('POST', MLMODEL_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ml_model_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', MLMODEL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ml_model_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', MLMODEL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ml_model_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', MLMODEL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(
            response.get('json_content')['training_corpus_details'],
            self.raw_data['training_corpus_details'])

    def test_create_ml_model_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', MLMODEL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(
            response.get('json_content')['training_corpus_details'],
            self.raw_data['training_corpus_details'])


class TestLDImageSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['ld_image_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_ld_image_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LDIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['multilinguality_type'],
                          self.raw_data['multilinguality_type'])

    def test_create_ld_image_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'invalid type'
        response = api_call('POST', LDIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'multilinguality_type'] = 'http://w3id.org/meta-share/meta-share/other'

    def test_cannot_create_ld_image_schema_anonymous(self):
        response = api_call('POST', LDIMG_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ld_image_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LDIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ld_image_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LDIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ld_image_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LDIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['multilinguality_type'],
                          self.raw_data['multilinguality_type'])

    def test_create_ld_image_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LDIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['multilinguality_type'],
                          self.raw_data['multilinguality_type'])


class TestLDVideoSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['ld_video_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_ld_video_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LDVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['media_type'],
                          self.raw_data['media_type'])

    def test_create_ld_video_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'invalid type'
        response = api_call('POST', LDVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'multilinguality_type'] = 'http://w3id.org/meta-share/meta-share/other'

    def test_cannot_create_ld_video_schema_anonymous(self):
        response = api_call('POST', LDVID_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ld_video_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LDVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ld_video_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LDVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ld_video_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LDVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['multilinguality_type'],
                          self.raw_data['multilinguality_type'])

    def test_create_ld_video_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LDVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['multilinguality_type'],
                          self.raw_data['multilinguality_type'])


class TestLDTextSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['ld_text_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_ld_text_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LDTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['multilinguality_type'],
                          self.raw_data['multilinguality_type'])

    def test_create_ld_text_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'invalid type'
        response = api_call('POST', LDTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'multilinguality_type'] = 'http://w3id.org/meta-share/meta-share/other'

    def test_cannot_create_ld_text_schema_anonymous(self):
        response = api_call('POST', LDTXT_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ld_text_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LDTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ld_text_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LDTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_ld_text_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LDTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['multilinguality_type'],
                          self.raw_data['multilinguality_type'])

    def test_create_ld_text_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LDTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['multilinguality_type'],
                          self.raw_data['multilinguality_type'])


class TestLCRImageSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['lcr_image_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_lcr_image_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LCRIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_lcr_image_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'bilingual'
        response = api_call('POST', LCRIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/bilingual'

    def test_cannot_create_lcr_image_schema_anonymous(self):
        response = api_call('POST', LCRIMG_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_image_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LCRIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_image_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LCRIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_image_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LCRIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_lcr_image_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LCRIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])


class TestLCRVideoSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['lcr_video_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_lcr_video_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LCRVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_lcr_video_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'bilingual'
        response = api_call('POST', LCRVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/bilingual'

    def test_cannot_create_lcr_video_schema_anonymous(self):
        response = api_call('POST', LCRVID_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_video_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LCRVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_video_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LCRVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_video_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LCRVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_lcr_video_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LCRVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])


class TestLCRAudioSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['lcr_audio_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_lcr_audio_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LCRAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_lcr_audio_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'bilingual'
        response = api_call('POST', LCRAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/bilingual'

    def test_cannot_create_lcr_audio_schema_anonymous(self):
        response = api_call('POST', LCRAUDIO_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_audio_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LCRAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_audio_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LCRAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_audio_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LCRAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_lcr_audio_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LCRAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])


class TestLCRTextSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['lcr_text_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_lcr_text_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LCRTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_lcr_text_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'bilingual'
        response = api_call('POST', LCRTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/bilingual'

    def test_cannot_create_lcr_text_schema_anonymous(self):
        response = api_call('POST', LCRTXT_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_text_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LCRTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_text_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LCRTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_lcr_text_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LCRTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_lcr_text_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LCRTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])


class TestCorpusNumericalPartSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['corpus_text_numerical_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_corpus_numerical_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', CRPNUM_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(
            response.get('json_content')['type_of_text_numerical_content'],
            self.raw_data['type_of_text_numerical_content'])

    def test_create_corpus_numerical_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['type_of_text_numerical_content'] = 'integer'
        response = api_call('POST', CRPNUM_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['type_of_text_numerical_content'] = {
            'en': 'integer'
        }

    def test_cannot_create_corpus_numerical_schema_anonymous(self):
        response = api_call('POST', CRPNUM_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_numerical_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', CRPNUM_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_numerical_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', CRPNUM_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_numerical_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', CRPNUM_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(
            response.get('json_content')['type_of_text_numerical_content'],
            self.raw_data['type_of_text_numerical_content'])

    def test_create_corpus_numerical_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', CRPNUM_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(
            response.get('json_content')['type_of_text_numerical_content'],
            self.raw_data['type_of_text_numerical_content'])


class TestCorpusImagePartSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['corpus_image_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_corpus_image_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', CRPIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_corpus_image_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'invdalid'
        response = api_call('POST', CRPIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/bilingual'

    def test_cannot_create_corpus_image_schema_anonymous(self):
        response = api_call('POST', CRPIMG_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_image_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', CRPIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_image_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', CRPIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_image_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', CRPIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_corpus_image_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', CRPIMG_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])


class TestCorpusVideoPartSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['corpus_video_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_corpus_video_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', CRPVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_corpus_video_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'invdalid'
        response = api_call('POST', CRPVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/bilingual'

    def test_cannot_create_corpus_video_schema_anonymous(self):
        response = api_call('POST', CRPVID_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_video_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', CRPVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_video_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', CRPVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_video_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', CRPVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_corpus_video_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', CRPVID_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])


class TestCorpusAudioPartSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['corpus_audio_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_corpus_audio_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', CRPAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_corpus_audio_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'invdalid'
        response = api_call('POST', CRPAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            'linguality_type'] = 'http://w3id.org/meta-share/meta-share/bilingual'

    def test_cannot_create_corpus_audio_schema_anonymous(self):
        response = api_call('POST', CRPAUDIO_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_audio_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', CRPAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)

    def test_create_corpus_audio_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', CRPAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_audio_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', CRPAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_corpus_audio_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', CRPAUDIO_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])


class TestCorpusTextPartSchemaEndpoint(EndpointTestCase):
    json_file = open('test_fixtures/dummy_data.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['text_corpus_part']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_corpus_text_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', CRPTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_corpus_text_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['multilinguality_type'] = 'not valid url'
        response = api_call('POST', CRPTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data[
            "linguality_type"] = 'http://w3id.org/meta-share/meta-share/bilingual'

    def test_cannot_create_corpus_text_schema_anonymous(self):
        response = api_call('POST', CRPTXT_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_text_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', CRPTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_text_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', CRPTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_corpus_text_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', CRPTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])

    def test_create_corpus_text_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', CRPTXT_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['linguality_type'],
                          self.raw_data['linguality_type'])


class TestLRSToolServiceSchemaEndpoint(EndpointTestCase):
    json_file = open('registry/tests/fixtures/tool.json', 'r')
    json_str = json_file.read()
    json_file.close()
    raw_data = json.loads(json_str)['described_entity']['lr_subclass']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_tool_service_schema_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', LRS_TOOL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['lr_type'],
                          self.raw_data['lr_type'])

    def test_create_tool_service_schema_will_fail_with_bad_input(self):
        user, token = self.admin, self.admin_token
        self.raw_data['function'] = 'function'
        response = api_call('POST', LRS_TOOL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['function'] = [
            "http://w3id.org/meta-share/omtd-share/NamedEntityRecognition",
            "http://w3id.org/meta-share/omtd-share/PosTagging"
        ]

    def test_cannot_create_tool_service_schema_anonymous(self):
        response = api_call('POST', LRS_TOOL_SCHEMA_ENDPOINT, self.client,
                            data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_tool_service_schema_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', LRS_TOOL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_tool_service_schema_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', LRS_TOOL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_create_tool_service_schema_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('POST', LRS_TOOL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['lr_type'],
                          self.raw_data['lr_type'])

    def test_create_tool_service_schema_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('POST', LRS_TOOL_SCHEMA_ENDPOINT, self.client,
                            auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(response.get('json_content')['lr_type'],
                          self.raw_data['lr_type'])


class TestSchemaTwoEndpoint(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_schema_2_organization_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}described_entity/organization/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_lr_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}described_entity/lr/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_project_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}described_entity/project/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_g_g_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}actor/generic_group/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_g_o_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}actor/generic_organization/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_g_p_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}actor/generic_person/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_grammar_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}ld_subclass/grammar/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_model_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}ld_subclass/model/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)


    def test_schema_2_corpus_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}lr_subclass/corpus/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_ld_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}lr_subclass/language_description/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_lcr_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}lr_subclass/lcr/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_schema_2_tool_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('OPTIONS', f'{SCHEMA_2_ENDPOINT}lr_subclass/tool_service/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
