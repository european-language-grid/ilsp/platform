import copy
import json
import logging

from django.core.exceptions import ValidationError
from test_utils import SerializerTestCase

from registry import serializers
from registry.serializers import GenericOrganizationSerializer
from registry.validators import (
    validate_language_code, validate_required_tag, validate_docker_image_reference,
    validate_unique_multiple_multilingual_field
)

LOGGER = logging.getLogger(__name__)


class TestValidateLanguageCodeHelperFunction(SerializerTestCase):

    @classmethod
    def setUpTestData(cls):
        cls.code_dict = {
            'en': 'english code',
            'fr': 'french code',
            'de': 'german code'
        }

    def test_validate_language_code_one_no_failure(self):
        self.assertTrue(validate_language_code(self.code_dict) is None)

    def test_validate_language_code_one_failure(self):
        fail_dict = copy.deepcopy(self.code_dict)
        fail_dict['fail code'] = 'fail'
        with self.assertRaises(ValidationError) as err:
            validate_language_code(fail_dict)
        self.assertEquals(
                str(["Given BCP47: 'fail code' is not valid language code (see also "
                 "https://tools.ietf.org/html/bcp47)"]), str(err.exception))

    def test_validate_language_code_multiple_failures(self):
        fail_dict = copy.deepcopy(self.code_dict)
        fail_dict['fail code'] = 'fail'
        fail_dict['second fail code'] = 'fail once more'
        with self.assertRaises(ValidationError) as err:
            validate_language_code(fail_dict)
        self.assertEquals(
                str(["Given BCP47: 'fail code', 'second fail code' are not valid language codes (see also "
                 "https://tools.ietf.org/html/bcp47)"]), str(err.exception))


class TestValidateRequiredTagHelperFunction(SerializerTestCase):

    @classmethod
    def setUpTestData(cls):
        cls.code_dict = {
            'fr': 'french code',
            'de': 'german code'
        }

    def test_validate_required_tag_no_failure(self):
        correct_dict = {
            'en': 'english code'
        }
        self.assertTrue(validate_required_tag(correct_dict) is None)

    def test_validate_required_tag_fails(self):
        with self.assertRaises(ValidationError) as err:
            validate_required_tag(self.code_dict)
        self.assertEquals(
                str(["Enter text in English. Must use 'en' language code."]), str(err.exception))


class TestValidateStringValidator(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = GenericOrganizationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.go_instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    def test_fails_if_value_of_multilingual_field_key_is_not_a_string(self):
        no_oi_data = copy.deepcopy(self.raw_data)
        no_oi_data['organization_name'] = {'en': ['Random', 'Random']}
        no_oi_data['organization_identifier'] = []
        no_oi_data['website'] = ['http://www.randomweb.com']
        serializer = GenericOrganizationSerializer(data=no_oi_data)
        if serializer.is_valid():
            go_instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_passes_if_value_of_multilingual_field_key_is_a_string(self):
        no_oi_data = copy.deepcopy(self.raw_data)
        no_oi_data['organization_name'] = {'en': 'Random'}
        no_oi_data['organization_identifier'] = []
        no_oi_data['website'] = ['http://www.randomweb.com']
        serializer = GenericOrganizationSerializer(data=no_oi_data)
        if serializer.is_valid():
            go_instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertTrue(serializer.is_valid())


class TestMaxLengthValidator(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = serializers.LanguageResourceSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_lr_serializer_fails_due_to_max_length_serializer_validator(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['resource_name'] = {
            'en': 'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE'
                  ' 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE'
                  ' 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE'
                  ' 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE'
                  ' 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE'
                  ' 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
                  'ANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named '
                  'Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE 9 English Named Entity RecognizerANNIE'
                  ' 9 English Named Entity RecognizerANNIE 9 English Named Entity Recognizer'
        }
        raw_data['resource_short_name'] = {'en': 'new rsn'}
        raw_data['lr_identifier'] = []
        serializer = serializers.LanguageResourceSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())


class TestValidateDockerImageReference(SerializerTestCase):

    def test_valid_docker_image_reference_with_host_name(self):
        value = 'docker.io/username/image:latest'
        self.assertTrue(validate_docker_image_reference(value))

    def test_valid_docker_image_reference_without_host_name(self):
        value = 'username/image:latest'
        self.assertTrue(validate_docker_image_reference(value))

    def test_invalid_docker_image_reference_url(self):
        value = 'https://hub.docker.com/repository/docker/username/image'
        with self.assertRaises(ValidationError) as err:
            validate_docker_image_reference(value)
        self.assertEquals(
                str([f'"{value}" does not comply with the expected input. '
                      'Follow the template: [host.name/]image/name:version']), str(err.exception))


class TestValidateUniqueRecommendedControlVocabulary(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = serializers.OrganizationSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.debug(cls.serializer.errors)

    def test_valid_unique_recommended_cv(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] =  [
            'My operation function',
            'http://w3id.org/meta-share/omtd-share/Alignment'
        ]
        raw_data['organization_short_name'] = [{'en': 'new rsn'}]
        raw_data['organization_identifier'] = []
        serializer = serializers.OrganizationSerializer(data=raw_data)
        serializer.is_valid()
        self.assertTrue(serializer.is_valid())

    def test_invalid_unique_recommended_cv_double_free(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = [
            'My operation function',
            'My operation function',
            'http://w3id.org/meta-share/omtd-share/Alignment'
        ]
        raw_data['organization_short_name'] = [{'en': 'new rsn'}]
        raw_data['organization_identifier'] = []
        serializer = serializers.OrganizationSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertTrue('lt_area' in serializer.errors)
        self.assertEqual(serializer.errors['lt_area'][0],
                         'Contains duplicate values.')


    def test_invalid_unique_recommended_cv_double_cv(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = [
            'My operation function',
            'http://w3id.org/meta-share/omtd-share/Alignment',
            'http://w3id.org/meta-share/omtd-share/Alignment'
        ]
        raw_data['organization_short_name'] = [{'en': 'new rsn'}]
        raw_data['organization_identifier'] = []
        serializer = serializers.OrganizationSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertTrue('lt_area' in serializer.errors)
        self.assertEqual(serializer.errors['lt_area'][0],
                         'Contains duplicate values.')

    def test_invalid_unique_recommended_cv_double_free_capital(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['lt_area'] = [
            'My operation function',
            '\tMY OPERATION function\t\n',
            'http://w3id.org/meta-share/omtd-share/Alignment'
        ]
        raw_data['organization_short_name'] = [{'en': 'new rsn'}]
        raw_data['organization_identifier'] = []
        serializer = serializers.OrganizationSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertTrue('lt_area' in serializer.errors)
        self.assertEqual(serializer.errors['lt_area'][0],
                         'Contains duplicate values.')


class TestValidateUniqueMultipleMultilingualField(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']

    def test_valid_multiple_multilingual_value(self):
        value = [
            {'en': 'academic', 'el': 'ακαδημαικός'},
            {'en': 'university'},
            {'en': 'machine translation'},
            {'en': 'free software'}
        ]
        self.assertTrue(validate_unique_multiple_multilingual_field(value))

    def test_invalid_value_not_list(self):
        value = {'en': 'academic', 'el': 'ακαδημαικός'}
        try:
            validate_unique_multiple_multilingual_field(value)
        except ValidationError as err:
            self.assertEquals(
                str([f"Expected value of type /list/, but received {value} which is of type {type(value)}."]),
                str(err))

    def test_invalid_contains_duplicate_same_value_en(self):
        value = [
            {'en': 'academic', 'el': 'ακαδημαικός'},
            {'en': 'academic', 'fr': 'académique'},
            {'en': 'university'},
            {'en': 'machine translation'},
            {'en': 'free software'}
        ]
        try:
            validate_unique_multiple_multilingual_field(value)
        except ValidationError as err:
            self.assertEquals(
                str([f"List contains duplicate values: "
                     f"{({'en': 'academic', 'el': 'ακαδημαικός'}, {'en': 'academic', 'fr': 'académique'})}"]),
                str(err))

    def test_invalid_organization_keywords_duplicate(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['keyword'] = [
            {'en': 'academic', 'el': 'ακαδημαικός'},
            {'en': 'academic', 'fr': 'académique'},
            {'en': 'university'},
            {'en': 'machine translation'},
            {'en': 'free software'}
        ]
        raw_data['organization_short_name'] = [{'en': 'new rsn'}]
        raw_data['organization_identifier'] = []
        serializer = serializers.OrganizationSerializer(data=raw_data)
        self.assertFalse(serializer.is_valid())
        self.assertTrue('keyword' in serializer.errors)
        self.assertEqual(serializer.errors['keyword'][0],
                         f"List contains duplicate values: "
                         f"{({'en': 'academic', 'el': 'ακαδημαικός'}, {'en': 'academic', 'fr': 'académique'})}")


