import logging

from django.core.exceptions import ObjectDoesNotExist

from accounts.models import ELGUser
from catalogue_backend.celery import app as celery_app
from dle.dle_report_index.documents import DLEReportDocument
from email_notifications.email_notification_utils import admin_merge_notification
from management.catalog_report_index.documents import CatalogReportDocument
from management.management_dashboard_index.documents import MyResourcesDocument, MyValidationsDocument
from registry import models
from registry.models import DescribedEntity
from registry.search_indexes.documents import MetadataDocument
from registry.serializer_tasks import GetRelatedRecords
from registry.utils.task_utils import update_affected_md_records

LOGGER = logging.getLogger(__name__)


@celery_app.task(name='admin_entity_merging')
def initiate_entity_merge(pk_pair_list, username):
    pairs = [(i['source'], i['target']) for i in pk_pair_list]
    entity_pair_mapping = {}
    for pair in pairs:
        try:
            entity_pair_mapping[DescribedEntity.objects.get(pk=pair[0]).entity_type].append(pair)
        except KeyError:
            entity_pair_mapping.update({DescribedEntity.objects.get(pk=pair[0]).entity_type: [pair]})
    return_list = []
    for _k, _v in entity_pair_mapping.items():
        merge_entities(_v)
        return_list.append('Entities merged.')

        # if _k == 'Organization':
        #     merge_organizations(_v)
        #     return_list.append('Organizations merged.')
        # if _k == 'Project':
        #     merge_projects(_v)
        #     return_list.append('Projects merged.')
        # if _k == 'LicenceTerms':
        #     merge_licences(_v)
        #     return_list.append('Licence Terms merged.')
        # if _k == 'LanguageResource':
        #     return_list.append('LR merging functionality not ready yet.')
        # if _k == 'Person':
        #     return_list.append('Person merging functionality not ready yet.')
        # if _k == 'Group':
        #     return_list.append('Group merging functionality not ready yet.')
        # if _k == 'Document':
        #     return_list.append('Document merging functionality not ready yet.')
        # if _k == 'Repository':
        #     return_list.append('Repository merging functionality not ready yet.')

    user = ELGUser.objects.get(username=username)
    admin_merge_notification(user=user)
    return return_list


def merge_entities(list_of_pairs):
    mdr_pks_to_update = set()
    # Replace source Organization with target one
    for source_pk, target_pk in list_of_pairs:
        try:
            entity_source = models.DescribedEntity.objects.get(pk=source_pk)
            entity_target = models.DescribedEntity.objects.get(pk=target_pk)
        except ObjectDoesNotExist:
            continue

        get_related_instances = GetRelatedRecords(entity_source)
        relations, potential_mdrs = get_related_instances.get_merging_fields()
        LOGGER.debug(f'{entity_target.entity_type} target {entity_target.pk} - {entity_target}')
        record_target = getattr(entity_target, 'metadata_record_of_described_entity', None)
        if record_target:
            LOGGER.debug(f'with md {record_target.pk}')
            mdr_pks_to_update.add(record_target.pk)

        # Add target Organization as affected metadata record
        LOGGER.debug(f'{entity_source.entity_type} source {entity_source.pk} - {entity_source}')
        record_target = getattr(entity_source, 'metadata_record_of_described_entity', None)
        if record_target:
            LOGGER.debug(f'with md {record_target.pk}')
            mdr_pks_to_update.add(record_target.pk)


        # Replace source with target Organization
        for rel_name, _rel in relations:
            try:
                relation = getattr(_rel, rel_name, None)
                if not relation:
                    continue
                if relation.exists():
                    relation.add(entity_target)
                    relation.remove(entity_source)
            except AttributeError:
                setattr(_rel, rel_name, entity_target)
                _rel.save()

        for md in potential_mdrs:
            mdr_pks_to_update.add(md.pk)

        delete_entity(entity_source)

    # Update affected metadata records
    update_affected_md_records(list(mdr_pks_to_update))


def delete_entity(entity):
    LOGGER.debug(f'Deleting source {entity.pk} - {entity}')
    record = getattr(entity.describedentity_ptr, 'metadata_record_of_described_entity', None)
    if record:
        if record.management_object.status == 'p':
            record.management_object.unpublish()
            MetadataDocument().update(record, action='delete', raise_on_error=False)
            CatalogReportDocument().catalogue_report_update(record.management_object, action='delete',
                                                            raise_on_error=False)
            DLEReportDocument().dle_report_update(record, action='delete', raise_on_error=False)
        record.management_object.mark_as_deleted(reason='Delete due to merging')
        MyResourcesDocument().update(record.management_object, action='delete', raise_on_error=False)
        MyValidationsDocument().update(record.management_object, action='delete', raise_on_error=False)
    else:
        entity.delete()


def merge_projects(list_of_pairs):
    mdr_pks_to_update = set()
    # Replace source Project with target one
    for source_pk, target_pk in list_of_pairs:
        try:
            project_source = models.Project.objects.get(pk=source_pk)
            project_target = models.Project.objects.get(pk=target_pk)
        except ObjectDoesNotExist:
            continue

        LOGGER.debug(f'Project target {project_target.pk} - {project_target}')
        record_target = getattr(project_target, 'metadata_record_of_described_entity', None)
        if record_target:
            LOGGER.debug(f'with md {record_target.pk}')
            mdr_pks_to_update.add(record_target.pk)

        # Add target project as affected metadata record
        LOGGER.debug(f'Project source {project_source.pk} - {project_source}')
        record_target = getattr(project_source, 'metadata_record_of_described_entity', None)
        if record_target:
            LOGGER.debug(f'with md {record_target.pk}')
            mdr_pks_to_update.add(record_target.pk)

        # Replace source with target Project
        # ActualUse.usage_project - m2m
        for _au in project_source.actual_uses_of_usage_project.all():
            _au.usage_project.add(target_pk)
            _au.usage_project.remove(source_pk)
            LOGGER.debug('ActualUse.usage_project {} - {}'.format(
                _au.pk, _au.usage_project.all()))
            try:
                possible_md = _au.language_resource_of_actual_use.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # LanguageResource.funding_project - m2m
        for _lr in project_source.language_resources_of_funding_project.all():
            _lr.funding_project.add(target_pk)
            _lr.funding_project.remove(source_pk)
            LOGGER.debug('LanguageResource.funding_project {} - {}'.format(
                _lr.pk, _lr.funding_project.all()))
            try:
                possible_md = _lr.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Project.replaces_project - m2m
        for _proj in project_source.projects_of_replaces_project.all():
            _proj.replaces_project.add(target_pk)
            _proj.replaces_project.remove(source_pk)
            LOGGER.debug('Project.replaces_project {} - {}'.format(
                _proj.pk, _proj.replaces_project.all()))
            try:
                possible_md = _proj.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass

        # Delete source Project
        delete_entity(project_source)

    # Update affected metadata records
    update_affected_md_records(list(mdr_pks_to_update))


def merge_organizations(list_of_pairs):
    mdr_pks_to_update = set()
    # Replace source Organization with target one
    for source_pk, target_pk in list_of_pairs:
        try:
            organization_source = models.Organization.objects.get(pk=source_pk)
            organization_target = models.Organization.objects.get(pk=target_pk)
        except ObjectDoesNotExist:
            continue

        LOGGER.debug(f'Organization target {organization_target.pk} - {organization_target}')
        record_target = getattr(organization_target, 'metadata_record_of_described_entity', None)
        if record_target:
            LOGGER.debug(f'with md {record_target.pk}')
            mdr_pks_to_update.add(record_target.pk)

        # Add target Organization as affected metadata record
        LOGGER.debug(f'Organization source {organization_source.pk} - {organization_source}')
        record_target = getattr(organization_source, 'metadata_record_of_described_entity', None)
        if record_target:
            LOGGER.debug(f'with md {record_target.pk}')
            mdr_pks_to_update.add(record_target.pk)

        # Replace source with target Organization
        # LanguageResource.contact - m2m
        for _lr in organization_source.language_resources_of_contact.all():
            _lr.contact.add(organization_target)
            _lr.contact.remove(organization_source)

            try:
                possible_md = _lr.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # LanguageResource.ipr_holder - m2m
        for _lr in organization_source.language_resources_of_ipr_holder.all():
            _lr.ipr_holder.add(organization_target)
            _lr.ipr_holder.remove(organization_source)

            try:
                possible_md = _lr.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # LanguageResource.recource_provider - m2m
        for _lr in organization_source.language_resources_of_resource_provider.all():
            _lr.resource_provider.add(organization_target)
            _lr.resource_provider.remove(organization_source)

            try:
                possible_md = _lr.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # LanguageResource.resource_creator - m2m
        for _lr in organization_source.language_resources_of_resource_creator.all():
            _lr.resource_creator.add(organization_target)
            _lr.resource_creator.remove(organization_source)

            try:
                possible_md = _lr.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Evaluation.evaluator - m2m
        for _eval in organization_source.evaluations_of_evaluator.all():
            _eval.evaluator.add(organization_target)
            _eval.evaluator.remove(organization_source)

            try:
                possible_md = _eval.tool_service_of_evaluation.toolservice.language_resource_of_lr_subclass.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Validation.validator - m2m
        for _val in organization_source.validations_of_validator.all():
            _val.validator.add(organization_target)
            _val.validator.remove(organization_source)

            try:
                possible_md = _val.language_resource_of_validation.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Attribution.attributed_agent
        for _attr in organization_source.attributions_of_attributed_agent.all():
            _attr.attributed_agent.add(organization_target)
            _attr.attributed_agent.remove(organization_source)

            try:
                lrs = _attr.language_resources_of_qualified_attribution.all()
                for _lr in lrs:
                    possible_md = _lr.metadata_record_of_described_entity
                    if possible_md:
                        mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Annotation.annotator - m2m
        for _anno in organization_source.annotations_of_annotator.all():
            _anno.annotator.add(organization_target)
            _anno.annotator.remove(organization_source)
            for _media_part in [_anno.corpus_text_part_of_annotation, _anno.corpus_text_numerical_part_of_annotation,
                                _anno.corpus_audio_part_of_annotation, _anno.corpus_video_part_of_annotation,
                                _anno.corpus_image_part_of_annotation]:
                try:
                    possible_md = _media_part.corpus_of_corpus_media_part.language_resource_of_lr_subclass.metadata_record_of_described_entity
                    if possible_md:
                        mdr_pks_to_update.add(possible_md.pk)
                except ObjectDoesNotExist:
                    pass
        # CorpusAudioPart.recorder - m2m
        for _cap in organization_source.corpus_audio_parts_of_recorder.all():
            _cap.recorder.add(organization_target)
            _cap.recorder.remove(organization_source)
            try:
                possible_md = _cap.corpus_of_corpus_media_part.language_resource_of_lr_subclass.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # CorpusVideoPart.recorder - m2m
        for _cvp in organization_source.corpus_video_parts_of_recorder.all():
            _cvp.recorder.add(organization_target)
            _cvp.recorder.remove(organization_source)
            try:
                possible_md = _cvp.corpus_of_corpus_media_part.language_resource_of_lr_subclass.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # CorpusTextNumericalPart.recorder
        for _ctnp in organization_source.corpus_text_numerical_parts_of_recorder.all():
            _ctnp.recorder.add(organization_target)
            _ctnp.recorder.remove(organization_source)
            try:
                possible_md = _ctnp.corpus_of_corpus_media_part.language_resource_of_lr_subclass.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # DatasetDistibution.distribution_rights_holder - m2m
        for _dd in organization_source.dataset_distributions_of_distribution_rights_holder.all():
            _dd.distribution_rights_holder.add(organization_target)
            _dd.distribution_rights_holder.remove(organization_source)
            for _sub in [_dd.corpus_of_dataset_distribution,
                         _dd.lexical_conceptual_resource_of_dataset_distribution,
                         _dd.language_description_of_dataset_distribution]:
                try:
                    possible_md = _sub.language_resource_of_lr_subclass.metadata_record_of_described_entity
                    if possible_md:
                        mdr_pks_to_update.add(possible_md.pk)
                except ObjectDoesNotExist:
                    pass
        # SoftwareDistribution.distributio_rights_holder - m2m
        for _sd in organization_source.software_distributions_of_distribution_rights_holder.all():
            _sd.distribution_rights_holder.add(organization_target)
            _sd.distribution_rights_holder.remove(organization_source)
            try:
                possible_md = _sd.tool_service_of_software_distribution.language_resource_of_lr_subclass.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Person.member_of_association - m2m
        for _per in organization_source.persons_of_member_of_association.all():
            _per.member_of_association.add(organization_target)
            _per.member_of_association.remove(organization_source)
            try:
                possible_md = _per.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Repository.repository_institution - m2o
        for _repo in organization_source.repositories_of_repository_institution.all():
            _repo.repository_institution.add(organization_target)
            _repo.repository_institution.remove(organization_source)
            try:
                possible_md = _repo.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Organization.is_division_of - m2m
        for _org in organization_source.organizations_of_is_division_of.all():
            # Ignore replace as is_division_of myself
            if _org.pk == organization_target:
                continue
            _org.is_division_of.add(organization_target)
            _org.is_division_of.remove(organization_source)
            try:
                possible_md = _org.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Organization.member_of_association
        for _org in organization_source.organizations_of_member_of_association.all():
            # Ignore replace as member_of_association myself
            if _org.pk == organization_target:
                continue
            _org.member_of_association.add(organization_target)
            _org.member_of_association.remove(organization_source)
            try:
                possible_md = _org.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Organization.replaces_organization - m2m
        for _org in organization_source.organizations_of_replaces_organization.all():
            # Ignore replace as replaces_organization myself
            if _org.pk == organization_target:
                continue
            _org.replaces_organization.add(organization_target)
            _org.replaces_organization.remove(organization_source)
            try:
                possible_md = _org.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Affiliation.affiliated_organization - m2o
        for _aff in organization_source.affiliations_of_affiliated_organization.all():
            _aff.affiliated_organization = organization_target
            _aff.save()
            try:
                possible_md = _aff.person_of_affiliation.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Project.coordinator - m2o
        for _proj in organization_source.projects_of_coordinator.all():
            _proj.coordinator = organization_target
            _proj.save()
            try:
                possible_md = _proj.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Project.participating_organization - m2m
        for _proj in organization_source.projects_of_participating_organization.all():
            _proj.participating_organization.add(organization_target)
            _proj.participating_organization.remove(organization_source)
            try:
                possible_md = _proj.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass
        # Project.funder - m2m
        for _proj in organization_source.projects_of_funder.all():
            _proj.funder.add(organization_target)
            _proj.funder.remove(organization_source)

            try:
                possible_md = _proj.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass

        # Delete source Project
        delete_entity(organization_source)

    # Update affected metadata records
    update_affected_md_records(list(mdr_pks_to_update))


def merge_licences(list_of_pairs):
    mdr_pks_to_update = set()
    # Replace source LicenceTerms with target one
    for source_pk, target_pk in list_of_pairs:
        try:
            licence_source = models.LicenceTerms.objects.get(pk=source_pk)
            licence_target = models.LicenceTerms.objects.get(pk=target_pk)
        except ObjectDoesNotExist:
            continue

        LOGGER.debug(f'LicenceTerms target {licence_target.pk} - {licence_target}')
        record_target = getattr(licence_target, 'metadata_record_of_described_entity', None)
        if record_target:
            LOGGER.debug(f'with md {record_target.pk}')
            mdr_pks_to_update.add(record_target.pk)

        # Add target LicenceTerms as affected metadata record
        LOGGER.debug(f'LicenceTerms source {licence_source.pk} - {licence_source}')
        record_target = getattr(licence_source, 'metadata_record_of_described_entity', None)
        if record_target:
            LOGGER.debug(f'with md {record_target.pk}')
            mdr_pks_to_update.add(record_target.pk)

        # Replace source with target LicenceTerms
        # DatasetDistribution.licence_terms - m2m
        for _dd in licence_source.dataset_distributions_of_licence_terms.all():
            _dd.licence_terms.add(target_pk)
            _dd.licence_terms.remove(source_pk)
            LOGGER.debug('DatasetDistribution.licence_terms {} - {}'.format(
                _dd.pk, _dd.licence_terms.all()))
            if _dd.corpus_of_dataset_distribution:
                try:
                    possible_md = _dd.corpus_of_dataset_distribution.language_resource_of_lr_subclass.metadata_record_of_described_entity
                    if possible_md:
                        mdr_pks_to_update.add(possible_md.pk)
                except ObjectDoesNotExist:
                    pass
            elif _dd.lexical_conceptual_resource_of_dataset_distribution:
                try:
                    possible_md = _dd.lexical_conceptual_resource_of_dataset_distribution.language_resource_of_lr_subclass.metadata_record_of_described_entity
                    if possible_md:
                        mdr_pks_to_update.add(possible_md.pk)
                except ObjectDoesNotExist:
                    pass
            elif _dd.language_description_of_dataset_distribution:
                try:
                    possible_md = _dd.language_description_of_dataset_distribution.language_resource_of_lr_subclass.metadata_record_of_described_entity
                    if possible_md:
                        mdr_pks_to_update.add(possible_md.pk)
                except ObjectDoesNotExist:
                    pass
        # SoftwareDistribution.licence_terms - m2m
        for _sd in licence_source.software_distributions_of_licence_terms.all():
            _sd.licence_terms.add(target_pk)
            _sd.licence_terms.remove(source_pk)
            LOGGER.debug('SoftwareDistribution.licence_terms {} - {}'.format(
                _sd.pk, _sd.licence_terms.all()))
            possible_md = None

            try:
                possible_md = _sd.tool_service_of_software_distribution.language_resource_of_lr_subclass.metadata_record_of_described_entity
                if possible_md:
                    mdr_pks_to_update.add(possible_md.pk)
            except ObjectDoesNotExist:
                pass

        # Delete source Project
        delete_entity(licence_source)

    # Update affected metadata records
    update_affected_md_records(list(mdr_pks_to_update))
