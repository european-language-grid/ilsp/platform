
from management.models import Manager
from registry.models import (
    MetadataRecordIdentifier, GenericMetadataRecord, AudioGenreIdentifier,
    Cost, DistributionTextFeature,
    DistributionAudioFeature, DistributionTextNumericalFeature,
    DistributionVideoFeature, DistributionImageFeature,
    DomainIdentifier, ImageGenreIdentifier, LanguageDescriptionSubclass,
    additionalInfo, SpeechGenreIdentifier,
    SubjectIdentifier, TextGenreIdentifier,
    TextTypeIdentifier, VideoGenreIdentifier, AccessRightsStatementIdentifier,
    AddressSet, RoleIdentifier, unspecifiedPart
)
from registry.serializers import (
    GenericPersonSerializer, GenericOrganizationSerializer, GenericGroupSerializer,
    GenericDocumentSerializer, GenericLicenceTermsSerializer, GenericProjectSerializer,
    GenericLanguageResourceSerializer, GenericRepositorySerializer
)

# Key: deleted model
# Value: {relation to be deleted, model of relation}
relation_mapping_to_model = {
    'MetadataRecord': {'metadata_record_identifier': MetadataRecordIdentifier,
                       'source_metadata_record': GenericMetadataRecord,
                       'management_object': Manager},
    'GenericMetadataRecord': {'metadata_record_identifier': MetadataRecordIdentifier},
    'DatasetDistribution': {'distribution_text_feature': DistributionTextFeature,
                            'distribution_video_feature': DistributionVideoFeature,
                            'distribution_text_numerical_feature': DistributionTextNumericalFeature,
                            'distribution_audio_feature': DistributionAudioFeature,
                            'distribution_image_feature': DistributionImageFeature,
                            'cost': Cost},
    'LanguageDescription': {'language_description_subclass': LanguageDescriptionSubclass,
                            'unspecified_part': unspecifiedPart},
    'LanguageResource': {'additional_info': additionalInfo},
    'Corpus': {'unspecified_part': unspecifiedPart},
    'LexicalConceptualResource': {'unspecified_part': unspecifiedPart},
    'Project': {'cost': Cost,
                'ec_max_contribution': Cost,
                'national_max_contribution': Cost},
    'Organization': {'head_office_address': AddressSet},
    'Division': {'head_office_address': AddressSet},
    'SoftwareDistribution': {'cost': Cost},
    'AudioGenre': {'audio_genre_identifier': AudioGenreIdentifier},
    'TextGenre': {'text_genre_identifier': TextGenreIdentifier},
    'ImageGenre': {'image_genre_identifier': ImageGenreIdentifier},
    'SpeechGenre': {'speech_genre_identifier': SpeechGenreIdentifier},
    'VideoGenre': {'video_genre_identifier': VideoGenreIdentifier},
    'Subject': {'subject_identifier': SubjectIdentifier},
    'TextType': {'text_type_identifier': TextTypeIdentifier},
    'Domain': {'domain_identifier': DomainIdentifier},
    'Role': {'role_identifier': RoleIdentifier},
    'AccessRightsStatement': {'access_rights_statement_identifier': AccessRightsStatementIdentifier},
}

# Key: deleted model
# Value: {relation to be deleted, related label to deleted model of relation to be deleted}
reverse_fk_relation_of_model = {
    'MetadataRecord': {'metadata_record_identifier': 'metadata_record_of_metadata_record_identifier',
                       'source_metadata_record': 'metadata_record_of_source_metadata_record',
                       'management_object': 'metadata_record_of_manager'},
    'GenericMetadataRecord': {'metadata_record_identifier': 'generic_metadata_record_of_metadata_record_identifier'},
    'DatasetDistribution': {'distribution_text_feature': 'dataset_distribution_of_distribution_text_feature',
                            'distribution_video_feature': 'dataset_distribution_of_distribution_video_feature',
                            'distribution_text_numerical_feature':
                                'dataset_distribution_of_distribution_text_numerical_feature',
                            'distribution_audio_feature': 'dataset_distribution_of_distribution_audio_feature',
                            'distribution_image_feature': 'dataset_distribution_of_distribution_image_feature',
                            'cost': 'dataset_distribution_of_cost'
                            },
    'LanguageDescription': {'language_description_subclass':
                                'language_description_of_language_description_subclass',
                            'unspecified_part': 'language_description_of_unspecified_part'},
    'Corpus': {'unspecified_part': 'corpus_of_unspecified_part'},
    'LexicalConceptualResource': {'unspecified_part': 'lexical_conceptual_resource_of_unspecified_part'},
    'LanguageResource': {'additional_info': 'language_resources_of_additional_info'},
    'Project': {'cost': 'project_of_cost',
                'ec_max_contribution': 'project_of_ec_max_contribution',
                'national_max_contribution': 'project_of_national_max_contribution'},
    'Organization': {'head_office_address': 'organization_of_head_office_address'},
    'Division': {'head_office_address': 'division_of_head_office_address'},
    'SoftwareDistribution': {'cost': 'software_distribution_of_cost'},
    'AudioGenre': {'audio_genre_identifier': 'audio_genre_of_audio_genre_identifier'},
    'TextGenre': {'text_genre_identifier': 'text_genre_of_text_genre_identifier'},
    'ImageGenre': {'image_genre_identifier': 'image_genre_of_image_genre_identifier'},
    'SpeechGenre': {'speech_genre_identifier': 'speech_genre_of_speech_genre_identifier'},
    'VideoGenre': {'video_genre_identifier': 'video_genre_of_video_genre_identifier'},
    'Subject': {'subject_identifier': 'subject_of_subject_identifier'},
    'TextType': {'text_type_identifier': 'text_type_of_text_type_identifier'},
    'Domain': {'domain_identifier': 'domain_of_domain_identifier'},
    'Role': {'role_identifier': 'role_of_role_identifier'},
    'AccessRightsStatement': {'access_rights_statement_identifier':
                                  'access_rights_statement_of_access_rights_statement_identifier'},
}

serializer_generic_entity_mapping = {
    'Person': GenericPersonSerializer,
    'Organization': GenericOrganizationSerializer,
    'Group': GenericGroupSerializer,
    'Document': GenericDocumentSerializer,
    'LicenceTerms': GenericLicenceTermsSerializer,
    'Project': GenericProjectSerializer,
    'LanguageResource': GenericLanguageResourceSerializer,
    'Repository': GenericRepositorySerializer
}