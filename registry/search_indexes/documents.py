import logging
from packaging import version as _ver

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import QuerySet, Q
from django_elasticsearch_dsl import Index, fields
from django_elasticsearch_dsl.documents import DocType
from elasticsearch_dsl import Object
from language_tags import tags
from rest_framework.reverse import reverse

from management.models import PUBLISHED
from registry.choices import (
    LT_CLASS_RECOMMENDED_CHOICES, LINGUALITY_TYPE_CHOICES,
    MULTILINGUALITY_TYPE_CHOICES, MEDIA_TYPE_CHOICES, MODEL_TYPE_RECOMMENDED_CHOICES,
    MODEL_FUNCTION_RECOMMENDED_CHOICES, LD_SUBCLASS_CHOICES
)
from registry.models import MetadataRecord
from registry.utils.checks import is_tool_service
from registry.utils.retrieval_utils import retrieve_default_lang
from registry.utils.string_utils import sanitize_field
from utils.glottolog_service import requests
from utils.index_utils import (
    lowercase_normalizer, synonym_analyzer, map_resource_type, map_ld_subclass,
    map_condition_of_use, map_access_rights_statement, EU_LANGUAGES
)
from utils.ms_ontology_service.requests import get_function_categories

LOGGER = logging.getLogger(__name__)

# Name of the Elasticsearch index
metadata_record = Index('metadata_records')
# See Elasticsearch Indices API reference for available settings
metadata_record.settings(
    number_of_shards=1,
    number_of_replicas=0
)


# metadata_record.analyzer(synonym_analyzer)


@metadata_record.doc_type
class MetadataDocument(DocType):
    class Django:
        model = MetadataRecord  # The model associated with this Document

    id = fields.IntegerField(attr='id')

    resource_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        analyzer=synonym_analyzer
    )

    resource_short_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        multi=True,
        analyzer=synonym_analyzer
    )

    description = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        analyzer=synonym_analyzer
    )

    resource_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    resource_type_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    version_order = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    is_active_version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    other_versions_count = fields.IntegerField()

    harvested_versions_exceeding_limit = fields.BooleanField()

    entity_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    entity_type_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    keywords = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    detail = fields.TextField()

    licences = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    licence_short_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    licence_short_name_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    condition_of_use = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    access_rights = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    media_type = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    languages = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    linguality_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    multilinguality_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    languages_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    languages_eu = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    languages_rest = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    language_tag = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    language_id = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    language_variety = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        multi=True
    )

    country_of_registration = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    country_of_registration_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    language_dependent = fields.BooleanField()

    functions = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    pair_function_function_category = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    function_categories = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    functions_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    intended_applications = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    pair_intended_application_with_category = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    intended_application_categories = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    model_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    model_function = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    intended_applications_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    source = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    is_division_of = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    organization_order = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    elg_integrated_services_and_data = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    views = fields.IntegerField()
    downloads = fields.IntegerField()
    has_data = fields.BooleanField()
    service_execution_count = fields.IntegerField()

    under_construction = fields.BooleanField()
    featured = fields.BooleanField()
    for_information_only = fields.BooleanField()

    creation_date = fields.DateField()
    last_date_updated = fields.DateField()
    elg_compatible_service = fields.BooleanField()
    elg_compatible_data_resource = fields.BooleanField()
    duplicate = fields.ObjectField()
    potential_duplicate = fields.ObjectField()
    proxied = fields.BooleanField()
    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        """
        Return the queryset that should be indexed by this doc type.
        """
        return MetadataRecord.objects.filter(
            (Q(described_entity__entity_type='Project')
             | (Q(described_entity__entity_type='Organization')
                & ~Q(described_entity__actor__organization__organization_legal_status='http://w3id.org/meta-share/meta-share/fundingAuthority'))
             | (Q(described_entity__entity_type='LanguageResource')
                & (Q(management_object__is_latest_version=True) | Q(management_object__under_construction=True)))) &
            Q(management_object__status=PUBLISHED) &
            Q(management_object__deleted=False) &
            Q(management_object__hide_record_as_duplicate=False)
        )

    def catalogue_update(self, thing, refresh=None, action="index", raise_on_error=False, **kwargs):
        """
        Update each document in ES for a model, iterable of models or queryset
        """
        if action == "index":
            if isinstance(thing, MetadataRecord):
                thing = self.get_queryset().filter(pk=thing.pk)
            elif isinstance(thing, QuerySet):
                thing = self.get_queryset().intersection(thing)
            elif isinstance(thing, list):
                thing = self.get_queryset().filter(pk__in=[instance.pk for instance in thing])
        try:
            if raise_on_error:
                result = super().update(thing, refresh, action=action, raise_on_error=raise_on_error, **kwargs)
            else:
                result = super().update(thing, refresh, action, **kwargs)
        except Exception:
            LOGGER.exception('Error in elastic indexer update')
        else:
            return result

    # PREPARE FIELDS
    def prepare_resource_name(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return retrieve_default_lang(instance.described_entity.resource_name)
        elif instance.described_entity.entity_type == 'Project':
            return retrieve_default_lang(instance.described_entity.project_name)
        elif instance.described_entity.entity_type == 'Organization':
            return retrieve_default_lang(instance.described_entity.organization_name)

    def prepare_resource_short_name(self, instance):
        return_list = []
        if instance.described_entity.entity_type == 'LanguageResource' and \
                instance.described_entity.resource_short_name:
            return_list.append(retrieve_default_lang(instance.described_entity.resource_short_name))
        elif instance.described_entity.entity_type == 'Project' and \
                instance.described_entity.project_short_name:
            return_list.append(retrieve_default_lang(instance.described_entity.project_short_name))
        elif instance.described_entity.entity_type == 'Organization' and \
                instance.described_entity.organization_short_name:
            for name in instance.described_entity.organization_short_name:
                return_list.append(retrieve_default_lang(name))
        return return_list

    def prepare_description(self, instance):
        try:
            if instance.described_entity.entity_type == 'LanguageResource':
                return sanitize_field(retrieve_default_lang(instance.described_entity.description), handle_nbsp=True)
            elif instance.described_entity.entity_type == 'Project':
                return sanitize_field(retrieve_default_lang(instance.described_entity.project_summary),
                                      handle_nbsp=True)
            elif instance.described_entity.entity_type == 'Organization':
                return sanitize_field(retrieve_default_lang(instance.described_entity.organization_bio),
                                      handle_nbsp=True)
        except TypeError:
            return ''

    def prepare_resource_type(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(instance.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(
                    LD_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type

    def prepare_resource_type_facet(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(instance.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(
                    LD_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type

    def prepare_version(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return map_resource_type(instance.described_entity.version)

    def prepare_version_order(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            version_order = map_resource_type(instance.described_entity.version)
            if version_order == 'unspecified':
                version_order = ''
            return version_order

    def prepare_is_active_version(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return instance.management_object.is_active_version

    def prepare_other_versions_count(self, instance):
        """
        Count of other versions that are non-deleted, published,
        plus itself
        """
        count = instance.management_object.versioned_record_managers.filter(
            status=PUBLISHED,
            deleted=False).count()
        if count > 0:
            return count

    def prepare_harvested_versions_exceeding_limit(self, instance):
        """
        return versions exceeding limit for harvested records
        """
        if getattr(instance, 'source_of_metadata_record', None):
            return instance.management_object.versions_exceeding_limit
        else:
            return None

    def prepare_entity_type(self, instance):
        return instance.described_entity.entity_type

    def prepare_entity_type_facet(self, instance):
        return instance.described_entity.entity_type

    def prepare_keywords(self, instance):
        try:
            result = list()
            if instance.described_entity.keyword is None:
                return []
            for k_dict in instance.described_entity.keyword:
                result.extend(k_dict.values())
            return result
        except AttributeError:
            return []

    def prepare_detail(self, instance):
        return f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.id})}"

    def prepare_licences(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource' \
                and not instance.management_object.under_construction:
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    for distribution in instance.described_entity.lr_subclass.dataset_distribution.all():
                        for licence in distribution.licence_terms.all():
                            result.append(
                                retrieve_default_lang(licence.licence_terms_name))
                elif resource_type == 'ToolService':
                    for distribution in instance.described_entity.lr_subclass.software_distribution.all():
                        for licence in distribution.licence_terms.all():
                            result.append(
                                retrieve_default_lang(licence.licence_terms_name))
            except IndexError:
                return result
        return list(set(result))

    def prepare_licence_short_name(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource' \
                and not instance.management_object.under_construction:
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    for distribution in instance.described_entity.lr_subclass.dataset_distribution.all():
                        for licence in distribution.licence_terms.all():
                            if licence.licence_terms_short_name:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_short_name))
                            else:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_name))
                elif resource_type == 'ToolService':
                    for distribution in instance.described_entity.lr_subclass.software_distribution.all():
                        for licence in distribution.licence_terms.all():
                            if licence.licence_terms_short_name:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_short_name))
                            else:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_name))
            except IndexError:
                return result
        return list(set(result))

    def prepare_licence_short_name_facet(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource' \
                and not instance.management_object.under_construction:
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    for distribution in instance.described_entity.lr_subclass.dataset_distribution.all():
                        for licence in distribution.licence_terms.all():
                            if licence.licence_terms_short_name:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_short_name))
                            else:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_name))
                elif resource_type == 'ToolService':
                    for distribution in instance.described_entity.lr_subclass.software_distribution.all():
                        for licence in distribution.licence_terms.all():
                            if licence.licence_terms_short_name:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_short_name))
                            else:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_name))
            except IndexError:
                return result
        return list(set(result))

    def prepare_condition_of_use(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                # Get distributions
                distributions = None
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    distributions = instance.described_entity.lr_subclass.dataset_distribution.all()
                elif resource_type == 'ToolService':
                    distributions = instance.described_entity.lr_subclass.software_distribution.all()
                for distribution in distributions:
                    # Map condition_of_use
                    for licence in distribution.licence_terms.all():
                        for c_o_u in licence.condition_of_use:
                            condition = map_condition_of_use(c_o_u)
                            if condition:
                                result.append(condition)
                    # Map access_rights_statements
                    for access_right in distribution.access_rights.all():
                        conditions = map_access_rights_statement(retrieve_default_lang(access_right.category_label))
                        if conditions:
                            for condition in conditions:
                                result.append(condition)

            except IndexError:
                return result
        return list(set(result))

    def prepare_access_rights(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    for distribution in instance.described_entity.lr_subclass.dataset_distribution.all():
                        for a_r in distribution.access_rights.all():
                            result.append(
                                retrieve_default_lang(a_r.category_label))
                elif resource_type == 'ToolService':
                    for distribution in instance.described_entity.lr_subclass.software_distribution.all():
                        for a_r in distribution.access_rights.all():
                            result.append(
                                retrieve_default_lang(a_r.category_label))
            except IndexError:
                return result
        return list(set(result))

    def prepare_media_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result.append('unspecified')
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'media_type', None):
                            continue
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type))
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result.append('unspecified')
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if not getattr(media_part, 'media_type', None):
                            continue
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type))
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result.append('unspecified')
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                result.append('unspecified')
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            if not getattr(media_part, 'media_type', None):
                                continue
                            result.append(MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type))
            except IndexError:
                return result
        return list(set(result))

    def _get_language_name(self, lang):
        if lang.language_id == 'mis':
            language_name = requests.get_name(lang.glottolog_code)
        else:
            language_name = tags.description(lang.language_id)[0]
        return language_name

    def _check_language_list(self, part, language_list=None, not_in_language_list=False):
        result = list()
        for lang in part.language.all():
            lang_name = self._get_language_name(lang)
            if language_list is None:
                result.extend([lang_name])
            else:
                if not_in_language_list:
                    if lang_name not in language_list:
                        result.extend([lang_name])
                else:
                    if lang_name in language_list:
                        result.extend([lang_name])
        return result

    def _from_language_list(self, instance, language_list=None, not_in_language_list=False):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result_lang = self._check_language_list(
                                instance.described_entity.lr_subclass.unspecified_part, language_list,
                                not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        result_lang = self._check_language_list(media_part, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result_lang = self._check_language_list(
                                instance.described_entity.lr_subclass.unspecified_part, language_list,
                                not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        result_lang = self._check_language_list(media_part, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result_lang = self._check_language_list(
                                instance.described_entity.lr_subclass.unspecified_part, language_list,
                                not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                result_lang = self._check_language_list(
                                    instance.described_entity.lr_subclass.unspecified_part, language_list,
                                    not_in_language_list)
                                if result_lang:
                                    result.extend(result_lang)
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            result_lang = self._check_language_list(media_part, language_list, not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        result_lang = self._check_language_list(input_resource, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        result_lang = self._check_language_list(output_resource, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
            except IndexError:
                return result
        return list(set(result))

    def prepare_languages(self, instance):
        # handle different cases, based on resource_type
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance)
        else:
            return []

    def prepare_languages_facet(self, instance):
        # handle different cases, based on resource_type
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance)
        else:
            return []

    def prepare_languages_eu(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance, EU_LANGUAGES)
        else:
            return []

    def prepare_languages_rest(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance, EU_LANGUAGES, True)
        else:
            return []

    def prepare_language_id(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_id)
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_id)
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_id)
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                for language in unspecified_part.language.all():
                                    result.append(language.language_id)
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            for language in media_part.language.all():
                                result.append(language.language_id)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        for language in input_resource.language.all():
                            result.append(language.language_id)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        for language in output_resource.language.all():
                            result.append(language.language_id)
            except IndexError:
                return result
        return list(set(result))

    def prepare_language_tag(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_tag)
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        for language in media_part.language.all():
                            result.append(language.language_tag)
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_tag)
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        for language in media_part.language.all():
                            result.append(language.language_tag)
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_tag)
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                for language in unspecified_part.language.all():
                                    result.append(language.language_tag)
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            for language in media_part.language.all():
                                result.append(language.language_tag)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        for language in input_resource.language.all():
                            result.append(language.language_tag)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        for language in output_resource.language.all():
                            result.append(language.language_tag)
            except IndexError:
                return result
        return list(set(result))

    def prepare_language_variety(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                if getattr(language, 'language_variety_name', None):
                                    result.append(language.language_variety_name['en'])
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        for language in media_part.language.all():
                            if getattr(language, 'language_variety_name', None):
                                result.append(language.language_variety_name['en'])
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                if getattr(language, 'language_variety_name', None):
                                    result.append(language.language_variety_name['en'])
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        for language in media_part.language.all():
                            if getattr(language, 'language_variety_name', None):
                                result.append(language.language_variety_name['en'])
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                if getattr(language, 'language_variety_name', None):
                                    result.append(language.language_variety_name['en'])
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                for language in unspecified_part.language.all():
                                    if getattr(language, 'language_variety_name', None):
                                        result.append(language.language_variety_name['en'])
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            for language in media_part.language.all():
                                if getattr(language, 'language_variety_name', None):
                                    result.append(language.language_variety_name['en'])
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        for language in input_resource.language.all():
                            if getattr(language, 'language_variety_name', None):
                                result.append(language.language_variety_name['en'])
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        for language in output_resource.language.all():
                            if getattr(language, 'language_variety_name', None):
                                result.append(language.language_variety_name['en'])
            except IndexError:
                return result
        return list(set(result))

    def prepare_linguality_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part and getattr(unspecified_part, 'linguality_type', None):
                            result.append(LINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.linguality_type))
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part and getattr(unspecified_part, 'linguality_type', None):
                            result.append(LINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.linguality_type))
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part and getattr(unspecified_part, 'linguality_type', None):
                            result.append(LINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.linguality_type))
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part and getattr(unspecified_part, 'linguality_type', None):
                                result.append(LINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.linguality_type))
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            if not getattr(media_part, 'linguality_type', None):
                                continue
                            result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
            except IndexError:
                return result
        return list(set(result))

    def prepare_multilinguality_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part and getattr(unspecified_part, 'multilinguality_type', None):
                            result.append(
                                MULTILINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.multilinguality_type))
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'multilinguality_type', None):
                            continue
                        result.append(MULTILINGUALITY_TYPE_CHOICES.__getitem__(media_part.multilinguality_type))
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part and getattr(unspecified_part, 'multilinguality_type', None):
                            result.append(
                                MULTILINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.multilinguality_type))
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if not getattr(media_part, 'multilinguality_type', None):
                            continue
                        result.append(MULTILINGUALITY_TYPE_CHOICES.__getitem__(media_part.multilinguality_type))
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part and getattr(unspecified_part, 'multilinguality_type', None):
                            result.append(
                                MULTILINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.multilinguality_type))
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part and getattr(unspecified_part, 'multilinguality_type', None):
                                result.append(
                                    MULTILINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.multilinguality_type))
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            if not getattr(media_part, 'multilinguality_type', None):
                                continue
                            result.append(MULTILINGUALITY_TYPE_CHOICES.__getitem__(media_part.multilinguality_type))
            except IndexError:
                return result
        return list(set(result))

    def prepare_country_of_registration(self, instance):
        """
        For Organizations index the specified country (if exists)
        """
        result = list()
        if instance.described_entity.entity_type == 'Organization':
            # Get the primary AddressSet if exists (head_office_address)
            office_address = None
            if instance.described_entity.head_office_address:
                office_address = instance.described_entity.head_office_address
            # Now try to get the country from the AddressSet. If 'office_address' is None, return 'result' which is
            # an empty list
            if office_address:
                result_country = office_address.country
                result.append(tags.region(result_country).description[0])
                return result
        return result

    def prepare_country_of_registration_facet(self, instance):
        """
        For Organizations index the specified country (if exists)
        """
        result = list()
        if instance.described_entity.entity_type == 'Organization':
            # Get the primary AddressSet if exists (head_office_address)
            office_address = None
            if instance.described_entity.head_office_address:
                office_address = instance.described_entity.head_office_address
            # Now try to get the country from the AddressSet. If 'office_address' is None, return 'result' which is
            # an empty list
            if office_address:
                result_country = office_address.country
                result.append(tags.region(result_country).description[0])
                return result
        return result

    def prepare_functions(self, instance):
        if is_tool_service(instance):
            function = []
            for f in instance.described_entity.lr_subclass.function:
                try:
                    function.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(f))
                except KeyError:
                    function.append(f)
            return function
        else:
            return []

    def prepare_function_categories(self, instance):
        categories = list()
        if is_tool_service(instance):
            for f in instance.described_entity.lr_subclass.function:
                try:
                    categories.extend(get_function_categories(f.split('/')[-1]))
                except KeyError:
                    categories.extend(get_function_categories(f))
        elif instance.described_entity.entity_type == 'LanguageResource':
            function = []
            if not getattr(instance.described_entity.lr_subclass, 'ld_subclass',
                           None) == 'http://w3id.org/meta-share/meta-share/model':
                return []
            for func in instance.described_entity.lr_subclass.language_description_subclass.model_function:
                try:
                    categories.extend(get_function_categories(func.split('/')[-1]))
                except KeyError:
                    categories.extend(get_function_categories(func))
            return function
        return list(set(categories))

    def prepare_pair_function_function_category(self, instance):
        categories = list()
        if is_tool_service(instance):
            for _f in instance.described_entity.lr_subclass.function:
                try:
                    f = LT_CLASS_RECOMMENDED_CHOICES.__getitem__(_f)
                except KeyError:
                    f = _f
                cat_pair = f'{f} + '
                try:
                    cat_pair += '//'.join(get_function_categories(_f.split('/')[-1]))
                except KeyError:
                    cat_pair += '//'.join(get_function_categories(_f))
                categories.append(cat_pair)
        elif instance.described_entity.entity_type == 'LanguageResource':
            function = []
            if not getattr(instance.described_entity.lr_subclass, 'ld_subclass',
                           None) == 'http://w3id.org/meta-share/meta-share/model':
                return []
            for func in instance.described_entity.lr_subclass.language_description_subclass.model_function:
                try:
                    f = LT_CLASS_RECOMMENDED_CHOICES.__getitem__(func)
                except KeyError:
                    f = func
                cat_pair = f'{func} + '
                try:
                    cat_pair += '//'.join(get_function_categories(func.split('/')[-1]))
                except KeyError:
                    cat_pair += '//'.join(get_function_categories(func))
                categories.append(cat_pair)
        return list(set(categories))

    def prepare_functions_facet(self, instance):
        if is_tool_service(instance):
            function = []
            for f in instance.described_entity.lr_subclass.function:
                try:
                    function.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(f))
                except KeyError:
                    function.append(f)
            return function
        elif instance.described_entity.entity_type == 'LanguageResource':
            function = []
            if not getattr(instance.described_entity.lr_subclass, 'ld_subclass',
                           None) == 'http://w3id.org/meta-share/meta-share/model':
                return []
            for func in instance.described_entity.lr_subclass.language_description_subclass.model_function:
                try:
                    function.append(MODEL_FUNCTION_RECOMMENDED_CHOICES.__getitem__(func))
                except KeyError:
                    function.append(func)
            return function
        else:
            return []

    def prepare_language_dependent(self, instance):
        if is_tool_service(instance):
            return instance.described_entity.lr_subclass.language_dependent
        else:
            return None

    def prepare_intended_applications(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            intended_application = []
            if not instance.described_entity.intended_application:
                return []
            for application in instance.described_entity.intended_application:
                try:
                    intended_application.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(application))
                except KeyError:
                    intended_application.append(application)
            return intended_application
        else:
            return []

    def prepare_intended_application_categories(self, instance):
        categories = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            if not instance.described_entity.intended_application:
                return []
            for application in instance.described_entity.intended_application:
                try:
                    categories.extend(get_function_categories(application.split('/')[-1]))
                except KeyError:
                    categories.extend(get_function_categories(application))
        return list(set(categories))

    def prepare_pair_intended_application_with_category(self, instance):
        categories = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            if not instance.described_entity.intended_application:
                return []
            for application in instance.described_entity.intended_application:
                try:
                    f = LT_CLASS_RECOMMENDED_CHOICES.__getitem__(application)
                except KeyError:
                    f = application
                cat_pair = f'{f} + '
                try:
                    cat_pair += '//'.join(get_function_categories(application.split('/')[-1]))
                except KeyError:
                    cat_pair += '//'.join(get_function_categories(application))
                categories.append(cat_pair)
        return list(set(categories))

    def prepare_model_type(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            model_type = []
            if not getattr(instance.described_entity.lr_subclass, 'ld_subclass',
                           None) == 'http://w3id.org/meta-share/meta-share/model':
                return []
            if instance.described_entity.lr_subclass.language_description_subclass.model_type:
                for _type in instance.described_entity.lr_subclass.language_description_subclass.model_type:
                    try:
                        model_type.append(MODEL_TYPE_RECOMMENDED_CHOICES.__getitem__(_type))
                    except KeyError:
                        model_type.append(_type)
                return model_type
            else:
                return []
        else:
            return []

    def prepare_model_function(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            model_function = []
            if not getattr(instance.described_entity.lr_subclass, 'ld_subclass',
                           None) == 'http://w3id.org/meta-share/meta-share/model':
                return []
            for func in instance.described_entity.lr_subclass.language_description_subclass.model_function:
                try:
                    model_function.append(MODEL_FUNCTION_RECOMMENDED_CHOICES.__getitem__(func))
                except KeyError:
                    model_function.append(func)
            return model_function
        else:
            return []

    def prepare_intended_applications_facet(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            intended_application = []
            if not instance.described_entity.intended_application:
                return []
            for application in instance.described_entity.intended_application:
                try:
                    intended_application.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(application))
                except KeyError:
                    intended_application.append(application)
            return intended_application
        else:
            return []

    def prepare_views(self, instance):
        views = 0
        try:
            views = instance.stats.all_versions_views
        except ObjectDoesNotExist:
            views = 0
        return views

    def prepare_downloads(self, instance):
        downloads = 0
        try:
            downloads = instance.stats.all_versions_downloads
        except ObjectDoesNotExist:
            downloads = 0
        if downloads:
            return downloads

    def prepare_service_execution_count(self, instance):
        from analytics.models import ServiceStats
        """
        Given a metadata record x, if x is an LT Service, return times this service has been called, else 0
        :param instance: metadata record instance
        :return: times this service has been called, else 0
        """
        if instance.management_object.functional_service:
            lt_service = instance.lt_service
            all_version_services_qr = lt_service.all_version_services.all()
            all_version_services = [ver for ver in all_version_services_qr]
            all_version_services.append(lt_service)
            service_execution_count = 0
            for service in all_version_services:
                try:
                    service_execution_count += ServiceStats.aggregate(service)
                except ObjectDoesNotExist:
                    service_execution_count += 0
            return service_execution_count

    def prepare_has_data(self, instance):
        try:
            return instance.management_object.has_content_file
        except ObjectDoesNotExist:
            return None

    def prepare_under_construction(self, instance):
        return instance.management_object.under_construction

    def prepare_featured(self, instance):
        return instance.management_object.featured

    def prepare_for_information_only(self, instance):
        return instance.management_object.for_information_only

    def prepare_creation_date(self, instance):
        return instance.metadata_creation_date

    def prepare_last_date_updated(self, instance):
        return instance.metadata_last_date_updated

    def prepare_elg_compatible_service(self, instance):
        if is_tool_service(instance):
            return instance.management_object.functional_service
        return None

    def prepare_elg_compatible_data_resource(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource' and \
                instance.described_entity.lr_subclass in ['LexicalConceptualResource', 'Corpus']:
            return instance.management_object.service_compliant_dataset
        elif instance.described_entity.entity_type == 'LanguageResource' and \
                instance.described_entity.lr_subclass == 'LanguageDescription':
            if instance.described_entity.lr_subclass.language_description_subclass == 'Model':
                return instance.management_object.elg_compatible_model
            elif instance.described_entity.lr_subclass.language_description_subclass == 'Grammar':
                return instance.management_object.service_compliant_dataset

    def prepare_elg_integrated_services_and_data(self, instance):
        if is_tool_service(instance):
            if instance.management_object.functional_service:
                return 'ELG compatible services'
        if instance.described_entity.entity_type == 'LanguageResource':
            if instance.management_object.has_content_file:
                return 'ELG hosted data'
        # TODO add when needed
        # if instance.described_entity.entity_type == 'LanguageResource' \
        #         and instance.described_entity.lr_subclass in ['LanguageDescription', 'Corpus', 'LexicalConceptualResource'] \
        #         and (instance.management_object.elg_compatible_model
        #              or instance.management_object.service_compliant_dataset):
        #     return 'ELG compatible data resource'
        return None

    def prepare_duplicate(self, instance):
        initial_list = []

        if instance.described_entity.entity_type == 'LanguageResource':
            for e_m in instance.described_entity.is_exact_match_with.all():
                try:
                    duplicate_record = MetadataRecord.objects.get(described_entity__pk=e_m.pk)
                except ObjectDoesNotExist:
                    continue
                source_repo = getattr(duplicate_record, 'source_of_metadata_record', None)
                if not source_repo:
                    source_repo_name = 'ELG / ELE'
                else:
                    source_repo_name = source_repo.repository_name['en']
                initial_list.append({'source': source_repo_name,
                                    'resource_name': duplicate_record.described_entity.resource_name['en'],
                                    'version': duplicate_record.described_entity.version,
                                    'url': duplicate_record.get_display_url()})

        return_list = []
        source_set = set([i['source'] for i in initial_list])
        for _s in source_set:
            records_of_source = [i for i in initial_list if i['source'] == _s]
            r_n_set = set([i['resource_name'] for i in records_of_source])
            for r_n in r_n_set:
                return_list.append(sorted([i for i in records_of_source if i['resource_name'] == r_n],
                                          key=lambda x: _ver.parse(x['version']),
                                          reverse=True)[0])

        return return_list

    def prepare_potential_duplicate(self, instance):
        initial_list = []
        if instance.described_entity.entity_type == 'LanguageResource':
            for e_m in instance.described_entity.is_potential_duplicate_with.all():
                try:
                    duplicate_record = MetadataRecord.objects.get(described_entity__pk=e_m.pk)
                except ObjectDoesNotExist:
                    continue
                source_repo = getattr(duplicate_record, 'source_of_metadata_record', None)
                if not source_repo:
                    source_repo_name = 'ELG / ELE'
                else:
                    source_repo_name = source_repo.repository_name['en']
                initial_list.append({'source': source_repo_name,
                                     'resource_name': duplicate_record.described_entity.resource_name['en'],
                                     'version': duplicate_record.described_entity.version,
                                     'url': duplicate_record.get_display_url()})

        return_list = []
        source_set = set([i['source'] for i in initial_list])
        for _s in source_set:
            records_of_source = [i for i in initial_list if i['source'] == _s]
            r_n_set = set([i['resource_name'] for i in records_of_source])
            for r_n in r_n_set:
                return_list.append(sorted([i for i in records_of_source if i['resource_name'] == r_n],
                                          key=lambda x: _ver.parse(x['version']),
                                          reverse=True)[0])
        return return_list

    def prepare_proxied(self, instance):
        if is_tool_service(instance) \
                and instance.management_object.functional_service:
            return instance.management_object.proxied
        return None

    def prepare_source(self, instance):
        source_repo = getattr(instance, 'source_of_metadata_record', None)
        if source_repo:
            return source_repo.repository_name['en']
        else:
            return 'ELG / ELE'

    def prepare_is_division_of(self, instance):
        """
        For Organizations index the is_division_of.organization_name
        """
        result = []
        if instance.described_entity.entity_type == 'Organization':
            if instance.described_entity.is_division_of:
                for division in instance.described_entity.is_division_of.all():
                    result.append(retrieve_default_lang(division.organization_name))
        return result

    def prepare_organization_order(self, instance):
        """
        For ordering of organziations and divsions of organizations together
        """
        result = None
        if instance.described_entity.entity_type == 'Organization':
            result = ''
            name = retrieve_default_lang(instance.described_entity.organization_name)
            # find the byte values of the lower case characters of organization name and add them as a string
            for char in name.lower():
                result += f'{ord(char)}'
            if instance.described_entity.is_division_of.all():
                parent_result = ''
                parent_list = []
                for parent in instance.described_entity.is_division_of.all():
                    parent_list.append(retrieve_default_lang(parent.organization_name))
                # if the organization is a division of a parent organization, add the organization name to the
                # byte value of the lower case parent name
                for parent_name in sorted(parent_list):
                    for char in parent_name.lower():
                        parent_result += f'{ord(char)}'
                result = parent_result + name
            # The lowercase a, b, c characters have the byte values 97, 98, 99 respectively, so a 0 is added as the
            # first character of the string for them to be ordered first. The rest of the special characters whose
            # byte values start with 9, also get a 0 as the first character of the string to be ordered according to
            # special characters with byte values below 65
            if result[0] == '9':
                result = '0' + result

        return result
