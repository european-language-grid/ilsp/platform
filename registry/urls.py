from django.urls import path, include
from rest_framework import routers

from dle import urls as dle_urls
from management import urls as management_urls
from registry import (
    views, lookups_views, schema_views
)
from registry.search_indexes import urls as search_index_urls
from dle.dle_report_index import urls as dle_report_index_urls

# CRU
router = routers.DefaultRouter()
router.register('metadatarecord', views.MetadataRecordViewSet,
                basename='metadatarecord')
router.register('xmlmetadatarecord', views.MetadataRecordXMLView, basename='xmlmetadatarecord')
router.register('batch_xml_retrieve', views.BatchMetadataRecordXMLView, basename='batch_xml_retrieve')
router.register('datacite-record', views.DataCiteMetadataRecordViewSet,
                basename='datacite-record')

# Schema Views
router.register('schema/described_entity/person', schema_views.SchemaPerson,
                basename='schemaperson')
router.register('schema/described_entity/organization',
                schema_views.SchemaOrganization, basename='schemaorganization')
router.register('schema/described_entity/group', schema_views.SchemaGroup,
                basename='schemagroup')
router.register('schema/described_entity/project', schema_views.SchemaProject,
                basename='schemaproject')
router.register('schema/described_entity/licence', schema_views.SchemaLicence,
                basename='schemalicence')
router.register('schema/described_entity/document', schema_views.SchemaDocument,
                basename='schemadocument')
router.register('schema/described_entity/lr',
                schema_views.SchemaLanguageResource, basename='schemalr')
router.register('schema/actor/generic_person', schema_views.SchemaGenericPerson,
                basename='schemagenericperson')
router.register('schema/actor/generic_organization',
                schema_views.SchemaGenericOrganization,
                basename='schemagenericorganization')
router.register('schema/actor/generic_group', schema_views.SchemaGenericGroup,
                basename='schemagenericgroup')
router.register('schema/lr_subclass/corpus', schema_views.SchemaCorpus,
                basename='schemacorpus')
router.register('schema/lr_subclass/lcr',
                schema_views.SchemaLexicalConceptualResource,
                basename='schemalrc')
router.register('schema/lr_subclass/language_description',
                schema_views.SchemaLanguageDescription,
                basename='schemalanguagedescription')
router.register('schema/lr_subclass/tool_service',
                schema_views.SchemaToolService, basename='schematoolservice')
router.register('schema/media_part/corpus_text_part',
                schema_views.SchemaCorpusTextPart,
                basename='schemacorpustextpart')
router.register('schema/media_part/corpus_audio_part',
                schema_views.SchemaCorpusAudioPart,
                basename='schemacorpusaudiopart')
router.register('schema/media_part/corpus_video_part',
                schema_views.SchemaCorpusVideoPart,
                basename='schemacorpusvideotpart')
router.register('schema/media_part/corpus_image_part',
                schema_views.SchemaCorpusImagePart,
                basename='schemacorpusimagepart')
router.register('schema/media_part/corpus_text_numerical_part',
                schema_views.SchemaCorpusTextNumericalPart,
                basename='schemacorpustextnumericalpart')
router.register('schema/media_part/lcr_text_part',
                schema_views.SchemaLexicalConceptualResourceTextPart,
                basename='schemalrctextnpart')
router.register('schema/media_part/lcr_audio_part',
                schema_views.SchemaLexicalConceptualResourceAudioPart,
                basename='schemalrcaudionpart')
router.register('schema/media_part/lcr_video_part',
                schema_views.SchemaLexicalConceptualResourceVideoPart,
                basename='schemalrcvideopart')
router.register('schema/media_part/lcr_image_part',
                schema_views.SchemaLexicalConceptualResourceImagePart,
                basename='schemalrcimagepart')
router.register('schema/media_part/language_description_text_part',
                schema_views.SchemaLanguageDescriptionTextPart,
                basename='schemalanguagedescriptiontextnpart')
router.register('schema/media_part/language_description_video_part',
                schema_views.SchemaLanguageDescriptionVideoPart,
                basename='schemalanguagedescriptionvideopart')
router.register('schema/media_part/language_description_image_part',
                schema_views.SchemaLanguageDescriptionImagePart,
                basename='schemalanguagedescriptionimagepart')
router.register('schema/ld_subclass/model', schema_views.SchemaModel,
                basename='schemamodel')
router.register('schema/ld_subclass/grammar', schema_views.SchemaGrammar,
                basename='schemagrammar')

# Match Views
router.register('match/organization', lookups_views.OrganizationMatchView,
                basename='organizationmatch')
router.register('match/project', lookups_views.ProjectMatchView,
                basename='projectmatch')
router.register('match/person', lookups_views.PersonMatchView,
                basename='personmatch')
router.register('match/group', lookups_views.GroupMatchView,
                basename='groupmatch')
router.register('match/document', lookups_views.DocumentMatchView,
                basename='documentmatch')
router.register('match/licence_terms', lookups_views.LicenceTermsMatchView,
                basename='licencetermsmatch')
router.register('match/repository', lookups_views.RepositoryMatchView,
                basename='repositorymatch')
router.register('match/lr', lookups_views.LanguageResourceMatchView,
                basename='lrmatch')

# Lookup Views
router.register('lookup/organization', lookups_views.OrganizationLookUpView,
                basename='organizationlookup')
router.register('lookup/person', lookups_views.PersonLookUpView,
                basename='personlookup')
router.register('lookup/group', lookups_views.GroupLookUpView,
                basename='grouplookup')
router.register('lookup/actor', lookups_views.ActorLookUpView,
                basename='actorlookup')
router.register('lookup/domain', lookups_views.DomainLookUpView,
                basename='domainlookup')
router.register('lookup/text_type', lookups_views.TextTypeLookUpView,
                basename='texttypelookup')
router.register('lookup/audio_genre', lookups_views.AudioGenreLookUpView,
                basename='audiogenrelookup')
router.register('lookup/speech_genre', lookups_views.SpeechGenreLookUpView,
                basename='speechgenrelookup')
router.register('lookup/image_genre', lookups_views.ImageGenreLookUpView,
                basename='imagegenrelookup')
router.register('lookup/video_genre', lookups_views.VideoGenreLookUpView,
                basename='videogenrelookup')
router.register('lookup/text_genre', lookups_views.TextGenreLookUpView,
                basename='textgenrelookup')
router.register('lookup/subject', lookups_views.SubjectLookUpView,
                basename='subjectlookup')
router.register('lookup/access_rights_statement',
                lookups_views.AccessRightsStatementLookUpView,
                basename='accessrightsstatementlookup')
router.register('lookup/cv_recommended', lookups_views.CVRecommendedLookUpView,
                basename='ltarealookup')
router.register('lookup/language', lookups_views.LanguageLookUpView,
                basename='languagelookup')
router.register('lookup/country', lookups_views.CountryLookUpView,
                basename='countrylookup')
router.register('lookup/project', lookups_views.ProjectLookUpView,
                basename='projectlookup')
router.register('lookup/lr', lookups_views.LanguageResourceLookUpView,
                basename='lrlookup')
router.register('lookup/document', lookups_views.DocumentLookUpView,
                basename='documentlookup')
router.register('lookup/licence_terms', lookups_views.LicenceTermsLookUpView,
                basename='licencetermslookup')
router.register('lookup/repository', lookups_views.RepositoryLookUpView,
                basename='repositorylookup')
router.register('lookup/keyword', lookups_views.KeywordLookUpView,
                basename='keywordlookup')
router.register('lookup/service_offered', lookups_views.ServiceOfferedLookUpView,
                basename='serviceoffered')
router.register('lookup/corpus_register', lookups_views.CorpusRegisterLookUpView,
                basename='corpusregister')

app_name = 'registry'

urlpatterns = (
    path('', include(router.urls)),
    path('rdf_retrieve/<int:pk>/', views.XMLToRDFView.as_view(), name='xml_to_rdf'),
    path('move_unspecified/<int:pk>/', views.MoveUnspecifiedToMediaView.as_view()),
    path('json_batch_upload/', views.batch_metadata_upload_json,
         name='json_batch_upload'),
    path('concept-record/<concept_pid>/', views.concept_record,
         name='concept-record'),
    path('batch_create/', views.batch_metadata_upload,
         name='batch_metadatarecord'),
    # Schema2 views
    path('schema2/<supermodel>/<model>/',
         schema_views.schema_version2,
         name='schema2_toolservice'),
    path('management/', include(management_urls)),
    path('search/', include(search_index_urls)),
    path('dle_report_index/', include(dle_report_index_urls)),
    path('dle-info/', include(dle_urls))
)

