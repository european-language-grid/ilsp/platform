import logging
from collections import (
    defaultdict, OrderedDict
)
from itertools import chain

from django.core.exceptions import ObjectDoesNotExist
from packaging import version as _ver

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q, QuerySet
from django.utils.translation import gettext_lazy as _
from drf_writable_nested import WritableNestedModelSerializer
from language_tags import tags
from rest_framework import serializers
from rest_framework.fields import empty

from harvesting_scripts.harvesting_utils import harvesting_duplicate_handling, \
    harvesting_versions_of_existing_records_handling, harvesting_version_and_duplicate_handling
from management.serializers import (
    ContentFileSerializer, ManagerSerializer
)
from registry import (
    models, choices
)
from registry.choices import (
    LR_IDENTIFIER_SCHEME_CHOICES, METADATA_RECORD_IDENTIFIER_SCHEME_CHOICES
)
from registry.fields import (
    SerializersMultilingualField, SerializersVersionField
)
from registry.registry_identifier import (
    remove_registry_identifier, add_identifier, get_identifier_value
)
from registry.retrieval_mechanisms import (
    retrieve_by_registry_identifier,
    retrieve_person_by_email,
    retrieve_model_by_website, retrieve_licence_by_licence_terms_and_name,
    retrieve_model_by_all_elements, retrieve_model_by_organization_name,
    retrieve_person_by_full_name, retrieve_document_by_title,
    retrieve_licence_terms_given_only_name,
    retrieve_lr_by_resource_name,
    retrieve_project_by_grant_number_or_name,
    retrieve_by_non_registry_identifier,
    retrieve_model_by_pk, retrieve_repository_by_repository_url_and_name, retrieve_repository_given_only_name
)
from registry.serializers_display import (
    DisplaySerializer,
    DisplayGenericSerializer,
    display_email, display_org_email
)
from registry.serializers_polymorphic import CustomPolymorphicSerializer as PolymorphicSerializer
from registry.serializers_draft import DraftSerializer
from registry.serializers_related import RelatedSerializer
from registry.serializers_xml import (
    XMLSerializer, XMLPolymorphicSerializer
)
from registry.utils.checks import (
    described_entity_is_generic, is_functional_service
)
from registry.utils.language_tag_utils import collective_language_ids
from registry.utils.retrieval_utils import get_unlisted_versions
from registry.utils.string_utils import (
    sanitize_field, ALLOWED_TAGS, ALLOWED_ATTRS, ALLOWED_PROTOCOLS, h_encode
)
from registry.utils.task_utils import handle_consent_email_upon_creation
from registry.validators import (
    dataset_distribution_validation,
    unique_identifier_scheme_validation,
    unspecified_in_condition_of_use_validation, record_already_exists,
    validate_dict_value_length, validate_dict_existence, dataset_distribution_unspecified_part_validation,
    unique_language_validation, validate_unique_multiple_multilingual_field
)
from utils.encoding import encode_iri_to_uri
from utils.glottolog_service import requests

LOGGER = logging.getLogger(__name__)

HIDDEN_VALUE_URL = 'http://www.hiddenLocation.org'
HIDDEN_VALUE_STR = 'Hidden value'


class MetadataRecordIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a metadata record
    """
    metadata_record_identifier_scheme = serializers.ChoiceField(
        choices=choices.METADATA_RECORD_IDENTIFIER_SCHEME_CHOICES,
        label=_('Metadata record identifier scheme'),
        help_text=_('The name of the scheme according to which the metadata'
                    ' record identifier is assigned by the authority that'
                    ' issues it (e.g., DOI, URL, etc.)'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.MetadataRecordIdentifier
        fields = (
            'pk',
            'metadata_record_identifier_scheme',
            'value',
        )

    def update(self, instance, validated_data):
        if instance.metadata_record_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return instance
        return super().update(instance, validated_data)


class LRIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a language resource
    """
    lr_identifier_scheme = serializers.ChoiceField(
        choices=choices.LR_IDENTIFIER_SCHEME_CHOICES,
        label=_('LR identifier scheme'),
        help_text=_('The name of the scheme according to which the LRT'
                    ' identifier is assigned by the authority that issues it'
                    ' (e.g., DOI, ISLRN, etc.)'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.LRIdentifier
        fields = (
            'pk',
            'lr_identifier_scheme',
            'value',
        )

    def update(self, instance, validated_data):
        if instance.lr_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return instance
        return super().update(instance, validated_data)


class GenericMetadataRecordSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplayGenericSerializer, DraftSerializer, RelatedSerializer):
    """
    """
    # OneToOne relation
    metadata_record_identifier = MetadataRecordIdentifierSerializer(
        label=_('Metadata record identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a metadata record'),
    )

    class Meta:
        model = models.GenericMetadataRecord
        fields = (
            'pk',
            'metadata_record_identifier',
        )

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        md_id_xml_field_name = self.Meta.model.schema_fields['metadata_record_identifier']
        if representation[md_id_xml_field_name] is not None and \
                representation[md_id_xml_field_name][
                    MetadataRecordIdentifierSerializer.Meta.model.schema_fields['metadata_record_identifier_scheme']
                ] == settings.REGISTRY_IDENTIFIER_SCHEMA:
            del representation[md_id_xml_field_name]
        return representation


class PersonalIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a person
    """
    personal_identifier_scheme = serializers.ChoiceField(
        choices=choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES,
        label=_('Person identfier scheme'),
        help_text=_('The name of the scheme used to identify a person'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.PersonalIdentifier
        fields = (
            'pk',
            'personal_identifier_scheme',
            'value',
        )

    def update(self, instance, validated_data):
        if instance.personal_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return instance
        return super().update(instance, validated_data)


class RepositoryIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a repository
    """
    repository_identifier_scheme = serializers.ChoiceField(
        choices=choices.REPOSITORY_IDENTIFIER_SCHEME_CHOICES,
        label=_('Repository identifier scheme'),
        help_text=_('The name of the scheme used to identify a repository'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.RepositoryIdentifier
        fields = (
            'pk',
            'repository_identifier_scheme',
            'value',
        )

    def update(self, instance, validated_data):
        if instance.repository_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return instance
        return super().update(instance, validated_data)


class OrganizationIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify an organization
    """
    organization_identifier_scheme = serializers.ChoiceField(
        choices=choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES,
        label=_('Organization identifier scheme'),
        help_text=_('The name of the scheme used to identify an organization'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.OrganizationIdentifier
        fields = (
            'pk',
            'organization_identifier_scheme',
            'value',
        )

    def update(self, instance, validated_data):
        if instance.organization_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return instance
        return super().update(instance, validated_data)


class AddressSetSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Links to the physical address of an organization, group or
    person represented as a set of distinct elements (street,
    number, zip code, city, etc.)
    """
    address = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Address'),
        help_text=_('The street and the number of the postal address of a'
                    ' person or organization'),
    )
    region = SerializersMultilingualField(
        validators=[validate_dict_value_length(200)],
        allow_null=True,
        required=False,
        label=_('Region'),
        help_text=_('The name of the region, county or department as mentioned'
                    ' in the postal address of a person or organization'),
    )
    city = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('City'),
        help_text=_('The name of the city, town or village as mentioned in the'
                    ' postal address of a person or organization '),
    )
    country = serializers.ChoiceField(
        choices=choices.REGION_ID_CHOICES,
        label=_('Country'),
        help_text=_('Introduces the name of the country mentioned in the postal'
                    ' address of a person or organization as defined in the'
                    ' list of values of ISO 3166'),
    )

    class Meta:
        model = models.AddressSet
        fields = (
                'pk',
                'address',
                'region',
                'zip_code',
                'city',
                'country',
        )
        extra_kwargs = {
                'address': {'style': {'max_length': 300, }},
                'region': {'style': {'max_length': 200, }},
                'city': {'style': {'max_length': 100, }},
        }


class GenericOrganizationSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplayGenericSerializer, DraftSerializer, RelatedSerializer):
    """
    """
    organization_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Organization name'),
        help_text=_('The official title of an organization'),
    )
    organization_short_name = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Organization short name'),
        help_text=_('Introduces the short name (abbreviation, acronym, etc.)'
                    ' used for an organization'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    organization_identifier = OrganizationIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Organization identifier'),
        help_text=_('A string used to uniquely identify an organization'),
    )
    # OneToOne relation
    head_office_address = AddressSetSerializer(
        allow_null=True,
        required=False,
        label=_('Address (head office)'),
        help_text=_('Links to the physical address of the head office of an'
                    ' organization or group represented as a set of distinct'
                    ' elements (street, zip code, city, etc.)'),
    )
    organization_alternative_name = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300)]
        ),
        allow_null=True,
        required=False,
        label=_('Organization alternative name'),
        help_text=_('Introduces an alternative name (other than the official'
                    ' title or the short name) used for an organization'),
        validators=[validate_unique_multiple_multilingual_field]
    )

    class Meta:
        model = models.Organization
        fields = (
            'pk',
            'actor_type',
            'organization_name',
            'organization_short_name',
            'organization_identifier',
            'website',
            'is_division_of',
            'head_office_address',
            'organization_alternative_name',
        )
        extra_kwargs = {
            'organization_name': {'style': {'max_length': 300, }},
            'organization_short_name': {'style': {'max_length': 100, }},
            'organization_identifier': {'style': {'recommended': True, }},
            'website': {'style': {'recommended': True, }},
            'head_office_address': {'style': {'recommended': True, }},
            'organization_alternative_name': {'style': {'max_length': 300, 'recommended': True, }},
        }
        to_display_false = [
            'actor_type',
        ]

    def get_fields(self):
        fields = super().get_fields()
        fields['is_division_of'] = GenericOrganizationSerializer(
            many=True,
            allow_null=True,
            required=False,
            label=_('Division of'),
            help_text=_('Links a division (e.g., company branch, university'
                        ' department or faculty) to the organization to which it'
                        ' belongs')
        )
        return fields

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if registry identifier exists
        instance = retrieve_by_registry_identifier(self.Meta.model,
                                                   validated_data,
                                                   request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if non-registry identifier exists
        instance = retrieve_by_non_registry_identifier(self.Meta.model,
                                                       validated_data,
                                                       request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if website exists
        instance = retrieve_model_by_website(self.Meta.model, validated_data, request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if organization name exists
        instance = retrieve_model_by_organization_name(self.Meta.model,
                                                       validated_data, request_user)
        if instance is not None:
            return instance
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'organization_identifier',
                                                    'organization_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'organization_identifier',
                                                       'organization_identifier_scheme')
        instance = super().create(validated_data)
        handle_consent_email_upon_creation(instance,
                                           name='To Whom It May',
                                           surname='Concern',
                                           entity=self.Meta.model
                                           )
        return instance

    def update(self, instance, validated_data):
        # ignore update for generic serializers
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'website')
        return super().to_internal_value(data)

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='organization_identifier',
                                            identifier_scheme='organization_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['organization_identifier'],
                                                    OrganizationIdentifierSerializer.Meta.model.schema_fields[
                                                        'organization_identifier_scheme'])
        return representation


class AffiliationSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Introduces information on an organization to whom the person is
    affiliated and the data relevant to this relation (i.e.
    position, email, etc.)
    """
    position = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Position'),
        help_text=_('The position or the title of a person if affiliated to an'
                    ' organization '),
    )
    # FK relation
    affiliated_organization = GenericOrganizationSerializer(
        label=_('Affiliated organization'),
        help_text=_('Specifies the organization that a person is affiliated'
                    ' with or works for'),
    )
    # Reverse FK relation
    address_set = AddressSetSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Address'),
        help_text=_('Links to the physical address of an organization, group or'
                    ' person represented as a set of distinct elements (street,'
                    ' number, zip code, city, etc.)'),
    )

    class Meta:
        model = models.Affiliation
        fields = (
            'pk',
            'position',
            'affiliated_organization',
            'email',
            'homepage',
            'address_set',
            'telephone_number',
            'fax_number',
        )
        extra_kwargs = {
            'position': {'style': {'max_length': 100, 'recommended': True, }},
            'email': {'style': {'recommended': True, }},
        }

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'homepage')
        return super().to_internal_value(data)

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        # Remove email due to GDPR, if no consent
        if not ret.get('email', None) is None:
            display_email(ret, True)
        return ret


class GenericPersonSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplayGenericSerializer, DraftSerializer, RelatedSerializer):
    """
    """
    surname = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Surname'),
        help_text=_('Introduces the surname (i.e. the name shared by members of'
                    ' a family, also known as \'family name\' or \'last name\') of'
                    ' a person'),
    )
    given_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(200), validate_dict_existence],
        label=_('Given name'),
        help_text=_('Introduces the given name (also known as \'first name\') of'
                    ' a person'),
    )
    name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Name'),
        help_text=_('Introduces the full name of a person; recommended format'
                    ' \'surname, givenName\''),
    )
    # Reverse FK relation
    personal_identifier = PersonalIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Person identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a person'),
    )
    # Reverse FK relation
    affiliation = AffiliationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Affiliation'),
        help_text=_('Introduces information on an organization to whom the'
                    ' person is affiliated and the data relevant to this'
                    ' relation (i.e. position, email, etc.)'),
    )

    class Meta:
        model = models.Person
        fields = (
            'pk',
            'actor_type',
            'surname',
            'given_name',
            'name',
            'personal_identifier',
            'email',
            'affiliation',
        )
        extra_kwargs = {
            'surname': {'style': {'max_length': 300, }},
            'given_name': {'style': {'max_length': 200, }},
            'name': {'style': {'max_length': 300, }},
            'personal_identifier': {'style': {'recommended': True, }},
            'email': {'style': {'recommended': True, }},
        }
        to_display_false = [
            'actor_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if registry identifier exists
        instance = retrieve_by_registry_identifier(self.Meta.model,
                                                   validated_data,
                                                   request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if non-registry identifier exists
        instance = retrieve_by_non_registry_identifier(self.Meta.model,
                                                       validated_data,
                                                       request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if Person with email exists
        instance = retrieve_person_by_email(validated_data, request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if Person with surname and given_name exists
        instance = retrieve_person_by_full_name(validated_data, request_user)
        if instance is not None:
            return instance
        # Remove Regisry identifiers
        # From validated data
        LOGGER.debug("{} - Remove from record Regisry ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'personal_identifier',
                                                    'personal_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Regisry ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'personal_identifier',
                                                       'personal_identifier_scheme')
        instance = super().create(validated_data)
        handle_consent_email_upon_creation(instance,
                                           name=instance.given_name['en'],
                                           surname=instance.surname['en'],
                                           entity=self.Meta.model
                                           )
        return instance

    def update(self, instance, validated_data):
        # ignore update for generic serializers
        return instance

    def validate(self, data):
        for_information_only = self.context.get('for_information_only', False)
        if for_information_only is False and data.get('name', None) is not None:
            raise serializers.ValidationError(
                {'name': _(f"{self.fields['name'].label} can be used only for infomation only records.")},
                code='invalid',
            )
        unique_identifier_scheme_validation(data,
                                            entity_identifier='personal_identifier',
                                            identifier_scheme='personal_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        # Remove email due to GDPR
        if self.Meta.model.schema_fields['email'] in representation:
            del representation[self.Meta.model.schema_fields['email']]
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['personal_identifier'],
                                                    PersonalIdentifierSerializer.Meta.model.schema_fields[
                                                        'personal_identifier_scheme'])
        return representation

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        # Remove email due to GDPR, if no consent
        if not ret.get('email', None) is None:
            display_email(ret, instance.consent)
        return ret


class GenericRepositorySerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplayGenericSerializer, DraftSerializer, RelatedSerializer):
    """
    """
    repository_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Repository name'),
        help_text=_('The official full name of the repository'),
    )
    # Reverse FK relation
    repository_identifier = RepositoryIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Repository identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a repository'),
    )
    # FK relation
    repository_institution = GenericOrganizationSerializer(
        allow_null=True,
        required=False,
        label=_('Repository institution'),
        help_text=_('Groups information on the institution that is responsible'
                    ' for the repository'),
    )

    class Meta:
        model = models.Repository
        fields = (
            'pk',
            'repository_name',
            'repository_url',
            'repository_identifier',
            'repository_institution',
        )
        extra_kwargs = {
            'repository_name': {'style': {'max_length': 300, }},
            'repository_institution': {'style': {'recommended': True, }},
        }

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if registry identifier exists
        instance = retrieve_by_registry_identifier(self.Meta.model,
                                                   validated_data,
                                                   request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if non-registry identifier exists
        instance = retrieve_by_non_registry_identifier(self.Meta.model,
                                                       validated_data,
                                                       request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if repository url and repository name exist
        instance = retrieve_repository_by_repository_url_and_name(validated_data, request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if repository name exists
        instance = retrieve_repository_given_only_name(validated_data, request_user)
        if instance is not None:
            return instance
        # Remove Register identifiers
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'repository_identifier',
                                                    'repository_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'repository_identifier',
                                                       'repository_identifier_scheme')
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        # ignore update for generic serializers
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'repository_url')
        return super().to_internal_value(data)

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='repository_identifier',
                                            identifier_scheme='repository_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['repository_identifier'],
                                                    RepositoryIdentifierSerializer.Meta.model.schema_fields[
                                                        'repository_identifier_scheme'])
        return representation


class additionalInfoSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Introduces a point that can be used for further information
    (e.g. a landing page with a more detailed description of the
    resource or a general email that can be contacted for further
    queries)
    """

    class Meta:
        model = models.additionalInfo
        fields = (
            'pk',
            'landing_page',
            'email',
        )

    def validate(self, data):
        if data.get('landing_page', None) and data.get('email', None):
            raise serializers.ValidationError(
                _('Use either Landing page or Email, not both'),
                code='invalid',
            )
        if not (data.get('landing_page', None) or data.get('email', None)):
            raise serializers.ValidationError(
                _('Use either Landing page or Email'),
                code='invalid',
            )
        return data

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'landing_page')
        return super().to_internal_value(data)

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        # Remove email due to GDPR, if no consent
        if not ret.get('email', None) is None:
            display_email(ret, instance.consent)
        return ret


class GroupIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify a group
    """
    organization_identifier_scheme = serializers.ChoiceField(
        choices=choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES,
        label=_('Organization identifier scheme'),
        help_text=_('The name of the scheme used to identify an organization'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.GroupIdentifier
        fields = (
            'pk',
            'organization_identifier_scheme',
            'value',
        )

    def update(self, instance, validated_data):
        if instance.organization_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return instance
        return super().update(instance, validated_data)


class GenericGroupSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplayGenericSerializer, DraftSerializer, RelatedSerializer):
    """
    """
    organization_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Organization name'),
        help_text=_('The official title of an organization'),
    )
    # Reverse FK relation
    group_identifier = GroupIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Group identifier'),
        help_text=_('A string used to uniquely identify a group'),
    )

    class Meta:
        model = models.Group
        fields = (
            'pk',
            'actor_type',
            'organization_name',
            'group_identifier',
            'website',
        )
        extra_kwargs = {
            'organization_name': {'style': {'max_length': 300, }},
            'group_identifier': {'style': {'recommended': True, }},
            'website': {'style': {'recommended': True, }},
        }
        to_display_false = [
            'actor_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if registry identifier exists
        instance = retrieve_by_registry_identifier(self.Meta.model,
                                                   validated_data,
                                                   request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if non-registry identifier exists
        instance = retrieve_by_non_registry_identifier(self.Meta.model,
                                                       validated_data,
                                                       request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if website exists
        instance = retrieve_model_by_website(self.Meta.model, validated_data, request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if organization name exists
        instance = retrieve_model_by_organization_name(self.Meta.model,
                                                       validated_data,
                                                       request_user)
        if instance is not None:
            return instance
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'group_identifier',
                                                    'organization_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'group_identifier',
                                                       'organization_identifier_scheme')
        instance = super().create(validated_data)
        handle_consent_email_upon_creation(instance,
                                           name='To Whom It May',
                                           surname='Concern',
                                           entity=self.Meta.model
                                           )
        return instance

    def update(self, instance, validated_data):
        # ignore update for generic serializers
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'website')
        return super().to_internal_value(data)

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='group_identifier',
                                            identifier_scheme='organization_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['group_identifier'],
                                                    OrganizationIdentifierSerializer.Meta.model.schema_fields[
                                                        'organization_identifier_scheme'])
        return representation


class ActorSerializer(
    PolymorphicSerializer, WritableNestedModelSerializer,
    XMLPolymorphicSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A person, organization or group that participates in an event or
    process
    """

    class Meta:
        model = models.Actor
        fields = '__all__'

    model_serializer_mapping = {
        models.Person: GenericPersonSerializer,
        models.Organization: GenericOrganizationSerializer,
        models.Group: GenericGroupSerializer
    }

    resource_type_field_name = 'actor_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _(
                    'Actor type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]


class DomainIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify a domain according to a
    specific classification scheme
    """
    domain_classification_scheme = serializers.ChoiceField(
        choices=choices.DOMAIN_CLASSIFICATION_SCHEME_CHOICES,
        label=_('Domain classification scheme'),
        help_text=_('A classification scheme devised by an authority for'
                    ' domains'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.DomainIdentifier
        fields = (
            'pk',
            'domain_classification_scheme',
            'value',
        )


class DomainSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Identifies the domain according to which a resource is
    classified
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    domain_identifier = DomainIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Domain identifier'),
        help_text=_('A string used to uniquely identify a domain according to a'
                    ' specific classification scheme'),
    )

    class Meta:
        model = models.Domain
        fields = (
            'pk',
            'category_label',
            'domain_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve instance given all elements
        instance = retrieve_model_by_all_elements(self.Meta.model,
                                                  validated_data)
        if instance is not None:
            return instance
        # If no instance retrieve create a new one
        instance = super().create(validated_data)
        return instance


class SubjectIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify a subject according to a
    specific classification scheme
    """
    subject_classification_scheme = serializers.ChoiceField(
        choices=choices.SUBJECT_CLASSIFICATION_SCHEME_CHOICES,
        label=_('Subject classification scheme'),
        help_text=_('A classification scheme used for subjects/topics devised'
                    ' by an authority or organization'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.SubjectIdentifier
        fields = (
            'pk',
            'subject_classification_scheme',
            'value',
        )


class SubjectSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Specifies the subject/topic of a language resource
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    subject_identifier = SubjectIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Subject identifier'),
        help_text=_('A string used to uniquely identify a subject according to'
                    ' a specific classification scheme'),
    )

    class Meta:
        model = models.Subject
        fields = (
            'pk',
            'category_label',
            'subject_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve instance given all elements
        instance = retrieve_model_by_all_elements(self.Meta.model,
                                                  validated_data)
        if instance is not None:
            return instance
        # If no instance retrieve create a new one
        instance = super().create(validated_data)
        return instance


class ProjectIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string (e.g., PID, internal to an organization, issued by the
    funding authority, etc.) used to uniquely identify a project
    """
    project_identifier_scheme = serializers.ChoiceField(
        choices=choices.PROJECT_IDENTIFIER_SCHEME_CHOICES,
        label=_('Project identifier scheme'),
        help_text=_('The name of the scheme according to which an identifier is'
                    ' assigned to a project by the authority that issues it'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.ProjectIdentifier
        fields = (
            'pk',
            'project_identifier_scheme',
            'value',
        )

    def update(self, instance, validated_data):
        if instance.project_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return instance
        return super().update(instance, validated_data)


class GenericProjectSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplayGenericSerializer, DraftSerializer, RelatedSerializer):
    """
    """
    project_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Project name'),
        help_text=_('The official title of a project'),
    )
    project_short_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Project short name'),
        help_text=_('Introduces a short name (e.g., acronym, abbreviated form)'
                    ' by which a project is known'),
    )
    # Reverse FK relation
    project_identifier = ProjectIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Project identifier'),
        help_text=_('A string (e.g., PID, internal to an organization, issued'
                    ' by the funding authority, etc.) used to uniquely identify'
                    ' a project'),
    )
    # ManyToMany relation
    funder = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Funder'),
        help_text=_('Identifies the person or organization or group that has'
                    ' financed the project'),
    )

    class Meta:
        model = models.Project
        fields = (
            'pk',
            'project_name',
            'project_short_name',
            'project_identifier',
            'website',
            'grant_number',
            'funding_type',
            'funder',
        )
        extra_kwargs = {
            'project_name': {'style': {'max_length': 300, }},
            'project_short_name': {'style': {'max_length': 100, }},
            'website': {'style': {'recommended': True, }},
            'grant_number': {'style': {'recommended': True, }},
            'funder': {'style': {'recommended': True, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if registry identifier exists
        instance = retrieve_by_registry_identifier(self.Meta.model,
                                                   validated_data,
                                                   request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if non-registry identifier exists
        instance = retrieve_by_non_registry_identifier(self.Meta.model,
                                                       validated_data,
                                                       request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if grant_number or project_name exists
        instance = retrieve_project_by_grant_number_or_name(validated_data, request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if website exists
        instance = retrieve_model_by_website(self.Meta.model, validated_data, request_user)
        if instance is not None:
            return instance
        # Remove Register identifier to verify that repo creates each own only
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'project_identifier',
                                                    'project_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'project_identifier',
                                                       'project_identifier_scheme')
        instance = super().create(validated_data)
        handle_consent_email_upon_creation(instance,
                                           name='To Whom It May',
                                           surname='Concern',
                                           entity=self.Meta.model
                                           )
        return instance

    def update(self, instance, validated_data):
        # ignore update for generic serializers
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'website')
        return super().to_internal_value(data)

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='project_identifier',
                                            identifier_scheme='project_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['project_identifier'],
                                                    ProjectIdentifierSerializer.Meta.model.schema_fields[
                                                        'project_identifier_scheme'])
        return representation


class GenericLanguageResourceSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplayGenericSerializer, DraftSerializer, RelatedSerializer):
    """
    A resource composed of linguistic material used in the
    construction, improvement and/or evaluation of language
    processing applications, but also, in a broader sense, in
    language and language-mediated research studies and
    applications; the term is used with a broader meaning,
    encompassing (a) data sets (textual, multimodal/multimedia and
    lexical data, grammars, language models, etc.) in machine
    readable form, and (b) tools/technologies/services used for
    their processing and management.
    """
    resource_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000), validate_dict_existence],
        label=_('Resource name'),
        help_text=_('Introduces a human-readable name or title by which the'
                    ' resource is known'),
    )
    # Reverse FK relation
    lr_identifier = LRIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Language Resource identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a language resource'),
    )

    class Meta:
        model = models.LanguageResource
        fields = (
            'pk',
            'resource_name',
            'lr_identifier',
            'version',
        )
        extra_kwargs = {
            'resource_name': {'style': {'max_length': 1000, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if registry identifier exists
        instance = retrieve_by_registry_identifier(self.Meta.model,
                                                   validated_data,
                                                   request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if non-registry identifier exists
        instance = retrieve_by_non_registry_identifier(self.Meta.model,
                                                       validated_data,
                                                       request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if resource_name exists
        instance = retrieve_lr_by_resource_name(validated_data, request_user)
        if instance is not None:
            return instance
        # Remove Register identifier to verify that repo creates each own only
        # From validated data
        LOGGER.debug(
            "{} - Remove from record Register ID - validate data.".format(
                self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'lr_identifier',
                                                    'lr_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug(
            "{} - Remove from record Register ID - initial data.".format(
                self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'lr_identifier',
                                                       'lr_identifier_scheme')
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        # ignore update for generic serializers
        return instance

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        if ret['version'] == '':
            ret['version'] = 'unspecified'
        return ret

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='lr_identifier',
                                            identifier_scheme='lr_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation, self.Meta.model.schema_fields['lr_identifier'],
                                                    LRIdentifierSerializer.Meta.model.schema_fields[
                                                        'lr_identifier_scheme'])
        return representation


class DocumentIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a document (mainly intended for
    published documents)
    """
    document_identifier_scheme = serializers.ChoiceField(
        choices=choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES,
        label=_('Document identifier scheme'),
        help_text=_('A scheme according to which an identifier is assigned by'
                    ' the authority that issues it (e.g., DOI, PubMed Central,'
                    ' etc.) specifically for publications'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.DocumentIdentifier
        fields = (
            'pk',
            'document_identifier_scheme',
            'value',
        )

    def update(self, instance, validated_data):
        if instance.document_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return instance
        return super().update(instance, validated_data)


class GenericDocumentSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplayGenericSerializer, DraftSerializer, RelatedSerializer):
    """
    """
    title = SerializersMultilingualField(
        validators=[validate_dict_value_length(500), validate_dict_existence],
        label=_('Title'),
        help_text=_('Introduces a human-readable text (typically short) used as'
                    ' a name for referring to the document being described'),
    )
    # Reverse FK relation
    document_identifier = DocumentIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Document identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a document (mainly'
                    ' intended for published documents)'),
    )

    class Meta:
        model = models.Document
        fields = (
            'pk',
            'title',
            'document_identifier',
            'bibliographic_record',
        )
        extra_kwargs = {
            'title': {'style': {'max_length': 500, }},
            'bibliographic_record': {'style': {'recommended': True, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if registry identifier exists
        instance = retrieve_by_registry_identifier(self.Meta.model,
                                                   validated_data,
                                                   request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if non-registry identifier exists
        instance = retrieve_by_non_registry_identifier(self.Meta.model,
                                                       validated_data,
                                                       request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if title exists
        instance = retrieve_document_by_title(validated_data, request_user)
        if instance is not None:
            return instance
        # From validated data
        LOGGER.debug(
            "{} - Remove from record Register ID - validate data.".format(
                self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'document_identifier',
                                                    'document_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug(
            "{} - Remove from record Register ID - initial data.".format(
                self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'document_identifier',
                                                       'document_identifier_scheme')
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        # ignore update for generic serializers
        return instance

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='document_identifier',
                                            identifier_scheme='document_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['document_identifier'],
                                                    DocumentIdentifierSerializer.Meta.model.schema_fields[
                                                        'document_identifier_scheme'])
        return representation


class ActualUseSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Provides information on how the resource has been used (e.g., in
    a project, for a publication, for testing a tool/service, etc.)
    """
    # ManyToMany relation
    has_outcome = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Outcomes'),
        help_text=_('Links to LR B which is created / extracted from LR A (the'
                    ' one being described), i.e. LR A has been used as the'
                    ' basis/initial/source material for LR B'),
    )
    # ManyToMany relation
    usage_project = GenericProjectSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Usage project'),
        help_text=_('Links to a project in which the language resource has been'
                    ' used'),
    )
    # ManyToMany relation
    usage_report = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Usage report'),
        help_text=_('Links to a document (e.g., research article, report)'
                    ' describing a project, application, experiment or use case'
                    ' where the language resource has been used'),
    )
    actual_use_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Actual use details'),
        help_text=_('Describes in free text specific use cases or situations'
                    ' where the language resource has been used'),
    )

    class Meta:
        model = models.ActualUse
        fields = (
            'pk',
            'used_in_application',
            'has_outcome',
            'usage_project',
            'usage_report',
            'actual_use_details',
        )
        extra_kwargs = {
            'used_in_application': {'style': {'recommended': True, }},
            'usage_project': {'style': {'recommended': True, }},
            'usage_report': {'style': {'recommended': True, }},
            'actual_use_details': {'style': {'max_length': 500, }},
        }

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        used_in_app_xml_field_name = self.Meta.model.schema_fields['used_in_application']
        if representation.get(used_in_app_xml_field_name, None):
            representation[used_in_app_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[used_in_app_xml_field_name], choices.LT_CLASS_RECOMMENDED_CHOICES,
                'ms:LTClassRecommended', 'ms:LTClassOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['used_in_application'] = super().from_xml_representation_for_recommended_cv(ret['used_in_application'],
                                                                                        'used_in_application',
                                                                                        choices.LT_CLASS_RECOMMENDED_CHOICES,
                                                                                        'ms:LTClassRecommended',
                                                                                        'ms:LTClassOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['used_in_application'] = super().to_display_representation_for_recommended_cv(
            ret['used_in_application'], choices.LT_CLASS_RECOMMENDED_CHOICES)
        return ret


class ValidationSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Provides information on each of the the validation procedure(s)
    a language resource may have undergone (e.g., formal validation
    of the whole resource, content validation of one part of the
    resource, etc.).
    """
    validation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Validation details'),
        help_text=_('Provides additional information in the form of free text'
                    ' about the validation process of a language resource'),
    )
    # ManyToMany relation
    is_validated_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Validation tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' validating LR A (the one being described)'),
    )
    # ManyToMany relation
    validation_report = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Validation report'),
        help_text=_('Links to a document with detailed information on the'
                    ' validation process and results'),
    )
    # ManyToMany relation
    validator = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Validator'),
        help_text=_('Links a validation to the person(s), group(s) or'
                    ' organization(s) that have performed a specific validation'
                    ' of a language resource'),
    )

    class Meta:
        model = models.Validation
        fields = (
            'pk',
            'validation_type',
            'validation_mode',
            'validation_details',
            'validation_extent',
            'is_validated_by',
            'validation_report',
            'validator',
        )
        extra_kwargs = {
            'validation_details': {'style': {'max_length': 500, }},
            'validation_report': {'style': {'recommended': True, }},
        }


class RelationSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Links a language resource to other related resources specifying
    also the type of relation
    """
    relation_type = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Relation type'),
        help_text=_('Describes in a free text statement a relation holding'
                    ' between two Language Resources not yet covered by the'
                    ' schema'),
    )
    # FK relation
    related_lr = GenericLanguageResourceSerializer(
        label=_('Related language resource/technology'),
        help_text=_('Identifies the language resource with which a relation is'
                    ' holding with the one being described'),
    )

    class Meta:
        model = models.Relation
        fields = (
            'pk',
            'relation_type',
            'related_lr',
        )
        extra_kwargs = {
            'relation_type': {'style': {'max_length': 150, }},
        }


class RoleIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify a role according to a
    specific scheme
    """
    role_identifier_scheme = serializers.ChoiceField(
        choices=choices.ROLE_IDENTIFIER_SCHEME_CHOICES,
        label=_('Role identifier scheme'),
        help_text=_('The name of the scheme used to identify a role'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.RoleIdentifier
        fields = (
            'pk',
            'role_identifier_scheme',
            'value',
        )


class RoleSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Category/class of the roles for an agent
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    role_identifier = RoleIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Role identifier'),
        help_text=_('A string used to uniquely identify a role according to a'
                    ' specific scheme'),
    )

    class Meta:
        model = models.Role
        fields = (
            'pk',
            'category_label',
            'role_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }


class AttributionSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    """
    # FK relation
    attributed_agent = ActorSerializer(
        label='',
        help_text=''
    )
    # ManyToMany relation
    had_role = RoleSerializer(
        many=True,
        allow_empty=False,
        label=_('Had role'),
        help_text=_('Links to the role with which an agent is attributed for a'
                    ' certain activity'),
    )

    class Meta:
        model = models.Attribution
        fields = (
            'pk',
            'attributed_agent',
            'had_role',
        )


class LanguageSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Specifies the language that is used in the resource or supported
    by the tool/service, expressed according to the BCP47
    recommendation
    """
    language_id = serializers.ChoiceField(
        choices=choices.LANGUAGE_ID_CHOICES,
        label=_('Language'),
        help_text=_('The identifier of the language subelement according to the'
                    ' IETF BCP47 guidelines (i.e. ISO 639-3 codes when existing'
                    ' supplemented with ISO 639-3 codes for new entries)'),
    )
    language_variety_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(150)],
        allow_null=True,
        required=False,
        label=_('Language variety name'),
        help_text=_('A textual string used for referring to a language variety'),
    )

    class Meta:
        model = models.Language
        fields = (
            'pk',
            'language_tag',
            'language_id',
            'script_id',
            'region_id',
            'variant_id',
            'glottolog_code',
            'language_variety_name',
        )
        extra_kwargs = {
            'language_variety_name': {'style': {'max_length': 150, }},
        }

    def validate(self, data):
        if data.get('language_id', None) == 'mis' and not data.get('glottolog_code'):
            raise serializers.ValidationError(
                {'glottolog_code': _(
                    f"{self.fields['glottolog_code'].label} is required for Uncoded Languages.")},
                code='invalid',
            )
        if data.get('glottolog_code', None):
            valid_glottolog = requests.check_glottocode(data.get('glottolog_code', ''))
            if not valid_glottolog:
                raise serializers.ValidationError(
                    {'glottolog_code': _(
                        f"{data.get('glottolog_code', None)} is not a valid option for {self.fields['glottolog_code'].label}.")},
                    code='invalid',
                )
        return data

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        if str(tags.language(instance.language_id).script) == instance.script_id:
            instance.script_id = None
        ret = super().to_display_representation(instance)
        if getattr(instance, 'glottolog_code', None):
            glottolog_label = requests.get_name(instance.glottolog_code) or instance.glottolog_code
            ret['glottolog_code']['label'] = {'en': glottolog_label}
        return ret


class unspecifiedPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Used exclusively for "for-information" records where the media
    type cannot be computed or is not filled in
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    metalanguage = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    modality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Modality type details'),
        help_text=_('Provides further information on the modalities represented'
                    ' in a language resource'),
    )

    class Meta:
        model = models.unspecifiedPart
        fields = (
            'pk',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'metalanguage',
            'modality_type',
            'modality_type_details',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'modality_type_details': {'style': {'max_length': 500, }},
        }

    def create(self, validated_data):
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance

    def validate(self, data):
        unique_language_validation(data, 'language')
        unique_language_validation(data, 'metalanguage')
        return data


class TextTypeIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify a text type according to a
    specific classification scheme
    """
    text_type_classification_scheme = serializers.ChoiceField(
        choices=choices.TEXT_TYPE_CLASSIFICATION_SCHEME_CHOICES,
        label=_('Text type classification scheme'),
        help_text=_('A classification scheme used for text types devised by an'
                    ' authority or organization'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.TextTypeIdentifier
        fields = (
            'pk',
            'text_type_classification_scheme',
            'value',
        )


class TextTypeSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Specifies the text type (e.g., factual, literary, etc.)
    according to which a text corpus (part) is classified
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    text_type_identifier = TextTypeIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Text type identifier'),
        help_text=_('A string used to uniquely identify a text type according'
                    ' to a specific classification scheme'),
    )

    class Meta:
        model = models.TextType
        fields = (
            'pk',
            'category_label',
            'text_type_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve instance given all elements
        instance = retrieve_model_by_all_elements(self.Meta.model,
                                                  validated_data)
        if instance is not None:
            return instance
        # If no instance retrieve create a new one
        instance = super().create(validated_data)
        return instance


class TextGenreIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify a text genre according to a
    specific classification scheme
    """
    text_genre_classification_scheme = serializers.ChoiceField(
        choices=choices.TEXT_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        label=_('Text genre classification scheme'),
        help_text=_('A classification scheme used for text genres devised by an'
                    ' authority or organization'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.TextGenreIdentifier
        fields = (
            'pk',
            'text_genre_classification_scheme',
            'value',
        )


class TextGenreSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A category of text characterized by a particular style, form, or
    content according to a specific classification scheme
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    text_genre_identifier = TextGenreIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Text genre identifier'),
        help_text=_('A string used to uniquely identify a text genre according'
                    ' to a specific classification scheme'),
    )

    class Meta:
        model = models.TextGenre
        fields = (
            'pk',
            'category_label',
            'text_genre_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        # Retrieve instance given all elements
        instance = retrieve_model_by_all_elements(self.Meta.model,
                                                  validated_data)
        if instance is not None:
            return instance
        # If no instance retrieve create a new one
        instance = super().create(validated_data)
        return instance


class AnnotationSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Links a corpus to its annotated part(s)
    """
    annotation_type = serializers.ListField(
        child=serializers.CharField(max_length=300),
        allow_empty=False,
        label=_('Annotation type'),
        help_text=_('Specifies the annotation type of the annotated version(s)'
                    ' of a resource or the annotation type a tool/ service'
                    ' requires or produces as an output'),
    )
    # ManyToMany relation
    guidelines = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Guidelines'),
        help_text=_('Links to the document used as guidelines for the creation'
                    ' or annotation of a corpus'),
    )
    # ManyToMany relation
    typesystem = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Typesystem'),
        help_text=_('Specifies the typesystem (preferrably through an'
                    ' identifier or URL) that has been used for the annotation'
                    ' of a resource or that is required for the input resource'
                    ' of a tool/service or that should be used (dependency) for'
                    ' the annotation or used in the training of a ML model'),
    )
    # ManyToMany relation
    annotation_resource = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation resource'),
        help_text=_('Specifies the annotation resource used for annotating a'
                    ' corpus or that has been used for the input resource of a'
                    ' tool/service or that should be used (dependency) for the'
                    ' annotation'),
    )
    theoretic_model = SerializersMultilingualField(
        validators=[validate_dict_value_length(200)],
        allow_null=True,
        required=False,
        label=_('Theoretic model'),
        help_text=_('Indicates the theoretic model applied for the'
                    ' creation/enrichment of the resource, by name and/or by'
                    ' reference (URL or bibliographic reference) to informative'
                    ' material about the theoretic model used'),
    )
    annotation_mode_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000)],
        allow_null=True,
        required=False,
        label=_('Annotation mode details'),
        help_text=_('Provides further information on annotation process '),
    )
    # ManyToMany relation
    is_annotated_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation tool'),
        help_text=_('Links to a tool/service B that has been used for'
                    ' annotating LR A (the one being described), e.g., a'
                    ' tagger, NER, etc.'),
    )
    # ManyToMany relation
    annotator = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotator'),
        help_text=_('Identifies the person or organisation responsible for the'
                    ' annotation of a resource'),
    )
    interannotator_agreement = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Interannotator agreement'),
        help_text=_('Provides information on the inter-annotator agreement and'
                    ' the methods/metrics applied'),
    )
    intraannotator_agreement = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Intra-annotator agreement'),
        help_text=_('Provides information on the intra-annotator agreement and'
                    ' the methods/metrics applied '),
    )
    # ManyToMany relation
    annotation_report = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation report'),
        help_text=_('Links to a report describing the annotation process, tool,'
                    ' method, etc. of the language resource'),
    )

    class Meta:
        model = models.Annotation
        fields = (
            'pk',
            'annotation_type',
            'annotated_element',
            'segmentation_level',
            'annotation_standoff',
            'guidelines',
            'typesystem',
            'annotation_resource',
            'theoretic_model',
            'annotation_mode',
            'annotation_mode_details',
            'is_annotated_by',
            'annotator',
            'annotation_start_date',
            'annotation_end_date',
            'interannotator_agreement',
            'intraannotator_agreement',
            'annotation_report',
        )
        extra_kwargs = {
            'annotated_element': {'style': {'recommended': True, }},
            'guidelines': {'style': {'recommended': True, }},
            'typesystem': {'style': {'recommended': True, }},
            'annotation_resource': {'style': {'recommended': True, }},
            'theoretic_model': {'style': {'max_length': 200, }},
            'annotation_mode_details': {'style': {'max_length': 1000, }},
            'is_annotated_by': {'style': {'recommended': True, }},
            'interannotator_agreement': {'style': {'max_length': 300, }},
            'intraannotator_agreement': {'style': {'max_length': 300, }},
            'annotation_report': {'style': {'recommended': True, }},
        }

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        annotation_type_xml_field_name = self.Meta.model.schema_fields['annotation_type']
        if representation.get(annotation_type_xml_field_name, None):
            representation[annotation_type_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[annotation_type_xml_field_name], choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES,
                'ms:annotationTypeRecommended', 'ms:annotationTypeOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['annotation_type'] = super().from_xml_representation_for_recommended_cv(ret['annotation_type'],
                                                                                    'annotation_type',
                                                                                    choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES,
                                                                                    'ms:annotationTypeRecommended',
                                                                                    'ms:annotationTypeOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['annotation_type'] = super().to_display_representation_for_recommended_cv(
            ret['annotation_type'], choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES)
        return ret


class LinkToOtherMediaSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Describes the linking between different media parts of a
    resource (how they interact with or link to each other)
    """
    other_media = serializers.ListField(
        child=serializers.ChoiceField(
            choices=choices.OTHER_MEDIA_CHOICES),
        allow_empty=False,
        label=_('Other media'),
        help_text=_('Specifies the media types that are linked to the media'
                    ' type described within the same resource'),
    )
    media_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Media type details'),
        help_text=_('Provides further information on the way the media types'
                    ' are linked and/or synchronized with each other within the'
                    ' same resource'),
    )

    class Meta:
        model = models.LinkToOtherMedia
        fields = (
            'pk',
            'other_media',
            'media_type_details',
            'synchronized_with_text',
            'synchronized_with_audio',
            'synchronized_with_video',
            'synchronized_with_image',
            'synchronized_with_text_numerical',
        )
        extra_kwargs = {
            'media_type_details': {'style': {'max_length': 500, }},
        }

    def validate(self, data):
        self._errors = dict()
        for media in data.get('other_media', []):
            if media == choices.OTHER_MEDIA_CHOICES.AUDIO and \
                    data.get('synchronized_with_audio', None) is None:
                self._errors['synchronized_with_audio'] = _(
                    'This field may not be null')
            elif media == choices.OTHER_MEDIA_CHOICES.IMAGE and \
                    data.get('synchronized_with_image', None) is None:
                self._errors['synchronized_with_image'] = _(
                    'This field may not be null')
            elif media == choices.OTHER_MEDIA_CHOICES.TEXT and \
                    data.get('synchronized_with_text', None) is None:
                self._errors['synchronized_with_text'] = _(
                    'This field may not be null')
            elif media == choices.OTHER_MEDIA_CHOICES.TEXT_NUMERICAL and \
                    data.get('synchronized_with_text_numerical', None) is None:
                self._errors['synchronized_with_text_numerical'] = _(
                    'This field may not be null')
            elif media == choices.OTHER_MEDIA_CHOICES.VIDEO and \
                    data.get('synchronized_with_video', None) is None:
                self._errors['synchronized_with_video'] = _(
                    'This field may not be null')
        if self._errors:
            raise serializers.ValidationError(self._errors)

        return data


class CorpusTextPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The part of a corpus (or a whole corpus) that consists of
    textual segments (e.g., a corpus of publications, or
    transcriptions of an oral corpus, or subtitles, etc.)
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    text_type = TextTypeSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Text type'),
        help_text=_('Specifies the text type (e.g., factual, literary, etc.)'
                    ' according to which a text corpus (part) is classified'),
    )
    # ManyToMany relation
    text_genre = TextGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Text genre'),
        help_text=_('A category of text characterized by a particular style,'
                    ' form, or content according to a specific classification'
                    ' scheme'),
    )
    # Reverse FK relation
    annotation = AnnotationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )
    # ManyToMany relation
    is_created_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    creation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    # Reverse FK relation
    link_to_other_media = LinkToOtherMediaSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )

    class Meta:
        model = models.CorpusTextPart
        fields = (
            'pk',
            'corpus_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'modality_type',
            'text_type',
            'text_genre',
            'annotation',
            'creation_mode',
            'is_created_by',
            'has_original_source',
            'original_source_description',
            'synthetic_data',
            'creation_details',
            'link_to_other_media',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'text_type': {'style': {'recommended': True, }},
            'annotation': {'style': {'recommended': True, }},
            'original_source_description': {'style': {'max_length': 500, }},
            'creation_details': {'style': {'max_length': 3000, }},
        }
        to_display_false = [
            'corpus_media_type',
            'media_type',
        ]

    def validate(self, data):
        language_ids = [language['language_id'] for language in data['language']]
        contains_collective_lang = [(language in collective_language_ids) for language in language_ids]
        contains_collective_lang = True if True in contains_collective_lang else False
        if (len(data.get('language', None)) >= 2 or contains_collective_lang) \
                and not data.get('multilinguality_type', None):
            raise serializers.ValidationError(
                {'multilinguality_type': _(
                    'Multilinguality type cannot be blank')},
                code='invalid',
            )
        unique_language_validation(data, 'language')
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance


class AudioGenreIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify an audio genre according to a
    specific classification scheme
    """
    audio_genre_classification_scheme = serializers.ChoiceField(
        choices=choices.AUDIO_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        label=_('Audio genre classification scheme'),
        help_text=_('A classification scheme devised for audio genres by an'
                    ' organization/authority'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.AudioGenreIdentifier
        fields = (
            'pk',
            'audio_genre_classification_scheme',
            'value',
        )


class AudioGenreSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A classification of audio parts based on extra-linguistic and
    internal linguistic criteria and reflected on the audio style,
    form or content
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    audio_genre_identifier = AudioGenreIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Audio genre identifier'),
        help_text=_('A string used to uniquely identify an audio genre'
                    ' according to a specific classification scheme'),
    )

    class Meta:
        model = models.AudioGenre
        fields = (
            'pk',
            'category_label',
            'audio_genre_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve instance given all elements
        instance = retrieve_model_by_all_elements(self.Meta.model,
                                                  validated_data)
        if instance is not None:
            return instance
        # If no instance retrieve create a new one
        instance = super().create(validated_data)
        return instance


class SpeechGenreIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify a speech genre according to a
    specific classification scheme
    """
    speech_genre_classification_scheme = serializers.ChoiceField(
        choices=choices.SPEECH_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        label=_('Speech genre classification scheme'),
        help_text=_('A classification scheme used for speech genres devised by'
                    ' an authority or organization'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.SpeechGenreIdentifier
        fields = (
            'pk',
            'speech_genre_classification_scheme',
            'value',
        )


class SpeechGenreSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A category for the conventionalized discourse of the speech part
    of a language resource, based on extra-linguistic and internal
    linguistic criteria
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    speech_genre_identifier = SpeechGenreIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Speech genre identifier'),
        help_text=_('A string used to uniquely identify a speech genre'
                    ' according to a specific classification scheme'),
    )

    class Meta:
        model = models.SpeechGenre
        fields = (
            'pk',
            'category_label',
            'speech_genre_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve instance given all elements
        instance = retrieve_model_by_all_elements(self.Meta.model,
                                                  validated_data)
        if instance is not None:
            return instance
        # If no instance retrieve create a new one
        instance = super().create(validated_data)
        return instance


class CorpusAudioPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The part of a corpus (or whole corpus) that consists of audio
    segments
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    audio_genre = AudioGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Audio genre'),
        help_text=_('A classification of audio parts based on extra-linguistic'
                    ' and internal linguistic criteria and reflected on the'
                    ' audio style, form or content'),
    )
    # ManyToMany relation
    speech_genre = SpeechGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Speech genre'),
        help_text=_('A category for the conventionalized discourse of the'
                    ' speech part of a language resource, based on extra-'
                    ' linguistic and internal linguistic criteria'),
    )
    # Reverse FK relation
    annotation = AnnotationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )
    legend = SerializersMultilingualField(
        validators=[validate_dict_value_length(200)],
        allow_null=True,
        required=False,
        label=_('Legend'),
        help_text=_('Introduces the legend of the soundtrack'),
    )
    interaction = SerializersMultilingualField(
        validators=[validate_dict_value_length(200)],
        allow_null=True,
        required=False,
        label=_('Interaction'),
        help_text=_('Specifies the parts that interact in an audio or video'
                    ' component'),
    )
    recording_device_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Recording device type details'),
        help_text=_('Free text description of the recording device'),
    )
    source_channel_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Source channel name'),
        help_text=_('Introduces the name of the source channel through which'
                    ' the recording has been made'),
    )
    source_channel_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Source channel details'),
        help_text=_('Provides information on the details of the channel'
                    ' equipment used (brand, type, etc.) in free text'),
    )
    # ManyToMany relation
    recorder = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Recorder'),
        help_text=_('Links to the recorder(s) of the audio or video part of the'
                    ' resource'),
    )
    capturing_device_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Capturing device type details'),
        help_text=_('Provides further information on the capturing device'),
    )
    capturing_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Capturing details'),
        help_text=_('Provides further information on the capturing method and'
                    ' procedure'),
    )
    dialect_accent_of_participants = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Dialect accent of participants'),
        help_text=_('Provides information on the dialect accent of the group of'
                    ' participants'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    geographic_distribution_of_participants = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(200)]
        ),
        allow_null=True,
        required=False,
        label=_('Geographic distribution of participants'),
        help_text=_('Gives information on the geographic distribution of the'
                    ' participants'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    is_created_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    creation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    # Reverse FK relation
    link_to_other_media = LinkToOtherMediaSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )

    class Meta:
        model = models.CorpusAudioPart
        fields = (
            'pk',
            'corpus_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'modality_type',
            'audio_genre',
            'speech_genre',
            'annotation',
            'speech_item',
            'non_speech_item',
            'legend',
            'noise_level',
            'naturality',
            'conversational_type',
            'scenario_type',
            'audience',
            'interactivity',
            'interaction',
            'recording_device_type',
            'recording_device_type_details',
            'recording_platform_software',
            'recording_environment',
            'source_channel',
            'source_channel_type',
            'source_channel_name',
            'source_channel_details',
            'recorder',
            'capturing_device_type',
            'capturing_device_type_details',
            'capturing_details',
            'capturing_environment',
            'sensor_technology',
            'scene_illumination',
            'number_of_participants',
            'age_group_of_participants',
            'age_range_start_of_participants',
            'age_range_end_of_participants',
            'sex_of_participants',
            'origin_of_participants',
            'dialect_accent_of_participants',
            'geographic_distribution_of_participants',
            'hearing_impairment_of_participants',
            'speaking_impairment_of_participants',
            'number_of_trained_speakers',
            'speech_influence',
            'creation_mode',
            'is_created_by',
            'has_original_source',
            'original_source_description',
            'synthetic_data',
            'creation_details',
            'link_to_other_media',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'audio_genre': {'style': {'recommended': True, }},
            'speech_genre': {'style': {'recommended': True, }},
            'annotation': {'style': {'recommended': True, }},
            'legend': {'style': {'max_length': 200, }},
            'interaction': {'style': {'max_length': 200, }},
            'recording_device_type_details': {'style': {'max_length': 500, }},
            'source_channel_name': {'style': {'max_length': 100, }},
            'source_channel_details': {'style': {'max_length': 500, }},
            'capturing_device_type_details': {'style': {'max_length': 500, }},
            'capturing_details': {'style': {'max_length': 500, }},
            'number_of_participants': {'style': {'recommended': True, }},
            'dialect_accent_of_participants': {
                'style': {'max_length': 150, 'recommended': True, }},
            'geographic_distribution_of_participants': {
                'style': {'max_length': 200, 'recommended': True, }},
            'original_source_description': {'style': {'max_length': 500, }},
            'creation_details': {'style': {'max_length': 3000, }},
        }
        to_display_false = [
            'corpus_media_type',
            'media_type',
        ]

    def validate(self, data):
        language_ids = [language['language_id'] for language in data['language']]
        contains_collective_lang = [(language in collective_language_ids) for language in language_ids]
        contains_collective_lang = True if True in contains_collective_lang else False
        if (len(data.get('language', None)) >= 2 or contains_collective_lang) \
                and not data.get('multilinguality_type', None):
            raise serializers.ValidationError(
                {'multilinguality_type': _(
                    'Multilinguality type cannot be blank')},
                code='invalid',
            )
        unique_language_validation(data, 'language')
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance


class VideoGenreIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify a video genre according to a
    specific classification scheme
    """
    video_genre_classification_scheme = serializers.ChoiceField(
        choices=choices.VIDEO_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        label=_('Video genre classification scheme'),
        help_text=_('A classification scheme devised by an authority for video'
                    ' genres'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.VideoGenreIdentifier
        fields = (
            'pk',
            'video_genre_classification_scheme',
            'value',
        )


class VideoGenreSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A classification of video parts based on extra-linguistic and
    internal linguistic criteria and reflected on the video style,
    form or content
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    video_genre_identifier = VideoGenreIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Video genre identifier'),
        help_text=_('A string used to uniquely identify a video genre according'
                    ' to a specific classification scheme'),
    )

    class Meta:
        model = models.VideoGenre
        fields = (
            'pk',
            'category_label',
            'video_genre_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve instance given all elements
        instance = retrieve_model_by_all_elements(self.Meta.model,
                                                  validated_data)
        if instance is not None:
            return instance
        # If no instance retrieve create a new one
        instance = super().create(validated_data)
        return instance


class DynamicElementSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Groups information on the dynamic elements that are represented
    in the video part of the resource
    """
    type_of_element = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Type of element'),
        help_text=_('Specifies the type of objects or people represented in the'
                    ' video or image part of the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    distractor = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Distractor'),
        help_text=_('Identifies any distractors visible in the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    interactive_media = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(200)]
        ),
        allow_null=True,
        required=False,
        label=_('Interactive media'),
        help_text=_('Any interactive media visible in the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    face_view = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Face view'),
        help_text=_('Indicates the view of the face(s) that appear in the video'
                    ' or on the image part of the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    face_expression = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Face expression'),
        help_text=_('Indicates the facial expressions visible in the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    body_movement = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Body movement'),
        help_text=_('Indicates the body parts that move in the video part of'
                    ' the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    gesture = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(200)]
        ),
        allow_null=True,
        required=False,
        label=_('Gesture'),
        help_text=_('Indicates the type of gestures visible in the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    hand_arm_movement = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Hand arm movement'),
        help_text=_('Indicates the movement of hands and/or arms visible in the'
                    ' resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    hand_manipulation = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Hand manipulation'),
        help_text=_('Gives information on the manipulation of objects by hand'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    head_movement = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Head movement'),
        help_text=_('Indicates the movements of the head visible in the'
                    ' resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    eye_movement = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Eye movement'),
        help_text=_('Indicates the movement of the eyes visible in the'
                    ' resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )

    class Meta:
        model = models.DynamicElement
        fields = (
            'pk',
            'type_of_element',
            'body_part',
            'distractor',
            'interactive_media',
            'face_view',
            'face_expression',
            'body_movement',
            'gesture',
            'hand_arm_movement',
            'hand_manipulation',
            'head_movement',
            'eye_movement',
            'poses_per_subject',
        )
        extra_kwargs = {
            'type_of_element': {'style': {'max_length': 300, }},
            'distractor': {'style': {'max_length': 150, }},
            'interactive_media': {'style': {'max_length': 200, }},
            'face_view': {'style': {'max_length': 100, }},
            'face_expression': {'style': {'max_length': 150, }},
            'body_movement': {'style': {'max_length': 150, }},
            'gesture': {'style': {'max_length': 200, }},
            'hand_arm_movement': {'style': {'max_length': 150, }},
            'hand_manipulation': {'style': {'max_length': 100, }},
            'head_movement': {'style': {'max_length': 100, }},
            'eye_movement': {'style': {'max_length': 150, }},
        }


class CorpusVideoPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The part of a corpus (or whole corpus) that consists of video
    files (e.g., a corpus of film documentaries)
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    video_genre = VideoGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Video genre'),
        help_text=_('A classification of video parts based on extra-linguistic'
                    ' and internal linguistic criteria and reflected on the'
                    ' video style, form or content'),
    )
    # Reverse FK relation
    annotation = AnnotationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )
    type_of_video_content = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Type of video content'),
        help_text=_('Main type of object or people represented in the video'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    dynamic_element = DynamicElementSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Dynamic element'),
        help_text=_('Groups information on the dynamic elements that are'
                    ' represented in the video part of the resource'),
    )
    interaction = SerializersMultilingualField(
        validators=[validate_dict_value_length(200)],
        allow_null=True,
        required=False,
        label=_('Interaction'),
        help_text=_('Specifies the parts that interact in an audio or video'
                    ' component'),
    )
    recording_device_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Recording device type details'),
        help_text=_('Free text description of the recording device'),
    )
    source_channel_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Source channel name'),
        help_text=_('Introduces the name of the source channel through which'
                    ' the recording has been made'),
    )
    source_channel_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Source channel details'),
        help_text=_('Provides information on the details of the channel'
                    ' equipment used (brand, type, etc.) in free text'),
    )
    # ManyToMany relation
    recorder = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Recorder'),
        help_text=_('Links to the recorder(s) of the audio or video part of the'
                    ' resource'),
    )
    capturing_device_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Capturing device type details'),
        help_text=_('Provides further information on the capturing device'),
    )
    capturing_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Capturing details'),
        help_text=_('Provides further information on the capturing method and'
                    ' procedure'),
    )
    dialect_accent_of_participants = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Dialect accent of participants'),
        help_text=_('Provides information on the dialect accent of the group of'
                    ' participants'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    geographic_distribution_of_participants = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(200)]
        ),
        allow_null=True,
        required=False,
        label=_('Geographic distribution of participants'),
        help_text=_('Gives information on the geographic distribution of the'
                    ' participants'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    is_created_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    creation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    # Reverse FK relation
    link_to_other_media = LinkToOtherMediaSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )

    class Meta:
        model = models.CorpusVideoPart
        fields = (
            'pk',
            'corpus_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'modality_type',
            'video_genre',
            'annotation',
            'type_of_video_content',
            'text_included_in_video',
            'dynamic_element',
            'naturality',
            'conversational_type',
            'scenario_type',
            'audience',
            'interactivity',
            'interaction',
            'recording_device_type',
            'recording_device_type_details',
            'recording_platform_software',
            'recording_environment',
            'source_channel',
            'source_channel_type',
            'source_channel_name',
            'source_channel_details',
            'recorder',
            'capturing_device_type',
            'capturing_device_type_details',
            'capturing_details',
            'capturing_environment',
            'sensor_technology',
            'scene_illumination',
            'number_of_participants',
            'age_group_of_participants',
            'age_range_start_of_participants',
            'age_range_end_of_participants',
            'sex_of_participants',
            'origin_of_participants',
            'dialect_accent_of_participants',
            'geographic_distribution_of_participants',
            'hearing_impairment_of_participants',
            'speaking_impairment_of_participants',
            'number_of_trained_speakers',
            'speech_influence',
            'creation_mode',
            'is_created_by',
            'has_original_source',
            'original_source_description',
            'synthetic_data',
            'creation_details',
            'link_to_other_media',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'video_genre': {'style': {'recommended': True, }},
            'annotation': {'style': {'recommended': True, }},
            'type_of_video_content': {'style': {'max_length': 300, }},
            'interaction': {'style': {'max_length': 200, }},
            'recording_device_type_details': {'style': {'max_length': 500, }},
            'source_channel_name': {'style': {'max_length': 100, }},
            'source_channel_details': {'style': {'max_length': 500, }},
            'capturing_device_type_details': {'style': {'max_length': 500, }},
            'capturing_details': {'style': {'max_length': 500, }},
            'number_of_participants': {'style': {'recommended': True, }},
            'dialect_accent_of_participants': {
                'style': {'max_length': 150, 'recommended': True, }},
            'geographic_distribution_of_participants': {
                'style': {'max_length': 200, 'recommended': True, }},
            'original_source_description': {'style': {'max_length': 500, }},
            'creation_details': {'style': {'max_length': 3000, }},
        }
        to_display_false = [
            'corpus_media_type',
            'media_type',
        ]

    def validate(self, data):
        language_ids = [language['language_id'] for language in data['language']]
        contains_collective_lang = [(language in collective_language_ids) for language in language_ids]
        contains_collective_lang = True if True in contains_collective_lang else False
        if (len(data.get('language', None)) >= 2 or contains_collective_lang) \
                and not data.get('multilinguality_type', None):
            raise serializers.ValidationError(
                {'multilinguality_type': _(
                    'Multilinguality type cannot be blank')},
                code='invalid',
            )
        unique_language_validation(data, 'language')
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance


class ImageGenreIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify an image genre according to a
    specific classification scheme
    """
    image_genre_classification_scheme = serializers.ChoiceField(
        choices=choices.IMAGE_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        label=_('Image genre classification scheme'),
        help_text=_('A classification scheme devised by an authority for image'
                    ' genres'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.ImageGenreIdentifier
        fields = (
            'pk',
            'image_genre_classification_scheme',
            'value',
        )


class ImageGenreSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A category of images characterized by a particular style, form,
    or content according to a specific classification scheme
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    image_genre_identifier = ImageGenreIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Image genre identifier'),
        help_text=_('A string used to uniquely identify an image genre'
                    ' according to a specific classification scheme'),
    )

    class Meta:
        model = models.ImageGenre
        fields = (
            'pk',
            'category_label',
            'image_genre_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve instance given all elements
        instance = retrieve_model_by_all_elements(self.Meta.model,
                                                  validated_data)
        if instance is not None:
            return instance
        # If no instance retrieve create a new one
        instance = super().create(validated_data)
        return instance


class StaticElementSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Groups information on the static elements visible on images
    """
    type_of_element = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Type of element'),
        help_text=_('Specifies the type of objects or people represented in the'
                    ' video or image part of the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    face_view = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Face view'),
        help_text=_('Indicates the view of the face(s) that appear in the video'
                    ' or on the image part of the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    face_expression = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Face expression'),
        help_text=_('Indicates the facial expressions visible in the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    artifact_part = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Artifact part'),
        help_text=_('Indicates the parts of the artifacts represented in the'
                    ' image corpus'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    landscape_part = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Landscape part'),
        help_text=_('Indicates the landscape parts represented in the image'
                    ' corpus'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    person_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(150)],
        allow_null=True,
        required=False,
        label=_('Person description'),
        help_text=_('Provides descriptive features for the persons represented'
                    ' in the image corpus'),
    )
    thing_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(150)],
        allow_null=True,
        required=False,
        label=_('Thing description'),
        help_text=_('Provides description of the things represented in the'
                    ' image corpus'),
    )
    organization_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(10000)],
        allow_null=True,
        required=False,
        label=_('Organization description'),
        help_text=_('Provides description of the organizations that may appear'
                    ' in the image corpus'),
    )
    event_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(150)],
        allow_null=True,
        required=False,
        label=_('Event description'),
        help_text=_('Provides description of any events represented in the'
                    ' image corpus'),
    )

    class Meta:
        model = models.StaticElement
        fields = (
            'pk',
            'type_of_element',
            'body_part',
            'face_view',
            'face_expression',
            'artifact_part',
            'landscape_part',
            'person_description',
            'thing_description',
            'organization_description',
            'event_description',
        )
        extra_kwargs = {
            'type_of_element': {'style': {'max_length': 300, }},
            'face_view': {'style': {'max_length': 100, }},
            'face_expression': {'style': {'max_length': 150, }},
            'artifact_part': {'style': {'max_length': 150, }},
            'landscape_part': {'style': {'max_length': 100, }},
            'person_description': {'style': {'max_length': 150, }},
            'thing_description': {'style': {'max_length': 150, }},
            'organization_description': {'style': {'max_length': 10000, }},
            'event_description': {'style': {'max_length': 150, }},
        }


class CorpusImagePartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The part of a corpus (or whole corpus) that consists of images
    (e.g., g a corpus of photographs and their captions)
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    image_genre = ImageGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Image genre'),
        help_text=_('A category of images characterized by a particular style,'
                    ' form, or content according to a specific classification'
                    ' scheme'),
    )
    # Reverse FK relation
    annotation = AnnotationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )
    type_of_image_content = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Type of image content'),
        help_text=_('The main types of object or people represented in the'
                    ' image corpus'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    static_element = StaticElementSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Static element'),
        help_text=_('Groups information on the static elements visible on'
                    ' images'),
    )
    capturing_device_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Capturing device type details'),
        help_text=_('Provides further information on the capturing device'),
    )
    capturing_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Capturing details'),
        help_text=_('Provides further information on the capturing method and'
                    ' procedure'),
    )
    # ManyToMany relation
    is_created_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    creation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    # Reverse FK relation
    link_to_other_media = LinkToOtherMediaSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )

    class Meta:
        model = models.CorpusImagePart
        fields = (
            'pk',
            'corpus_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'modality_type',
            'image_genre',
            'annotation',
            'type_of_image_content',
            'text_included_in_image',
            'static_element',
            'capturing_device_type',
            'capturing_device_type_details',
            'capturing_details',
            'capturing_environment',
            'sensor_technology',
            'scene_illumination',
            'creation_mode',
            'is_created_by',
            'has_original_source',
            'original_source_description',
            'synthetic_data',
            'creation_details',
            'link_to_other_media',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'language': {'style': {'recommended': True, }},
            'image_genre': {'style': {'recommended': True, }},
            'annotation': {'style': {'recommended': True, }},
            'type_of_image_content': {'style': {'max_length': 500, }},
            'capturing_device_type_details': {'style': {'max_length': 500, }},
            'capturing_details': {'style': {'max_length': 500, }},
            'original_source_description': {'style': {'max_length': 500, }},
            'creation_details': {'style': {'max_length': 3000, }},
        }
        to_display_false = [
            'corpus_media_type',
            'media_type',
        ]

    def validate(self, data):
        language_ids = [language['language_id'] for language in data['language']]
        contains_collective_lang = [(language in collective_language_ids) for language in language_ids]
        contains_collective_lang = True if True in contains_collective_lang else False
        if (len(data.get('language', None)) >= 2 or contains_collective_lang) \
                and not data.get('multilinguality_type', None):
            raise serializers.ValidationError(
                {'multilinguality_type': _(
                    'Multilinguality type cannot be blank')},
                code='invalid',
            )
        unique_language_validation(data, 'language')
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance


class CorpusTextNumericalPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The part of a corpus (or whole corpus) that consists of sets of
    textual representations of measurements and observations linked
    to sensorimotor recordings
    """
    # Reverse FK relation
    annotation = AnnotationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )
    type_of_text_numerical_content = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Type of textNumerical content'),
        help_text=_('Specifies the content that is represented in the'
                    ' textNumerical part of the resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    recording_device_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Recording device type details'),
        help_text=_('Free text description of the recording device'),
    )
    source_channel_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Source channel name'),
        help_text=_('Introduces the name of the source channel through which'
                    ' the recording has been made'),
    )
    source_channel_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Source channel details'),
        help_text=_('Provides information on the details of the channel'
                    ' equipment used (brand, type, etc.) in free text'),
    )
    # ManyToMany relation
    recorder = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Recorder'),
        help_text=_('Links to the recorder(s) of the audio or video part of the'
                    ' resource'),
    )
    capturing_device_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Capturing device type details'),
        help_text=_('Provides further information on the capturing device'),
    )
    capturing_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Capturing details'),
        help_text=_('Provides further information on the capturing method and'
                    ' procedure'),
    )
    dialect_accent_of_participants = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(150)]
        ),
        allow_null=True,
        required=False,
        label=_('Dialect accent of participants'),
        help_text=_('Provides information on the dialect accent of the group of'
                    ' participants'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    geographic_distribution_of_participants = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(200)]
        ),
        allow_null=True,
        required=False,
        label=_('Geographic distribution of participants'),
        help_text=_('Gives information on the geographic distribution of the'
                    ' participants'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    is_created_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    creation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    # Reverse FK relation
    link_to_other_media = LinkToOtherMediaSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )

    class Meta:
        model = models.CorpusTextNumericalPart
        fields = (
            'pk',
            'corpus_media_type',
            'media_type',
            'modality_type',
            'annotation',
            'type_of_text_numerical_content',
            'recording_device_type',
            'recording_device_type_details',
            'recording_platform_software',
            'recording_environment',
            'source_channel',
            'source_channel_type',
            'source_channel_name',
            'source_channel_details',
            'recorder',
            'capturing_device_type',
            'capturing_device_type_details',
            'capturing_details',
            'capturing_environment',
            'sensor_technology',
            'scene_illumination',
            'number_of_participants',
            'age_group_of_participants',
            'age_range_start_of_participants',
            'age_range_end_of_participants',
            'sex_of_participants',
            'origin_of_participants',
            'dialect_accent_of_participants',
            'geographic_distribution_of_participants',
            'hearing_impairment_of_participants',
            'speaking_impairment_of_participants',
            'number_of_trained_speakers',
            'speech_influence',
            'creation_mode',
            'is_created_by',
            'has_original_source',
            'original_source_description',
            'synthetic_data',
            'creation_details',
            'link_to_other_media',
        )
        extra_kwargs = {
            'annotation': {'style': {'recommended': True, }},
            'type_of_text_numerical_content': {'style': {'max_length': 500, }},
            'recording_device_type_details': {'style': {'max_length': 500, }},
            'source_channel_name': {'style': {'max_length': 100, }},
            'source_channel_details': {'style': {'max_length': 500, }},
            'capturing_device_type_details': {'style': {'max_length': 500, }},
            'capturing_details': {'style': {'max_length': 500, }},
            'number_of_participants': {'style': {'recommended': True, }},
            'dialect_accent_of_participants': {
                'style': {'max_length': 150, 'recommended': True, }},
            'geographic_distribution_of_participants': {
                'style': {'max_length': 200, 'recommended': True, }},
            'original_source_description': {'style': {'max_length': 500, }},
            'creation_details': {'style': {'max_length': 3000, }},
        }
        to_display_false = [
            'corpus_media_type',
            'media_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance


class CorpusMediaPartSerializer(
    PolymorphicSerializer, WritableNestedModelSerializer,
    XMLPolymorphicSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A part/segment of a corpus based on its media type
    classification
    """

    class Meta:
        model = models.CorpusMediaPart
        fields = '__all__'

    model_serializer_mapping = {
        models.CorpusTextPart: CorpusTextPartSerializer,
        models.CorpusAudioPart: CorpusAudioPartSerializer,
        models.CorpusVideoPart: CorpusVideoPartSerializer,
        models.CorpusImagePart: CorpusImagePartSerializer,
        models.CorpusTextNumericalPart: CorpusTextNumericalPartSerializer
    }

    resource_type_field_name = 'corpus_media_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _(
                    'Corpus media type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]

    def _get_serializer_from_resource_type(self, resource_type):
        try:
            model = self.resource_type_model_mapping[resource_type]
        except KeyError:
            if self.get_draft():
                raise serializers.ValidationError({
                    self.resource_type_field_name: '{0} cannot be blank'.format(
                        self.resource_type_field_name
                    )
                })
            raise serializers.ValidationError({
                self.resource_type_field_name: 'Invalid {0}'.format(
                    self.resource_type_field_name
                )
            })

        return self._get_serializer_from_model_or_instance(model)


class SizeSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Specifies the size of a countable entity with regard to the
    SizeUnit measurement in form of a number
    """
    amount = serializers.FloatField(
        label=_('Amount'),
        help_text=_('Specifies the number of units that constitute anything'
                    ' that can be measured (e.g. size of a data resource or'
                    ' cost, etc.)'),
    )
    size_unit = serializers.CharField(
        trim_whitespace=True,
        max_length=200,
        label=_('Size unit'),
        help_text=_('Specifies the unit that is used when providing information'
                    ' on the size of the resource or of resource parts'),
    )
    size_text = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Size (free text)'),
        help_text=_('Specifies the size of a resource in the form of a free'
                    ' text'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    language = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    domain = DomainSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    # ManyToMany relation
    text_genre = TextGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Text genre'),
        help_text=_('A category of text characterized by a particular style,'
                    ' form, or content according to a specific classification'
                    ' scheme'),
    )
    # ManyToMany relation
    audio_genre = AudioGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Audio genre'),
        help_text=_('A classification of audio parts based on extra-linguistic'
                    ' and internal linguistic criteria and reflected on the'
                    ' audio style, form or content'),
    )
    # ManyToMany relation
    speech_genre = SpeechGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Speech genre'),
        help_text=_('A category for the conventionalized discourse of the'
                    ' speech part of a language resource, based on extra-'
                    ' linguistic and internal linguistic criteria'),
    )
    # ManyToMany relation
    video_genre = VideoGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Video genre'),
        help_text=_('A classification of video parts based on extra-linguistic'
                    ' and internal linguistic criteria and reflected on the'
                    ' video style, form or content'),
    )
    # ManyToMany relation
    image_genre = ImageGenreSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Image genre'),
        help_text=_('A category of images characterized by a particular style,'
                    ' form, or content according to a specific classification'
                    ' scheme'),
    )

    class Meta:
        model = models.Size
        fields = (
            'pk',
            'amount',
            'size_unit',
            'size_text',
            'language',
            'domain',
            'text_genre',
            'audio_genre',
            'speech_genre',
            'video_genre',
            'image_genre',
            'computed',
        )
        extra_kwargs = {
            'size_text': {'style': {'max_length': 100, }},
        }

    def validate(self, data):
        for_information_only = self.context.get('for_information_only', False)
        if for_information_only is False and data.get('size_text', None) is not None:
            raise serializers.ValidationError(
                {'size_text': _(f"{self.fields['size_text'].label} can be used only for infomation only records.")},
                code='invalid',
            )
        unique_language_validation(data, 'language')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        size_unit_xml_field_name = self.Meta.model.schema_fields['size_unit']
        if representation.get(size_unit_xml_field_name, None):
            representation[size_unit_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[size_unit_xml_field_name], choices.SIZE_UNIT_RECOMMENDED_CHOICES,
                'ms:sizeUnitRecommended', 'ms:sizeUnitOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['size_unit'] = super().from_xml_representation_for_recommended_cv(ret['size_unit'],
                                                                              'size_unit',
                                                                              choices.SIZE_UNIT_RECOMMENDED_CHOICES,
                                                                              'ms:sizeUnitRecommended',
                                                                              'ms:sizeUnitOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['size_unit'] = super().to_display_representation_for_recommended_cv(
            ret['size_unit'], choices.SIZE_UNIT_RECOMMENDED_CHOICES)
        return ret


class DistributionTextFeatureSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of text resources/parts
    """
    # Reverse FK relation
    size = SizeSerializer(
        many=True,
        allow_empty=False,
        label=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    data_format = serializers.ListField(
        child=serializers.CharField(
            max_length=200),
        allow_empty=False,
        label=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )

    class Meta:
        model = models.DistributionTextFeature
        fields = (
            'pk',
            'size',
            'data_format',
            'mimetype',
            'character_encoding',
        )
        extra_kwargs = {
            'character_encoding': {'style': {'recommended': True, }},
        }

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'mimetype')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class DistributionTextNumericalFeatureSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of numerical text resources/parts
    """
    # Reverse FK relation
    size = SizeSerializer(
        many=True,
        allow_empty=False,
        label=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    data_format = serializers.ListField(
        child=serializers.CharField(
            max_length=200),
        allow_empty=False,
        label=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )

    class Meta:
        model = models.DistributionTextNumericalFeature
        fields = (
            'pk',
            'size',
            'data_format',
            'mimetype',
        )

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'mimetype')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class DurationSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    """
    amount = serializers.FloatField(
        label=_('Amount'),
        help_text=_('Specifies the number of units that constitute anything'
                    ' that can be measured (e.g. size of a data resource or'
                    ' cost, etc.)'),
    )
    duration_unit = serializers.ChoiceField(
        choices=choices.DURATION_UNIT_CHOICES,
        label=_('Duration unit'),
        help_text=_('Specifies the unit used for measuring the duration of a'
                    ' resource'),
    )

    class Meta:
        model = models.Duration
        fields = (
            'pk',
            'amount',
            'duration_unit',
        )


class AudioFormatSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Specifies the features of the format of the audio part of a
    language resource
    """
    data_format = serializers.CharField(
        trim_whitespace=True,
        max_length=200,
        label=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    compressed = serializers.BooleanField(
        label=_('Compressed'),
        help_text=_('Whether the audio, video or image is compressed or not'),
    )

    class Meta:
        model = models.AudioFormat
        fields = (
            'pk',
            'data_format',
            'signal_encoding',
            'sampling_rate',
            'quantization',
            'byte_order',
            'sign_convention',
            'audio_quality_measure_included',
            'number_of_tracks',
            'recording_quality',
            'compressed',
            'compression_name',
            'compression_loss',
        )
        extra_kwargs = {
            'byte_order': {'style': {'recommended': True, }},
        }

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class DistributionAudioFeatureSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of audio resources/parts
    """
    # Reverse FK relation
    size = SizeSerializer(
        many=True,
        allow_empty=False,
        label=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    # Reverse FK relation
    duration_of_audio = DurationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Duration of audio'),
        help_text=_('Specifies the duration of the audio recording including'
                    ' silences, music, pauses, etc.'),
    )
    # Reverse FK relation
    duration_of_effective_speech = DurationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Duration of effective speech'),
        help_text=_('Specifies the duration of effective speech of the audio'
                    ' (part of a) resource'),
    )
    data_format = serializers.ListField(
        child=serializers.CharField(
            max_length=200),
        allow_empty=False,
        label=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    # Reverse FK relation
    audio_format = AudioFormatSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Detailed audio format'),
        help_text=_('Specifies the features of the format of the audio part of'
                    ' a language resource'),
    )

    class Meta:
        model = models.DistributionAudioFeature
        fields = (
            'pk',
            'size',
            'duration_of_audio',
            'duration_of_effective_speech',
            'data_format',
            'audio_format',
            'mimetype',
        )
        extra_kwargs = {
            'duration_of_audio': {'style': {'recommended': True, }},
        }

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'mimetype')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class ResolutionSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Groups together information on the image resolution
    """
    size_width = serializers.IntegerField(
        label=_('Size width'),
        help_text=_('The frame width in pixels'),
    )
    size_height = serializers.IntegerField(
        label=_('Size height'),
        help_text=_('The frame height in pixels'),
    )
    resolution_standard = serializers.ChoiceField(
        choices=choices.RESOLUTION_STANDARD_CHOICES,
        label=_('Resolution standard'),
        help_text=_('Specifies the standard to which the resolution conforms'),
    )

    class Meta:
        model = models.Resolution
        fields = (
            'pk',
            'size_width',
            'size_height',
            'resolution_standard',
        )


class ImageFormatSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Provides information on each of the format(s) of the image part
    of a resource
    """
    data_format = serializers.CharField(
        trim_whitespace=True,
        max_length=200,
        label=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    # ManyToMany relation
    resolution = ResolutionSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Resolution'),
        help_text=_('Groups together information on the image resolution'),
    )
    compressed = serializers.BooleanField(
        label=_('Compressed'),
        help_text=_('Whether the audio, video or image is compressed or not'),
    )

    class Meta:
        model = models.ImageFormat
        fields = (
            'pk',
            'data_format',
            'colour_space',
            'colour_depth',
            'resolution',
            'visual_modelling',
            'compressed',
            'compression_name',
            'compression_loss',
            'raster_or_vector_graphics',
            'quality',
        )

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class DistributionImageFeatureSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of image resources/parts
    """
    # Reverse FK relation
    size = SizeSerializer(
        many=True,
        allow_empty=False,
        label=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    data_format = serializers.ListField(
        child=serializers.CharField(
            max_length=200),
        allow_empty=False,
        label=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    # Reverse FK relation
    image_format = ImageFormatSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Detailed image format'),
        help_text=_('Provides information on each of the format(s) of the image'
                    ' part of a resource'),
    )

    class Meta:
        model = models.DistributionImageFeature
        fields = (
            'pk',
            'size',
            'data_format',
            'image_format',
            'mimetype',
        )

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'mimetype')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class VideoFormatSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Groups information on the format(s) of a resource; repeated if
    parts of the resource are in different formats
    """
    data_format = serializers.CharField(
        trim_whitespace=True,
        max_length=200,
        label=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    # ManyToMany relation
    resolution = ResolutionSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Resolution'),
        help_text=_('Groups together information on the image resolution'),
    )
    compressed = serializers.BooleanField(
        label=_('Compressed'),
        help_text=_('Whether the audio, video or image is compressed or not'),
    )

    class Meta:
        model = models.VideoFormat
        fields = (
            'pk',
            'data_format',
            'colour_space',
            'colour_depth',
            'frame_rate',
            'resolution',
            'visual_modelling',
            'fidelity',
            'compressed',
            'compression_name',
            'compression_loss',
        )

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class DistributionVideoFeatureSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of video resources/parts
    """
    # Reverse FK relation
    size = SizeSerializer(
        many=True,
        allow_empty=False,
        label=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    data_format = serializers.ListField(
        child=serializers.CharField(
            max_length=200),
        allow_empty=False,
        label=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    # Reverse FK relation
    video_format = VideoFormatSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Detailed video format'),
        help_text=_('Groups information on the format(s) of a resource;'
                    ' repeated if parts of the resource are in different'
                    ' formats'),
    )

    class Meta:
        model = models.DistributionVideoFeature
        fields = (
            'pk',
            'size',
            'data_format',
            'video_format',
            'mimetype',
        )

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'mimetype')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class distributionUnspecifiedFeatureSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    """
    # Reverse FK relation
    size = SizeSerializer(
        many=True,
        allow_empty=False,
        label=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    data_format = serializers.ListField(
        child=serializers.CharField(max_length=200),
        allow_empty=False,
        label=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )

    class Meta:
        model = models.distributionUnspecifiedFeature
        fields = (
            'pk',
            'size',
            'data_format',
            'mimetype',
        )

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'mimetype')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class LicenceIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify a licence
    """
    licence_identifier_scheme = serializers.ChoiceField(
        choices=choices.LICENCE_IDENTIFIER_SCHEME_CHOICES,
        label=_('Licence identifier scheme'),
        help_text=_('The name of the scheme according to which an identifier is'
                    ' assigned to a licence by the authority that issues it'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.LicenceIdentifier
        fields = (
            'pk',
            'licence_identifier_scheme',
            'value',
        )

    def update(self, instance, validated_data):
        if instance.licence_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return instance
        return super().update(instance, validated_data)


class GenericLicenceTermsSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplayGenericSerializer, DraftSerializer, RelatedSerializer):
    """
    """
    licence_terms_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(200), validate_dict_existence],
        label=_('Licence/terms of use/service name'),
        help_text=_('The name by which a legal document (e.g., licence, terms'
                    ' of use, terms of service) is known'),
    )
    licence_terms_url = serializers.ListField(
        child=serializers.URLField(),
        allow_empty=False,
        label=_('Licence/terms of use/service URL'),
        help_text=_('Links to the URL where the text of a licence/terms of'
                    ' use/service is found'),
    )
    condition_of_use = serializers.ListField(
        child=serializers.ChoiceField(
            choices=choices.CONDITION_OF_USE_CHOICES),
        allow_empty=False,
        label=_('Condition of use'),
        help_text=_('Links a licence with a specific condition/term of use'
                    ' imposed for accessing a language resource. It is an'
                    ' optional element and only to be taken as providing brief'
                    ' human readable information on the fact that the language'
                    ' resource is provided under a specific set of conditions.'
                    ' These correspond to the most frequently used conditions'
                    ' imposed by the licensor (via the specified licence). The'
                    ' proper exposition of all conditions and possible'
                    ' exceptions is to be found inside the licence text.'
                    ' Depositors should, hence, carefully choose the values of'
                    ' this field to match the licence chosen and users should'
                    ' carefully read that licence before using the language'
                    ' resource.'),
    )
    # Reverse FK relation
    licence_identifier = LicenceIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Licence identifier'),
        help_text=_('A string used to uniquely identify a licence'),
    )

    class Meta:
        model = models.LicenceTerms
        fields = (
            'pk',
            'licence_terms_name',
            'licence_terms_url',
            'condition_of_use',
            'licence_category',
            'licence_identifier',
        )
        extra_kwargs = {
            'licence_terms_name': {'style': {'max_length': 200, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if registry identifier exists
        instance = retrieve_by_registry_identifier(self.Meta.model,
                                                   validated_data,
                                                   request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if non-registry identifier exists
        instance = retrieve_by_non_registry_identifier(self.Meta.model,
                                                       validated_data,
                                                       request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if both licence_terms_url and licence_terms_name exists
        instance = retrieve_licence_by_licence_terms_and_name(validated_data, request_user)
        if instance is not None:
            return instance
        # Retrieve appropriate instance if only licence_terms_name exists
        instance = retrieve_licence_terms_given_only_name(validated_data, request_user)
        if instance is not None:
            return instance
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'licence_identifier',
                                                    'licence_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'licence_identifier',
                                                       'licence_identifier_scheme')
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        # ignore update for generic serializers
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'licence_terms_url')
        return super().to_internal_value(data)

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='licence_identifier',
                                            identifier_scheme='licence_identifier_scheme')
        unspecified_in_condition_of_use_validation(data, 'condition_of_use')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['licence_identifier'],
                                                    LicenceIdentifierSerializer.Meta.model.schema_fields[
                                                        'licence_identifier_scheme'])
        return representation


class CostSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Introduces the cost for accessing a resource or the overall
    budget of a project, formally described as a set of amount and
    currency unit
    """
    amount = serializers.FloatField(
        label=_('Amount'),
        help_text=_('Specifies the number of units that constitute anything'
                    ' that can be measured (e.g. size of a data resource or'
                    ' cost, etc.)'),
    )
    currency = serializers.ChoiceField(
        choices=choices.CURRENCY_CHOICES,
        label=_('Currency'),
        help_text=_('Specifies the currency used for describing the cost of a'
                    ' resource'),
    )

    class Meta:
        model = models.Cost
        fields = (
            'pk',
            'amount',
            'currency',
        )


class AccessRightsStatementIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A string used to uniquely identify an access rights statement
    according to a specific scheme
    """
    access_rights_statement_scheme = serializers.ChoiceField(
        choices=choices.ACCESS_RIGHTS_STATEMENT_SCHEME_CHOICES,
        label=_('Access rights statement scheme'),
        help_text=_('The name of the scheme according to which an access rights'
                    ' statement is assigned to a distribution'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.AccessRightsStatementIdentifier
        fields = (
            'pk',
            'access_rights_statement_scheme',
            'value',
        )


class AccessRightsStatementSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    """
    category_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    # OneToOne relation
    access_rights_statement_identifier = AccessRightsStatementIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('AccessRightsStatement identifier'),
        help_text=_('A string used to uniquely identify an access rights'
                    ' statement according to a specific scheme'),
    )

    class Meta:
        model = models.AccessRightsStatement
        fields = (
            'pk',
            'category_label',
            'access_rights_statement_identifier',
        )
        extra_kwargs = {
            'category_label': {'style': {'max_length': 150, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Retrieve appropriate instance if pk exists
        instance = retrieve_model_by_pk(self.Meta.model,
                                        self.initial_data,
                                        request_user)
        if instance is not None:
            return instance
        # Retrieve instance given all elements
        instance = retrieve_model_by_all_elements(self.Meta.model,
                                                  validated_data)
        if instance is not None:
            return instance
        # If no instance retrieve create a new one
        instance = super().create(validated_data)
        return instance


class DatasetDistributionSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Any form with which a dataset is distributed, such as a
    downloadable form in a specific format (e.g., spreadsheet, plain
    text, etc.) or an API with which it can be accessed
    """
    dataset_distribution_form = serializers.ChoiceField(
        choices=choices.DATASET_DISTRIBUTION_FORM_CHOICES,
        label=_('Dataset distribution form'),
        help_text=_('The form (medium/channel) used for distributing a language'
                    ' resource consisting of data (e.g., a corpus, a lexicon,'
                    ' etc.)'),
    )
    # ManyToMany relation
    is_accessed_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Access tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' accessing LR A (the one being described), e.g., a corpus'
                    ' workbench, s/w for lexicon access'),
    )
    # ManyToMany relation
    is_displayed_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Visualization tool'),
        help_text=_('Links to a tool/service B that is (or can be) used to'
                    ' display LR A (the one being described), e.g., a tool for'
                    ' visualizing relations in a lexicon, or annotations in a'
                    ' corpus'),
    )
    # ManyToMany relation
    is_queried_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Query tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' querying LR A (the one being described)'),
    )
    # Reverse FK relation
    distribution_text_feature = DistributionTextFeatureSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Text feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of text resources/parts'),
    )
    # Reverse FK relation
    distribution_text_numerical_feature = DistributionTextNumericalFeatureSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Numerical text feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of numerical text'
                    ' resources/parts'),
    )
    # Reverse FK relation
    distribution_audio_feature = DistributionAudioFeatureSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Audio feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of audio resources/parts'),
    )
    # Reverse FK relation
    distribution_image_feature = DistributionImageFeatureSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Image feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of image resources/parts'),
    )
    # Reverse FK relation
    distribution_video_feature = DistributionVideoFeatureSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Video feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of video resources/parts'),
    )
    # OneToOne relation
    distribution_unspecified_feature = distributionUnspecifiedFeatureSerializer(
        allow_null=True,
        required=False,
        label=_('Features'),
        help_text=''
    )
    # ManyToMany relation
    licence_terms = GenericLicenceTermsSerializer(
        many=True,
        allow_empty=True,
        allow_null=False,
        label=_('Licence'),
        help_text=_('Links the distribution (distributable form) of a language'
                    ' resource to the licence or terms of use/service (a'
                    ' specific legal document) with which it is distributed'),
    )
    attribution_text = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000)],
        allow_null=True,
        required=False,
        label=_('Attribution text'),
        help_text=_('The text that must be quoted for attribution purposes when'
                    ' using a resource - for cases where a resource is provided'
                    ' with a restriction on attribution'),
        read_only=True,
    )
    # OneToOne relation
    cost = CostSerializer(
        allow_null=True,
        required=False,
        label=_('Cost'),
        help_text=_('Introduces the cost for accessing a resource or the'
                    ' overall budget of a project, formally described as a set'
                    ' of amount and currency unit'),
    )
    copyright_statement = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Copyright notice'),
        help_text=_('Introduces a free text statement that may be included with'
                    ' the language resource, usually containing the name(s) of'
                    ' copyright holders and licensing terms'),
    )
    # ManyToMany relation
    distribution_rights_holder = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Distribution rights holder'),
        help_text=_('Identifies a person or an organization that holds the'
                    ' distribution rights. The range and scope of distribution'
                    ' rights is defined in the distribution agreement. The'
                    ' distributor in most cases only has a limited licence to'
                    ' distribute the work and collect royalties on behalf of'
                    ' the licensor or the IPR holder and cannot give to any'
                    ' recipient of the work permissions that exceed the scope'
                    ' of the distribution agreement (e.g., to allow uses of the'
                    ' work that are not defined in the distribution agreement)'),
    )
    # ManyToMany relation
    access_rights = AccessRightsStatementSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Access rights'),
        help_text=_('Specifies the rights for accessing the distributable'
                    ' form(s) of a language resource (preferrably in accordance'
                    ' to a formalised vocabulary)'),
    )

    # FK relation
    dataset = ContentFileSerializer(
        allow_null=True,
        required=False
    )

    class Meta:
        model = models.DatasetDistribution
        fields = (
            'pk',
            'dataset_distribution_form',
            'download_location',
            'access_location',
            'private_resource',
            'is_accessed_by',
            'is_displayed_by',
            'is_queried_by',
            'samples_location',
            'package_format',
            'distribution_text_feature',
            'distribution_text_numerical_feature',
            'distribution_audio_feature',
            'distribution_image_feature',
            'distribution_video_feature',
            'distribution_unspecified_feature',
            'licence_terms',
            'attribution_text',
            'cost',
            'membership_institution',
            'copyright_statement',
            'availability_start_date',
            'availability_end_date',
            'distribution_rights_holder',
            'access_rights',
            'dataset'
        )
        extra_kwargs = {
            'private_resource': {'style': {'recommended': True, }},
            'samples_location': {'style': {'recommended': True, }},
            'attribution_text': {'style': {'max_length': 1000, }},
            'cost': {'style': {'recommended': True, }},
            'membership_institution': {'style': {'recommended': True, }},
            'copyright_statement': {'style': {'max_length': 3000, }},
        }

    # DONE -> XSD based script Validation added to X parent classes with X in [Corpus, LanguageDescription, LCR]
    def validate(self, data):
        xml_upload_with_data = self.context.get('xml_upload_with_data')
        service_compliant_dataset = self.context.get('service_compliant_dataset')

        # for_information_only validation
        for_information_only = self.context.get('for_information_only', False)
        if for_information_only is False:
            # licence_terms is obligatory for not for-information-only records
            if len(data.get('licence_terms', [])) == 0:
                raise serializers.ValidationError(
                    {'licence_terms': _(f"{self.fields['licence_terms'].label}"
                                        f" may not be empty.")},
                    code='invalid',
                )

        else:
            # access_rights is obligatory for for-information-only records
            if len(data.get('access_rights', [])) == 0 and not data.get('licence_terms'):
                raise serializers.ValidationError(
                    {'access_rights': _(f"{self.fields['access_rights'].label}"
                                        f" may not be empty.")},
                    code='invalid',
                )

        # access_location, distribution_location and download_location
        # are obligatory upon conditions (distribution form value)
        if data.get('dataset_distribution_form', None) in [
            choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_INTERFACE,
            choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_QUERY,
            choices.DATASET_DISTRIBUTION_FORM_CHOICES.BLU_RAY,
            choices.DATASET_DISTRIBUTION_FORM_CHOICES.CD_ROM,
            choices.DATASET_DISTRIBUTION_FORM_CHOICES.HARD_DISK,
            choices.DATASET_DISTRIBUTION_FORM_CHOICES.DVD_R,
            choices.DATASET_DISTRIBUTION_FORM_CHOICES.OTHER,
            choices.DATASET_DISTRIBUTION_FORM_CHOICES.UNSPECIFIED
        ] \
                and not data.get('access_location', None):
            raise serializers.ValidationError(
                {'access_location': _(f"{self.fields['access_location'].label} cannot be blank")},
                code='invalid',
            )
        if service_compliant_dataset \
                and data.get('dataset_distribution_form',
                             None) == choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_QUERY \
                and len(data.get('is_queried_by', [])) > 1:
            raise serializers.ValidationError(
                {'is_queried_by': _(f"{self.fields['is_queried_by'].label} is required "
                                    f"to contain exactly one published elg compatible service")},
                code='invalid',
            )
        if not xml_upload_with_data:
            # Downloadable distribution form
            if data.get('dataset_distribution_form', None) == choices.DATASET_DISTRIBUTION_FORM_CHOICES.DOWNLOADABLE \
                    and all([data.get('download_location', '') == '',
                             data.get('access_location', '') == '',
                             data.get('dataset', None) is None]):
                raise serializers.ValidationError(
                    {'non_field_error': _(f"{self.fields['download_location'].label} or "
                                          f"{self.fields['access_location'].label}  is required")},
                    code='invalid',
                )
        # # Rest distribution form
        # if data.get('dataset_distribution_form', None) in [
        #     choices.DATASET_DISTRIBUTION_FORM_CHOICES.BLU_RAY,
        #     choices.DATASET_DISTRIBUTION_FORM_CHOICES.CD_ROM,
        #     choices.DATASET_DISTRIBUTION_FORM_CHOICES.HARD_DISK,
        #     choices.DATASET_DISTRIBUTION_FORM_CHOICES.DVD_R,
        #     choices.DATASET_DISTRIBUTION_FORM_CHOICES.OTHER,
        #     choices.DATASET_DISTRIBUTION_FORM_CHOICES.UNSPECIFIED] \
        #         and not data.get('distribution_location', None):
        #     raise serializers.ValidationError(
        #         {'distribution_location': _(
        #             f"{self.fields['distribution_location'].label} cannot be blank")},
        #         code='invalid',
        #     )
        return data

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'download_location')
        data = encode_iri_to_uri(data, 'access_location')
        data = encode_iri_to_uri(data, 'samples_location')
        return super().to_internal_value(data)

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        # add pk to dataset distribution display representation
        if instance:
            ret.update({'pk': instance.pk})
            ret.move_to_end('pk', last=False)
        return ret

    def to_xml_representation(self, instance=None, add_id=False):
        record_id = self.context.get('record_id')
        if instance.dataset is not None:
            record_id = h_encode(record_id)
            dist_pk = h_encode(instance.pk)
            instance.download_location = f'{settings.DJANGO_URL}/{settings.HOME_PREFIX}/storage/{record_id}/{dist_pk}/'
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        return representation


class PeriodOfTimeSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    """

    class Meta:
        model = models.PeriodOfTime
        fields = (
            'pk',
            'start_date',
            'end_date',
        )


class CorpusSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A structured collection of pieces of data (textual, audio,
    video, multimodal/multimedia, etc.) typically of considerable
    size and selected according to criteria external to the data
    (e.g. size, type of language, type of text producers or expected
    audience, etc.) to represent as comprehensively as possible the
    object of study
    """
    corpus_subclass = serializers.ChoiceField(
        choices=choices.CORPUS_SUBCLASS_CHOICES,
        label=_('Corpus subclass'),
        help_text=_('Introduces a classification of corpora into types (used'
                    ' for descriptive reasons)'),
    )
    # OneToOne relation
    unspecified_part = unspecifiedPartSerializer(
        allow_null=True,
        required=False,
        label=_('Part'),
        help_text=_('Used exclusively for \'for-information\' records where the'
                    ' media type cannot be computed or is not filled in'),
    )
    # Reverse FK relation
    corpus_media_part = CorpusMediaPartSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Corpus part'),
        help_text=_('A part/segment of a corpus based on its media type'
                    ' classification'),
    )
    # Reverse FK relation
    dataset_distribution = DatasetDistributionSerializer(
        many=True,
        allow_empty=False,
        label=_('Dataset distribution'),
        help_text=_('Any form with which a dataset is distributed, such as a'
                    ' downloadable form in a specific format (e.g.,'
                    ' spreadsheet, plain text, etc.) or an API with which it'
                    ' can be accessed'),
    )
    personal_data_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Personal data details'),
        help_text=_('If the resource includes personal data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    sensitive_data_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Sensitive data details'),
        help_text=_('If the resource includes sensitive data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    anonymization_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000)],
        allow_null=True,
        required=False,
        label=_('Anonymization details'),
        help_text=_('If the resource has been anonymized, this field can be'
                    ' used for entering more information, e.g., tool or method'
                    ' used for the anonymization, by whom it has been'
                    ' performed, whether there was any check of the results,'
                    ' etc.'),
    )
    # ManyToMany relation
    is_analysed_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Analysis tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for analysing LR A (the one being described), e.g.,'
                    ' a statistical tool'),
    )
    # ManyToMany relation
    is_edited_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Editing tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' editing LR A (the one being described)'),
    )
    # ManyToMany relation
    is_elicited_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Elicitation tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for eliciting LR A (the one being described)'),
    )
    # FK relation
    is_annotated_version_of = GenericLanguageResourceSerializer(
        allow_null=True,
        required=False,
        label=_('Annotated version of'),
        help_text=_('Links to a corpus B which is the raw corpus that has been'
                    ' annotated (corpus A, the one being described)'),
    )
    # FK relation
    is_aligned_version_of = GenericLanguageResourceSerializer(
        allow_null=True,
        required=False,
        label=_('Aligned version of'),
        help_text=_('Links to a corpus B which is the aligned version of corpus'
                    ' A (the one being described)'),
    )
    # ManyToMany relation
    is_converted_version_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Converted version of'),
        help_text=_('Links to LR B that has been the outcome of a conversion'
                    ' procedure from LR A (the one being described), e.g., a'
                    ' PDF to text conversion'),
    )
    time_coverage = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500)]
        ),
        allow_null=True,
        required=False,
        label=_('Time coverage'),
        help_text=_('Indicates the time period that the content of a resource'
                    ' is about'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    geographic_coverage = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500)]
        ),
        allow_null=True,
        required=False,
        label=_('Geographic coverage'),
        help_text=_('Indicates the geographic region that the content of a'
                    ' resource is about; for countries, recommended use of'
                    ' ISO-3166'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    temporal = PeriodOfTimeSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Temporal'),
        help_text=_('Temporal characteristics of the resource'),
    )
    register = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Register'),
        help_text=_('Specifies the type of register (any of the varieties of a'
                    ' language that a speaker uses in a particular social'
                    ' context [Merriam-Webster]) of the contents of a language'
                    ' resource'),
        validators=[validate_unique_multiple_multilingual_field]
    )

    class Meta:
        model = models.Corpus
        fields = (
            'pk',
            'lr_type',
            'corpus_subclass',
            'unspecified_part',
            'corpus_media_part',
            'dataset_distribution',
            'personal_data_included',
            'personal_data_details',
            'sensitive_data_included',
            'sensitive_data_details',
            'anonymized',
            'anonymization_details',
            'is_analysed_by',
            'is_edited_by',
            'is_elicited_by',
            'is_annotated_version_of',
            'is_aligned_version_of',
            'is_converted_version_of',
            'time_coverage',
            'geographic_coverage',
            'temporal',
            'spatial',
            'register',
            'user_query',
        )
        extra_kwargs = {
            'personal_data_included': {'style': {'recommended': True, }},
            'personal_data_details': {'style': {'max_length': 500, }},
            'sensitive_data_included': {'style': {'recommended': True, }},
            'sensitive_data_details': {'style': {'max_length': 500, }},
            'anonymization_details': {'style': {'max_length': 1000, }},
            'is_annotated_version_of': {'style': {'recommended': True, }},
            'time_coverage': {'style': {'max_length': 500, }},
            'geographic_coverage': {'style': {'max_length': 500, }},
            'register': {'style': {'max_length': 100, }},
        }
        to_display_false = [
            'lr_type',
        ]

    def validate(self, data):
        # service compliant dataset
        service_compliant_dataset = self.context.get('service_compliant_dataset')
        if service_compliant_dataset and data.get('dataset_distribution', None) is not None:
            valid_dataset = False
            distributions = data['dataset_distribution']
            accessible_through_query_distributions = []
            for dist in distributions:
                if dist.get(
                        'dataset_distribution_form') == choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_QUERY:
                    accessible_through_query_distributions.append(dist)
            for dist in accessible_through_query_distributions:
                if not dist.get('is_queried_by'):
                    break
                related_lr = dist['is_queried_by'][0]
                query_dict = {
                    'described_entity__languageresource__resource_name__en': related_lr['resource_name']['en'],
                    'described_entity__languageresource__version': related_lr['version'],
                    'management_object__status': 'p',
                    'management_object__functional_service': True
                }
                functional_service = models.MetadataRecord.objects.filter(**query_dict).first()
                if functional_service:
                    valid_dataset = True
                    break
            if not valid_dataset:
                raise serializers.ValidationError(
                    {'dataset_distribution': _('A service_compliant_dataset needs to be linked to at least one dataset'
                                               ' distribution with dataset_distribution_form='
                                               'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
                                               ' and "is_queried_by" needs to contain exactly one '
                                               'published elg compatible service.')},
                    code='invalid',
                )
        if (data.get('personal_data_included', None) == choices.PERSONAL_DATA_INCLUDED_CHOICES.YES_P
            or data.get('sensitive_data_included', None) == choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S) \
                and data.get('anonymized', None) is None:
            raise serializers.ValidationError(
                {'anonymized': _(f"{self.fields['anonymized'].label} cannot be blank.")},
                code='invalid',
            )

        # for_information_only validation
        for_information_only = self.context.get('for_information_only', False)
        # corpus_media_part XOR unspecified_part for_information_only records
        if for_information_only:
            if data.get('unspecified_part', None) is not None and len(data.get('corpus_media_part', [])) != 0:
                raise serializers.ValidationError(
                    _(f"{self.fields['unspecified_part'].label} and {self.fields['corpus_media_part'].label} "
                      f"cannot be used together"),
                    code='invalid',
                )
            if data.get('unspecified_part', None) is None and len(data.get('corpus_media_part', [])) == 0:
                raise serializers.ValidationError(
                    _(f"Use either {self.fields['unspecified_part'].label} or"
                      f" {self.fields['corpus_media_part'].label}"),
                    code='invalid',
                )
        else:
            # unspecified_part is only for for_information_only records
            if data.get('unspecified_part', None) is not None:
                raise serializers.ValidationError(
                    {'unspecified_part': _(f"{self.fields['unspecified_part'].label} "
                                           f"can be used only for information only records.")},
                    code='invalid',
                )
            # distribution_unspecified_feature  is only for for_information_only records or models
            if data.get('dataset_distribution', None):
                for d in data['dataset_distribution']:
                    if d.get('distribution_unspecified_feature', None) is not None:
                        raise serializers.ValidationError(
                            {'dataset_distribution': _(f"distribution_unspecified_feature "
                                                       f"can be used only for information only records or for models.")},
                            code='invalid',
                        )
            # person_data_included and sensitive_data_included are mandatory for not for information only records
            if not data.get('personal_data_included', ''):
                raise serializers.ValidationError(
                    {f"{self.fields['personal_data_included'].label}": _(
                        f"{self.fields['personal_data_included'].label} "
                        f"cannot be blank.")},
                    code='invalid',
                )
            if not data.get('sensitive_data_included', ''):
                raise serializers.ValidationError(
                    {f"{self.fields['sensitive_data_included'].label}": _(
                        f"{self.fields['sensitive_data_included'].label} "
                        f"cannot be blank.")},
                    code='invalid',
                )

        dataset_distribution_validation(data,
                                        media_part_field='corpus_media_part',
                                        media_type_field='corpus_media_type',
                                        media_part='CorpusTextPart',
                                        dataset_distribution_media_field='distribution_text_feature',
                                        message_feature='Text feature cannot be blank',
                                        message_part='Text feature cannot exist when text part does not.')
        dataset_distribution_validation(data,
                                        media_part_field='corpus_media_part',
                                        media_type_field='corpus_media_type',
                                        media_part='CorpusTextNumericalPart',
                                        dataset_distribution_media_field='distribution_text_numerical_feature',
                                        message_feature='Numerical text feature cannot be blank',
                                        message_part='Numerical text feature cannot exist when numerical text part does not.')
        dataset_distribution_validation(data,
                                        media_part_field='corpus_media_part',
                                        media_type_field='corpus_media_type',
                                        media_part='CorpusAudioPart',
                                        dataset_distribution_media_field='distribution_audio_feature',
                                        message_feature='Audio feature cannot be blank',
                                        message_part='Audio feature cannot exist when audio part does not.')
        dataset_distribution_validation(data,
                                        media_part_field='corpus_media_part',
                                        media_type_field='corpus_media_type',
                                        media_part='CorpusVideoPart',
                                        dataset_distribution_media_field='distribution_video_feature',
                                        message_feature='Video feature cannot be blank',
                                        message_part='Video feature cannot exist when video part does not.')
        dataset_distribution_validation(data,
                                        media_part_field='corpus_media_part',
                                        media_type_field='corpus_media_type',
                                        media_part='CorpusImagePart',
                                        dataset_distribution_media_field='distribution_image_feature',
                                        message_feature='Image feature cannot be blank',
                                        message_part='Image feature cannot exist when image part does not.')

        dataset_distribution_unspecified_part_validation(data)
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'spatial')
        return super().to_internal_value(data)


class LexicalConceptualResourceTextPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A part (or whole set) of a lexical/conceptual resource that
    consists of textual elements
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    metalanguage = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )

    class Meta:
        model = models.LexicalConceptualResourceTextPart
        fields = (
            'pk',
            'lcr_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'metalanguage',
            'modality_type',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'metalanguage': {'style': {'recommended': True, }},
        }
        to_display_false = [
            'lcr_media_type',
            'media_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance

    def validate(self, data):
        unique_language_validation(data, 'language')
        unique_language_validation(data, 'metalanguage')
        return data


class LexicalConceptualResourceAudioPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The part (or whole set of) a lexical/conceptual resource that
    consists of audio elements (e.g., a set of audio files for a
    lexicon)
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    metalanguage = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    legend = SerializersMultilingualField(
        validators=[validate_dict_value_length(200)],
        allow_null=True,
        required=False,
        label=_('Legend'),
        help_text=_('Introduces the legend of the soundtrack'),
    )

    class Meta:
        model = models.LexicalConceptualResourceAudioPart
        fields = (
            'pk',
            'lcr_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'metalanguage',
            'modality_type',
            'speech_item',
            'non_speech_item',
            'legend',
            'noise_level',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'metalanguage': {'style': {'recommended': True, }},
            'legend': {'style': {'max_length': 200, }},
        }
        to_display_false = [
            'lcr_media_type',
            'media_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance

    def validate(self, data):
        unique_language_validation(data, 'language')
        unique_language_validation(data, 'metalanguage')
        return data


class LexicalConceptualResourceVideoPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The part (or whole set) of a lexical/conceptual resource that
    consists of videos (e.g., a set of video files for the lexicon
    of a sign language)
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    metalanguage = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    type_of_video_content = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Type of video content'),
        help_text=_('Main type of object or people represented in the video'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    dynamic_element = DynamicElementSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Dynamic element'),
        help_text=_('Groups information on the dynamic elements that are'
                    ' represented in the video part of the resource'),
    )

    class Meta:
        model = models.LexicalConceptualResourceVideoPart
        fields = (
            'pk',
            'lcr_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'metalanguage',
            'modality_type',
            'type_of_video_content',
            'text_included_in_video',
            'dynamic_element',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'metalanguage': {'style': {'recommended': True, }},
            'type_of_video_content': {'style': {'max_length': 300, }},
        }
        to_display_false = [
            'lcr_media_type',
            'media_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance

    def validate(self, data):
        unique_language_validation(data, 'language')
        unique_language_validation(data, 'metalanguage')
        return data


class LexicalConceptualResourceImagePartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A part (or whole set of) a lexical/conceptual resource that
    consists of images (e.g., a set of images used for a lexicon)
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    metalanguage = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    type_of_image_content = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Type of image content'),
        help_text=_('The main types of object or people represented in the'
                    ' image corpus'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    static_element = StaticElementSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Static element'),
        help_text=_('Groups information on the static elements visible on'
                    ' images'),
    )

    class Meta:
        model = models.LexicalConceptualResourceImagePart
        fields = (
            'pk',
            'lcr_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'metalanguage',
            'modality_type',
            'type_of_image_content',
            'text_included_in_image',
            'static_element',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'language': {'style': {'recommended': True, }},
            'metalanguage': {'style': {'recommended': True, }},
            'type_of_image_content': {'style': {'max_length': 500, }},
        }
        to_display_false = [
            'lcr_media_type',
            'media_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance

    def validate(self, data):
        unique_language_validation(data, 'language')
        unique_language_validation(data, 'metalanguage')
        return data


class LexicalConceptualResourceMediaPartSerializer(
    PolymorphicSerializer, WritableNestedModelSerializer,
    XMLPolymorphicSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A part/segment of a lexical/conceptual resource based on its
    media type classification
    """

    class Meta:
        model = models.LexicalConceptualResourceMediaPart
        fields = '__all__'

    model_serializer_mapping = {
        models.LexicalConceptualResourceTextPart: LexicalConceptualResourceTextPartSerializer,
        models.LexicalConceptualResourceAudioPart: LexicalConceptualResourceAudioPartSerializer,
        models.LexicalConceptualResourceVideoPart: LexicalConceptualResourceVideoPartSerializer,
        models.LexicalConceptualResourceImagePart: LexicalConceptualResourceImagePartSerializer
    }

    resource_type_field_name = 'lcr_media_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _(
                    'LCR media type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]

    def _get_serializer_from_resource_type(self, resource_type):
        try:
            model = self.resource_type_model_mapping[resource_type]
        except KeyError:
            if self.get_draft():
                raise serializers.ValidationError({
                    self.resource_type_field_name: '{0} cannot be blank'.format(
                        self.resource_type_field_name
                    )
                })
            raise serializers.ValidationError({
                self.resource_type_field_name: 'Invalid {0}'.format(
                    self.resource_type_field_name
                )
            })

        return self._get_serializer_from_model_or_instance(model)


class LexicalConceptualResourceSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A resource organised on the basis of lexical or conceptual
    entries (lexical items, terms, concepts etc.) with their
    supplementary information (e.g. grammatical, semantic,
    statistical information, etc.)
    """
    encoding_level = serializers.ListField(
        child=serializers.ChoiceField(
            choices=choices.ENCODING_LEVEL_CHOICES),
        allow_empty=False,
        label=_('Encoding level'),
        help_text=_('Classifies the contents of a lexical/conceptual resource'
                    ' or language description as regards the linguistic level'
                    ' of analysis it caters for'),
    )
    # ManyToMany relation
    external_reference = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('External reference'),
        help_text=_('Provides reference to another resource to which the'
                    ' lexicalConceptualResource is linked (e.g., link to a'
                    ' wordnet or ontology)'),
    )
    theoretic_model = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(200)]
        ),
        allow_null=True,
        required=False,
        label=_('Theoretic model'),
        help_text=_('Indicates the theoretic model applied for the'
                    ' creation/enrichment of the resource, by name and/or by'
                    ' reference (URL or bibliographic reference) to informative'
                    ' material about the theoretic model used'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # OneToOne relation
    unspecified_part = unspecifiedPartSerializer(
        allow_null=True,
        required=False,
        label=_('Part'),
        help_text=_('Used exclusively for \'for-information\' records where the'
                    ' media type cannot be computed or is not filled in'),
    )
    # Reverse FK relation
    lexical_conceptual_resource_media_part = LexicalConceptualResourceMediaPartSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Lexical/ Conceptual resource part'),
        help_text=_('A part/segment of a lexical/conceptual resource based on'
                    ' its media type classification'),
    )
    # Reverse FK relation
    dataset_distribution = DatasetDistributionSerializer(
        many=True,
        allow_empty=False,
        label=_('Dataset distribution'),
        help_text=_('Any form with which a dataset is distributed, such as a'
                    ' downloadable form in a specific format (e.g.,'
                    ' spreadsheet, plain text, etc.) or an API with which it'
                    ' can be accessed'),
    )
    personal_data_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Personal data details'),
        help_text=_('If the resource includes personal data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    sensitive_data_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Sensitive data details'),
        help_text=_('If the resource includes sensitive data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    anonymization_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000)],
        allow_null=True,
        required=False,
        label=_('Anonymization details'),
        help_text=_('If the resource has been anonymized, this field can be'
                    ' used for entering more information, e.g., tool or method'
                    ' used for the anonymization, by whom it has been'
                    ' performed, whether there was any check of the results,'
                    ' etc.'),
    )
    # ManyToMany relation
    is_analysed_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Analysis tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for analysing LR A (the one being described), e.g.,'
                    ' a statistical tool'),
    )
    # ManyToMany relation
    is_edited_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Editing tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' editing LR A (the one being described)'),
    )
    # ManyToMany relation
    is_elicited_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Elicitation tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for eliciting LR A (the one being described)'),
    )
    # ManyToMany relation
    is_converted_version_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Converted version of'),
        help_text=_('Links to LR B that has been the outcome of a conversion'
                    ' procedure from LR A (the one being described), e.g., a'
                    ' PDF to text conversion'),
    )
    time_coverage = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500)]
        ),
        allow_null=True,
        required=False,
        label=_('Time coverage'),
        help_text=_('Indicates the time period that the content of a resource'
                    ' is about'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    geographic_coverage = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500)]
        ),
        allow_null=True,
        required=False,
        label=_('Geographic coverage'),
        help_text=_('Indicates the geographic region that the content of a'
                    ' resource is about; for countries, recommended use of'
                    ' ISO-3166'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    temporal = PeriodOfTimeSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Temporal'),
        help_text=_('Temporal characteristics of the resource'),
    )

    class Meta:
        model = models.LexicalConceptualResource
        fields = (
            'pk',
            'lr_type',
            'lcr_subclass',
            'encoding_level',
            'content_type',
            'external_reference',
            'theoretic_model',
            'extratextual_information',
            'extratextual_information_unit',
            'unspecified_part',
            'lexical_conceptual_resource_media_part',
            'dataset_distribution',
            'personal_data_included',
            'personal_data_details',
            'sensitive_data_included',
            'sensitive_data_details',
            'anonymized',
            'anonymization_details',
            'is_analysed_by',
            'is_edited_by',
            'is_elicited_by',
            'is_converted_version_of',
            'time_coverage',
            'geographic_coverage',
            'temporal',
            'spatial',
        )
        extra_kwargs = {
            'lcr_subclass': {'style': {'recommended': True, }},
            'content_type': {'style': {'recommended': True, }},
            'theoretic_model': {'style': {'max_length': 200, }},
            'personal_data_included': {'style': {'recommended': True, }},
            'personal_data_details': {'style': {'max_length': 500, }},
            'sensitive_data_included': {'style': {'recommended': True, }},
            'sensitive_data_details': {'style': {'max_length': 500, }},
            'anonymization_details': {'style': {'max_length': 1000, }},
            'time_coverage': {'style': {'max_length': 500, }},
            'geographic_coverage': {'style': {'max_length': 500, }},
        }
        to_display_false = [
            'lr_type',
        ]

    def validate(self, data):
        # service compliant dataset
        service_compliant_dataset = self.context.get('service_compliant_dataset')
        if service_compliant_dataset and data.get('dataset_distribution', None) is not None:
            valid_dataset = False
            distributions = data['dataset_distribution']
            accessible_through_query_distributions = []
            for dist in distributions:
                if dist.get(
                        'dataset_distribution_form') == choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_QUERY:
                    accessible_through_query_distributions.append(dist)
            for dist in accessible_through_query_distributions:
                related_lr = dist['is_queried_by'][0]
                query_dict = {
                    'described_entity__languageresource__resource_name__en': related_lr['resource_name']['en'],
                    'described_entity__languageresource__version': related_lr['version'],
                    'management_object__status': 'p',
                    'management_object__functional_service': True
                }
                functional_service = models.MetadataRecord.objects.filter(**query_dict).first()
                if functional_service:
                    valid_dataset = True
                    break
            if not valid_dataset:
                raise serializers.ValidationError(
                    {'dataset_distribution': _('A service_compliant_dataset needs to be linked to at least one dataset'
                                               ' distribution with dataset_distribution_form='
                                               'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
                                               ' and "is_queried_by" needs to contain exactly one '
                                               'published elg compatible service.')},
                    code='invalid',
                )

        # for_information_only validation
        for_information_only = self.context.get('for_information_only', False)
        # corpus_media_part XOR unspecified_part for_information_only records
        if for_information_only:
            if data.get('unspecified_part', None) is not None and \
                    len(data.get('lexical_conceptual_resource_media_part', [])) != 0:
                raise serializers.ValidationError(
                    _(f"{self.fields['unspecified_part'].label} and "
                      f"{self.fields['lexical_conceptual_resource_media_part'].label} "
                      f"cannot be used together"),
                    code='invalid',
                )
            if data.get('unspecified_part', None) is None and \
                    len(data.get('lexical_conceptual_resource_media_part', [])) == 0:
                raise serializers.ValidationError(
                    _(f"Use either {self.fields['unspecified_part'].label} or"
                      f" {self.fields['lexical_conceptual_resource_media_part'].label}"),
                    code='invalid',
                )
        else:
            # unspecified_part is only for for_information_only records
            if data.get('unspecified_part', None) is not None:
                raise serializers.ValidationError(
                    {'unspecified_part': _(f"{self.fields['unspecified_part'].label} "
                                           f"can be used only for information only records.")},
                    code='invalid',
                )
            # distribution_unspecified_feature is only for for_information_only records or models
            if data.get('dataset_distribution', None):
                for d in data['dataset_distribution']:
                    if d.get('distribution_unspecified_feature', None) is not None:
                        raise serializers.ValidationError(
                            {'dataset_distribution': _(f"distribution_unspecified_feature "
                                                       f"can be used only for information only records or for models.")},
                            code='invalid',
                        )
            # person_data_included and sensitive_data_included are mandatory for not for information only records
            if not data.get('personal_data_included', None):
                raise serializers.ValidationError(
                    {f"{self.fields['personal_data_included'].label}": _(
                        f"{self.fields['personal_data_included'].label} "
                        f"cannot be blank.")},
                    code='invalid',
                )
            if not data.get('sensitive_data_included', None):
                raise serializers.ValidationError(
                    {f"{self.fields['sensitive_data_included'].label}": _(
                        f"{self.fields['sensitive_data_included'].label} "
                        f"cannot be blank.")},
                    code='invalid',
                )

        if (data.get('personal_data_included', None) == choices.PERSONAL_DATA_INCLUDED_CHOICES.YES_P
            or data.get('sensitive_data_included', None) == choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S) \
                and data.get('anonymized', None) is None:
            raise serializers.ValidationError(
                {'anonymized': _(f"{self.fields['anonymized'].label} cannot be blank.")},
                code='invalid',
            )

        dataset_distribution_validation(data,
                                        media_part_field='lexical_conceptual_resource_media_part',
                                        media_type_field='lcr_media_type',
                                        media_part='LexicalConceptualResourceTextPart',
                                        dataset_distribution_media_field='distribution_text_feature',
                                        message_feature='Text feature cannot be blank',
                                        message_part='Text feature cannot exist when text part does not.')
        dataset_distribution_validation(data,
                                        media_part_field='lexical_conceptual_resource_media_part',
                                        media_type_field='lcr_media_type',
                                        media_part='LexicalConceptualResourceAudioPart',
                                        dataset_distribution_media_field='distribution_audio_feature',
                                        message_feature='Audio feature cannot be blank',
                                        message_part='Audio feature cannot exist when audio part does not.')
        dataset_distribution_validation(data,
                                        media_part_field='lexical_conceptual_resource_media_part',
                                        media_type_field='lcr_media_type',
                                        media_part='LexicalConceptualResourceVideoPart',
                                        dataset_distribution_media_field='distribution_video_feature',
                                        message_feature='Video feature cannot be blank',
                                        message_part='Video feature cannot exist when video part does not.')
        dataset_distribution_validation(data,
                                        media_part_field='lexical_conceptual_resource_media_part',
                                        media_type_field='lcr_media_type',
                                        media_part='LexicalConceptualResourceImagePart',
                                        dataset_distribution_media_field='distribution_image_feature',
                                        message_feature='Image feature cannot be blank',
                                        message_part='Image feature cannot exist when image part does not.')

        dataset_distribution_unspecified_part_validation(data)
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'spatial')
        return super().to_internal_value(data)


class GrammarSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A set of rules governing what strings are valid or allowable in
    a language or text
    [https://en.oxforddictionaries.com/definition/grammar]
    """
    encoding_level = serializers.ListField(
        child=serializers.ChoiceField(
            choices=choices.ENCODING_LEVEL_CHOICES),
        allow_empty=False,
        label=_('Encoding level'),
        help_text=_('Classifies the contents of a lexical/conceptual resource'
                    ' or language description as regards the linguistic level'
                    ' of analysis it caters for'),
    )
    theoretic_model = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(200)]
        ),
        allow_null=True,
        required=False,
        label=_('Theoretic model'),
        help_text=_('Indicates the theoretic model applied for the'
                    ' creation/enrichment of the resource, by name and/or by'
                    ' reference (URL or bibliographic reference) to informative'
                    ' material about the theoretic model used'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    formalism = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Formalism'),
        help_text=_('Specifies the formalism (bibliographic reference, URL,'
                    ' name) used for the creation/enrichment of the resource'
                    ' (grammar or tool/service)'),
    )
    # ManyToMany relation
    requires_lr = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Required LRT'),
        help_text=_('Links to LR B that is required for the operation of a'
                    ' tool/service or computational grammar (the one that is'
                    ' described)'),
    )
    attached_lexicon_position = SerializersMultilingualField(
        validators=[validate_dict_value_length(200)],
        allow_null=True,
        required=False,
        label=_('Attached lexicon position'),
        help_text=_('Indicates the position of the lexicon, if attached to the'
                    ' grammar'),
    )
    robustness = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Robustness'),
        help_text=_('Introduces a free text statement on the robustness of the'
                    ' grammar (how well the grammar can cope with'
                    ' misspelt/unknown, etc. input, i.e. whether it can produce'
                    ' even partial interpretations of the input)'),
    )
    shallowness = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Shallowness'),
        help_text=_('Introduces a free text statement on the shallowness of the'
                    ' grammar (how deep the syntactic analysis performed by the'
                    ' grammar can be)'),
    )
    output = SerializersMultilingualField(
        validators=[validate_dict_value_length(150)],
        allow_null=True,
        required=False,
        label=_('Output'),
        help_text=_('Indicates whether the output of the operation of the'
                    ' grammar is a statement of grammaticality'
                    ' (grammatical/ungrammatical) or structures (interpretation'
                    ' of the input)'),
    )

    class Meta:
        model = models.Grammar
        fields = (
            'pk',
            'ld_subclass_type',
            'encoding_level',
            'theoretic_model',
            'formalism',
            'ld_task',
            'grammatical_phenomena_coverage',
            'weighted_grammar',
            'requires_lr',
            'related_lexicon_type',
            'attached_lexicon_position',
            'compatible_lexicon_type',
            'robustness',
            'shallowness',
            'output',
        )
        extra_kwargs = {
            'theoretic_model': {'style': {'max_length': 200, }},
            'formalism': {'style': {'max_length': 100, 'recommended': True, }},
            'ld_task': {'style': {'recommended': True, }},
            'attached_lexicon_position': {'style': {'max_length': 200, }},
            'robustness': {'style': {'max_length': 100, }},
            'shallowness': {'style': {'max_length': 100, }},
            'output': {'style': {'max_length': 150, }},
        }
        to_display_false = [
            'ld_subclass_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance


class NGramModelSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A language model consisting of n-grams, i.e. specific sequences
    of a number of words
    """
    base_item = serializers.ListField(
        child=serializers.ChoiceField(
            choices=choices.BASE_ITEM_CHOICES),
        allow_empty=False,
        label=_('Base item'),
        help_text=_('Type of item that is represented in the n-gram resource'),
    )
    order = serializers.IntegerField(
        label=_('Order'),
        help_text=_('Specifies the maximum number of items in the sequence'),
    )

    class Meta:
        model = models.NGramModel
        fields = (
            'pk',
            'base_item',
            'order',
            'perplexity',
            'is_factored',
            'factor',
            'smoothing',
            'interpolated',
        )


class ModelSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The model artifact that is created through a training process
    involving an algorithm (that is, the learning algorithm) and the
    training data to learn from
    """
    model_function = serializers.ListField(
        child=serializers.CharField(max_length=200),
        allow_empty=False,
        label=_('Model function'),
        help_text=_('Specifies the operation/function/task that a model'
                    ' performs'),
    )
    # ManyToMany relation
    typesystem = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Typesystem'),
        help_text=_('Specifies the typesystem (preferrably through an'
                    ' identifier or URL) that has been used for the annotation'
                    ' of a resource or that is required for the input resource'
                    ' of a tool/service or that should be used (dependency) for'
                    ' the annotation or used in the training of a ML model'),
    )
    # ManyToMany relation
    annotation_schema = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation schema'),
        help_text=_('The annotation schema used for annotating a resource or'
                    ' for the input resource (if annotated) of a tool/service'
                    ' or that should be used (dependency) for the annotation'),
    )
    # ManyToMany relation
    annotation_resource = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation resource'),
        help_text=_('Specifies the annotation resource used for annotating a'
                    ' corpus or that has been used for the input resource of a'
                    ' tool/service or that should be used (dependency) for the'
                    ' annotation'),
    )
    # ManyToMany relation
    tagset = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Tagset'),
        help_text=_('The tagset used for annotating a resource or for the input'
                    ' resource (if annotated) of a tool/service or that should'
                    ' be used (dependency) for the annotation'),
    )
    algorithm = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Algorithm'),
        help_text=_('Identifies the training algorithm used for the model'
                    ' (e.g., maximum entropy, svm, etc.)'),
    )
    algorithm_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Algorithm details'),
        help_text=_('Provides a detailed description of the algorithm, incl.'
                    ' info on whether it\'s supervised or not'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    training_corpus_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Training corpus details'),
        help_text=_('Provides a detailed description of the training corpus'
                    ' (e.g., size, number of features, etc.)'),
    )
    training_process_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(2000)],
        allow_null=True,
        required=False,
        label=_('Training process details'),
        help_text=_('Provides a detailed description of the training process'
                    ' (pre-processing, pretraining, etc.)'),
    )
    bias_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(2000)],
        allow_null=True,
        required=False,
        label=_('Bias details'),
        help_text=_('Provides a free text account on bias considerations of a'
                    ' model'),
    )
    # ManyToMany relation
    requires_lr = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Required LRT'),
        help_text=_('Links to LR B that is required for the operation of a'
                    ' tool/service or computational grammar (the one that is'
                    ' described)'),
    )
    # OneToOne relation
    n_gram_model = NGramModelSerializer(
        allow_null=True,
        required=False,
        label=_('N-gram model'),
        help_text=_('A language model consisting of n-grams, i.e. specific'
                    ' sequences of a number of words'),
    )

    class Meta:
        model = models.Model
        fields = (
            'pk',
            'ld_subclass_type',
            'model_type',
            'model_function',
            'model_variant',
            'typesystem',
            'annotation_schema',
            'annotation_resource',
            'tagset',
            'method',
            'development_framework',
            'algorithm',
            'algorithm_details',
            'has_original_source',
            'training_corpus_details',
            'training_process_details',
            'bias_details',
            'requires_lr',
            'n_gram_model',
        )
        extra_kwargs = {
            'model_type': {'style': {'recommended': True, }},
            'model_variant': {'style': {'recommended': True, }},
            'development_framework': {'style': {'recommended': True, }},
            'algorithm': {'style': {'max_length': 100, }},
            'algorithm_details': {'style': {'max_length': 500, }},
            'has_original_source': {'style': {'recommended': True, }},
            'training_corpus_details': {'style': {'max_length': 500, 'recommended': True, }},
            'training_process_details': {'style': {'max_length': 2000, 'recommended': True, }},
            'bias_details': {'style': {'max_length': 2000, 'recommended': True, }},
            'requires_lr': {'style': {'recommended': True, }},
            'n_gram_model': {'style': {'recommended': True, }},
        }
        to_display_false = [
            'ld_subclass_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        model_type_xml_field_name = self.Meta.model.schema_fields['model_type']
        if representation.get(model_type_xml_field_name):
            representation[model_type_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[model_type_xml_field_name], choices.MODEL_TYPE_RECOMMENDED_CHOICES,
                'ms:modelTypeRecommended', 'ms:modelTypeOther'
            )
        model_function_xml_field_name = self.Meta.model.schema_fields['model_function']
        if representation.get(model_function_xml_field_name):
            representation[model_function_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[model_function_xml_field_name], choices.MODEL_FUNCTION_RECOMMENDED_CHOICES,
                'ms:modelFunctionRecommended', 'ms:modelFunctionOther'
            )
        dev_framework_xml_field_name = self.Meta.model.schema_fields['development_framework']
        if representation.get(dev_framework_xml_field_name):
            representation[dev_framework_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[dev_framework_xml_field_name], choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES,
                'ms:DevelopmentFrameworkRecommended', 'ms:DevelopmentFrameworkOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        if ret.get('model_type'):
            ret['model_type'] = super().from_xml_representation_for_recommended_cv(
                ret['model_type'],
                'model_type', choices.MODEL_TYPE_RECOMMENDED_CHOICES,
                'ms:modelTypeRecommended', 'ms:modelTypeOther'
            )
        if ret.get('model_function'):
            ret['model_function'] = super().from_xml_representation_for_recommended_cv(
                ret['model_function'],
                'model_function', choices.MODEL_FUNCTION_RECOMMENDED_CHOICES,
                'ms:modelFunctionRecommended', 'ms:modelFunctionOther'
            )
        if ret.get('development_framework'):
            ret['development_framework'] = super().from_xml_representation_for_recommended_cv(
                ret['development_framework'],
                'development_framework', choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES,
                'ms:DevelopmentFrameworkRecommended', 'ms:DevelopmentFrameworkOther'
            )
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        if ret.get('model_type'):
            ret['model_type'] = super().to_display_representation_for_recommended_cv(
                ret['model_type'], choices.MODEL_TYPE_RECOMMENDED_CHOICES)
        if ret.get('model_function'):
            ret['model_function'] = super().to_display_representation_for_recommended_cv(
                ret['model_function'], choices.MODEL_FUNCTION_RECOMMENDED_CHOICES)
        if ret.get('development_framework'):
            ret['development_framework'] = super().to_display_representation_for_recommended_cv(
                ret['development_framework'], choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES)
        return ret


class LanguageDescriptionSubclassSerializer(
    PolymorphicSerializer, WritableNestedModelSerializer,
    XMLPolymorphicSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The type of the language description (used for documentation
    purposes)
    """

    class Meta:
        model = models.LanguageDescriptionSubclass
        fields = '__all__'

    model_serializer_mapping = {
        models.Grammar: GrammarSerializer,
        models.Model: ModelSerializer
    }

    resource_type_field_name = 'ld_subclass_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _(
                    'LD subclass cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]

    def _get_serializer_from_resource_type(self, resource_type):
        try:
            model = self.resource_type_model_mapping[resource_type]
        except KeyError:
            if self.get_draft():
                raise serializers.ValidationError({
                    self.resource_type_field_name: '{0} cannot be blank'.format(
                        self.resource_type_field_name
                    )
                })
            raise serializers.ValidationError({
                self.resource_type_field_name: 'Invalid {0}'.format(
                    self.resource_type_field_name
                )
            })

        return self._get_serializer_from_model_or_instance(model)


class LanguageDescriptionTextPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The textual part (or whole set) of a language description
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    metalanguage = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    creation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    # ManyToMany relation
    is_created_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    # Reverse FK relation
    link_to_other_media = LinkToOtherMediaSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )

    class Meta:
        model = models.LanguageDescriptionTextPart
        fields = (
            'pk',
            'ld_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'metalanguage',
            'modality_type',
            'creation_mode',
            'creation_details',
            'is_created_by',
            'has_original_source',
            'original_source_description',
            'link_to_other_media',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'metalanguage': {'style': {'recommended': True, }},
            'creation_details': {'style': {'max_length': 3000, }},
            'original_source_description': {'style': {'max_length': 500, }},
        }
        to_display_false = [
            'ld_media_type',
            'media_type',
        ]

    def validate(self, data):
        language_ids = [language['language_id'] for language in data['language']]
        contains_collective_lang = [(language in collective_language_ids) for language in language_ids]
        contains_collective_lang = True if True in contains_collective_lang else False
        if (len(data.get('language', None)) >= 2 or contains_collective_lang) \
                and not data.get('multilinguality_type', None):
            raise serializers.ValidationError(
                {'multilinguality_type': _(
                    'Multilinguality type cannot be blank')},
                code='invalid',
            )
        unique_language_validation(data, 'language')
        unique_language_validation(data, 'metalanguage')
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance


class LanguageDescriptionImagePartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The part (or whole set) of a language description that consists
    of images (e.g., a grammar part with photos or images for sign
    languages)
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    metalanguage = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    type_of_image_content = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Type of image content'),
        help_text=_('The main types of object or people represented in the'
                    ' image corpus'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    static_element = StaticElementSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Static element'),
        help_text=_('Groups information on the static elements visible on'
                    ' images'),
    )
    creation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    # ManyToMany relation
    is_created_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    # Reverse FK relation
    link_to_other_media = LinkToOtherMediaSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )

    class Meta:
        model = models.LanguageDescriptionImagePart
        fields = (
            'pk',
            'ld_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'metalanguage',
            'modality_type',
            'type_of_image_content',
            'text_included_in_image',
            'static_element',
            'creation_mode',
            'creation_details',
            'is_created_by',
            'has_original_source',
            'original_source_description',
            'link_to_other_media',)
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'language': {'style': {'recommended': True, }},
            'metalanguage': {'style': {'recommended': True, }},
            'type_of_image_content': {'style': {'max_length': 500, }},
            'creation_details': {'style': {'max_length': 3000, }},
            'original_source_description': {'style': {'max_length': 500, }},
        }
        to_display_false = [
            'ld_media_type',
            'media_type',
        ]

    def validate(self, data):
        language_ids = [language['language_id'] for language in data['language']]
        contains_collective_lang = [(language in collective_language_ids) for language in language_ids]
        contains_collective_lang = True if True in contains_collective_lang else False
        if (len(data.get('language', None)) >= 2 or contains_collective_lang) \
                and not data.get('multilinguality_type', None):
            raise serializers.ValidationError(
                {'multilinguality_type': _(
                    'Multilinguality type cannot be blank')},
                code='invalid',
            )
        unique_language_validation(data, 'language')
        unique_language_validation(data, 'metalanguage')
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance


class LanguageDescriptionVideoPartSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The part (or whole set) of a language description that consists
    of videos (e.g., a grammar part with videos for sign languages)
    """
    multilinguality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # ManyToMany relation
    metalanguage = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    type_of_video_content = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Type of video content'),
        help_text=_('Main type of object or people represented in the video'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    dynamic_element = DynamicElementSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Dynamic element'),
        help_text=_('Groups information on the dynamic elements that are'
                    ' represented in the video part of the resource'),
    )
    creation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    # ManyToMany relation
    is_created_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    # Reverse FK relation
    link_to_other_media = LinkToOtherMediaSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )

    class Meta:
        model = models.LanguageDescriptionVideoPart
        fields = (
            'pk',
            'ld_media_type',
            'media_type',
            'linguality_type',
            'multilinguality_type',
            'multilinguality_type_details',
            'language',
            'metalanguage',
            'modality_type',
            'type_of_video_content',
            'text_included_in_video',
            'dynamic_element',
            'creation_mode',
            'creation_details',
            'is_created_by',
            'has_original_source',
            'original_source_description',
            'link_to_other_media',
        )
        extra_kwargs = {
            'multilinguality_type_details': {'style': {'max_length': 500, }},
            'metalanguage': {'style': {'recommended': True, }},
            'type_of_video_content': {'style': {'max_length': 300, }},
            'creation_details': {'style': {'max_length': 3000, }},
            'original_source_description': {'style': {'max_length': 500, }},
        }
        to_display_false = [
            'ld_media_type',
            'media_type',
        ]

    def validate(self, data):
        language_ids = [language['language_id'] for language in data['language']]
        contains_collective_lang = [(language in collective_language_ids) for language in language_ids]
        contains_collective_lang = True if True in contains_collective_lang else False
        if (len(data.get('language', None)) >= 2 or contains_collective_lang) \
                and not data.get('multilinguality_type', None):
            raise serializers.ValidationError(
                {'multilinguality_type': _(
                    'Multilinguality type cannot be blank')},
                code='invalid',
            )
        unique_language_validation(data, 'language')
        unique_language_validation(data, 'metalanguage')
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Generate linguality_type given language
        validated_data = generate_linguality_type(validated_data)
        instance = super().update(instance, validated_data)
        return instance


class LanguageDescriptionMediaPartSerializer(
    PolymorphicSerializer, WritableNestedModelSerializer,
    XMLPolymorphicSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A part/segment of a language description based on its media type
    classification
    """

    class Meta:
        model = models.LanguageDescriptionMediaPart
        fields = '__all__'

    model_serializer_mapping = {
        models.LanguageDescriptionTextPart: LanguageDescriptionTextPartSerializer,
        models.LanguageDescriptionImagePart: LanguageDescriptionImagePartSerializer,
        models.LanguageDescriptionVideoPart: LanguageDescriptionVideoPartSerializer
    }

    resource_type_field_name = 'ld_media_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _(
                    'LD media type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]

    def _get_serializer_from_resource_type(self, resource_type):
        try:
            model = self.resource_type_model_mapping[resource_type]
        except KeyError:
            if self.get_draft():
                raise serializers.ValidationError({
                    self.resource_type_field_name: '{0} cannot be blank'.format(
                        self.resource_type_field_name
                    )
                })
            raise serializers.ValidationError({
                self.resource_type_field_name: 'Invalid {0}'.format(
                    self.resource_type_field_name
                )
            })

        return self._get_serializer_from_model_or_instance(model)


class LanguageDescriptionSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A resource that describes a language or some aspect(s) of a
    language via a systematic documentation of linguistic structures
    """
    ld_subclass = serializers.ChoiceField(
        choices=choices.LD_SUBCLASS_CHOICES,
        label=_('Language Description subclass'),
        help_text=_('A classification of language descriptions into models,'
                    ' grammars etc.'),
    )
    # OneToOne relation
    language_description_subclass = LanguageDescriptionSubclassSerializer(
        allow_null=True,
        required=False,
        label=_('Language description type'),
        help_text=_('The type of the language description (used for'
                    ' documentation purposes)'),
    )
    running_environment_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(200)],
        allow_null=True,
        required=False,
        label=_('Running environment details'),
        help_text=_('Provides further information on the running environment of'
                    ' a tool/service or of a language description'),
    )
    # OneToOne relation
    unspecified_part = unspecifiedPartSerializer(
        allow_null=True,
        required=False,
        label=_('Part'),
        help_text=_('Used exclusively for \'for-information\' records where the'
                    ' media type cannot be computed or is not filled in'),
    )
    # Reverse FK relation
    language_description_media_part = LanguageDescriptionMediaPartSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Language description part'),
        help_text=_('A part/segment of a language description based on its'
                    ' media type classification'),
    )
    # Reverse FK relation
    dataset_distribution = DatasetDistributionSerializer(
        many=True,
        allow_empty=False,
        label=_('Dataset distribution'),
        help_text=_('Any form with which a dataset is distributed, such as a'
                    ' downloadable form in a specific format (e.g.,'
                    ' spreadsheet, plain text, etc.) or an API with which it'
                    ' can be accessed'),
    )
    personal_data_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Personal data details'),
        help_text=_('If the resource includes personal data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    sensitive_data_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Sensitive data details'),
        help_text=_('If the resource includes sensitive data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    anonymization_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000)],
        allow_null=True,
        required=False,
        label=_('Anonymization details'),
        help_text=_('If the resource has been anonymized, this field can be'
                    ' used for entering more information, e.g., tool or method'
                    ' used for the anonymization, by whom it has been'
                    ' performed, whether there was any check of the results,'
                    ' etc.'),
    )
    # ManyToMany relation
    is_analysed_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Analysis tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for analysing LR A (the one being described), e.g.,'
                    ' a statistical tool'),
    )
    # ManyToMany relation
    is_edited_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Editing tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' editing LR A (the one being described)'),
    )
    # ManyToMany relation
    is_elicited_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Elicitation tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for eliciting LR A (the one being described)'),
    )
    # ManyToMany relation
    is_converted_version_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Converted version of'),
        help_text=_('Links to LR B that has been the outcome of a conversion'
                    ' procedure from LR A (the one being described), e.g., a'
                    ' PDF to text conversion'),
    )
    time_coverage = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500)]
        ),
        allow_null=True,
        required=False,
        label=_('Time coverage'),
        help_text=_('Indicates the time period that the content of a resource'
                    ' is about'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    geographic_coverage = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500)]
        ),
        allow_null=True,
        required=False,
        label=_('Geographic coverage'),
        help_text=_('Indicates the geographic region that the content of a'
                    ' resource is about; for countries, recommended use of'
                    ' ISO-3166'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    temporal = PeriodOfTimeSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Temporal'),
        help_text=_('Temporal characteristics of the resource'),
    )

    class Meta:
        model = models.LanguageDescription
        fields = (
            'pk',
            'lr_type',
            'ld_subclass',
            'language_description_subclass',
            'requires_software',
            'required_hardware',
            'additional_hw_requirements',
            'running_environment_details',
            'unspecified_part',
            'language_description_media_part',
            'dataset_distribution',
            'personal_data_included',
            'personal_data_details',
            'sensitive_data_included',
            'sensitive_data_details',
            'anonymized',
            'anonymization_details',
            'is_analysed_by',
            'is_edited_by',
            'is_elicited_by',
            'is_converted_version_of',
            'time_coverage',
            'geographic_coverage',
            'temporal',
            'spatial',
        )
        extra_kwargs = {
            'additional_hw_requirements': {'style': {'recommended': True, }},
            'running_environment_details': {'style': {'max_length': 200, }},
            'personal_data_included': {'style': {'recommended': True, }},
            'personal_data_details': {'style': {'max_length': 500, }},
            'sensitive_data_included': {'style': {'recommended': True, }},
            'sensitive_data_details': {'style': {'max_length': 500, }},
            'anonymization_details': {'style': {'max_length': 1000, }},
            'time_coverage': {'style': {'max_length': 500, }},
            'geographic_coverage': {'style': {'max_length': 500, }},
        }
        to_display_false = [
            'lr_type',
        ]

    def validate(self, data):
        # for_information_only validation
        for_information_only = self.context.get('for_information_only', False)
        # language_description_media_part XOR unspecified_part for_information_only records
        if for_information_only:
            if data.get('unspecified_part', None) is not None and \
                    len(data.get('language_description_media_part', [])) != 0:
                raise serializers.ValidationError(
                    _(f"{self.fields['unspecified_part'].label} and "
                      f"{self.fields['language_description_media_part'].label} "
                      f"cannot be used together"),
                    code='invalid',
                )
            if data.get('unspecified_part', None) is None and \
                    len(data.get('language_description_media_part', [])) == 0:
                raise serializers.ValidationError(
                    _(f"Use either {self.fields['unspecified_part'].label} or"
                      f" {self.fields['language_description_media_part'].label}"),
                    code='invalid',
                )
        else:
            # unspecified_part is only for for_information_only records
            if data.get('unspecified_part', None) is not None \
                    and data.get('ld_subclass', None) != choices.LD_SUBCLASS_CHOICES.MODEL:
                raise serializers.ValidationError(
                    {'unspecified_part': _(f"{self.fields['unspecified_part'].label} "
                                           f"can be used only for information only records or for models.")},
                    code='invalid',
                )
            # unspecified_part is obligatory for LanguageDescription.model
            if data.get('unspecified_part', None) is None \
                    and data.get('ld_subclass', None) == choices.LD_SUBCLASS_CHOICES.MODEL:
                raise serializers.ValidationError(
                    {'unspecified_part': _(f"{self.fields['unspecified_part'].label} "
                                           f"cannot be blank.")},
                    code='invalid',
                )
        # if ld_subclass=model, then only language_description_subclass must be Model
        if data.get('ld_subclass', None) == choices.LD_SUBCLASS_CHOICES.MODEL \
                and (data.get('language_description_subclass', None)
                     and data['language_description_subclass'].get('ld_subclass_type', None) != 'Model'):
            raise serializers.ValidationError(
                {'language_description_subclass': _(f"ld_subclass_type "
                                                    f"must be a Model.")},
                code='invalid',
            )
        # if ld_subclass=grammar, then only language_description_subclass must be Grammar
        if data.get('ld_subclass', None) == choices.LD_SUBCLASS_CHOICES.GRAMMAR \
                and (data.get('language_description_subclass', None)
                     and data['language_description_subclass'].get('ld_subclass_type', None) != 'Grammar'):
            raise serializers.ValidationError(
                {'language_description_subclass': _(f"ld_subclass_type "
                                                    f"must be a Grammar.")},
                code='invalid',
            )
        # if ld_subclass=other, then language_description_subclass must be empty
        if data.get('ld_subclass', None) == choices.LD_SUBCLASS_CHOICES.OTHER \
                and (data.get('language_description_subclass', None) is not None):
            raise serializers.ValidationError(
                {'language_description_subclass': _(f"{self.fields['language_description_subclass'].label} "
                                                    f"must be empty.")},
                code='invalid',
            )

        if (data.get('personal_data_included', None) == choices.PERSONAL_DATA_INCLUDED_CHOICES.YES_P
            or data.get('sensitive_data_included', None) == choices.SENSITIVE_DATA_INCLUDED_CHOICES.YES_S) \
                and data.get('anonymized', None) is None:
            raise serializers.ValidationError(
                {'anonymized': _(f"{self.fields['anonymized'].label} cannot be blank.")},
                code='invalid',
            )
        if data.get('ld_subclass', None) == choices.LD_SUBCLASS_CHOICES.MODEL \
                and data.get('dataset_distribution', None):
            for d in data['dataset_distribution']:
                if d.get('distribution_unspecified_feature', None) is None:
                    raise serializers.ValidationError(
                        {'dataset_distribution': _(f"distribution_unspecified_feature"
                                                   f" cannot be blank for models.")},
                        code='invalid',
                    )
        if not for_information_only and data.get('ld_subclass', None) != choices.LD_SUBCLASS_CHOICES.MODEL \
                and data.get('dataset_distribution', None):
            for d in data['dataset_distribution']:
                if d.get('distribution_unspecified_feature', None) is not None:
                    raise serializers.ValidationError(
                        {'dataset_distribution': _(f"distribution_unspecified_feature"
                                                   f"can be used only for information only records or for models.")},
                        code='invalid',
                    )

        dataset_distribution_validation(data,
                                        media_part_field='language_description_media_part',
                                        media_type_field='ld_media_type',
                                        media_part='LanguageDescriptionTextPart',
                                        dataset_distribution_media_field='distribution_text_feature',
                                        message_feature='Text feature cannot be blank',
                                        message_part='Text feature cannot exist when text part does not.')
        dataset_distribution_validation(data,
                                        media_part_field='language_description_media_part',
                                        media_type_field='ld_media_type',
                                        media_part='LanguageDescriptionVideoPart',
                                        dataset_distribution_media_field='distribution_video_feature',
                                        message_feature='Video feature cannot be blank',
                                        message_part='Video feature cannot exist when video part does not.')
        dataset_distribution_validation(data,
                                        media_part_field='language_description_media_part',
                                        media_type_field='ld_media_type',
                                        media_part='LanguageDescriptionImagePart',
                                        dataset_distribution_media_field='distribution_image_feature',
                                        message_feature='Image feature cannot be blank',
                                        message_part='Image feature cannot exist when image part does not.')

        dataset_distribution_unspecified_part_validation(data)
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'spatial')
        return super().to_internal_value(data)


class SoftwareDistributionSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Any form with which software is distributed (e.g., web services,
    executable or code files, etc.)
    """
    software_distribution_form = serializers.ChoiceField(
        choices=choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES,
        label=_('Software distribution form'),
        help_text=_('The medium, delivery channel or form (e.g., source code,'
                    ' API, web service, etc.) through which a software object'
                    ' is distributed'),
    )
    # ManyToMany relation
    is_described_by = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Described in'),
        help_text=_('Links to a document that describes the contents or'
                    ' operation of a language resource'),
    )
    # ManyToMany relation
    licence_terms = GenericLicenceTermsSerializer(
        many=True,
        allow_empty=True,
        allow_null=False,
        label=_('Licence'),
        help_text=_('Links the distribution (distributable form) of a language'
                    ' resource to the licence or terms of use/service (a'
                    ' specific legal document) with which it is distributed'),
    )
    # OneToOne relation
    cost = CostSerializer(
        allow_null=True,
        required=False,
        label=_('Cost'),
        help_text=_('Introduces the cost for accessing a resource or the'
                    ' overall budget of a project, formally described as a set'
                    ' of amount and currency unit'),
    )
    attribution_text = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000)],
        allow_null=True,
        required=False,
        label=_('Attribution text'),
        help_text=_('The text that must be quoted for attribution purposes when'
                    ' using a resource - for cases where a resource is provided'
                    ' with a restriction on attribution'),
        read_only=True,
    )
    copyright_statement = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Copyright notice'),
        help_text=_('Introduces a free text statement that may be included with'
                    ' the language resource, usually containing the name(s) of'
                    ' copyright holders and licensing terms'),
    )
    # ManyToMany relation
    distribution_rights_holder = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Distribution rights holder'),
        help_text=_('Identifies a person or an organization that holds the'
                    ' distribution rights. The range and scope of distribution'
                    ' rights is defined in the distribution agreement. The'
                    ' distributor in most cases only has a limited licence to'
                    ' distribute the work and collect royalties on behalf of'
                    ' the licensor or the IPR holder and cannot give to any'
                    ' recipient of the work permissions that exceed the scope'
                    ' of the distribution agreement (e.g., to allow uses of the'
                    ' work that are not defined in the distribution agreement)'),
    )
    # ManyToMany relation
    access_rights = AccessRightsStatementSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Access rights'),
        help_text=_('Specifies the rights for accessing the distributable'
                    ' form(s) of a language resource (preferrably in accordance'
                    ' to a formalised vocabulary)'),
    )
    # FK relation
    package = ContentFileSerializer(
        allow_null=True,
        required=False
    )

    class Meta:
        model = models.SoftwareDistribution
        fields = (
            'pk',
            'software_distribution_form',
            'execution_location',
            'download_location',
            'docker_download_location',
            'service_adapter_download_location',
            'access_location',
            'demo_location',
            'private_resource',
            'is_described_by',
            'additional_hw_requirements',
            'command',
            'web_service_type',
            'operating_system',
            'package_format',
            'licence_terms',
            'cost',
            'membership_institution',
            'attribution_text',
            'copyright_statement',
            'availability_start_date',
            'availability_end_date',
            'distribution_rights_holder',
            'access_rights',
            'package',
            'elg_compatible_distribution',
            'initial_docker_download_location',
            'initial_service_adapter_download_location'
        )
        read_only_fields = [
            'initial_docker_download_location',
            'initial_service_adapter_download_location'
        ]
        extra_kwargs = {
            'demo_location': {'style': {'recommended': True, }},
            'private_resource': {'style': {'recommended': True, }},
            'is_described_by': {'style': {'recommended': True, }},
            'additional_hw_requirements': {'style': {'recommended': True, }},
            'cost': {'style': {'recommended': True, }},
            'membership_institution': {'style': {'recommended': True, }},
            'attribution_text': {'style': {'max_length': 1000, }},
            'copyright_statement': {'style': {'max_length': 3000, }},
        }

    def validate(self, data):
        # for_information_only validation
        for_information_only = self.context.get('for_information_only', False)
        if for_information_only is False:
            # licence_terms is obligatory for not for-information-only records
            if len(data.get('licence_terms', [])) == 0:
                raise serializers.ValidationError(
                    {'licence_terms': _(f"{self.fields['licence_terms'].label}"
                                        f" may not be empty.")},
                    code='invalid',
                )

        else:
            # access_rights is obligatory for for-information-only records
            if len(data.get('access_rights', [])) == 0 and not data.get('licence_terms', None):
                raise serializers.ValidationError(
                    {'access_rights': _(f"{self.fields['access_rights'].label}"
                                        f" may not be empty.")},
                    code='invalid',
                )

        xml_upload_with_data = self.context.get('xml_upload_with_data')
        # docker_download_location, access_location, execution_location and download_location
        # are obligatory upon conditions (distribution form value)
        functional_service = self.context.get('functional_service')
        # DockerImage distribution form - for functional software
        if functional_service and data.get('software_distribution_form',
                                           None) == choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE:
            # docker_download_location is obligatory
            if not data.get('docker_download_location', None):
                raise serializers.ValidationError(
                    {'docker_download_location': _(
                        f"{self.fields['docker_download_location'].label} cannot be blank.")},
                    code='invalid',
                )
        # DockerImage distribution form - for non functional software
        elif data.get('software_distribution_form',
                      None) == choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE \
                and not data.get('docker_download_location', None):
            raise serializers.ValidationError(
                {'docker_download_location': _(
                    f"{self.fields['docker_download_location'].label} cannot be blank.")},
                code='invalid',
            )
        # WebService distribution form
        if data.get('software_distribution_form',
                    None) == choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WEB_SERVICE \
                and all([data.get('execution_location', '') == '',
                         data.get('access_location', '') == '']):
            raise serializers.ValidationError(
                {'non_field_error': _(
                    f"{self.fields['execution_location'].label} or {self.fields['access_location'].label} cannot be blank.")},
                code='invalid',
            )
        # Other or Unspecified distribution form
        if data.get('software_distribution_form',
                    None) in [choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.OTHER,
                              choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.UNSPECIFIED] \
                and all([data.get('download_location', '') == '',
                         data.get('access_location', '') == '']):
            raise serializers.ValidationError(
                {'non_field_error': _(
                    f"{self.fields['download_location'].label} or {self.fields['access_location'].label} cannot be blank.")},
                code='invalid',
            )
        if not xml_upload_with_data:
            # ExecutableCode distribution form
            if data.get('software_distribution_form',
                        None) == choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.EXECUTABLE_CODE \
                    and all([data.get('download_location', '') == '',
                             data.get('access_location', '') == '',
                             data.get('execution_location', '') == '',
                             data.get('package', None) is None]):
                raise serializers.ValidationError(
                    {'non_field_error': _(
                        f"{self.fields['download_location'].label}, {self.fields['access_location'].label}, "
                        f"{self.fields['execution_location'].label} cannot be blank"
                        f" or data must have been related with this distribution."
                    )},
                    code='invalid',
                )
            # Rest distributions form
            if data.get('software_distribution_form',
                        None) in [choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.LIBRARY,
                                  choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.PLUGIN,
                                  choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE,
                                  choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_CODE,
                                  choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WORKFLOW_FILE] \
                    and all([data.get('download_location', '') == '',
                             data.get('access_location', '') == '',
                             data.get('package', None) is None]):
                raise serializers.ValidationError(
                    {'non_field_error': _(
                        f"{self.fields['download_location'].label}, {self.fields['access_location'].label} cannot be blank"
                        f" or data must have been related with this distribution."
                    )},
                    code='invalid',
                )

        # web_service_type obligatory upon condition
        if data.get('software_distribution_form',
                    None) == choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WEB_SERVICE and \
                not data.get('web_service_type', None):
            raise serializers.ValidationError(
                {'web_service_type': _(
                    f"{self.fields['web_service_type'].label} cannot be blank.")},
                code='invalid',
            )
        # private_resource obligatory upon condition
        if data.get('software_distribution_form',
                    None) == choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE and \
                data.get('private_resource', None) is None:
            raise serializers.ValidationError(
                {'private_resource': _(
                    f"{self.fields['private_resource'].label} cannot be blank.")},
                code='invalid',
            )
        return data

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'execution_location')
        data = encode_iri_to_uri(data, 'download_location')
        data = encode_iri_to_uri(data, 'access_location')
        data = encode_iri_to_uri(data, 'demo_location')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        record_id = self.context.get('record_id')
        if instance.private_resource is not False:
            instance.docker_download_location = HIDDEN_VALUE_STR
            instance.service_adapter_download_location = HIDDEN_VALUE_STR
            instance.download_location = HIDDEN_VALUE_URL
            instance.execution_location = HIDDEN_VALUE_URL
        if instance.package is not None:
            record_id = h_encode(record_id)
            dist_pk = h_encode(instance.pk)
            instance.download_location = f'{settings.DJANGO_URL}/{settings.HOME_PREFIX}/storage/{record_id}/{dist_pk}/'
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        return representation

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        uncensored = self.context.get('uncensored')
        if instance.private_resource is not False \
                and not uncensored:
            instance.docker_download_location = HIDDEN_VALUE_STR
            instance.service_adapter_download_location = HIDDEN_VALUE_STR
            instance.download_location = HIDDEN_VALUE_URL
            instance.execution_location = HIDDEN_VALUE_URL
        ret = super().to_display_representation(instance)
        # add pk to software distribution display representation
        if instance:
            ret.update({'pk': instance.pk})
            ret.move_to_end('pk', last=False)
        return ret


class SampleSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Introduces a combination of the sample text(s) or sample file(s)
    and optional tags that can be used for feeding a processing
    service for testing purposes
    """

    class Meta:
        model = models.Sample
        fields = (
            'pk',
            'sample_text',
            'samples_location',
            'tag',
        )
        extra_kwargs = {
            'sample_text': {'style': {'recommended': True, }},
            'samples_location': {'style': {'recommended': True, }},
            'tag': {'style': {'recommended': True, }},
        }

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'samples_location')
        return super().to_internal_value(data)


class ProcessingResourceSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    """
    processing_resource_type = serializers.ChoiceField(
        choices=choices.PROCESSING_RESOURCE_TYPE_CHOICES,
        label=_('Processing resource type'),
        help_text=_('Specifies the resource type that a tool/service takes as'
                    ' input or produces as output'),
    )
    # ManyToMany relation
    language = LanguageSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    # Reverse FK relation
    sample = SampleSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Sample'),
        help_text=_('Introduces a combination of the sample text(s) or sample'
                    ' file(s) and optional tags that can be used for feeding a'
                    ' processing service for testing purposes'),
    )
    # FK relation
    typesystem = GenericLanguageResourceSerializer(
        allow_null=True,
        required=False,
        label=_('Typesystem'),
        help_text=_('Specifies the typesystem (preferrably through an'
                    ' identifier or URL) that has been used for the annotation'
                    ' of a resource or that is required for the input resource'
                    ' of a tool/service or that should be used (dependency) for'
                    ' the annotation or used in the training of a ML model'),
    )
    # ManyToMany relation
    annotation_schema = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation schema'),
        help_text=_('The annotation schema used for annotating a resource or'
                    ' for the input resource (if annotated) of a tool/service'
                    ' or that should be used (dependency) for the annotation'),
    )
    # FK relation
    annotation_resource = GenericLanguageResourceSerializer(
        allow_null=True,
        required=False,
        label=_('Annotation resource'),
        help_text=_('Specifies the annotation resource used for annotating a'
                    ' corpus or that has been used for the input resource of a'
                    ' tool/service or that should be used (dependency) for the'
                    ' annotation'),
    )
    modality_type_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Modality type details'),
        help_text=_('Provides further information on the modalities represented'
                    ' in a language resource'),
    )

    class Meta:
        model = models.ProcessingResource
        fields = (
            'pk',
            'processing_resource_type',
            'language',
            'media_type',
            'data_format',
            'mimetype',
            'character_encoding',
            'sample',
            'annotation_type',
            'segmentation_level',
            'typesystem',
            'annotation_schema',
            'annotation_resource',
            'modality_type',
            'modality_type_details',
        )
        extra_kwargs = {
            'media_type': {'style': {'recommended': True, }},
            'data_format': {'style': {'recommended': True, }},
            'sample': {'style': {'recommended': True, }},
            'annotation_type': {'style': {'recommended': True, }},
            'modality_type_details': {'style': {'max_length': 500, }},
        }

    # DONE -> Moved to parent (ToolServiceSerializer)
    def validate(self, data):
        unique_language_validation(data, 'language')
        return data

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'mimetype')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        annotation_type_xml_field_name = self.Meta.model.schema_fields['annotation_type']
        if representation.get(annotation_type_xml_field_name, None):
            representation[annotation_type_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[annotation_type_xml_field_name], choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES,
                'ms:annotationTypeRecommended', 'ms:annotationTypeOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        ret['annotation_type'] = super().from_xml_representation_for_recommended_cv(ret['annotation_type'],
                                                                                    'annotation_type',
                                                                                    choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES,
                                                                                    'ms:annotationTypeRecommended',
                                                                                    'ms:annotationTypeOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        ret['annotation_type'] = super().to_display_representation_for_recommended_cv(
            ret['annotation_type'], choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES)
        return ret


class PerformanceIndicatorSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Describes the performance indicator used for the evaluation of a
    tool/service
    """
    metric = serializers.ChoiceField(
        choices=choices.METRIC_CHOICES,
        label=_('Metric'),
        help_text=_('Identifies the metric used for the evaluation of the'
                    ' tool/service'),
    )
    measure = serializers.FloatField(
        label=_('Measure'),
        help_text=_('Defines the measure calculated for the metric'),
    )
    unit_of_measure_metric = serializers.CharField(
        trim_whitespace=True,
        max_length=100,
        label=_('Unit of measure'),
        help_text=_('Identifies the unit of measure used in the calculation of'
                    ' the metric (for the evaluation of a tool/service)'),
    )

    class Meta:
        model = models.PerformanceIndicator
        fields = (
            'pk',
            'metric',
            'measure',
            'unit_of_measure_metric',
        )


class EvaluationSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Provides information on the evaluation process and results for a
    tool/service
    """
    # Reverse FK relation
    performance_indicator = PerformanceIndicatorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Performance indicator'),
        help_text=_('Describes the performance indicator used for the'
                    ' evaluation of a tool/service'),
    )
    # ManyToMany relation
    evaluation_report = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Evaluation report'),
        help_text=_('Links to a report describing the evaluation process, tool,'
                    ' method, etc. of the tool or service'),
    )
    # ManyToMany relation
    is_evaluated_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Evaluation tool'),
        help_text=_('Links to a tool/service B that has been used to evaluate'
                    ' LR A (the one being described)'),
    )
    evaluation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Evaluation details'),
        help_text=_('Provides further information on the evaluation process of'
                    ' a tool or service'),
    )
    # ManyToMany relation
    evaluator = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Evaluator'),
        help_text=_('Describes the person or organization that evaluated the'
                    ' tool or service'),
    )

    class Meta:
        model = models.Evaluation
        fields = (
            'pk',
            'evaluation_level',
            'evaluation_type',
            'evaluation_criterion',
            'evaluation_measure',
            'gold_standard_location',
            'performance_indicator',
            'evaluation_report',
            'is_evaluated_by',
            'evaluation_details',
            'evaluator',
        )
        extra_kwargs = {
            'evaluation_measure': {'style': {'recommended': True, }},
            'gold_standard_location': {'style': {'recommended': True, }},
            'performance_indicator': {'style': {'recommended': True, }},
            'evaluation_report': {'style': {'recommended': True, }},
            'evaluation_details': {'style': {'max_length': 500, }},
        }

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'gold_standard_location')
        return super().to_internal_value(data)


class EnumerationValueSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Introduces a value of a list used inside parameters
    """
    value_name = serializers.CharField(
        trim_whitespace=True,
        max_length=None,
        label=_('Value name'),
        help_text=_('Introduces a free text used to identify the value of a'
                    ' list inside parameters'),
    )
    value_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Value label'),
        help_text=_('Introduces a short free text used to show to the user for'
                    ' the value of a list used in parameters'),
    )
    value_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500), validate_dict_existence],
        label=_('Value description'),
        help_text=_('Introduces a short free text used to show to the user as a'
                    ' tip for the value of a list used in parameters'),
    )

    class Meta:
        model = models.EnumerationValue
        fields = (
            'pk',
            'value_name',
            'value_label',
            'value_description',
        )
        extra_kwargs = {
            'value_label': {'style': {'max_length': 150, }},
            'value_description': {'style': {'max_length': 500, 'recommended': True, }},
        }


class ParameterSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Introduces a parameter used for running a tool/service
    """
    parameter_name = serializers.CharField(
        trim_whitespace=True,
        max_length=100,
        label=_('Parameter name'),
        help_text=_('Introduces the name of the paramter as sent to a'
                    ' processing service'),
    )
    parameter_label = SerializersMultilingualField(
        validators=[validate_dict_value_length(150), validate_dict_existence],
        label=_('Parameter label'),
        help_text=_('Introduces a short name for a parameter suitable for use'
                    ' as a field label in a user interface'),
    )
    parameter_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(100), validate_dict_existence],
        label=_('Parameter description'),
        help_text=_('Provides a short account of he parameter (e.g., function'
                    ' it performs, input / output requirements, etc.) in free'
                    ' text'),
    )
    parameter_type = serializers.ChoiceField(
        choices=choices.PARAMETER_TYPE_CHOICES,
        label=_('Parameter type'),
        help_text=_('Classifies the parameter according to a specific (not yet'
                    ' standardised) typing system (e.g., whether it\'s boolean,'
                    ' string, integer, a document, mapping, etc.)'),
    )
    optional = serializers.BooleanField(
        label=_('Optional'),
        help_text=_('Specifies whether the parameter should be treated as'
                    ' mandatory or optional by user interfaces'),
    )
    multi_value = serializers.BooleanField(
        label=_('Multi-value'),
        help_text=_('Specifies whether the parameter takes a list of values'),
    )
    # Reverse FK relation
    enumeration_value = EnumerationValueSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Enumeration value'),
        help_text=_('Introduces a value of a list used inside parameters'),
    )

    class Meta:
        model = models.Parameter
        fields = (
            'pk',
            'parameter_name',
            'parameter_label',
            'parameter_description',
            'parameter_type',
            'optional',
            'multi_value',
            'default_value',
            'data_format',
            'enumeration_value',
        )
        extra_kwargs = {
            'parameter_label': {'style': {'max_length': 150, }},
            'parameter_description': {'style': {'max_length': 100, }},
            'enumeration_value': {'style': {'recommended': True, }},
        }

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        data_format_xml_field_name = self.Meta.model.schema_fields['data_format']
        if representation.get(data_format_xml_field_name, None):
            representation[data_format_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[data_format_xml_field_name], choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                'ms:dataFormatRecommended', 'ms:dataFormatOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['data_format'] = super().from_xml_representation_for_recommended_cv(ret['data_format'],
                                                                                'data_format',
                                                                                choices.DATA_FORMAT_RECOMMENDED_CHOICES,
                                                                                'ms:dataFormatRecommended',
                                                                                'ms:dataFormatOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['data_format'] = super().to_display_representation_for_recommended_cv(
            ret['data_format'], choices.DATA_FORMAT_RECOMMENDED_CHOICES)
        return ret


class ToolServiceSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A tool/service/any piece of software that performs language
    processing and/or any Language Technology related operation.
    """
    function = serializers.ListField(
        child=serializers.CharField(
            max_length=200),
        allow_empty=False,
        label=_('Function'),
        help_text=_('Specifies the operation/function/task that a software'
                    ' object performs'),
    )
    # Reverse FK relation
    software_distribution = SoftwareDistributionSerializer(
        many=True,
        allow_empty=False,
        label=_('Software distribution'),
        help_text=_('Any form with which software is distributed (e.g., web'
                    ' services, executable or code files, etc.)'),
    )
    language_dependent = serializers.BooleanField(
        label=_('Language dependent'),
        help_text=_('Indicates whether the operation of the tool or service is'
                    ' language dependent or not'),
    )
    # Reverse FK relation
    input_content_resource = ProcessingResourceSerializer(
        many=True,
        allow_empty=False,
        label=_('Input content resource'),
        help_text=_('Specifies the requirements set by a tool/service for the'
                    ' (content) resource that it processes'),
    )
    # Reverse FK relation
    output_resource = ProcessingResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Output resource'),
        help_text=_('Specifies the output results of a tool/service, i.e. the'
                    ' features of the processed (content) resource'),
    )
    # ManyToMany relation
    requires_lr = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Required LRT'),
        help_text=_('Links to LR B that is required for the operation of a'
                    ' tool/service or computational grammar (the one that is'
                    ' described)'),
    )
    # FK relation
    typesystem = GenericLanguageResourceSerializer(
        allow_null=True,
        required=False,
        label=_('Typesystem'),
        help_text=_('Specifies the typesystem (preferrably through an'
                    ' identifier or URL) that has been used for the annotation'
                    ' of a resource or that is required for the input resource'
                    ' of a tool/service or that should be used (dependency) for'
                    ' the annotation or used in the training of a ML model'),
    )
    # ManyToMany relation
    annotation_schema = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation schema'),
        help_text=_('The annotation schema used for annotating a resource or'
                    ' for the input resource (if annotated) of a tool/service'
                    ' or that should be used (dependency) for the annotation'),
    )
    # ManyToMany relation
    annotation_resource = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation resource'),
        help_text=_('Specifies the annotation resource used for annotating a'
                    ' corpus or that has been used for the input resource of a'
                    ' tool/service or that should be used (dependency) for the'
                    ' annotation'),
    )
    # ManyToMany relation
    ml_model = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('ML model'),
        help_text=_('Specifies the ML model that must be used together with the'
                    ' tool/service to perform the desired task'),
    )
    formalism = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Formalism'),
        help_text=_('Specifies the formalism (bibliographic reference, URL,'
                    ' name) used for the creation/enrichment of the resource'
                    ' (grammar or tool/service)'),
    )
    running_environment_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(200)],
        allow_null=True,
        required=False,
        label=_('Running environment details'),
        help_text=_('Provides further information on the running environment of'
                    ' a tool/service or of a language description'),
    )
    # Reverse FK relation
    evaluation = EvaluationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Evaluation'),
        help_text=_('Provides information on the evaluation process and results'
                    ' for a tool/service'),
    )
    # Reverse FK relation
    parameter = ParameterSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Parameter'),
        help_text=_('Introduces a parameter used for running a tool/service'),
    )

    class Meta:
        model = models.ToolService
        fields = (
            'pk',
            'lr_type',
            'function',
            'software_distribution',
            'language_dependent',
            'input_content_resource',
            'output_resource',
            'requires_lr',
            'typesystem',
            'annotation_schema',
            'annotation_resource',
            'ml_model',
            'development_framework',
            'framework',
            'formalism',
            'method',
            'implementation_language',
            'requires_software',
            'required_hardware',
            'running_environment_details',
            'running_time',
            'trl',
            'evaluated',
            'evaluation',
            'previous_annotation_types_policy',
            'parameter',
        )
        extra_kwargs = {
            'output_resource': {'style': {'recommended': True, }},
            'typesystem': {'style': {'recommended': True, }},
            'annotation_schema': {'style': {'recommended': True, }},
            'annotation_resource': {'style': {'recommended': True, }},
            'ml_model': {'style': {'recommended': True, }},
            'development_framework': {'style': {'recommended': True, }},
            'formalism': {'style': {'max_length': 100, }},
            'implementation_language': {'style': {'recommended': True, }},
            'running_environment_details': {'style': {'max_length': 200, }},
            'trl': {'style': {'recommended': True, }},
            'evaluated': {'style': {'recommended': True, }},
            'parameter': {'style': {'recommended': True, }},
        }
        to_display_false = [
            'lr_type',
        ]

    # DONE - Validation for language_dependent
    def validate(self, data):
        self._errors = dict()
        if data.get('language_dependent', None) and data.get(
                'input_content_resource', None):
            for input_data in data.get('input_content_resource', []):
                if not input_data.get('language', None):
                    self._errors['input_content_resource'] = {
                        'language': _('Language cannot be blank')}
            if data.get('output_resource', None):
                for output_data in data.get('output_resource', []):
                    if not output_data.get('language'):
                        self._errors['output_resource'] = {
                            'language': _('Language cannot be blank')}
        # if functional_service=True, there must be one SoftwareDistribution
        # with software_distribution_form=DockerImage
        functional_service = self.context.get('functional_service')
        if functional_service and data.get('software_distribution', None) is not None:
            dockerImage = False
            for dist in data['software_distribution']:
                if dist['software_distribution_form'] == choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE:
                    dockerImage = True
            if dockerImage is False:
                raise serializers.ValidationError(
                    {'software_distribution': _(
                        f"There must be one {self.fields['software_distribution'].label} with "
                        f"{SoftwareDistributionSerializer().fields['software_distribution_form'].label}="
                        f"{choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.DOCKER_IMAGE}"
                    )},
                    code='invalid',
                )
        if self._errors:
            raise serializers.ValidationError(self._errors)
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        # function
        function_xml_field_name = self.Meta.model.schema_fields['function']
        representation[function_xml_field_name] = super().to_xml_representation_for_recommended_cv(
            representation[function_xml_field_name], choices.LT_CLASS_RECOMMENDED_CHOICES,
            'ms:LTClassRecommended', 'ms:LTClassOther'
        )
        # development_framework
        dev_framework_xml_field_name = self.Meta.model.schema_fields['development_framework']
        if representation.get(dev_framework_xml_field_name):
            representation[dev_framework_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[dev_framework_xml_field_name], choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES,
                'ms:DevelopmentFrameworkRecommended', 'ms:DevelopmentFrameworkOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['function'] = super().from_xml_representation_for_recommended_cv(ret['function'],
                                                                             'function',
                                                                             choices.LT_CLASS_RECOMMENDED_CHOICES,
                                                                             'ms:LTClassRecommended', 'ms:LTClassOther')
        if ret.get('development_framework'):
            ret['development_framework'] = super().from_xml_representation_for_recommended_cv(
                ret['development_framework'],
                'development_framework', choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES,
                'ms:DevelopmentFrameworkRecommended', 'ms:DevelopmentFrameworkOther'
            )
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['function'] = super().to_display_representation_for_recommended_cv(
            ret['function'], choices.LT_CLASS_RECOMMENDED_CHOICES)
        if ret.get('development_framework'):
            ret['development_framework'] = super().to_display_representation_for_recommended_cv(
                ret['development_framework'], choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES)
        return ret


class LRSubclassSerializer(
    PolymorphicSerializer, WritableNestedModelSerializer,
    XMLPolymorphicSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Introduces the type of language resource that is described in
    the metadata record
    """

    class Meta:
        model = models.LRSubclass
        fields = '__all__'

    model_serializer_mapping = {
        models.Corpus: CorpusSerializer,
        models.LexicalConceptualResource: LexicalConceptualResourceSerializer,
        models.LanguageDescription: LanguageDescriptionSerializer,
        models.ToolService: ToolServiceSerializer
    }

    resource_type_field_name = 'lr_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _('LR type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]


class LanguageResourceSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A resource composed of linguistic material used in the
    construction, improvement and/or evaluation of language
    processing applications, but also, in a broader sense, in
    language and language-mediated research studies and
    applications; the term is used with a broader meaning,
    encompassing (a) data sets (textual, multimodal/multimedia and
    lexical data, grammars, language models, etc.) in machine
    readable form, and (b) tools/technologies/services used for
    their processing and management.
    """
    resource_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000), validate_dict_existence],
        label=_('Resource name'),
        help_text=_('Introduces a human-readable name or title by which the'
                    ' resource is known'),
    )
    resource_short_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Resource short name'),
        help_text=_('Introduces a short form (e.g., abbreviation, acronym,'
                    ' etc.) used to refer to a language resource'),
    )
    description = SerializersMultilingualField(
        validators=[validate_dict_value_length(10000), validate_dict_existence],
        label=_('Description'),
        help_text=_('Introduces a short free-text account that provides'
                    ' information about the resource (e.g., function, contents,'
                    ' technical information, etc.)'),
    )
    # Reverse FK relation
    lr_identifier = LRIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Language Resource identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a language resource'),
    )
    version = SerializersVersionField(
        max_length=50,
        default='1.0.0 (automatically assigned)',
        required=False,
        label=_('Version'),
        help_text=_('Associates a language resource with a pattern that'
                    ' indicates its version; the recommended way is to follow'
                    ' the semantic versioning guidelines (http://semver.org)'
                    ' and use a numeric pattern of the form'
                    ' major_version.minor_version.patch'),
    )
    update_frequency = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Update frequency'),
        help_text=_('Specifies the frequency with which the resource is'
                    ' updated'),
    )
    revision = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Revision'),
        help_text=_('Provides an account of the revisions in free text or a'
                    ' link to a document with revisions'),
    )
    # ManyToMany relation
    additional_info = additionalInfoSerializer(
        many=True,
        allow_empty=False,
        label=_('Additional information'),
        help_text=_('Introduces a point that can be used for further'
                    ' information (e.g. a landing page with a more detailed'
                    ' description of the resource or a general email that can'
                    ' be contacted for further queries)'),
    )
    # ManyToMany relation
    contact = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Contact'),
        help_text=_('Specifies the data of the person/organization/group that'
                    ' can be contacted for information about a language'
                    ' resource'),
    )
    citation_text = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000)],
        allow_null=True,
        required=False,
        label=_('Citation text'),
        help_text=_('Provides the text that must be quoted for citation'
                    ' purposes when using a language resource'),
        read_only=True,
    )
    citation_all_versions = serializers.SerializerMethodField(read_only=True)
    # ManyToMany relation
    ipr_holder = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('IPR holder'),
        help_text=_('A person or an organization who holds the full'
                    ' Intellectual Property Rights (Copyright, trademark, etc.)'
                    ' that subsist in the resource. The IPR holder could be'
                    ' different from the creator that may have assigned the'
                    ' rights to the IPR holder (e.g., an author as a creator'
                    ' assigns her rights to the publisher who is the IPR'
                    ' holder) and the distributor that holds a specific licence'
                    ' (i.e. a permission) to distribute the work via a specific'
                    ' distributor.'),
    )
    keyword = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100), validate_dict_existence]
        ),
        allow_empty=False,
        label=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    domain = DomainSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    # ManyToMany relation
    subject = SubjectSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Subject'),
        help_text=_('Specifies the subject/topic of a language resource'),
    )
    # ManyToMany relation
    resource_provider = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Resource provider'),
        help_text=_('The person/organization responsible for providing,'
                    ' curating, maintaining and making available (publishing)'
                    ' the resource'),
    )
    # ManyToMany relation
    resource_creator = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Resource creator'),
        help_text=_('Links a resource to the person, group or organisation that'
                    ' has created the resource'),
    )
    # ManyToMany relation
    funding_project = GenericProjectSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Funded by'),
        help_text=_('Links a language resource to the project that has funded'
                    ' its creation, enrichment, extension, etc.'),
    )
    creation_details = SerializersMultilingualField(
        validators=[validate_dict_value_length(3000)],
        allow_null=True,
        required=False,
        label=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    # ManyToMany relation
    has_original_source = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    # ManyToMany relation
    is_created_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    # Reverse FK relation
    actual_use = ActualUseSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Actual use'),
        help_text=_('Provides information on how the resource has been used'
                    ' (e.g., in a project, for a publication, for testing a'
                    ' tool/service, etc.)'),
    )
    # Reverse FK relation
    validation = ValidationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Validation'),
        help_text=_('Provides information on each of the the validation'
                    ' procedure(s) a language resource may have undergone'
                    ' (e.g., formal validation of the whole resource, content'
                    ' validation of one part of the resource, etc.).'),
    )
    # ManyToMany relation
    is_documented_by = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Documented in'),
        help_text=_('Links a language resource to a document (e.g., research'
                    ' paper describing its contents or its use in a project,'
                    ' user manual, etc.) or any other form of documentation'
                    ' (e.g., a URL with support information) that is related to'
                    ' the resource'),
    )
    # ManyToMany relation
    is_described_by = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Described in'),
        help_text=_('Links to a document that describes the contents or'
                    ' operation of a language resource'),
    )
    # ManyToMany relation
    is_to_be_cited_by = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Preferred citation'),
        help_text=_('Links to the publication that must be used when citing the'
                    ' language resource'),
    )
    # ManyToMany relation
    is_cited_by = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Cited in'),
        help_text=_('Links to a publication that cites the language resource'
                    ' described'),
    )
    # ManyToMany relation
    is_reviewed_by = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Reviewed in'),
        help_text=_('Links to a document that contains the review of LR A (the'
                    ' one being described)'),
    )
    # ManyToMany relation
    is_part_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Part of'),
        help_text=_('Links to LR B which contains LR A (the one being'
                    ' described), e.g., a bilingual corpus that includes a'
                    ' monolingual corpus'),
    )
    # ManyToMany relation
    is_part_with = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Part with'),
        help_text=_('Links to LR B that together with LR A (the one being'
                    ' described) are parts of LR C'),
    )
    # ManyToMany relation
    is_similar_to = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Similar to'),
        help_text=_('Links to LR B that bears resemblances to LR A (the one'
                    ' being described), e.g., they have been built with the'
                    ' same theoretical principles or are the same with'
                    ' different formats or processed at the same level with'
                    ' different tools'),
    )
    # ManyToMany relation
    is_exact_match_with = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Exact match with'),
        help_text=_('Links to LR B that has the same contents with LR A; they'
                    ' may have different names or the same name and be stored'
                    ' on different locations'),
    )
    # ManyToMany relation
    is_potential_duplicate_with = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Potential duplicate with'),
        help_text=_('Links to LR B that may have the same contents with LR A;'
                    ' for instance, they have the same name but they are stored'
                    ' at different locations or have slightly different'
                    ' descriptions'),
    )
    # Reverse FK relation
    has_metadata = GenericMetadataRecordSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Other metadata record'),
        help_text=_('Links to an external metadata record that describes the'
                    ' same language resource in another catalogue, repository,'
                    ' etc.'),
    )
    # ManyToMany relation
    is_archived_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Archiving tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for archiving LR A (the one being described)'),
    )
    # ManyToMany relation
    is_continuation_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Continuation of'),
        help_text=_('Links to LR B that forms the basis of LR A (the one being'
                    ' described) upon which it has continued to extend /'
                    ' enrich'),
    )
    # ManyToMany relation
    replaces = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Previous versions'),
        help_text=_('Links to LR B that is an older version of LR A (the one'
                    ' being described) and has been replaced by it'),
    )
    # ManyToMany relation
    is_related_to_lr = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Related to Language Resource/Technology'),
        help_text=_('Links to a language resource that holds a relation with'
                    ' the entity being described (without further specification'
                    ' of the relation type)'),
    )
    # FK relation
    is_version_of = GenericLanguageResourceSerializer(
        allow_null=True,
        required=False,
        label=_('Version of'),
        help_text=_('Links to LR B that is a version (corrected, annotated,'
                    ' enriched, processed, etc.) of LR A (the one being'
                    ' described)'),
    )
    # Reverse FK relation
    relation = RelationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Relation'),
        help_text=_('Links a language resource to other related resources'
                    ' specifying also the type of relation'),
    )
    # ManyToMany relation
    qualified_attribution = AttributionSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Attribution'),
        help_text=_('Used for relations to agents not covered by the classic'
                    ' relations (e.g., funders, contributors, etc.)'),
    )
    # ManyToMany relation
    is_compatible_with = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Compatible with'),
        help_text=_('Links to a LR B that can be used together with LR A (the'
                    ' one being described), e.g., a lexicon that is compatible'
                    ' with a grammar'),
    )
    # ManyToMany relation
    has_part = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Parts'),
        help_text=_('Links to LR B which is contained in LR A (the one being'
                    ' described), e.g., a monolingual corpus part of a'
                    ' bilingual corpus'),
    )
    # ManyToMany relation
    has_version = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Versions'),
        help_text=_('Links to LR B that is a version of LR A (the one being'
                    ' described)'),
    )
    # ManyToMany relation
    is_continued_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Continued by'),
        help_text=_('Links to a LR B that extends / continues / enriches LR A'
                    ' (the one being described)'),
    )
    # ManyToMany relation
    is_replaced_with = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('New version'),
        help_text=_('Links to LR B that is a newer version of LR A (the one'
                    ' being described) and replaces it'),
    )
    # ManyToMany relation
    has_outcome = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Outcomes'),
        help_text=_('Links to LR B which is created / extracted from LR A (the'
                    ' one being described), i.e. LR A has been used as the'
                    ' basis/initial/source material for LR B'),
    )
    # ManyToMany relation
    has_aligned_version = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Aligned versions'),
        help_text=_('Links to a corpus B which contains an aligned version of'
                    ' corpus A (the one being described)'),
    )
    # ManyToMany relation
    has_annotated_version = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotated versions'),
        help_text=_('Links to a corpus B which contains the annotations of'
                    ' corpus A (the one being described)'),
    )
    # ManyToMany relation
    has_converted_version = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Converted versions'),
        help_text=_('Links to LR B that has been used for creating LR A (the'
                    ' one being described) through a conversion procedure,'
                    ' e.g., a PDF to text conversion'),
    )
    # ManyToMany relation
    has_archived = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Archived Language Resources and Technologies'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for archiving LR A (the one being described)'),
    )
    # ManyToMany relation
    has_evaluated = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Evaluated Language Resources and Technologies'),
        help_text=_('Links a tool/service that has been used for evaluating an'
                    ' LR A to the data of this evaluation'),
    )
    # ManyToMany relation
    has_elicited = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Elicited Language Resources and Technologies'),
        help_text=_('Links a tool/service B that is (or can be or has been)'
                    ' used for eliciting LR A to LR A'),
    )
    # ManyToMany relation
    has_created = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Created Language Resources and Technologies'),
        help_text=_('Links a tool/service B that is (or can be or has been)'
                    ' used for creating LR A to LR A'),
    )
    # ManyToMany relation
    has_annotated = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotated Language Resources and Technologies'),
        help_text=_('Links a tool/service B that has been used for annotating'
                    ' LR A (e.g., a tagger, NER, etc.) to LR A'),
    )
    # ManyToMany relation
    has_analysed = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Analysed Language Resources and Technologies'),
        help_text=_('Links a tool/service B that is (or can be or has been)'
                    ' used for analysing LR A (e.g., a statistical tool) to LR'
                    ' A'),
    )
    # ManyToMany relation
    has_validated = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Validated Language Resources and Technologies'),
        help_text=_('Links a tool/service B that is (or can be) used for'
                    ' validating LR A to the data of this validation'),
    )
    # ManyToMany relation
    accesses = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Accesses'),
        help_text=_('Links a tool/service B that is (or can be) used for'
                    ' accessing LR A (e.g., a corpus workbench, s/w for lexicon'
                    ' access) to the LR A'),
    )
    # ManyToMany relation
    displays = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Displays'),
        help_text=_('Links a tool/service B that is (or can be) used to display'
                    ' LR A (e.g., a tool for visualizing relations in a'
                    ' lexicon, or annotations in a corpus) to the LR A'),
    )
    # ManyToMany relation
    edits = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Edits'),
        help_text=_('Links a tool/service B that is (or can be) used for'
                    ' editing LR A (the one being described) to LR A'),
    )
    # ManyToMany relation
    queries = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Query services'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for querying LR A (the one being described)'),
    )
    # ManyToMany relation
    is_typesystem_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Typesystem for'),
        help_text=_('Links a typesystem to the corpus where it has been used'
                    ' for annotating or to data of an annotation tool/service'
                    ' that has been or can be combined with'),
    )
    # ManyToMany relation
    is_ml_model_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('ML model for'),
        help_text=_('Links to a ML model that is is related to LR A (the one'
                    ' being described)'),
    )
    # ManyToMany relation
    is_annotation_resource_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation resource for'),
        help_text=_('Links an annotation resource to the corpus where it has'
                    ' been used for annotating or to data of an annotation'
                    ' tool/service that has been or can be combined with'),
    )
    # ManyToMany relation
    is_annotation_schema_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Annotation schema for'),
        help_text=_('Links an annotation schema to the corpus where it has been'
                    ' used for annotating or to data of an annotation'
                    ' tool/service that has been or can be combined with'),
    )
    # ManyToMany relation
    is_tagset_of = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Tagset for'),
        help_text=_('Links a tagset to the resource where it has been used for'
                    ' annotating or to a tool/service or that should be used'
                    ' (dependency) for the annotation'),
    )
    # ManyToMany relation
    is_required_lr_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Required Language Resources and Technologies'),
        help_text=_('Links to a language resource that is required for the'
                    ' operation of a tool/service or computational grammar'),
    )
    # ManyToMany relation
    is_supplemented_by = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Supplemented by'),
        help_text=_('Links to a language resource that is supplemented by'
                    ' another resource'),
    )
    # ManyToMany relation
    is_supplement_to = GenericLanguageResourceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Supplement to'),
        help_text=_('Links to a language resource that is supplement to another'
                    ' resource'),
    )
    # OneToOne relation
    lr_subclass = LRSubclassSerializer(
        label=_('Subclass'),
        help_text=_('Introduces the type of language resource that is described'
                    ' in the metadata record'),
    )

    class Meta:
        model = models.LanguageResource
        fields = (
            'pk',
            'entity_type',
            'physical_resource',
            'resource_name',
            'resource_short_name',
            'description',
            'lr_identifier',
            'logo',
            'version',
            'version_date',
            'update_frequency',
            'revision',
            'additional_info',
            'contact',
            'mailing_list_name',
            'discussion_url',
            'citation_text',
            'citation_all_versions',
            'ipr_holder',
            'keyword',
            'domain',
            'subject',
            'resource_provider',
            'publication_date',
            'resource_creator',
            'creation_start_date',
            'creation_end_date',
            'funding_project',
            'creation_mode',
            'creation_details',
            'has_original_source',
            'original_source_description',
            'is_created_by',
            'intended_application',
            'actual_use',
            'validated',
            'validation',
            'is_documented_by',
            'is_described_by',
            'is_to_be_cited_by',
            'is_cited_by',
            'is_reviewed_by',
            'is_part_of',
            'is_part_with',
            'is_similar_to',
            'is_exact_match_with',
            'is_potential_duplicate_with',
            'has_metadata',
            'is_archived_by',
            'is_continuation_of',
            'replaces',
            'is_related_to_lr',
            'is_version_of',
            'relation',
            'support_type',
            'complies_with',
            'qualified_attribution',
            'is_compatible_with',
            'has_part',
            'has_version',
            'is_continued_by',
            'is_replaced_with',
            'has_outcome',
            'has_aligned_version',
            'has_annotated_version',
            'has_converted_version',
            'has_archived',
            'has_evaluated',
            'has_elicited',
            'has_created',
            'has_annotated',
            'has_analysed',
            'has_validated',
            'accesses',
            'displays',
            'edits',
            'queries',
            'is_typesystem_of',
            'is_ml_model_of',
            'is_annotation_resource_of',
            'is_annotation_schema_of',
            'is_tagset_of',
            'is_required_lr_by',
            'is_supplemented_by',
            'is_supplement_to',
            'lr_subclass',
        )
        extra_kwargs = {
            'resource_name': {'style': {'max_length': 1000, }},
            'resource_short_name': {'style': {'max_length': 300, 'recommended': True, }},
            'description': {'style': {'max_length': 10000, }},
            'lr_identifier': {'style': {'recommended': True, }},
            'logo': {'style': {'recommended': True, }},
            'version': {'style': {'recommended': True, }},
            'version_date': {'style': {'recommended': True, }},
            'update_frequency': {'style': {'max_length': 100, }},
            'revision': {'style': {'max_length': 500, }},
            'contact': {'style': {'recommended': True, }},
            'citation_text': {'style': {'max_length': 1000, }},
            'keyword': {'style': {'max_length': 100, }},
            'domain': {'style': {'recommended': True, }},
            'resource_provider': {'style': {'recommended': True, }},
            'publication_date': {'style': {'recommended': True, }},
            'resource_creator': {'style': {'recommended': True, }},
            'funding_project': {'style': {'recommended': True, }},
            'creation_details': {'style': {'max_length': 3000, }},
            'original_source_description': {'style': {'max_length': 500, }},
            'intended_application': {'style': {'recommended': True, }},
            'is_documented_by': {'style': {'recommended': True, }},
            'is_to_be_cited_by': {'style': {'recommended': True, }},
            'is_part_of': {'style': {'recommended': True, }},
            'is_similar_to': {'style': {'recommended': True, }},
            'replaces': {'style': {'recommended': True, }},
            'is_related_to_lr': {'style': {'recommended': True, }},
            'is_version_of': {'style': {'recommended': True, }},
            'relation': {'style': {'recommended': True, }},
            'complies_with': {'style': {'recommended': True, }},
        }
        to_display_false = [
            'entity_type',
            'physical_resource',
        ]

    def get_citation_all_versions(self, obj):
        from management.utils.publication_field_generation import AutomaticPublicationFieldGeneration
        try:
            record = obj.metadata_record_of_described_entity
        except models.DescribedEntity.metadata_record_of_described_entity.RelatedObjectDoesNotExist:
            return None

        if record.management_object.is_latest_version:
            latest_versioned_record = record
        else:
            latest_versioned_manager = record.management_object.versioned_record_managers.filter(is_latest_version=True)
            if not latest_versioned_manager.exists():
                return None
            latest_versioned_record = latest_versioned_manager.first().metadata_record_of_manager

        gen = AutomaticPublicationFieldGeneration(instance=latest_versioned_record)
        citation_all_versions = gen.gen_cite_all_versions()
        return citation_all_versions

    def create(self, validated_data):
        validated_data.pop('citation_all_versions', None)
        self._save_kwargs = defaultdict(dict)
        # Remove Register identifier to verify that repo creates each own only
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'lr_identifier',
                                                    'lr_identifier_scheme')
        if validated_data.get('description'):
            validated_data['description'] = sanitize_field(validated_data['description'], tags=ALLOWED_TAGS,
                                                           attrs=ALLOWED_ATTRS,
                                                           protocols=ALLOWED_PROTOCOLS, keep_new_lines=True)
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'lr_identifier',
                                                       'lr_identifier_scheme')
        under_construction = self.context.get('under_construction', False)
        if under_construction:
            self.initial_data['replaces'] = []
            self.initial_data['is_replaced_with'] = []
        instance = super().create(validated_data)

        return instance

    def update(self, instance, validated_data):
        validated_data.pop('citation_all_versions', None)
        self._save_kwargs = defaultdict(dict)
        if validated_data.get('description'):
            validated_data['description'] = sanitize_field(validated_data['description'], tags=ALLOWED_TAGS,
                                                           attrs=ALLOWED_ATTRS,
                                                           protocols=ALLOWED_PROTOCOLS, keep_new_lines=True)

        under_construction = self.context.get('under_construction', False)
        # If lr instance is part of a MetadataRecord
        if hasattr(instance, 'metadata_record_of_described_entity'):
            if under_construction or (instance.metadata_record_of_described_entity and
                                      instance.metadata_record_of_described_entity.management_object.under_construction):
                self.initial_data['replaces'] = []
                self.initial_data['is_replaced_with'] = []
        # get doi identifiers from instance if they exist
        doi_queryset = instance.lr_identifier.filter(lr_identifier_scheme=LR_IDENTIFIER_SCHEME_CHOICES.DOI)

        instance = super().update(instance, validated_data)

        # make sure doi identifiers are still in instance
        for doi_identifier in doi_queryset:
            if doi_identifier not in instance.lr_identifier.all():
                instance.lr_identifier.add(doi_identifier)

        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'logo')
        data = encode_iri_to_uri(data, 'discussion_url')
        # Even if empty value is provided, override with default
        if data.get('version', '') == '':
            data['version'] = self.fields.get('version').default
        return super().to_internal_value(data)

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='lr_identifier',
                                            identifier_scheme='lr_identifier_scheme')
        if self.instance and getattr(self.instance, 'metadata_record_of_described_entity', None) and \
                data.get('version') != self.instance.version:
            record_manager = self.instance.metadata_record_of_described_entity.management_object
            if record_manager.status == 'u':
                raise serializers.ValidationError(_(
                    'Cannot edit version when record has already been published'
                ))
        return data

    def _relation_representation(self, representation, relation_name, relation_enrichment_lrs, is_symmetric=False,
                                 xml_representation=False, display_representation=False,
                                 ):
        '''
        Generates the representation (xml or display) of a given relation by enriching them with LRs from relation_enrichment
        '''
        assert (xml_representation is True) ^ (
                    display_representation is True), "xml_representation and display_representation are mutually exclussive"
        if xml_representation:
            relation_name = self.Meta.model.schema_fields[relation_name]
            for lr in relation_enrichment_lrs:
                # TODO: how to find out if a related lr is full md record and published one taking into account context?
                related_lr = GenericLanguageResourceSerializer(instance=lr,
                                                               context=self.context).to_xml_representation()
                if is_symmetric:
                    if related_lr not in representation[relation_name]:
                        representation[relation_name].append(related_lr)
                else:
                    representation[relation_name].append(related_lr)
        if display_representation and len(relation_enrichment_lrs) != 0:
            if representation[relation_name] is None:
                representation[relation_name] = OrderedDict()
                representation[relation_name]['field_label'] = dict(en=self.fields.get(relation_name).label)
                representation[relation_name]['field_value'] = list()
            for lr in relation_enrichment_lrs:
                related_lr = GenericLanguageResourceSerializer(context=self.context).to_display_representation(
                    instance=lr)
                # Enrich relation with related lr if and only if the user (from context) could see the link to full record
                if related_lr.get('full_metadata_record', None) is not None:
                    if is_symmetric:
                        if related_lr not in representation[relation_name]['field_value']:
                            representation[relation_name]['field_value'].append(related_lr)
                    else:
                        representation[relation_name]['field_value'].append(related_lr)
            if len(representation[relation_name]['field_value']) == 0:
                representation[relation_name] = None
            if representation.get(relation_name) and representation.get(relation_name, {}).get('field_value', None):
                final_field_value = []
                for f_v in representation[relation_name]['field_value']:
                    if f_v.get('version', None) and f_v['version']['field_value'] in [fv['version']['field_value'] for fv
                                                         in representation[relation_name]['field_value'] if
                                                                                      (fv.get('version', None)
                                                                                       and fv != f_v)]:
                        if f_v.get('full_metadata_record') and f_v not in final_field_value:
                            final_field_value.append(f_v)
                        else:
                            continue
                    else:
                        if f_v not in final_field_value:
                            final_field_value.append(f_v)
                representation[relation_name]['field_value'] = final_field_value
        return representation[relation_name]

    def _recalculate_direct_and_inverse_relations(self, instance, representation,
                                                  xml_representation=False,
                                                  display_representation=False
                                                  ):
        '''
        Recalculates direct relation from their respective inverse ones, and materialized
        inverse relation from their respective direct ones.
        '''
        assert (xml_representation is True) ^ (
                    display_representation is True), "xml_representation and display_representation are mutually exclussive"

        # Recalculate relations among LR and LR
        relation_lr = {
            # Recalculate Direct isPartOf from Inverse hasPart
            'is_part_of': 'has_part',
            # Recalculate Inverse hasPart from Direct isPartOf
            'has_part': 'is_part_of',
            # Recalculate Direct isPartWith due to symmetry
            'is_part_with': 'is_part_with',
            # Recalculate Inverse hasVersion from Direct isVersionOf
            'has_version': 'is_version_of',
            # Recalculate Direct isContinuationOf from Inverse isContinuedBy
            'is_continuation_of': 'is_continued_by',
            # Recalculate Inverse isContinuedBy from Direct isContinuationOf
            'is_continued_by': 'is_continuation_of',
            # Recalculate Direct replaces from Inverse isReplacedWith
            'replaces': 'is_replaced_with',
            # Recalculate Inverse isReplacedWith from Direct replaces
            'is_replaced_with': 'replaces',
            # Recalculate Direct isArchivedBy from Inverse hasArchived
            'is_archived_by': 'has_archived',
            # Recalculate Inverse hasArchive from Direct isArchivedBy
            'has_archived': 'is_archived_by',
            # Recalculate Direct isRelatedToLr due to symmetry
            'is_related_to_lr': 'is_related_to_lr',
            # Recalculate Direct isCompatibleWith due to symmetry
            'is_compatible_with': 'is_compatible_with',
            # Recalculate Direct isExactMatchWith due to symmetry
            'is_exact_match_with': 'is_exact_match_with',
            # Recalculate Direct isPotentialDuplicateWith due to symmetry
            'is_potential_duplicate_with': 'is_potential_duplicate_with',
            # Recalculate Direct isSimilarTo due to symmetry
            'is_similar_to': 'is_similar_to',
            # Recalculate Inverse hasValidated from Direct LR.validation.isValidatedBy
            'has_validated': 'validation__is_validated_by',
            # Recalculate Direct hasOriginalSource from Direct LR.ActualUse.hasOutcome
            'has_original_source': 'actual_use__has_outcome',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            if relation_to in ['replaces', 'is_replaced_with']:
                order_by_version = True
            else:
                order_by_version = False
            if relation_to in ['is_related_to_lr', 'is_compatible_with',
                               'is_exact_match_with', 'is_potential_duplicate_with',
                               'is_similar_to', 'is_part_with']:
                is_symmetric = True
            else:
                is_symmetric = False
            enrichment = create_inverse_relation_lr_from_lrs(Q(**query), order_by_version=order_by_version)
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment, is_symmetric=is_symmetric,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)

        # Recalculate relations from Corpus
        relation_lr = {
            # Recalculate Inverse hasAlignedVersion from Corpus.isAlignedVersionOf
            'has_aligned_version': 'is_aligned_version_of',
            # Recalculate Inverse hasAnnotatedVersion from Corpus.isAnnotatedVersionOf
            'has_annotated_version': 'is_annotated_version_of',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            enrichment = create_inverse_relation_lr_from_corpora(Q(**query))
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)

        # Recalculate relations from Corpus, LCR and LD
        relation_lr = {
            # Recalculate Inverse hasConvertedVersion from Corpus/LCR/LD.isConvertedVersionOf
            'has_converted_version': 'is_converted_version_of',
            # Recalculate Inverse hasAnalysed from Corpus/LCR/LD.isAnalysedBy
            'has_analysed': 'is_analysed_by',
            # Recalculate Inverse edits from Corpus/LCR/LD.isEditedBy
            'edits': 'is_edited_by',
            # Recalculate Inverse hasElicited from Corpus/LCR/LD.isElicitedBy
            'has_elicited': 'is_elicited_by',
            # Recalculate Inverse accesses from Corpus/LCR/LD.DatasetDistribution.isAccessedBy
            'accesses': 'dataset_distribution__is_accessed_by',
            # Recalculate Inverse displays from Corpus/LCR/LD.DatasetDistribution.isDisplayedBy
            'displays': 'dataset_distribution__is_displayed_by',
            # Recalculate Inverse queries from Corpus/LCR/LD.DatasetDistribution.isQueriedBy
            'queries': 'dataset_distribution__is_queried_by',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            enrichment = create_inverse_relation_lr_from_corpora(Q(**query))
            enrichment = enrichment + create_inverse_relation_lr_from_lcrs(Q(**query))
            enrichment = enrichment + create_inverse_relation_lr_from_lds(Q(**query))
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)

        # Recalculate relations from CorpusMediaPart
        relation_lr = {
            # Recalculate Inverse  hasAnnotated from Annotation.isAnnotatedBy
            'has_annotated': 'annotation__is_annotated_by',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            enrichment = create_inverse_relation_lr_from_corpora_media_part(Q(**query))
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)

        # Recalculate relations from LD.Model
        relation_lr = {
            # Recalculate Inverse isTagsetOf from LD.Model.tagset
            'is_tagset_of': 'tagset',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            enrichment = create_inverse_relation_lr_from_models(Q(**query))
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)

        # Recalculate relations from ToolService
        # Recalculate Inverse isMLModelOf from MLModel
        relation_to = 'is_ml_model_of'
        relation_from = 'ml_model'
        query = {'%s' % relation_from: instance.pk}
        enrichment = create_inverse_relation_lr_from_toolservice(Q(**query))
        if xml_representation:
            relation_to_name = self.Meta.model.schema_fields[relation_to]
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)
        else:
            # Split isMLModelOf enrichment results into functional ToolService and rest
            is_ml_model_of_functional = []
            is_ml_model_of_rest = []
            for lr in enrichment:
                if described_entity_is_generic(lr):
                    is_ml_model_of_rest.append(lr)
                else:
                    if is_functional_service(lr.metadata_record_of_described_entity):
                        is_ml_model_of_functional.append(lr)
                    else:
                        is_ml_model_of_rest.append(lr)
            # Enrich isMLModelOf with LRs from ismlmodelof_rest
            if len(is_ml_model_of_rest) != 0:
                representation[relation_to] = self._relation_representation(representation, relation_to,
                                                                             is_ml_model_of_rest,
                                                                             xml_representation=False,
                                                                             display_representation=True)
            # Create ismlmodelof_functional that contains only functional ToolService
            # from ToolService.mlmodel
            if len(is_ml_model_of_functional) != 0:
                representation['is_ml_model_of_functional'] = OrderedDict()
                representation['is_ml_model_of_functional']['field_label'] = dict(en='ML model for (functional)')
                representation['is_ml_model_of_functional']['field_value'] = list()
                for lr in is_ml_model_of_functional:
                    related_lr = GenericLanguageResourceSerializer(context=self.context).to_display_representation(
                        instance=lr)
                    representation['is_ml_model_of_functional']['field_value'].append(related_lr)

        # Recalculate relations from ToolService
        relation_lr = {
            # Recalculate Inverse hasEvaluated from Tool.Evaluation.isEvaluatedBy
            'has_evaluated': 'evaluation__is_evaluated_by',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            enrichment = create_inverse_relation_lr_from_toolservice(Q(**query))
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)

        # Recalculate relations from ToolService, CorpusMediaPart.Annotation and LD.Model
        relation_lr = {
            # Recalculate Inverse isAnnotationResourceOf from annotationResource
            'is_annotation_resource_of': 'annotation_resource',
            # Recalculate Inverse isTypesystemOf from typesystem
            'is_typesystem_of': 'typesystem',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            enrichment = create_inverse_relation_lr_from_toolservice(Q(**query))
            enrichment = enrichment + create_inverse_relation_lr_from_models(Q(**query))
            query = {'annotation__%s' % relation_from: instance.pk}
            enrichment = enrichment + create_inverse_relation_lr_from_corpora_media_part(Q(**query))
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)

        # Recalculate relations from ToolService, ToolService.inputContentResource, ToolService.outputResource & LD.Model
        relation_lr = {
            # Recalculate Inverse isAnnotationSchemaOf from annotationSchema
            'is_annotation_schema_of': 'annotation_schema',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            query_input = {'input_content_resource__%s' % relation_from: instance.pk}
            query_output = {'output_resource__%s' % relation_from: instance.pk}
            enrichment = create_inverse_relation_lr_from_toolservice(Q(**query) | Q(**query_input) | Q(**query_output))
            enrichment = enrichment + create_inverse_relation_lr_from_models(Q(**query))
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)

        # Recalculate relations from LanguageResource, CorpusMediaPart, LDMediaPart
        relation_lr = {
            # Recalculate Inverse hasCreated from isCreatedBy
            'has_created': 'is_created_by',
            # Recalculate Inverse hasOutcome from hasOriginalSource
            'has_outcome': 'has_original_source',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            enrichment = list(create_inverse_relation_lr_from_lrs(Q(**query)))
            enrichment = enrichment + create_inverse_relation_lr_from_corpora_media_part(Q(**query))
            enrichment = enrichment + create_inverse_relation_lr_from_lds_media_part(Q(**query))
            if relation_to in ['has_outcome']:
                enrichment = enrichment + create_inverse_relation_lr_from_models(Q(**query))
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)

        # Recalculate relations from ToolService, LanguageDescription.Model & LanguageDescription.Grammar
        relation_lr = {
            # Recalculate Inverse isRequiredByLR from requiresLR
            'is_required_lr_by': 'requires_lr',
        }
        for relation_to, relation_from in relation_lr.items():
            query = {'%s' % relation_from: instance.pk}
            enrichment = list(create_inverse_relation_lr_from_toolservice(Q(**query)))
            enrichment = enrichment + create_inverse_relation_lr_from_models(Q(**query))
            enrichment = enrichment + create_inverse_relation_lr_from_grammars(Q(**query))
            if xml_representation:
                relation_to_name = self.Meta.model.schema_fields[relation_to]
            else:
                relation_to_name = relation_to
            representation[relation_to_name] = self._relation_representation(representation, relation_to,
                                                                             enrichment,
                                                                             xml_representation=xml_representation,
                                                                             display_representation=display_representation)
        return representation

    def to_xml_representation(self, instance=None, add_id=False):
        if not instance:
            instance = self.instance
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        # Add direct and inverse relations
        representation = self._recalculate_direct_and_inverse_relations(instance, representation,
                                                                        xml_representation=True)
        # If lr instance is part of a MetadataRecord
        # TODO Missing tests
        if hasattr(instance, 'metadata_record_of_described_entity'):
            # If record is hosted at system - Add ID with scheme=url and value=https://doi.org/+ DOI value from DOI ID
            if instance.metadata_record_of_described_entity.management_object.is_elg_record:
                # Get with schema URL: ELG landing page
                value = get_identifier_value(
                    representation, self.Meta.model.schema_fields['lr_identifier'],
                    LRIdentifierSerializer.Meta.model.schema_fields['lr_identifier_scheme'],
                    LR_IDENTIFIER_SCHEME_CHOICES.DOI, '#text'
                )
                if value is not None:
                    # Add new ID
                    representation = add_identifier(
                        representation, self.Meta.model.schema_fields['lr_identifier'],
                        LRIdentifierSerializer.Meta.model.schema_fields['lr_identifier_scheme'],
                        LR_IDENTIFIER_SCHEME_CHOICES.URL, '#text', 'https://doi.org/' + value
                    )
            # If record is not hosted at system - Add ID with scheme=url and value= landing page of record
            else:
                value = instance.metadata_record_of_described_entity.get_display_url()
                # Add new ID
                representation = add_identifier(
                     representation, self.Meta.model.schema_fields['lr_identifier'],
                     LRIdentifierSerializer.Meta.model.schema_fields['lr_identifier_scheme'],
                     LR_IDENTIFIER_SCHEME_CHOICES.URL, '#text', value
                )
        # Remove System Identifier
        representation = remove_registry_identifier(
            representation, self.Meta.model.schema_fields['lr_identifier'],
            LRIdentifierSerializer.Meta.model.schema_fields['lr_identifier_scheme']
        )
        # end of code missing tests
        intended_application_xml_field_name = self.Meta.model.schema_fields['intended_application']
        if representation.get(intended_application_xml_field_name, None):
            representation[intended_application_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[intended_application_xml_field_name], choices.LT_CLASS_RECOMMENDED_CHOICES,
                'ms:LTClassRecommended', 'ms:LTClassOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['intended_application'] = super().from_xml_representation_for_recommended_cv(
            ret['intended_application'], 'intended_application', choices.LT_CLASS_RECOMMENDED_CHOICES,
            'ms:LTClassRecommended', 'ms:LTClassOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret = self._recalculate_direct_and_inverse_relations(instance, ret, display_representation=True)

        ret['intended_application'] = super().to_display_representation_for_recommended_cv(
            ret['intended_application'], choices.LT_CLASS_RECOMMENDED_CHOICES)
        ############################
        # Add Inverse relations

        # Combine relations replaces and is_replaced_with
        # to other_versions
        all_versions = retrieve_all_versions_display(
            ret['is_replaced_with'],
            instance,
            ret['replaces'],
            context=self.context
        )
        if all_versions:
            ret['all_versions'] = all_versions

        # Rest of inverse relations
        ret['reverse_relations'] = OrderedDict()
        ret['reverse_relations']['field_label'] = OrderedDict(
            en='Relations to other entities')
        ret['reverse_relations']['field_value'] = list()

        #
        # Inverse relation - isReplacedWith
        if ret['is_replaced_with']:
            ret['reverse_relations']['field_value'].append(ret['is_replaced_with'])
            del ret['is_replaced_with']

        #
        # Inverse relation isAnnotationResourceOf
        if ret['is_annotation_resource_of']:
            ret['reverse_relations']['field_value'].append(ret['is_annotation_resource_of'])
            del ret['is_annotation_resource_of']

        #
        # Inverse relation isAnnotationSchemaOf
        if ret['is_annotation_schema_of']:
            ret['reverse_relations']['field_value'].append(ret['is_annotation_schema_of'])
            del ret['is_annotation_schema_of']

        #
        # Inverse relation isModelOf
        if ret['is_ml_model_of']:
            ret['reverse_relations']['field_value'].append(ret['is_ml_model_of'])
            del ret['is_ml_model_of']

        #
        # Inverse relation isRequiredByLR
        if ret['is_required_lr_by']:
            ret['reverse_relations']['field_value'].append(ret['is_required_lr_by'])
            del ret['is_required_lr_by']

        #
        # Inverse relation isTypesystemOf
        if ret['is_typesystem_of']:
            ret['reverse_relations']['field_value'].append(ret['is_typesystem_of'])
            del ret['is_typesystem_of']

        #
        # Inverse relation - hasOriginalSource
        if ret['has_original_source']:
            ret['reverse_relations']['field_value'].append(ret['has_original_source'])
            del ret['has_original_source']

        #
        # Inverse relation hasOutcome
        if ret['has_outcome']:
            ret['reverse_relations']['field_value'].append(ret['has_outcome'])
            del ret['has_outcome']

        #
        # Inverse relation - hasPart
        if ret['has_part']:
            ret['reverse_relations']['field_value'].append(ret['has_part'])
            del ret['has_part']

        #
        # Inverse relation - isPartWith
        if ret['is_part_with']:
            ret['reverse_relations']['field_value'].append(ret['is_part_with'])
            del ret['is_part_with']

        #
        # Inverse relation hasVersion
        if ret['has_version']:
            ret['reverse_relations']['field_value'].append(ret['has_version'])
            del ret['has_version']

        #
        # Inverse relation hasAlignedVersion
        if ret['has_aligned_version']:
            ret['reverse_relations']['field_value'].append(ret['has_aligned_version'])
            del ret['has_aligned_version']

        #
        # Inverse relation hasAnnotatedVersion
        if ret['has_annotated_version']:
            ret['reverse_relations']['field_value'].append(ret['has_annotated_version'])
            del ret['has_annotated_version']
        #
        # Inverse relation hasConvertedVersion
        if ret['has_converted_version']:
            ret['reverse_relations']['field_value'].append(ret['has_converted_version'])
            del ret['has_converted_version']

        #
        # Inverse relation isContinuedBy
        if ret['is_continued_by']:
            ret['reverse_relations']['field_value'].append(ret['is_continued_by'])
            del ret['is_continued_by']

        #
        # Inverse relation isExactMatchWith
        if ret['is_exact_match_with']:
            ret['reverse_relations']['field_value'].append(ret['is_exact_match_with'])
            del ret['is_exact_match_with']

        #
        # Inverse relation isPotentialDuplicateWith
        if ret['is_potential_duplicate_with']:
            ret['reverse_relations']['field_value'].append(ret['is_potential_duplicate_with'])
            del ret['is_potential_duplicate_with']

        #
        # Inverse relation isSimilarTo
        if ret['is_similar_to']:
            ret['reverse_relations']['field_value'].append(ret['is_similar_to'])
            del ret['is_similar_to']

        #
        # Inverse relations accesses
        if ret['accesses']:
            ret['reverse_relations']['field_value'].append(ret['accesses'])
            del ret['accesses']

        #
        # Inverse relations hasAnalysed
        if ret['has_analysed']:
            ret['reverse_relations']['field_value'].append(ret['has_analysed'])
            del ret['has_analysed']

        #
        # Inverse relations hasAnnotated
        if ret['has_annotated']:
            ret['reverse_relations']['field_value'].append(ret['has_annotated'])
            del ret['has_annotated']

        #
        # Inverse relation hasArchived
        if ret['has_archived']:
            ret['reverse_relations']['field_value'].append(ret['has_archived'])
            del ret['has_archived']

        #
        # Inverse relation hasCreated
        if ret['has_created']:
            ret['reverse_relations']['field_value'].append(ret['has_created'])
            del ret['has_created']

        #
        # Inverse relation displays
        if ret['displays']:
            ret['reverse_relations']['field_value'].append(ret['displays'])
            del ret['displays']

        #
        # Inverse relation edits
        if ret['edits']:
            ret['reverse_relations']['field_value'].append(ret['edits'])
            del ret['edits']

        #
        # Inverse relation hasElicited
        if ret['has_elicited']:
            ret['reverse_relations']['field_value'].append(ret['has_elicited'])
            del ret['has_elicited']

        #
        # Inverse relation hasEvaluated
        if ret['has_evaluated']:
            ret['reverse_relations']['field_value'].append(ret['has_evaluated'])
            del ret['has_evaluated']

        #
        # Inverse relation queries
        if ret['queries']:
            ret['reverse_relations']['field_value'].append(ret['queries'])
            del ret['queries']

        #
        # Inverse relation hasValidated
        if ret['has_validated']:
            ret['reverse_relations']['field_value'].append(ret['has_validated'])
            del ret['has_validated']

        #
        # Inverse relation isTagsetOf
        if ret['is_tagset_of']:
            ret['reverse_relations']['field_value'].append(ret['is_tagset_of'])
            del ret['is_tagset_of']

        # TODO Reverse relation relation R2

        # if no relation is added, set to null
        if len(ret['reverse_relations']['field_value']) == 0:
            ret['reverse_relations'] = None
        return ret


class LicenceTermsSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A legal document (licence or terms of use/service) with which
    the language resource is distributed
    """
    # Reverse FK relation
    licence_identifier = LicenceIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Licence identifier'),
        help_text=_('A string used to uniquely identify a licence'),
    )
    licence_terms_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(200), validate_dict_existence],
        label=_('Licence/terms of use/service name'),
        help_text=_('The name by which a legal document (e.g., licence, terms'
                    ' of use, terms of service) is known'),
    )
    licence_terms_short_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Licence/Terms of use/service short name'),
        help_text=_('Introduces the short name (abbreviation, acronym, etc.)'
                    ' used for a licence or terms of use document'),
    )
    licence_terms_alternative_name = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300)]
        ),
        allow_null=True,
        required=False,
        label=_('Licence/terms of use/service name'),
        help_text=_('Introduces an alternative name (other than the short name)'
                    ' used for a licence or terms of use'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    licence_terms_url = serializers.ListField(
        child=serializers.URLField(),
        allow_empty=False,
        label=_('Licence/terms of use/service URL'),
        help_text=_('Links to the URL where the text of a licence/terms of'
                    ' use/service is found'),
    )
    condition_of_use = serializers.ListField(
        child=serializers.ChoiceField(
            choices=choices.CONDITION_OF_USE_CHOICES),
        allow_empty=False,
        label=_('Condition of use'),
        help_text=_('Links a licence with a specific condition/term of use'
                    ' imposed for accessing a language resource. It is an'
                    ' optional element and only to be taken as providing brief'
                    ' human readable information on the fact that the language'
                    ' resource is provided under a specific set of conditions.'
                    ' These correspond to the most frequently used conditions'
                    ' imposed by the licensor (via the specified licence). The'
                    ' proper exposition of all conditions and possible'
                    ' exceptions is to be found inside the licence text.'
                    ' Depositors should, hence, carefully choose the values of'
                    ' this field to match the licence chosen and users should'
                    ' carefully read that licence before using the language'
                    ' resource.'),
    )

    class Meta:
        model = models.LicenceTerms
        fields = (
            'pk',
            'entity_type',
            'licence_identifier',
            'licence_terms_name',
            'licence_terms_short_name',
            'licence_terms_alternative_name',
            'licence_terms_url',
            'logo',
            'condition_of_use',
            'licence_category',
        )
        extra_kwargs = {
            'licence_identifier': {'style': {'recommended': True, }},
            'licence_terms_name': {'style': {'max_length': 200, }},
            'licence_terms_short_name': {
                'style': {'max_length': 100, 'recommended': True, }},
            'licence_terms_alternative_name': {
                'style': {'max_length': 300, 'recommended': True, }},
        }
        to_display_false = [
            'entity_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'licence_identifier',
                                                    'licence_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'licence_identifier',
                                                       'licence_identifier_scheme')
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'licence_terms_url')
        data = encode_iri_to_uri(data, 'logo')
        return super().to_internal_value(data)

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='licence_identifier',
                                            identifier_scheme='licence_identifier_scheme')
        unspecified_in_condition_of_use_validation(data, 'condition_of_use')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['licence_identifier'],
                                                    LicenceIdentifierSerializer.Meta.model.schema_fields[
                                                        'licence_identifier_scheme'])
        return representation


class DocumentSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A piece of written, printed, or electronic matter that is
    primarily intended for reading
    """
    # Reverse FK relation
    document_identifier = DocumentIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Document identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a document (mainly'
                    ' intended for published documents)'),
    )
    title = SerializersMultilingualField(
        validators=[validate_dict_value_length(500), validate_dict_existence],
        label=_('Title'),
        help_text=_('Introduces a human-readable text (typically short) used as'
                    ' a name for referring to the document being described'),
    )
    alternative_title = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500)]
        ),
        allow_null=True,
        required=False,
        label=_('Alternative title'),
        help_text=_('Introduces an alternative title (e.g. short title) used'
                    ' for the document being described'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    subtitle = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(500)]
        ),
        allow_null=True,
        required=False,
        label=_('Subtitle'),
        help_text=_('Introduces a secondary title used for a document'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    author = GenericPersonSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Author'),
        help_text=_('Introduces the details for a person that has created the'
                    ' document being described'),
    )
    # ManyToMany relation
    editor = GenericPersonSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Editor'),
        help_text=_('Introduces the details for a person that has edited the'
                    ' document being described'),
    )
    # ManyToMany relation
    contributor = GenericPersonSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Contributor'),
        help_text=_('Introduces the details for a person that has contributed'
                    ' in some way to the production of a document or resource'),
    )
    publisher = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Publisher'),
        help_text=_('Introduces the name of an organization that has published'
                    ' the document being described'),
    )
    abstract = SerializersMultilingualField(
        validators=[validate_dict_value_length(10000)],
        allow_null=True,
        required=False,
        label=_('Abstract'),
        help_text=_('Introduces a summary of the contents of a document'),
    )
    journal = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Journal'),
        help_text=_('Introduces the name of the journal where the document'
                    ' being described has been published'),
    )
    book_title = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Book title'),
        help_text=_('Introduces the human readable text that is used as the'
                    ' title of a book (differentiating from the title of an'
                    ' article)'),
    )
    series = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Series'),
        help_text=_('Introduces the title of the series of a book/journal where'
                    ' the document being described has been published'),
    )
    conference = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Conference'),
        help_text=_('Introduces the name of the conference where the document'
                    ' being described was presented'),
    )
    # ManyToMany relation
    domain = DomainSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    # ManyToMany relation
    subject = SubjectSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Subject'),
        help_text=_('Specifies the subject/topic of a language resource'),
    )
    keyword = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
        validators=[validate_unique_multiple_multilingual_field]
    )

    class Meta:
        model = models.Document
        fields = (
            'pk',
            'entity_type',
            'documentation_type',
            'document_identifier',
            'bibliographic_record',
            'document_type',
            'title',
            'alternative_title',
            'subtitle',
            'author',
            'editor',
            'contributor',
            'publication_date',
            'publisher',
            'version_type',
            'abstract',
            'journal',
            'book_title',
            'volume',
            'series',
            'pages',
            'conference',
            'lt_area',
            'domain',
            'subject',
            'keyword',
        )
        extra_kwargs = {
            'document_identifier': {'style': {'recommended': True, }},
            'bibliographic_record': {'style': {'recommended': True, }},
            'title': {'style': {'max_length': 500, }},
            'alternative_title': {'style': {'max_length': 500, }},
            'subtitle': {'style': {'max_length': 500, }},
            'author': {'style': {'recommended': True, }},
            'publication_date': {'style': {'recommended': True, }},
            'publisher': {'style': {'max_length': 300, 'recommended': True, }},
            'abstract': {'style': {'max_length': 10000, }},
            'journal': {'style': {'max_length': 100, }},
            'book_title': {'style': {'max_length': 500, }},
            'series': {'style': {'max_length': 300, }},
            'conference': {'style': {'max_length': 300, }},
            'lt_area': {'style': {'recommended': True, }},
            'keyword': {'style': {'max_length': 100, 'recommended': True, }},
        }
        to_display_false = [
            'entity_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'document_identifier',
                                                    'document_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'document_identifier',
                                                       'document_identifier_scheme')
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='document_identifier',
                                            identifier_scheme='document_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['document_identifier'],
                                                    DocumentIdentifierSerializer.Meta.model.schema_fields[
                                                        'document_identifier_scheme'])
        lt_area_xml_field_name = self.Meta.model.schema_fields['lt_area']
        if representation.get(lt_area_xml_field_name, None):
            representation[lt_area_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[lt_area_xml_field_name], choices.LT_CLASS_RECOMMENDED_CHOICES,
                'ms:LTClassRecommended', 'ms:LTClassOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['lt_area'] = super().from_xml_representation_for_recommended_cv(ret['lt_area'],
                                                                            'lt_area',
                                                                            choices.LT_CLASS_RECOMMENDED_CHOICES,
                                                                            'ms:LTClassRecommended', 'ms:LTClassOther')

        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['lt_area'] = super().to_display_representation_for_recommended_cv(
            ret['lt_area'], choices.LT_CLASS_RECOMMENDED_CHOICES)

        # Add reverse relations
        ret['reverse_relations'] = OrderedDict()
        ret['reverse_relations']['field_label'] = OrderedDict(
            en='Relations to other entities')
        ret['reverse_relations']['field_value'] = list()

        #
        # Reverse relation - cites
        cites = create_inverse_relation_lr_from_lrs_display(
            Q(is_cited_by=instance.pk),
            'Cites',
            context=self.context)
        if cites:
            ret['reverse_relations']['field_value'].append(
                cites)

        #
        # Reverse relation - documents
        documents = create_inverse_relation_lr_from_lrs_display(
            Q(is_documented_by=instance.pk),
            'Documents',
            context=self.context)
        if documents:
            ret['reverse_relations']['field_value'].append(
                documents)

        #
        # Reverse relation - documentsAnnotation
        relation_label = 'Documents the annotation of '
        documents_annotation_corpora_media_part = create_inverse_relation_lr_from_corpora_media_part_display(
            Q(annotation__annotation_report=instance.pk),
            relation_label,
            ['Text', 'Audio', 'Video', 'Image', 'TextNumerical'],
            context=self.context
        )
        if documents_annotation_corpora_media_part:
            ret['reverse_relations']['field_value'].append(
                documents_annotation_corpora_media_part)

        #
        # Reverse relation - documentsEvaluation
        documents_evaluation = create_inverse_relation_lr_from_toolservice_display(
            Q(evaluation__evaluation_report=instance.pk),
            'Documents evaluation',
            context=self.context)
        if documents_evaluation:
            ret['reverse_relations']['field_value'].append(
                documents_evaluation)

        #
        # Reverse relation - documentsGuidelines
        relation_label = 'Guidelines for'
        documents_guidelines_corpora_media_part = create_inverse_relation_lr_from_corpora_media_part_display(
            Q(annotation__guidelines=instance.pk),
            relation_label,
            ['Text', 'Audio', 'Video', 'Image', 'TextNumerical'],
            context=self.context
        )
        if documents_guidelines_corpora_media_part:
            ret['reverse_relations']['field_value'].append(
                documents_guidelines_corpora_media_part)

        #
        # Reverse relation - describes
        relation_label = 'Describes'
        describes_lr = create_inverse_relation_lr_from_lrs_display(
            Q(is_described_by=instance.pk),
            relation_label,
            context=self.context
        )
        describes_tool = create_inverse_relation_lr_from_toolservice_display(
            Q(software_distribution__is_described_by=instance.pk),
            relation_label,
            context=self.context)
        describes = combine_inverse_relations_display(describes_lr, describes_tool)
        if describes:
            ret['reverse_relations']['field_value'].append(
                describes)

        #
        # Reverse relation - reviewes
        reviewes = create_inverse_relation_lr_from_lrs_display(
            Q(is_reviewed_by=instance.pk),
            'Reviewes',
            context=self.context)
        if reviewes:
            ret['reverse_relations']['field_value'].append(
                reviewes)

        #
        # Reverse relation - documentsUsage
        documents_usage = create_inverse_relation_lr_from_lrs_display(
            Q(actual_use__usage_report=instance.pk),
            'Documents the usage of',
            context=self.context)
        if documents_usage:
            ret['reverse_relations']['field_value'].append(
                documents_usage)

        #
        # Reverse relation - documentsValidation
        documents_validation = create_inverse_relation_lr_from_lrs_display(
            Q(validation__validation_report=instance.pk),
            'Documents the validation of',
            context=self.context)
        if documents_validation:
            ret['reverse_relations']['field_value'].append(
                documents_validation)

        #
        # Reverse relation - isPreferredCitationFor
        preferred_citation = create_inverse_relation_lr_from_lrs_display(
            Q(is_to_be_cited_by=instance.pk),
            'Is Preferred Citation For',
            context=self.context)
        if preferred_citation:
            ret['reverse_relations']['field_value'].append(
                preferred_citation)

        # if no relation is added, set to null
        if len(ret['reverse_relations']['field_value']) == 0:
            ret['reverse_relations'] = None
        return ret

class SocialMediaOccupationalAccountSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Introduces the social media or occupational account details of
    an entity
    """
    social_media_occupational_account_type = serializers.ChoiceField(
        choices=choices.SOCIAL_MEDIA_OCCUPATIONAL_ACCOUNT_TYPE_CHOICES,
        label=_('Social media occupational account type'),
        help_text=_('Specifies the type of the social media or occupational'
                    ' account'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.SocialMediaOccupationalAccount
        fields = (
            'pk',
            'social_media_occupational_account_type',
            'value',
        )


class OrganizationSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A company or other group of people that works together for a
    particular purpose [https://dictionary.cambridge.org/dictionary/
    english/organization]
    """
    # Reverse FK relation
    organization_identifier = OrganizationIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Organization identifier'),
        help_text=_('A string used to uniquely identify an organization'),
    )
    organization_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Organization name'),
        help_text=_('The official title of an organization'),
    )
    organization_short_name = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Organization short name'),
        help_text=_('Introduces the short name (abbreviation, acronym, etc.)'
                    ' used for an organization'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    organization_alternative_name = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300)]
        ),
        allow_null=True,
        required=False,
        label=_('Organization alternative name'),
        help_text=_('Introduces an alternative name (other than the official'
                    ' title or the short name) used for an organization'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    organization_bio = SerializersMultilingualField(
        validators=[validate_dict_value_length(10000)],
        allow_null=True,
        required=False,
        label=_('Organization description'),
        help_text=_('Introduces a short free-text account that provides'
                    ' information on an organization'),
    )
    service_offered = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Service offered'),
        help_text=_('Lists the service(s) offered by an organization or person'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    discipline = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Discipline'),
        help_text=_('Introduces an academic field of study or research area'
                    ' that a person, group or organization is expert in'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    domain = DomainSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    keyword = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    member_of_association = GenericOrganizationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Member of association'),
        help_text=_('Links to an association or network that the organization'
                    ' or person is member of'),
    )
    # OneToOne relation
    head_office_address = AddressSetSerializer(
        allow_null=True,
        required=False,
        label=_('Address (head office)'),
        help_text=_('Links to the physical address of the head office of an'
                    ' organization or group represented as a set of distinct'
                    ' elements (street, zip code, city, etc.)'),
    )
    # Reverse FK relation
    other_office_address = AddressSetSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Address (other offices)'),
        help_text=_('Links to the physical address of an office (other than the'
                    ' principal one) of an organization or group represented as'
                    ' a set of distinct elements (street, zip code, city,'
                    ' etc.)'),
    )
    # Reverse FK relation
    social_media_occupational_account = SocialMediaOccupationalAccountSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Social account'),
        help_text=_('Introduces the social media or occupational account'
                    ' details of an entity'),
    )
    # ManyToMany relation
    is_division_of = GenericOrganizationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Division of'),
        help_text=_('Links a division (e.g., company branch, university'
                    ' department or faculty) to the organization to which it'
                    ' belongs'),
    )
    # ManyToMany relation
    replaces_organization = GenericOrganizationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Replaces organization'),
        help_text=_('Specified an organization that has been replaced by the'
                    ' organization which is described (e.g. following a merge'
                    ' or acquisition action)'),
    )

    class Meta:
        model = models.Organization
        fields = (
            'pk',
            'entity_type',
            'organization_identifier',
            'organization_name',
            'organization_short_name',
            'organization_alternative_name',
            'organization_role',
            'organization_legal_status',
            'startup',
            'organization_bio',
            'logo',
            'lt_area',
            'service_offered',
            'discipline',
            'domain',
            'keyword',
            'member_of_association',
            'email',
            'website',
            'head_office_address',
            'other_office_address',
            'telephone_number',
            'fax_number',
            'social_media_occupational_account',
            'division_category',
            'is_division_of',
            'replaces_organization',
        )
        extra_kwargs = {
            'organization_identifier': {'style': {'recommended': True, }},
            'organization_name': {'style': {'max_length': 300, }},
            'organization_short_name': {'style': {'max_length': 100, 'recommended': True, }},
            'organization_alternative_name': {'style': {'max_length': 300, 'recommended': True, }},
            'organization_bio': {'style': {'max_length': 10000, 'recommended': True, }},
            'logo': {'style': {'recommended': True, }},
            'lt_area': {'style': {'recommended': True, }},
            'service_offered': {'style': {'max_length': 100, 'recommended': True, }},
            'discipline': {'style': {'max_length': 100, }},
            'domain': {'style': {'recommended': True, }},
            'keyword': {'style': {'max_length': 100, 'recommended': True, }},
            'email': {'style': {'recommended': True, }},
            'website': {'style': {'recommended': True, }},
            'head_office_address': {'style': {'recommended': True, }},
            'social_media_occupational_account': {'style': {'recommended': True, }},
            'division_category': {'style': {'recommended': True, }},
            'is_division_of': {'style': {'recommended': True, }},
        }
        to_display_false = [
            'entity_type',
        ]

    # DONE <rule>not to be filled in if organizationLegalStatus is not 'http://w3id.org/meta-share/meta-share/sme'</rule>
    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='organization_identifier',
                                            identifier_scheme='organization_identifier_scheme')
        if data.get('head_office_address', '') in ['', None] \
                and not data.get('other_office_address', []) in [[], None]:
            raise serializers.ValidationError(
                {'head_office_address': _(
                    f"{self.fields['head_office_address'].label} must be completed before "
                    f"{self.fields['other_office_address'].label}")},
                code='invalid',
            )
        if data.get('organization_legal_status', None) not in [
            'http://w3id.org/meta-share/meta-share/sme'] \
                and data.get('startup', None) is True:
            raise serializers.ValidationError(
                {'startup': _(
                    f"{self.fields['startup'].label} must not be filled if "
                    f"{self.fields['organization_legal_status'].label} is not "
                    f"{self.fields['organization_legal_status'].choices['http://w3id.org/meta-share/meta-share/sme']}")},
                code='invalid',
            )
        if data.get('division_category', None) in ['http://w3id.org/meta-share/meta-share/branch',
                                                   'http://w3id.org/meta-share/meta-share/department',
                                                   'http://w3id.org/meta-share/meta-share/faculty',
                                                   'http://w3id.org/meta-share/meta-share/group',
                                                   'http://w3id.org/meta-share/meta-share/institute',
                                                   'http://w3id.org/meta-share/meta-share/laboratory',
                                                   'http://w3id.org/meta-share/meta-share/school',
                                                   'http://w3id.org/meta-share/meta-share/subsidiary',
                                                   'http://w3id.org/meta-share/meta-share/unit1'] \
                and (data.get('is_division_of', None) is None or len(data.get('is_division_of', [])) == 0):
            raise serializers.ValidationError(
                {'is_division_of': _(f"{self.fields['is_division_of'].label} cannot be blank")},
                code='invalid',
            )
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'organization_identifier',
                                                    'organization_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'organization_identifier',
                                                       'organization_identifier_scheme')
        if validated_data.get('organization_bio'):
            validated_data['organization_bio'] = sanitize_field(validated_data['organization_bio'], tags=ALLOWED_TAGS,
                                                                attrs=ALLOWED_ATTRS,
                                                                protocols=ALLOWED_PROTOCOLS, keep_new_lines=True)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        if validated_data.get('organization_bio'):
            validated_data['organization_bio'] = sanitize_field(validated_data['organization_bio'], tags=ALLOWED_TAGS,
                                                                attrs=ALLOWED_ATTRS,
                                                                protocols=ALLOWED_PROTOCOLS, keep_new_lines=True)
        instance = super().update(instance, validated_data)
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'logo')
        data = encode_iri_to_uri(data, 'website')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['organization_identifier'],
                                                    OrganizationIdentifierSerializer.Meta.model.schema_fields[
                                                        'organization_identifier_scheme'])
        lt_area_xml_field_name = self.Meta.model.schema_fields['lt_area']
        if representation.get(lt_area_xml_field_name, None):
            representation[lt_area_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[lt_area_xml_field_name], choices.LT_CLASS_RECOMMENDED_CHOICES,
                'ms:LTClassRecommended', 'ms:LTClassOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['lt_area'] = super().from_xml_representation_for_recommended_cv(ret['lt_area'],
                                                                            'lt_area',
                                                                            choices.LT_CLASS_RECOMMENDED_CHOICES,
                                                                            'ms:LTClassRecommended', 'ms:LTClassOther')
        return ret

    def is_division_of_recursive(self, instance=None, depth=0):
        if instance is not None and depth < 2:
            is_division_of_representation = OrderedDict()
            is_division_of_representation['organization'] = OrderedDict()
            is_division_of_representation['organization']['field_label'] = dict(
                en='Organization')
            is_division_of_representation['organization']['field_value'] = \
                GenericOrganizationSerializer().to_display_representation(instance=instance)
            is_division_of = OrganizationSerializer().is_division_of_recursive(
                instance=instance.is_division_of, depth=depth + 1
            )
            if is_division_of is not None:
                is_division_of_representation['is_division_of'] = OrderedDict()
                is_division_of_representation['is_division_of']['field_label'] = dict(
                    en='Is division of')
                is_division_of_representation['is_division_of']['field_value'] = is_division_of
            else:
                is_division_of_representation['is_division_of'] = None
            return is_division_of_representation
        return None

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['lt_area'] = super().to_display_representation_for_recommended_cv(
            ret['lt_area'], choices.LT_CLASS_RECOMMENDED_CHOICES)
        # Remove email due to GDPR, if no consent
        if not ret.get('email', None) is None:
            display_org_email(ret, instance)

        # Reverse relationship of Organization
        # Reverse relation - hasDivision
        has_division = create_inverse_relation_organization_display(
            Q(is_division_of=instance.pk),
            'Divisions',
            context=self.context
        )
        if has_division:
            ret['has_division'] = has_division

        # Reverse relation - IsReplaceWith
        is_replaced_with = create_inverse_relation_organization_display(
            Q(replaces_organization=instance.pk),
            'Replaced with',
            context=self.context
        )
        if is_replaced_with:
            ret['is_replaced_with'] = is_replaced_with

        # Add reverse relations of Organization-Actor
        ret['reverse_relations'] = create_inverse_relation_for_actor_display(instance,
                                                                             context=self.context)
        if ret['reverse_relations'] is None:
            # Add reverse relations
            ret['reverse_relations'] = OrderedDict()
            ret['reverse_relations']['field_label'] = dict(
                en='Relations to other entities')
            ret['reverse_relations']['field_value'] = list()
        # Add reverse relations of Organization
        # Reverse relation - isCoordinatorOf
        is_coordinator_of = create_inverse_relation_project_display(
            Q(coordinator=instance.pk),
            'Coordinated projects',
            context=self.context
        )
        if is_coordinator_of:
            ret['reverse_relations']['field_value'].append(is_coordinator_of)

        # Reverse relation - isParticipatingOrganizationOf
        is_participating_organization_of = create_inverse_relation_project_display(
            Q(participating_organization=instance.pk),
            'Participant in projects',
            context=self.context
        )
        if is_participating_organization_of:
            ret['reverse_relations']['field_value'].append(
                is_participating_organization_of)

        # if no relation is added, set to null
        if len(ret['reverse_relations']['field_value']) == 0:
            ret['reverse_relations'] = None
        return ret


class GroupSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A set of persons related to some aspect of a language resource,
    that do not have a legal status (e.g. a group of software
    developers working on the same software)
    """
    # Reverse FK relation
    group_identifier = GroupIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Group identifier'),
        help_text=_('A string used to uniquely identify a group'),
    )
    organization_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Organization name'),
        help_text=_('The official title of an organization'),
    )
    organization_short_name = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Organization short name'),
        help_text=_('Introduces the short name (abbreviation, acronym, etc.)'
                    ' used for an organization'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    organization_alternative_name = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300)]
        ),
        allow_null=True,
        required=False,
        label=_('Organization alternative name'),
        help_text=_('Introduces an alternative name (other than the official'
                    ' title or the short name) used for an organization'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    organization_bio = SerializersMultilingualField(
        validators=[validate_dict_value_length(10000)],
        allow_null=True,
        required=False,
        label=_('Organization description'),
        help_text=_('Introduces a short free-text account that provides'
                    ' information on an organization'),
    )
    service_offered = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Service offered'),
        help_text=_('Lists the service(s) offered by an organization or person'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    discipline = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Discipline'),
        help_text=_('Introduces an academic field of study or research area'
                    ' that a person, group or organization is expert in'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    domain = DomainSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    keyword = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    address_set = AddressSetSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Address'),
        help_text=_('Links to the physical address of an organization, group or'
                    ' person represented as a set of distinct elements (street,'
                    ' number, zip code, city, etc.)'),
    )
    # Reverse FK relation
    social_media_occupational_account = SocialMediaOccupationalAccountSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Social account'),
        help_text=_('Introduces the social media or occupational account'
                    ' details of an entity'),
    )

    class Meta:
        model = models.Group
        fields = (
            'pk',
            'entity_type',
            'group_identifier',
            'organization_name',
            'organization_short_name',
            'organization_alternative_name',
            'organization_role',
            'organization_bio',
            'logo',
            'lt_area',
            'service_offered',
            'discipline',
            'domain',
            'keyword',
            'email',
            'website',
            'address_set',
            'telephone_number',
            'fax_number',
            'social_media_occupational_account',
        )
        extra_kwargs = {
            'group_identifier': {'style': {'recommended': True, }},
            'organization_name': {'style': {'max_length': 300, }},
            'organization_short_name': {'style': {'max_length': 100, 'recommended': True, }},
            'organization_alternative_name': {'style': {'max_length': 300, 'recommended': True, }},
            'organization_bio': {'style': {'max_length': 10000, 'recommended': True, }},
            'logo': {'style': {'recommended': True, }},
            'lt_area': {'style': {'recommended': True, }},
            'service_offered': {'style': {'max_length': 100, }},
            'discipline': {'style': {'max_length': 100, }},
            'domain': {'style': {'recommended': True, }},
            'keyword': {'style': {'max_length': 100, 'recommended': True, }},
            'email': {'style': {'recommended': True, }},
            'website': {'style': {'recommended': True, }},
        }
        to_display_false = [
            'entity_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'group_identifier',
                                                    'organization_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'group_identifier',
                                                       'organization_identifier_scheme')
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'logo')
        data = encode_iri_to_uri(data, 'website')
        return super().to_internal_value(data)

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='group_identifier',
                                            identifier_scheme='organization_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['group_identifier'],
                                                    OrganizationIdentifierSerializer.Meta.model.schema_fields[
                                                        'organization_identifier_scheme'])
        lt_area_xml_field_name = self.Meta.model.schema_fields['lt_area']
        if representation.get(lt_area_xml_field_name, None):
            representation[lt_area_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[lt_area_xml_field_name], choices.LT_CLASS_RECOMMENDED_CHOICES,
                'ms:LTClassRecommended', 'ms:LTClassOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['lt_area'] = super().from_xml_representation_for_recommended_cv(ret['lt_area'],
                                                                            'lt_area',
                                                                            choices.LT_CLASS_RECOMMENDED_CHOICES,
                                                                            'ms:LTClassRecommended', 'ms:LTClassOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['lt_area'] = super().to_display_representation_for_recommended_cv(
            ret['lt_area'], choices.LT_CLASS_RECOMMENDED_CHOICES)
        # Remove email due to GDPR, if no consent
        if not ret.get('email', None) is None:
            display_email(ret, instance.consent)
        ret['reverse_relations'] = create_inverse_relation_for_actor_display(instance,
                                                                             context=self.context)
        return ret


class PersonSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A human being
    """
    # Reverse FK relation
    personal_identifier = PersonalIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Person identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a person'),
    )
    surname = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Surname'),
        help_text=_('Introduces the surname (i.e. the name shared by members of'
                    ' a family, also known as \'family name\' or \'last name\') of'
                    ' a person'),
    )
    given_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(200), validate_dict_existence],
        label=_('Given name'),
        help_text=_('Introduces the given name (also known as \'first name\') of'
                    ' a person'),
    )
    name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Name'),
        help_text=_('Introduces the full name of a person; recommended format'
                    ' \'surname, givenName\''),
    )
    honorific_prefix = SerializersMultilingualField(
        validators=[validate_dict_value_length(20)],
        allow_null=True,
        required=False,
        label=_('Honorific prefix'),
        help_text=_('Introduces a honorific prefix (or title) such as'
                    ' Dr./Mr/Ms. that precedes the name of a person'),
    )
    short_bio = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000)],
        allow_null=True,
        required=False,
        label=_('Short bio'),
        help_text=_('Introduces a free text that can be used as a short bio for'
                    ' a person'),
    )
    service_offered = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Service offered'),
        help_text=_('Lists the service(s) offered by an organization or person'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    discipline = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Discipline'),
        help_text=_('Introduces an academic field of study or research area'
                    ' that a person, group or organization is expert in'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    domain = DomainSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    keyword = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    member_of_association = GenericOrganizationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Member of association'),
        help_text=_('Links to an association or network that the organization'
                    ' or person is member of'),
    )
    # Reverse FK relation
    address_set = AddressSetSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Address'),
        help_text=_('Links to the physical address of an organization, group or'
                    ' person represented as a set of distinct elements (street,'
                    ' number, zip code, city, etc.)'),
    )
    # Reverse FK relation
    social_media_occupational_account = SocialMediaOccupationalAccountSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Social account'),
        help_text=_('Introduces the social media or occupational account'
                    ' details of an entity'),
    )
    # Reverse FK relation
    affiliation = AffiliationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Affiliation'),
        help_text=_('Introduces information on an organization to whom the'
                    ' person is affiliated and the data relevant to this'
                    ' relation (i.e. position, email, etc.)'),
    )

    class Meta:
        model = models.Person
        fields = (
            'pk',
            'entity_type',
            'personal_identifier',
            'surname',
            'given_name',
            'name',
            'honorific_prefix',
            'short_bio',
            'image3',
            'lt_area',
            'service_offered',
            'discipline',
            'domain',
            'keyword',
            'member_of_association',
            'email',
            'homepage',
            'address_set',
            'telephone_number',
            'fax_number',
            'social_media_occupational_account',
            'affiliation',
        )
        extra_kwargs = {
            'personal_identifier': {'style': {'recommended': True, }},
            'surname': {'style': {'max_length': 300, }},
            'given_name': {'style': {'max_length': 200, }},
            'name': {'style': {'max_length': 300, }},
            'honorific_prefix': {'style': {'max_length': 20, }},
            'short_bio': {'style': {'max_length': 1000, 'recommended': True, }},
            'image3': {'style': {'recommended': True, }},
            'lt_area': {'style': {'recommended': True, }},
            'service_offered': {'style': {'max_length': 100, }},
            'discipline': {'style': {'max_length': 100, }},
            'keyword': {'style': {'max_length': 100, 'recommended': True, }},
            'email': {'style': {'recommended': True, }},
            'homepage': {'style': {'recommended': True, }},
            'social_media_occupational_account': {'style': {'recommended': True, }},
            'affiliation': {'style': {'recommended': True, }},
        }
        to_display_false = [
            'entity_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'personal_identifier',
                                                    'personal_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'personal_identifier',
                                                       'personal_identifier_scheme')
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'image3')
        data = encode_iri_to_uri(data, 'homepage')
        return super().to_internal_value(data)

    def validate(self, data):
        for_information_only = self.context.get('for_information_only', False)
        if for_information_only is False and data.get('name', None) is not None:
            raise serializers.ValidationError(
                {'name': _(f"{self.fields['name'].label} can be used only for infomation only records.")},
                code='invalid',
            )
        unique_identifier_scheme_validation(data,
                                            entity_identifier='personal_identifier',
                                            identifier_scheme='personal_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        # Remove email due to GDPR
        if self.Meta.model.schema_fields['email'] in representation:
            del representation[self.Meta.model.schema_fields['email']]
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['personal_identifier'],
                                                    PersonalIdentifierSerializer.Meta.model.schema_fields[
                                                        'personal_identifier_scheme'])
        lt_area_xml_field_name = self.Meta.model.schema_fields['lt_area']
        if representation.get(lt_area_xml_field_name, None):
            representation[lt_area_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[lt_area_xml_field_name], choices.LT_CLASS_RECOMMENDED_CHOICES,
                'ms:LTClassRecommended', 'ms:LTClassOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['lt_area'] = super().from_xml_representation_for_recommended_cv(ret['lt_area'],
                                                                            'lt_area',
                                                                            choices.LT_CLASS_RECOMMENDED_CHOICES,
                                                                            'ms:LTClassRecommended', 'ms:LTClassOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        ret['lt_area'] = super().to_display_representation_for_recommended_cv(
            ret['lt_area'], choices.LT_CLASS_RECOMMENDED_CHOICES)
        # Remove email due to GDPR, if no consent
        if not ret.get('email', None) is None:
            display_email(ret, instance.consent)

        # Add reverse relations Person-Actor
        ret['reverse_relations'] = create_inverse_relation_for_actor_display(instance,
                                                                             context=self.context)
        if ret['reverse_relations'] is None:
            # Add reverse relations
            ret['reverse_relations'] = OrderedDict()
            ret['reverse_relations']['field_label'] = OrderedDict(
                en='Relations to other entities')
            ret['reverse_relations']['field_value'] = list()

        # Add reverse relations Person
        # Reverse relation - isMetadataCreatorOf
        is_metadata_creator_of = create_inverse_relation_metadata_record_from_metadata_record_display(
            Q(metadata_creator=instance.pk),
            'Created metadata records',
            context=self.context
        )
        if is_metadata_creator_of:
            ret['reverse_relations']['field_value'].append(
                is_metadata_creator_of)

        # Reverse relation - isMetadataCuratorOf
        is_metadata_curator_of = create_inverse_relation_metadata_record_from_metadata_record_display(
            Q(metadata_curator=instance.pk),
            'Curated metadata records',
            context=self.context
        )
        if is_metadata_curator_of:
            ret['reverse_relations']['field_value'].append(
                is_metadata_curator_of)

        # if no relation is added, set to null
        if len(ret['reverse_relations']['field_value']) == 0:
            ret['reverse_relations'] = None
        return ret


class ProjectSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A set of operations undertaken as a whole by an individual or
    organization and related to some aspect of the lifecycle of the
    language resource (e.g. funding, deployment, etc.)
    """
    # Reverse FK relation
    project_identifier = ProjectIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Project identifier'),
        help_text=_('A string (e.g., PID, internal to an organization, issued'
                    ' by the funding authority, etc.) used to uniquely identify'
                    ' a project'),
    )
    project_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Project name'),
        help_text=_('The official title of a project'),
    )
    project_short_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(100)],
        allow_null=True,
        required=False,
        label=_('Project short name'),
        help_text=_('Introduces a short name (e.g., acronym, abbreviated form)'
                    ' by which a project is known'),
    )
    project_alternative_name = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(300)]
        ),
        allow_null=True,
        required=False,
        label=_('Project alternative name'),
        help_text=_('Introduces an alternative name (other than the short name)'
                    ' used for a project'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # ManyToMany relation
    funder = ActorSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Funder'),
        help_text=_('Identifies the person or organization or group that has'
                    ' financed the project'),
    )
    # ManyToMany relation
    domain = DomainSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    # ManyToMany relation
    subject = SubjectSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Subject'),
        help_text=_('Specifies the subject/topic of a language resource'),
    )
    keyword = serializers.ListField(
        child=SerializersMultilingualField(
            validators=[validate_dict_value_length(100)]
        ),
        allow_null=True,
        required=False,
        label=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
        validators=[validate_unique_multiple_multilingual_field]
    )
    # Reverse FK relation
    social_media_occupational_account = SocialMediaOccupationalAccountSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Social account'),
        help_text=_('Introduces the social media or occupational account'
                    ' details of an entity'),
    )
    project_summary = SerializersMultilingualField(
        validators=[validate_dict_value_length(10000)],
        allow_null=True,
        required=False,
        label=_('Project summary'),
        help_text=_('Introduces a short description (in free text) of the main'
                    ' objectives, mission or contents of the project'),
    )
    # OneToOne relation
    cost = CostSerializer(
        allow_null=True,
        required=False,
        label=_('Cost'),
        help_text=_('Introduces the cost for accessing a resource or the'
                    ' overall budget of a project, formally described as a set'
                    ' of amount and currency unit'),
    )
    # OneToOne relation
    ec_max_contribution = CostSerializer(
        allow_null=True,
        required=False,
        label=_('EC max contribution'),
        help_text=_('Introduces the maximum amount of money contributed from EC'
                    ' funds'),
    )
    # OneToOne relation
    national_max_contribution = CostSerializer(
        allow_null=True,
        required=False,
        label=_('National max contribution'),
        help_text=_('Introduces the maximum amount of money contributed from'
                    ' national funds'),
    )
    # FK relation
    coordinator = GenericOrganizationSerializer(
        allow_null=True,
        required=False,
        label=_('Coordinator'),
        help_text=_('Introduces the organization that coordinates the project'
                    ' being described'),
    )
    # ManyToMany relation
    participating_organization = GenericOrganizationSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Participating organization'),
        help_text=_('Indicates the organization(s) that participate (or have'
                    ' participated) in a project'),
    )
    # ManyToMany relation
    is_related_to_document = GenericDocumentSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Related to document'),
        help_text=_('Links to a document that is somehow related to the entity'
                    ' that is described'),
    )
    project_report = SerializersMultilingualField(
        validators=[validate_dict_value_length(1000)],
        allow_null=True,
        required=False,
        label=_('Project report'),
        help_text=_('Links to a report issued by a project'),
    )
    # ManyToMany relation
    replaces_project = GenericProjectSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Replaces project'),
        help_text=_('Specifies a project that has been replaced by the project'
                    ' described'),
    )

    class Meta:
        model = models.Project
        fields = (
            'pk',
            'entity_type',
            'project_identifier',
            'project_name',
            'project_short_name',
            'project_alternative_name',
            'funding_type',
            'funder',
            'funding_country',
            'project_start_date',
            'project_end_date',
            'email',
            'website',
            'logo',
            'lt_area',
            'domain',
            'subject',
            'keyword',
            'social_media_occupational_account',
            'grant_number',
            'project_summary',
            'cost',
            'ec_max_contribution',
            'national_max_contribution',
            'funding_scheme_category',
            'status',
            'related_call',
            'related_programme',
            'related_subprogramme',
            'coordinator',
            'participating_organization',
            'is_related_to_document',
            'project_report',
            'replaces_project',
        )
        extra_kwargs = {
            'project_identifier': {'style': {'recommended': True, }},
            'project_name': {'style': {'max_length': 300, }},
            'project_short_name': {'style': {'max_length': 100, 'recommended': True, }},
            'project_alternative_name': {'style': {'max_length': 300, 'recommended': True, }},
            'funding_type': {'style': {'recommended': True, }},
            'funder': {'style': {'recommended': True, }},
            'funding_country': {'style': {'recommended': True, }},
            'email': {'style': {'recommended': True, }},
            'website': {'style': {'recommended': True, }},
            'logo': {'style': {'recommended': True, }},
            'lt_area': {'style': {'recommended': True, }},
            'domain': {'style': {'recommended': True, }},
            'keyword': {'style': {'max_length': 100, 'recommended': True, }},
            'social_media_occupational_account': {'style': {'recommended': True, }},
            'project_summary': {'style': {'max_length': 10000, 'recommended': True, }},
            'project_report': {'style': {'max_length': 1000, }},
        }
        to_display_false = [
            'entity_type',
        ]

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Remove Register identifier to verify that repo creates each own only
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'project_identifier',
                                                    'project_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'project_identifier',
                                                       'project_identifier_scheme')
        if validated_data.get('project_summary'):
            validated_data['project_summary'] = sanitize_field(validated_data['project_summary'], tags=ALLOWED_TAGS,
                                                               attrs=ALLOWED_ATTRS,
                                                               protocols=ALLOWED_PROTOCOLS, keep_new_lines=True)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        if validated_data.get('project_summary'):
            validated_data['project_summary'] = sanitize_field(validated_data['project_summary'], tags=ALLOWED_TAGS,
                                                               attrs=ALLOWED_ATTRS,
                                                               protocols=ALLOWED_PROTOCOLS, keep_new_lines=True)
        instance = super().update(instance, validated_data)
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'website')
        data = encode_iri_to_uri(data, 'logo')
        return super().to_internal_value(data)

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='project_identifier',
                                            identifier_scheme='project_identifier_scheme')
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['project_identifier'],
                                                    ProjectIdentifierSerializer.Meta.model.schema_fields[
                                                        'project_identifier_scheme'])
        lt_area_xml_field_name = self.Meta.model.schema_fields['lt_area']
        if representation.get(lt_area_xml_field_name, None):
            representation[lt_area_xml_field_name] = super().to_xml_representation_for_recommended_cv(
                representation[lt_area_xml_field_name], choices.LT_CLASS_RECOMMENDED_CHOICES,
                'ms:LTClassRecommended', 'ms:LTClassOther'
            )
        return representation

    def from_xml_representation(self, xml_data):
        ret = super().from_xml_representation(xml_data)
        ret['lt_area'] = super().from_xml_representation_for_recommended_cv(ret['lt_area'],
                                                                            'lt_area',
                                                                            choices.LT_CLASS_RECOMMENDED_CHOICES,
                                                                            'ms:LTClassRecommended', 'ms:LTClassOther')
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        if not ret.get('email', None) is None:
            display_email(ret, instance.consent)
        ret['lt_area'] = super().to_display_representation_for_recommended_cv(
            ret['lt_area'], choices.LT_CLASS_RECOMMENDED_CHOICES)
        #
        # Add reverse relations

        # Reverse relation - isReplacedBy
        is_replaced_with = create_inverse_relation_project_display(
            Q(replaces_project=instance.pk),
            'Replaced with',
            context=self.context
        )
        if is_replaced_with:
            ret['is_replaced_with'] = is_replaced_with

        # Rest of Reverse relations
        ret['reverse_relations'] = OrderedDict()
        ret['reverse_relations']['field_label'] = OrderedDict(
            en='Relations to other entities')
        ret['reverse_relations']['field_value'] = list()

        #
        # Reverse relation - isFundingProjectOf
        has_original_source = create_inverse_relation_lr_from_lrs_display(
            Q(funding_project=instance.pk),
            'Funded Language Resources and Technologies',
            context=self.context)
        if has_original_source:
            ret['reverse_relations']['field_value'].append(
                has_original_source)

        #
        # Reverse relation - isUsageProjectOf
        is_usage_project_of = create_inverse_relation_lr_from_lrs_display(
            Q(actual_use__usage_project=instance.pk),
            'Used Language Resources and Technologies',
            context=self.context)
        if is_usage_project_of:
            ret['reverse_relations']['field_value'].append(
                is_usage_project_of)

        # if no relation is added, set to null
        if len(ret['reverse_relations']['field_value']) == 0:
            ret['reverse_relations'] = None

        return ret


class provenanceSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Groups together information regarding provenance of the contents
    of the repository, in case of aggregating repositories
    """
    # ManyToMany relation
    originating_repository = GenericRepositorySerializer(
        many=True,
        allow_empty=False,
        label=_('Originating repository'),
        help_text=_('Groups together information regarding the originating'
                    ' repository, i.e. the one being harvested in case of'
                    ' aggregating repositories'),
    )

    class Meta:
        model = models.provenance
        fields = (
            'pk',
            'originating_repository',
            'harvesting_frequency',
        )


class RepositorySerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    Groups together all information required for the description of
    repositories
    """
    repository_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300), validate_dict_existence],
        label=_('Repository name'),
        help_text=_('The official full name of the repository'),
    )
    repository_additional_name = SerializersMultilingualField(
        validators=[validate_dict_value_length(300)],
        allow_null=True,
        required=False,
        label=_('Repository additional name'),
        help_text=_('An additional name (usually a short name, acronym etc.)'
                    ' for the repository'),
    )
    # Reverse FK relation
    repository_identifier = RepositoryIdentifierSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Repository identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a repository'),
    )
    repository_url = serializers.URLField(
        label=_('Repository URL'),
        help_text=_('Links to the URL that hosts the catalogue'),
    )
    # ManyToMany relation
    repository_language = LanguageSerializer(
        many=True,
        allow_empty=False,
        label=_('Repository language'),
        help_text=_('The primary language of the repository interface'),
    )
    description = SerializersMultilingualField(
        validators=[validate_dict_value_length(10000), validate_dict_existence],
        label=_('Description'),
        help_text=_('Introduces a short free-text account that provides'
                    ' information about the resource (e.g., function, contents,'
                    ' technical information, etc.)'),
    )
    # FK relation
    repository_institution = GenericOrganizationSerializer(
        label=_('Repository institution'),
        help_text=_('Groups information on the institution that is responsible'
                    ' for the repository'),
    )
    # ManyToMany relation
    repository_scientific_leader = GenericPersonSerializer(
        many=True,
        allow_empty=False,
        label=_('Repository scientific leader'),
        help_text=_('Groups information on the person that acts as the'
                    ' Scientific Responsible for the repository'),
    )
    # ManyToMany relation
    repository_technical_responsible = GenericPersonSerializer(
        many=True,
        allow_empty=False,
        label=_('Repository technical responsible'),
        help_text=_('Groups information on the person acts as the Technical'
                    ' Responsible for the repository'),
    )
    mission_statement_url = serializers.URLField(
        max_length=1000,
        allow_blank=True, allow_null=True,
        label=_('Mission statement URL'),
        help_text=_('The URL of a mission statement describing the designated'
                    ' community of the repository'),
        default='https://www.clarin.gr/en/about/what-is-clarin'
    )
    # Reverse FK relation
    provenance = provenanceSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Provenance'),
        help_text=_('Groups together information regarding provenance of the'
                    ' contents of the repository, in case of aggregating'
                    ' repositories'),
    )
    hosted_repository = serializers.BooleanField(
        label=_('Hosted repository'),
        help_text=_('Used for identifying the hosted repository'),
        default=False,
    )

    class Meta:
        model = models.Repository
        fields = (
            'pk',
            'entity_type',
            'repository_name',
            'repository_additional_name',
            'repository_identifier',
            'repository_url',
            'repository_type',
            'provider_type',
            'repository_country',
            'repository_language',
            'description',
            'repository_institution',
            'repository_scientific_leader',
            'repository_technical_responsible',
            'operation_start_date',
            'operation_end_date',
            'mission_statement_url',
            'provenance',
            'pid_system',
            'id_provider_url',
            'hosted_repository',
        )
        extra_kwargs = {
            'repository_name': {'style': {'max_length': 300, }},
            'repository_additional_name': {'style': {'max_length': 300, }},
            'description': {'style': {'max_length': 10000, }},
        }
        to_display_false = [
            'entity_type',
        ]

    def validate(self, data):
        unique_identifier_scheme_validation(data,
                                            entity_identifier='repository_identifier',
                                            identifier_scheme='repository_identifier_scheme')
        if data.get('repository_type', None) == choices.REPOSITORY_TYPE_CHOICES.AGGREGATING \
                and data.get('provenance', None) is None:
            raise serializers.ValidationError(
                {'provenance': _(f"{self.fields['provenance'].label} cannot be blank")},
                code='invalid',
            )
        unique_language_validation(data, 'repository_language')
        return data

    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Remove Register identifiers
        # From validated data
        LOGGER.debug("{} - Remove from record Register ID - validate data.".format(
            self.Meta.model))
        validated_data = remove_registry_identifier(validated_data,
                                                    'repository_identifier',
                                                    'repository_identifier_scheme')
        # From initial data - due to DRF nested model
        LOGGER.debug("{} - Remove from record Register ID - initial data.".format(
            self.Meta.model))
        self.initial_data = remove_registry_identifier(self.initial_data,
                                                       'repository_identifier',
                                                       'repository_identifier_scheme')
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance

    def to_internal_value(self, data):
        data = encode_iri_to_uri(data, 'repository_url')
        data = encode_iri_to_uri(data, 'mission_statement_url')
        data = encode_iri_to_uri(data, 'id_provider_url')
        return super().to_internal_value(data)

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        representation = remove_registry_identifier(representation,
                                                    self.Meta.model.schema_fields['repository_identifier'],
                                                    RepositoryIdentifierSerializer.Meta.model.schema_fields[
                                                        'repository_identifier_scheme'])
        return representation


class DescribedEntitySerializer(
    PolymorphicSerializer, WritableNestedModelSerializer,
    XMLPolymorphicSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    The (type of) entity that is being described by a metadata
    record
    """

    class Meta:
        model = models.DescribedEntity
        fields = '__all__'

    model_serializer_mapping = {
        models.LanguageResource: LanguageResourceSerializer,
        models.LicenceTerms: LicenceTermsSerializer,
        models.Document: DocumentSerializer,
        models.Organization: OrganizationSerializer,
        models.Group: GroupSerializer,
        models.Person: PersonSerializer,
        models.Project: ProjectSerializer,
        models.Repository: RepositorySerializer
    }

    resource_type_field_name = 'entity_type'

    def _get_resource_type_from_mapping(self, mapping):
        if not mapping or not mapping.get(self.resource_type_field_name, None):
            raise serializers.ValidationError(
                {self.resource_type_field_name: _(
                    'entity type cannot be blank')},
                code='invalid',
            )
        return mapping[self.resource_type_field_name]


class MetadataRecordSerializer(
    WritableNestedModelSerializer, XMLSerializer,
    DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    A set of formalized structured information used to describe the
    contents, structure, function, etc. of an entity, usually
    according to a specific set of rules (metadata schema)
    """
    # OneToOne relation
    metadata_record_identifier = MetadataRecordIdentifierSerializer(
        allow_null=True,
        required=False,
        label=_('Metadata record identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a metadata record'),
    )
    # ManyToMany relation
    metadata_curator = GenericPersonSerializer(
        many=True,
        allow_null=True,
        required=False,
        label=_('Metadata curator'),
        help_text=_('A person responsible for the creation, update, enrichment,'
                    ' etc. of a metadata record describing an entity'),
    )
    # FK relation
    metadata_creator = GenericPersonSerializer(
        allow_null=True,
        required=False,
        label=_('Metadata creator'),
        help_text=_('Introduces the person who has created the metadata record'),
    )
    # FK relation
    source_of_metadata_record = GenericRepositorySerializer(
        allow_null=True,
        required=False,
        label=_('Source of metadata record'),
        help_text=_('The entity (repository, catalogue, archive, etc.) from'
                    ' which the metadata record has been imported into the new'
                    ' catalogue'),
    )
    # OneToOne relation
    source_metadata_record = GenericMetadataRecordSerializer(
        allow_null=True,
        required=False,
        label=_('Source metadata record'),
        help_text=_('Links, in some cases (e.g., harvesting, population from'
                    ' other catalogues, conversion from other metadata records,'
                    ' etc.), to the metadata record that has been used as the'
                    ' basis for the creation of the metadata record'),
    )
    revision = SerializersMultilingualField(
        validators=[validate_dict_value_length(500)],
        allow_null=True,
        required=False,
        label=_('Revision'),
        help_text=_('Provides an account of the revisions in free text or a'
                    ' link to a document with revisions'),
    )
    # OneToOne relation
    described_entity = DescribedEntitySerializer(
        label=_('Type of described entity'),
        help_text=_('The (type of) entity that is being described by a metadata'
                    ' record'),
    )
    # OneToOne relation
    management_object = ManagerSerializer()

    class Meta:
        model = models.MetadataRecord
        fields = (
            'pk',
            'metadata_record_identifier',
            'metadata_creation_date',
            'metadata_last_date_updated',
            'metadata_curator',
            'complies_with',
            'metadata_creator',
            'source_of_metadata_record',
            'source_metadata_record',
            'revision',
            'described_entity',
            'management_object'
        )
        extra_kwargs = {
            'source_of_metadata_record': {'style': {'recommended': True, }},
            'revision': {'style': {'max_length': 500, }},
        }

    def create(self, validated_data):
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context
        # Remove metadata record identifier to verify that registry creates each own only
        # From validated data
        if 'metadata_record_identifier' in validated_data:
            del validated_data['metadata_record_identifier']
        # From initial data, due to drf-nested package
        if 'metadata_record_identifier' in self.initial_data:
            del self.initial_data['metadata_record_identifier']

        replaces = None
        if validated_data.get('described_entity'):
            replaces = validated_data['described_entity'].get('replaces', []).copy()

        is_replaced_with = None
        if validated_data.get('described_entity'):
            is_replaced_with = validated_data['described_entity'].get('is_replaced_with', []).copy()

        instance = super().create(validated_data)

        # duplicate handling
        if self.context.get('allow_duplicates'):
            if self.context.get('duplicates_found'):
                check_landing_page = False
                if getattr(instance, 'source_of_metadata_record', None) \
                        and instance.source_of_metadata_record.repository_name['en'] in ['Zenodo']:
                    check_landing_page = True
                harvesting_duplicate_handling(instance, check_landing_page)
            else:
                harvesting_versions_of_existing_records_handling(instance)
            harvesting_version_and_duplicate_handling(instance)

        # add unlisted versions to record
        get_unlisted_versions(instance, replaces, is_replaced_with)

        # If replaces is edited handle replaces
        if replaces or is_replaced_with:
            for lr in instance.described_entity.replaces.filter(
                    metadata_record_of_described_entity__management_object__under_construction=True):
                instance.described_entity.replaces.remove(lr)
            for lr in instance.described_entity.is_replaced_with.filter(
                    metadata_record_of_described_entity__management_object__under_construction=True):
                instance.described_entity.is_replaced_with.remove(lr)
            record_manager = instance.management_object
            record_manager.handle_replaces_editing(instance.described_entity.replaces.all(),
                                                   instance.described_entity.is_replaced_with.all(),
                                                   request_user)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        # Get the request user
        request_user_context = self.context.get('request_user')
        request_user = get_user_model().objects.get(username=settings.SYSTEM_USER)
        if request_user_context:
            request_user = request_user_context

        record_manager = instance.management_object

        validated_version = None
        if validated_data.get('described_entity'):
            validated_version = validated_data['described_entity'].get('version')
        if validated_version and validated_version != instance.described_entity.version \
                and record_manager.status in ['d', 'i']:
            record_manager.handle_version_editing(validated_version)

        replaces = None
        if validated_data.get('described_entity'):
            replaces = validated_data['described_entity'].get('replaces', []).copy()

        is_replaced_with = None
        if validated_data.get('described_entity'):
            is_replaced_with = validated_data['described_entity'].get('is_replaced_with', []).copy()

        instance = super().update(instance, validated_data)

        if replaces or is_replaced_with:
            for lr in instance.described_entity.replaces.filter(
                    metadata_record_of_described_entity__management_object__under_construction=True):
                instance.described_entity.replaces.remove(lr)
            for lr in instance.described_entity.is_replaced_with.filter(
                    metadata_record_of_described_entity__management_object__under_construction=True):
                instance.described_entity.is_replaced_with.remove(lr)

            record_manager.handle_replaces_editing(instance.described_entity.replaces.all(),
                                                   instance.described_entity.is_replaced_with.all(),
                                                   request_user)
        return instance

    def validate(self, data):
        entity_type = None
        if data.get('described_entity') and data['described_entity'].get('entity_type'):
            entity_type = data['described_entity']['entity_type']
        pk = None
        if self.instance:
            pk = self.instance.pk
        allow_duplicates = self.context.get('allow_duplicates', False)
        record_already_exists(entity_type, pk, data, allow_duplicates)
        return data

    def to_xml_representation(self, instance=None, add_id=False):
        representation = super().to_xml_representation(instance=instance, add_id=add_id)
        md_id_xml_field_name = self.Meta.model.schema_fields['metadata_record_identifier']
        # Remove System Identifier
        if representation[md_id_xml_field_name] is not None and \
                representation[md_id_xml_field_name][
                    MetadataRecordIdentifierSerializer.Meta.model.schema_fields['metadata_record_identifier_scheme']
                ] == settings.REGISTRY_IDENTIFIER_SCHEMA:
            del representation[md_id_xml_field_name]
        # Add as MetadataRecordIdentifer the view page URL + format
        # TODO Missing tests
        value = self.instance.get_display_url() + '?format=elg'
        # Add new ID
        representation = add_identifier(
            representation, self.Meta.model.schema_fields['metadata_record_identifier'],
            MetadataRecordIdentifierSerializer.Meta.model.schema_fields['metadata_record_identifier_scheme'],
            METADATA_RECORD_IDENTIFIER_SCHEME_CHOICES.URL, '#text', value, md=True
        )
        # end of code missing tests
        return representation

    def from_xml_representation(self, xml_data=empty):
        ret = super(MetadataRecordSerializer, self).from_xml_representation()
        ret['management_object'] = dict()
        return ret

    def to_display_representation(self, instance=None):
        if not instance:
            instance = self.instance
        ret = super().to_display_representation(instance)
        # for zenodo records, show only up to 10 distributions
        source = getattr(instance, 'source_of_metadata_record', None)
        if source and instance.described_entity.entity_type == 'LanguageResource':
            source_name = getattr(source, 'repository_name', {}).get('en', '')
            if source_name == 'Zenodo':
                try:
                    if len(ret['described_entity']['field_value']['lr_subclass']['field_value']['dataset_distribution']['field_value']) > 10:
                        ret['described_entity']['field_value']['lr_subclass']['field_value']['dataset_distribution']['field_value'] = ret['described_entity']['field_value']['lr_subclass']['field_value']['dataset_distribution']['field_value'][:10]
                except KeyError:
                    if len(ret['described_entity']['field_value']['lr_subclass']['field_value']['software_distribution']['field_value']) > 10:
                        ret['described_entity']['field_value']['lr_subclass']['field_value']['software_distribution']['field_value'] = ret['described_entity']['field_value']['lr_subclass']['field_value']['software_distribution']['field_value'][:10]
        # add pk to metadatarecord display representation
        if instance:
            ret.update({'pk': instance.pk})
            ret.move_to_end('pk', last=False)
        return ret


class FunderIdentifierSerializer(
    serializers.ModelSerializer, XMLSerializer, DisplaySerializer, DraftSerializer, RelatedSerializer):
    """
    """
    funder_identifier_scheme = serializers.ChoiceField(
        choices=choices.FUNDER_IDENTIFIER_SCHEME_CHOICES,
        label=_('Funder identfier scheme'),
        help_text=_('The name of the scheme used to identify a funder'),
    )
    value = serializers.CharField(
        trim_whitespace=True,
        max_length=1000,
        label=_('value'),
        help_text=''
    )

    class Meta:
        model = models.FunderIdentifier
        fields = (
            'pk',
            'funder_identifier_scheme',
            'value',
        )


def create_inverse_relation_lr_from_lrs_display(query, relation_label, order_by_version=False, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_lrs = models.LanguageResource.objects.filter(query).distinct()
    if order_by_version:
        related_lrs_temp = related_lrs
        related_lrs = chain(sorted([lr for lr in related_lrs_temp.exclude(version__exact='unspecified')],
                                   reverse=True,
                                   key=lambda x: _ver.parse(x.version)),
                            [lr for lr in related_lrs_temp.exclude(~Q(version__exact='unspecified'))])
    for lr in related_lrs:
        related_lr = GenericLanguageResourceSerializer(context=context).to_display_representation(
            instance=lr)
        if related_lr.get('full_metadata_record', None) is not None:
            reverse_relation['field_value'].append(related_lr)
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_lr_from_lrs(query, order_by_version=False):
    related_lrs = models.LanguageResource.objects.filter(query).distinct()
    if order_by_version:
        related_lrs_temp = related_lrs
        related_lrs = list(chain(related_lrs_temp.exclude(version__exact='unspecified').order_by('-version'),
                                 related_lrs_temp.exclude(~Q(version__exact='unspecified'))))
    return related_lrs


def create_inverse_relation_lr_from_corpora_display(query, relation_label, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_corpora = models.Corpus.objects.filter(query).distinct()
    for c in related_corpora:
        try:
            instance = c.language_resource_of_lr_subclass
            related_lr = GenericLanguageResourceSerializer(context=context).to_display_representation(instance=instance)
            if related_lr.get('full_metadata_record', None) is not None:
                reverse_relation['field_value'].append(related_lr)
        except ObjectDoesNotExist:
            continue
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_lr_from_corpora(query, order_by_version=False):
    related_lrs = []
    related_corpora = models.Corpus.objects.filter(query).distinct()
    for c in related_corpora:
        try:
            related_lr = c.language_resource_of_lr_subclass
            related_lrs.append(related_lr)
        except ObjectDoesNotExist:
            continue
    if order_by_version and len(related_lrs) > 0 :
        related_lrs_temp = related_lrs
        related_lrs = list(chain(related_lrs_temp.exclude(version__exact='unspecified').order_by('-version'),
                                 related_lrs_temp.exclude(~Q(version__exact='unspecified'))))
    return related_lrs


def create_inverse_relation_lr_from_corpora_media_part_display(query, relation_label,
                                                               media_list, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_lr = set()
    for media in media_list:
        related_media_part = QuerySet()
        if media == 'Text':
            related_media_part = models.CorpusTextPart.objects.filter(
                query).distinct()
        elif media == 'Audio':
            related_media_part = models.CorpusAudioPart.objects.filter(
                query).distinct()
        elif media == 'Video':
            related_media_part = models.CorpusVideoPart.objects.filter(
                query).distinct()
        elif media == 'Image':
            related_media_part = models.CorpusImagePart.objects.filter(
                query).distinct()
        elif media == 'TextNumerical':
            related_media_part = models.CorpusTextNumericalPart.objects.filter(
                query).distinct()
        for part in related_media_part:
            try:
                lr_instance = part.corpus_of_corpus_media_part.language_resource_of_lr_subclass
                related_lr.add(lr_instance)
            except ObjectDoesNotExist:
                continue
    for lr in related_lr:
        related_lr = GenericLanguageResourceSerializer(context=context).to_display_representation(
            instance=lr)
        if related_lr.get('full_metadata_record', None) is not None:
            reverse_relation['field_value'].append(related_lr)
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_lr_from_corpora_media_part(query,
                                                       media_list=['Text', 'Audio', 'Video', 'Image', 'TextNumerical'],
                                                       order_by_version=False):
    related_lrs = set()
    for media in media_list:
        related_media_part = QuerySet()
        if media == 'Text':
            related_media_part = models.CorpusTextPart.objects.filter(
                query).distinct()
        elif media == 'Audio':
            related_media_part = models.CorpusAudioPart.objects.filter(
                query).distinct()
        elif media == 'Video':
            related_media_part = models.CorpusVideoPart.objects.filter(
                query).distinct()
        elif media == 'Image':
            related_media_part = models.CorpusImagePart.objects.filter(
                query).distinct()
        elif media == 'TextNumerical':
            related_media_part = models.CorpusTextNumericalPart.objects.filter(
                query).distinct()
        for part in related_media_part:
            try:
                lr_instance = part.corpus_of_corpus_media_part.language_resource_of_lr_subclass
                related_lrs.add(lr_instance)
            except ObjectDoesNotExist:
                continue
    if order_by_version:
        related_lrs_temp = list(related_lrs)
        related_lrs = list(chain(related_lrs_temp.exclude(version__exact='unspecified').order_by('-version'),
                                 related_lrs_temp.exclude(~Q(version__exact='unspecified'))))
    return list(related_lrs)


def create_inverse_relation_lr_from_lds_display(query, relation_label, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_lds = models.LanguageDescription.objects.filter(query).distinct()
    for ld in related_lds:
        try:
            lr_instance = ld.language_resource_of_lr_subclass
            related_lr = GenericLanguageResourceSerializer(context=context).to_display_representation(instance=lr_instance)
            if related_lr.get('full_metadata_record', None) is not None:
                reverse_relation['field_value'].append(related_lr)
        except ObjectDoesNotExist:
            continue
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_lr_from_lds(query, order_by_version=False):
    related_lrs = []
    related_lds = models.LanguageDescription.objects.filter(query).distinct()
    for ld in related_lds:
        try:
            related_lr = ld.language_resource_of_lr_subclass
            related_lrs.append(related_lr)
        except ObjectDoesNotExist:
            continue
    if order_by_version:
        related_lrs_temp = related_lrs
        related_lrs = list(chain(related_lrs_temp.exclude(version__exact='unspecified').order_by('-version'),
                                 related_lrs_temp.exclude(~Q(version__exact='unspecified'))))
    return related_lrs


def create_inverse_relation_lr_from_models_display(query, relation_label, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_models = models.Model.objects.filter(query).distinct()
    for m in related_models:
        try:
            lr_instance = m.language_description_of_language_description_subclass.language_resource_of_lr_subclass
            related_lr = GenericLanguageResourceSerializer(context=context).to_display_representation(instance=lr_instance)
            if related_lr.get('full_metadata_record', None) is not None:
                reverse_relation['field_value'].append(related_lr)
        except ObjectDoesNotExist:
            continue
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_lr_from_models(query, order_by_version=False):
    related_lrs = []
    related_models = models.Model.objects.filter(query).distinct()
    for m in related_models:
        try:
            related_lr = m.language_description_of_language_description_subclass.language_resource_of_lr_subclass
            related_lrs.append(related_lr)
        except ObjectDoesNotExist:
            continue
    if order_by_version:
        related_lrs_temp = related_lrs
        related_lrs = list(chain(related_lrs_temp.exclude(version__exact='unspecified').order_by('-version'),
                                 related_lrs_temp.exclude(~Q(version__exact='unspecified'))))
    return related_lrs


def create_inverse_relation_lr_from_grammar_display(query, relation_label, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_grammars = models.Grammar.objects.filter(query).distinct()
    for m in related_grammars:
        try:
            lr_instance = m.language_description_of_language_description_subclass.language_resource_of_lr_subclass
            related_lr = GenericLanguageResourceSerializer(context=context).to_display_representation(
            instance=lr_instance)
            if related_lr.get('full_metadata_record', None) is not None:
                reverse_relation['field_value'].append(related_lr)
        except ObjectDoesNotExist:
            continue
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_lr_from_grammars(query, order_by_version=False):
    related_lrs = []
    related_models = models.Grammar.objects.filter(query).distinct()
    for m in related_models:
        try:
            related_lr = m.language_description_of_language_description_subclass.language_resource_of_lr_subclass
            related_lrs.append(related_lr)
        except ObjectDoesNotExist:
            continue
    if order_by_version:
        related_lrs_temp = related_lrs
        related_lrs = list(chain(related_lrs_temp.exclude(version__exact='unspecified').order_by('-version'),
                                 related_lrs_temp.exclude(~Q(version__exact='unspecified'))))
    return related_lrs


def create_inverse_relation_lr_from_lcrs_display(query, relation_label, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_lds = models.LexicalConceptualResource.objects.filter(
        query).distinct()
    for ld in related_lds:
        try:
            lr_instance = ld.language_resource_of_lr_subclass
            related_lr = GenericLanguageResourceSerializer(context=context).to_display_representation(
            instance=lr_instance)
            if related_lr.get('full_metadata_record', None) is not None:
                reverse_relation['field_value'].append(related_lr)
        except ObjectDoesNotExist:
            continue
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_lr_from_lcrs(query, order_by_version=False):
    related_lrs = []
    related_lcrs = models.LexicalConceptualResource.objects.filter(query).distinct()
    for lcr in related_lcrs:
        try:
            related_lr = lcr.language_resource_of_lr_subclass
            related_lrs.append(related_lr)
        except ObjectDoesNotExist:
            continue
    if order_by_version:
        related_lrs_temp = related_lrs
        related_lrs = list(chain(related_lrs_temp.exclude(version__exact='unspecified').order_by('-version'),
                                 related_lrs_temp.exclude(~Q(version__exact='unspecified'))))
    return related_lrs


def create_inverse_relation_lr_from_toolservice_display(query, relation_label, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_tool = models.ToolService.objects.filter(query).distinct()
    for tool in related_tool:
        try:
            lr_instance = tool.language_resource_of_lr_subclass
            related_lr = GenericLanguageResourceSerializer(context=context).to_display_representation(
                instance=lr_instance)
            if related_lr.get('full_metadata_record', None) is not None:
                reverse_relation['field_value'].append(related_lr)
        except ObjectDoesNotExist:
            continue
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_lr_from_toolservice(query, order_by_version=False):
    related_lrs = []
    related_tools = models.ToolService.objects.filter(query).distinct()
    for tool in related_tools:
        try:
            related_lr = tool.language_resource_of_lr_subclass
            related_lrs.append(related_lr)
        except ObjectDoesNotExist:
            continue
    if order_by_version:
        related_lrs_temp = related_lrs
        related_lrs = list(chain(related_lrs_temp.exclude(version__exact='unspecified').order_by('-version'),
                                 related_lrs_temp.exclude(~Q(version__exact='unspecified'))))
    return related_lrs


def combine_inverse_relations_display(relation1, relation2, label=None):
    relation_combined = None
    if relation1:
        relation_combined = relation1.copy()
    if relation_combined and relation2:
        relation_combined['field_value'] = (
                relation_combined['field_value'] + relation2['field_value']
        )
    elif relation2:
        relation_combined = relation2.copy()

    if relation_combined and label:
        relation_combined['field_label'] = label
    return relation_combined


def retrieve_all_versions_display(greater_versions, current, earlier_versions, context=dict()):
    all_versions = OrderedDict()
    all_versions['field_label'] = OrderedDict(en='All versions')
    all_versions['field_value'] = list()
    temp_field_value = []

    # Add current
    current = GenericLanguageResourceSerializer(context=context).to_display_representation(instance=current)

    # Highlight current value
    current['highlight'] = True
    all_versions['field_value'].append(current)

    if greater_versions:
        temp_field_value = greater_versions['field_value'].copy()

    if earlier_versions:
        temp_field_value += earlier_versions['field_value']

    if temp_field_value:
        sorted_temp_field_value = sorted([ver for ver in temp_field_value],
                                          reverse=True,
                                          key=lambda x: _ver.parse(x['version']['field_value'] if x.get('version') else ''))
        for sorted_version in sorted_temp_field_value:
            if not sorted_version in all_versions['field_value']:
                all_versions['field_value'].append(sorted_version)
    # For each version element in all_versions,
    # if it is an elg record and has DOI, display the absolute DOI url
    # TODO: add tests
    for version_elem in all_versions['field_value']:
        for lr_id in version_elem['lr_identifier']['field_value']:
            if (
                    lr_id['lr_identifier_scheme']['field_value'] == choices.LR_IDENTIFIER_SCHEME_CHOICES.DOI
                    and settings.DATACITE_PREFIX in lr_id['value']['field_value']

            ):
                lr_id['value']['field_value'] = f'{settings.DOI_RESOLVE_URL}{lr_id["value"]["field_value"]}'
    return all_versions


def create_inverse_relation_lr_from_lds_media_part_display(query, relation_label,
                                                           media_list, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_lr = set()
    for media in media_list:
        related_media_part = QuerySet()
        if media == 'Text':
            related_media_part = models.LanguageDescriptionTextPart.objects.filter(
                query).distinct()
        elif media == 'Video':
            related_media_part = models.LanguageDescriptionVideoPart.objects.filter(
                query).distinct()
        elif media == 'Image':
            related_media_part = models.LanguageDescriptionImagePart.objects.filter(
                query).distinct()
        for part in related_media_part:
            try:
                lr_instance = part.language_description_of_language_description_media_part.language_resource_of_lr_subclass
                related_lr.add(lr_instance)
            except ObjectDoesNotExist:
                continue
    for lr in related_lr:
        related_lr = GenericLanguageResourceSerializer(context=context).to_display_representation(
            instance=lr)
        if related_lr.get('full_metadata_record', None) is not None:
            reverse_relation['field_value'].append(related_lr)
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_lr_from_lds_media_part(query,
                                                   media_list=['Text', 'Video', 'Image'],
                                                   order_by_version=False):
    related_lrs = set()
    for media in media_list:
        related_media_part = QuerySet()
        if media == 'Text':
            related_media_part = models.LanguageDescriptionTextPart.objects.filter(
                query).distinct()
        elif media == 'Video':
            related_media_part = models.LanguageDescriptionVideoPart.objects.filter(
                query).distinct()
        elif media == 'Image':
            related_media_part = models.LanguageDescriptionImagePart.objects.filter(
                query).distinct()
        for part in related_media_part:
            try:
                lr_instance = part.language_description_of_language_description_media_part.language_resource_of_lr_subclass
                related_lrs.add(lr_instance)
            except ObjectDoesNotExist:
                continue
    if order_by_version:
        related_lrs_temp = list(related_lrs)
        related_lrs = list(chain(related_lrs_temp.exclude(version__exact='unspecified').order_by('-version'),
                                 related_lrs_temp.exclude(~Q(version__exact='unspecified'))))
    return list(related_lrs)


def create_inverse_relation_project_display(query, relation_label, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_projects = models.Project.objects.filter(query).distinct()
    for project in related_projects:
        related_pr = GenericProjectSerializer(context=context).to_display_representation(
            instance=project)
        if related_pr.get('full_metadata_record', None) is not None:
            reverse_relation['field_value'].append(related_pr)
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_organization_display(query, relation_label, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_organizations = models.Organization.objects.filter(query).distinct()
    for org in related_organizations:
        related_org = GenericOrganizationSerializer(context=context).to_display_representation(
            instance=org)
        if related_org.get('full_metadata_record', None) is not None:
            reverse_relation['field_value'].append(related_org)
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


# TODO: add show only when full_metadata_record exists?
def create_inverse_relation_metadata_record_from_metadata_record_display(query,
                                                                         relation_label,
                                                                         context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_mds = models.MetadataRecord.objects.filter(query).distinct()
    for md in related_mds:
        related_md = GenericMetadataRecordSerializer(context=context).to_display_representation(
            instance=md)
        if related_md.get('full_metadata_record', None) is not None:
            reverse_relation['field_value'].append(related_md)
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None


def create_inverse_relation_metadata_record_from_lrs_display(query, relation_label, context=dict()):
    reverse_relation = OrderedDict()
    reverse_relation['field_label'] = OrderedDict(en=relation_label)
    reverse_relation['field_value'] = list()
    related_lrs = models.LanguageResource.objects.filter(query).distinct()
    for lr in related_lrs:
        related_md = GenericMetadataRecordSerializer(context=context).to_display_representation(
            instance=lr.metadatarecord)
        if related_md.get('full_metadata_record', None) is not None:
            reverse_relation['field_value'].append(related_md)
    if len(reverse_relation['field_value']) != 0:
        return reverse_relation
    return None
    # TODO Deprecated: Check for deletion


def create_inverse_relation_for_actor_display(instance, context=dict()):
    # Add reverse relations
    reverse_relations = OrderedDict()
    reverse_relations['field_label'] = OrderedDict(
        en='Relations to other entities')
    reverse_relations['field_value'] = list()

    #
    # Reverse relation - isAnnotatorOf
    relation_label = 'Annotated Language Resources and Technologies'
    is_annotator_of_corpus_media_part = create_inverse_relation_lr_from_corpora_media_part_display(
        Q(annotation__annotator=instance.pk),
        relation_label,
        ['Text', 'Image', 'Video', 'Audio', 'TextNumerical'],
        context=context
    )
    if is_annotator_of_corpus_media_part:
        reverse_relations['field_value'].append(
            is_annotator_of_corpus_media_part)

    #
    # Reverse relation - isContactFor
    is_contact_for = create_inverse_relation_lr_from_lrs_display(
        Q(contact=instance.pk),
        'Contact for',
        context=context)
    if is_contact_for:
        reverse_relations['field_value'].append(
            is_contact_for)

    #
    # Reverse relation - isDistributionRightsHolderOf
    relation_label = 'Holds distribution rights for'
    rights_holder_corpus = create_inverse_relation_lr_from_corpora_display(
        Q(dataset_distribution__distribution_rights_holder=instance.pk),
        relation_label,
        context=context
    )
    rights_holder_lcr = create_inverse_relation_lr_from_lcrs_display(
        Q(dataset_distribution__distribution_rights_holder=instance.pk),
        relation_label,
        context=context
    )
    rights_holder_ld = create_inverse_relation_lr_from_lds_display(
        Q(dataset_distribution__distribution_rights_holder=instance.pk),
        relation_label,
        context=context
    )
    rights_holder_toolservice = create_inverse_relation_lr_from_toolservice_display(
        Q(software_distribution__distribution_rights_holder=instance.pk),
        relation_label,
        context=context
    )
    rights_holder = combine_inverse_relations_display(rights_holder_corpus,
                                                      rights_holder_lcr)
    rights_holder = combine_inverse_relations_display(rights_holder, rights_holder_ld)
    rights_holder = combine_inverse_relations_display(rights_holder,
                                                      rights_holder_toolservice)
    if rights_holder:
        reverse_relations['field_value'].append(rights_holder)

    #
    # Reverse relation - isEvaluatorOf
    is_evaluator_of = create_inverse_relation_lr_from_toolservice_display(
        Q(evaluation__evaluator=instance.pk),
        'Evaluated Language Resources and Technologies',
        context=context
    )
    if is_evaluator_of:
        reverse_relations['field_value'].append(is_evaluator_of)

    #
    # Reverse relation - isFunderOf
    is_funder_of = create_inverse_relation_project_display(
        Q(funder=instance.pk),
        'Funded projects',
        context=context
    )
    if is_funder_of:
        reverse_relations['field_value'].append(is_funder_of)

    #
    # Reverse relation - isIPRHolderOf
    is_iprholder_of = create_inverse_relation_lr_from_lrs_display(
        Q(ipr_holder=instance.pk),
        'Holds IPR for',
        context=context
    )
    if is_iprholder_of:
        reverse_relations['field_value'].append(is_iprholder_of)

    #
    # Reverse relation - isRecorderOf
    is_recorder_of = create_inverse_relation_lr_from_corpora_media_part_display(
        Q(recorder=instance.pk),
        'Recorded Language Resources',
        ['Audio', 'Video', 'TextNumerical'],
        context=context
    )
    if is_recorder_of:
        reverse_relations['field_value'].append(is_recorder_of)

    #
    # Reverse relation - isResourceCreatorOf
    is_resource_creator_of = create_inverse_relation_lr_from_lrs_display(
        Q(resource_creator=instance.pk),
        'Created Language Resources and Technologies',
        context=context
    )
    if is_resource_creator_of:
        reverse_relations['field_value'].append(is_resource_creator_of)

    #
    # Reverse relation - isResourceProviderOf
    is_resource_provider_of = create_inverse_relation_lr_from_lrs_display(
        Q(resource_provider=instance.pk),
        'Provided Language Resources and Technologies',
        context=context
    )
    if is_resource_provider_of:
        reverse_relations['field_value'].append(is_resource_provider_of)

    #
    # Reverse relation - isValidatorOf
    is_validator_of = create_inverse_relation_lr_from_lrs_display(
        Q(validation__validator=instance.pk),
        'Validated Language Resources and Technologies',
        context=context
    )
    if is_validator_of:
        reverse_relations['field_value'].append(is_validator_of)

    # if no relation is added, set to null
    if len(reverse_relations['field_value']) == 0:
        reverse_relations = None
    return reverse_relations


def generate_linguality_type(validated_data):
    # Generate linguality_type
    len_language = len(validated_data['language'])
    language_ids = [language['language_id'] for language in validated_data['language']]
    contains_collective_lang = [(language in collective_language_ids) for language in language_ids]
    contains_collective_lang = True if True in contains_collective_lang else False
    if len_language == 0:
        validated_data['linguality_type'] = None
    if len_language == 1 and not contains_collective_lang:
        validated_data['linguality_type'] = choices.LINGUALITY_TYPE_CHOICES.MONOLINGUAL
    elif len_language == 2 and not contains_collective_lang:
        validated_data['linguality_type'] = choices.LINGUALITY_TYPE_CHOICES.BILINGUAL
    elif len_language > 2 or contains_collective_lang:
        validated_data['linguality_type'] = choices.LINGUALITY_TYPE_CHOICES.MULTILINGUAL
    return validated_data
