import itertools
import logging
import re
from packaging import version as _ver

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import (
    URLValidator, EmailValidator, RegexValidator
)
from django.db.models import Q
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _
from language_tags import tags
from rest_framework import serializers

from registry import choices
from registry.utils.language_tag_utils import generate_temp_language_tag
from registry.utils.string_utils import equal_multilingual_dict

LOGGER = logging.getLogger(__name__)


# TODO Deprecated?
def validate_lang_code(code):
    if not tags.check(code):
        raise ValidationError(
            _(
                '{} is not a valid language code (https://tools.ietf.org/html/bcp47)'.format(code)))


# TODO are we using it somewhere?
def validate_lang_codes(code_dict):
    """
    BCP47 Validation
    Validates languages codes that are keys in a dict, e.g. JSONField (used by MultilingualField)
    """

    for key in code_dict.keys():
        validate_lang_code(key)


# TODO are we using it somewhere?
def validate_language_tag(tag):
    """
    BCP47 Validation
    Uses the language_tags package to verify that "tag" is valid IANA language tag
    """
    if tags.tag(tag).errors:
        messages = [err.message for err in tags.tag(tag).errors]
        raise ValidationError(messages)


# TODO are we using it somewhere?
def data_not_empty(data):
    """
    Checks if the passed data is empty or None. Use for non-empty ArrayFields and not empty JSONFields
    """
    if not data:
        raise ValidationError(_('This field requires at least one item'))


# TODO are we using it somewhere?
def validate_region_code(region):
    """
    BCP47 Validation
    Uses the language_tags package to verify that "region" is valid IANA region code
    """
    if not tags.region(region):
        raise ValidationError(
            _('{} is not a valid region code (https://tools.ietf.org/html/bcp47)'.format(region)))


# TODO are we using it somewhere?
def validate_script_code(script):
    """
    BCP47 Validation
    Uses the language_tags package to verify that "script" is valid IANA script code
    """
    if not tags.subtags(script.lower()):
        raise ValidationError(
            _('{} is not a valid script code (https://tools.ietf.org/html/bcp47)'.format(script)))


# TODO are we using it somewhere?
def validate_url(value):
    """
    Just reuse the built-in URLValidator to return a custom message (will appear in an API JSON Response)
    """
    url_validator = URLValidator()
    try:
        url_validator.__call__(value)
    except ValidationError:
        raise ValidationError(_("The value '{}' is not a valid url".
                                format(value)))


# TODO are we using it somewhere?
def validate_email(value):
    """
    Just reuse the built-in EmailValidator to return a custom message (will appear in an API JSON Response)
    """
    email_validator = EmailValidator()
    try:
        email_validator.__call__(value)
    except ValidationError:
        raise ValidationError(
            _("The value '{}' is not a valid email address".format(value)))


# TODO are we using it somewhere?
def validate_orcid(value):
    """
    Validates an orcid identifier string
    """
    # https://orcid.org/0000-000x-xxxx-xxxx.
    # https://orcid.org/0000-0001-2345-6789
    return RegexValidator(
        regex=r'^https:\/\/orcid.org\/0000-000\d-\d{4}-\d{3}[0-9X]$',
        message=_('Use the format: https://orcid.org/0000-000x-xxxx-xxxx.')
    )(value)


def validate_docker_image_reference(value):
    """
    Validates if the value is a valid docker image reference, that looks like [host.name/]image/name:version
    and not a URL. Raises ValidationError when value contains :/ or //
    :param value: the docker image reference
    :raises: ValidationError
    :returns: True if value is a valid docker image reference
    """
    if not isinstance(value, str):
        raise ValidationError(
            _("Expected value of type /str/.".format(value)),
            code='invalid')
    pattern = re.compile('(//)|(:/)')
    if pattern.search(value):
        raise ValidationError(f'"{value}" does not comply with the expected input. '
                              'Follow the template: [host.name/]image/name:version')
    return True


def validate_language_code(codes_dict):
    """
    Validates languages codes that are keys in a dict,
    e.g. JSONField (used by MultilingualField).
    Language tags are based on BCP 47 (RFC 5646) and
    the latest IANA language subtag registry.
    """

    invalid_codes = [code for code in codes_dict
                     if not tags.check(code)]
    if invalid_codes:
        invalid_codes_str = ', '.join('\'{}\''.format(inv_code)
                                      for inv_code in invalid_codes)
        if len(invalid_codes) == 1:
            raise ValidationError(
                _("Given BCP47: {} is not valid language code "
                  "(see also https://tools.ietf.org/html/bcp47)"
                  .format(invalid_codes_str)),
                code='invalid')
        else:
            raise ValidationError(
                _("Given BCP47: {} are not valid language codes "
                  "(see also https://tools.ietf.org/html/bcp47)"
                  .format(invalid_codes_str)),
                code='invalid')


def validate_required_tag(codes_dict):
    """
    Raises ValidationError if language code 'en' is not key in the
    codes' dict
    :param codes_dict: a dict whose keys are language tags
    :raises: ValidationError
    """
    if codes_dict and settings.REQUIRED_LANGUAGE_TAG not in codes_dict:
        raise ValidationError(
            _("Enter text in English. Must use '{}' language code."
              .format(settings.REQUIRED_LANGUAGE_TAG)),
            code='required_language_code')


def validate_value_str(codes_dict):
    """
    Raises ValidationError if value of language tag isn't a string
    :param codes_dict: a dict whose keys are language tags
    :raises: ValidationError
    """
    if isinstance(codes_dict, dict):
        for k, v in codes_dict.items():
            if not isinstance(v, str):
                raise ValidationError(
                    _("Value of key '{}' must be of type /str/.".format(k)),
                    code='invalid')


def validate_value_non_blank_str(codes_dict):
    """
    Raises ValidationError if value of language tag isn't a string
    :param codes_dict: a dict whose keys are language tags
    :raises: ValidationError
    """
    if isinstance(codes_dict, dict):
        for k, v in codes_dict.items():
            if isinstance(v, str) and len(v) == 0:
                raise ValidationError(
                    _("Value of key '{}' cannot be blank".format(k)),
                    code='invalid')


def validate_dict_existence(codes_dict):
    """
    Raises ValidationError if dict does not exist
    :param codes_dict: a dict whose keys are language tags
    :raises: ValidationError
    """
    if isinstance(codes_dict, dict) and len(codes_dict) == 0:
        raise ValidationError(
            _("This field is required."),
            code='invalid')


def validate_dict_value_length(length):
    """
    Raises ValidationError if dict does not exist
    :param length: max length for each language
    :raises: ValidationError
    """

    def validate_value_length(codes_dict):
        if isinstance(codes_dict, dict):
            for k, v in codes_dict.items():
                if isinstance(v, str) and len(v) > length:
                    raise ValidationError(
                        _("Value of key '{}' cannot be more than {} characters long.".format(k, length)),
                        code='invalid')

    return validate_value_length


def validate_version_regex_validator(value):
    """
    Raises ValidationError if value doesn't follow a specific regex expression
    :param value: a string
    :raises: ValidationError
    """
    if not isinstance(value, str):
        raise ValidationError(
            _("Expected value of type /str/.".format(value)),
            code='invalid')
    version = _ver.parse(value)
    pattern = re.compile(r'^(unspecified)?(([0-9]+\.)?([0-9]+\.)?[0-9]+?)?(1\.0\.0 \(automatically assigned\))?$')
    if not (isinstance(version, _ver.Version) or pattern.match(version.__str__())):
        raise ValidationError('"%s" does not comply with the expected input. '
                              'Follow the template: 1.0.0-alpha, if version is known, else enter: unspecified' % value)


def validate_unique_multiple_multilingual_field(value):
    """
    Raises ValidationError if value contains duplicates
    ie they have the same value for all their shared languages
    :param value: a string
    :raises: ValidationError
    """
    if not isinstance(value, list):
        raise ValidationError(
            _(f"Expected value of type /list/, but received {value} which is of type {type(value)}."),
            code='invalid')
    for pair in itertools.combinations(value, 2):
        if equal_multilingual_dict(*pair):
            raise ValidationError(
                _(f"List contains duplicate values: {pair}"),
                code='invalid')
    return True


def dataset_distribution_validation(data, media_part_field, media_type_field, media_part,
                                    dataset_distribution_media_field, message_feature, message_part):
    if data.get(media_part_field, None) and data.get('dataset_distribution', None):
        x = []
        for d in data[media_part_field]:
            if d.get(media_type_field, None) == media_part:
                x.append(1)

        if len(x) > 0 and not any(
                dist.get(dataset_distribution_media_field, None) for dist in data['dataset_distribution']):
            raise serializers.ValidationError(
                {dataset_distribution_media_field: _(message_feature)},
                code='invalid',
            )
        elif len(x) == 0 and any(
                dist.get(dataset_distribution_media_field, None) for dist in data['dataset_distribution']):
            raise serializers.ValidationError(
                {dataset_distribution_media_field: _(message_part)},
                code='invalid',
            )


def unique_language_validation(data, language_field):
    if data.get(language_field, None):
        val_list = []
        for lang in data[language_field]:
            tag = generate_temp_language_tag(lang)
            variety = lang.get('language_variety_name', None)
            glottolog = lang.get('glottolog_code', None)
            if tag:
                _var = variety.get('en') if variety else None
                val_list.append((('tag', tag), ('variety', _var), ('glottolog', glottolog)))
        if len(val_list) > len(set(val_list)):
            raise serializers.ValidationError(
                {language_field: _('Unique language_tag, language_variety, glottolog code are required between '
                                   'listed languages.')},
                code='invalid',
            )


def dataset_distribution_unspecified_part_validation(data):
    if data.get('unspecified_part', None):
        for d in data.get('dataset_distribution', []):
            if d.get('distribution_unspecified_feature', None) is None:
                raise serializers.ValidationError(
                    {'distribution_unspecified_feature': _('distribution_unspecified_feature cannot be empty')},
                    code='invalid',
                )


def unique_identifier_scheme_validation(data, entity_identifier, identifier_scheme):
    scheme_list = []
    if data.get(entity_identifier, None):
        if len(data.get(entity_identifier)) > 1:
            scheme_list = [i[identifier_scheme] for i in data[entity_identifier]]
    if len(scheme_list) > len(set(scheme_list)):
        raise serializers.ValidationError(
            {entity_identifier: _('Use unique identification schemes')},
            code='invalid',
        )


def unspecified_in_condition_of_use_validation(data, condition_of_use_field):
    if len(data[condition_of_use_field]) > 1 and \
            choices.CONDITION_OF_USE_CHOICES.UNSPECIFIED in data[condition_of_use_field]:
        raise serializers.ValidationError(
            {condition_of_use_field: _(f'{choices.CONDITION_OF_USE_CHOICES.UNSPECIFIED} must be used alone.')},
            code='invalid',
        )


def record_already_exists(entity_type, pk, data, allow_duplicates=False):
    from registry.models_identifier_mapping import sender_mapping_relation, sender_mapping_scheme
    from registry.models import MetadataRecord
    if entity_type not in ['LanguageResource', 'Project', 'Organization', 'LicenceTerms', 'Person']:
        return None
    resource_name_mapping = {
        'LanguageResource': 'resource_name',
        'Project': 'project_name',
        'Organization': 'organization_name',
        'LicenceTerms': 'licence_terms_name',
        'Person': 'surname'
    }
    resource_short_name_mapping = {
        'LanguageResource': 'resource_short_name',
        'Project': 'project_short_name',
        'Organization': 'organization_short_name',
        'LicenceTerms': 'licence_terms_short_name',
        'Person': 'given_name'
    }
    field_mapping = {
        entity_type: {
            'resource_name': data['described_entity'].get(resource_name_mapping[entity_type]),
            'resource_short_name': data['described_entity'].get(resource_short_name_mapping[entity_type]),
            'resource_identifier': data['described_entity'].get(sender_mapping_relation[entity_type]),
            'version': data['described_entity'].get('version', '1.0.0 (automatically assigned)'),
            'parent_name': data['described_entity'].get('is_division_of'),
            'website': data['described_entity'].get('website'),
            'email': data['described_entity'].get('email'),
            'grant_number': data['described_entity'].get('grant_number'),
            'licence_terms_url': data['described_entity'].get('licence_terms_url')
        }
    }
    fringe_field_mapping = {
        'Project': {'website': data['described_entity'].get('website'),
                    'grant_number': data['described_entity'].get('grant_number')},
        'Organization': {'website': data['described_entity'].get('website')}
    }
    query = None
    duplicate_query = None
    if allow_duplicates and data.get('source_of_metadata_record', {}).get('repository_name', {}).get('en'):
        duplicate_query = Q(
            source_of_metadata_record__repository_name__en=data['source_of_metadata_record']['repository_name']['en']) \
                          & Q(management_object__deleted=False)
    if field_mapping[entity_type]['resource_name']:
        resource_name_query_mapping = {
            'LanguageResource': {'resource_name': {'described_entity__languageresource__resource_name__en__iexact':
                                                       field_mapping[entity_type]['resource_name'].get('en'),
                                                   'described_entity__languageresource__version':
                                                       field_mapping[entity_type].get('version')
                                                   }
                                 },
            'Project': {'resource_name': {'described_entity__project__project_name__en__iexact':
                                              field_mapping[entity_type]['resource_name'].get('en')
                                          }
                        },
            'Organization': {'resource_name': {'described_entity__actor__organization__organization_name__en__iexact':
                                                   field_mapping[entity_type]['resource_name'].get('en')
                                               }
                             },
            'LicenceTerms': {'resource_name': {'described_entity__licenceterms__licence_terms_name__en__iexact':
                                                   field_mapping[entity_type]['resource_name'].get('en'),
                                               'described_entity__licenceterms__licence_terms_url__overlap':
                                                   field_mapping[entity_type].get('licence_terms_url')
                                               }
                             },
            'Person': {'resource_name': {'described_entity__actor__person__surname__en__iexact':
                                             field_mapping[entity_type]['resource_name'].get('en'),
                                         'described_entity__actor__person__email__overlap':
                                             field_mapping[entity_type].get('email')
                                         }
                       },
        }
        if entity_type == 'Organization' and field_mapping[entity_type].get('parent_name'):
            parent_names = []
            for parent in field_mapping[entity_type].get('parent_name'):
                parent_names.append(parent['organization_name'].get('en'))
            resource_name_query_mapping['Organization'][
                'resource_name'].update({
                'described_entity__actor__organization__is_division_of__organization_name__en__in':
                    parent_names})
        else:
            resource_name_query_mapping['Organization'][
                'resource_name'].update({'described_entity__actor__organization__is_division_of__isnull': True})
        query = Q(**resource_name_query_mapping[entity_type]['resource_name'])
    if field_mapping[entity_type]['resource_name'] and field_mapping[entity_type]['resource_short_name']:
        resource_short_name_query_mapping = {
            'LanguageResource': {'resource_short_name': 'described_entity__languageresource__resource_short_name__en__iexact',
                                 'version': {
                                     'described_entity__languageresource__version': field_mapping[entity_type].get(
                                         'version')
                                 }
                                 },
            'Project': {'resource_short_name': 'described_entity__project__project_short_name__en__iexact'},
            'Organization': {'resource_short_name':
                                 'described_entity__actor__organization__organization_short_name__icontains'},
            'LicenceTerms': {'resource_short_name': 'described_entity__licenceterms__licence_terms_short_name__en__iexact',
                             'licence_terms_url': {
                                               'described_entity__licenceterms__licence_terms_url__overlap':
                                                   field_mapping[entity_type].get('licence_terms_url')
                                               }
                             },
            'Person': {'resource_short_name': 'described_entity__actor__person__given_name__en__iexact',
                       'email': {
                                         'described_entity__actor__person__email__overlap':
                                             field_mapping[entity_type].get('email')
                                         }
                       },
        }
        resource_short_name_query = None
        if entity_type == 'LanguageResource':
            resource_short_name_query = (Q(
                **{resource_short_name_query_mapping[entity_type]['resource_short_name']: field_mapping[entity_type][
                    'resource_short_name'].get('en')
                   }) &
                                         Q(**resource_short_name_query_mapping[entity_type]['version']))
        elif entity_type == 'Project':
            resource_short_name_query = Q(
                **{resource_short_name_query_mapping[entity_type]['resource_short_name']: field_mapping[entity_type][
                    'resource_short_name'].get('en')
                   })
        elif entity_type == 'Organization':
            for short_name in field_mapping[entity_type]['resource_short_name']:
                query_dict = {resource_short_name_query_mapping[entity_type]['resource_short_name']:
                                  short_name.get('en')
                              }
                if field_mapping[entity_type].get('parent_name'):
                    parent_names = []
                    for parent in field_mapping[entity_type].get('parent_name'):
                        parent_names.append(parent['organization_name'].get('en'))
                    query_dict.update({
                        'described_entity__actor__organization__is_division_of__organization_name__en__in':
                            parent_names})
                else:
                    query_dict.update({
                        'described_entity__actor__organization__is_division_of__isnull': True})
                new_query = Q(
                    **query_dict
                )
                if not resource_short_name_query:
                    resource_short_name_query = new_query
                else:
                    resource_short_name_query = resource_short_name_query | new_query
        elif entity_type == 'LicenceTerms':
            resource_short_name_query = (Q(
                **{resource_short_name_query_mapping[entity_type]['resource_short_name']: field_mapping[entity_type][
                    'resource_short_name'].get('en')
                   }) &
                                         Q(**resource_short_name_query_mapping[entity_type]['licence_terms_url']))
        elif entity_type == 'Person':
            resource_short_name_query = (Q(
                **{resource_short_name_query_mapping[entity_type]['resource_short_name']: field_mapping[entity_type][
                    'resource_short_name'].get('en')
                   }) &
                                         Q(**resource_short_name_query_mapping[entity_type]['email']))
        query = query | resource_short_name_query
    if field_mapping[entity_type]['resource_name'] and field_mapping[entity_type]['resource_identifier']:
        identifier_schema_query_mapping = {
            'LanguageResource': {'scheme': 'described_entity__languageresource__lr_identifier__lr_identifier_scheme',
                                 'value': 'described_entity__languageresource__lr_identifier__value'},
            'Project': {'scheme': 'described_entity__project__project_identifier__project_identifier_scheme',
                        'value': 'described_entity__project__project_identifier__value'},
            'Organization': {
                'scheme': 'described_entity__actor__organization__organization_identifier__organization_identifier_scheme',
                'value': 'described_entity__actor__organization__organization_identifier__value'},
            'LicenceTerms': {
                'scheme': 'described_entity__licenceterms__licence_identifier__licence_identifier_scheme',
                'value': 'described_entity__licenceterms__licence_identifier__value'},
            'Person': {
                'scheme': 'described_entity__actor__person__personal_identifier__personal_identifier_scheme',
                'value': 'described_entity__actor__person__personal_identifier__value'},
        }
        resource_identifier_query = None
        for identifier in field_mapping[entity_type]['resource_identifier']:
            new_query = Q(Q(**{identifier_schema_query_mapping[entity_type]['scheme']: identifier.get(
                sender_mapping_scheme[entity_type])})
                          & Q(**{identifier_schema_query_mapping[entity_type]['value']: identifier.get('value')}))
            if not resource_identifier_query:
                resource_identifier_query = new_query
            else:
                resource_identifier_query = resource_identifier_query | new_query
        query = query | resource_identifier_query
        if duplicate_query:
            duplicate_query = duplicate_query & resource_identifier_query
    if field_mapping[entity_type]['resource_name'] and fringe_field_mapping.get(entity_type):
        fringe_query_mapping = {
            'Project': {'grant_number': 'described_entity__project__grant_number__iexact',
                        'website': 'described_entity__project__website__overlap'},
            'Organization': {'website':
                                 'described_entity__actor__organization__website__overlap'},
        }
        fringe_query = None
        for k, v in fringe_query_mapping[entity_type].items():
            if fringe_field_mapping[entity_type][k]:
                new_query = Q(**{v: fringe_field_mapping[entity_type][k]})
                if not fringe_query:
                    fringe_query = new_query
                else:
                    fringe_query = fringe_query | new_query
                query = query | fringe_query
    qr_list = []
    if query:
        query = query & Q(management_object__deleted=False)
        if duplicate_query:
            query = duplicate_query
        queryset = MetadataRecord.objects.filter(query).distinct()
        pk_to_exclude = set()
        if queryset \
                and entity_type == 'Organization' \
                and field_mapping[entity_type]['resource_short_name']:
            s_n = []
            for short_name in field_mapping[entity_type]['resource_short_name']:
                s_n.append(short_name.get('en'))
            for org in queryset:
                short_names = []
                if getattr(org, 'described_entity', None):
                    short_names = getattr(org.described_entity, 'organization_short_name', []) or []
                for short_name in short_names:
                    if short_name.get('en', '') not in s_n:
                        pk_to_exclude.add(org.id)
                        continue
        if pk:
            pk_to_exclude.add(pk)
            qr_list = [item for item in queryset.exclude(id__in=pk_to_exclude)]
        else:
            qr_list = [item for item in queryset.exclude(id__in=pk_to_exclude)]
    if qr_list:
        duplicate_names = []
        u_c_items = []
        for item in qr_list:
            if hasattr(item.described_entity, 'version'):
                duplicate_names.append([item.__str__(), item.described_entity.version])
            elif hasattr(item.described_entity, 'is_division_of') and item.described_entity.is_division_of.all():
                return_list = [item.__str__()]
                for parent in item.described_entity.is_division_of.all():
                    return_list.append(parent.organization_name.get('en'))
                duplicate_names.append(return_list)
            else:
                duplicate_names.append(item.__str__())
            if item.management_object.under_construction:
                u_c_items.append(item.__str__())
        error_dict = {'duplication_error': _('This record already exists.'),
                      'duplicate_records': _(f'{duplicate_names}'),
                      'possible_conflicts': {
                          'entity_name': _('records have the same name (and version if language resource)'),
                          'entity_short_name': _('records have the same short '
                                                 'name (and version if language resource)'),
                          'entity_identifier': _('records have the same identifier')
                      },
        }
        specific_conflicts = {
            'Project': _('At least one Project with the same website or grant number exists'),
            'LicenceTerms': _(
                'At least one Licence with the same name / url combination or short name / url combination exists'),
            'Person': _(
                'At least one Person with the same given_name / email combination or surname / email combination exists')
        }
        if specific_conflicts.get(entity_type):
            error_dict.update({'entity_specific_conflicts': specific_conflicts[entity_type]})
        if u_c_items:
            u_c_item_name = ', '.join(u_c_items)
            error_dict.update({
                'notice': _(
                    f'{f"The record {u_c_item_name} has" if len(u_c_items) == 1 else f"The records {u_c_item_name} have"}'
                    f' the under construction status. If you are looking to add the complete version'
                    f' please request the unpublication of the record, so it can be edited and'
                    f' submitted as such.')
            })

        raise serializers.ValidationError(error_dict,
                                          code='invalid',
                                          )
    return None


def validate_metadata_record_with_data(obj):
    """
    A validation
    :param obj: the record to check if valid
    :return: a tuple with whether the record is valid or not and the appropriate error message
    """
    # Record no LR or has no data
    if obj.described_entity.entity_type != 'LanguageResource' or not obj.management_object.has_content_file:
        return True, None
    # Check that content_files are connected with appropriate distributions
    for data in obj.management_object.content_files.all():
        if obj.described_entity.lr_subclass.lr_type == 'ToolService':
            if hasattr(data, 'software_distributions') and \
                    len(data.software_distributions.all()) == 0:
                return False, _(f'Invalid record. The record has data not related with any Software Distribution')
            for distribution in data.software_distributions.all():
                if distribution.software_distribution_form not in [
                    choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.LIBRARY,
                    choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.PLUGIN,
                    choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE,
                    choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_CODE,
                    choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WORKFLOW_FILE
                ]:
                    return False, _(f'Invalid record. The record has data and must be related with a '
                                    f'Software Distribution with software distribution form in '
                                    f'[{choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.LIBRARY},'
                                    f'{choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.PLUGIN},'
                                    f'{choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_AND_EXECUTABLE_CODE},'
                                    f'{choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.SOURCE_CODE},'
                                    f'{choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES.WORKFLOW_FILE}].'
                                    )
                else:
                    # Check tha appropriate distribution is also a distribution of the record
                    if distribution not in obj.described_entity.lr_subclass.software_distribution.all():
                        return False, _(f'Invalid record. The related software distribution is '
                                        f'not a distribution of the record')

        else:
            if hasattr(data, 'dataset_distributions') and \
                    len(data.dataset_distributions.all()) == 0:
                return False, _(f'Invalid record. The record has data not related with any Dataset Distribution')
            for distribution in data.dataset_distributions.all():
                if distribution.dataset_distribution_form != choices.DATASET_DISTRIBUTION_FORM_CHOICES.DOWNLOADABLE:
                    return False, _(f'Invalid record. The record has data and must be related with a '
                                    f'Dataset Distribution with dataset distribution form='
                                    f'{choices.DATASET_DISTRIBUTION_FORM_CHOICES.DOWNLOADABLE}')
                else:
                    # Check tha appropriate distribution is also a distribution of the record
                    if distribution not in obj.described_entity.lr_subclass.dataset_distribution.all():
                        return False, _(f'Invalid record. The related dataset distribution is '
                                        f'not a distribution of the record')
    return True, None


@deconstructible
class ValidateUniquenessInRecommendedControlVocabulary:
    def __init__(self, choices_cv):
        self.choices = choices_cv

    def __call__(self, value):
        if isinstance(value, list):
            transformed_values = set()
            for v in value:
                v_label = v
                if v in self.choices:
                    v_label = self.choices[v]
                v_label = v_label.strip().lower()
                if v_label in transformed_values:
                    raise ValidationError(
                        _("Contains duplicate values."),
                        code='invalid')
                else:
                    transformed_values.add(v_label)

    def __eq__(self, other):
        return (
                isinstance(other, ValidateUniquenessInRecommendedControlVocabulary) and
                self.choices == other.choices
        )


validate_unique_annotation_type = ValidateUniquenessInRecommendedControlVocabulary(
    choices.ANNOTATION_TYPE_RECOMMENDED_CHOICES)

validate_unique_lt_class = ValidateUniquenessInRecommendedControlVocabulary(choices.LT_CLASS_RECOMMENDED_CHOICES)

validate_unique_data_format = ValidateUniquenessInRecommendedControlVocabulary(choices.DATA_FORMAT_RECOMMENDED_CHOICES)

validate_unique_model_type = ValidateUniquenessInRecommendedControlVocabulary(choices.MODEL_TYPE_RECOMMENDED_CHOICES)

validate_unique_model_function = ValidateUniquenessInRecommendedControlVocabulary(
    choices.MODEL_FUNCTION_RECOMMENDED_CHOICES)

validate_unique_development_framework = ValidateUniquenessInRecommendedControlVocabulary(
    choices.DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES)


