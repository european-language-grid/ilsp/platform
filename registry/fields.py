import json
from packaging import version as _ver

from django.contrib.postgres.fields import JSONField
from django.db import models
from rest_framework import serializers

from registry import (
    choices_old, validators
)
from utils.string_utils import to_snake_case


class VersionField(models.CharField):
    """
    CharField that needs to follow a specific regex
    """
    description = 'Dict having IANA language codes keys'

    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [validators.validate_version_regex_validator]
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        if value is None:
            return value
        version = _ver.parse(value)
        _val = version.__str__()
        return _val


class SerializersVersionField(serializers.CharField):
    """
    CharField that needs to follow a specific regex
    """
    description = 'Dict having IANA language codes keys'

    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [validators.validate_version_regex_validator]
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        # We're lenient with allowing basic numerics to be coerced into strings,
        # but other types should fail. Eg. unclear if booleans should represent as `true` or `True`,
        # and composites such as lists are likely user error.
        if isinstance(data, bool) or not isinstance(data, (str, int, float,)):
            self.fail('invalid')
        value = str(data)
        version = _ver.parse(value)
        version_value = version.__str__()
        return version_value.strip() if self.trim_whitespace else version_value

    def to_representation(self, value):
        version = _ver.parse(value)
        version_value = version.__str__()
        return version_value


class MultilingualField(JSONField):
    """
    JSONField whose keys are IANA language code tags
    """
    description = 'Dict having IANA language codes keys'

    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [validators.validate_language_code,
                                validators.validate_required_tag,
                                validators.validate_value_str,
                                validators.validate_value_non_blank_str]
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        if value is None:
            return value
        val_dict = {}
        for k, v in value.items():
            if isinstance(v, str):
                val_dict[k] = v.strip()
            else:
                val_dict[k] = str(v).strip()
        return val_dict


class SerializersMultilingualField(serializers.JSONField):
    """
    Serializer for JSONField whose keys are IANA language code tags
    """
    description = 'Dict having IANA language codes keys'

    def __init__(self, *args, **kwargs):
        vals = [validators.validate_language_code,
                validators.validate_required_tag,
                validators.validate_value_str,
                validators.validate_value_non_blank_str]
        if kwargs.get('validators'):
            kwargs['validators'] = kwargs['validators'] + vals
        else:
            kwargs['validators'] = vals
        super().__init__(*args, **kwargs)

    def to_internal_value(self, data):
        try:
            if isinstance(data, dict):
                val_data = {}
                for k, v in data.items():
                    if isinstance(v, str):
                        val_data[k] = v.strip()
                data = val_data
            if self.binary or getattr(data, 'is_json_string', False):
                if isinstance(data, bytes):
                    data = data.decode()
                return json.loads(data)
            else:
                json.dumps(data, cls=self.encoder)
        except (TypeError, ValueError):
            self.fail('invalid')
        return data

    def to_representation(self, value):
        if value is None:
            return value
        if self.binary:
            value = json.dumps(value, cls=self.encoder)
            value = value.encode()
            return value
        if isinstance(value, dict):
            val_dict = {}
            for k, v in value.items():
                if isinstance(v, str):
                    val_dict[k] = v.strip()
                else:
                    val_dict[k] = str(v).strip()
            return val_dict


# Deprecated Code
class CharField(models.CharField):
    """
    CharField stripping leading and trailing whitespace
    """
    def to_python(self, value):
        if not value:
            return value
        if isinstance(value, str):
            _val = value.strip()
            return _val
        else:
            return str(value).strip()


# Deprecated Code
class SerializersCharField(serializers.CharField):
    """
    CharField that needs to follow a specific regex
    """
    def to_internal_value(self, data):
        # We're lenient with allowing basic numerics to be coerced into strings,
        # but other types should fail. Eg. unclear if booleans should represent as `true` or `True`,
        # and composites such as lists are likely user error.
        if isinstance(data, bool) or not isinstance(data, (str, int, float,)):
            self.fail('invalid')
        value = str(data).strip()
        return value

    def to_representation(self, value):
        value = str(value).strip()
        return value


# Deprecated Code - Not to be used
class EnumeratedURLField(models.URLField):
    """
    URLField that takes choices from the ELG-Share xsd schema
    """
    description = 'URL that takes values from the ELG-SHARE xsd schema'

    def __init__(self, element='', *args, **kwargs):
        self.element = element
        vocab = getattr(choices_old, f'{to_snake_case(self.element).upper()}_CHOICES_OLD')
        kwargs['choices'] = getattr(vocab, 'choices')
        kwargs['max_length'] = getattr(vocab, 'max_length')

        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if self.element:
            kwargs['element'] = self.element
        return name, path, args, kwargs


# Deprecated Code - Not to be used
class EnumeratedCharField(models.CharField):
    """
    CharField that takes choices from the ELG-Share xsd schema
    """
    description = 'String that takes values from the ELG-Share xsd schema'

    def __init__(self, element='', *args, **kwargs):
        self.element = element
        vocab = getattr(choices_old, f'{to_snake_case(self.element).upper()}_CHOICES_OLD')
        kwargs['choices'] = getattr(vocab, 'choices')
        kwargs['max_length'] = getattr(vocab, 'max_length')

        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if self.element:
            kwargs['element'] = self.element
        return name, path, args, kwargs
