import re
import bleach
from hashids import Hashids

ALLOWED_TAGS = ['a', 'li', 'ol', 'p', 'ul', 'b', 'i', 'u', 'strong', 'em', 'code', 'hr', 'blockquote', 'pre', 'h3',
                'h2', 'br', 'sub', 'strike', 'caption', 'table', 'tbody', 'thead', 'th', 'td', 'tr', 'notes-element']
ALLOWED_ATTRS = {
    'a': ['href', 'name', 'target']
}
ALLOWED_PROTOCOLS = ['http', 'https']


def _handle_spaced_tags(value, taglist, newlines):
    if taglist:
        return value
    else:
        if newlines:
            return value.replace('</li>', '\n').replace('</tr>', '\n')
    return value.replace('</p>', ' ').replace('</li>', ' ').replace('</tr>', ' ')


def sanitize_field(field, tags=[], attrs={}, protocols=[], keep_new_lines=False, handle_nbsp=False):
    """
    Cleans input text from html tags, leading and trailing spaces, normalizes spaces and removes linebreaks.
    The function handles both simple strings and dict values
    """
    if isinstance(field, dict):
        for k, v in field.items():
            field[k] = sanitize_field(_handle_spaced_tags(v, tags, keep_new_lines), tags=tags, attrs=attrs,
                                      protocols=protocols, keep_new_lines=True, handle_nbsp=handle_nbsp)
        output = field
    else:
        if keep_new_lines:
            output = re.sub(" {2,}", " ",
                            bleach.clean(_handle_spaced_tags(field, tags, keep_new_lines),
                                         strip=True, strip_comments=True, tags=tags, attributes=attrs,
                                         protocols=protocols).strip())
        else:
            output = re.sub(" {2,}", " ",
                            bleach.clean(_handle_spaced_tags(field, tags, keep_new_lines),
                                         strip=True, strip_comments=True, tags=tags, attributes=attrs,
                                         protocols=protocols).replace('\n', ' ').strip())
        if handle_nbsp:
            output = output.replace('&nbsp;', ' ')
        output = re.sub(" {2,}", " ", output)
    return output


hashids = Hashids(min_length=8)


def h_encode(id):
    return hashids.encode(id)


def h_decode(h):
    z = hashids.decode(h)
    if z:
        return z[0]


class HashIdConverter:
    regex = '[a-zA-Z0-9]{8,}'

    def to_python(self, value):
        return h_decode(value)

    def to_url(self, value):
        return h_encode(value)


def equal_multilingual_dict(multilang_dict1, multilang_dict2):
    '''
    Two Multilingual dictionaries are equal if they have the same value for
    all their shared languages
    '''
    shared_keys = multilang_dict1.keys() & multilang_dict2.keys()
    for key in shared_keys:
        if multilang_dict1[key].strip().lower() != multilang_dict2[key].strip().lower():
            return False
    return True


