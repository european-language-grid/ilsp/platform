import language_tags
from language_tags import tags


def get_bcp47_choices(tag_type):
    result = list()
    for tag in language_tags.data.get(tag_type):
        try:
            accessor = '_'.join(language_tags.tags.description(tag)[0].split(' ')).upper()
            label = '; '.join(language_tags.tags.description(tag))
            if tag_type == 'script':
                tag = tag.capitalize()
            elif tag_type == 'region':
                label = language_tags.tags.region(tag).description[0]
                tag = tag.upper()
            if not tag_type == 'language' \
                    or (tag_type == 'language' and not language_tags.tags.language(tag).deprecated):
                result.append(tuple([tag, accessor, label]))
        except IndexError:
            continue
    return result


def generate_temp_language_tag(data):
    language_tag = f'{data.get("language_id", "")}'
    if data.get('script_id'):
        if str(tags.language(data.get('language_id', "")).script) != data.get('script_id'):
            language_tag += f'-{data.get("script_id")}'
    if data.get('region_id'):
        language_tag += f'-{data.get("region_id")}'
    if data.get('variant_id'):
        if len(data.get('variant_id', [])) == 1:
            language_tag += f'-{data["variant_id"][0]}'
        else:
            language_tag += "-".join(data['variant_id'])
    return language_tag


collective_language_ids = [
    'aav',
    'afa',
    'alg',
    'alv',
    'apa',
    'aqa',
    'aql',
    'art',
    'ath',
    'auf',
    'aus',
    'awd',
    'azc',
    'bad',
    'bai',
    'bat',
    'ber',
    'bih',
    'bnt',
    'btk',
    'cai',
    'cau',
    'cba',
    'ccn',
    'ccs',
    'cdc',
    'cdd',
    'cel',
    'cmc',
    'cpe',
    'cpf',
    'cpp',
    'crp',
    'csu',
    'cus',
    'day',
    'dmn',
    'dra',
    'egx',
    'esx',
    'euq',
    'fiu',
    'fox',
    'gem',
    'gme',
    'gmq',
    'gmw',
    'grk',
    'him',
    'hmx',
    'hok',
    'hyx',
    'iir',
    'ijo',
    'inc',
    'ine',
    'ira',
    'iro',
    'itc',
    'jpx',
    'kar',
    'kdo',
    'khi',
    'kro',
    'map',
    'mkh',
    'mno',
    'mul',
    'mun',
    'myn',
    'nah',
    'nai',
    'ngf',
    'nic',
    'nub',
    'omq',
    'omv',
    'oto',
    'paa',
    'phi',
    'plf',
    'poz',
    'pqe',
    'pqw',
    'pra',
    'qwe',
    'roa',
    'sai',
    'sal',
    'sdv',
    'sem',
    'sgn',
    'sio',
    'sit',
    'sla',
    'smi',
    'son',
    'sqj',
    'ssa',
    'syd',
    'tai',
    'tbq',
    'trk',
    'tup',
    'tut',
    'tuw',
    'urj',
    'wak',
    'wen',
    'xgn',
    'xnd',
    'ypk',
    'zhx',
    'zle',
    'zls',
    'zlw',
    'znd'

]
