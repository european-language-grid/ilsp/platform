from collections import Counter
from difflib import SequenceMatcher

from django.db.models import Q
from packaging import version as _ver

from django.conf import settings

from management.models import PUBLISHED
from processing.models import TOOL_TYPE
from registry import choices
from registry.models import MetadataRecord
from registry.models_identifier_mapping import sender_mapping_scheme

from utils.string_utils import to_snake_case
from urllib.parse import quote_plus, urljoin


def field_choices_to_list(model_name):
    scheme = sender_mapping_scheme[model_name]
    scheme_choices = getattr(choices, f'{to_snake_case(scheme).upper()}_CHOICES')
    choices_list = list()
    for i in scheme_choices:
        choices_list.append(i[0])
    return choices_list


def retrieve_default_lang(dictionary):
    """
    Retrieve the value in the required language tag from a language field
    If none is found retrieve the next available
    :param dictionary: the language field
    :return:
    """
    try:
        return dictionary[settings.REQUIRED_LANGUAGE_TAG]
    except KeyError:
        return next(iter(dictionary.values()))


def get_similarity(query, resource_name, qr_short_name):
    similarity = []
    for name in resource_name.values():
        similarity.append(SequenceMatcher(None, name.lower(),
                                          query.lower()).ratio())

    if qr_short_name:
        if isinstance(qr_short_name, dict):
            for name in qr_short_name.values():
                similarity.append(SequenceMatcher(None, name.lower(),
                                                  query.lower()).ratio())
        elif isinstance(qr_short_name, list):
            for short_name in qr_short_name:
                for name in short_name.values():
                    similarity.append(SequenceMatcher(None, name.lower(),
                                                      query.lower()).ratio())
    return max(similarity)


def get_lt_service_info(instance):
    service_instance = instance
    if instance.management_object.service_compliant_dataset:
        distributions = instance.described_entity.lr_subclass.dataset_distribution.all()
        queried_by_instances = []
        for dist in distributions:
            if dist.dataset_distribution_form == choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_QUERY:
                for lr in dist.is_queried_by.all():
                    queried_by_instances.append(lr.pk)
        service_instance = MetadataRecord.objects.filter(described_entity__id__in=queried_by_instances,
                                                         management_object__status='p',
                                                         management_object__functional_service=True).first()
    accessor_id = service_instance.lt_service.accessor_id if service_instance.management_object.is_latest_version \
        else service_instance.lt_service.accessor_id + f'?version={quote_plus(service_instance.described_entity.version)}'
    url = \
        f'{settings.ROOT_URL}/execution/async/' \
        f'process/' \
        f'{accessor_id}'
    sync_url = \
        f'{settings.ROOT_URL}/execution/' \
        f'process/' \
        f'{accessor_id}'

    return_dict = {
        'elg_gui_url': urljoin(settings.ROOT_URL, service_instance.lt_service.elg_gui_url),
        'elg_execution_location': url,
        'elg_execution_location_sync': sync_url,
        'tool_type': TOOL_TYPE._display_map.get(
            service_instance.lt_service.tool_type)
    }
    return return_dict


def get_unlisted_versions(instance, replaces, is_replaced_with):
    entity_type = instance.described_entity.entity_type
    if not entity_type == 'LanguageResource':
        return None
    resource_name = instance.described_entity.resource_name.get('en')
    instance_version = instance.described_entity.version
    if not resource_name:
        return None

    exclude_pks = []
    for dup in instance.described_entity.is_exact_match_with.filter(metadata_record_of_described_entity__isnull=False):
        exclude_pks.append(dup.metadata_record_of_described_entity.pk)
    for p_d in instance.described_entity.is_potential_duplicate_with.filter(
            metadata_record_of_described_entity__isnull=False):
        exclude_pks.append(p_d.metadata_record_of_described_entity.pk)

    unlisted_versions = MetadataRecord.objects.filter(
        (Q(described_entity__languageresource__resource_name__en=resource_name) &
        Q(management_object__status=PUBLISHED))).exclude((Q(pk__in=exclude_pks) |
                                                     Q(described_entity__languageresource__version=instance_version)))
    newer_versions = []
    older_versions = []
    for version_record in unlisted_versions:
        if _ver.parse(instance_version) < _ver.parse(
                version_record.described_entity.version):
            newer_versions.append(version_record.described_entity)
        else:
            older_versions.append(version_record.described_entity)
    for entity in newer_versions:
        if entity not in instance.described_entity.is_replaced_with.all():
            instance.described_entity.is_replaced_with.add(entity)
        if not is_replaced_with:
            is_replaced_with.append(entity)
    for entity in older_versions:
        if entity not in instance.described_entity.replaces.all():
            instance.described_entity.replaces.add(entity)
        if not replaces:
            replaces.append(entity)

