import itertools
import logging
from collections import OrderedDict

from django.contrib.auth import get_user_model

from dle.dle_report_index.documents import DLEReportDocument
from email_notifications.tasks import personal_info_consent_email
from management.catalog_report_index.documents import CatalogReportDocument
from management.management_dashboard_index.documents import MyResourcesDocument, MyValidationsDocument
from management.utils.publication_field_generation import AutomaticPublicationFieldGeneration
from registry.models import MetadataRecord
from registry.search_indexes.documents import MetadataDocument

LOGGER = logging.getLogger(__name__)


def order_after_serialization(mapping):
    if isinstance(mapping, dict) or isinstance(mapping, OrderedDict):
        for k in mapping:
            if isinstance(mapping[k], dict):
                mapping[k] = OrderedDict(mapping[k])
            order_after_serialization(mapping[k])
            if isinstance(mapping[k], list):
                for element in mapping[k]:
                    if isinstance(element, dict):
                        order_after_serialization(element)
    return mapping


def return_metadatarecord_landing_page(instance, display_uncensored):
    keys = ['pk' if field.name == 'id' else field.name for field in MetadataRecord._meta.fields]
    landing_page = instance.management_object.landing_page_display \
        if not display_uncensored else instance.management_object.uncensored_landing_page_display
    data = OrderedDict()
    for key in keys:
        data[key] = landing_page[key]
    return dict(data)


def handle_consent_email_upon_creation(instance, name, surname, entity):
    if instance.email:
        users = get_user_model().objects.filter(username__in=instance.email).values_list('username', flat=True)
        emails = [email for email in instance.email if email not in users
                  and not entity.objects.filter(email__icontains=email).exclude(id=instance.id).exists()]
        if emails:
            personal_info_consent_email.apply_async(args=[name,
                                                          surname,
                                                          emails])
        else:
            for mail in instance.email:
                consent_qrs = [entity.objects.filter(email__icontains=mail).values_list('consent', flat=True)]
                consent_list = list(itertools.chain.from_iterable(consent_qrs))
                if False in consent_list:
                    instance.consent = False
                    break


def update_affected_md_records(records_to_fix):
    # Update view pages for records_fixed
    records = MetadataRecord.objects.filter(id__in=records_to_fix)
    records_to_fix_published = set()
    managements_to_fix_published = set()
    managements_to_fix = set()
    for record in records:
        LOGGER.debug(f'Update record {record.pk} - {record}')
        managements_to_fix.add(record.management_object)
        if record.management_object.status == 'p':
            apf = AutomaticPublicationFieldGeneration(record)
            apf.generate_lr_fields('citation_text')
            apf.generate_json_ld()
            apf.generate_landing_page(save_manager=True)
            records_to_fix_published.add(record)
            managements_to_fix_published.add(record.management_object)
    # Update indices
    LOGGER.debug(f'Update indices for affected records')
    MetadataDocument().catalogue_update(list(records_to_fix_published))
    CatalogReportDocument().catalogue_report_update(list(managements_to_fix_published))
    DLEReportDocument().dle_report_update(list(records_to_fix_published))
    MyResourcesDocument().update(list(managements_to_fix))
    MyValidationsDocument().update(list(managements_to_fix))
