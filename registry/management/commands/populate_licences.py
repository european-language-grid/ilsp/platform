from django.core.exceptions import ObjectDoesNotExist
from django.core.management import BaseCommand

from accounts.models import ELGUser
from email_notifications.email_notification_utils import spdx_licences_add
from registry.models import (
    Person, PersonalIdentifier, LicenceTerms,
    LicenceIdentifier, MetadataRecord
)

import json
import logging
import pprint

from registry.serializers import MetadataRecordSerializer, GenericPersonSerializer

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Populate licence from SPDX'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            "path_to_spdx_repo",
            type=str,
            help="The path to the spdx cloned repo to work on"
        )

    def handle(self, *args, **options):
        spdx_licences_file = options['path_to_spdx_repo'] + '/json/licenses.json'
        LOGGER.info(f'Open file {spdx_licences_file}')
        # Open SPDX Json file that contains all licences
        try:
            with open(spdx_licences_file) as json_file:
                loaded_licences = json.load(json_file)
        except FileNotFoundError as error:
            LOGGER.info(error)
            return
        # Set curator
        try:
            curator = Person.objects.get(given_name__en='ELG', surname__en='SYSTEM')
            LOGGER.info(f'Person "ELG SYSTEM" found')
            curator_serializer = GenericPersonSerializer(curator)
        except ObjectDoesNotExist:
            LOGGER.info(f'Person "ELG SYSTEM" not found. Creating...')
            curator_serializer = GenericPersonSerializer(
                data={
                    'given_name': {'en': 'ELG'},
                    'surname': {'en': 'SYSTEM'}
                }
            )
            curator_serializer.is_valid()
            curator_serializer.save()
        # For each not deprecated licence_spdx generate MetadataRecord json and use serializer to add
        generated_licences = list()
        count = 0
        for licence_spdx in loaded_licences['licenses']:
            if not licence_spdx['isDeprecatedLicenseId']:
                LOGGER.info(licence_spdx['name'])
                pprint.pprint(licence_spdx)
                licence_json = dict()
                # model
                licence_json['metadata_curator'] = [curator_serializer.data]
                licence_json['metadata_creator'] = curator_serializer.data
                licence_json['described_entity'] = dict()
                licence_json['described_entity']['entity_type'] = 'LicenceTerms'
                # licence_terms_name
                licence_json['described_entity']['licence_terms_name'] = {'en': licence_spdx['name']}
                # licence_terms_short_name
                licence_json['described_entity']['licence_terms_short_name'] = {'en': licence_spdx['licenseId']}
                # licence_terms_url
                licence_json['described_entity']['licence_terms_url'] = licence_spdx['seeAlso']
                licence_json['described_entity']['condition_of_use'] = ['http://w3id.org/meta-share/meta-share/unspecified']
                # licence_identifier
                licence_json['described_entity']['licence_identifier'] = list()
                licence_id_json = dict()
                licence_id_json['value'] = licence_spdx['licenseId']
                licence_id_json['licence_identifier_scheme'] = 'http://w3id.org/meta-share/meta-share/SPDX'
                licence_json['described_entity']['licence_identifier'].append(licence_id_json)
                licence_json['management_object'] = {}
                #pprint.pprint(licence_json)
                licence_serializer = MetadataRecordSerializer(data=licence_json)
                if licence_serializer.is_valid():
                    licence_instance = licence_serializer.save()
                    print(f'Saved in pk {licence_instance.pk}')
                    manager = licence_instance.management_object
                    manager.curator = ELGUser.objects.get(username='elg-system')
                    manager.save()
                    # Publish licence record
                    licence_instance.management_object.publish(force=True)
                    generated_licences.append(licence_instance)
                else:
                    print(licence_serializer.errors)
        LOGGER.info(f'Added {len(generated_licences)} licences')
        spdx_licences_add(generated_licences)

