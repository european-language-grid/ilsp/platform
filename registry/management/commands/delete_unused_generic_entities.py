import logging

from django.core.management import BaseCommand
from django.db.transaction import atomic

from registry.models import DescribedEntity
from registry.serializer_tasks import GetRelatedRecords

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Delete unused generic entities"

    def add_arguments(self, parser):
        parser.add_argument(
            "--write",
            action="store_true",
            default=False,
            help="Actually edit the database",
        )

    @atomic
    def handle(self, *args, write, **kwargs):
        if not write:
            self.stdout.write("In dry run mode (--write not passed)")

        unused_de_pks = self.find_unused_described_entities()
        unused_des = DescribedEntity.objects.filter(pk__in=unused_de_pks)

        if write:
            deleted_entities = {}
            entity_list = ['LanguageResource', 'LicenceTerms', 'Document', 'Organization',
                           'Group', 'Person', 'Project', 'Repository']
            for entity in entity_list:
                unused_entities = unused_des.filter(entity_type=entity)
                result = unused_entities.delete()
                deleted_entities[entity] = result
            self.stdout.write(f"Deleted {deleted_entities}")

    def find_unused_described_entities(self):
        # Find generic described entities, aka no related MetadatRecord
        print('Retrieving generic described entities')
        generic_des = DescribedEntity.objects.filter(metadata_record_of_described_entity__isnull=True).order_by('pk')
        print(f'Retrieved {len(generic_des)} generic described entities')
        generic_des_pks_to_delete = set()
        for gde in generic_des:
            # find if used
            get_related_instances = GetRelatedRecords(gde)
            related_instances = get_related_instances.get_all_fields()
            if len(related_instances) == 0:
                if gde.entity_type == 'Person':
                    if getattr(gde.actor.person, 'user', None) is None:
                        generic_des_pks_to_delete.add(gde.pk)
                        print(f'{gde.entity_type} - {gde.pk} - {gde} added to deleted')
                else:
                    generic_des_pks_to_delete.add(gde.pk)
                    print(f'{gde.entity_type} - {gde.pk} - {gde} added to deleted')
        print(f'{len(generic_des_pks_to_delete)} unused generic entities found')
        return generic_des_pks_to_delete
