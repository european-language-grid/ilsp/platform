import json

from django.core.management import BaseCommand

from oai.zenodo_utils import HarvestZenodo


class Command(BaseCommand):
    help = 'initiate zenodo harvesting'

    def handle(self, *args, **options):
        z_h = HarvestZenodo()
        z_h.initiate_zenodo_harvesting()


