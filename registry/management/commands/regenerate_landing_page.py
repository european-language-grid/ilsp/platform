import json

from django.core.management import BaseCommand

from management.models import PUBLISHED
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer
from utils.encoding import LazyEncoder
# loading gc
import gc

class Command(BaseCommand):
    help = 'Regenerate precalculated landing pages of published ' \
           'records of type: [LanguageResource, Project, Organization]'

    def add_arguments(self, parser):
        parser.add_argument(
            "pk",
            type=int,
            help="PK given to as a starting point to query records.",
            nargs='?'
        )
        parser.add_argument(
            "--start_at",
            action='store_true',
            default=False,
            help="Query records added after the given pk.",
        )

    def handle(self, *args, start_at, pk, **options):

        records_to_update = MetadataRecord.objects.filter(
            described_entity__entity_type__in=['LanguageResource', 'Project', 'Organization'],
            management_object__status__in=[PUBLISHED],
            management_object__deleted=False).order_by('pk')

        if start_at:
            self.stdout.write(f"Starting from record {pk} (--start_at passed)")
            records_to_update = records_to_update.filter(pk__gte=pk).order_by('pk')

        count = 0
        for record in records_to_update:
            self.stdout.write(self.style.SUCCESS(f'{count}/{len(records_to_update)} - Regenerating landing page for record {record.pk} - {record}'))
            record.management_object.generate_landing_page_display()
            count = count + 1
            # Add manual calling garbage collector to avoid killing the script
            if count%100 == 0:
                gc.collect()


