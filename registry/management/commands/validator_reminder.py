import logging
from smtplib import SMTPRecipientsRefused

from django.conf import settings
from django.core.mail import send_mail
from django.core.management import BaseCommand
from django.utils import timezone

from accounts.models import ELGUser
from utils.sorting_utils import sort_list_of_tuples
from management.models import Validator, INGESTED

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Send email reminder to validators'

    def handle(self, *args, **options):
        LOGGER.info('Sending pending validation notifications')
        validators = ELGUser.objects.filter(groups__name__in=['legal_validator',
                                                              'metadata_validator',
                                                              'technical_validator'])
        reminder_dict = dict()
        for validator in validators:
            reminder_dict[validator.email] = dict()
            legal_validations = []
            metadata_validations = []
            technical_validations = []
            validator_queryset = Validator.objects.filter(user=validator)
            for val in validator_queryset:
                if val.assigned_for == 'Legal':
                    for manager in val.assigned_to.all():
                        if manager.legal_validator != validator:
                            val.assigned_to.remove(manager.id)
                        else:
                            if manager.status == INGESTED \
                                    and manager.deleted is False \
                                    and manager.legally_valid is None:
                                legal_validations.append((manager, (timezone.now() - manager.ingestion_date).days))
                if val.assigned_for == 'Metadata':
                    for manager in val.assigned_to.all():
                        if manager.metadata_validator != validator:
                            val.assigned_to.remove(manager.id)
                        else:
                            if manager.status == INGESTED \
                                    and manager.deleted is False \
                                    and (manager.metadata_valid is None
                                         or (manager.has_content_file and manager.technically_valid is None)):
                                metadata_validations.append((manager, (timezone.now() - manager.ingestion_date).days))
                if val.assigned_for == 'Technical':
                    for manager in val.assigned_to.all():
                        if manager.technical_validator != validator:
                            val.assigned_to.remove(manager.id)
                        else:
                            if manager.status == INGESTED \
                                    and manager.deleted is False \
                                    and (manager.technically_valid is None
                                         or (manager.functional_service and manager.metadata_valid is None)):
                                technical_validations.append((manager, (timezone.now() - manager.ingestion_date).days))
                val.save()
            reminder_dict[validator.email].update({
                'username': validator.username,
                'validations': {
                    'legal_validations': legal_validations,
                    'metadata_validations': metadata_validations,
                    'technical_validations': technical_validations
                }
            })
        for email in reminder_dict.keys():
            sum_of_validations = len(reminder_dict[email]['validations']['legal_validations']) \
                                 + len(reminder_dict[email]['validations']['metadata_validations']) \
                                 + len(reminder_dict[email]['validations']['technical_validations'])

            pending_legal_validations = ''
            for i, j in sort_list_of_tuples(reminder_dict[email]['validations']['legal_validations']):
                vld_type = 'Legal Validation'
                pending_legal_validations += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; [{vld_type}] for ' \
                                             f'<a href={i.metadata_record_of_manager.get_display_url()}?auth=true>' \
                                             f'{i.metadata_record_of_manager.__str__()} ' \
                                             f'(id: {i.metadata_record_of_manager.id}) ' \
                                             f'(curator: {i.curator})</a> ' \
                                             f'submitted {j} days ago</p>'

            pending_metadata_validations = ''
            for i, j in sort_list_of_tuples(reminder_dict[email]['validations']['metadata_validations']):
                vld_type = f'{"Metadata Validation" if not (i.functional_service or i.has_content_file) else "Metadata/Technical Validation"}'
                pending_metadata_validations += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; [{vld_type}] for ' \
                                                f'<a href={i.metadata_record_of_manager.get_display_url()}?auth=true>' \
                                                f'{i.metadata_record_of_manager.__str__()} ' \
                                                f'(id: {i.metadata_record_of_manager.id}) ' \
                                                f'(curator: {i.curator})</a> ' \
                                                f'submitted {j} days ago</p>'

            pending_technical_validations = ''
            for i, j in sort_list_of_tuples(reminder_dict[email]['validations']['technical_validations']):
                vld_type = f'{"Technical Validation" if not (i.functional_service or i.has_content_file) else "Metadata/Technical Validation"}'
                pending_technical_validations += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; [{vld_type}] for ' \
                                                 f'<a href={i.metadata_record_of_manager.get_display_url()}?auth=true>' \
                                                 f'{i.metadata_record_of_manager.__str__()} ' \
                                                 f'(id: {i.metadata_record_of_manager.id}) ' \
                                                 f'(curator: {i.curator})</a> ' \
                                                 f'submitted {j} days ago</p>'
            body = f"""
                                <p>Dear {reminder_dict[email]['username']},</p>
                                <p>You have {sum_of_validations} pending validations:</p>
                                <p>{pending_legal_validations if pending_legal_validations else ''}</p>
                                <p>{pending_metadata_validations if pending_metadata_validations else ''}</p>
                                <p>{pending_technical_validations if pending_technical_validations else ''}</p>
                                <p>The {settings.PROJECT_NAME}  group</p>
                                            """
            try:
                if sum_of_validations:
                    send_mail(
                        subject=f'{settings.PROJECT_NAME} notification: [Important] Pending validations',
                        message=body,
                        from_email=settings.DEFAULT_FROM_EMAIL,
                        recipient_list=[email],
                        fail_silently=settings.EMAIL_FAIL_SILENTLY,
                        html_message=body
                    )
                    LOGGER.info(f'User {reminder_dict[email]["username"]} notified.')
                else:
                    LOGGER.info(f'User {reminder_dict[email]["username"]} has no pending validations.')
            except SMTPRecipientsRefused:
                LOGGER.info(f'Email {email} not found.')
                continue
        LOGGER.info('All notifications sent.')
