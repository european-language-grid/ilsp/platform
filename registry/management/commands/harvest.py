import logging

from django.core.mail import send_mail
from django.core.management import BaseCommand
from django.template.loader import render_to_string
from catalogue_backend.settings import settings
from oai.models import Provider

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Harvest all registered OAI-PMH providers'

    def handle(self, *args, **options):
        LOGGER.info('Harvesting task initiated')
        recipients = settings.HARVEST_EMAILS
        results = list()
        providers = list(Provider.objects.filter(active=True))
        for provider in providers:
            LOGGER.info(f'Harvesting repository "{provider.repository.repository_name["en"]}" '
                        f'({providers.index(provider) + 1}/{len(providers)})')
            result = provider.harvest()
            if not result['added_to_elg']:
                LOGGER.info('No new resources added')
            # Add result to overall results
            results.append(result)

            # prepare personal email for this provider
            message = render_to_string(template_name='oai/email/harvest.html', context={'results': [result]})
            LOGGER.info(f'Sending email report to {result.get("email_to")}')
            if result.get('email_to'):
                send_mail(
                    subject=f'ELG Harvesting Report',
                    message=message,
                    from_email=settings.EMAIL_HOST_USER,
                    # filter out all superusers' emails, except the 'elg-system' user
                    recipient_list=result.get('email_to'),
                    fail_silently=False,
                    html_message=message
                )
                # if there is a personal email sent, exclude this recipient from the overall report recipients, if found
                for email in result.get('email_to'):
                    try:
                        if not email.endswith('@athenarc.gr'):
                            recipients.remove(email)
                    except ValueError:
                        pass
        # Send overall email
        message = render_to_string(template_name='oai/email/harvest.html', context={'results': results})
        LOGGER.info('Sending email report to admins')
        send_mail(
            subject=f'ELG Harvesting Report',
            message=message,
            from_email=settings.EMAIL_HOST_USER,
            # filter out all superusers' emails, except the 'elg-system' user
            recipient_list=recipients,
            fail_silently=False,
            html_message=message
        )
