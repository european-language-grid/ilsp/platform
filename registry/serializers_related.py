import logging

from django.core.exceptions import FieldDoesNotExist, ObjectDoesNotExist
from django.db.models import Q
from rest_framework.fields import SkipField
from rest_framework.serializers import Serializer, ModelSerializer, ListSerializer
from rest_polymorphic.serializers import PolymorphicSerializer

from registry.models import MetadataRecord, ToolService, Corpus, LexicalConceptualResource, Model, Grammar, \
    LanguageResource, Project, Organization, Document, Group, GenericMetadataRecord, LanguageDescription
from registry.serializer_tasks import GetRelatedRecords

LOGGER = logging.getLogger(__name__)


class RelatedSerializer(Serializer):
    """
    An related_published_instances of OrderedDict used for serialization for the landing page
    """

    def _get_model_field(self, field_name, instance):
        return instance._meta.get_field(field_name)

    def _map_model_to_query(self):
        model_to_query = {
            ToolService: 'described_entity__languageresource__lr_subclass__id__in',
            Corpus: 'described_entity__languageresource__lr_subclass__id__in',
            LexicalConceptualResource: 'described_entity__languageresource__lr_subclass__id__in',
            Model: 'described_entity__languageresource__lr_subclass__languagedescription__language_description_subclass__id__in',
            Grammar: 'described_entity__languageresource__lr_subclass__languagedescription__language_description_subclass__id__in',
            LanguageDescription: 'described_entity__languageresource__lr_subclass__id__in',
            LanguageResource: 'described_entity__languageresource__id__in',
            Project: 'described_entity__project__id__in',
            Organization: 'described_entity__actor__organization__id__in',
            # Document: 'described_entity__document__id',
            # Group: 'described_entity__actor__group__id',
            # Person: 'described_entity__actor__person__id'
        }
        return model_to_query

    def _get_published_related_instances(self, instance, request_user=None):
        published_related_pks = set()
        related_pk_set = set()
        if (getattr(instance, 'metadata_record_of_described_entity', None)
                or isinstance(instance, Organization)):
            related_pk_set.add((instance.pk, instance._meta.model))
        else:
            get_related_instances = GetRelatedRecords(instance)
            relations = get_related_instances.get_fields_for_landing_pages()
            related_pk_set = set([(rel.pk, rel._meta.model) for rel in relations])
        for _pk, _model in related_pk_set:
            if _model not in self._map_model_to_query():
                continue
            related_published_mdr = MetadataRecord.objects.filter(Q(**{self._map_model_to_query()[_model]: [_pk]}) &
                                                                  Q(described_entity__entity_type__in=[
                                                                      'LanguageResource',
                                                                      'Organization',
                                                                      'Project']) &
                                                                  Q(management_object__status__in=['p']))
            if request_user:
                related_user_mdr = MetadataRecord.objects.filter(Q(**{self._map_model_to_query()[_model]: [_pk]}) &
                                                                 Q(described_entity__entity_type__in=[
                                                                     'LanguageResource',
                                                                     'Organization',
                                                                     'Project']) &
                                                                 Q(management_object__curator__in=[
                                                                     request_user.first().pk]))
                if related_user_mdr.exists():
                    published_related_pks.add(related_user_mdr.first().pk)
            if related_published_mdr.exists():
                published_related_pks.add(related_published_mdr.first().pk)
        return published_related_pks

    def update_related_landing_pages(self, instance=None, all_related_published_instance_pks=None, request_user=None):
        ignore_fields = ['management_object', 'dataset', 'package']
        if instance is None:
            instance = self.instance
        if not all_related_published_instance_pks:
            all_related_published_instance_pks = set()
        related_published_instance_pks = self._get_published_related_instances(instance, request_user=request_user)
        all_related_published_instance_pks.update(related_published_instance_pks)
        if isinstance(self, PolymorphicSerializer):
            related_published_instance_pks = self.model_serializer_mapping[
                type(instance)].update_related_landing_pages(
                instance,
                all_related_published_instance_pks=all_related_published_instance_pks,
                request_user=request_user
            )
            all_related_published_instance_pks.update(related_published_instance_pks)
        else:
            fields = self._readable_fields
            for field in fields:
                # The field in model
                try:
                    field_model = self._get_model_field(field.field_name, instance)
                except FieldDoesNotExist:
                    continue

                try:
                    # the actual value of the field
                    attribute = field.get_attribute(instance)
                except SkipField:
                    continue

                # For custom serializer fields
                if field.field_name in ignore_fields:
                    continue
                # if field is an inner model
                elif isinstance(field, ModelSerializer):
                    if attribute:
                        related_published_instance_pks = field.update_related_landing_pages(
                            attribute,
                            all_related_published_instance_pks=all_related_published_instance_pks,
                            request_user=request_user
                        )
                        all_related_published_instance_pks.update(related_published_instance_pks)
                # if field is a list of inner models
                elif isinstance(field, ListSerializer) and isinstance(field.child, ModelSerializer):
                    # if list is empty, make all null
                    if len(attribute.all()) == 0:
                        pass
                    else:
                        for child in attribute.all():
                            if child:
                                related_published_instance_pks = field.child.update_related_landing_pages(
                                    child,
                                    all_related_published_instance_pks=all_related_published_instance_pks,
                                    request_user=request_user
                                )
                                all_related_published_instance_pks.update(related_published_instance_pks)

        published_pk_list = list(all_related_published_instance_pks)
        if published_pk_list \
                and self.__class__.__name__ == 'MetadataRecordSerializer':
            for _m in instance.management_object.versioned_record_managers.all():
                published_pk_list.append(_m.metadata_record_of_manager.pk)
            if instance.pk in published_pk_list:
                published_pk_list.remove(instance.pk)
            LOGGER.info(f'Landing pages of records with the following ids will be updated: {published_pk_list}')
            related_mds = MetadataRecord.objects.filter(id__in=list(set(published_pk_list)))
            for md in related_mds:
                md.management_object.generate_landing_page_display()
                LOGGER.info(f'Landing page updated for {md.__str__()}. (id: {md.pk})')
        return published_pk_list
