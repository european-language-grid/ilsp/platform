from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from utils.image_utils import map_docker_locations


def elg_service_publication(instance):
    if instance.management_object.under_construction:
        return None
    distribution = instance.described_entity.lr_subclass.software_distribution.filter(elg_compatible_distribution=True)
    if len(distribution) == 0:
        return Response(
            'None of the distributions of this elg compatible '
            'service have been tagged as the elg integrated distribution.',
            status=status.HTTP_400_BAD_REQUEST
        )
    if len(distribution) > 1:
        return Response(
            'Only one  of the distributions of this elg '
            'compatible service can be tagged as the elg integrated distribution.',
            status=status.HTTP_400_BAD_REQUEST
        )
    for dist in distribution:
        map_docker_locations(dist)
        dist.save()


def elg_service_integration(distributions):
    docker_distributions = distributions.filter(
        software_distribution_form='http://w3id.org/meta-share/meta-share/dockerImage')
    if len(docker_distributions) == 0:
        return False, {'elg_compatible_distribution':
                           'The record needs one distribution with a docker image software distribution form '
                           'and execution location.'}
    elif len(docker_distributions) == 1:
        for dist in docker_distributions:
            elg_compatible_distribution = distributions.filter(
                elg_compatible_distribution=True)
            if len(elg_compatible_distribution) == 0:
                dist.elg_compatible_distribution = True
                dist.save()
        return True, None
    elif len(docker_distributions) > 1:
        elg_compatible_distribution = distributions.filter(
            elg_compatible_distribution=True)
        if not len(elg_compatible_distribution) == 1:
            return False, {'elg_compatible_distribution':
                               'You have more than one distribution that could be tagged as an elg compatible distribution,'
                               'please check the documentation and return to the editor to tag your'
                               'preferred distribution.'}
        return True, None


def elg_service_ingestion_validation(instance):
    if instance.management_object.under_construction:
        return True, None
    distributions = instance.described_entity.lr_subclass.software_distribution.all()
    is_valid, error = elg_service_integration(distributions)
    if not is_valid:
        return is_valid, error
    elg_compatible_distribution = distributions.filter(
        elg_compatible_distribution=True)
    if len(elg_compatible_distribution) == 0:
        return False, {'elg_compatible_distribution': 'None of the distributions of this elg compatible '
                                                      'service have been tagged as the elg integrated '
                                                      'distribution.'}
    elif len(elg_compatible_distribution) > 1:
        return False, {'elg_compatible_distribution': 'Only one  of the distributions of this elg '
                                                      'compatible service can be tagged as the elg integrated distribution.'}
    else:
        if not getattr(elg_compatible_distribution.first(), 'execution_location', None):
            return False, {'elg_compatible_distribution': "execution_location cannot be blank."}
    return True, None
