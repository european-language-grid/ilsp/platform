import json
import logging
import re
from collections import OrderedDict

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q, Count
from catalogue_backend.celery import app as celery_app

from registry.choices import (
    LR_IDENTIFIER_SCHEME_CHOICES, LT_CLASS_RECOMMENDED_CHOICES, LD_SUBCLASS_CHOICES
)

from utils.encoding import LazyEncoder
# redeclare due circular import by "from registry.utils.retrieval_utils import retrieve_default_lang"
from utils.cv_choice_extraction import extract_data_from_cv

LOGGER = logging.getLogger(__name__)


def default_lang(dictionary):
    """
    Retrieve the value in the required language tag from a language field
    If none is found retrieve the next available
    :param dictionary: the language field
    :return:
    """
    try:
        return dictionary[settings.REQUIRED_LANGUAGE_TAG]
    except KeyError:
        return next(iter(dictionary.values()))


class AutomaticPublicationFieldGeneration:

    def __init__(self, instance):
        self.instance = instance

    def _generate_creator(self, lang_code, field):
        resource_creator = ''
        resource_creator_mapping = {
            'Organization': 'organization_name',
            'Group': 'organization_name',
            'Person': 'surname'
        }
        resource_creators = [creator for creator in self.instance.described_entity.resource_creator.all()]
        for i in range(len(resource_creators)):
            while len(resource_creators) > 0:
                actor = resource_creators.pop(i)
                entity_type = actor.entity_type
                return_creator = getattr(actor, resource_creator_mapping[entity_type]).get(
                    lang_code, getattr(actor, resource_creator_mapping[entity_type])["en"])
                if entity_type == 'Person':
                    if field == 'citation_text':
                        return_creator += f', {"" if actor.given_name.get(lang_code) == "unspecified" else actor.given_name.get(lang_code, actor.given_name["en"])}'
                    elif field == 'attribution_text':
                        return_creator = f'{"" if actor.given_name.get(lang_code) == "unspecified" else actor.given_name.get(lang_code, actor.given_name["en"]) + " "}' \
                                         + return_creator
                if entity_type == 'Organization':
                    parent_org = getattr(actor, 'is_division_of', None)
                    for parent in parent_org.all():
                        return_creator += f'{" - " + parent.organization_name.get(lang_code, parent.organization_name["en"])}'
                if not resource_creator:
                    resource_creator = return_creator
                else:
                    if len(resource_creators) != 0:
                        if field == 'citation_text':
                            resource_creator += '; ' + return_creator
                        elif field == 'attribution_text':
                            resource_creator += ', ' + return_creator
                    else:
                        if field == 'citation_text':
                            resource_creator += '; ' + return_creator
                        elif field == 'attribution_text':
                            resource_creator += ' and ' + return_creator
        return resource_creator

    def _generate_resource_provider(self, lang_code, field):
        resource_provider = ''
        resource_provider_mapping = {
            'Organization': 'organization_name',
            'Group': 'organization_name',
            'Person': 'surname'
        }
        resource_providers = [creator for creator in self.instance.described_entity.resource_provider.all()]
        for i in range(len(resource_providers)):
            while len(resource_providers) > 0:
                actor = resource_providers.pop(i)
                entity_type = actor.entity_type
                return_provider = getattr(actor, resource_provider_mapping[entity_type]).get(
                    lang_code, getattr(actor, resource_provider_mapping[entity_type])["en"])
                if entity_type == 'Person':
                    if field == 'citation_text':
                        return_provider += f', {"" if actor.given_name.get(lang_code) == "unspecified" else actor.given_name.get(lang_code, actor.given_name["en"])}'
                    elif field == 'attribution_text':
                        return_provider = f'{"" if actor.given_name.get(lang_code) == "unspecified" else actor.given_name.get(lang_code, actor.given_name["en"]) + " "}' \
                                          + return_provider
                if entity_type == 'Organization':
                    parent_org = getattr(actor, 'is_division_of', None)
                    for parent in parent_org.all():
                        return_provider += f'{" - " + parent.organization_name.get(lang_code, parent.organization_name["en"])}'
                if not resource_provider:
                    resource_provider = return_provider
                else:
                    if len(resource_providers) != 0:
                        if field == 'citation_text':
                            resource_provider += '; ' + return_provider
                        elif field == 'attribution_text':
                            resource_provider += ', ' + return_provider
                    else:
                        if field == 'citation_text':
                            resource_provider += '; ' + return_provider
                        elif field == 'attribution_text':
                            resource_provider += ' and ' + return_provider
        return resource_provider

    def _generate_publication_date(self):
        if self.instance.described_entity.version_date:
            return self.instance.described_entity.version_date.strftime("%Y, %B %d")
        elif self.instance.described_entity.publication_date:
            return self.instance.described_entity.publication_date.strftime("%Y, %B %d")
        elif self.instance.metadata_creation_date:
            return self.instance.metadata_creation_date.strftime("%Y")

    def _generate_title(self, lang_code):
        return self.instance.described_entity.resource_name.get(
            lang_code, self.instance.described_entity.resource_name["en"]
        )

    def _generate_citation_resource_type(self):
        resource_type = self.instance.described_entity.lr_subclass.lr_type
        resource_type_datacite_mapping = {
            'LexicalConceptualResource': 'Dataset',
            'ToolService': 'Software'
        }
        ld_datacite_mapping = {
            'Other': 'Other',
            'Grammar': 'Dataset',
            'Model': 'Model'
        }
        display_mapping = {
            'ToolService': 'Tool/Service',
            'LexicalConceptualResource': 'Lexical/Conceptual Resource'
        }
        corpus_media_type_mapping = {
            'http://w3id.org/meta-share/meta-share/audio': 'Audio',
            'http://w3id.org/meta-share/meta-share/image': 'Image',
            'http://w3id.org/meta-share/meta-share/text': 'Text',
            'http://w3id.org/meta-share/meta-share/textNumerical': 'Numerical text',
            'http://w3id.org/meta-share/meta-share/video': 'Video'
        }
        return_citation_resource_type = ''
        if resource_type in ['Corpus']:
            media_types = ''
            media_parts = [media_part for media_part in
                           self.instance.described_entity.lr_subclass.corpus_media_part.all()]
            for i in range(len(media_parts)):
                while len(media_parts) > 0:
                    media_part = media_parts.pop(i)
                    if not media_types:
                        media_types = f'{corpus_media_type_mapping[media_part.media_type]}'
                    else:
                        if len(media_parts) != 0:
                            media_types += ', ' + f'{corpus_media_type_mapping[media_part.media_type]}'
                        else:
                            media_types += ' and ' + f'{corpus_media_type_mapping[media_part.media_type]}'
            return_citation_resource_type = f'Dataset ({media_types} corpus)'

        if resource_type in ['ToolService', 'LexicalConceptualResource']:
            return_citation_resource_type = f'{resource_type_datacite_mapping[resource_type]}' \
                                            f' ({display_mapping[resource_type]})'

        if resource_type in ['LanguageDescription']:
            ld_subclass = self.instance.described_entity.lr_subclass.ld_subclass
            ld_subclass_type_mapping = {
                'http://w3id.org/meta-share/meta-share/grammar': 'Grammar',
                'http://w3id.org/meta-share/meta-share/model': 'Model',
                'http://w3id.org/meta-share/meta-share/other': 'Other'
            }
            ld_type = ld_subclass_type_mapping.get(ld_subclass, '')
            return_citation_resource_type = f'{ld_datacite_mapping[ld_type]}' \
                                            f'{f"({ld_type})" if ld_type == "Grammar" else ""}'

        return return_citation_resource_type

    def _generate_identifier(self):
        from registry.models import LRIdentifier
        if self.instance.management_object.is_elg_record:
            try:
                identifier_suffix = self.instance.described_entity.lr_identifier.get(
                    lr_identifier_scheme=LR_IDENTIFIER_SCHEME_CHOICES.DOI,
                    value__startswith=settings.DATACITE_PREFIX
                ).value
            except LRIdentifier.DoesNotExist:
                # send error message
                LOGGER.error(f'DOI is not added to the Lr Identifiers of the record [{self.instance.pk}]')
                identifier = self.instance.get_display_url()
            else:
                identifier = f'{settings.DOI_RESOLVE_URL}{identifier_suffix}'
        else:
            identifier = self.instance.get_display_url()

        return identifier

    def _generate_licence_terms(self, distribution, lang_code):
        dist_licence = ''
        licences = [licence for licence in distribution.licence_terms.all()]
        for i in range(len(licences)):
            while len(licences) > 0:
                licence = licences.pop(i)
                licence_terms_name = licence.licence_terms_name
                term_name = f'{licence_terms_name.get(lang_code, licence_terms_name.get("en"))}'
                licence_terms_url = licence.licence_terms_url
                term_url = f'{licence_terms_url[0] if len(licence_terms_url) == 1 else ", ".join(licence_terms_url)}'
                return_licence = f'{term_name} ({term_url})'
                if not dist_licence:
                    dist_licence = return_licence
                else:
                    if len(licences) != 0:
                        dist_licence += ', ' + return_licence
                    else:
                        dist_licence += ' and ' + return_licence
        return dist_licence

    def _generate_citation_text(self, publication_date, version, resource_type, identifier):
        creator_en = self._generate_creator('en', 'citation_text')
        title_en = self._generate_title("en")
        provider = self._generate_resource_provider('en', 'citation_text')
        publisher = (
            "European Language Grid" if (
                    not self.instance.management_object.is_elg_record
                    or (self.instance.management_object.is_elg_record and not provider)
            )
            else provider
        )

        citation_text = {
            'en': f'{creator_en if creator_en else title_en}'
                  f' ({publication_date}).'
                  f'{f" {title_en}." if creator_en else ""}'
                  f' Version {version}.'
                  f'{f" {publisher}." if self.instance.management_object.is_elg_record else ""}'
                  f' [{resource_type}].'
                  f'{f" Source: {publisher}." if not self.instance.management_object.is_elg_record else ""}'
                  f' {identifier}'
        }
        self.instance.described_entity.citation_text = citation_text
        self.instance.described_entity.save()

    def _generate_attribution_text(self, publisher, identifier):
        resource_type_distribution_mapping = {
            'Corpus': getattr(self.instance.described_entity.lr_subclass,
                              'dataset_distribution',
                              None),
            'LexicalConceptualResource': getattr(self.instance.described_entity.lr_subclass,
                                                 'dataset_distribution',
                                                 None),
            'LanguageDescription': getattr(self.instance.described_entity.lr_subclass,
                                           'dataset_distribution',
                                           None),
            'ToolService': getattr(self.instance.described_entity.lr_subclass,
                                   'software_distribution',
                                   None)
        }
        creator_en = self._generate_creator('en', 'attribution_text')
        resource_type = self.instance.described_entity.lr_subclass.lr_type
        for distribution in resource_type_distribution_mapping[resource_type].all():
            distribution.attribution_text = {
                'en': f'{self._generate_title("en")}'
                      f' {f"by {creator_en} " if creator_en else ""}used under'
                      f' {self._generate_licence_terms(distribution, "en")}.'
                      f' Source: {identifier}'
                      f' ({publisher})'
            }
            distribution.save()

    def _get_distribution_data_formats(self, distribution):
        result = list()
        try:
            for dist_text_feat in distribution.distribution_text_feature.all():
                result.extend(extract_data_from_cv(dist_text_feat, 'data_format'))
            for dist_text_num_feat in distribution.distribution_text_numerical_feature.all():
                result.extend(extract_data_from_cv(dist_text_num_feat, 'data_format'))
            for dist_aud_feat in distribution.distribution_audio_feature.all():
                result.extend(extract_data_from_cv(dist_aud_feat, 'data_format'))
            for dist_img_feat in distribution.distribution_image_feature.all():
                result.extend(extract_data_from_cv(dist_img_feat, 'data_format'))
            for dist_vid_feat in distribution.distribution_video_feature.all():
                result.extend(extract_data_from_cv(dist_vid_feat, 'data_format'))
            result = list(filter(("unspecified").__ne__, result))
        except AttributeError:
            return result
        return list(set(result))

    def generate_landing_page(self, save_manager=False):
        # avoid cyclical import error
        from registry.serializers import MetadataRecordSerializer
        self.instance.management_object.landing_page_display = json.loads(
            json.dumps(MetadataRecordSerializer(self.instance).to_display_representation(
                self.instance),
                cls=LazyEncoder,
                indent=4)
        )
        self.instance.management_object.uncensored_landing_page_display = json.loads(
            json.dumps(MetadataRecordSerializer(self.instance, context={'uncensored': True}).to_display_representation(
                self.instance),
                cls=LazyEncoder,
                indent=4)
        )
        if save_manager:
            self.instance.management_object.save()

    def generate_lr_fields(self, fields):
        if not self.instance.described_entity.entity_type == 'LanguageResource':
            return 'No field generation required'
        if isinstance(fields, str):
            fields = [fields]
        publication_date = self._generate_publication_date()
        version = self.instance.described_entity.version
        publisher = f'European Language Grid'
        resource_type = self._generate_citation_resource_type()
        identifier = self._generate_identifier()
        fields_generated = 0
        for field in fields:
            if field == 'attribution_text':
                self._generate_attribution_text(publisher, identifier)
                fields_generated += 1
            if field == 'citation_text':
                self._generate_citation_text(publication_date, version, resource_type, identifier)
                fields_generated += 1
        if fields_generated:
            return f'{" and ".join(fields)} fields generated'
        else:
            return 'The specified fields cannot be generated automatically'

    def gen_cite_all_versions(self):
        """
        Generate citation text for all versions. The information
        is taken from the latest versioned record.
        - If record is hosted lr or functional service, identifier will be doi
        - else identifier will be internal elg url.
        """
        publication_date = self._generate_publication_date()
        resource_type = self._generate_citation_resource_type()

        # If record is hosted lr or functional service
        # identifier will be doi, else internal elg url
        if settings.DATACITE_PREFIX in self.instance.management_object.concept_pid:
            identifier = f'{settings.DOI_RESOLVE_URL}{self.instance.management_object.concept_pid}'
        else:
            identifier = f'{settings.DJANGO_URL}/catalogue/cpid/{self.instance.management_object.concept_pid}/'

        creator_en = self._generate_creator('en', 'citation_text')
        provider = self._generate_resource_provider('en', 'citation_text')
        publisher = (
            "European Language Grid" if (
                    not self.instance.management_object.is_elg_record
                    or (self.instance.management_object.is_elg_record and not provider)
            )
            else provider
        )

        title_en = self._generate_title("en")

        citation_text = {
            'en': f'{creator_en if creator_en else title_en}'
                  f' ({publication_date}).'
                  f'{f" {title_en}." if creator_en else ""}'
                  f'{f" {publisher}." if self.instance.management_object.is_elg_record else ""}'
                  f' [{resource_type}].'
                  f'{f" Source: {publisher}." if not self.instance.management_object.is_elg_record else ""}'
                  f' {identifier}'
        }
        return citation_text

    def generate_json_ld(self, save_manager=False):
        """
        Generates a schema.org JSON-LD structure for a given record, for use in Google Datasets Search
        """
        if not self.instance.described_entity.entity_type == 'LanguageResource':
            return 'No field generation required'
        json_ld = OrderedDict()
        json_ld["@context"] = "https://schema.org/"
        json_ld["@type"] = "Dataset"
        json_ld['name'] = default_lang(self.instance.described_entity.resource_name)
        if self.instance.described_entity.resource_short_name:
            json_ld['alternateName'] = default_lang(self.instance.described_entity.resource_short_name)
        json_ld['description'] = default_lang(self.instance.described_entity.description)
        json_ld['url'] = self.instance.get_display_url()
        json_ld['keywords'] = list()
        for kw in self.instance.described_entity.keyword:
            json_ld['keywords'].extend(kw.values())
        if self.instance.described_entity.lr_subclass.lr_type == 'ToolService':
            for function in self.instance.described_entity.lr_subclass.function:
                try:
                    json_ld['keywords'].append(LT_CLASS_RECOMMENDED_CHOICES[function])
                # Just output user defined function
                except KeyError:
                    json_ld['keywords'].append(function)
        # licences
        licences = list()
        try:
            distributions = self.instance.described_entity.lr_subclass.dataset_distribution.all()
        except:
            distributions = self.instance.described_entity.lr_subclass.software_distribution.all()
        for distribution in distributions:
            for licence in distribution.licence_terms.all():
                licences.append({
                    "@type": "CreativeWork",
                    "name": default_lang(licence.licence_terms_name),
                    "url": licence.licence_terms_url[0]
                })
        json_ld['license'] = licences
        resource_creators = list()
        for resource_creator in self.instance.described_entity.resource_creator.all():
            entity_type = resource_creator.entity_type
            if entity_type == 'Organization':
                resource_creators.append({
                    "@type": entity_type,
                    "url": resource_creator.website,
                    "name": default_lang(resource_creator.organization_name)
                })
        if resource_creators:
            json_ld['creator'] = resource_creators
        json_ld['includedInDataCatalog'] = {
            "@type": "DataCatalog",
            "name": "European Language Grid",
            "url": "https://live.european-language-grid.eu"
        }
        distr = list()
        for distribution in distributions:
            dist_dict = dict()
            data_formats = self._get_distribution_data_formats(distribution)
            dist_dict['@type'] = 'DataDownload'
            if data_formats:
                dist_dict['encodingFormat'] = data_formats
                distr.append(dist_dict)
        if distr:
            json_ld['distribution'] = distr
        self.instance.management_object.json_ld = json_ld
        if save_manager:
            self.instance.management_object.save()

    def generate_elg_compatible(self):
        """
        Checks whether the record is an elg compatible LanguageResource. ELG compatible means:
        - For models: that there is at least one ELG functional Tool/Service pointing to model at hand via mlModel and
                      the Tool/Service is already published and not deleted
        """
        if self.instance.described_entity.entity_type == 'LanguageResource':
            if self.instance.described_entity.lr_subclass.lr_type == 'LanguageDescription' and \
                    self.instance.described_entity.lr_subclass.ld_subclass == LD_SUBCLASS_CHOICES.MODEL:
                for tool in self.instance.described_entity.tool_services_of_ml_model.all():
                    if tool.language_resource_of_lr_subclass.metadata_record_of_described_entity.management_object.functional_service and \
                            tool.language_resource_of_lr_subclass.metadata_record_of_described_entity.management_object.status == 'p' and \
                            not tool.language_resource_of_lr_subclass.metadata_record_of_described_entity.management_object.deleted:
                        self.instance.management_object.elg_compatible_model = True
                        self.instance.management_object.save()
                        return
        return


class PostPublicationMerge:

    def __int__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(self, record_pk):
        r_c = self.manage_record_curation(record_pk)
        return r_c

    def manage_record_curation(self, record_pk):
        from registry.models import MetadataRecord
        from registry.merging_mechanisms import merge_entities
        instance = MetadataRecord.objects.get(pk=record_pk)
        entity_type = instance.described_entity.entity_type
        updated_pks = []

        if entity_type == 'Organization':
            updated_pks = self.manage_organization_curation(instance)
        elif entity_type == 'LanguageResource':
            updated_pks = self.manage_lr_curation(instance)
        elif entity_type == 'Project':
            updated_pks = self.manage_project_curation(instance)
        elif entity_type == 'LicenceTerms':
            updated_pks = self.manage_licence_term_curation(instance)
        elif entity_type == 'Person':
            updated_pks = self.manage_person_curation(instance)
        elif entity_type == 'Group':
            updated_pks = self.manage_group_curation(instance)
        elif entity_type == 'Document':
            updated_pks = self.manage_document_curation(instance)
        elif entity_type == 'Repository':
            updated_pks = self.manage_repo_curation(instance)

        if updated_pks:
            merge_entities(updated_pks)

        return updated_pks

    def manage_organization_curation(self, instance):
        from registry.models import Organization, OrganizationIdentifier
        from registry.utils.retrieval_utils import field_choices_to_list

        retrieved_generic_org_list = []
        organization_target = instance.described_entity
        identifiers = instance.described_entity.organization_identifier
        valid_schema = field_choices_to_list('Organization')
        websites = instance.described_entity.website
        organization_name = instance.described_entity.organization_name
        parent_organizations = instance.described_entity.is_division_of.all()
        generic_orgs = Organization.objects.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        # Retrieve generic organizations to merge by identifier matching
        for _identifier in identifiers.all():
            if _identifier.organization_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA \
                    or _identifier.organization_identifier_scheme not in valid_schema \
                    or not hasattr(_identifier, 'value'):
                continue
            query_filter = dict()
            query_filter['organization_identifier_scheme'] = _identifier.organization_identifier_scheme
            query_filter['value'] = getattr(_identifier, 'value', '')
            org_identifiers = OrganizationIdentifier.objects.filter(**query_filter)
            for oi in org_identifiers.all():
                generic_instances = generic_orgs.filter(organization_identifier=oi.pk)
                retrieved_generic_org_list.extend([org.pk for org in generic_instances])
        # Retrieve generic organizations to merge by website
        for website in websites:
            if website.endswith('/'):
                # stores (website url without slash) as index 0 and (website url with slash) as index 1
                pairs = [website[:-1], website]
            else:
                # stores (website url without slash) as index 0 and (website url with slash) as index 1
                pairs = [website, website + '/']
            # remove http(s) from the query link
            http_pattern = re.compile(r"https?://?")
            for i, link in enumerate(pairs):
                pairs[i] = http_pattern.sub('', link).strip()
            # query for model instance that contains website url without slash
            # (will return all instances that contain said url in any form)
            generic_instances = generic_orgs.filter(website__icontains=pairs[0])
            retrieved_generic_org_list.extend([org.pk for org in generic_instances])
        # Retrieve generic organizations to merge by pointing to same is_division_of
        if parent_organizations is not None and len(parent_organizations) > 0:
            # To reduce the search space, first retrieve those organizations
            # with just the same length of is_division_of
            generic_instances = generic_orgs.annotate(
                c=Count('is_division_of')).filter(c=len(parent_organizations))
            parent_matches = []
            for parent_org in parent_organizations:
                generic_instances = generic_instances.filter(
                    Q(is_division_of__organization_name__en=parent_org.organization_name['en']))
                for org in generic_instances:
                    for parent in org.is_division_of.all():
                        for k, v in parent.organization_name.items():
                            if v.lower().strip() == parent_org.organization_name['en'].lower().strip():
                                parent_matches.append(org)
            matches = set([org for org in parent_matches for k, v in org.organization_name.items()
                           if v.lower().strip() == organization_name['en'].lower().strip()])
        else:
            generic_instances = generic_orgs.filter(
                Q(is_division_of__isnull=True))
            matches = set([org for org in generic_instances for k, v in org.organization_name.items()
                           if v.lower().strip() == organization_name['en'].lower().strip()])
        retrieved_generic_org_list.extend([org.pk for org in matches])
        # Merge organizations
        list_of_pairs_to_merge = list()
        for organization_source_pk in list(set(retrieved_generic_org_list)):
            list_of_pairs_to_merge.append((organization_source_pk, organization_target.pk))
        return list_of_pairs_to_merge

    def manage_lr_curation(self, instance):
        from registry.models import LanguageResource, LRIdentifier
        from registry.utils.retrieval_utils import field_choices_to_list

        retrieved_generic_lr_list = []
        lr_target = instance.described_entity
        identifiers = instance.described_entity.lr_identifier
        valid_schema = field_choices_to_list('LanguageResource')
        resource_name = instance.described_entity.resource_name
        version = instance.described_entity.version
        generic_lrs = LanguageResource.objects.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        # Retrieve generic lrs to merge by identifier matching
        for _identifier in identifiers.all():
            if _identifier.lr_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA \
                    or _identifier.lr_identifier_scheme not in valid_schema \
                    or not hasattr(_identifier, 'value'):
                continue
            query_filter = dict()
            query_filter['lr_identifier_scheme'] = _identifier.lr_identifier_scheme
            query_filter['value'] = getattr(_identifier, 'value', '')
            lr_identifiers = LRIdentifier.objects.filter(**query_filter)
            for li in lr_identifiers.all():
                generic_instances = generic_lrs.filter(lr_identifier=li.pk)
                retrieved_generic_lr_list.extend([lr.pk for lr in generic_instances])
        # Retrieve generic lrs to merge by resource_name matching
        if resource_name:
            generic_instances = generic_lrs.filter(resource_name__icontains=resource_name['en'], version=version)
            name_matches = set([lr for lr in generic_instances for k, v in lr.resource_name.items()
                           if v.lower().strip() == resource_name['en'].lower().strip()])
            retrieved_generic_lr_list.extend([lr.pk for lr in name_matches])
        # Merge lrs
        list_of_pairs_to_merge = list()
        for lr_source_pk in list(set(retrieved_generic_lr_list)):
            list_of_pairs_to_merge.append((lr_source_pk, lr_target.pk))
        return list_of_pairs_to_merge

    def manage_project_curation(self, instance):
        from registry.models import Project, ProjectIdentifier
        from registry.utils.retrieval_utils import field_choices_to_list

        retrieved_generic_proj_list = list()
        project_target = instance.described_entity
        identifiers = instance.described_entity.project_identifier
        valid_schema = field_choices_to_list('Project')
        websites = instance.described_entity.website
        project_name = instance.described_entity.project_name
        grant_number = instance.described_entity.grant_number
        generic_proj = Project.objects.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        # Retrieve generic projects to merge by identifier matching
        for _identifier in identifiers.all():
            if _identifier.project_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA \
                    or getattr(_identifier, 'project_identifier_scheme') not in valid_schema \
                    or not hasattr(_identifier, 'value'):
                continue
            query_filter = dict()
            query_filter['project_identifier_scheme'] = _identifier.project_identifier_scheme
            query_filter['value'] = getattr(_identifier, 'value')
            proj_identifiers = ProjectIdentifier.objects.filter(**query_filter)
            for pi in proj_identifiers.all():
                generic_instances = generic_proj.filter(project_identifier=pi.pk)
                retrieved_generic_proj_list.extend([proj.pk for proj in generic_instances])
        # Retrieve generic projects to merge by grant_number matching
        if grant_number:
            generic_instances = generic_proj.filter(grant_number__iregex=r'\y{}\y'.format(grant_number))
            if generic_instances:
                retrieved_generic_proj_list.extend([proj.pk for proj in generic_instances])
        # Retrieve generic projects to merge by project_name__en matching
        name_matches = set([proj for proj in generic_proj for k, v in proj.project_name.items() if
                            v.lower().strip() == project_name['en'].lower().strip()])
        retrieved_generic_proj_list.extend([proj.pk for proj in name_matches])
        # Retrieve generic projects to merge by website matching
        for website in websites:
            if website.endswith('/'):
                # stores (website url without slash) as index 0 and (website url with slash) as index 1
                pairs = [website[:-1], website]
            else:
                # stores (website url without slash) as index 0 and (website url with slash) as index 1
                pairs = [website, website + '/']
            # remove http(s) from the query link
            http_pattern = re.compile(r"https?://?")
            for i, link in enumerate(pairs):
                pairs[i] = http_pattern.sub('', link).strip()
            # query for model instance that contains website url without slash
            # (will return all instances that contain said url in any form)
            generic_instances = generic_proj.filter(website__icontains=pairs[0])
            retrieved_generic_proj_list.extend([proj.pk for proj in generic_instances])
        # Merge projects
        list_of_pairs_to_merge = list()
        for project_source in list(set(retrieved_generic_proj_list)):
            list_of_pairs_to_merge.append((project_source, project_target.pk))
        return list_of_pairs_to_merge

    def manage_licence_term_curation(self, instance):
        from registry.models import LicenceTerms, LicenceIdentifier
        from registry.utils.retrieval_utils import field_choices_to_list

        retrieved_generic_lt_list = list()
        lt_target = instance.described_entity
        identifiers = instance.described_entity.licence_identifier
        valid_schema = field_choices_to_list('LicenceTerms')
        licence_terms_urls = instance.described_entity.licence_terms_url
        licence_terms_name = instance.described_entity.licence_terms_name
        generic_lt = LicenceTerms.objects.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        # Retrieve generic lts to merge by identifier matching
        for _identifier in identifiers.all():
            if _identifier.licence_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA \
                    or getattr(_identifier, 'licence_identifier_scheme') not in valid_schema \
                    or not hasattr(_identifier, 'value'):
                continue
            query_filter = dict()
            query_filter['licence_identifier_scheme'] = _identifier.licence_identifier_scheme
            query_filter['value'] = getattr(_identifier, 'value')
            licence_identifiers = LicenceIdentifier.objects.filter(**query_filter)
            for li in licence_identifiers.all():
                generic_instances = generic_lt.filter(licence_identifier=li.pk)
                retrieved_generic_lt_list.extend([lt.pk for lt in generic_instances])
        # Retrieve generic lts to merge by url matching:
        for url in licence_terms_urls:
            cc_lang = ''
            if 'creativecommons' in url:
                cc = url.strip().split('/')
                for i, slug in enumerate(list(reversed(cc[-3:]))):
                    _i = -(i + 1)
                    if len(slug) == 2:
                        cc_lang = cc.pop(_i)
                        break

                url = '/'.join([slug for slug in cc if slug]).replace(':', ':/')
            if url.endswith('/'):
                # stores (licence term url without slash) as index 0 and (licence term url with slash) as index 1
                pairs = [url[:-1], url]
            else:
                # stores (licence term url without slash) as index 0 and (licence term url with slash) as index 1
                pairs = [url, url + '/']
            if 'creativecommons' in url:
                pairs.extend([url + '/' + cc_lang, url + '/' + cc_lang + '/'])
            # remove http(s) from the query link
            http_pattern = re.compile(r"https?://?")
            for i, link in enumerate(pairs):
                pairs[i] = http_pattern.sub('', link).strip().lower()
            generic_instances = generic_lt.filter(licence_terms_url__icontains=pairs[0])
            if generic_instances:
                retrieved_urls = []
                for instance in generic_instances:
                    instance_pk = instance.pk
                    retrieved_urls.extend([(http_pattern.sub('', lt_url).strip().lower(), instance_pk)
                                      for lt_url in instance.licence_terms_url])
                # make sure that the instance, that contains the exact licence_term url, either with or without slash,
                # is retrieved
                if retrieved_urls:
                    ret_urls = [_l[0] for _l in retrieved_urls]
                    _intersection = set(pairs).intersection(set(ret_urls))
                    ret_instances = [_l[1] for _l in retrieved_urls if _l[0] in _intersection]
                    same_url_licences = generic_instances.filter(pk__in=ret_instances)
                    retrieved_generic_lt_list.extend([lt.pk for lt in same_url_licences])
        # Retrieve generic lts to merge by name matching
        generic_instances = generic_lt.filter(licence_terms_name__en__iexact=licence_terms_name['en'])
        retrieved_generic_lt_list.extend([lt.pk for lt in generic_instances])
        # Merge licence terms
        list_of_pairs_to_merge = list()
        for lt_source in list(set(retrieved_generic_lt_list)):
            list_of_pairs_to_merge.append((lt_source, lt_target.pk))
        return list_of_pairs_to_merge

    def manage_person_curation(self, instance):
        from registry.models import Person, PersonalIdentifier
        from registry.utils.retrieval_utils import field_choices_to_list
        retrieved_generic_per_list = list()
        person_target = instance.described_entity
        identifiers = instance.described_entity.personal_identifier
        valid_schema = field_choices_to_list('Person')
        emails = instance.described_entity.email
        surname = instance.described_entity.surname
        given_name = instance.described_entity.given_name
        generic_per = Person.objects.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        # Retrieve generic projects to merge by identifier matching
        for _identifier in identifiers.all():
            if _identifier.personal_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA \
                    or getattr(_identifier, 'personal_identifier_scheme', None) not in valid_schema \
                    or not hasattr(_identifier, 'value'):
                continue
            query_filter = dict()
            query_filter['personal_identifier_scheme'] = _identifier.personal_identifier_scheme
            query_filter['value'] = getattr(_identifier, 'value')
            per_identifiers = PersonalIdentifier.objects.filter(**query_filter)
            for pi in per_identifiers.all():
                generic_instances = generic_per.filter(personal_identifier=pi.pk)
                retrieved_generic_per_list.extend([per.pk for per in generic_instances])
        # Retrieve generic persons to merge by email matching
        for email in emails:
            generic_instances = generic_per.filter(email__icontains=email)
            retrieved_generic_per_list.extend([per.pk for per in generic_instances])
        # Retrieve generic persons to merge by full name
        generic_instances = generic_per.filter(surname__en__iexact=surname['en'],
                                          given_name__en__iexact=given_name['en'])
        retrieved_generic_per_list.extend([per.pk for per in generic_instances])
        # Merge projects
        list_of_pairs_to_merge = list()
        for person_source in list(set(retrieved_generic_per_list)):
            list_of_pairs_to_merge.append((person_source, person_target.pk))
        return list_of_pairs_to_merge

    def manage_group_curation(self, instance):
        from registry.models import Group, GroupIdentifier
        from registry.utils.retrieval_utils import field_choices_to_list

        retrieved_generic_group_list = []
        group_target = instance.described_entity
        identifiers = instance.described_entity.group_identifier
        valid_schema = field_choices_to_list('Group')
        websites = instance.described_entity.website
        organization_name = instance.described_entity.organization_name
        generic_orgs = Group.objects.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        # Retrieve generic organizations to merge by identifier matching
        for _identifier in identifiers.all():
            if _identifier.organization_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA \
                    or _identifier.organization_identifier_scheme not in valid_schema \
                    or not hasattr(_identifier, 'value'):
                continue
            query_filter = dict()
            query_filter['organization_identifier_scheme'] = _identifier.organization_identifier_scheme
            query_filter['value'] = getattr(_identifier, 'value', '')
            org_identifiers = GroupIdentifier.objects.filter(**query_filter)
            for oi in org_identifiers.all():
                generic_instances = generic_orgs.filter(group_identifier=oi.pk)
                retrieved_generic_group_list.extend([org.pk for org in generic_instances])
        # Retrieve generic group to merge by website
        for website in websites:
            if website.endswith('/'):
                # stores (website url without slash) as index 0 and (website url with slash) as index 1
                pairs = [website[:-1], website]
            else:
                # stores (website url without slash) as index 0 and (website url with slash) as index 1
                pairs = [website, website + '/']
            # remove http(s) from the query link
            http_pattern = re.compile(r"https?://?")
            for i, link in enumerate(pairs):
                pairs[i] = http_pattern.sub('', link).strip()
            # query for model instance that contains website url without slash
            # (will return all instances that contain said url in any form)
            generic_instances = generic_orgs.filter(website__icontains=pairs[0])
            retrieved_generic_group_list.extend([org.pk for org in generic_instances])
        # Retrieve generic groups to merge by organization name
        matches = set([org for org in generic_orgs for k, v in org.organization_name.items()
                       if v.lower().strip() == organization_name['en'].lower().strip()])
        retrieved_generic_group_list.extend([org.pk for org in matches])
        # Merge organizations
        list_of_pairs_to_merge = list()
        for group_source_pk in list(set(retrieved_generic_group_list)):
            list_of_pairs_to_merge.append((group_source_pk, group_target.pk))
        return list_of_pairs_to_merge

    def manage_document_curation(self, instance):
        from registry.models import Document, DocumentIdentifier
        from registry.utils.retrieval_utils import field_choices_to_list

        retrieved_generic_doc_list = []
        doc_target = instance.described_entity
        identifiers = instance.described_entity.document_identifier
        valid_schema = field_choices_to_list('Document')
        title = instance.described_entity.title
        generic_docs = Document.objects.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        # Retrieve generic docs to merge by identifier matching
        for _identifier in identifiers.all():
            if _identifier.document_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA \
                    or _identifier.document_identifier_scheme not in valid_schema \
                    or not hasattr(_identifier, 'value'):
                continue
            query_filter = dict()
            query_filter['document_identifier_scheme'] = _identifier.document_identifier_scheme
            query_filter['value'] = getattr(_identifier, 'value', '')
            doc_identifiers = DocumentIdentifier.objects.filter(**query_filter)
            for di in doc_identifiers.all():
                generic_instances = generic_docs.filter(document_identifier=di.pk)
                retrieved_generic_doc_list.extend([doc.pk for doc in generic_instances])
        # Retrieve generic docs to merge by resource_name matching
        if title:
            generic_instances = generic_docs.filter(title__icontains=title['en'])
            name_matches = set([doc for doc in generic_instances for k, v in doc.title.items()
                                if v.lower().strip() == title['en'].lower().strip()])
            retrieved_generic_doc_list.extend([doc.pk for doc in name_matches])
        # Merge lrs
        list_of_pairs_to_merge = list()
        for doc_source_pk in list(set(retrieved_generic_doc_list)):
            list_of_pairs_to_merge.append((doc_source_pk, doc_target.pk))
        return list_of_pairs_to_merge

    def manage_repo_curation(self, instance):
        from registry.models import Repository, RepositoryIdentifier
        from registry.utils.retrieval_utils import field_choices_to_list

        retrieved_generic_repo_list = list()
        repo_target = instance.described_entity
        identifiers = instance.described_entity.repository_identifier
        valid_schema = field_choices_to_list('Repository')
        repository_urls = [instance.described_entity.repository_url]
        repository_name = instance.described_entity.repository_name
        generic_repo = Repository.objects.filter(
            Q(describedentity_ptr__metadata_record_of_described_entity__isnull=True))
        # Retrieve generic repos to merge by identifier matching
        for _identifier in identifiers.all():
            if _identifier.repository_identifier_scheme == settings.REGISTRY_IDENTIFIER_SCHEMA \
                    or getattr(_identifier, 'repository_identifier_scheme') not in valid_schema \
                    or not hasattr(_identifier, 'value'):
                continue
            query_filter = dict()
            query_filter['repository_identifier_scheme'] = _identifier.repository_identifier_scheme
            query_filter['value'] = getattr(_identifier, 'value')
            repo_identifiers = RepositoryIdentifier.objects.filter(**query_filter)
            for ri in repo_identifiers.all():
                generic_instances = generic_repo.filter(repository_identifier=ri.pk)
                retrieved_generic_repo_list.extend([repo.pk for repo in generic_instances])
        # Retrieve generic repos to merge by url matching:
        for url in repository_urls:
            if url.endswith('/'):
                # stores (licence term url without slash) as index 0 and (licence term url with slash) as index 1
                pairs = [url[:-1], url]
            else:
                # stores (licence term url without slash) as index 0 and (licence term url with slash) as index 1
                pairs = [url, url + '/']
            # remove http(s) from the query link
            http_pattern = re.compile(r"https?://?")
            for i, link in enumerate(pairs):
                pairs[i] = http_pattern.sub('', link).strip().lower()
            generic_instances = generic_repo.filter(repository_url__icontains=pairs[0])
            if generic_instances:
                retrieved_urls = []
                for instance in generic_instances:
                    instance_pk = instance.pk
                    retrieved_urls.extend([(http_pattern.sub('', repo_url).strip().lower(), instance_pk)
                                           for repo_url in [instance.repository_url]])
                # make sure that the instance, that contains the exact licence_term url, either with or without slash,
                # is retrieved
                if retrieved_urls:
                    ret_urls = [_l[0] for _l in retrieved_urls]
                    _intersection = set(pairs).intersection(set(ret_urls))
                    ret_instances = [_l[1] for _l in retrieved_urls if _l[0] in _intersection]
                    same_url_licences = generic_instances.filter(pk__in=ret_instances)
                    retrieved_generic_repo_list.extend([repo.pk for repo in same_url_licences])
        # Retrieve generic repos to merge by name matching
        generic_instances = generic_repo.filter(repository_name__en__iexact=repository_name['en'])
        retrieved_generic_repo_list.extend([repo.pk for repo in generic_instances])
        # Merge repositories
        list_of_pairs_to_merge = list()
        for repo_source in list(set(retrieved_generic_repo_list)):
            list_of_pairs_to_merge.append((repo_source, repo_target.pk))
        return list_of_pairs_to_merge


def manage_post_publication_merging(record_pk):
    record_curation = PostPublicationMerge()
    record_curation(record_pk)
    return 'Possible record curation underway.'


def manage_record_elg_compatible_check(record_pk):
    from registry.models import MetadataRecord
    record = MetadataRecord.objects.get(pk=record_pk)
    records_affected = set()

    LOGGER.info(f'Check {record} for ELG compatible LRs')
    # During publication of a ToolService, check if Tool/Service is ELG compatible
    # and find affected metadata records, ie points to published, not deleted model via ml_model
    if record.described_entity.entity_type == 'LanguageResource' and \
       record.described_entity.lr_subclass.lr_type == 'ToolService' and \
        record.management_object.status == 'p' and not record.management_object.deleted and \
        record.management_object.functional_service:
        LOGGER.info(f'{record} checks conditions for ToolService')
        lrs = record.described_entity.lr_subclass.ml_model.all()
        print(f'{record} has {len(lrs)} ml_model')
        for lr in lrs:
            record_affected = getattr(lr, 'metadata_record_of_described_entity', None)
            if record_affected and record_affected.management_object.status == 'p' and \
                not record_affected.management_object.deleted and \
                record_affected.described_entity.lr_subclass.lr_type == 'LanguageDescription' and \
                record_affected.described_entity.lr_subclass.ld_subclass == LD_SUBCLASS_CHOICES.MODEL:
                LOGGER.info(f'Affected record {record_affected} checks conditions for Model')
                record_affected.management_object.elg_compatible_model = True
                record_affected.management_object.save()
                apf = AutomaticPublicationFieldGeneration(record_affected)
                apf.generate_landing_page(save_manager=True)
                records_affected.add(record_affected)
    LOGGER.info(f'Update indices for affected records')
    from registry.search_indexes.documents import MetadataDocument
    MetadataDocument().catalogue_update(list(records_affected))
    return list(records_affected)


@celery_app.task(name='post_publication_actions')
def manage_post_publication_actions(record_pk):
    manage_post_publication_merging(record_pk)
    manage_record_elg_compatible_check(record_pk)
    return 'Possible post publication actions underway.'
