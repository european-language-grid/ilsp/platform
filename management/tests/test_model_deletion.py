import json
import logging

from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.deletion import ProtectedError
from test_utils import SerializerTestCase

from accounts.models import ELGUser
from management.models import Manager, ContentFile, Validator
from registry import serializers
from registry.models import MetadataRecord, SoftwareDistribution, DatasetDistribution

LOGGER = logging.getLogger(__name__)


class TestDeletionFunctionalityManager(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = ELGUser.objects.create(username='test-manager@test.com',
                                      first_name='test',
                                      last_name='manager',
                                      email='test-manager@test.com')
        legal, _ = Group.objects.get_or_create(name='legal_validator')
        meta, _ = Group.objects.get_or_create(name='metadata_validator')
        tech, _ = Group.objects.get_or_create(name='technical_validator')
        user.groups.add(legal)
        user.groups.add(meta)
        user.groups.add(tech)
        user.save()
        # From JSON data, create a metadata record for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()
        instance.management_object.save()
        instance.management_object.ingest()
        instance.management_object.curator = user
        instance.management_object.legal_validator = user
        instance.management_object.metadata_validator = user
        instance.management_object.technical_validator = user
        instance.management_object.save()
        cls.mdr_manager = Manager.objects.get(id=instance.management_object.pk)
        manager = Manager.objects.create()
        manager.curator = user
        manager.legal_validator = user
        manager.metadata_validator = user
        manager.technical_validator = user
        manager.save()
        c_f = ContentFile.objects.create(file=f'{manager.identifier}/test_filename.zip',
                                         record_manager=manager)
        # Retrieve pk from nested models
        cls.nested_models = dict()
        cls.nested_models['curator'] = manager.curator.pk
        cls.nested_models['legal_val'] = manager.legal_validator.pk
        cls.nested_models['meta_val'] = manager.metadata_validator.pk
        cls.nested_models['tech_val'] = manager.technical_validator.pk
        cls.nested_models['content_file'] = c_f.pk
        manager.delete()

    def test_metadata_record_deletion(self):
        try:
            manager_id = self.mdr_manager.pk
            mdr_id = self.mdr_manager.metadata_record_of_manager.pk
            self.mdr_manager.delete()
            try:
                obj = MetadataRecord.objects.get(
                    id=mdr_id)
                manager = Manager.objects.get(id=manager_id)
            except ObjectDoesNotExist:
                self.assertTrue(False)
            self.assertTrue(False)
        except ProtectedError:
            self.assertTrue(True)

    def test_curator_deletion(self):
        try:
            obj = ELGUser.objects.get(
                id=self.nested_models['curator'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_legal_validator_deletion(self):
        try:
            obj = ELGUser.objects.get(
                id=self.nested_models['legal_val'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_metadata_validator_deletion(self):
        try:
            obj = ELGUser.objects.get(
                id=self.nested_models['meta_val'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_technical_validator_deletion(self):
        try:
            obj = ELGUser.objects.get(
                id=self.nested_models['tech_val'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_content_file_deletion(self):
        try:
            obj = ContentFile.objects.get(
                id=self.nested_models['content_file'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityContentFile(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = ELGUser.objects.create(username='test-manager@test.com',
                                      first_name='test',
                                      last_name='manager',
                                      email='test-manager@test.com')
        legal, _ = Group.objects.get_or_create(name='legal_validator')
        meta, _ = Group.objects.get_or_create(name='metadata_validator')
        tech, _ = Group.objects.get_or_create(name='technical_validator')
        user.groups.add(legal)
        user.groups.add(meta)
        user.groups.add(tech)
        user.save()
        # From JSON data, create a metadata record for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()
        instance.management_object.save()
        instance.management_object.curator = user
        instance.management_object.save()
        c_f = ContentFile.objects.create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                         record_manager=instance.management_object)
        soft_dist = SoftwareDistribution.objects.get(
            id=instance.described_entity.lr_subclass.software_distribution.first().id)
        soft_dist.package = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        soft_dist.save()
        instance.management_object.ingest()
        json_file_corpus = open('registry/tests/fixtures/corpus.json', 'r')
        json_str_corpus = json_file_corpus.read()
        json_file_corpus.close()

        json_data_corpus = json.loads(json_str_corpus)
        serializer_corpus = serializers.MetadataRecordSerializer(data=json_data_corpus)
        serializer_corpus.is_valid()
        instance_corpus = serializer_corpus.save()
        instance_corpus.management_object.save()
        instance_corpus.management_object.curator = user
        instance_corpus.management_object.save()
        c_f_corpus = ContentFile.objects.create(file=f'{instance_corpus.management_object.identifier}/test_filename.zip',
                                         record_manager=instance_corpus.management_object)
        data_dist = DatasetDistribution.objects.get(
            id=instance_corpus.described_entity.lr_subclass.dataset_distribution.first().id)
        data_dist.package = ContentFile.objects.get(file=f'{instance_corpus.management_object.identifier}/test_filename.zip')
        soft_dist.save()
        instance_corpus.management_object.ingest()
        # Retrieve pk from nested models
        cls.nested_models = dict()
        cls.nested_models['management_object'] = instance.management_object.pk
        cls.nested_models['software_distribution'] = soft_dist.pk
        cls.nested_models['dataset_distribution'] = data_dist.pk
        c_f.delete()
        c_f_corpus.delete()

    def test_manager_deletion(self):
        try:
            obj = Manager.objects.get(
                id=self.nested_models['management_object'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_software_dist_deletion(self):
        try:
            obj = SoftwareDistribution.objects.get(
                id=self.nested_models['software_distribution'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_dataset_dist_deletion(self):
        try:
            obj = DatasetDistribution.objects.get(
                id=self.nested_models['dataset_distribution'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityValidator(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = ELGUser.objects.create(username='test-val@test.com',
                                      first_name='test',
                                      last_name='val',
                                      email='test-val@test.com')
        meta, _ = Group.objects.get_or_create(name='metadata_validator')
        user.groups.add(meta)
        user.save()
        meta_val, _ = Validator.objects.get_or_create(user=user, assigned_for='Metadata')
        # From JSON data, create a metadata record for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        serializer = serializers.MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()
        instance.management_object.curator = user
        instance.management_object.save()
        instance.management_object.ingest()
        # Retrieve pk from nested models
        cls.nested_models = dict()
        cls.nested_models['user'] = meta_val.user.pk
        cls.nested_models['manager'] = meta_val.assigned_to.get(id=instance.management_object.pk).pk
        meta_val.delete()

    def test_user_deletion(self):
        try:
            obj = ELGUser.objects.get(
                id=self.nested_models['user'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_manager_deletion(self):
        try:
            obj = Manager.objects.get(
                id=self.nested_models['manager'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)
