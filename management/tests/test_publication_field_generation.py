import copy
import json
from test_utils import SerializerTestCase
from management.utils.publication_field_generation import AutomaticPublicationFieldGeneration, \
    manage_record_elg_compatible_check
from registry.serializers import (
    MetadataRecordSerializer, GenericLanguageResourceSerializer
)
from registry.models import MetadataRecord


class TestAutomaticPublicationFieldGeneration(SerializerTestCase):

    @classmethod
    def setUpTestData(cls):
        # Load data for model
        json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.json_data_model = json.loads(json_str)
        # Load data for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.json_data_tool = json.loads(json_str)
        # Load data for corpus
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.json_data_corpus = json.loads(json_str)
        # Load data for grammar
        json_file = open('registry/tests/fixtures/ld_grammar.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.json_data_grammar = json.loads(json_str)

    def test_generate_elg_compatible_model_is_elg_compatible(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Functional model'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, is published and not deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        apf = AutomaticPublicationFieldGeneration(serializer_model.instance)
        apf.generate_elg_compatible()
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertTrue(instance_model.management_object.elg_compatible_model)

    def test_generate_elg_compatible_model_is_not_elg_compatible_service_not_published(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Functional model'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, is not published and not deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'i'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        apf = AutomaticPublicationFieldGeneration(serializer_model.instance)
        apf.generate_elg_compatible()
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertFalse(instance_model.management_object.elg_compatible_model)

    def test_generate_elg_compatible_model_is_not_elg_compatible_service_deleted(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Functional model'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, is published and deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = True
        instance_tool.management_object.save()

        apf = AutomaticPublicationFieldGeneration(serializer_model.instance)
        apf.generate_elg_compatible()
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertFalse(instance_model.management_object.elg_compatible_model)

    def test_generate_elg_compatible_model_is_not_elg_compatible_service_not_functional(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Functional model'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()

        # Create service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'None Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': False})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as not functional, is published and not deleted
        instance_tool.management_object.functional_service = False
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        apf = AutomaticPublicationFieldGeneration(serializer_model.instance)
        apf.generate_elg_compatible()
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertFalse(instance_model.management_object.elg_compatible_model)

    def test_generate_elg_compatible_corpus_is_not_elg_compatible(self):
        # Create corpus
        json_corpus = copy.deepcopy(self.json_data_corpus)
        json_corpus['described_entity']['resource_name']['en'] = 'Corpus'
        serializer_corpus = MetadataRecordSerializer(data=json_corpus)
        self.assertTrue(serializer_corpus.is_valid())
        instance_corpus = serializer_corpus.save()

        apf = AutomaticPublicationFieldGeneration(serializer_corpus.instance)
        apf.generate_elg_compatible()
        # Get instance_corpus again to be sure you have an updated version before asserting
        instance_corpus = MetadataRecord.objects.get(pk=instance_corpus.pk)
        self.assertFalse(instance_corpus.management_object.elg_compatible_model)

    def test_generate_elg_compatible_grammar_is_not_elg_compatible(self):
        # Create grammar
        json_grammar = copy.deepcopy(self.json_data_grammar)
        json_grammar['described_entity']['resource_name']['en'] = 'Grammar'
        serializer_grammar = MetadataRecordSerializer(data=json_grammar)
        self.assertTrue(serializer_grammar.is_valid())
        instance_grammar = serializer_grammar.save()

        apf = AutomaticPublicationFieldGeneration(serializer_grammar.instance)
        apf.generate_elg_compatible()
        # Get instance_grammar again to be sure you have an updated version before asserting
        instance_grammar = MetadataRecord.objects.get(pk=instance_grammar.pk)
        self.assertFalse(instance_grammar.management_object.elg_compatible_model)


class TestManageRecordElgCompatibleCheck(SerializerTestCase):

    @classmethod
    def setUpTestData(cls):
        # Load data for model
        json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.json_data_model = json.loads(json_str)
        # Load data for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.json_data_tool = json.loads(json_str)
        # Load data for corpus
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.json_data_corpus = json.loads(json_str)
        # Load data for grammar
        json_file = open('registry/tests/fixtures/ld_grammar.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.json_data_grammar = json.loads(json_str)

    def test_check_tool_to_elg_compatible_model(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Model to be elg compatible'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()
        # Set conditions that Model is declared is published and not deleted
        instance_model.management_object.status = 'p'
        instance_model.management_object.deleted = False
        instance_model.management_object.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, is published and not deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        records_affected_pks = manage_record_elg_compatible_check(instance_tool.pk)
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertTrue(instance_model.management_object.elg_compatible_model)

    def test_check_tool_not_published_is_not_elg_compatible_model(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Model not to be elg compatible'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()
        # Set conditions that Model is declared is published and not deleted
        instance_model.management_object.status = 'p'
        instance_model.management_object.deleted = False
        instance_model.management_object.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, not published and not deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'i'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        records_affected_pks = manage_record_elg_compatible_check(instance_tool.pk)
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertFalse(instance_model.management_object.elg_compatible_model)

    def test_check_tool_deleted_is_not_elg_compatible_model(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Model not to be elg compatible'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()
        # Set conditions that Model is declared is published and not deleted
        instance_model.management_object.status = 'p'
        instance_model.management_object.deleted = False
        instance_model.management_object.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, is published and deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = True
        instance_tool.management_object.save()

        records_affected_pks = manage_record_elg_compatible_check(instance_tool.pk)
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertFalse(instance_model.management_object.elg_compatible_model)

    def test_check_tool_not_functional_is_not_elg_compatible_model(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Model not to be elg compatible'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()
        # Set conditions that Model is declared is published and not deleted
        instance_model.management_object.status = 'p'
        instance_model.management_object.deleted = False
        instance_model.management_object.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': False})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as not functional, is published and not deleted
        instance_tool.management_object.functional_service = False
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        records_affected_pks = manage_record_elg_compatible_check(instance_tool.pk)
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertFalse(instance_model.management_object.elg_compatible_model)

    def test_check_tool_is_not_elg_compatible_model_not_published(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Model not be elg compatible'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()
        # Set conditions that Model is declared not published and not deleted
        instance_model.management_object.status = 'i'
        instance_model.management_object.deleted = False
        instance_model.management_object.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, is published and not deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        records_affected_pks = manage_record_elg_compatible_check(instance_tool.pk)
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertFalse(instance_model.management_object.elg_compatible_model)

    def test_check_tool_is_not_elg_compatible_model_deleted(self):
        # Create "functional" Model
        json_model = copy.deepcopy(self.json_data_model)
        json_model['described_entity']['resource_name']['en'] = 'Model not be elg compatible'
        serializer_model = MetadataRecordSerializer(data=json_model)
        self.assertTrue(serializer_model.is_valid())
        instance_model = serializer_model.save()
        # Set conditions that Model is declared is published and deleted
        instance_model.management_object.status = 'p'
        instance_model.management_object.deleted = True
        instance_model.management_object.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_model.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, is published and not deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        records_affected_pks = manage_record_elg_compatible_check(instance_tool.pk)
        # Get instance_model again to be sure you have an updated version before asserting
        instance_model = MetadataRecord.objects.get(pk=instance_model.pk)
        self.assertFalse(instance_model.management_object.elg_compatible_model)

    def test_check_tool_is_not_elg_compatible_grammar(self):
        # Create Grammar
        json_grammar = copy.deepcopy(self.json_data_grammar)
        json_grammar['described_entity']['resource_name']['en'] = 'Grammar'
        serializer_grammar = MetadataRecordSerializer(data=json_grammar)
        self.assertTrue(serializer_grammar.is_valid())
        instance_grammar = serializer_grammar.save()
        # Set conditions that Model is declared is published and not deleted
        instance_grammar.management_object.status = 'p'
        instance_grammar.management_object.deleted = False
        instance_grammar.management_object.save()

        # Create functional service and make it point to model
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_grammar.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, is published and not deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        records_affected_pks = manage_record_elg_compatible_check(instance_tool.pk)
        # Get instance_grammar again to be sure you have an updated version before asserting
        instance_grammar = MetadataRecord.objects.get(pk=instance_grammar.pk)
        self.assertFalse(instance_grammar.management_object.elg_compatible_model)

    def test_check_tool_is_not_elg_compatible_corpus(self):
        # Create Corpus
        json_corpus = copy.deepcopy(self.json_data_corpus)
        json_corpus['described_entity']['resource_name']['en'] = 'Corpus'
        serializer_corpus = MetadataRecordSerializer(data=json_corpus)
        self.assertTrue(serializer_corpus.is_valid())
        instance_corpus = serializer_corpus.save()
        # Set conditions that corpus is declared is published and not deleted
        instance_corpus.management_object.status = 'p'
        instance_corpus.management_object.deleted = False
        instance_corpus.management_object.save()

        # Create functional service and make it point to corpus
        json_tool = copy.deepcopy(self.json_data_tool)
        json_tool['described_entity']['resource_name']['en'] = 'Functional tool'
        json_tool['described_entity']['lr_subclass']['ml_model'] = [
            GenericLanguageResourceSerializer(instance=instance_corpus.described_entity).data
        ]
        serializer_tool = MetadataRecordSerializer(data=json_tool, context={'functional_service': True})
        self.assertTrue(serializer_tool.is_valid())
        instance_tool = serializer_tool.save()
        # Set conditions that Tool/Service is declared as functional, is published and not deleted
        instance_tool.management_object.functional_service = True
        instance_tool.management_object.status = 'p'
        instance_tool.management_object.deleted = False
        instance_tool.management_object.save()

        records_affected_pks = manage_record_elg_compatible_check(instance_tool.pk)
        # Get instance_corpus again to be sure you have an updated version before asserting
        instance_corpus = MetadataRecord.objects.get(pk=instance_corpus.pk)
        self.assertFalse(instance_corpus.management_object.elg_compatible_model)