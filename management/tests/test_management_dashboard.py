import logging

from django.conf import settings
from registry.models import MetadataRecord
from test_utils import import_metadata, api_call, get_test_user, EndpointTestCase

LOGGER = logging.getLogger(__name__)


DASHBOARD_ENDPOINT = f'/{settings.HOME_PREFIX}/api/management/dashboard/'


class TestManagementDashboardEndpoint(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_can_see_dashboard_superuser(self):
        user, token = self.admin, self.admin_token
        for i in self.metadata_records:
            i.management_object.curator = user
            i.management_object.save()
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_items/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_dashboard_status_filter(self):
        user, token = self.admin, self.admin_token
        for i in self.metadata_records:
            i.management_object.curator = user
            i.management_object.save()
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_items/?status=published', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_can_not_see_dashboard_anonymous(self):
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_items/', self.client, )
        self.assertEquals(response.get('status_code'), 403)

    def test_can_not_see_dashboard_consumer(self):
        user, token = self.consumer, self.consumer_token
        for i in self.metadata_records:
            i.management_object.curator = user
            i.management_object.save()
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_items/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_can_not_see_dashboard_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        for i in self.metadata_records:
            i.management_object.curator = user
            i.management_object.save()
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_items/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_can_not_see_dashboard_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        for i in self.metadata_records:
            i.management_object.curator = user
            i.management_object.save()
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_items/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_can_not_see_dashboard_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        for i in self.metadata_records:
            i.management_object.curator = user
            i.management_object.save()
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_items/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_can_see_dashboard_content_manager(self):
        user, token = self.c_m, self.c_m_token
        for i in self.metadata_records:
            i.management_object.curator = user
            i.management_object.save()
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_items/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_can_see_dashboard_provider(self):
        user, token = self.provider, self.provider_token
        for i in self.metadata_records:
            i.management_object.curator = user
            i.management_object.save()
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_items/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))


class TestValidationDashboardEndpoint(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)
        for record in cls.metadata_records:
            record.management_object.ingest()

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_can_see_validation_dashboard_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_validations/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_validation_dashboard_status_filter(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}?status=ingested', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_can_not_see_validation_dashboard_anonymous(self):
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_validations/', self.client)
        self.assertEquals(response.get('status_code'), 403)

    def test_can_not_see_validation_dashboard_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_validations/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_can_see_validation_dashboard_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_validations/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_can_see_validation_dashboard_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_validations/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_can_see_validation_dashboard_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_validations/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_can_see_validation_dashboard_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_validations/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_cannot_see_validation_dashboard_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('GET', f'{DASHBOARD_ENDPOINT}my_validations/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)
