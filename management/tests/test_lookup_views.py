import json
import logging

from django.conf import settings
from rolepermissions import roles

from accounts.models import ELGUser
from management.models import Validator, ContentFile
from registry.models import MetadataRecord, DatasetDistribution
from test_utils import import_metadata, api_call, get_test_user, EndpointTestCase

LOGGER = logging.getLogger(__name__)

MDR_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/metadatarecord/'
MNG_ENDPOINT = f'/{settings.HOME_PREFIX}/api/management/'


class TestCuratorsLookupView(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json',
                 'registry/tests/fixtures/corpus.json', 'registry/tests/fixtures/ld_model.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    raw_data_tool = json.loads(open(test_data[1], encoding='utf-8').read())
    raw_data_lr_with_data = json.loads(open(test_data[2], encoding='utf-8').read())
    raw_data_lr_no_data = json.loads(open(test_data[3], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.curatorA1 = ELGUser.objects.create(username='curatorA1')
        roles.assign_role(cls.curatorA1, 'provider')
        cls.cm1 = ELGUser.objects.create(username='cm1')
        cls.curatorA2 = ELGUser.objects.create(username='curatorA2')
        roles.assign_role(cls.curatorA2, 'provider')
        cls.legal_validatorA1 = ELGUser.objects.create(username='legal_validatorA1')
        cls.legal_validatorA2 = ELGUser.objects.create(username='legal_validatorA2')
        cls.metadata_validatorA1 = ELGUser.objects.create(username='metadata_validatorA1')
        cls.metadata_validatorA2 = ELGUser.objects.create(username='metadata_validatorA2')
        cls.technical_validatorA1 = ELGUser.objects.create(username='technical_validatorA1')
        cls.technical_validatorA2 = ELGUser.objects.create(username='technical_validatorA2')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()
    
    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_lookup_curator_superuser(self):
        user, token = self.admin, self.admin_token
        roles.assign_role(self.legal_validatorA1, 'legal_validator')
        Validator.objects.get_or_create(user=self.legal_validatorA1, assigned_for='Legal')
        roles.assign_role(self.legal_validatorA2, 'legal_validator')
        Validator.objects.get_or_create(user=self.legal_validatorA2, assigned_for='Legal')
        roles.assign_role(self.technical_validatorA1, 'technical_validator')
        Validator.objects.get_or_create(user=self.technical_validatorA1, assigned_for='Technical')
        roles.assign_role(self.metadata_validatorA1, 'metadata_validator')
        Validator.objects.get_or_create(user=self.metadata_validatorA1, assigned_for='Metadata')
        roles.assign_role(self.cm1, 'content_manager')
        instance = MetadataRecord.objects.get(id=self.metadata_records[2].id)
        ContentFile.objects.get_or_create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                       record_manager=instance.management_object)
        dist = DatasetDistribution.objects.get(id=instance.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset_distribution_form = 'http://w3id.org/meta-share/meta-share/downloadable'
        dist.dataset = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        dist.save()
        ingestion = api_call('PATCH', f'{MNG_ENDPOINT}ingest/?record_id__in={instance.id}',
                             self.client, auth=self.authenticate(token), data=self.raw_data)
        ingested_instance = MetadataRecord.objects.get(id=instance.id)
        instance_2 = MetadataRecord.objects.get(id=self.metadata_records[3].id)
        ContentFile.objects.get_or_create(file=f'{instance_2.management_object.identifier}/test_filename.zip',
                                       record_manager=instance_2.management_object)
        dist = DatasetDistribution.objects.get(id=instance_2.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset = ContentFile.objects.get(file=f'{instance_2.management_object.identifier}/test_filename.zip')
        dist.save()
        ingestion_2 = api_call('PATCH', f'{MNG_ENDPOINT}ingest/?record_id__in={instance_2.id}',
                             self.client, auth=self.authenticate(token))
        ingested_instance_2 = MetadataRecord.objects.get(id=instance_2.id)
        ingested_instance_2.management_object.curator = self.curatorA2
        ingested_instance_2.management_object.save()
        response = api_call('GET', f'{MNG_ENDPOINT}curator/lookup/?curator_id={instance_2.management_object.curator.id}',
                            self.client, auth=self.authenticate(token))
        
        self.assertEquals(response.get('status_code'), 200)

    def test_lookup_curator_superuser_fails_with_no_param(self):
        user, token = self.admin, self.admin_token
        roles.assign_role(self.legal_validatorA1, 'legal_validator')
        Validator.objects.get_or_create(user=self.legal_validatorA1, assigned_for='Legal')
        roles.assign_role(self.legal_validatorA2, 'legal_validator')
        Validator.objects.get_or_create(user=self.legal_validatorA2, assigned_for='Legal')
        roles.assign_role(self.technical_validatorA1, 'technical_validator')
        Validator.objects.get_or_create(user=self.technical_validatorA1, assigned_for='Technical')
        roles.assign_role(self.metadata_validatorA1, 'metadata_validator')
        Validator.objects.get_or_create(user=self.metadata_validatorA1, assigned_for='Metadata')
        roles.assign_role(self.cm1, 'content_manager')
        instance = MetadataRecord.objects.get(id=self.metadata_records[2].id)
        ContentFile.objects.get_or_create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                       record_manager=instance.management_object)
        dist = DatasetDistribution.objects.get(id=instance.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset_distribution_form = 'http://w3id.org/meta-share/meta-share/downloadable'
        dist.dataset = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        dist.save()
        ingestion = api_call('PATCH', f'{MNG_ENDPOINT}ingest/?record_id__in={instance.id}',
                             self.client, auth=self.authenticate(token), data=self.raw_data)
        ingested_instance = MetadataRecord.objects.get(id=instance.id)
        instance_2 = MetadataRecord.objects.get(id=self.metadata_records[3].id)
        ContentFile.objects.get_or_create(file=f'{instance_2.management_object.identifier}/test_filename.zip',
                                       record_manager=instance_2.management_object)
        dist = DatasetDistribution.objects.get(id=instance_2.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset = ContentFile.objects.get(file=f'{instance_2.management_object.identifier}/test_filename.zip')
        dist.save()
        ingestion_2 = api_call('PATCH', f'{MNG_ENDPOINT}ingest/?record_id__in={instance_2.id}',
                             self.client, auth=self.authenticate(token))
        ingested_instance_2 = MetadataRecord.objects.get(id=instance_2.id)
        ingested_instance_2.management_object.curator = self.curatorA2
        ingested_instance_2.management_object.save()
        response = api_call('GET', f'{MNG_ENDPOINT}curator/lookup/',
                            self.client, auth=self.authenticate(token))
        
        self.assertEquals(response.get('status_code'), 400)

    def test_lookup_curator_provider(self):
        user, token = self.provider, self.provider_token
        roles.assign_role(self.legal_validatorA1, 'legal_validator')
        Validator.objects.get_or_create(user=self.legal_validatorA1, assigned_for='Legal')
        roles.assign_role(self.legal_validatorA2, 'legal_validator')
        Validator.objects.get_or_create(user=self.legal_validatorA2, assigned_for='Legal')
        roles.assign_role(self.technical_validatorA1, 'technical_validator')
        Validator.objects.get_or_create(user=self.technical_validatorA1, assigned_for='Technical')
        roles.assign_role(self.metadata_validatorA1, 'metadata_validator')
        Validator.objects.get_or_create(user=self.metadata_validatorA1, assigned_for='Metadata')
        roles.assign_role(self.cm1, 'content_manager')
        instance = MetadataRecord.objects.get(id=self.metadata_records[2].id)
        ContentFile.objects.get_or_create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                       record_manager=instance.management_object)
        dist = DatasetDistribution.objects.get(id=instance.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset_distribution_form = 'http://w3id.org/meta-share/meta-share/downloadable'
        dist.dataset = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        dist.save()
        ingestion = api_call('PATCH', f'{MNG_ENDPOINT}ingest/?record_id__in={instance.id}',
                             self.client, auth=self.authenticate(token), data=self.raw_data)
        ingested_instance = MetadataRecord.objects.get(id=instance.id)
        instance_2 = MetadataRecord.objects.get(id=self.metadata_records[3].id)
        ContentFile.objects.get_or_create(file=f'{instance_2.management_object.identifier}/test_filename.zip',
                                       record_manager=instance_2.management_object)
        dist = DatasetDistribution.objects.get(id=instance_2.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset = ContentFile.objects.get(file=f'{instance_2.management_object.identifier}/test_filename.zip')
        dist.save()
        ingestion_2 = api_call('PATCH', f'{MNG_ENDPOINT}ingest/?record_id__in={instance_2.id}',
                             self.client, auth=self.authenticate(token))
        ingested_instance_2 = MetadataRecord.objects.get(id=instance_2.id)
        ingested_instance_2.management_object.curator = self.curatorA2
        ingested_instance_2.management_object.save()
        response = api_call('GET', f'{MNG_ENDPOINT}curator/lookup/?curator_id={instance_2.management_object.curator.id}',
                            self.client, auth=self.authenticate(token))
        
        self.assertEquals(response.get('status_code'), 200)

    def test_lookup_curator_content_manager(self):
        user, token = self.c_m, self.c_m_token
        instance = MetadataRecord.objects.get(id=self.metadata_records[2].id)
        ContentFile.objects.get_or_create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                          record_manager=instance.management_object)
        dist = DatasetDistribution.objects.get(id=instance.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset_distribution_form = 'http://w3id.org/meta-share/meta-share/downloadable'
        dist.dataset = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        dist.save()
        instance_2 = MetadataRecord.objects.get(id=self.metadata_records[1].id)
        response = api_call('GET', f'{MNG_ENDPOINT}curator/lookup/?curator_id={instance_2.management_object.curator.id}',
                            self.client, auth=self.authenticate(token))
        
        self.assertEquals(response.get('status_code'), 200)

    def test_lookup_curator_validator(self):
        user, token = self.legal_val, self.legal_val_token
        instance = MetadataRecord.objects.get(id=self.metadata_records[2].id)
        ContentFile.objects.get_or_create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                          record_manager=instance.management_object)
        dist = DatasetDistribution.objects.get(id=instance.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset_distribution_form = 'http://w3id.org/meta-share/meta-share/downloadable'
        dist.dataset = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        dist.save()
        instance_2 = MetadataRecord.objects.get(id=self.metadata_records[1].id)
        response = api_call('GET', f'{MNG_ENDPOINT}curator/lookup/?curator_id={instance_2.management_object.curator.id}',
                            self.client, auth=self.authenticate(token))
        
        self.assertEquals(response.get('status_code'), 403)

    def test_lookup_curator_consumer(self):
        user, token = self.consumer, self.consumer_token
        instance = MetadataRecord.objects.get(id=self.metadata_records[2].id)
        ContentFile.objects.get_or_create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                          record_manager=instance.management_object)
        dist = DatasetDistribution.objects.get(id=instance.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset_distribution_form = 'http://w3id.org/meta-share/meta-share/downloadable'
        dist.dataset = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        dist.save()
        instance_2 = MetadataRecord.objects.get(id=self.metadata_records[1].id)
        response = api_call('GET',
                            f'{MNG_ENDPOINT}curator/lookup/?curator_id={instance_2.management_object.curator.id}',
                            self.client, auth=self.authenticate(token))
        
        self.assertEquals(response.get('status_code'), 403)

    def test_lookup_curator_anonymus(self):
        instance = MetadataRecord.objects.get(id=self.metadata_records[2].id)
        ContentFile.objects.get_or_create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                          record_manager=instance.management_object)
        dist = DatasetDistribution.objects.get(id=instance.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset_distribution_form = 'http://w3id.org/meta-share/meta-share/downloadable'
        dist.dataset = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        dist.save()
        instance_2 = MetadataRecord.objects.get(id=self.metadata_records[1].id)
        response = api_call('GET',
                            f'{MNG_ENDPOINT}curator/lookup/?curator_id={instance_2.management_object.curator.id}',
                            self.client)
        
        self.assertEquals(response.get('status_code'), 403)
