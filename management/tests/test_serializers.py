import copy
import json
import logging
import requests
from django.conf import settings
from test_utils import SerializerTestCase
from rolepermissions import roles

from accounts.api import UserSerializer
from accounts.models import ELGUser
from management.models import Validator, ContentFile
from management.tests import keycloak
from management.serializers import CuratorSerializer, TechnicalValidatorSerializer, LegalValidatorSerializer, \
    MetadataValidatorSerializer, ManagerSerializer
from registry.models import SoftwareDistribution
from test_utils import import_metadata

LOGGER = logging.getLogger(__name__)


class TestCuratorSerializer(SerializerTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    raw_data_2 = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpTestData(cls):
        cls.curatorA1, _ = ELGUser.objects.get_or_create(username='curatorA1')
        roles.assign_role(cls.curatorA1, 'provider')
        cls.cm1, _ = ELGUser.objects.get_or_create(username='cm1')
        cls.curatorA2, _ = ELGUser.objects.get_or_create(username='curatorA2')
        roles.assign_role(cls.curatorA2, 'provider')
        cls.metadata_records = import_metadata(cls.test_data)

    def test_curator_serializer_contains_expected_fields(self):
        serializer = CuratorSerializer(self.metadata_records[0].management_object)
        data = serializer.data
        self.assertEquals(tuple(data.keys()),
                          tuple(CuratorSerializer.Meta.fields))

    def test_curator_serializer_fields_work_as_intended(self):
        instance = copy.deepcopy(self.metadata_records[0])
        serializer = CuratorSerializer(instance.management_object)
        data = serializer.data
        self.assertEquals(data['record'],
                          f'{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/{instance.pk}/')
        self.assertEquals(data['curator'], instance.management_object.curator.id)
        self.assertEquals(data['curator_username'], instance.management_object.curator.username)
        self.assertTrue(UserSerializer(self.curatorA1).data in data['available_curators'])
        self.assertTrue(UserSerializer(self.curatorA2).data in data['available_curators'])


class TestTechnicalValidatorSerializer(SerializerTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    raw_data_2 = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpTestData(cls):
        cls.legal_validatorA1, _ = ELGUser.objects.get_or_create(username='legal_validator_test_1')
        roles.assign_role(cls.legal_validatorA1, 'legal_validator')
        Validator.objects.get_or_create(user=cls.legal_validatorA1, assigned_for='Legal')
        cls.metadata_validatorA1, _ = ELGUser.objects.get_or_create(username='metadata_validator_test_1')
        roles.assign_role(cls.metadata_validatorA1, 'metadata_validator')
        Validator.objects.get_or_create(user=cls.metadata_validatorA1, assigned_for='Metadata')
        cls.technical_validatorA1, _ = ELGUser.objects.get_or_create(username='technical_validator_test_1')
        roles.assign_role(cls.technical_validatorA1, 'technical_validator')
        Validator.objects.get_or_create(user=cls.technical_validatorA1, assigned_for='Technical')
        cls.cm1, _ = ELGUser.objects.get_or_create(username='cm1')
        cls.technical_validatorA2, _ = ELGUser.objects.get_or_create(username='technical_validator_test_2')
        roles.assign_role(cls.technical_validatorA2, 'technical_validator')
        Validator.objects.get_or_create(user=cls.technical_validatorA2, assigned_for='Technical')
        cls.providerA1, _ = ELGUser.objects.get_or_create(username='provider_for_sure')
        roles.assign_role(cls.providerA1, 'provider')
        cls.metadata_records = import_metadata(cls.test_data)

    def test_technical_validator_serializer_contains_expected_fields(self):
        serializer = TechnicalValidatorSerializer(self.metadata_records[0].management_object)
        data = serializer.data
        self.assertEquals(tuple(data.keys()),
                          tuple(TechnicalValidatorSerializer.Meta.fields))

    def test_technical_validator_serializer_fields_work_as_intended(self):
        instance = copy.deepcopy(self.metadata_records[1])
        ContentFile.objects.get_or_create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                       record_manager=instance.management_object)
        dist = SoftwareDistribution.objects.get(id=instance.described_entity.lr_subclass.software_distribution.first().id)
        dist.package = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        dist.save()
        instance.management_object.ingest()
        serializer = TechnicalValidatorSerializer(instance.management_object)
        data = serializer.data
        self.assertEquals(data['record'],
                          f'{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/{instance.pk}/')
        self.assertEquals(data['technical_validator'], instance.management_object.technical_validator.id)
        self.assertTrue(UserSerializer(self.technical_validatorA1).data in data['available_technical_validators'])
        self.assertTrue(UserSerializer(self.technical_validatorA2).data in data['available_technical_validators'])

    def test_technical_validator_serializer_update(self):
        instance = copy.deepcopy(self.metadata_records[0])
        instance.management_object.ingest()
        serializer = TechnicalValidatorSerializer(instance.management_object,
                                                  data={'technical_validator': self.providerA1})
        if serializer.is_valid():
            instance_2 = serializer.save()
        self.assertFalse(serializer.is_valid())


class TestLegalValidatorSerializer(SerializerTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    raw_data_2 = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpTestData(cls):
        cls.legal_validatorA1, _ = ELGUser.objects.get_or_create(username='legal_validator_test_1')
        roles.assign_role(cls.legal_validatorA1, 'legal_validator')
        Validator.objects.get_or_create(user=cls.legal_validatorA1, assigned_for='Legal')
        cls.cm1, _ = ELGUser.objects.get_or_create(username='cm1')
        cls.legal_validatorA2, _ = ELGUser.objects.get_or_create(username='legal_validator_test_2')
        roles.assign_role(cls.legal_validatorA2, 'legal_validator')
        Validator.objects.get_or_create(user=cls.legal_validatorA2, assigned_for='Legal')
        cls.providerA1, _ = ELGUser.objects.get_or_create(username='provider_for_sure')
        roles.assign_role(cls.providerA1, 'provider')
        cls.metadata_records = import_metadata(cls.test_data)

    def test_legal_validator_serializer_contains_expected_fields(self):
        serializer = LegalValidatorSerializer(self.metadata_records[0].management_object)
        data = serializer.data
        self.assertEquals(tuple(data.keys()),
                          tuple(LegalValidatorSerializer.Meta.fields))

    def test_legal_validator_serializer_fields_work_as_intended(self):
        instance = copy.deepcopy(self.metadata_records[1])
        ContentFile.objects.get_or_create(file=f'{instance.management_object.identifier}/test_filename.zip',
                                       record_manager=instance.management_object)
        dist = SoftwareDistribution.objects.get(id=instance.described_entity.lr_subclass.software_distribution.first().id)
        dist.package = ContentFile.objects.get(file=f'{instance.management_object.identifier}/test_filename.zip')
        dist.save()
        instance.management_object.ingest()
        serializer = LegalValidatorSerializer(instance.management_object)
        data = serializer.data
        self.assertEquals(data['record'],
                          f'{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/{instance.pk}/')
        self.assertEquals(data['legal_validator'], instance.management_object.legal_validator.id)
        self.assertTrue(len(data['available_legal_validators']) >= 3)

    def test_legal_validator_serializer_update(self):
        instance = copy.deepcopy(self.metadata_records[0])
        instance.management_object.ingest()
        serializer = LegalValidatorSerializer(instance.management_object, data={'legal_validator': self.providerA1})
        self.assertFalse(serializer.is_valid())


class TestMetadataValidatorSerializer(SerializerTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    raw_data_2 = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpTestData(cls):
        cls.metadata_validatorA1, _ = ELGUser.objects.get_or_create(username='metadata_validator_test_1')
        roles.assign_role(cls.metadata_validatorA1, 'metadata_validator')
        Validator.objects.get_or_create(user=cls.metadata_validatorA1, assigned_for='Metadata')
        cls.cm1, _ = ELGUser.objects.get_or_create(username='cm1')
        cls.metadata_validatorA2, _ = ELGUser.objects.get_or_create(username='metadata_validator_test_2')
        roles.assign_role(cls.metadata_validatorA2, 'metadata_validator')
        Validator.objects.get_or_create(user=cls.metadata_validatorA2, assigned_for='Metadata')
        cls.providerA1, _ = ELGUser.objects.get_or_create(username='provider_for_sure')
        roles.assign_role(cls.providerA1, 'provider')
        cls.metadata_records = import_metadata(cls.test_data)

    def test_metadata_validator_serializer_contains_expected_fields(self):
        serializer = MetadataValidatorSerializer(self.metadata_records[0].management_object)
        data = serializer.data
        self.assertEquals(tuple(data.keys()),
                          tuple(MetadataValidatorSerializer.Meta.fields))

    def test_metadata_validator_serializer_fields_work_as_intended(self):
        instance = copy.deepcopy(self.metadata_records[0])
        instance.management_object.ingest()
        serializer = MetadataValidatorSerializer(instance.management_object)
        data = serializer.data
        self.assertEquals(data['record'],
                          f'{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/{instance.pk}/')
        self.assertEquals(data['metadata_validator'], instance.management_object.metadata_validator.id)
        self.assertTrue(len(data['available_metadata_validators']) >= 3)

    def test_metadata_validator_serializer_update(self):
        instance = copy.deepcopy(self.metadata_records[0])
        instance.management_object.ingest()
        serializer = MetadataValidatorSerializer(instance.management_object,
                                                 data={'metadata_validator': self.providerA1})
        self.assertFalse(serializer.is_valid())


class TestManagerSerializer(SerializerTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    raw_data_2 = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpTestData(cls):
        cls.metadata_validatorA1, _ = ELGUser.objects.get_or_create(username='metadata_validator_test_1')
        roles.assign_role(cls.metadata_validatorA1, 'metadata_validator')
        Validator.objects.get_or_create(user=cls.metadata_validatorA1, assigned_for='Metadata')
        cls.cm1, _ = ELGUser.objects.get_or_create(username='cm1')
        cls.metadata_validatorA2, _ = ELGUser.objects.get_or_create(username='metadata_validator_test_2')
        roles.assign_role(cls.metadata_validatorA2, 'metadata_validator')
        Validator.objects.get_or_create(user=cls.metadata_validatorA2, assigned_for='Metadata')
        cls.providerA1, _ = ELGUser.objects.get_or_create(username='provider_for_sure')
        roles.assign_role(cls.providerA1, 'provider')
        cls.metadata_records = import_metadata(cls.test_data)

    def test_manager_serializer_contains_expected_fields(self):
        data = {}
        serializer = ManagerSerializer(data=data)
        if serializer.is_valid():
            self.assertTrue(True)
        else:
            self.assertTrue(False)
