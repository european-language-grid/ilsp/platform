import copy
import json
import logging
from unittest import skip

from django.conf import settings
from django.contrib.admin.sites import AdminSite
from rolepermissions import roles

from management.models import Manager, Validator, ContentFile
from management.admin import (
    LegalValidation, MetadataValidation, TechnicalValidation, ValidationAdmin,
    LegalValidationAdmin, MetadataValidationAdmin, TechnicalValidationAdmin, LegalValidatorForm,
    TechnicalValidatorForm, MetadataValidatorForm
)
from processing.serializers import RegisteredLTServiceSerializer
from registry.models import MetadataRecord, SoftwareDistribution, DatasetDistribution
from registry.serializers import MetadataRecordSerializer
from test_utils import (
    import_metadata, MockRequest, get_test_user, EndpointTestCase
)

LOGGER = logging.getLogger(__name__)

ADMIN_ENDPOINT = f'/{settings.HOME_PREFIX}/admin/'


class TestLegalValidatorForm(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    @skip('admin model deprecated')
    def test_legal_validator_form(self):
        form_data = {
            'review_comment': 'Review comment here',
            'validator_note': 'Validator note here',
            'slack_channel': 'http://www.slackchannelurl.com',
            'jira_ticket': 'http://www.jiraticketurl.com',
            'legally_approve': None
        }
        form = LegalValidatorForm(data=form_data, instance=self.metadata_records[0].management_object)
        self.assertTrue(form.is_valid())

    @skip('admin model deprecated')
    def test_legal_validator_form_passes_when_legally_valid_but_no_review_comment(self):
        form_data = {
            'review_comment': None,
            'validator_note': 'Validator note here',
            'slack_channel': 'http://www.slackchannelurl.com',
            'jira_ticket': 'http://www.jiraticketurl.com',
            'legally_approve': True
        }
        form = LegalValidatorForm(data=form_data, instance=self.metadata_records[0].management_object)
        self.assertTrue(form.is_valid())

    @skip('admin model deprecated')
    def tet_legal_validator_form_fails_when_not_legally_valid_but_no_review_comment(self):
        form_data = {
            'review_comment': None,
            'validator_note': 'Validator note here',
            'slack_channel': 'http://www.slackchannelurl.com',
            'jira_ticket': 'http://www.jiraticketurl.com',
            'legally_approve': False
        }
        form = LegalValidatorForm(data=form_data, instance=self.metadata_records[0].management_object)
        self.assertFalse(form.is_valid())


class TestMetadataValidatorForm(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    @skip('admin model deprecated')
    def test_metadata_validator_form(self):
        form_data = {
            'review_comment': 'Review comment here',
            'validator_note': 'Validator note here',
            'slack_channel': 'http://www.slackchannelurl.com',
            'jira_ticket': 'http://www.jiraticketurl.com',
            'metadata_approve': None
        }
        form = MetadataValidatorForm(data=form_data, instance=self.metadata_records[0].management_object)
        self.assertTrue(form.is_valid())

    @skip('admin model deprecated')
    def test_metadata_validator_form_passes_when_legally_valid_but_no_review_comment(self):
        form_data = {
            'review_comment': None,
            'validator_note': 'Validator note here',
            'slack_channel': 'http://www.slackchannelurl.com',
            'jira_ticket': 'http://www.jiraticketurl.com',
            'metadata_approve': True
        }
        form = MetadataValidatorForm(data=form_data, instance=self.metadata_records[0].management_object)
        self.assertTrue(form.is_valid())

    @skip('admin model deprecated')
    def tet_metadata_validator_form_fails_when_not_legally_valid_but_no_review_comment(self):
        form_data = {
            'review_comment': None,
            'validator_note': 'Validator note here',
            'slack_channel': 'http://www.slackchannelurl.com',
            'jira_ticket': 'http://www.jiraticketurl.com',
            'metadata_approve': False
        }
        form = MetadataValidatorForm(data=form_data, instance=self.metadata_records[0].management_object)
        self.assertFalse(form.is_valid())


class TestTechnicalValidatorForm(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json', 'registry/tests/fixtures/corpus.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data_pending = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer_pending = RegisteredLTServiceSerializer(data=cls.ltd_data_pending)
        if cls.serializer_pending.is_valid():
            cls.instance_pending = cls.serializer_pending.save()
        else:
            LOGGER.info(cls.serializer_pending.errors)
        LOGGER.info('Setup has finished')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    @skip('admin model deprecated')
    def test_technical_validator_form(self):
        form_data = {
            'review_comment': 'Review comment here',
            'validator_note': 'Validator note here',
            'slack_channel': 'http://www.slackchannelurl.com',
            'jira_ticket': 'http://www.jiraticketurl.com',
            'technically_valid': None
        }
        form = TechnicalValidatorForm(data=form_data, instance=self.metadata_records[0].management_object)
        self.assertTrue(form.is_valid())

    @skip('admin model deprecated')
    def test_technical_validator_form_fails_when_technically_valid_but_no_review_comment_lr_not_completed(self):
        form_data = {
            'review_comment': None,
            'validator_note': 'Validator note here',
            'slack_channel': 'http://www.slackchannelurl.com',
            'jira_ticket': 'http://www.jiraticketurl.com',
            'technically_valid': True
        }
        form = TechnicalValidatorForm(data=form_data, instance=self.metadata_records[1].management_object)
        self.assertFalse(form.is_valid())

    @skip('admin model deprecated')
    def test_technical_validator_form_passes_when_technically_valid_but_no_review_comment_lr_completed(self):
        form_data = {
            'review_comment': None,
            'validator_note': 'Validator note here',
            'slack_channel': 'http://www.slackchannelurl.com',
            'jira_ticket': 'http://www.jiraticketurl.com',
            'technically_valid': True
        }
        json_file = open(self.test_data[1], 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['resource_name'] = {"en": "test passes"}
        json_data['described_entity']['resource_short_name'] = {}
        json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=json_data)
        md_serializer.is_valid()
        md_instance = md_serializer.save()
        ltd_data_completed = {
            'metadata_record': md_instance.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143asd120x'
        }
        serializer_completed = RegisteredLTServiceSerializer(data=ltd_data_completed)
        if serializer_completed.is_valid():
            instance_completed = serializer_completed.save()
        else:
            LOGGER.info(serializer_completed.errors)
        LOGGER.info('Setup has finished')
        ltd_data_completed_updated = copy.deepcopy(ltd_data_completed)
        ltd_data_completed_updated['elg_execution_location'] = 'http://www.executionlocation.com/differentlink'
        ltd_data_completed_updated['status'] = 'COMPLETED'
        ltd_data_completed_updated['elg_hosted'] = True
        serializer_updated = RegisteredLTServiceSerializer(instance_completed, data=ltd_data_completed_updated)
        serializer_updated.is_valid()
        instance_updated = serializer_updated.save()
        form = TechnicalValidatorForm(data=form_data, instance=md_instance.management_object)
        self.assertTrue(form.is_valid())


class TestValidationAdmin(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json', 'registry/tests/fixtures/corpus.json',
                 'registry/tests/fixtures/ld_model.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    @skip('admin model deprecated')
    def test_get_form_legal(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        roles.assign_role(self.admin, 'legal_validator')
        Validator.objects.create(user=self.admin, assigned_for='Legal')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        instance = self.metadata_records[0].management_object

    @skip('admin model deprecated')
    def test_get_form_metadata(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        roles.assign_role(self.admin, 'metadata_validator')
        Validator.objects.create(user=self.admin, assigned_for='Metadata')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))

    @skip('admin model deprecated')
    def test_get_form_technical(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        roles.assign_role(self.admin, 'technical_validator')
        Validator.objects.create(user=self.admin, assigned_for='Technical')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))

    @skip('admin model deprecated')
    def test_get_legal_queryset(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        roles.assign_role(self.admin, 'legal_validator')
        Validator.objects.create(user=self.admin, assigned_for='Legal')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        ContentFile.objects.get_or_create(file=f'{self.metadata_records[1].management_object.identifier}/test_filename.zip',
                                       record_manager=self.metadata_records[1].management_object)
        dist = SoftwareDistribution.objects.get(id=self.metadata_records[1].described_entity.lr_subclass.software_distribution.first().id)
        dist.package = ContentFile.objects.get(file=f'{self.metadata_records[1].management_object.identifier}/test_filename.zip')
        dist.save()
        self.metadata_records[1].management_object.ingest()
        self.assertTrue(val_admin.get_queryset(request, vld_type='legal_validator'))

    @skip('admin model deprecated')
    def test_get_metadata_queryset(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        roles.assign_role(self.admin, 'metadata_validator')
        roles.assign_role(self.admin, 'technical_validator')
        Validator.objects.create(user=self.admin, assigned_for='Metadata')
        Validator.objects.create(user=self.admin, assigned_for='Technical')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.metadata_records[0].management_object.ingest()
        self.assertTrue(val_admin.get_queryset(request, vld_type='metadata_validator'))

    @skip('admin model deprecated')
    def test_get_technical_queryset(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        roles.assign_role(self.admin, 'technical_validator')
        Validator.objects.create(user=self.admin, assigned_for='Technical')
        self.metadata_records[2].management_object.functional_service = True
        self.metadata_records[2].management_object.save()
        self.metadata_records[2].management_object.ingest()
        self.metadata_records[1].management_object.technical_validator = self.admin
        self.metadata_records[1].management_object.save()
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.assertTrue(val_admin.get_queryset(request, vld_type='technical_validator'))

    @skip('admin model deprecated')
    def test_get_resource_type_lr(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        instance = self.metadata_records[1]
        self.assertTrue(instance.described_entity.lr_subclass.lr_type
                        == val_admin._get_resource_type(instance.management_object))

    @skip('admin model deprecated')
    def test_get_resource_type_non_lr(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        instance = self.metadata_records[0]
        self.assertTrue(
            instance.described_entity.entity_type == val_admin._get_resource_type(instance.management_object))

    @skip('admin model deprecated')
    def test_has_change_permission(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        roles.assign_role(self.admin, 'technical_validator')
        Validator.objects.create(user=self.admin, assigned_for='Technical')
        instance = self.metadata_records[1]
        instance.management_object.ingest()
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.assertTrue(val_admin.has_change_permission(request, instance.management_object))

    @skip('admin model deprecated')
    def test_has_change_permission_false_when_internal(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        roles.assign_role(self.admin, 'technical_validator')
        Validator.objects.create(user=self.admin, assigned_for='Technical')
        instance = self.metadata_records[1]
        instance.management_object.status = 'i'
        instance.management_object.save()
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.assertFalse(val_admin.has_change_permission(request, instance.management_object))

    @skip('admin model deprecated')
    def test_has_change_permission_false_when_published(self):
        val_admin = ValidationAdmin(model=Manager, admin_site=AdminSite())
        roles.assign_role(self.admin, 'technical_validator')
        Validator.objects.create(user=self.admin, assigned_for='Technical')
        instance = self.metadata_records[1]
        instance.management_object.ingest()
        instance.management_object.publish()
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.assertFalse(val_admin.has_change_permission(request, instance.management_object))


class TestLegalValidationAdmin(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    @skip('admin model deprecated')
    def test_get_form(self):
        legal_val_admin = LegalValidationAdmin(model=LegalValidation, admin_site=AdminSite())
        roles.assign_role(self.admin, 'legal_validator')
        Validator.objects.create(user=self.admin, assigned_for='Legal')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))

    @skip('admin model deprecated')
    def test_get_queryset_when_user_assigned(self):
        legal_val_admin = LegalValidationAdmin(model=LegalValidation, admin_site=AdminSite())
        roles.assign_role(self.admin, 'legal_validator')
        Validator.objects.create(user=self.admin, assigned_for='Legal')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        ContentFile.objects.get_or_create(file=f'{self.metadata_records[1].management_object.identifier}/test_filename.zip',
                                       record_manager=self.metadata_records[1].management_object)
        dist = SoftwareDistribution.objects.get(id=self.metadata_records[1].described_entity.lr_subclass.software_distribution.first().id)
        dist.package = ContentFile.objects.get(file=f'{self.metadata_records[1].management_object.identifier}/test_filename.zip')
        dist.save()
        self.metadata_records[1].management_object.ingest()
        self.assertTrue(str(self.metadata_records[1]) in str(legal_val_admin.get_queryset(request)))

    @skip('admin model deprecated')
    def test_get_queryset_when_user_not_assigned(self):
        legal_val_admin = LegalValidationAdmin(model=LegalValidation, admin_site=AdminSite())
        roles.assign_role(self.admin, 'legal_validator')
        Validator.objects.create(user=self.admin, assigned_for='Legal')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.assertFalse(legal_val_admin.get_queryset(request))


class TestMetadataValidationAdmin(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    @skip('admin model deprecated')
    def test_get_form(self):
        metadata_val_admin = MetadataValidationAdmin(model=MetadataValidation, admin_site=AdminSite())
        roles.assign_role(self.admin, 'metadata_validator')
        Validator.objects.create(user=self.admin, assigned_for='Metadata')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))

    @skip('admin model deprecated')
    def test_get_queryset_when_user_assigned(self):
        metadata_val_admin = MetadataValidationAdmin(model=MetadataValidation, admin_site=AdminSite())
        roles.assign_role(self.admin, 'metadata_validator')
        Validator.objects.create(user=self.admin, assigned_for='Metadata')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.metadata_records[0].management_object.ingest()
        self.assertTrue(str(self.metadata_records[0]) in str(metadata_val_admin.get_queryset(request)))

    @skip('admin model deprecated')
    def test_get_queryset_when_user_not_assigned(self):
        metadata_val_admin = MetadataValidationAdmin(model=MetadataValidation, admin_site=AdminSite())
        roles.assign_role(self.admin, 'metadata_validator')
        Validator.objects.create(user=self.admin, assigned_for='Metadata')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.assertFalse(metadata_val_admin.get_queryset(request))


class TestTechnicalValidationAdmin(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()
    
    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    @skip('admin model deprecated')
    def test_get_form(self):
        technical_val_admin = TechnicalValidationAdmin(model=TechnicalValidation, admin_site=AdminSite())
        roles.assign_role(self.admin, 'technical_validator')
        Validator.objects.create(user=self.admin, assigned_for='Technical')
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.assertTrue(True)

    @skip('admin model deprecated')
    def test_get_queryset_when_user_assigned(self):
        roles.assign_role(self.admin, 'technical_validator')
        Validator.objects.create(user=self.admin, assigned_for='Technical')
        self.metadata_records[1].management_object.functional_service = True
        self.metadata_records[1].management_object.save()
        self.metadata_records[1].management_object.ingest()
        self.metadata_records[1].management_object.technical_validator = self.admin
        self.metadata_records[1].management_object.save()
        technical_val_admin = TechnicalValidationAdmin(model=TechnicalValidation, admin_site=AdminSite())
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.assertTrue(str(self.metadata_records[1]) in str(technical_val_admin.get_queryset(request)))

    @skip('admin model deprecated')
    def test_get_queryset_when_user_not_assigned(self):
        roles.assign_role(self.admin, 'technical_validator')
        Validator.objects.create(user=self.admin, assigned_for='Technical')
        self.metadata_records[0].management_object.functional_service = True
        self.metadata_records[0].management_object.save()
        technical_val_admin = TechnicalValidationAdmin(model=TechnicalValidation, admin_site=AdminSite())
        request = MockRequest(user=self.admin, auth=self.authenticate(self.admin_token))
        self.assertFalse(technical_val_admin.get_queryset(request))
