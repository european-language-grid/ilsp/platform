from datacite import DataCiteMDSClient, DataCiteRESTClient
from storages.backends.s3boto3 import S3Boto3Storage

from django.conf import settings


class DataStorage(S3Boto3Storage):
    def path(self, name):
        pass

    def get_accessed_time(self, name):
        pass

    def get_created_time(self, name):
        pass


ELG_STORAGE = DataStorage(signature_version='s3v4')

# Initialize the REST client.
ELGDataCiteRESTClient = DataCiteRESTClient(
    username=settings.DATACITE_REPOSITORY_ID,
    password=settings.DATACITE_PASSWORD,
    prefix=settings.DATACITE_PREFIX,
    test_mode=settings.DATACITE_TEST_MODE
)

# Initialize the MDS client
ELGDataCiteMDSClient = DataCiteMDSClient(
    username=settings.DATACITE_REPOSITORY_ID,
    password=settings.DATACITE_PASSWORD,
    prefix=settings.DATACITE_PREFIX,
    test_mode=settings.DATACITE_TEST_MODE
)
