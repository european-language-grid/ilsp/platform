from rest_framework import routers
from .management_dashboard_index import urls as dashboard_urls
from django.urls import include, path

from management import views, lookup_views

router = routers.DefaultRouter()

app_name = 'management'

urlpatterns = (
 path('', include(router.urls)),
 path('publish/', views.PublishView.as_view()),
 path('ingest/', views.IngestView.as_view()),
 path('legal-validator/', views.LegalValidatorListView.as_view()),
 path('legal-validator/update/', views.LegalValidatorUpdateView.as_view()),
 path('dashboard_legal_validator/', views.LegalValidatorSelfView.as_view()),
 path('metadata-validator/', views.MetadataValidatorListView.as_view()),
 path('metadata-validator/update/', views.MetadataValidatorUpdateView.as_view()),
 path('dashboard_metadata_validator/', views.MetadataValidatorSelfView.as_view()),
 path('technical-validator/', views.TechnicalValidatorListView.as_view()),
 path('technical-validator/update/', views.TechnicalValidatorUpdateView.as_view()),
 path('dashboard_technical_validator/', views.TechnicalValidatorSelfView.as_view()),
 path('curator/', views.CuratorListView.as_view()),
 path('curator/update/', views.CuratorUpdateView.as_view()),
 path('curator/lookup/', lookup_views.CuratorLookupView.as_view()),
 path('dashboard_curator/', views.CuratorSelfView.as_view()),
 path('record-info/<int:pk>/', views.RecordInfoView.as_view(), name='record-info'),
 path('legally-valid/<int:pk>/', views.LegallyApproveView.as_view(), name='legally-valid'),
 path('technically-valid/<int:pk>/', views.TechnicallyApproveView.as_view(), name='technically-valid'),
 path('batch-technical-validation/', views.BatchTechnicalApprovalView.as_view(), name='batch-technical-validation '),
 path('metadata-valid/<int:pk>/', views.MetadataApproveView.as_view(), name='metadata-valid'),
 path('unpublish/', views.UnpublishView.as_view()),
 path('delete/', views.MarkAsDeletedView.as_view()),
 path('restore/', views.RestoreView.as_view()),
 path('unpublication-request/', views.UnpublicationRequestView.as_view()),
 path('claim/<int:pk>/', views.ClaimRecordView.as_view()),
 path('create_version/<int:pk>/', views.CreateVersionView.as_view()),
 path('deactivate_version/', views.DeactivateVersionView.as_view()),
 path('copy_resource/<int:pk>/', views.CopyResourceView.as_view()),
 path('download/<int:pk>/', views.download, name='download'),
 # path('upload/<int:pk>/', views.upload),
 path('content-files/<int:metadata_record_of_manager__pk>/', views.ContentFilesView.as_view(), name='datasets'),
 path('upload/<int:metadata_record_of_manager__pk>/',
      views.StorageAuthorizationView.as_view(), name='storage-authorization'),
 path('dashboard/', include(dashboard_urls)),
 path('data-report/', views.catalog_data_report, name='data-report'),
 path('tool-report/', views.catalog_tool_report, name='tool-report')
)
