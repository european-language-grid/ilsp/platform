import csv
import datetime
import io
import logging
import os
from urllib.parse import unquote

from botocore.exceptions import ClientError
from dateutil import parser as date_parser
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import redirect
from django_filters.rest_framework import DjangoFilterBackend
from elasticsearch import Elasticsearch, helpers
from elasticsearch_dsl import Search, Q as esQ
from packaging import version as _ver
from rest_framework import (
    generics, status
)
from rest_framework import serializers as r_s
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rolepermissions.checkers import has_role

from accounts import elg_permissions
from accounts.elg_permissions import (
    response_forbidden
)
from analytics.consumer_index.documents import MyDownloadsDocument
from analytics.models import UserDownloadStats, MetadataRecordStats
from catalogue_backend.settings.settings import ES_URL
from dle.dle_report_index.documents import DLEReportDocument
from dle.service import DLEService
from email_notifications.tasks import (
    ingestion_notifications, manual_validation_assignment_notifications,
    curator_content_manager_notifications, version_inactive_notification
)
from processing.models import RegisteredLTService
from processing.serializers import RegisteredLTServiceSerializer
from registry.choices import LICENCE_CATEGORY_CHOICES, LR_IDENTIFIER_SCHEME_CHOICES
from registry.models import MetadataRecord, DatasetDistribution, \
    SoftwareDistribution
from registry.search_indexes.documents import MetadataDocument
from registry.validators import validate_metadata_record_with_data
from registry.view_tasks import compute_dle_score
from utils.deletion_utils import find_related_records
from utils.sorting_utils import sort_list_of_tuples
from . import ELG_STORAGE
from . import filters
from . import serializers
from .catalog_report_index.documents import CatalogReportDocument
from .management_dashboard_index.documents import MyResourcesDocument, MyValidationsDocument
from .models import (
    Manager, INGESTED, INTERNAL, DRAFT, PUBLISHED,
    UNPUBLISHED, Validator, ContentFile, _create_uuid
)
from .permissions import DownloadDatasetPermission, TechnicalValidationPermission, UnpublishViewPermissions, \
    BatchTechnicalValidationPermission
from .serializers import ManagerSerializer
from .utils.elg_service_integration import elg_service_publication, elg_service_ingestion_validation

LOGGER = logging.getLogger(__name__)

s3client = ELG_STORAGE.connection.meta.client


class IngestView(generics.UpdateAPIView):
    """
    Ingestion view for records, supports single and bulk ingestion
    :query_params: record_id__in= mdr.id
    """
    lookup_field = 'metadata_record_of_manager__id'
    permission_classes = [elg_permissions.IsSuperUserOrProviderOrContentManager]
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = ManagerSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                deleted=False,
                status=INTERNAL
            )

        return Manager.objects.filter(
            Q(deleted=False),
            Q(status=INTERNAL),
            Q(curator=user)
        ).distinct()

    def update(self, request, *args, **kwargs):
        requested_ids = request.query_params.get('record_id__in', None)
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set record_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )

        instances = self.filter_queryset(self.get_queryset())
        updated_ids = []
        not_valid_ids = dict()
        to_ingest = list()

        content = list()
        for instance in instances:
            id_ = instance.metadata_record_of_manager.pk
            is_valid, error_msg = validate_metadata_record_with_data(instance.metadata_record_of_manager)
            valid_ingestion = True
            if not is_valid:
                not_valid_ids[id_] = error_msg
                valid_ingestion = False
            previous_status = instance.status
            response_status = ''
            if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource' \
                    and instance.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService' \
                    and instance.functional_service \
                    and not instance.under_construction:
                is_valid, error_msg = elg_service_ingestion_validation(instance.metadata_record_of_manager)
                if not is_valid:
                    not_valid_ids[id_] = error_msg
                    valid_ingestion = False
                else:
                    response_status = self.register_lt_service(instance.metadata_record_of_manager)
            if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource' \
                    and instance.metadata_record_of_manager.described_entity.lr_identifier.filter(
                        lr_identifier_scheme=LR_IDENTIFIER_SCHEME_CHOICES.DOI).exists() \
                    and (instance.under_construction
                         or not (instance.functional_service or instance.has_content_file)):
                not_valid_ids[id_] = 'Records with DOI must be submitted ' \
                                     'as an ELG compatible service or a record with hosted data.'
                valid_ingestion = False
            if valid_ingestion:
                updated_ids.append(id_)
                instance.ingest()
                to_ingest.append(instance)
            return_dict = {
                'pk': id_,
                'updated': (True if id_ in updated_ids else False),
                'pre': previous_status,
                'post': instance.status,
                'record': f"{settings.DJANGO_URL}"
                          f"{reverse('registry:metadatarecord-detail', kwargs={'pk': id_})}"
            }
            if id_ in not_valid_ids.keys():
                return_dict['error'] = not_valid_ids.get(id_, '')
            if response_status == 'LT Service Registered':
                return_dict[
                    'registered_lt_service_link'] = f"{settings.DJANGO_URL}{reverse('processing:lt_service', kwargs={'metadata_record__pk': instance.metadata_record_of_manager.pk})}"
            elif response_status:
                return_dict[
                    'registered_lt_service_link'] = f"LT service registration failed. {response_status}"
            content.append(return_dict)

        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        MyResourcesDocument().update(records)
        MyValidationsDocument().update(records)
        Manager.objects.bulk_update(
            to_ingest,
            [
                'status', 'ingestion_date',
                'legal_validator', 'metadata_validator', 'technical_validator'
            ]
        )
        ingestion_notifications.apply_async(args=[updated_ids, not_valid_ids])
        if len(content) > 0:
            return Response(
                content,
                status=status.HTTP_200_OK
            )
        else:
            failed_dict = {'message': []}
            for id_ in requested_ids_list:
                failed_dict['message'].append(f'Record (id: {id_}) cannot be ingested')
                if not_valid_ids.get(id_, ''):
                    failed_dict['error'] = not_valid_ids[id_]

            return Response(failed_dict,
                            status=status.HTTP_406_NOT_ACCEPTABLE)

    def register_lt_service(self, instance):
        error_text = ''
        try:
            if instance.described_entity.lr_subclass.lr_type == 'ToolService':
                elg_compatible_distribution = False
                for software_distribution in instance.described_entity.lr_subclass.software_distribution.all():
                    if software_distribution.elg_compatible_distribution:
                        exec_loc = software_distribution.execution_location
                        docker_download_loc = software_distribution.docker_download_location
                        service_adapter_loc = software_distribution.service_adapter_download_location
                        elg_compatible_distribution = True
                        try:
                            lt_service_ser = RegisteredLTServiceSerializer(
                                data=dict(metadata_record=instance.pk)
                            )
                            lt_service_ser.is_valid(raise_exception=True)
                            lt_service = lt_service_ser.save()
                            published_versions = instance.management_object.versioned_record_managers.filter(
                                status=PUBLISHED,
                                metadata_record_of_manager__lt_service__isnull=False
                            ).order_by('-metadata_record_of_manager__described_entity__languageresource__version')
                            if published_versions.exists():
                                latest_published_version = RegisteredLTService.objects.get(
                                    metadata_record=published_versions.first().metadata_record_of_manager.pk
                                )
                                lt_service.tool_type = latest_published_version.tool_type
                                lt_service.accessor_id = latest_published_version.accessor_id
                                lt_service.elg_gui_url = latest_published_version.elg_gui_url
                                lt_service.save()
                            else:
                                versions = instance.management_object.versioned_record_managers.filter(
                                    metadata_record_of_manager__lt_service__isnull=False
                                ).order_by(
                                    '-metadata_record_of_manager__described_entity__languageresource__version'
                                )
                                if versions.exists():
                                    latest_version = RegisteredLTService.objects.get(
                                        metadata_record=versions.first().metadata_record_of_manager.pk
                                    )
                                    lt_service.tool_type = latest_version.tool_type
                                    lt_service.accessor_id = latest_version.accessor_id
                                    lt_service.elg_gui_url = latest_version.elg_gui_url
                                    lt_service.save()
                            break
                        except r_s.ValidationError as e:
                            if e.args[0].get('metadata_record') \
                                    and e.args[0]['metadata_record'][0] == 'This field must be unique.':
                                lt_service = RegisteredLTService.objects.get(metadata_record=instance)
                                update_service = False
                                if exec_loc != lt_service.initial_elg_execution_location:
                                    lt_service.elg_execution_location = exec_loc
                                    lt_service.initial_elg_execution_location = exec_loc
                                    update_service = True
                                if docker_download_loc != lt_service.docker_download_location:
                                    update_service = True
                                if service_adapter_loc != lt_service.service_adapter_download_location:
                                    update_service = True
                                if update_service:
                                    lt_service.status = 'PENDING'
                                    lt_service.save()
                    else:
                        elg_compatible_distribution = False
                if not elg_compatible_distribution:
                    error_text = f'{instance} does not have a distribution tagged as elg compatible.'
            else:
                error_text = f'{instance} has been bypassed because it is not a Tool/Service'
        except AttributeError as e:
            if "object has no attribute" in e.args[0]:
                error_text = f'{instance} has been bypassed because it is not a Tool/Service'
        if error_text:
            return error_text
        return 'LT Service Registered'


class LegalValidatorListView(generics.ListAPIView):
    """
    List view of all legal validators
    """
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.LegalValidatorSerializer

    def get_queryset(self):
        return Manager.objects.filter(
            status__in=INGESTED,
            deleted=False
        )


class LegalValidatorUpdateView(generics.UpdateAPIView):
    """
    Update view for legal validator assignment to records
    """
    renderer_classes = [JSONRenderer]
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.ManagerSerializer

    def get_queryset(self):
        return Manager.objects.filter(
            status__in=INGESTED,
            deleted=False
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        requested_ids = request.query_params.get('record_id__in', None)
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set manager_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )
        instances = self.filter_queryset(self.get_queryset())
        updated_ids = []
        previously_assigned_validators_list = []
        for instance in instances:
            if request.data.get('legal_validator'):
                updated_ids.append(instance.metadata_record_of_manager.pk)
                if instance.legal_validator:
                    try:
                        previously_assigned_validator = Validator.objects.get(user=instance.legal_validator,
                                                                              assigned_for='Legal')
                        previously_assigned_validator.assigned_to.remove(instance.id)
                        previously_assigned_validator.save()
                        previously_assigned_validators_list.append(instance.legal_validator.id)
                    except ObjectDoesNotExist:
                        pass
            serializer = self.get_serializer(
                instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            assignee_validator = Validator.objects.get(user=serializer.instance.legal_validator, assigned_for='Legal')
            assignee_validator.assigned_to.add(instance.id)
            assignee_validator.save()
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        content = list()
        for id_ in requested_ids_list:
            content.append({
                'pk': id_,
                'updated': (True if id_ in updated_ids else False)
            })
        manual_validation_assignment_notifications.apply_async(args=[updated_ids,
                                                                     'legal_validator',
                                                                     request.user.username,
                                                                     previously_assigned_validators_list])
        return Response(content)


class LegalValidatorSelfView(generics.ListAPIView):
    """
    List view of assigned records for legal validators
    """
    renderer_classes = [JSONRenderer]
    permission_classes = [elg_permissions.IsSuperUserOrLegalValidatorOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.ManagerSerializer

    def get_queryset(self):
        user = self.request.user
        return Manager.objects.filter(
            status__in=INGESTED,
            legal_validator=user,
            deleted=False
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return_list = list()
        for n, instance in enumerate(serializer.data):
            instance[
                'record'] = f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': queryset[n].metadata_record_of_manager.pk})}"
            return_list.append(instance)
        return Response(return_list, status.HTTP_200_OK)


class MetadataValidatorListView(generics.ListAPIView):
    """
    List view of all metadata validators
    """
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.MetadataValidatorSerializer

    def get_queryset(self):
        return Manager.objects.filter(
            status__in=INGESTED,
            deleted=False
        )


class MetadataValidatorUpdateView(generics.UpdateAPIView):
    """
    Update view to assign metadata validator to records
    """
    renderer_classes = [JSONRenderer]
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.ManagerSerializer

    def get_queryset(self):
        return Manager.objects.filter(
            status__in=INGESTED,
            deleted=False
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        requested_ids = request.query_params.get('record_id__in', None)
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set manager_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )
        instances = self.filter_queryset(self.get_queryset())
        updated_ids = []
        previously_assigned_validators_list = []
        errors = {}
        for instance in instances:
            if request.data.get('metadata_validator'):
                if instance.functional_service:
                    errors.update({
                        instance.metadata_record_of_manager.id: f'A metadata validator could not be'
                                                                f' assigned to the record, since'
                                                                f' it is a functional service.'
                                                                f' Please assign'
                                                                f' a technical validator to perform the'
                                                                f' metadata and technical validation.'})
                    continue
                updated_ids.append(instance.metadata_record_of_manager.pk)
                if instance.metadata_validator:
                    try:
                        previously_assigned_validator = Validator.objects.get(user=instance.metadata_validator,
                                                                              assigned_for='Metadata')
                        previously_assigned_validator.assigned_to.remove(instance.id)
                        previously_assigned_validator.save()
                        previously_assigned_validators_list.append(instance.metadata_validator.id)
                    except ObjectDoesNotExist:
                        pass
            serializer = self.get_serializer(
                instance, data=request.data, partial=partial)
            if instance.functional_service:
                serializer.initial_data['technical_validator'] = request.data.get('metadata_validator')
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            assignee_validator = Validator.objects.get(user=serializer.instance.metadata_validator,
                                                       assigned_for='Metadata')
            assignee_validator.assigned_to.add(instance.id)
            assignee_validator.save()

        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        content = list()
        for id_ in requested_ids_list:
            id_content = {
                'pk': id_,
                'updated': (True if id_ in updated_ids else False)
            }
            if id_ in errors.keys():
                id_content['error'] = errors[id_]
            content.append(id_content)

        manual_validation_assignment_notifications.apply_async(args=[updated_ids,
                                                                     'metadata_validator',
                                                                     request.user.username,
                                                                     previously_assigned_validators_list])
        return Response(content)


class MetadataValidatorSelfView(generics.ListAPIView):
    """
    List view of records assigned to metadata validators
    """
    renderer_classes = [JSONRenderer]
    permission_classes = [elg_permissions.IsSuperUserOrMetadataValidatorOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.ManagerSerializer

    def get_queryset(self):
        user = self.request.user
        return Manager.objects.filter(
            status__in=INGESTED,
            metadata_validator=user,
            deleted=False
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return_list = list()
        for n, instance in enumerate(serializer.data):
            instance[
                'record'] = f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': queryset[n].metadata_record_of_manager.pk})}"
            return_list.append(instance)
        return Response(return_list, status.HTTP_200_OK)


class TechnicalValidatorListView(generics.ListAPIView):
    """
    List view of technical validators
    """
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.TechnicalValidatorSerializer

    def get_queryset(self):
        return Manager.objects.filter(
            status__in=INGESTED,
            deleted=False
        )


class TechnicalValidatorUpdateView(generics.UpdateAPIView):
    """
    Update view to assign technical validator to records
    """
    renderer_classes = [JSONRenderer]
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.ManagerSerializer

    def get_queryset(self):
        return Manager.objects.filter(
            status__in=INGESTED,
            deleted=False
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        requested_ids = request.query_params.get('record_id__in', None)
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set manager_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )
        instances = self.filter_queryset(self.get_queryset())
        updated_ids = []
        previously_assigned_validators_list = []
        errors = {}
        for instance in instances:
            if request.data.get('technical_validator'):
                if instance.has_content_file:
                    errors.update({
                        instance.metadata_record_of_manager.id: f'A technical validator could not be'
                                                                f' assigned to the record, since it has'
                                                                f' data attached. Please assign'
                                                                f' a metadata validator to perform the'
                                                                f' metadata and technical validation.'})
                    continue
                updated_ids.append(instance.metadata_record_of_manager.pk)
                if instance.technical_validator:
                    try:
                        previously_assigned_validator = Validator.objects.get(user=instance.technical_validator,
                                                                              assigned_for='Technical')
                        previously_assigned_validator.assigned_to.remove(instance.id)
                        previously_assigned_validator.save()
                        previously_assigned_validators_list.append(instance.technical_validator.id)
                    except ObjectDoesNotExist:
                        pass
            serializer = self.get_serializer(
                instance, data=request.data, partial=partial)
            if instance.functional_service:
                serializer.initial_data['metadata_validator'] = request.data.get('technical_validator')
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            assignee_validator = Validator.objects.get(user=serializer.instance.technical_validator,
                                                       assigned_for='Technical')
            assignee_validator.assigned_to.add(instance.id)
            assignee_validator.save()

        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        content = list()

        for id_ in requested_ids_list:
            id_content = {
                'pk': id_,
                'updated': (True if id_ in updated_ids else False)
            }
            if id_ in errors.keys():
                id_content['error'] = errors[id_]
            content.append(id_content)

        manual_validation_assignment_notifications.apply_async(args=[updated_ids,
                                                                     'technical_validator',
                                                                     request.user.username,
                                                                     previously_assigned_validators_list])
        return Response(content)


class TechnicalValidatorSelfView(generics.ListAPIView):
    """
    List view of records assign to technical validator
    """
    renderer_classes = [JSONRenderer]
    permission_classes = [elg_permissions.IsSuperUserOrTechnicalValidatorOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.ManagerSerializer

    def get_queryset(self):
        user = self.request.user
        return Manager.objects.filter(
            status__in=INGESTED,
            technical_validator=user,
            deleted=False
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return_list = list()
        for n, instance in enumerate(serializer.data):
            instance[
                'record'] = f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': queryset[n].metadata_record_of_manager.pk})}"
            return_list.append(instance)
        return Response(return_list, status.HTTP_200_OK)


class LegallyApproveView(generics.UpdateAPIView):
    """
    Update view to legally approve a record
    :query_params: approval= True | False
    """
    queryset = Manager.objects.all()
    permission_classes = [elg_permissions.IsSuperUserOrLegalValidatorOrContentManager]
    serializer_class = serializers.LegalValidationFormSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                status__in=INGESTED,
                deleted=False
            )
        return Manager.objects.filter(
            status__in=INGESTED,
            legal_validator=user,
            deleted=False
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        pk = kwargs.pop('pk', False)

        queryset = self.get_queryset()
        try:
            instance = queryset.get(metadata_record_of_manager__pk=pk)
        except Manager.DoesNotExist:
            return Response(
                f'Record (id: {pk}) cannot be legally approved',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        approval = serializer.data['legally_approve']

        content = list()

        previous_legal_status = instance.legally_valid
        updated_items = []
        if approval \
                and instance.metadata_valid \
                and instance.technically_valid:
            if instance.functional_service:
                elg_service_publication(instance.metadata_record_of_manager)
            instance.handle_version_publication(updated_items)
        instance.approve(updated_fields=['legally_valid'], legal_approval=approval)
        post_legal_status = instance.legally_valid
        updated_items.append(instance.metadata_record_of_manager)
        content.append({
            'pk': instance.metadata_record_of_manager.pk,
            'updated': True if previous_legal_status != post_legal_status else False,
            'record': f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.metadata_record_of_manager.pk})}"
        })

        record = Manager.objects.get(metadata_record_of_manager__pk=pk)
        if record.status == INTERNAL:
            MyResourcesDocument().update(record)
        MyValidationsDocument().update(record)
        if record.status == PUBLISHED:
            MyResourcesDocument().update(record)
            MetadataDocument().catalogue_update(list(set(updated_items)))
            manager_list = [md.management_object for md in list(set(updated_items))]
            CatalogReportDocument().catalogue_report_update(manager_list)
            updated_ids = [item.id for item in updated_items]
            compute_dle_score.apply_async(args=[list(set(updated_ids))])

        return Response(
            content,
            status=status.HTTP_200_OK
        )


class TechnicallyApproveView(generics.RetrieveAPIView, generics.UpdateAPIView):
    """
    Update view to technically approve a record
    :query_params: approved: True | False if not an lr
    :request data: registered lt service approval/rejection if an lr
    """
    queryset = MetadataRecord.objects.all()
    permission_classes = [TechnicalValidationPermission]
    serializer_class = serializers.TechnicalValidationFormSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                status__in=INGESTED,
                deleted=False
            )
        return Manager.objects.filter(
            status__in=INGESTED,
            technical_validator=user,
            deleted=False
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        pk = kwargs.pop('pk', False)

        queryset = self.get_queryset()
        try:
            instance = queryset.get(metadata_record_of_manager__pk=pk)
        except Manager.DoesNotExist:
            return Response(
                f'Record (id: {pk}) cannot be technically approved',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        technical_approval = serializer.data['technically_approve']
        metadata_approval = serializer.data['metadata_approve']
        content = list()
        previous_technical_status = instance.technically_valid
        previous_metadata_status = instance.metadata_valid
        updated_items = []
        if instance.legally_valid \
                and (previous_metadata_status or metadata_approval) \
                and (previous_technical_status or technical_approval):
            if instance.functional_service:
                elg_service_publication(instance.metadata_record_of_manager)
            instance.handle_version_publication(updated_items)
        instance.approve(updated_fields=['technically_valid',
                                         'metadata_valid'],
                         technical_approval=technical_approval,
                         metadata_approval=metadata_approval)
        updated_items.append(instance.metadata_record_of_manager)
        record = Manager.objects.get(metadata_record_of_manager__pk=pk)
        post_technical_status = record.technically_valid
        post_metadata_status = record.metadata_valid
        updated_items.append(instance.metadata_record_of_manager)
        content.append({
            'pk': instance.metadata_record_of_manager.pk,
            'updated': True if previous_technical_status != post_technical_status or previous_metadata_status != post_metadata_status else False,
            'record': f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.metadata_record_of_manager.pk})}"
        })
        if record.status == INTERNAL:
            MyResourcesDocument().update(record)
        MyValidationsDocument().update(record)
        if record.status == PUBLISHED:
            MyResourcesDocument().update(record)
            MetadataDocument().catalogue_update(list(set(updated_items)))
            manager_list = [md.management_object for md in list(set(updated_items))]
            CatalogReportDocument().catalogue_report_update(manager_list)
            updated_ids = [item.id for item in updated_items]
            compute_dle_score.apply_async(args=[list(set(updated_ids))])

        return Response(
            content,
            status=status.HTTP_200_OK
        )


class BatchTechnicalApprovalView(generics.RetrieveAPIView, generics.UpdateAPIView):
    """
    Update view to technically approve a batch of elg compatible services
    :query_params: approved: True | False if not an lr
    :request data: registered lt service approval/rejection if an lr
    """
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    permission_classes = [BatchTechnicalValidationPermission]
    serializer_class = serializers.TechnicalValidationFormSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                status__in=INGESTED,
                deleted=False,
                functional_service=True
            )
        return Manager.objects.filter(
            status__in=INGESTED,
            technical_validator=user,
            deleted=False,
            functional_service=True
        )

    def update(self, request, *args, **kwargs):
        requested_ids = request.query_params.get('record_id__in', None)
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set record_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )
        partial = kwargs.pop('partial', False)
        instances = self.filter_queryset(self.get_queryset())
        if len(instances) == 0:
            return Response(
                f'Only submitted ELG compatible services assigned to you can be validated in batches.',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        resource_index_records = []
        validation_index_records = []
        all_published_items_to_update = []
        content = list()
        failed_requested_ids = list(set(requested_ids_list) - set([i.metadata_record_of_manager.pk for i in instances]))
        if failed_requested_ids:
            content.append({
                'errors': {'pk list': failed_requested_ids,
                           'details': 'Only submitted ELG compatible services assigned to you can be validated in batches.'},
            })
        for instance in instances:
            _pk = instance.pk
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            technical_approval = serializer.data['technically_approve']
            metadata_approval = serializer.data['metadata_approve']
            previous_technical_status = instance.technically_valid
            previous_metadata_status = instance.metadata_valid
            updated_items = []
            if instance.legally_valid \
                    and (previous_metadata_status or metadata_approval) \
                    and (previous_technical_status or technical_approval):
                if instance.functional_service:
                    elg_service_publication(instance.metadata_record_of_manager)
                instance.handle_version_publication(updated_items)
            instance.approve(updated_fields=['technically_valid',
                                             'metadata_valid'],
                             technical_approval=technical_approval,
                             metadata_approval=metadata_approval)
            updated_items.append(instance.metadata_record_of_manager)
            record = Manager.objects.get(pk=_pk)
            post_technical_status = record.technically_valid
            post_metadata_status = record.metadata_valid
            updated_items.append(instance.metadata_record_of_manager)
            update_successful = True if previous_technical_status != post_technical_status or previous_metadata_status != post_metadata_status else False
            content.append({
                'pk': instance.metadata_record_of_manager.pk,
                'updated': update_successful,
                'record': f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.metadata_record_of_manager.pk})}"
            })
            if update_successful:
                if record.status in INTERNAL:
                    resource_index_records.append(record)
                if record.status == PUBLISHED:
                    resource_index_records.append(record)
                    all_published_items_to_update.extend(updated_items)
            validation_index_records.append(record)

        if resource_index_records:
            MyResourcesDocument().update(list(set(resource_index_records)))
        MyValidationsDocument().update(validation_index_records)
        if all_published_items_to_update:
            index_items = list(set(all_published_items_to_update))
            MetadataDocument().catalogue_update(index_items)
            manager_list = [md.management_object for md in list(set(index_items))]
            CatalogReportDocument().catalogue_report_update(manager_list)
            updated_ids = [item.id for item in index_items]
            compute_dle_score.apply_async(args=[list(set(updated_ids))])

        return Response(
            content,
            status=status.HTTP_200_OK
        )


class MetadataApproveView(generics.UpdateAPIView):
    """
    Update view to approve the validity of the metadata record
    :query_params: approval= True | False
    """
    queryset = MetadataRecord.objects.all()
    permission_classes = [elg_permissions.IsSuperUserOrMetadataValidatorOrContentManager]
    serializer_class = serializers.MetadataValidationFormSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                status__in=INGESTED,
                deleted=False
            )
        return Manager.objects.filter(
            status__in=INGESTED,
            metadata_validator=user,
            deleted=False
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        pk = kwargs.pop('pk', False)

        queryset = self.get_queryset()
        try:
            instance = queryset.get(metadata_record_of_manager__pk=pk)
        except Manager.DoesNotExist:
            return Response(
                f'Metadata of record (id: {pk}) cannot be approved',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        approval = serializer.data['metadata_approve']

        content = list()

        previous_metadata_status = instance.metadata_valid
        updated_items = []
        if instance.technically_valid \
                and instance.legally_valid \
                and approval:
            if instance.functional_service:
                elg_service_publication(instance.metadata_record_of_manager)
            instance.handle_version_publication(updated_items)
        instance.approve(updated_fields=['metadata_valid'], metadata_approval=approval)
        updated_items.append(instance.metadata_record_of_manager)
        post_metadata_status = instance.metadata_valid
        updated_items.append(instance.metadata_record_of_manager)
        content.append({
            'pk': instance.metadata_record_of_manager.pk,
            'updated': True if previous_metadata_status != post_metadata_status else False,
            'record': f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.metadata_record_of_manager.pk})}",
        })

        record = Manager.objects.get(metadata_record_of_manager__pk=pk)
        if record.status == INTERNAL:
            MyResourcesDocument().update(record)
        MyValidationsDocument().update(record)
        if record.status == PUBLISHED:
            MyResourcesDocument().update(record)
            MetadataDocument().catalogue_update(list(set(updated_items)))
            manager_list = [md.management_object for md in list(set(updated_items))]
            CatalogReportDocument().catalogue_report_update(manager_list)
            updated_ids = [item.id for item in updated_items]
            compute_dle_score.apply_async(args=[list(set(updated_ids))])

        return Response(
            content,
            status=status.HTTP_200_OK
        )


class PublishView(generics.UpdateAPIView):
    """
    Force publish view
    :query_params: record_id__in=mdr.id
    """
    lookup_field = 'metadata_record_of_manager__id'
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                deleted=False,
                status__in=[INGESTED, UNPUBLISHED]
            )

    def update(self, request, *args, **kwargs):
        requested_ids = request.query_params.get('record_id__in', None)
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set record_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )

        instances = self.filter_queryset(self.get_queryset())
        updated_ids = []
        updated_items = []
        content = list()
        for instance in instances:
            updated_ids.append(instance.metadata_record_of_manager.pk)
            if instance.functional_service:
                elg_service_publication(instance.metadata_record_of_manager)
            previous_status = instance.status
            instance.handle_version_publication(updated_items)
            instance.publish()
            content.append({
                'pk': instance.metadata_record_of_manager.pk,
                'updated': (True if instance.metadata_record_of_manager.pk in updated_ids else False),
                'pre': previous_status,
                'post': instance.status,
                'record': f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.metadata_record_of_manager.pk})}",
            })

        # Add records from indexer
        MetadataDocument().catalogue_update(list(set(updated_items)))
        updated_ids = [item.id for item in updated_items]
        compute_dle_score.apply_async(args=[list(set(updated_ids))])
        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        CatalogReportDocument().catalogue_report_update(records)
        MyResourcesDocument().update(records)
        MyValidationsDocument().update(records)

        if len(content) > 0:
            return Response(
                content,
                status=status.HTTP_200_OK
            )
        else:
            failed_dict = {'message': []}
            for id_ in requested_ids_list:
                failed_dict['message'].append(f'Record (id: {id_}) cannot be published')
            return Response(failed_dict,
                            status=status.HTTP_406_NOT_ACCEPTABLE)


class UnpublishView(generics.UpdateAPIView):
    """
    Unpublish record view
    :query_params: record_id__in=mdr.id
    """
    lookup_field = 'metadata_record_of_manager__id'
    permission_classes = [UnpublishViewPermissions]
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                deleted=False,
                status=PUBLISHED
            )

        return Manager.objects.filter(
            Q(deleted=False),
            Q(status=PUBLISHED),
            Q(curator=user),
            Q(under_construction=True)
        ).distinct()

    def update(self, request, *args, **kwargs):
        requested_ids = request.query_params.get('record_id__in', None)
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set record_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )

        unpublication_reason = request.data.get('reason')
        if not unpublication_reason:
            return Response(
                'A reason for the unpublication of the record is required',
                status=status.HTTP_400_BAD_REQUEST
            )

        instances = self.filter_queryset(self.get_queryset())
        updated_ids = []
        updated_items = []
        content = list()
        for instance in instances:
            updated_ids.append(instance.metadata_record_of_manager.pk)
            updated_items.append(instance.metadata_record_of_manager)
            previous_status = instance.status
            instance.handle_version_unpublication()
            instance.unpublish(unpublication_reason=unpublication_reason)
            content.append({
                'pk': instance.metadata_record_of_manager.pk,
                'updated': (True if instance.metadata_record_of_manager.pk in updated_ids else False),
                'pre': previous_status,
                'post': instance.status,
                'record': f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.metadata_record_of_manager.pk})}",
            })
        for _id in requested_ids_list:
            if _id not in updated_ids:
                content.append({'message': f'Record with ID: {_id} cannot be unpublished.'})
        # Add records from indexer
        MetadataDocument().catalogue_update(updated_items, action='delete', raise_on_error=False)
        DLEReportDocument().dle_report_update(list(set(updated_items)), action='delete', raise_on_error=False)
        try:
            DLEService.notify_delete(list(set(updated_ids)))
        except:
            pass
        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        CatalogReportDocument().catalogue_report_update(records, action='delete', raise_on_error=False)
        MyResourcesDocument().update(records)
        MyValidationsDocument().update(records)
        if len(content) > 0:
            return Response(
                content,
                status=status.HTTP_200_OK
            )
        else:
            failed_dict = {'message': []}
            for id_ in requested_ids_list:
                failed_dict['message'].append(f'Record (id: {id_}) cannot be unpublished')
            return Response(failed_dict,
                            status=status.HTTP_406_NOT_ACCEPTABLE)


class MarkAsDeletedView(generics.UpdateAPIView):
    """
    Mark record as deleted view
    :query_params: record_id__in=mdr.id
    """
    lookup_field = 'metadata_record_of_manager__id'
    permission_classes = [elg_permissions.IsSuperUserOrProviderOrContentManager]
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = ManagerSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(deleted=False).exclude(status=PUBLISHED)

        return Manager.objects.filter(
            (Q(curator=user) & Q(status__in=[DRAFT, INTERNAL]) & Q(deleted=False))
        ).distinct()

    def update(self, request, *args, **kwargs):
        requested_ids = request.query_params.get('record_id__in', None)
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set record_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )
        deletion_reason = request.data.get('reason')
        user = self.request.user
        if (user.is_superuser or has_role(user, 'content_manager')) and not deletion_reason:
            return Response(
                'A reason for the deletion is required',
                status=status.HTTP_400_BAD_REQUEST
            )
        instances = self.filter_queryset(self.get_queryset())
        require_reason = [True if (instance.ingestion_date and not instance.status == INTERNAL) else False
                          for instance in instances.all()]
        if any(require_reason) and not deletion_reason:
            return Response(
                'A reason for the deletion is required',
                status=status.HTTP_400_BAD_REQUEST
            )
        _, all_related_ids = find_related_records(instances)
        del_check = [True if _id in set(instances.all().values_list('pk', flat=True)) else False for _id in
                     all_related_ids]
        delete_all_related = False if False in del_check else True
        updated_ids = []
        content = list()
        for instance in instances.all().order_by('pk'):
            # TODO: assign pid to records
            # instance.metadata_record_of_manager.generate_pid()
            related_records, _ = find_related_records(instance)
            if not related_records \
                    or delete_all_related:
                updated_ids.append(instance.metadata_record_of_manager.pk)
                instance.mark_as_deleted(reason=deletion_reason)
            content_dict = {
                'pk': instance.metadata_record_of_manager.pk,
                'updated': (True if instance.metadata_record_of_manager.pk in updated_ids else False),
                'status': instance.status,
                'deleted': instance.deleted,
                'record': f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.metadata_record_of_manager.pk})}",
            }
            if related_records and not delete_all_related:
                content_dict.update({'error': f'Record {instance.metadata_record_of_manager.__str__()} cannot be '
                                              f'deleted since it\'s being referenced '
                                              f'by the following records {related_records}'})
            content.append(content_dict)
        # Add records from indexer
        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        MyResourcesDocument().update(records, action='delete', raise_on_error=False)
        MyValidationsDocument().update(records, action='delete', raise_on_error=False)
        curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                'mark_as_deleted',
                                                                deletion_reason])
        if len(content) > 0:
            return Response(
                content,
                status=status.HTTP_200_OK
            )
        else:
            failed_dict = {'message': []}
            for id_ in requested_ids_list:
                failed_dict['message'].append(f'Record (id: {id_}) cannot be deleted')
            return Response(failed_dict,
                            status=status.HTTP_406_NOT_ACCEPTABLE)


class RestoreView(generics.UpdateAPIView):
    """
    Restore a record that was marked as deleted view
    :query_params: record_id__in=mdr.id
    """
    lookup_field = 'metadata_record_of_manager__id'
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(deleted=True).exclude(status=PUBLISHED)

        return Manager.objects.filter(
            (Q(curator=user) & Q(status__in=[DRAFT, INTERNAL]))
        ).distinct()

    def update(self, request, *args, **kwargs):
        requested_ids = request.query_params.get('record_id__in', None)
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set record_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )

        instances = self.filter_queryset(self.get_queryset())
        updated_ids = []

        content = list()
        for instance in instances:
            updated_ids.append(instance.metadata_record_of_manager.pk)
            instance.undo_mark_deleted()
            content.append({
                'pk': instance.metadata_record_of_manager.pk,
                'updated': (True if instance.metadata_record_of_manager.pk in updated_ids else False),
                'status': instance.status,
                'deleted': instance.deleted,
                'record': f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.metadata_record_of_manager.pk})}",
            })

        # Add records from indexer
        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        MyResourcesDocument().update(records, action='index')
        curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                'restore'])
        if len(content) > 0:
            return Response(
                content,
                status=status.HTTP_200_OK
            )
        else:
            failed_dict = {'message': []}
            for id_ in requested_ids_list:
                failed_dict['message'].append(f'Record (id: {id_}) cannot be restored')
            return Response(failed_dict,
                            status=status.HTTP_406_NOT_ACCEPTABLE)


class DeactivateVersionView(generics.UpdateAPIView):
    """
    Deactivate version view
    """
    lookup_field = 'metadata_record_of_manager__id'
    permission_classes = [elg_permissions.IsSuperUserOrOwnerOrContentManager]
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = ManagerSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(status=PUBLISHED)

        return Manager.objects.filter(
            (Q(curator=user) & Q(status=PUBLISHED))
        ).distinct()

    def update(self, request, *args, **kwargs):
        requested_ids = request.query_params.get('record_id__in', None)
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set record_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )

        instances = self.filter_queryset(self.get_queryset())
        if len(instances) == 0:
            return Response(
                f'These services cannot be deactivated.',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        content_list = list()
        for instance in instances:
            previous_lt_service_status = ''
            previous_lt_service = RegisteredLTService.objects.filter(
                metadata_record__pk=instance.metadata_record_of_manager.pk)
            if previous_lt_service.exists():
                previous_lt_service_status = previous_lt_service.first().status
            instance.deactivate_older_version()
            lt_service_status = ''
            lt_service = RegisteredLTService.objects.filter(metadata_record__pk=instance.metadata_record_of_manager.pk)
            if lt_service.exists():
                lt_service_status = lt_service.first().status
            content = {
                'pk': instance.metadata_record_of_manager.pk,
                'updated': (
                    True if lt_service_status == 'DEPRECATED' and lt_service_status != previous_lt_service_status else False),
                'record': f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.metadata_record_of_manager.pk})}",
            }
            if not content.get('updated'):
                if instance.is_latest_version:
                    content.update({'latest_version': 'Cannot deactivate the latest version of a resource.'})
                if not instance.is_active_version:
                    content.update({'inactive_version': 'Resource is already inactive.'})
                if not getattr(instance.metadata_record_of_manager, 'lt_service', None):
                    content.update({'error': 'Resource has no service to be deactivated.'})
            else:
                record = Manager.objects.get(pk=instance.pk)
                MyResourcesDocument().update(record, action='index')
                MyValidationsDocument().update(record, action='index')
                records_version = record.metadata_record_of_manager.described_entity.version
                greater_versioned_record_managers = instance.versioned_record_managers.filter(
                    status=PUBLISHED,
                    deleted=False
                )
                greater_version_ids = []
                for version_record in greater_versioned_record_managers:
                    if _ver.parse(records_version) < _ver.parse(
                            version_record.metadata_record_of_manager.described_entity.version):
                        greater_version_ids.append(version_record.id)
                greater_versioned_records = MetadataRecord.objects.filter(
                    management_object__pk__in=greater_version_ids,
                    described_entity__languageresource__replaces=record.metadata_record_of_manager.described_entity.languageresource
                )
                initial_version_list = [
                    (i, _ver.parse(i.described_entity.version)) for i in greater_versioned_records
                ]
                sorted_version_list = sort_list_of_tuples(initial_version_list)
                latest_version = sorted_version_list[0][0]
                version_inactive_notification.apply_async(args=[latest_version.pk,
                                                                instance.pk,
                                                                False])
            content_list.append(content)
        return Response(
            content_list,
            status=status.HTTP_200_OK
        )


class UnpublicationRequestView(generics.UpdateAPIView):
    """
    Request record unpublicaiton view.
    :query_params: record_id__in=mdr.id
    """
    permission_classes = [elg_permissions.IsSuperUserOrProvider]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = ManagerSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return Manager.objects.filter(
                deleted=False,
                status=PUBLISHED,
                unpublication_requested=False
            )
        return Manager.objects.filter(
            deleted=False,
            status=PUBLISHED,
            unpublication_requested=False,
            curator=user
        ).distinct()

    def update(self, request, *args, **kwargs):
        requested_ids = request.query_params.get('record_id__in', None)
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set record_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )

        request_reason = request.data.get('reason')
        if not request_reason:
            return Response(
                'A reason for the request is required',
                status=status.HTTP_400_BAD_REQUEST
            )

        instances = self.filter_queryset(self.get_queryset())
        if len(instances) == 0:
            return Response(
                f'Unpublication of record cannot be requested.',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        updated_ids = []
        for instance in instances:
            updated_ids.append(instance.metadata_record_of_manager.pk)
            instance.request_unpublication(reason=request_reason)
        content = list()
        for id_ in requested_ids_list:
            content.append({
                'pk': id_,
                'notified': True if id_ in updated_ids else False
            })
        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        MyResourcesDocument().update(records)

        curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                'unpublication_request',
                                                                request_reason])
        return Response(
            content,
            status=status.HTTP_200_OK
        )


class ClaimRecordView(generics.UpdateAPIView):
    """
    Provider claim record view.
    """
    permission_classes = [elg_permissions.IsAuthenticated]
    queryset = Manager.objects.filter(
        deleted=False,
        status=PUBLISHED,
        curator__username=settings.SYSTEM_USER
    )

    def update(self, request, *args, **kwargs):
        pk = kwargs.get('pk', False)
        claimed_dict = dict()
        try:
            instance = self.queryset.get(Q(metadata_record_of_manager__id=pk))
            if not instance.is_claimable:
                return Response(
                    f'Record cannot be claimed',
                    status=status.HTTP_406_NOT_ACCEPTABLE
                )
            claim = instance.claim(claimed_by=request.user.username)
            claimed_dict[request.user.username] = [instance.metadata_record_of_manager.id]
        except Manager.DoesNotExist:
            return Response(
                f'Record cannot be claimed',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        content = {
            'pk': instance.metadata_record_of_manager.pk,
            'claimed': True if claim else False
        }
        if claim:
            curator_content_manager_notifications.apply_async(args=[[instance.metadata_record_of_manager.id],
                                                                    'claim',
                                                                    None,
                                                                    claimed_dict])
        return Response(
            content,
            status=status.HTTP_200_OK
        )


class CreateVersionView(generics.UpdateAPIView):
    """
    Create version of already existing record endpoint
    """
    permission_classes = [elg_permissions.IsSuperUserOrProviderOrContentManager]

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                status=PUBLISHED,
                deleted=False
            )
        return Manager.objects.filter(
            deleted=False,
            status=PUBLISHED,
            curator=user
        ).distinct()

    def update(self, request, *args, **kwargs):
        pk = kwargs.get('pk', False)
        version = request.data.get('version', '1.0.0 (automatically assigned)')
        is_functional = request.data.get('elg_compatible_service', False)
        version_date = request.data.get('version_date')
        resource_name = request.data.get('resource_name')
        try:
            queryset = self.get_queryset()
            instance = queryset.get(metadata_record_of_manager__pk=pk)
            if instance.under_construction:
                return Response(
                    f'New versions of under construction records cannot be created.',
                    status=status.HTTP_400_BAD_REQUEST
                )

            entity_type = instance.metadata_record_of_manager.described_entity.entity_type
            if not version and entity_type == 'LanguageResource':
                return Response(
                    f'Please specify a new version for this resource by following this template: 1.0.0',
                    status=status.HTTP_400_BAD_REQUEST
                )

            if entity_type in ['Project', 'Organization']:
                if not resource_name:
                    return Response(
                        f'Please specify a new name for this {entity_type.lower()}.',
                        status=status.HTTP_400_BAD_REQUEST
                    )

            # Check if version already exists
            if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
                other_versions = list(
                    instance.versioned_record_managers.filter(deleted=False).values_list(
                        'metadata_record_of_manager__described_entity__languageresource__version')
                )
                other_versions += instance.metadata_record_of_manager.described_entity.version
                if version in other_versions:
                    last_version = sorted(other_versions).pop()
                    return Response(
                        f'This version of the resource already exists. The latest version is: {last_version}',
                        status=status.HTTP_406_NOT_ACCEPTABLE
                    )
            new_version_record = instance.create_resource_version(version, version_date, resource_name, is_functional)
        except Manager.DoesNotExist:
            return Response(
                f'A new version cannot be created for this resource.',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )

        content = {
            'pk': instance.metadata_record_of_manager.pk,
            'version_created': True if new_version_record else False,
            'old_version': f'{instance.metadata_record_of_manager.__str__()} '
                           f'(id: {instance.metadata_record_of_manager.id})',
        }
        if new_version_record:
            content['new_version_pk'] = new_version_record.id
            content['new_version'] = f'{new_version_record.__str__()} (id: {new_version_record.id})'
            content['message'] = 'The new version can now be edited in your "My Items" dashboard.'
            curator_content_manager_notifications.apply_async(args=[[new_version_record.id],
                                                                    'create_version'])
            record = Manager.objects.filter(metadata_record_of_manager__pk=new_version_record.id)
            MyResourcesDocument().update(record)
        return Response(
            content,
            status=status.HTTP_200_OK
        )


class CopyResourceView(generics.UpdateAPIView):
    """
    Create a copy of an already existing record endpoint
    """
    permission_classes = [elg_permissions.IsSuperUserOrProviderOrContentManager]

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                deleted=False
            ).exclude(status=DRAFT)
        return Manager.objects.filter(
            deleted=False,
            curator=user
        ).exclude(status=DRAFT).distinct()

    def update(self, request, *args, **kwargs):
        pk = kwargs.get('pk', False)
        resource_name = request.data.get('resource_name')
        if not resource_name:
            return Response(
                f'Please specify a name for this item.',
                status=status.HTTP_400_BAD_REQUEST
            )
        version = request.data.get('version', '1.0.0 (automatically assigned)')
        is_functional = request.data.get('elg_compatible_service', False)
        given_name = request.data.get('given_name')
        try:
            queryset = self.get_queryset()
            instance = queryset.get(metadata_record_of_manager__pk=pk)
            entity_type = instance.metadata_record_of_manager.described_entity.entity_type
            if entity_type == 'Person':
                if not given_name:
                    return Response(
                        f'Please specify a given name for this person.',
                        status=status.HTTP_400_BAD_REQUEST
                    )
            new_resource = instance.copy_resource(resource_name, version, given_name, is_functional)
        except Manager.DoesNotExist:
            return Response(
                f'A copy of this resource cannot be created.',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        content = {
            'pk': instance.metadata_record_of_manager.pk,
            'resource_created': True if new_resource else False
        }
        if new_resource:
            content['new_resource_pk'] = new_resource.id
            content['new_resource'] = f'{new_resource.__str__()} (id: {new_resource.id})'
            content['message'] = 'The new resource can now be edited in your "My Items" dashboard.'
            curator_content_manager_notifications.apply_async(args=[[new_resource.id],
                                                                    'copy_resource'])
            record = Manager.objects.filter(metadata_record_of_manager__pk=new_resource.id)
            MyResourcesDocument().update(record)
        return Response(
            content,
            status=status.HTTP_200_OK
        )


class CuratorListView(generics.ListAPIView):
    """
    List view of current providers
    """
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    filter_backends = [DjangoFilterBackend]
    lookup_field = 'metadata_record_of_manager__id'
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.CuratorSerializer

    def get_queryset(self):
        return Manager.objects.filter(
            deleted=False
        )


class CuratorUpdateView(generics.UpdateAPIView):
    """
    Update view to assign provider as a metadatarecord curator
    """
    renderer_classes = [JSONRenderer]
    permission_classes = [elg_permissions.IsSuperUserOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.ManagerIdFilter
    serializer_class = serializers.ManagerSerializer

    def get_queryset(self):
        return Manager.objects.filter(
            status__in=PUBLISHED,
            curator__username=settings.SYSTEM_USER,
            deleted=False
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        requested_ids = request.query_params.get('record_id__in', None)
        # If it is not set a specified list of ids raise Exception
        # to avoid updating all records
        if requested_ids is None:
            return Response(
                'Set record_id__in query parameter.',
                status=status.HTTP_400_BAD_REQUEST
            )
        instances = self.filter_queryset(self.get_queryset())
        updated_ids = []
        for instance in instances:
            updated_ids.append(instance.metadata_record_of_manager.pk)
            instance.unpublish(notify=False)
            serializer = self.get_serializer(
                instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            serializer.instance.status = 'i'
            serializer.instance.save()
            if serializer.instance.curator.person:
                serializer.instance.metadata_record_of_manager.metadata_curator.add(serializer.instance.curator.person)
                serializer.instance.metadata_record_of_manager.save()
        requested_ids_list = [int(d) for d in requested_ids.split(',')]
        content = list()
        for id_ in requested_ids_list:
            content.append({
                'pk': id_,
                'updated': (True if id_ in updated_ids else False)
            })
        curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                'curator_assignment'])
        return Response(content)


class CuratorSelfView(generics.ListAPIView):
    """
    List view of records that a user is curator of
    """
    renderer_classes = [JSONRenderer]
    permission_classes = [elg_permissions.IsSuperUserOrProviderOrContentManager]
    lookup_field = 'metadata_record_of_manager__id'
    filter_backends = [DjangoFilterBackend]
    filter_class = filters.MetadataRecordIdFilter
    serializer_class = serializers.ManagerSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(
                deleted=False
            )

        return Manager.objects.filter(
            curator=user,
            deleted=False
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return_list = list()
        for n, instance in enumerate(serializer.data):
            instance['record'] = f"{settings.DJANGO_URL}" \
                                 f"{reverse('registry:metadatarecord-detail', kwargs={'pk': queryset[n].metadata_record_of_manager.pk})}"
            return_list.append(instance)
        return Response(return_list, status.HTTP_200_OK)


class RecordInfoView(generics.RetrieveAPIView):
    """
    List user's involvement in record
    """
    serializer_class = serializers.ManagerSerializer
    queryset = Manager
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        user = self.request.user
        pk = kwargs.pop('pk', False)
        try:
            instance = self.queryset.objects.get(metadata_record_of_manager__pk=pk)
        except Manager.DoesNotExist:
            return Response(
                f'Record (id: {pk}) does not exist.',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        return_dict = {
            'curator': True if instance.curator == user else False,
            'technical_validator': True if instance.technical_validator == user else False,
            'metadata_validator': True if instance.metadata_validator == user else False,
            'legal_validator': True if instance.legal_validator == user else False
        }
        return Response(
            return_dict,
            status=status.HTTP_200_OK
        )


@api_view(http_method_names=['POST'])
@permission_classes((DownloadDatasetPermission,))
def download(request, pk):
    record = MetadataRecord.objects.get(pk=pk)
    record_manager = record.management_object
    """
            Give permission for downloading a content file:
            1. if user is curator, supervisor of the record, validator of
                the record, with status INGEST, or admin give direct access
            2. requires user authentication: if not authenticated, 403
                2.a. allows access with signature: if not signature, 403
                2.b. allows direct access: if neither access with signature nor
                    direct access, 403
            3. allows access with signature: if not signature, 403
            4. missing licence category: 403
            """
    try:
        filename = unquote(request.headers['Filename'])
    except KeyError:
        return Response(
            {'detail': 'Filename header missing'},
            status.HTTP_400_BAD_REQUEST
        )
    try:
        distribution_id = request.headers['ELG-RESOURCE-DISTRIBUTION-ID']
    except KeyError:
        return Response(
            {'detail': 'ELG-RESOURCE-DISTRIBUTION-ID header missing'},
            status.HTTP_400_BAD_REQUEST
        )

    try:
        distribution = DatasetDistribution.objects.get(pk=distribution_id, dataset__file=filename)
    except DatasetDistribution.DoesNotExist:
        # Fallback to SoftwareDistribution
        try:
            distribution = SoftwareDistribution.objects.get(pk=distribution_id, package__file=filename)
        except SoftwareDistribution.DoesNotExist:
            return Response(
                {
                    'detail': f'Dataset or software distribution {distribution_id} '
                              f'does not exist or has no content file'},
                status.HTTP_404_NOT_FOUND

            )

    try:
        content_file = ContentFile.objects.get(file=filename)
    except ContentFile.DoesNotExist:
        LOGGER.error(f'Content file {filename} instance does not exist, but distribution {distribution_id} exists')
        return Response(
            {'detail': f'Content file {filename} does not exist'},
            status.HTTP_404_NOT_FOUND
        )

    # Verify file exists
    if not os.path.exists(content_file.file.storage.exists(content_file.file.name)):
        LOGGER.error(f'File {filename} is missing from storage')
        return Response(
            {'detail': f'File {filename} is missing from storage'},
            status.HTTP_404_NOT_FOUND
        )

    accept_signature = request.headers.get('ACCEPT-LICENCE', None)

    # If user is not the curator, supervisor of the record or the admin
    # or the legal or metadata validator for the ingested records,
    # check if conditions imposed from licence.licence_category are confirmed
    free_access = (
        True
        if (
                request.user == record_manager.curator
                or request.user.is_superuser
                or has_role(request.user, 'content_manager')
                or (
                        request.user in [record_manager.legal_validator,
                                         record_manager.metadata_validator,
                                         record_manager.technical_validator]
                        and record_manager.status == INGESTED
                )
        )
        else False
    )
    if not free_access:
        licences_categories = distribution.licence_terms.values_list('licence_category', flat=True).distinct()
        for licence_cat in licences_categories:
            if not licence_cat:
                error_msg = (
                    f'Distribution [{distribution.pk}] has '
                    f'a licence without licence category'
                )
                LOGGER.error(error_msg)
                return Response(
                    {'detail': error_msg},
                    status=status.HTTP_403_FORBIDDEN,
                )
            if LICENCE_CATEGORY_CHOICES.REQUIRES_USER_AUTHENTICATION in licence_cat:
                if not request.user.is_authenticated:
                    return Response(
                        {'detail': 'Authentication is required.'},
                        status=status.HTTP_403_FORBIDDEN
                    )

                if LICENCE_CATEGORY_CHOICES.ALLOWS_ACCESS_WITH_SIGNATURE in licence_cat:
                    if accept_signature is None:
                        return Response(
                            {'detail': 'Authentication and Accepting signature are required.'},
                            status=status.HTTP_403_FORBIDDEN,
                        )
                    if distribution.cost and distribution.cost.amount:
                        return response_forbidden(status=status.HTTP_402_PAYMENT_REQUIRED,
                                                  reason='Payment required.')
                elif LICENCE_CATEGORY_CHOICES.ALLOWS_DIRECT_ACCESS not in licence_cat:
                    error_msg = (
                        f'Distribution [{distribution.pk}] has '
                        f'{LICENCE_CATEGORY_CHOICES.REQUIRES_USER_AUTHENTICATION}'
                        f'category but {LICENCE_CATEGORY_CHOICES.ALLOWS_DIRECT_ACCESS} '
                        f'or {LICENCE_CATEGORY_CHOICES.ALLOWS_ACCESS_WITH_SIGNATURE} is missing'
                    )
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_403_FORBIDDEN,
                    )
            elif LICENCE_CATEGORY_CHOICES.ALLOWS_ACCESS_WITH_SIGNATURE in licence_cat:
                if accept_signature is None:
                    return Response(
                        {'detail': 'Authentication and Accepting signature are required.'},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                if distribution.cost and distribution.cost.amount:
                    return response_forbidden(status=status.HTTP_402_PAYMENT_REQUIRED,
                                              reason='Payment required.')
        # Update stats
        u_stats = UserDownloadStats(
            user=request.user if request.user.is_authenticated else get_user_model().objects.get(
                username='AnonymousUser'),
            source_of_download=request.META.get('HTTP_USER_AGENT', ''),
            metadata_record=record_manager.metadata_record_of_manager,
            licenses=list(distribution.licence_terms.values_list('licence_terms_name__en', flat=True)),
            is_latest_download=None,
            downloaded_distribution_id=distribution.pk
        )
        try:
            u_stats.full_clean()
        except ValidationError as e:
            LOGGER.error(e, exc_info=True)
        else:
            u_stats.save()
        # increment downloads stat
        md_stat, created = MetadataRecordStats.objects.get_or_create(
            metadata_record=record_manager.metadata_record_of_manager)
        md_stat.increment_downloads()
    file_path = content_file.file.name
    key = file_path
    try:
        s3client.head_object(Bucket=ELG_STORAGE.bucket_name, Key=key)
        response = s3client.generate_presigned_url(
            'get_object',
            Params={'Bucket': ELG_STORAGE.bucket_name, 'Key': key},
            ExpiresIn=10
        )
        try:
            user_ip = request.META['HTTP_X_FORWARDED_FOR']
        except KeyError:
            user_ip = request.META['REMOTE_ADDR']
        try:
            source_agent = request.META['HTTP_USER_AGENT']
        except KeyError:
            source_agent = ''
    except ClientError as e:
        if e.response['Error']['Code'] == "404":
            return Response({'message': f'File "{key}" was not found'}, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(e.response, status=e.response['Error']['Code'])

    return Response({"s3-url": response}, status=status.HTTP_200_OK)


@api_view(http_method_names=['GET'])
@permission_classes((DownloadDatasetPermission,))
def permanent_download_url(request, pk, dist_pk):
    record = MetadataRecord.objects.get(pk=pk)
    record_manager = record.management_object
    """
            Give permission for downloading a content file:
            1. if user is curator, supervisor of the record, validator of
                the record, with status INGEST, or admin give direct access
            2. requires user authentication: if not authenticated, 403
                2.a. allows access with signature: if not signature, 403
                2.b. allows direct access: if neither access with signature nor
                    direct access, 403
            3. allows access with signature: if not signature, 403
            4. missing licence category: 403
            """
    if not record_manager.has_content_file:
        return Response(
            {'detail': 'Resource has no hosted data'},
            status.HTTP_400_BAD_REQUEST
        )

    lr_type = record.described_entity.lr_subclass.lr_type
    try:
        if lr_type == 'ToolService':
            distribution = record.described_entity.lr_subclass.software_distribution.get(pk=dist_pk)
            filename = distribution.package.file
        else:
            distribution = record.described_entity.lr_subclass.dataset_distribution.get(pk=dist_pk)
            filename = distribution.dataset.file
    except ObjectDoesNotExist:
        return Response(
            {
                'detail': f'Dataset or software distribution {dist_pk} '
                          f'does not exist or has no content file'},
            status.HTTP_404_NOT_FOUND

        )
    try:
        content_file = ContentFile.objects.get(file=filename)
    except ContentFile.DoesNotExist:
        LOGGER.error(f'Content file {filename} instance does not exist, but distribution {dist_pk} exists')
        return Response(
            {'detail': f'Content file {filename} does not exist'},
            status.HTTP_404_NOT_FOUND
        )

    # Verify file exists
    if not os.path.exists(content_file.file.storage.exists(content_file.file.name)):
        LOGGER.error(f'File {filename} is missing from storage')
        return Response(
            {'detail': f'File {filename} is missing from storage'},
            status.HTTP_404_NOT_FOUND
        )

    accept_signature = request.headers.get('ACCEPT-LICENCE', None)

    # If user is not the curator, supervisor of the record or the admin
    # or the legal or metadata validator for the ingested records,
    # check if conditions imposed from licence.licence_category are confirmed
    free_access = (
        True
        if (
                request.user == record_manager.curator
                or request.user.is_superuser
                or has_role(request.user, 'content_manager')
                or (
                        request.user in [record_manager.legal_validator,
                                         record_manager.metadata_validator,
                                         record_manager.technical_validator]
                        and record_manager.status == INGESTED
                )
        )
        else False
    )
    if not free_access:
        licences_categories = distribution.licence_terms.values_list('licence_category', flat=True).distinct()
        for licence_cat in licences_categories:
            if not licence_cat:
                error_msg = (
                    f'Distribution [{distribution.pk}] has '
                    f'a licence without licence category'
                )
                LOGGER.error(error_msg)
                return Response(
                    {'detail': error_msg},
                    status=status.HTTP_403_FORBIDDEN,
                )
            if LICENCE_CATEGORY_CHOICES.REQUIRES_USER_AUTHENTICATION in licence_cat:
                if not request.user.is_authenticated:
                    return redirect(f'{record.get_display_url()}?auth=true')

                if LICENCE_CATEGORY_CHOICES.ALLOWS_ACCESS_WITH_SIGNATURE in licence_cat:
                    if accept_signature is None:
                        return redirect(f'{record.get_display_url()}?auth=true')
                    if distribution.cost and distribution.cost.amount:
                        return response_forbidden(status=status.HTTP_402_PAYMENT_REQUIRED,
                                                  reason='Payment required.')
                elif LICENCE_CATEGORY_CHOICES.ALLOWS_DIRECT_ACCESS not in licence_cat:
                    error_msg = (
                        f'Distribution [{distribution.pk}] has '
                        f'{LICENCE_CATEGORY_CHOICES.REQUIRES_USER_AUTHENTICATION}'
                        f'category but {LICENCE_CATEGORY_CHOICES.ALLOWS_DIRECT_ACCESS} '
                        f'or {LICENCE_CATEGORY_CHOICES.ALLOWS_ACCESS_WITH_SIGNATURE} is missing'
                    )
                    LOGGER.error(error_msg)
                    return redirect(f'{record.get_display_url()}?auth=true')
            elif LICENCE_CATEGORY_CHOICES.ALLOWS_ACCESS_WITH_SIGNATURE in licence_cat:
                if accept_signature is None:
                    return redirect(f'{record.get_display_url()}?auth=true')
                if distribution.cost and distribution.cost.amount:
                    return response_forbidden(status=status.HTTP_402_PAYMENT_REQUIRED,
                                              reason='Payment required.')
        # Update stats
        u_stats = UserDownloadStats(
            user=request.user if request.user.is_authenticated else get_user_model().objects.get(
                username='AnonymousUser'),
            source_of_download=request.META.get('HTTP_USER_AGENT', ''),
            metadata_record=record_manager.metadata_record_of_manager,
            licenses=list(distribution.licence_terms.values_list('licence_terms_name__en', flat=True)),
            is_latest_download=None,
            downloaded_distribution_id=distribution.pk
        )
        try:
            u_stats.full_clean()
        except ValidationError as e:
            LOGGER.error(e, exc_info=True)
        else:
            u_stats.save()
        # increment downloads stat
        md_stat, created = MetadataRecordStats.objects.get_or_create(
            metadata_record=record_manager.metadata_record_of_manager)
        md_stat.increment_downloads()
    file_path = content_file.file.name
    key = file_path
    try:
        s3client.head_object(Bucket=ELG_STORAGE.bucket_name, Key=key)
        response = s3client.generate_presigned_url(
            'get_object',
            Params={'Bucket': ELG_STORAGE.bucket_name, 'Key': key},
            ExpiresIn=10
        )
        try:
            user_ip = request.META['HTTP_X_FORWARDED_FOR']
        except KeyError:
            user_ip = request.META['REMOTE_ADDR']
        try:
            source_agent = request.META['HTTP_USER_AGENT']
        except KeyError:
            source_agent = ''
    except ClientError as e:
        if e.response['Error']['Code'] == "404":
            return Response({'message': f'File "{key}" was not found'}, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(e.response, status=e.response['Error']['Code'])

    return redirect(response)


class ContentFilesView(generics.RetrieveAPIView):
    lookup_field = 'metadata_record_of_manager__pk'
    permission_classes = [elg_permissions.IsSuperUserOrProviderOrContentManager]
    queryset = Manager.objects.all()
    serializer_class = serializers.ManagerContentFilesSerializer


class StorageAuthorizationView(generics.CreateAPIView):
    """
    Permission access to s3proxy for uploading, deleting, replacing, downloading
    content and media files view.
    """

    lookup_field = 'metadata_record_of_manager__pk'

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        1. if is for downloading record data
        3. Else is for uploading, deleting, notifying
        """
        action = self.request.data.get('action')
        if action == 'get':
            permissions = [DownloadDatasetPermission]
        else:
            permissions = [elg_permissions.IsSuperUserOrOwnerOrContentManager]
        return [p() for p in permissions]

    def get_queryset(self):
        user = self.request.user
        action = self.request.data.get('action')
        if action == 'get' or user.is_superuser or has_role(user, 'content_manager'):
            return Manager.objects.filter(deleted=False)

        return Manager.objects.filter(
            Q(deleted=False),
            (Q(curator=user) & Q(status__in=[DRAFT, INTERNAL]))
        )

    def create(self, request, *args, **kwargs):
        """
        Request from s3proxy, to receive permission for
        1. downloading file when action == get,
        1. uploading file when action == put,
            a. public scope for uploading record's media file
            b. private scope for uploading record's content file
        2. deleting file when action == delete,
            a. public scope for deleting media file
            b. private scope for deleting uploaded content file. If content file
                is referred to a distribution, do not delete it
        3. notifying for successful upload when action == notify:
            a. public scope for media file uploaded notification from s3proxy
            b. private scope for record's content file uploaded notification and
                creation of ContentFile instance
        Request kwargs:
        1. In header: Scope, Filename, ELG-RESOURCE-DISTRIBUTION-ID, ACCEPT-LICENCE
        2. In body: action, api_key, data={Filename, LastModified}
        """
        api_key = request.data.get('api_key')
        # TODO temporary clause, remove when proxy is working as intended
        if not request.data.get('action') == 'delete':
            # api key not found or incorrect
            if api_key != settings.API_KEY:
                return Response(
                    {'detail': 'Invalid API key or not specified'},
                    status.HTTP_401_UNAUTHORIZED
                )

        record_manager = self.get_object()
        try:
            scope = request.headers['Scope']
        except KeyError:
            return Response(
                {'detail': 'Scope header missing'},
                status.HTTP_400_BAD_REQUEST
            )
        try:
            filename = unquote(request.headers['Filename'])
        except KeyError:
            return Response(
                {'detail': 'Filename header missing'},
                status.HTTP_400_BAD_REQUEST
            )

        action = request.data.get('action')
        if action not in ['get', 'put', 'delete', 'notify']:
            return Response(
                {'detail': f'action [{action}] has not been specified or it is not acceptable'},
                status.HTTP_400_BAD_REQUEST
            )

        if action == 'get':
            return self.__get_action(request, record_manager, filename)
        elif action == 'put':
            return self.__put_action(request, record_manager, scope, filename)
        elif action == 'delete':
            return self.__delete_action(scope, filename)
        else:  # action == notify
            return self.__notify_action(request, record_manager, scope, filename)

    def __get_action(self, request, record_manager, filename):
        """
        Give permission for downloading a content file:
        1. if user is curator, supervisor of the record, validator of
            the record, with status INGEST, or admin give direct access
        2. requires user authentication: if not authenticated, 403
            2.a. allows access with signature: if not signature, 403
            2.b. allows direct access: if neither access with signature nor
                direct access, 403
        3. allows access with signature: if not signature, 403
        4. missing licence category: 403
        """
        try:
            distribution_id = request.headers['ELG-RESOURCE-DISTRIBUTION-ID']
        except KeyError:
            return Response(
                {'detail': 'ELG-RESOURCE-DISTRIBUTION-ID header missing'},
                status.HTTP_400_BAD_REQUEST
            )

        try:
            distribution = DatasetDistribution.objects.get(pk=distribution_id, dataset__file=filename)
        except DatasetDistribution.DoesNotExist:
            # Fallback to SoftwareDistribution
            try:
                distribution = SoftwareDistribution.objects.get(pk=distribution_id, package__file=filename)
            except SoftwareDistribution.DoesNotExist:
                return Response(
                    {
                        'detail': f'Dataset or software distribution {distribution_id} '
                                  f'does not exist or has no content file'},
                    status.HTTP_404_NOT_FOUND

                )

        try:
            content_file = ContentFile.objects.get(file=filename)
        except ContentFile.DoesNotExist:
            LOGGER.error(f'Content file {filename} instance does not exist, but distribution {distribution_id} exists')
            return Response(
                {'detail': f'Content file {filename} does not exist'},
                status.HTTP_404_NOT_FOUND
            )

        # Verify file exists
        if not os.path.exists(content_file.file.storage.exists(content_file.file.name)):
            LOGGER.error(f'File {filename} is missing from storage')
            return Response(
                {'detail': f'File {filename} is missing from storage'},
                status.HTTP_404_NOT_FOUND
            )

        accept_signature = request.headers.get('ACCEPT-LICENCE', None)

        # If user is not the curator, supervisor of the record or the admin
        # or the legal or metadata validator for the ingested records,
        # check if conditions imposed from licence.licence_category are confirmed
        free_access = (
            True
            if (
                    request.user == record_manager.curator
                    or request.user.is_superuser
                    or has_role(request.user, 'content_manager')
                    or (
                            request.user in [record_manager.legal_validator,
                                             record_manager.metadata_validator,
                                             record_manager.technical_validator]
                            and record_manager.status == INGESTED
                    )
            )
            else False
        )
        if not free_access:
            licences_categories = distribution.licence_terms.values_list('licence_category', flat=True).distinct()
            for licence_cat in licences_categories:
                if not licence_cat:
                    error_msg = (
                        f'Distribution [{distribution.pk}] has '
                        f'a licence without licence category'
                    )
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_403_FORBIDDEN,
                    )
                if LICENCE_CATEGORY_CHOICES.REQUIRES_USER_AUTHENTICATION in licence_cat:
                    if not request.user.is_authenticated:
                        return Response(
                            {'detail': 'Authentication is required.'},
                            status=status.HTTP_403_FORBIDDEN
                        )

                    if LICENCE_CATEGORY_CHOICES.ALLOWS_ACCESS_WITH_SIGNATURE in licence_cat:
                        if accept_signature is None:
                            return Response(
                                {'detail': 'Authentication and Accepting signature are required.'},
                                status=status.HTTP_403_FORBIDDEN,
                            )
                        if distribution.cost and distribution.cost.amount:
                            return response_forbidden(status=status.HTTP_402_PAYMENT_REQUIRED,
                                                      reason='Payment required.')
                    elif LICENCE_CATEGORY_CHOICES.ALLOWS_DIRECT_ACCESS not in licence_cat:
                        error_msg = (
                            f'Distribution [{distribution.pk}] has '
                            f'{LICENCE_CATEGORY_CHOICES.REQUIRES_USER_AUTHENTICATION}'
                            f'category but {LICENCE_CATEGORY_CHOICES.ALLOWS_DIRECT_ACCESS} '
                            f'or {LICENCE_CATEGORY_CHOICES.ALLOWS_ACCESS_WITH_SIGNATURE} is missing'
                        )
                        LOGGER.error(error_msg)
                        return Response(
                            {'detail': error_msg},
                            status=status.HTTP_403_FORBIDDEN,
                        )
                elif LICENCE_CATEGORY_CHOICES.ALLOWS_ACCESS_WITH_SIGNATURE in licence_cat:
                    if accept_signature is None:
                        return Response(
                            {'detail': 'Authentication and Accepting signature are required.'},
                            status=status.HTTP_400_BAD_REQUEST,
                        )
                    if distribution.cost and distribution.cost.amount:
                        return response_forbidden(status=status.HTTP_402_PAYMENT_REQUIRED,
                                                  reason='Payment required.')
            # Update stats
            u_stats = UserDownloadStats(
                user=request.user if request.user.is_authenticated else get_user_model().objects.get(
                    username='AnonymousUser'),
                source_of_download=request.META.get('HTTP_USER_AGENT', ''),
                metadata_record=record_manager.metadata_record_of_manager,
                licenses=list(distribution.licence_terms.values_list('licence_terms_name__en', flat=True)),
                is_latest_download=None,
                downloaded_distribution_id=distribution.pk
            )
            try:
                u_stats.full_clean()
            except ValidationError as e:
                LOGGER.error(e, exc_info=True)
            else:
                u_stats.save()
            # increment downloads stat
            md_stat, created = MetadataRecordStats.objects.get_or_create(
                metadata_record=record_manager.metadata_record_of_manager)
            md_stat.increment_downloads()

        file_path = content_file.file.name
        content = {
            'bucket': ELG_STORAGE.bucket_name,
            'filename': file_path,
        }
        return Response(content)

    def __put_action(self, request, record_manager, scope, filename):
        """
        Give permission for uploading a content or media file:
        """
        # set the base path of the file path
        bucket = settings.S3_PUBLIC_BUCKET if scope == 'public' else ELG_STORAGE.bucket_name

        if scope == 'private':
            # Upload a content file for the record
            # Ensure file type is in the list of ACCEPTED_DATA_FILE_TYPES.
            _, file_type = os.path.splitext(filename)
            if file_type not in settings.ACCEPTED_DATA_FILE_TYPES:
                return Response(
                    {'detail': (
                        f'[{filename}] has not an accepted file extension. '
                        f'Accepted file extensions are '
                        f'[{", ".join(settings.ACCEPTED_DATA_FILE_TYPES)}].'
                    )},
                    status.HTTP_400_BAD_REQUEST
                )
            # Replace or add a new file
            try:
                existing_content_file = record_manager.content_files.get(file__contains=filename)
            except ContentFile.DoesNotExist:
                # Create a new one
                # Set the relative path of the file path
                record_dir = record_manager.identifier
                file_path = os.path.join(record_dir, _create_uuid(), filename)
            except ContentFile.MultipleObjectsReturned:
                LOGGER.exception(
                    f'More than one content files {filename} for {record_manager.metadata_record.__str__()}.'
                )
                return Response(
                    {
                        'detail': f'More than one content files {filename} for {record_manager.metadata_record.__str__()}.'},
                    status.HTTP_403_FORBIDDEN
                )
            else:
                file_path = existing_content_file.file.name
        else:
            # Upload media file
            identifier = f'{_create_uuid()}_u{request.user.id}'
            file_path = f'{identifier}_{filename}'

        record_id = record_manager.metadata_record_of_manager.pk

        content = {
            'id': record_id,
            'bucket': bucket,
            'filename': file_path,
        }
        return Response(content)

    def __delete_action(self, scope, file):
        """
        Delete the uploaded file:
        1. if it is a record content file, update record's management
            information
        2. if it is a media file, give access for deletion
        """
        record_id = 0
        if scope == 'private':
            # Delete content file for the record
            # retrieve the content file
            try:
                content_file = ContentFile.objects.get(file=file)
                record_id = content_file.record_manager.metadata_record_of_manager.pk

            except ContentFile.DoesNotExist:
                return Response(
                    {'detail': f'Content file [{file}] does not exists.'},
                    status.HTTP_404_NOT_FOUND
                )
            if content_file.dataset_distributions.exists() or content_file.software_distributions.exists():
                return Response(
                    {'detail': f'Content file [{file}] is referred to a distribution and cannot be deleted.'},
                    status.HTTP_403_FORBIDDEN
                )
            else:
                content_file.delete()

        # set the base path of the file path
        bucket = (settings.S3_PUBLIC_BUCKET if scope == 'public' else ELG_STORAGE.bucket_name)

        content = {
            'id': record_id,
            'bucket': bucket,
            'filename': file,
        }
        return Response(content)

    def __notify_action(self, request, record_manager, scope, file):
        """
        After a successful content file upload:
        1. if record's content file, update the record's management
            information
        2. if a media file return ok
        """
        if scope == 'private':
            filename = '/'.join(request.data.get('data')['Location'].split('/')[4:])
            # Update the record management information
            record_manager.date_last_modified = date_parser.parse(
                request.data.get('data')['LastModified']
            )
            record_manager.save()
            # Create ContentFile instance
            ContentFile.objects.create(
                file=filename,
                record_manager=record_manager,
            )
        return Response({'action': 'update'})


def catalog_data_report(request):
    if has_role(request.user, ['admin', 'content_manager']):
        client = Elasticsearch(hosts=ES_URL)

        output = io.StringIO()
        title = f'ELG-Catalogue-data-report_{datetime.datetime.today().strftime("%Y-%m-%d")}'
        delimiter = ' | '
        query = {"query": {"bool": {"must_not": {"match": {"resource_type": "Tool/Service"}}}}}
        result = helpers.scan(client=client, scroll='2m', query=query, index='catalog_report_index')
        header = [
            'Resource ID',
            'Resource Name',
            'Resource Short Name',
            'Landing Page',
            'Source of Metadata Record',
            'Publication Date',
            'Resource Type',
            'Subclass',
            'Version',
            'Domain',
            'Keywords',
            'Subject',
            'Linguality Type',
            'Multilinguality Type',
            'Languages',
            'Dataset Distribution Form',
            'Licence Terms',
            'Data Format',
            'Time Coverage',
            'Geographic Coverage',
            'Register',
            'Text Type',
            'Text Genre',
            'Audio Genre',
            'Speech Genre',
            'Annotation Type',
            'Metalanguages',
            'Encoding Level',
            'ML Framework',
            'For Information',
            'Hosted'
        ]
        tsv_writer = csv.writer(output, delimiter='\t')
        tsv_writer.writerow(header)

        for item in result:
            record = item.get('_source')
            if record.get('languages'):
                if len(record.get('languages')) > 6:
                    languages = 'Multiple languages'
                else:
                    languages = delimiter.join(record.get('languages'))
            else:
                languages = 'N/A'

            if record.get('metalanguage'):
                if len(record.get('metalanguage')) > 6:
                    metalanguages = 'Multiple languages'
                else:
                    metalanguages = delimiter.join(record.get('metalanguage'))
            else:
                metalanguages = 'N/A'

            if record.get('landing_page'):
                landing_pages = delimiter.join(record.get('landing_page'))
            else:
                landing_pages = 'N/A'

            tsv_writer.writerow([
                record.get('id'),
                record.get('resource_name'),
                record.get('resource_short_name') or 'undefined',
                landing_pages,
                record.get('source_of_metadata_record'),
                record.get('publication_date'),
                record.get('resource_type'),
                record.get('subclass'),
                record.get('version'),
                delimiter.join(record.get('domain')) or 'N/A',
                delimiter.join(record.get('keywords')) or 'N/A',
                delimiter.join(record.get('subject')) or 'N/A',
                delimiter.join(record.get('linguality_type')) or 'N/A',
                delimiter.join(record.get('multilinguality_type')) or 'N/A',
                languages,
                delimiter.join(record.get('dataset_distribution_form')) or 'N/A',
                delimiter.join(record.get('licence_terms')) or 'N/A',
                delimiter.join(record.get('data_format')) or 'N/A',
                delimiter.join([item for sublist in record.get('time_coverage') for item in sublist]) or 'N/A',
                delimiter.join([item for sublist in record.get('geographic_coverage') for item in sublist]) or 'N/A',
                delimiter.join([item for sublist in record.get('register') for item in sublist]) or 'N/A',
                delimiter.join(record.get('text_type')) or 'N/A',
                delimiter.join(record.get('text_genre')) or 'N/A',
                delimiter.join(record.get('audio_genre')) or 'N/A',
                delimiter.join(record.get('speech_genre')) or 'N/A',
                delimiter.join(record.get('annotation_type')) or 'N/A',
                metalanguages,
                delimiter.join(record.get('encoding_level')) or 'N/A',
                record.get('ml_framework') or 'N/A',
                record.get('for_information'),
                record.get('hosted'),

            ])
        output.seek(0)
        response = HttpResponse(output.read(), content_type='text/tab-separated-values')
        output.close()
        response['Content-Disposition'] = "attachment; filename={}.tsv".format(title)

        return response
    else:
        response_forbidden()


def catalog_tool_report(request):
    if has_role(request.user, ['admin', 'content_manager']):
        client = Elasticsearch(hosts=ES_URL)

        output = io.StringIO()
        title = f'ELG-Catalogue-tool-report_{datetime.datetime.today().strftime("%Y-%m-%d")}'
        delimiter = ' | '
        query = {"query": {"bool": {"must": {"match": {"resource_type": "Tool/Service"}}}}}
        result = helpers.scan(client=client, scroll='2m', query=query, index='catalog_report_index')
        header = [
            'Resource ID',
            'Resource Name',
            'Resource Short Name',
            'Landing Page',
            'Source of Metadata Record',
            'Publication Date',
            'Resource Type',
            'Function',
            'Version',
            'Domain',
            'Keywords',
            'Subject',
            'Language Dependent',
            'Input Processing Type',
            'Input Media Type',
            'Input Languages',
            'Input Annotation Types',
            'Output Processing Type',
            'Output Media Type',
            'Output Languages',
            'Output Annotation Types',
            'Software Distribution Form',
            'Licence Terms',
            'Access Rights',
            'ELG Compatible',
            'For Information',
            'Hosted'
        ]
        tsv_writer = csv.writer(output, delimiter='\t')
        tsv_writer.writerow(header)

        for item in result:
            record = item.get('_source')
            if record.get('landing_page'):
                landing_pages = delimiter.join(record.get('landing_page'))
            else:
                landing_pages = 'N/A'

            if record.get('input_languages'):
                if len(record.get('input_languages')) > 6:
                    input_languages = 'Multiple languages'
                else:
                    input_languages = delimiter.join(record.get('input_languages'))
            else:
                input_languages = 'N/A'

            if record.get('output_languages'):
                if len(record.get('output_languages')) > 6:
                    output_languages = 'Multiple languages'
                else:
                    output_languages = delimiter.join(record.get('output_languages'))
            else:
                output_languages = 'N/A'

            tsv_writer.writerow([
                record.get('id'),
                record.get('resource_name'),
                record.get('resource_short_name') or 'undefined',
                landing_pages,
                record.get('source_of_metadata_record'),
                record.get('publication_date'),
                record.get('resource_type'),
                delimiter.join(record.get('function')) or 'N/A',
                record.get('version'),
                delimiter.join(record.get('domain')) or 'N/A',
                delimiter.join(record.get('keywords')) or 'N/A',
                delimiter.join(record.get('subject')) or 'N/A',
                record.get('language_independent'),
                delimiter.join(record.get('input_processing_type')) or 'N/A',
                delimiter.join(record.get('input_media_type')) or 'N/A',
                input_languages,
                delimiter.join(record.get('input_annotation_types')) or 'N/A',
                delimiter.join(record.get('output_processing_type')) or 'N/A',
                delimiter.join(record.get('output_media_type')) or 'N/A',
                output_languages,
                delimiter.join(record.get('output_annotation_types')) or 'N/A',
                delimiter.join(record.get('software_distribution_form')) or 'N/A',
                delimiter.join(record.get('licence_terms')) or 'N/A',
                delimiter.join(record.get('access_rights')) or 'N/A',
                record.get('elg_compatible'),
                record.get('for_information'),
                record.get('hosted')
            ])
        output.seek(0)
        response = HttpResponse(output.read(), content_type='text/tab-separated-values')
        output.close()
        response['Content-Disposition'] = "attachment; filename={}.tsv".format(title)

        return response
    else:
        response_forbidden()
