from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
    CompoundSearchFilterBackend,
    MultiMatchSearchFilterBackend, FacetedSearchFilterBackend)
from django_elasticsearch_dsl_drf.pagination import PageNumberPagination
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet
from elasticsearch_dsl import TermsFacet, Q

from accounts import elg_permissions
from utils.index_backends.backends import LuceneStringSearchFilterBackend
from management.management_dashboard_index.documents import MyResourcesDocument, MyValidationsDocument
from management.management_dashboard_index.serializers import MyResourcesDocumentSerializer, \
    MyValidationsDocumentSerializer


class MyResourcesDocumentView(BaseDocumentViewSet):
    """The MetadataDocument view."""

    def __init__(self, *args, **kwargs):
        super(MyResourcesDocumentView, self).__init__(*args, **kwargs)
        self.search = self.search.extra(track_total_hits=True)

    permission_classes = (elg_permissions.IsSuperUserOrProviderOrContentManager,)
    document = MyResourcesDocument
    serializer_class = MyResourcesDocumentSerializer
    pagination_class = PageNumberPagination
    lookup_field = 'id'
    filter_backends = [
        LuceneStringSearchFilterBackend,
        FilteringFilterBackend,
        FacetedSearchFilterBackend,
        CompoundSearchFilterBackend,
        MultiMatchSearchFilterBackend,
        DefaultOrderingFilterBackend,
        OrderingFilterBackend,
    ]

    simple_query_string_options = {
        "default_operator": "and",
    }

    # Define search fields
    search_fields = {
        'resource_name': {'boost': 4},
        'resource_type': {'boost': 5},
        'curator': None,
        'status': None,
        'keycloak_id': None
    }
    # Define filter fields
    filter_fields = {
        'id': None,
        'resource_name': 'resource_name.raw',
        'resource_type': 'items_facet.raw',
        'status': 'status.raw',
        'entity_type': 'entity_type.raw',
        'creation_date': None,
        'last_date_updated': None,
    }
    # Define ordering fields
    ordering_fields = {
        'last_date_updated': 'last_date_updated',
        'id': 'id',
        'resource_name': 'resource_name.raw',
        'creation_date': 'creation_date'
    }
    # Specify default ordering
    ordering = ('-last_date_updated', '_score', 'id', 'resource_name.raw')

    faceted_search_fields = {
        'status': {
            'field': 'status',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'resource_type': {
            'field': 'items_facet',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },
        'entity_type': {
            'field': 'items_facet',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        }
    }

    def get_queryset(self):
        queryset = self.search.query()
        queryset.model = self.document.Django.model
        user = self.request.user
        queryset = self.search.query("term", keycloak_id=user.keycloak_id)
        return queryset


class MyValidationsDocumentView(BaseDocumentViewSet):
    """The MetadataDocument view."""

    def __init__(self, *args, **kwargs):
        super(MyValidationsDocumentView, self).__init__(*args, **kwargs)
        self.search = self.search.extra(track_total_hits=True)

    permission_classes = (elg_permissions.IsSuperUserOrContentManagerOrValidator,)
    document = MyValidationsDocument
    serializer_class = MyValidationsDocumentSerializer
    pagination_class = PageNumberPagination
    lookup_field = 'id'
    filter_backends = [
        LuceneStringSearchFilterBackend,
        FilteringFilterBackend,
        FacetedSearchFilterBackend,
        CompoundSearchFilterBackend,
        MultiMatchSearchFilterBackend,
        DefaultOrderingFilterBackend,
        OrderingFilterBackend,
    ]

    simple_query_string_options = {
        "default_operator": "and",
    }

    # Define search fields
    search_fields = {
        'resource_name': {'boost': 4},
        'resource_type': {'boost': 5},
        'description': None,
        'curator': None,
        'status': None
    }
    # Define filter fields
    filter_fields = {
        'id': None,
        'resource_name': 'resource_name.raw',
        'resource_type': 'items_facet.raw',
        'legal_validator': 'legal_validator.raw',
        'metadata_validator': 'metadata_validator.raw',
        'technical_validator': 'technical_validator.raw',
        'curator': 'curator.raw',
        'legally_valid': 'legally_valid',
        'metadata_valid': 'metadata_valid',
        'technically_valid': 'technically_valid',
        'status': 'status.raw',
        'service_status': 'service_status.raw',
        'entity_type': 'entity_type.raw',
        'has_data': 'has_data',
        'elg_compatible_service': 'elg_compatible_service',
        'creation_date': None,
        'ingestion_date': None,
        'resubmitted': None
    }
    # Define ordering fields
    ordering_fields = {
        'ingestion_date': 'ingestion_date',
        'id': 'id',
        'resource_name': 'resource_name.raw'
    }
    # Specify default ordering
    ordering = ('-ingestion_date', '_score', 'id', 'resource_name.raw')

    faceted_search_fields = {
        'status': {
            'field': 'status',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'service_status': {
            'field': 'service_status',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'resource_type': {
            'field': 'items_facet',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },
        'legal_validator': {
            'field': 'legal_validator',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'metadata_validator': {
            'field': 'metadata_validator',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'technical_validator': {
            'field': 'technical_validator',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'curator': {
            'field': 'curator',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'legally_valid': {
            'field': 'legally_valid',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'metadata_valid': {
            'field': 'metadata_valid',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'technically_valid': {
            'field': 'technically_valid',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'entity_type': {
            'field': 'items_facet',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },
        'has_data': {
            'field': 'has_data',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },
        'elg_compatible_service': {
            'field': 'elg_compatible_service',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },
        'resubmitted': {
            'field': 'resubmitted',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
    }

    def get_queryset(self):
        queryset = self.search.query()
        queryset.model = self.document.Django.model
        user = self.request.user
        q = Q("multi_match", query=user.username, fields=['legal_validator', 'metadata_validator', 'technical_validator'])
        queryset = self.search.query(q)
        return queryset
