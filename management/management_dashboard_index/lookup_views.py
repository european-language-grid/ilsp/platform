import logging
from urllib.parse import unquote
from difflib import SequenceMatcher

from rest_framework import viewsets, status, mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from django.contrib.auth import get_user_model

from django.db.models import Q

from accounts.api import UserSerializer
from management.models import Manager

LOGGER = logging.getLogger(__name__)

ACTION_NOT_ALLOWED = {"detail": "You do not have permission to perform this action."}


class UserLookUpView(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        if 'query' in request.GET and len(request.query_params['query']) > 2:
            decoded_query_params = unquote(request.query_params['query'])
            queryset = self.queryset.filter(Q(username__icontains=decoded_query_params))
            serializer = self.get_serializer(queryset, many=True)
            return_list = list()
            for n, instance in enumerate(serializer.data):
                instance['similarity'] = SequenceMatcher(None, instance['username'].lower(),
                                                         decoded_query_params.lower()).ratio()
                return_list.append(instance)
            return_list.sort(key=lambda x: x.get('similarity'), reverse=True)
            return Response(return_list, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'},
                        status=status.HTTP_400_BAD_REQUEST)


