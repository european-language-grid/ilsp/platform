import csv
import datetime
import io
import logging

from botocore.exceptions import ClientError
from django import forms
from django.conf import settings
from django.contrib import admin, messages
# Register your models here.
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import path
from django.utils.safestring import mark_safe
from rolepermissions.checkers import has_role

from management import ELG_STORAGE
from management.catalog_report_index.documents import CatalogReportDocument
from management.management_dashboard_index.documents import MyResourcesDocument, MyValidationsDocument
from management.models import Manager, PUBLISHED, INTERNAL, Validator
from management.views import s3client
from registry.curation_mechanisms import initiate_curation, model_dict, serializer_dict
from registry.forms import CsvImportForm, CurationCsvImportForm, CurationCsvExportForm
from registry.merging_mechanisms import initiate_entity_merge
from registry.models import DescribedEntity, MetadataRecord
from registry.search_indexes.documents import MetadataDocument
from registry.serializer_tasks import GetRelatedRecords
from utils.admin_utils import ReadOnlyModelAdmin

LOGGER = logging.getLogger(__name__)


class ManagerAdmin(admin.ModelAdmin):
    """
    A Generic admin for all Manager objects
    """

    def has_add_permission(self, request, obj=None):
        return False


class LegalValidation(Manager):
    """
    A proxy model for Manager to handle Legal Validations' relevant fields
    """

    class Meta:
        proxy = True

    def __str__(self):
        return self.metadata_record_of_manager.__str__()


class MetadataValidation(Manager):
    """
    A proxy model for Manager to handle Metadata Validations' relevant fields
    """

    class Meta:
        proxy = True

    def __str__(self):
        return self.metadata_record_of_manager.__str__()


class TechnicalValidation(Manager):
    """
    A proxy model for Manager to handle Technical Validations' relevant fields
    """

    class Meta:
        proxy = True

    def __str__(self):
        return self.metadata_record_of_manager.__str__()


class ValidatorForm(forms.ModelForm):
    """
    Generic validation form
    """
    review_comment = forms.CharField(label='review comments (for the provider)', widget=forms.Textarea(),
                                     required=False)
    validator_note = forms.CharField(label='validator notes (internal)', widget=forms.Textarea(), required=False)


class LegalValidatorForm(ValidatorForm):
    """
    Validation form with Legal Validation relevant fields
    """

    class Meta:
        model = Manager
        fields = ('legally_valid',)

    def __init__(self, *args, **kwargs):
        super(LegalValidatorForm, self).__init__(*args, **kwargs)

    def clean(self):
        data = self.cleaned_data
        # rejecting a validation is not allowed if no reason is provided
        if data['legally_valid'] is False and not data.get('review_comment'):
            raise forms.ValidationError('You need to specify a review comment')


class TechnicalValidatorForm(ValidatorForm):
    """
    Validation form with Technical Validation relevant fields
    """

    class Meta:
        model = Manager
        fields = ('technically_valid', 'metadata_valid', 'slack_channel', 'jira_ticket')

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(TechnicalValidatorForm, self).__init__(*args, **kwargs)

    slack_channel = forms.URLField(widget=forms.TextInput(attrs={'size': 60}), required=False)
    jira_ticket = forms.URLField(widget=forms.TextInput(attrs={'size': 60}), required=False)

    def clean(self):
        # check if instance has lt service registration
        try:
            lt_registration = self.instance.metadata_record_of_manager.lt_service
        except ObjectDoesNotExist:
            lt_registration = None
        data = self.cleaned_data
        # we cannot approve a technical validation if there is a pending lt service registration for this record
        if data['technically_valid'] and lt_registration and lt_registration.status != 'COMPLETED':
            raise forms.ValidationError('There is a pending LT Service Registration for this record')
        # rejecting a validation is not allowed if no reason is provided
        if data['technically_valid'] is False and not data.get('review_comment'):
            raise forms.ValidationError('You need to specify a review comment')


class MetadataValidatorForm(ValidatorForm):
    """
    Validation form with Metadata Validation relevant fields
    """

    class Meta:
        model = Manager
        fields = ('metadata_valid', 'technically_valid')

    def __init__(self, *args, **kwargs):
        super(MetadataValidatorForm, self).__init__(*args, **kwargs)

    def clean(self):
        data = self.cleaned_data
        # rejecting a validation is not allowed if no reason is provided
        if data['metadata_valid'] is False and not data.get('review_comment'):
            raise forms.ValidationError('You need to specify a review comment')


class ValidationAdmin(ManagerAdmin):
    """
    Generic Validation Admin
    """
    search_fields = ['get_str']

    list_filter = ('legally_valid', 'metadata_valid', 'technically_valid', 'status')
    list_display = (
        'metadata_record_of_manager', 'has_content_file', 'metadata_valid', 'technically_valid', 'legally_valid',
        'status'
    )
    user = None
    # map roles to relevant forms and primary field
    validator_types = {
        'legal_validator': {'form': LegalValidatorForm, 'field': 'legally_valid'},
        'metadata_validator': {'form': MetadataValidatorForm, 'field': 'metadata_valid'},
        'technical_validator': {'form': TechnicalValidatorForm, 'field': 'technically_valid'},

    }
    vld_type = None
    admin_form = None

    change_form_template = 'admin/management/validation_change_form.html'

    def get_form(self, request, obj=None, **kwargs):
        self.vld_type = kwargs.pop('vld_type')
        if has_role(self.user, self.vld_type):
            self.admin_form = self.validator_types[self.vld_type]['form']
            form = self.admin_form
            form.current_user = request.user
            return form

    def get_queryset(self, request, **kwargs):
        self.vld_type = kwargs.pop('vld_type')
        self.user = request.user
        if has_role(self.user, self.vld_type):
            if self.vld_type != 'metadata_validator':
                return self.model.objects.filter(**{self.vld_type: request.user}).exclude(deleted=True)
            else:
                return self.model.objects.filter(Q(**{self.vld_type: request.user})).exclude((Q(functional_service=True)
                                                                                              & Q(
                            under_construction=False))
                                                                                             | (~Q(size=0) &
                                                                                                Q(etag__isnull=False)) |
                                                                                             Q(deleted=True))

    def get_str(self, obj):
        return obj.metadata_record_of_manager.__str__().split(']')[1].strip()

    def get_search_results(self, request, queryset, search_term):
        # search_term is what you input in admin site
        search_term_list = search_term.split(' ')  # ['apple','bar']
        if not any(search_term_list):
            return queryset, False

        queryset = self.get_queryset(request).filter(
            Q(
                metadata_record_of_manager__described_entity__languageresource__resource_name__en__icontains=search_term) |
            Q(metadata_record_of_manager__described_entity__project__project_name__en__icontains=search_term) |
            Q(
                metadata_record_of_manager__described_entity__actor__organization__organization_name__en__icontains=search_term) |
            Q(
                metadata_record_of_manager__described_entity__actor__group__organization_name__en__icontains=search_term) |
            Q(metadata_record_of_manager__described_entity__actor__person__surname__en__icontains=search_term) |
            Q(
                metadata_record_of_manager__described_entity__licenceterms__licence_terms_name__en__icontains=search_term) |
            Q(metadata_record_of_manager__described_entity__document__title__en__icontains=search_term) |
            Q(metadata_record_of_manager__described_entity__repository__repository_name__en__icontains=search_term)
        )

        return queryset, False

    def _get_resource_type(self, obj):
        if obj.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            return obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type
        return obj.metadata_record_of_manager.described_entity.entity_type

    def validator_download(self, request, obj):
        if request.user.is_superuser \
                or has_role(request.user, 'technical_validator') \
                or has_role(request.user, 'legal_validator') \
                or has_role(request.user, 'content_manager'):
            management_object = obj
            key = f'{management_object.identifier}/dataset.zip'
            try:
                s3client.head_object(Bucket=ELG_STORAGE.bucket_name, Key=key)
                response = s3client.generate_presigned_url(
                    'get_object',
                    Params={'Bucket': ELG_STORAGE.bucket_name, 'Key': key},
                    ExpiresIn=10
                )
            except ClientError as e:
                if e.response['Error']['Code'] == "404":
                    messages.warning(request,
                                     f'File {key} was not found.')
                    return None
                else:
                    messages.warning(request,
                                     f'{e.response}')
                    return None
            return response
        return None

    def change_view(self, request, object_id, form_url='', extra_context=None):
        obj = self.model.objects.get(id=object_id)
        if obj.status == 'i':
            messages.warning(request,
                             f'Record has been rejected and is no longer available for validation,'
                             f' you will get notified when the curator resubmits it for publication.')
        extra_context = extra_context or {}
        extra_context['landing_page'] = obj.metadata_record_of_manager.get_display_url()
        if not obj.functional_service and obj.has_content_file:
            extra_context['download_link'] = self.validator_download(request, obj)
        if hasattr(obj.metadata_record_of_manager, 'lt_service') and self.vld_type == 'technical_validator':
            extra_context['lt_service_link'] = f'{settings.DJANGO_URL}/' \
                                               f'catalogue_backend/admin/processing/registeredltservice/' \
                                               f'{obj.metadata_record_of_manager.lt_service.id}/change/'
        return super(ValidationAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

    def has_change_permission(self, request, obj=None):
        if obj:
            return obj.status == 'g'
        return super().has_change_permission(request, obj)

    def save_model(self, request, obj, form, change):
        if form.data['validator_note']:
            obj.validator_notes = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                  f'{form.data["validator_note"]}' \
                                  + f'{" | " + obj.validator_notes if obj.validator_notes else ""}'
        if form.data['review_comment']:
            obj.review_comments = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                  f'{form.data["review_comment"]}' \
                                  + f'{" | " + obj.review_comments if obj.review_comments else ""}'
        if form.data.get('slack_channel'):
            obj.slack_channel = f"{form.data['slack_channel']}"
        if form.data.get('jira_ticket'):
            obj.jira_ticket = f"{form.data['jira_ticket']}"
        super(ValidationAdmin, self).save_model(request, obj, form, change)
        if self.vld_type == 'legal_validator':
            obj.approve(legal_aproval=form.data.get(self.validator_types[self.vld_type]['field']))
        if self.vld_type == 'metadata_validator':
            obj.approve(metadata_aproval=form.data.get(self.validator_types[self.vld_type]['field']))
        if self.vld_type == 'technical_validator':
            obj.approve(technical_aproval=form.data.get(self.validator_types[self.vld_type]['field']))
        record = Manager.objects.get(pk=obj.pk)
        if record.status in [INTERNAL]:
            MyResourcesDocument().update(record)
        if record.status in [PUBLISHED]:
            MyResourcesDocument().update(record)
            MetadataDocument().catalogue_update(record.metadata_record_of_manager)
            CatalogReportDocument().catalogue_report_update(record)
        MyValidationsDocument().update(record)


class LegalValidationAdmin(ValidationAdmin):
    """
    Legal Validation Admin
    """
    readonly_fields = ('review_comments', 'validator_notes', 'slack_channel', 'jira_ticket', 'record_curator',
                       'technically_valid', 'technical_validator_assigned',
                       'metadata_valid', 'metadata_validator_assigned')

    def technical_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.technical_validator.username} <{obj.technical_validator.email}>'

    def metadata_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.metadata_validator.username} <{obj.metadata_validator.email}>'

    def record_curator(self, obj=None):
        if obj:
            return f'{obj.curator.first_name} {obj.curator.last_name} <{obj.curator.email}>'

    def get_form(self, request, obj=None, **kwargs):
        return super().get_form(request, vld_type='legal_validator')

    def get_queryset(self, request, **kwargs):
        return super(LegalValidationAdmin, self).get_queryset(request, vld_type='legal_validator')


class MetadataValidationAdmin(ValidationAdmin):
    """
    Metadata Validation Admin
    """
    readonly_fields = ('review_comments', 'validator_notes', 'slack_channel', 'jira_ticket', 'record_curator')

    def get_fields(self, request, obj=None):
        fields = super().get_fields(request, obj)
        if not obj.has_content_file:
            fields = list(fields)
            fields.remove('technically_valid')
            fields = tuple(fields)
        return fields

    def technical_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.technical_validator.username} <{obj.technical_validator.email}>'

    def legal_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.legal_validator.username} <{obj.legal_validator.email}>'

    def record_curator(self, obj=None):
        if obj:
            return f'{obj.curator.first_name} {obj.curator.last_name} <{obj.curator.email}>'

    def get_form(self, request, obj=None, **kwargs):
        return super().get_form(request, vld_type='metadata_validator')

    def get_queryset(self, request, **kwargs):
        return super(MetadataValidationAdmin, self).get_queryset(request, vld_type='metadata_validator')


class TechnicalValidationAdmin(ValidationAdmin):
    """
    Technical Validation Admin
    """
    readonly_fields = ('review_comments', 'validator_notes', 'execution_location', 'docker_download_location',
                       'service_adapter_download_location', 'record_curator', 'metadata_valid',
                       'metadata_validator_assigned', 'legally_valid', 'legal_validator_assigned')

    def get_fields(self, request, obj=None):
        fields = super().get_fields(request, obj)
        if (not (hasattr(obj.metadata_record_of_manager, 'lt_service') or obj.has_content_file)
            and obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService') \
                or (not obj.has_content_file
                    and obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type != 'ToolService'):
            fields = list(fields)
            fields.remove('metadata_valid')
            fields = tuple(fields)
        return fields

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields = list(readonly_fields)
        if hasattr(obj.metadata_record_of_manager, 'lt_service') or obj.has_content_file:
            readonly_fields.remove('metadata_valid')
        if not obj.functional_service and obj.has_content_file:
            readonly_fields.remove('execution_location')
            readonly_fields.remove('docker_download_location')
            readonly_fields.remove('service_adapter_download_location')
        readonly_fields = tuple(readonly_fields)
        return readonly_fields

    def execution_location(self, obj=None):
        if obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record_of_manager.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    return software_distribution.execution_location
                else:
                    return '-'
        else:
            return '-'

    def docker_download_location(self, obj=None):
        if obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record_of_manager.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    if software_distribution.docker_download_location:
                        return software_distribution.docker_download_location
                    else:
                        return '-'
        else:
            return '-'

    def service_adapter_download_location(self, obj=None):
        if obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record_of_manager.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    if software_distribution.service_adapter_download_location:
                        return software_distribution.service_adapter_download_location
                    else:
                        return '-'
        else:
            return '-'

    def metadata_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.metadata_validator.username} <{obj.metadata_validator.email}>'

    def legal_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.legal_validator.username} <{obj.legal_validator.email}>'

    def record_curator(self, obj=None):
        if obj:
            return f'{obj.curator.first_name} {obj.curator.last_name} <{obj.curator.email}>'

    def get_form(self, request, obj=None, **kwargs):
        return super().get_form(request, vld_type='technical_validator')

    def get_queryset(self, request, **kwargs):
        return super(TechnicalValidationAdmin, self).get_queryset(request, vld_type='technical_validator')


class ValidatorAdmin(admin.ModelAdmin):

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class Merge(DescribedEntity):
    class Meta:
        verbose_name = 'Merge Entity'
        verbose_name_plural = 'Merge Entities'
        proxy = True


class MergeAdmin(ReadOnlyModelAdmin):
    list_display = [
        'source_name', 'target_name', '_state'
    ]

    actions = ['merge']

    change_list_template = 'admin/management/csv_changelist.html'

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
            path('report-download/', self.report_download),
        ]
        return my_urls + urls

    def import_csv(self, request):
        if request.method == "POST":
            conflicts = []
            errors = []
            issues = []
            proceed = []
            fieldnames = ['source', 'target']
            csv_file = request.FILES["csv_file"]
            decoded_file = csv_file.read().decode('utf-8')
            io_string = io.StringIO(decoded_file)
            reader = csv.DictReader(io_string, delimiter=',')
            if not len(set(fieldnames).intersection(set(reader.fieldnames))) == len(set(fieldnames)):
                self.message_user(request,
                                  f"Please make sure all of the following headers exist in your file\n {fieldnames}")
                return HttpResponseRedirect(request.get_full_path())
            for row in reader:
                pk_target = row.get('target')
                pk_source = row.get('source')
                if pk_target and pk_source:
                    try:
                        pk_target = int(pk_target)
                        pk_source = int(pk_source)
                    except ValueError:
                        pass
                    if not isinstance(pk_target, int) or not isinstance(pk_source, int):
                        self.message_user(request,
                                          f"Please make sure all inputs are integers.")
                        return HttpResponseRedirect(request.get_full_path())
                    try:
                        _target = DescribedEntity.objects.get(pk=pk_target)
                        _source = DescribedEntity.objects.get(pk=pk_source)
                    except ObjectDoesNotExist:
                        issues.append({'source': pk_source,
                                       'target': pk_target,
                                       'error': f"One or both IDs from the pair {pk_target}, {pk_source} don't match an entity."})
                        continue
                    if not _target.entity_type == _source.entity_type:
                        issues.append({'source': pk_source,
                                       'target': pk_target,
                                       'error': f"Pairs match different entities."})
                        continue
                    if {_target.entity_type, _source.entity_type}.intersection({'Person', 'Document',
                                                                                'Repository', 'Group',
                                                                                'LanguageResource'}):
                        errors.append({'source': pk_source,
                                       'target': pk_target,
                                       'error': f"Functionality not available for merging yet"})
                        continue
                    target_mdr = getattr(_target, 'metadata_record_of_described_entity', None)
                    source_mdr = getattr(_source, 'metadata_record_of_described_entity', None)
                    if target_mdr and target_mdr.management_object.deleted:
                        errors.append({'source': pk_source,
                                       'target': pk_target,
                                       'error': f"Target is deleted."})
                        continue
                    if any(
                            [not target_mdr and not source_mdr,
                             target_mdr and not source_mdr]
                    ):
                        proceed.append({'source': pk_source,
                                        'target': pk_target})
                    elif not target_mdr and source_mdr:
                        conflicts.append({'source': pk_source,
                                          'target': pk_target,
                                          'conflict': 'Full - Generic'})
                    elif target_mdr and source_mdr:
                        conflicts.append({'source': pk_source,
                                          'target': pk_target,
                                          'conflict': 'Full - Full'})
            csv_file.close()
            request.session['_old_post'] = {'conflicts': conflicts,
                                            'errors': errors,
                                            'proceed': proceed}
            return HttpResponseRedirect(mark_safe(
                ".."
            ))
        form = CsvImportForm()
        payload = {"form": form}
        return render(
            request, "management/csv_form.html", payload
        )

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['title'] = 'Merge entities'
        return super().changelist_view(request, extra_context=extra_context)

    def get_session_data(self):
        sesh = self.request.session.get('_old_post', {})
        return sesh

    def get_queryset(self, request):
        qs = super(MergeAdmin, self).get_queryset(request)
        self.request = request
        sesh = self.get_session_data()
        print('sesh', sesh)
        source_pks = []
        if sesh:
            for _k, _v in sesh.items():
                if _k != 'issues':
                    for _d in _v:
                        source_pks.append(_d['source'])
            print('source', source_pks)
            qs = DescribedEntity.objects.filter(pk__in=source_pks)
            print(qs)
            return qs
        else:
            return DescribedEntity.objects.none()

    def source_name(self, obj):
        source_name = obj.__str__()
        source_pk = obj.pk
        source_record = 'Generic'
        full_record = getattr(obj, 'metadata_record_of_described_entity', None)
        if full_record:
            source_record = f'Full record (ID: {full_record.pk})'
        return_str = f'{source_name} - Entity ID: {source_pk} - {source_record}'
        return return_str

    def target_name(self, obj):
        sesh = self.request.session.get('_old_post')
        print(sesh)
        target_name = ''
        for _k, _v in sesh.items():
            print(_v)
            for _d in _v:
                if _d['source'] == obj.pk:
                    target = DescribedEntity.objects.get(pk=_d['target'])
                    target_name = target.__str__()
                    target_pk = target.pk
                    target_record = 'Generic'
                    full_record = getattr(target, 'metadata_record_of_described_entity', None)
                    if full_record:
                        target_record = f'Full record (ID: {full_record.pk})'
                    return_str = f'{target_name} - Entity ID: {target_pk} - {target_record}'
                    return return_str
        return target_name

    def _state(self, obj):
        sesh = self.request.session.get('_old_post')
        print('ss', sesh)
        state = ''
        for _k, _v in sesh.items():
            if _k == 'proceed':
                for _d in _v:
                    if _d['source'] == obj.pk:
                        return 'Proceed'
            elif _k == 'conflicts':
                for _d in _v:
                    if _d['source'] == obj.pk:
                        conflict = _d['conflict']
                        return f'Conflict [{conflict}]'
            elif _k == 'errors':
                for _d in _v:
                    if _d['source'] == obj.pk:
                        error = _d['error']
                        return f'Error [{error}]'
        return state

    def report_download(self, request):
        sesh = request.session.get('_old_post')
        error_messages = []
        for _k, _v in sesh.items():
            if _k in ['issues', 'errors']:
                for _d in _v:
                    error_messages.append(_d)
        header = ['source', 'target', 'error']
        output = io.StringIO()
        csv_writer = csv.writer(output, delimiter='\t')
        csv_writer.writerow(header)
        for _error in error_messages:
            source = _error.get('source', '')
            target = _error.get('target', '')
            error = _error.get('error', '')
            csv_writer.writerow([
                source,
                target,
                error
            ])
        output.seek(0)
        response = HttpResponse(output.read(), content_type='text/tab-separated-values')
        output.close()
        response['Content-Disposition'] = "attachment; filename={}.tsv".format('Merging error report')
        return response

    def merge(self, request, queryset):
        sesh = request.session.get('_old_post')
        all_valid_pairs = []
        for _k, _v in sesh.items():
            if _k not in ['issues', 'errors']:
                all_valid_pairs.extend(_v)
        source_pks = queryset.values_list('pk', flat=True)
        pks_to_merge = [pair for pair in all_valid_pairs if pair['source'] in source_pks]
        initiate_entity_merge.apply_async(args=[pks_to_merge, request.user.username])
        request.session.pop('_old_post')
        self.get_queryset(request)

    merge.short_description = 'Merge entities'


class Curation(DescribedEntity):
    class Meta:
        verbose_name = 'Update Element Value'
        verbose_name_plural = 'Update Element Values'
        proxy = True


class CurationAdmin(ReadOnlyModelAdmin):
    list_display = [
        'get_str'
    ]

    actions = ['curation']

    change_list_template = 'admin/management/curation_csv_changelist.html'

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
            path('csv-download/', self.csv_download),
        ]
        return my_urls + urls

    def import_csv(self, request):
        form = CurationCsvImportForm(
            initial={admin.ACTION_CHECKBOX_NAME: request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})
        if request.method == "POST":
            issues = []
            action = request.POST.get('action')
            removal = False
            if action == 'Remove':
                removal = True
            model_type = request.POST.get('model_type')
            if model_type not in model_dict.keys():
                self.message_user(request,
                                  f"Functionality for model {model_type} is not supported.")
                return HttpResponseRedirect(request.get_full_path())
            serializer = serializer_dict[model_type]
            fieldnames = ['entity_pk'] + list(serializer.Meta.fields)
            curation_csv_file = request.FILES["curation_csv_file"]
            curation_string = curation_csv_file.read().decode('utf-8')
            curation_file = io.StringIO(curation_string)
            reader = csv.DictReader(curation_file, delimiter=',')
            if not set(reader.fieldnames).issubset(set(fieldnames)):
                self.message_user(request,
                                  f"Please make sure there are no headers in your "
                                  f"file that don't belong in the following list \n {fieldnames}")
                return HttpResponseRedirect(request.get_full_path())
            feed_list = []
            for row in reader:
                model_id = row.get('pk')
                if model_id:
                    try:
                        int(model_id)
                        feed_list.append(row)
                    except ValueError:
                        issues.append(row)
            curation_file.close()
            request.session['_old_post'] = {'issues': issues,
                                            'feed_list': feed_list,
                                            'model_type': model_type,
                                            'removal': removal}
            return HttpResponseRedirect(mark_safe(
                ".."
            ))
        payload = {"form": form}
        return render(
            request, "management/curation_csv_form.html", payload
        )

    def csv_download(self, request):
        form = CurationCsvExportForm(
            initial={admin.ACTION_CHECKBOX_NAME: request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})
        if request.method == "POST":
            record_ids = request.POST.get('record_id')
            try:
                record_pks = [int(_pk.strip()) for _pk in record_ids.split(',')]
                records = MetadataRecord.objects.filter(pk__in=record_pks)
            except ObjectDoesNotExist:
                self.message_user(request,
                                  f"At least one of the requested records with ids {record_ids} was not found.")
                return HttpResponseRedirect(request.get_full_path())
            model_type = request.POST.get('model_type')
            if model_type not in model_dict.keys():
                self.message_user(request,
                                  f"Functionality for model {model_type} is not supported.")
                return HttpResponseRedirect(request.get_full_path())
            record_dict = {}
            for record in records:
                gt = GetRelatedRecords(record)
                children = gt.get_children_pks_of_instance()
                relations = [rel[1] for rel in children if rel[2] == model_type]
                record_dict.update({record: relations})

            serializer = serializer_dict[model_type]
            fieldnames = ['record_pk', 'entity_pk'] + list(serializer.Meta.fields)
            output = io.StringIO()
            csv_writer = csv.writer(output)
            csv_writer.writerow(fieldnames)
            final_write_list = []
            for record, relations in record_dict.items():
                for rel in relations:
                    write_list = [record.pk, record.described_entity.pk]
                    rel_dict = serializer(instance=rel).data
                    for field_name in fieldnames[2:]:
                        write_list.append(rel_dict[field_name])
                    if write_list not in final_write_list:
                        final_write_list.append(write_list)
            for f_w_l in final_write_list:
                csv_writer.writerow(f_w_l)
            output.seek(0)
            response = HttpResponse(output.read(), content_type='text/comma-separated-values')
            output.close()
            response['Content-Disposition'] = "attachment; filename={}.csv".format(
                f'{model_type.split(".")[-1]}_of_PK_{"_".join([str(_pk) for _pk in record_pks])}')
            return response
        payload = {"form": form}
        return render(
            request, "management/curation_export_csv_form.html", payload
        )

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['title'] = 'Curate model'
        return super().changelist_view(request, extra_context=extra_context)

    def get_session_data(self):
        sesh = self.request.session.get('_old_post', {})
        return sesh

    def get_queryset(self, request):
        qs = super(CurationAdmin, self).get_queryset(request)
        self.request = request
        sesh = self.get_session_data()
        print('sesh', sesh)
        row_pks = []
        if sesh:
            model_type = sesh.get('model_type')
            model = model_dict[model_type]
            for _k, _v in sesh.items():
                if _k == 'feed_list':
                    for _d in _v:
                        row_pks.append(_d['pk'])
            print('source', row_pks)
            qs = model.objects.filter(pk__in=row_pks)
            print(qs)
            return qs
        else:
            return DescribedEntity.objects.none()

    def get_str(self, obj):
        return obj.__str__()

    def curation(self, request, queryset):
        sesh = request.session.get('_old_post')
        all_valid_rows = sesh['feed_list']
        LOGGER.info(f'All valid rows {all_valid_rows}')
        issues = sesh['issues']
        removal = sesh['removal']
        LOGGER.info(f'Issues {issues}')
        rows_pks = queryset.values_list('pk', flat=True)
        LOGGER.info(f'Model pks {rows_pks}')
        chosen_rows_to_curate = [_l for _l in all_valid_rows if int(_l['pk']) in rows_pks]
        chosen_rows_to_curate.extend(issues)
        model_type = sesh.get('model_type')
        LOGGER.info(f'CSV :: {chosen_rows_to_curate}')
        LOGGER.info(f'Model type :: {model_type}')
        initiate_curation.apply_async(args=[chosen_rows_to_curate, model_type, request.user.username, removal])
        request.session.pop('_old_post')
        self.get_queryset(request)

    get_str.short_description = 'Model name'
    curation.short_description = 'Curate model'


admin.site.register(Manager, ManagerAdmin)
# TODO: Deprecated, evaluate for removal along with accompanying admin model code
# admin.site.register(LegalValidation, LegalValidationAdmin)
# admin.site.register(TechnicalValidation, TechnicalValidationAdmin)
# admin.site.register(MetadataValidation, MetadataValidationAdmin)
admin.site.register(Validator, ValidatorAdmin)
admin.site.register(Merge, MergeAdmin)
admin.site.register(Curation, CurationAdmin)
