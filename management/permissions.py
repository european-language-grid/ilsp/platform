from django.core.exceptions import ObjectDoesNotExist
from rest_framework.permissions import BasePermission, IsAuthenticated
from rolepermissions.checkers import has_role

from accounts.elg_permissions import is_owner
from management.models import PUBLISHED, DRAFT, INTERNAL, INGESTED
from registry.models import MetadataRecord


class DownloadDatasetPermission(BasePermission):
    """
    1. provider: Allows access for its own record independent of record status
    2. content manager or admin: Allows access independent of record status
    3. anonymousUser or authenticateUser: Allows access for status = [PUBLISHED]
    """

    def has_permission(self, request, view):
        try:
            obj_pk = view.kwargs.get('pk', view.kwargs.get('metadata_record_of_manager__pk', None))
            obj = MetadataRecord.objects.get(id=obj_pk)
            if obj.management_object.deleted:
                if request.user.is_superuser or has_role(request.user, 'content_manager'):
                    return True
                return False
            if obj.management_object.status == PUBLISHED:
                return True
            return bool(
                request.user and
                (
                        request.user.is_superuser or
                        has_role(request.user, 'content_manager') or
                        (
                                has_role(request.user, 'provider') and
                                obj.management_object.curator == request.user
                        ) or
                        (
                                obj.management_object.status == INGESTED and
                                (
                                        (has_role(request.user,
                                                  'legal_validator') and obj.management_object.legal_validator == request.user) or
                                        (has_role(request.user,
                                                  'metadata_validator') and obj.management_object.metadata_validator == request.user) or
                                        (has_role(request.user,
                                                  'technical_validator') and obj.management_object.technical_validator == request.user)
                                )
                        )
                )
            )
        except ObjectDoesNotExist:
            return False


class TechnicalValidationPermission(IsAuthenticated):
    """
    The request is authenticated as a superuser or content administrator.
    """

    def has_permission(self, request, view):
        try:
            if view.kwargs.get('pk', None):
                resource = MetadataRecord.objects.get(id=view.kwargs.get('pk', None))
            else:
                pk = request.query_params.get('record_id__in', None)
                resource = MetadataRecord.objects.get(id=pk)
            return bool(
                request.user and
                request.user.is_authenticated and
                (request.user.is_superuser or
                 has_role(request.user, 'content_manager') or
                 (has_role(request.user, 'technical_validator')
                  and resource.management_object.functional_service
                  and resource.management_object.metadata_validator == resource.management_object.technical_validator == request.user) or
                 (has_role(request.user, 'metadata_validator')
                  and resource.management_object.has_content_file
                  and resource.management_object.metadata_validator == resource.management_object.technical_validator == request.user)
                 )
            )
        except ObjectDoesNotExist:
            return False


class BatchTechnicalValidationPermission(IsAuthenticated):
    """
    The request is authenticated as a superuser or content administrator.
    """

    def has_permission(self, request, view):
        try:
            has_access = False
            if view.kwargs.get('pk', None):
                resource = MetadataRecord.objects.get(id=view.kwargs.get('pk', None))
                if resource.management_object.functional_service and \
                        resource.management_object.metadata_validator == resource.management_object.technical_validator == request.user:
                    has_access = True
            else:
                pk = request.query_params.get('record_id__in', None).split(',')
                pk_list = [int(_pk) for _pk in pk if _pk]
                resources = MetadataRecord.objects.filter(id__in=pk_list)
                has_access = False
                for resource in resources:
                    if resource.management_object.functional_service and \
                            resource.management_object.metadata_validator == resource.management_object.technical_validator == request.user:
                        has_access = True
                        break
            return bool(
                request.user and
                request.user.is_authenticated and
                (request.user.is_superuser or
                 has_role(request.user, 'content_manager') or
                 (has_role(request.user, 'technical_validator')
                  and has_access)
                 )
            )
        except ObjectDoesNotExist:
            return False


class UnpublishViewPermissions(IsAuthenticated):
    """
    The request is authenticated as a superuser or content administrator.
    """

    def has_permission(self, request, view):
        obj_pk = request.query_params.get('record_id__in', 0).strip()
        obj_pk_list = obj_pk.split(',')
        try:
            owner = False
            under_construction = False
            if obj_pk_list != [0]:
                resources = MetadataRecord.objects.filter(id__in=obj_pk_list)
                for resource in resources:
                    if is_owner(request.user, resource):
                        owner = True
                    if resource.management_object.under_construction:
                        under_construction = True
            else:
                owner = True
                under_construction = True
            return bool(
                request.user and
                request.user.is_authenticated and
                (request.user.is_superuser or
                 has_role(request.user, 'content_manager')
                 or (owner and under_construction))
            )
        except ObjectDoesNotExist:
            return False
