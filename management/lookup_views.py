import logging
from rest_framework import (
    generics, status
)
from rest_framework.response import Response

from accounts import elg_permissions

from django.conf import settings
from . import serializers
from .models import Manager

LOGGER = logging.getLogger(__name__)



class CuratorLookupView(generics.ListAPIView):
    """
    List view of current providers
    """
    permission_classes = [elg_permissions.IsSuperUserOrProviderOrContentManager]
    serializer_class = serializers.CuratorSerializer

    def get_queryset(self):
        return Manager.objects.filter(
            deleted=False
        )

    def list(self, request, *args, **kwargs):
        if 'curator_id' in request.GET:
            queryset = self.get_queryset().filter(curator__pk=request.query_params['curator_id']).first()
            serializer = self.get_serializer(queryset)
            return_dict = {
                'curator_id': serializer.data['curator'],
                'curator_username': serializer.data['curator_username']
            }
            return Response(return_dict, status.HTTP_200_OK)
        return Response({'Query Error': 'Please make a query'}, status=status.HTTP_400_BAD_REQUEST)
