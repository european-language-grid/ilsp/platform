import logging

from django.db.models import QuerySet
from django_elasticsearch_dsl import Index, fields
from django_elasticsearch_dsl.documents import DocType
from elasticsearch_dsl import analyzer
from language_tags import tags

from management.management_dashboard_index.documents import map_resource_type
from management.models import CatalogueReport
# Name of the Elasticsearch index
from registry.choices import (
    CORPUS_SUBCLASS_CHOICES, LCR_SUBCLASS_CHOICES, LINGUALITY_TYPE_CHOICES, MULTILINGUALITY_TYPE_CHOICES,
    DATASET_DISTRIBUTION_FORM_CHOICES, ENCODING_LEVEL_CHOICES, LD_SUBCLASS_CHOICES, LT_CLASS_RECOMMENDED_CHOICES,
    PROCESSING_RESOURCE_TYPE_CHOICES, MEDIA_TYPE_CHOICES, ANNOTATION_TYPE_RECOMMENDED_CHOICES,
    SOFTWARE_DISTRIBUTION_FORM_CHOICES
)
from registry.models import MetadataRecord
from registry.utils.checks import is_tool_service
from registry.utils.retrieval_utils import retrieve_default_lang
from utils.cv_choice_extraction import extract_data_from_cv
from utils.string_utils import normalize_space

LOGGER = logging.getLogger(__name__)

catalog_report = Index('catalog_report_index')
# See Elasticsearch Indices API reference for available settings
catalog_report.settings(
    number_of_shards=4,
    number_of_replicas=2
)

html_strip = analyzer(
    'html_strip',
    tokenizer='standard',
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)


def _check_language_list(part, language_list=None, not_in_language_list=False):
    result = list()
    for lang in part.language.all():
        if language_list is None:
            result.extend([tags.description(lang.language_id)[0]
                           for lang in part.language.all()])
        else:
            if not_in_language_list:
                if tags.description(lang.language_id)[0] not in language_list:
                    result.extend([tags.description(lang.language_id)[0]])
            else:
                if tags.description(lang.language_id)[0] in language_list:
                    result.extend([tags.description(lang.language_id)[0]])
    return result


def _from_language_list(instance, language_list=None, not_in_language_list=False):
    # handle different cases, based on resource_type
    result = list()
    if instance.described_entity.entity_type == 'LanguageResource':
        resource_type = instance.described_entity.lr_subclass.lr_type
        try:
            if resource_type == 'Corpus':
                for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                    if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                        continue
                    result_lang = _check_language_list(media_part, language_list, not_in_language_list)
                    if result_lang:
                        result.extend(result_lang)
            elif resource_type == 'LexicalConceptualResource':
                for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                    result_lang = _check_language_list(media_part, language_list, not_in_language_list)
                    if result_lang:
                        result.extend(result_lang)
            elif resource_type == 'LanguageDescription':
                for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                    result_lang = _check_language_list(media_part, language_list, not_in_language_list)
                    if result_lang:
                        result.extend(result_lang)
            elif resource_type == 'ToolService':
                for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                    result_lang = _check_language_list(input_resource, language_list, not_in_language_list)
                    if result_lang:
                        result.extend(result_lang)
                for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                    result_lang = _check_language_list(output_resource, language_list, not_in_language_list)
                    if result_lang:
                        result.extend(result_lang)
        except IndexError:
            return result
    return list(set(result))


@catalog_report.doc_type
class CatalogReportDocument(DocType):
    class Django:
        model = CatalogueReport  # The model associated with this DocType

    id = fields.IntegerField(attr='id')
    resource_name = fields.TextField()
    resource_short_name = fields.TextField()
    landing_page = fields.TextField()
    source_of_metadata_record = fields.TextField()
    publication_date = fields.DateField()
    resource_type = fields.TextField()
    subclass = fields.TextField()
    version = fields.TextField()
    domain = fields.TextField(multi=True)
    keywords = fields.TextField(multi=True)
    subject = fields.TextField(multi=True)
    linguality_type = fields.TextField(multi=True)
    multilinguality_type = fields.TextField(multi=True)
    languages = fields.TextField(multi=True)
    dataset_distribution_form = fields.TextField(multi=True)
    licence_terms = fields.TextField(multi=True)
    access_rights = fields.TextField(multi=True)
    data_format = fields.TextField(multi=True)
    time_coverage = fields.TextField(multi=True)
    geographic_coverage = fields.TextField(multi=True)
    register = fields.TextField(multi=True)
    text_type = fields.TextField(multi=True)
    text_genre = fields.TextField(multi=True)
    audio_genre = fields.TextField(multi=True)
    speech_genre = fields.TextField(multi=True)
    annotation_type = fields.TextField(multi=True)
    metalanguage = fields.TextField(multi=True)
    encoding_level = fields.TextField(multi=True)
    ml_framework = fields.TextField()
    function = fields.TextField(multi=True)
    language_independent = fields.BooleanField()
    input_processing_type = fields.TextField(multi=True)
    input_media_type = fields.TextField(multi=True)
    input_languages = fields.TextField(multi=True)
    input_annotation_types = fields.TextField(multi=True)
    output_processing_type = fields.TextField(multi=True)
    output_media_type = fields.TextField(multi=True)
    output_languages = fields.TextField(multi=True)
    output_annotation_types = fields.TextField(multi=True)
    software_distribution_form = fields.TextField(multi=True)
    elg_compatible = fields.BooleanField()
    for_information = fields.BooleanField()
    hosted = fields.BooleanField()
    # version_date = fields.TextField()
    # funding_project = fields.TextField(multi=True)
    # intended_applications = fields.TextField(multi=True)
    # additional_info = fields.TextField(multi=True)

    metadata_record = None

    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        return CatalogueReport.objects.filter(
            metadata_record_of_manager__described_entity__entity_type='LanguageResource',
            metadata_record_of_manager__isnull=False, deleted=False, status='p'
        )

    def catalogue_report_update(self, thing, refresh=None, action="index", raise_on_error=None, **kwargs):
        """
                Update each document in ES for a model, iterable of models or queryset
                """
        if action == "index":
            if isinstance(thing, MetadataRecord):
                thing = self.get_queryset().filter(pk=thing.pk)
            elif isinstance(thing, QuerySet):
                thing = self.get_queryset().intersection(thing)
            elif isinstance(thing, list):
                thing = self.get_queryset().filter(pk__in=[instance.pk for instance in thing])
        try:
            if raise_on_error:
                result = super().update(thing, refresh, action=action, raise_on_error=raise_on_error, **kwargs)
            else:
                result = super().update(thing, refresh, action, **kwargs)
        except Exception:
            LOGGER.exception('Error in elastic indexer update')
        else:
            return result

    # PREPARE FIELDS
    def prepare_id(self, instance):
        self.metadata_record = instance.metadata_record_of_manager
        try:
            return instance.metadata_record_of_manager.pk
        except MetadataRecord.DoesNotExist:
            return None

    def prepare_resource_name(self, instance):
        if self.metadata_record.described_entity.entity_type == 'LanguageResource':
            return normalize_space(retrieve_default_lang(self.metadata_record.described_entity.resource_name))

    def prepare_resource_short_name(self, instance):
        if self.metadata_record.described_entity.entity_type == 'LanguageResource' and \
                self.metadata_record.described_entity.resource_short_name:
            return normalize_space(retrieve_default_lang(self.metadata_record.described_entity.resource_short_name)) \
                or None
        return None

    def prepare_source_of_metadata_record(self, instance):
        if self.metadata_record.source_of_metadata_record:
            return retrieve_default_lang(self.metadata_record.source_of_metadata_record.repository_name)
        return None

    def prepare_publication_date(self, instance):
        if instance.publication_date is not None:
            return instance.publication_date.strftime("%Y-%m-%d")
        return None

    def prepare_resource_type(self, instance):
        if self.metadata_record.described_entity.entity_type == 'LanguageResource':
            return map_resource_type(self.metadata_record.described_entity.lr_subclass.lr_type)
        return self.metadata_record.described_entity.entity_type

    def prepare_version(self, instance):
        if self.metadata_record.described_entity.entity_type == 'LanguageResource':
            return self.metadata_record.described_entity.version

    def prepare_domain(self, instance):
        if self.metadata_record.described_entity.entity_type == 'LanguageResource':
            if self.metadata_record.described_entity.domain:
                return [
                    retrieve_default_lang(domain.category_label)
                    for domain in self.metadata_record.described_entity.domain.all()
                ]
        return list()

    def prepare_keywords(self, instance):
        try:
            result = list()
            if self.metadata_record.described_entity.keyword is None:
                return []
            for k_dict in self.metadata_record.described_entity.keyword:
                result.extend(k_dict.values())
            return result
        except AttributeError:
            return []

    def prepare_subject(self, instance):
        if self.metadata_record.described_entity.entity_type == 'LanguageResource':
            if self.metadata_record.described_entity.subject:
                return [
                    retrieve_default_lang(subject.category_label)
                    for subject in self.metadata_record.described_entity.subject.all()
                ]
        return list()

    def prepare_subclass(self, instance):
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.lr_type == 'Corpus':
                return CORPUS_SUBCLASS_CHOICES[self.metadata_record.described_entity.lr_subclass.corpus_subclass]
            elif self.metadata_record.described_entity.lr_subclass.lr_type == 'LexicalConceptualResource':
                try:
                    return LCR_SUBCLASS_CHOICES[self.metadata_record.described_entity.lr_subclass.lcr_subclass]
                except KeyError:
                    return None
            elif self.metadata_record.described_entity.lr_subclass.lr_type == 'LanguageDescription':
                if getattr(self.metadata_record.described_entity.lr_subclass, 'language_description_subclass'):
                    return self.metadata_record.described_entity.lr_subclass.language_description_subclass.ld_subclass_type
                else:
                    return LD_SUBCLASS_CHOICES.__getitem__(
                        self.metadata_record.described_entity.lr_subclass.ld_subclass)
        return None

    def prepare_linguality_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if not is_tool_service(self.metadata_record):
            resource_type = self.metadata_record.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    for media_part in self.metadata_record.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
                elif resource_type == 'LexicalConceptualResource':
                    for media_part in self.metadata_record.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
                elif resource_type == 'LanguageDescription':
                    for media_part in self.metadata_record.described_entity.lr_subclass.language_description_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
            except IndexError:
                return result
        return list(set(result))

    def prepare_multilinguality_type(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            try:
                if self.metadata_record.described_entity.lr_subclass.lr_type == 'Corpus':
                    for part in self.metadata_record.described_entity.lr_subclass.corpus_media_part.all():
                        if part.multilinguality_type:
                            result.append(MULTILINGUALITY_TYPE_CHOICES[part.multilinguality_type])
                elif self.metadata_record.described_entity.lr_subclass.lr_type == 'LexicalConceptualResource':
                    for part in self.metadata_record.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if part.multilinguality_type:
                            result.append(MULTILINGUALITY_TYPE_CHOICES[part.multilinguality_type])
                elif self.metadata_record.described_entity.lr_subclass.lr_type == 'LanguageDescription':
                    for part in self.metadata_record.described_entity.lr_subclass.language_description_media_part.all():
                        if part.multilinguality_type:
                            result.append(MULTILINGUALITY_TYPE_CHOICES[part.multilinguality_type])
            except AttributeError:
                return result
        return list(set(result))

    def prepare_languages(self, instance):
        # handle different cases, based on resource_type
        if not is_tool_service(self.metadata_record):
            return _from_language_list(self.metadata_record)
        else:
            return []

    def prepare_dataset_distribution_form(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            try:
                for dataset_distribution in self.metadata_record.described_entity.lr_subclass.dataset_distribution.all():
                    result.append(DATASET_DISTRIBUTION_FORM_CHOICES[dataset_distribution.dataset_distribution_form])
            except AttributeError:
                return result
        return list(set(result))

    def prepare_licence_terms(self, instance):
        # handle different cases, based on resource_type
        result = list()
        resource_type = self.metadata_record.described_entity.lr_subclass.lr_type
        try:
            if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' or \
                    resource_type == 'LanguageDescription':
                for distribution in self.metadata_record.described_entity.lr_subclass.dataset_distribution.all():
                    for licence in distribution.licence_terms.all():
                        result.append(
                            retrieve_default_lang(licence.licence_terms_name))
            elif resource_type == 'ToolService':
                for distribution in self.metadata_record.described_entity.lr_subclass.software_distribution.all():
                    for licence in distribution.licence_terms.all():
                        result.append(
                            retrieve_default_lang(licence.licence_terms_name))
        except (IndexError, AttributeError):
            return list(set(result))
        return list(set(result))

    def prepare_data_format(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            try:
                for dataset_distribution in self.metadata_record.described_entity.lr_subclass.dataset_distribution.all():
                    for dist_text_feat in dataset_distribution.distribution_text_feature.all():
                        result.extend(extract_data_from_cv(dist_text_feat, 'data_format'))
                    for dist_text_num_feat in dataset_distribution.distribution_text_numerical_feature.all():
                        result.extend(extract_data_from_cv(dist_text_num_feat, 'data_format'))
                    for dist_aud_feat in dataset_distribution.distribution_audio_feature.all():
                        result.extend(extract_data_from_cv(dist_aud_feat, 'data_format'))
                    for dist_img_feat in dataset_distribution.distribution_image_feature.all():
                        result.extend(extract_data_from_cv(dist_img_feat, 'data_format'))
                    for dist_vid_feat in dataset_distribution.distribution_video_feature.all():
                        result.extend(extract_data_from_cv(dist_vid_feat, 'data_format'))
            except AttributeError:
                return result
        return list(set(result))

    def prepare_time_coverage(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.time_coverage:
                result.extend(
                    list(tc.values()) for tc in self.metadata_record.described_entity.lr_subclass.time_coverage)
        return result

    def prepare_geographic_coverage(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.geographic_coverage:
                result.extend(list(gc.values()) for gc in
                              self.metadata_record.described_entity.lr_subclass.geographic_coverage)
        return result

    def prepare_register(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            try:
                if self.metadata_record.described_entity.lr_subclass.register:
                    result.extend(
                        list(reg.values()) for reg in self.metadata_record.described_entity.lr_subclass.register)
            except AttributeError:
                return result
        return result

    def prepare_text_type(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.lr_type == 'Corpus':
                for part in self.metadata_record.described_entity.lr_subclass.corpus_media_part.all():
                    try:
                        result.extend(
                            [retrieve_default_lang(text_type.category_label) for text_type in part.text_type.all()])
                    except AttributeError:
                        return result
        return list(set(result))

    def prepare_text_genre(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.lr_type == 'Corpus':
                for part in self.metadata_record.described_entity.lr_subclass.corpus_media_part.all():
                    try:
                        result.extend(
                            [retrieve_default_lang(text_genre.category_label) for text_genre in
                             part.text_genre.all()])
                    except AttributeError:
                        return result
        return list(set(result))

    def prepare_audio_genre(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.lr_type == 'Corpus':
                for part in self.metadata_record.described_entity.lr_subclass.corpus_media_part.all():
                    try:
                        result.extend(
                            [retrieve_default_lang(audio_genre.category_label) for audio_genre in
                             part.audio_genre.all()])
                    except AttributeError:
                        return result
        return list(set(result))

    def prepare_speech_genre(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.lr_type == 'Corpus':
                for part in self.metadata_record.described_entity.lr_subclass.corpus_media_part.all():
                    try:
                        result.extend(
                            [retrieve_default_lang(speech_genre.category_label) for speech_genre in
                             part.speech_genre.all()])
                    except AttributeError:
                        return result
        return list(set(result))

    def prepare_annotation_type(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.lr_type == 'Corpus':
                for media_part in self.metadata_record.described_entity.lr_subclass.corpus_media_part.all():
                    try:
                        for annotation in media_part.annotation.all():
                            result.extend([at for at in annotation.annotation_type])
                    except AttributeError:
                        return result
        return list(set(result))

    def prepare_metalanguage(self, instance):
        result = list()
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.lr_type == 'LexicalConceptualResource':
                for part in self.metadata_record.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                    result.extend([tags.description(lang.language_id)[0] for lang in part.metalanguage.all()])
            elif self.metadata_record.described_entity.lr_subclass.lr_type == 'LanguageDescription':
                for part in self.metadata_record.described_entity.lr_subclass.language_description_media_part.all():
                    result.extend([tags.description(lang.language_id)[0] for lang in part.metalanguage.all()])
        return list(set(result))

    def prepare_encoding_level(self, instance):
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.lr_type == 'LexicalConceptualResource':
                return [ENCODING_LEVEL_CHOICES[encoding_level] for encoding_level in
                        self.metadata_record.described_entity.lr_subclass.encoding_level]
            elif self.metadata_record.described_entity.lr_subclass.lr_type == 'LanguageDescription':
                try:
                    return [ENCODING_LEVEL_CHOICES[encoding_level] for encoding_level in
                            self.metadata_record.described_entity.lr_subclass.language_description_subclass.encoding_level]
                except AttributeError:
                    return list()
        return list()

    def prepare_ml_framework(self, instance):
        if not is_tool_service(self.metadata_record):
            if self.metadata_record.described_entity.lr_subclass.lr_type == 'LanguageDescription':
                try:
                    return self.metadata_record.described_entity.lr_subclass.language_description_subclass.ml_framework
                except AttributeError:
                    return None
        return None

    def prepare_landing_page(self, instance):
        result = list()
        if self.metadata_record.described_entity.entity_type == 'LanguageResource':
            for additional_info in self.metadata_record.described_entity.additional_info.all():
                if additional_info.landing_page:
                    result.append(additional_info.landing_page)
            result.append(self.metadata_record.get_display_url())
        return result

    def prepare_function(self, instance):
        if is_tool_service(self.metadata_record):
            function = []
            for f in self.metadata_record.described_entity.lr_subclass.function:
                try:
                    function.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(f))
                except KeyError:
                    function.append(f)
            return function
        else:
            return []

    def prepare_language_independent(self, instance):
        if is_tool_service(self.metadata_record):
            return not self.metadata_record.described_entity.lr_subclass.language_dependent
        return None

    def prepare_input_processing_type(self, instance):
        result = []
        if is_tool_service(self.metadata_record):
            for input_resource in self.metadata_record.described_entity.lr_subclass.input_content_resource.all():
                try:
                    result.append(PROCESSING_RESOURCE_TYPE_CHOICES.__getitem__(input_resource.processing_resource_type))
                except KeyError:
                    result.append(input_resource.processing_resource_type)
        return list(set(result))

    def prepare_input_media_type(self, instance):
        result = []
        if is_tool_service(self.metadata_record):
            for input_resource in self.metadata_record.described_entity.lr_subclass.input_content_resource.all():
                if input_resource.media_type:
                    try:
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(input_resource.media_type))
                    except KeyError:
                        result.append(input_resource.media_type)
        return list(set(result))

    def prepare_input_languages(self, instance):
        result = list()
        if is_tool_service(self.metadata_record):
            for input_resource in self.metadata_record.described_entity.lr_subclass.input_content_resource.all():
                result_lang = _check_language_list(input_resource, language_list=None, not_in_language_list=None)
                if result_lang:
                    result.extend(result_lang)
        return list(set(result))

    def prepare_input_annotation_types(self, instance):
        result = list()
        if is_tool_service(self.metadata_record):
            for input_resource in self.metadata_record.described_entity.lr_subclass.input_content_resource.all():
                if input_resource.annotation_type:
                    for annotation_type in input_resource.annotation_type:
                        try:
                            result.append(ANNOTATION_TYPE_RECOMMENDED_CHOICES.__getitem__(annotation_type))
                        except KeyError:
                            result.append(annotation_type)
        return list(set(result))

    def prepare_output_processing_type(self, instance):
        result = []
        if is_tool_service(self.metadata_record):
            for output_resource in self.metadata_record.described_entity.lr_subclass.output_resource.all():
                try:
                    result.append(
                        PROCESSING_RESOURCE_TYPE_CHOICES.__getitem__(output_resource.processing_resource_type))
                except KeyError:
                    result.append(output_resource.processing_resource_type)
        return list(set(result))

    def prepare_output_media_type(self, instance):
        result = []
        if is_tool_service(self.metadata_record):
            for output_resource in self.metadata_record.described_entity.lr_subclass.output_resource.all():
                if output_resource.media_type:
                    try:
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(output_resource.media_type))
                    except KeyError:
                        result.append(output_resource.media_type)
        return list(set(result))

    def prepare_output_languages(self, instance):
        result = list()
        if is_tool_service(self.metadata_record):
            for output_resource in self.metadata_record.described_entity.lr_subclass.output_resource.all():
                result_lang = _check_language_list(output_resource, language_list=None, not_in_language_list=None)
                if result_lang:
                    result.extend(result_lang)
        return list(set(result))

    def prepare_output_annotation_types(self, instance):
        result = list()
        if is_tool_service(self.metadata_record):
            for output_resource in self.metadata_record.described_entity.lr_subclass.output_resource.all():
                if output_resource.annotation_type:
                    for annotation_type in output_resource.annotation_type:
                        try:
                            result.append(ANNOTATION_TYPE_RECOMMENDED_CHOICES.__getitem__(annotation_type))
                        except KeyError:
                            result.append(annotation_type)
        return list(set(result))

    def prepare_software_distribution_form(self, instance):
        result = list()
        if is_tool_service(self.metadata_record):
            try:
                for software_distribution in self.metadata_record.described_entity.lr_subclass.software_distribution.all():
                    result.append(SOFTWARE_DISTRIBUTION_FORM_CHOICES[software_distribution.software_distribution_form])
            except AttributeError:
                return result
        return list(set(result))

    def prepare_access_rights(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if is_tool_service(self.metadata_record):
            for distribution in self.metadata_record.described_entity.lr_subclass.software_distribution.all():
                for access_right in distribution.access_rights.all():
                    result.append(
                        retrieve_default_lang(access_right.category_label))
        return list(set(result))

    def prepare_for_information(self, instance):
        return instance.for_information_only

    def prepare_elg_compatible(self, instance):
        if is_tool_service(self.metadata_record):
            return instance.functional_service

    def prepare_hosted(self, instance):
        if is_tool_service(self.metadata_record):
            if instance.functional_service:
                return True
        if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            if instance.has_content_file:
                return True
        # TODO add when needed
        # if instance.described_entity.entity_type == 'LanguageResource' \
        #         and instance.described_entity.lr_subclass in ['LanguageDescription', 'Corpus', 'LexicalConceptualResource'] \
        #         and (instance.management_object.elg_compatible_model
        #              or instance.management_object.service_compliant_dataset):
        #     return 'ELG compatible data resource'
        return False

    # def prepare_version_date(self, instance):
    #     if self.metadata_record.described_entity.entity_type == 'LanguageResource':
    #         return self.metadata_record.described_entity.version_date
    #
    # def prepare_funding_project(self, instance):
    #     if self.metadata_record.described_entity.entity_type == 'LanguageResource':
    #         return [
    #             retrieve_default_lang(project.project_name)
    #             for project in self.metadata_record.described_entity.funding_project.all()
    #         ]
    #
    # def prepare_intended_applications(self, instance):
    #     if self.metadata_record.described_entity.entity_type == 'LanguageResource':
    #         intended_application = []
    #         if not self.metadata_record.described_entity.intended_application:
    #             return []
    #         for application in self.metadata_record.described_entity.intended_application:
    #             try:
    #                 intended_application.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(application))
    #             except KeyError:
    #                 intended_application.append(application)
    #         return intended_application
    #     else:
    #         return []
    #
    # def prepare_additional_info(self, instance):
    #     if self.metadata_record.described_entity.entity_type == 'LanguageResource':
    #         result = list()
    #         for additional_info in self.metadata_record.described_entity.additional_info.all():
    #             if additional_info.landing_page:
    #                 result.append(additional_info.landing_page)
    #             if additional_info.email:
    #                 result.append(additional_info.email)
    #         return result
