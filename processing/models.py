import copy

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from model_utils import Choices

from accounts.models import ELGUser
from registry.models import MetadataRecord

TOOL_TYPE = Choices(
    ('http://w3id.org/meta-share/omtd-share/TextToSpeechSynthesis', 'TTS', 'Text-to-Speech Synthesis'),
    ('http://w3id.org/meta-share/omtd-share/MachineTranslation', 'MT', 'Machine Translation'),
    ('http://w3id.org/meta-share/omtd-share/InformationExtraction', 'IE', 'Information Extraction'),
    ('http://w3id.org/meta-share/omtd-share/SpeechRecognition', 'ASR', 'Speech Recognition'),
    ('http://w3id.org/meta-share/omtd-share/TextCategorization', 'TC', 'Text categorization'),
    ('http://w3id.org/meta-share/omtd-share/ResourceAccess', 'RA', 'Resource access'),
    ('http://w3id.org/meta-share/omtd-share/ImageProcessing', 'IP', 'Image processing'),
)


class RegisteredLTService(models.Model):
    """
    Stores information on LT Services reviewed by and registered with ELG
    """

    class Meta:
        verbose_name = 'LT Service Registration'
        verbose_name_plural = verbose_name

    metadata_record = models.OneToOneField(
        MetadataRecord,
        help_text='The metadata record that describes this LT Service',
        related_name='lt_service',
        null=True,
        on_delete=models.CASCADE,
    )

    tool_type = models.URLField(
        max_length=60,
        choices=TOOL_TYPE,
        null=True,
        blank=True
    )

    elg_execution_location = models.URLField(
        help_text='ELG location of execution of this LT Service',
        null=True,
        blank=True
    )

    initial_elg_execution_location = models.URLField(
        help_text='Initial location of execution of this LT Service',
        null=True,
        blank=True
    )

    docker_download_location = models.CharField(
        help_text='A location where the software in the form of a docker'
                  ' image can be downloaded from',
        max_length=1000,
        blank=True,
    )

    service_adapter_download_location = models.CharField(
        help_text='Τhe URL where the docker image of the service adapter can'
                  ' be downloaded from',
        max_length=1000,
        blank=True,
    )

    openAPI_spec_url = models.URLField(
        help_text='URL to the Open API json file',
        null=True,
        blank=True
    )

    elg_gui_url = models.CharField(
        help_text='URL GUI of the service',
        null=True,
        blank=True,
        max_length=300
    )

    yaml_file = models.URLField(blank=True, null=True)

    all_version_services = models.ManyToManyField(
        'self',
        symmetrical=True,
        blank=True,
        help_text="Identifies the different versions of the record "
                  "When a new version is released, the versions' services "
                  "are cross-linked."
    )

    status = models.CharField(
        choices=[
            ('DEPRECATED', 'Deprecated'),
            ('DELETED', 'Deleted'),
            ('NEW', 'New'),
            ('PENDING', 'Pending'),
            ('COMPLETED', 'Completed')
        ],
        max_length=10,
        default='NEW'
    )

    elg_hosted = models.BooleanField(null=True)
    requires_login = models.BooleanField(default=True)

    accessor_id = models.CharField(
        help_text='A unique accessor id for REST service calls',
        max_length=30,
        null=True,
        blank=True,
        default=None
    )

    def save(self, *args, **kwargs):
        if not self.pk:
            self.initial_elg_execution_location = self.elg_execution_location
        if self.pk:
            self.link_versioned_services()
        self.full_clean()
        super().save()
        if not self.pk:
            self.link_versioned_services()
        if self.status == 'COMPLETED':
            from registry.search_indexes.documents import MetadataDocument
            record_manager = self.metadata_record.management_object
            record_manager.requires_login = self.requires_login
            initial_proxied_value = copy.deepcopy(record_manager.proxied)
            if self.elg_hosted:
                record_manager.proxied = False
            elif self.elg_hosted is not None and not self.elg_hosted:
                record_manager.proxied = True
            record_manager.save()
            MetadataDocument().catalogue_update(self.metadata_record)
            if record_manager.status == 'p' \
                    and initial_proxied_value != record_manager.proxied:
                record_manager.generate_landing_page_display()

    def full_clean(self, exclude=None, validate_unique=True):
        if self.metadata_record.described_entity.entity_type != 'LanguageResource':
            raise ValidationError('This metadata record does not describe a Tool/Service')
        else:
            if self.metadata_record.described_entity.lr_subclass.lr_type != 'ToolService':
                raise ValidationError('This metadata record does not describe a Tool/Service')

        version_pks = []
        non_version_services = []
        version_accessor_id = set()
        if self.status in ['COMPLETED', 'Completed']:
            version_pks = list(self.all_version_services.values_list('pk', flat=True))
            version_pks.append(self.pk)
            non_version_services = RegisteredLTService.objects.filter(
                accessor_id=self.accessor_id,
                metadata_record__management_object__deleted=False).exclude(
                Q(id__in=version_pks) | Q(status='DELETED')
            )
            version_accessor_id = set(self.all_version_services.filter(status__in=['COMPLETED', 'Completed']).values_list(
                'accessor_id', flat=True))
        if self.status in ['COMPLETED', 'Completed'] \
                and self.accessor_id \
                and non_version_services.exists():
            raise ValidationError('Accessor id is unique to versions of the same record.')
        if self.status in ['COMPLETED', 'Completed'] \
                and len(version_accessor_id) > 1 \
                or (len(version_accessor_id) == 1
                    and self.accessor_id not in version_accessor_id):
            md_version_pks = list(self.all_version_services.values_list('metadata_record__pk', flat=True))
            raise ValidationError(f'The accessor id for all versions of this record needs to be tha same. '
                                  f'IDs of other versions of this service: {md_version_pks}')
        if self.status in ['COMPLETED', 'Completed']:
            if self.initial_elg_execution_location == self.elg_execution_location:
                raise ValidationError('ELG execution location has not been filled in properly.')
        if self.status in ['COMPLETED', 'Completed'] \
                and not self.accessor_id:
            raise ValidationError('Accessor id is required')

    def link_versioned_services(self):
        version_records_ids = self.metadata_record.management_object.versioned_record_managers.filter(
            metadata_record_of_manager__lt_service__isnull=False
        ).values_list('metadata_record_of_manager__lt_service__pk', flat=True)
        self.all_version_services.add(
            *RegisteredLTService.objects.filter(id__in=version_records_ids)
        )

    def __str__(self):
        return self.metadata_record.described_entity.resource_name['en'] \
               + f' (v{self.metadata_record.described_entity.version})'
