from django.urls import path, include
from rest_framework import routers

from processing import views

router = routers.DefaultRouter()

app_name = 'processing'

urlpatterns = [
    path('', include(router.urls)),
    path('process_permissions/', views.process_permissions),
    path('service-stats/', views.set_service_stats),
    path('lt_service/<int:metadata_record__pk>/', views.RegisteredLTServiceView.as_view({'get': 'retrieve',
                                                                                         'put': 'update',
                                                                                         'patch': 'partial_update'}),
         name='lt_service'),
    path('lt_service/batch_update/', views.BatchUpdateRegisteredLTService.as_view({'get': 'retrieve',
                                                                                   'put': 'update',
                                                                                   'patch': 'partial_update'}),
         name='batch_lt_service_update')
]
