import copy
import json
import logging

import xmltodict
from django.contrib.admin.sites import AdminSite
from django.contrib.messages import get_messages

from accounts.models import ELGUser
from django.conf import settings
from management.models import Manager
from processing.admin import RegisteredLTServiceAdmin, MetadataStatusFilter
from processing.models import RegisteredLTService
from processing.serializers import RegisteredLTServiceSerializer
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer
from test_utils import import_metadata, MockRequest, get_test_user, EndpointTestCase

LOGGER = logging.getLogger(__name__)

ADMIN_ENDPOINT = f'/{settings.HOME_PREFIX}/admin/'


class TestRegisteredLTServiceAdmin(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.user, cls.token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.metadata_records[1].management_object.status = 'i'
        cls.metadata_records[1].management_object.save()

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.service_stats = {
            'user': 'preferred_username',
            'start_at': '2020-01-01',
            'end_at': '2020-02-02',
            'lt_service': 'x0f2143120x',
            'bytes': 12,
            'elg_resource': None,
            'status': 'succeeded',
            'call_type': 'PermissionsRequest'
        }

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()
    
    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_get_search_results_no_matches_found(self):
        reg_lt_admin = RegisteredLTServiceAdmin(model=RegisteredLTService, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        response = self.client.post(ADMIN_ENDPOINT)
        print(queryset)
        print(reg_lt_admin.get_search_results(response.wsgi_request, queryset, 'random words'))
        self.assertTrue(True)
        self.assertFalse(reg_lt_admin.get_search_results(response.wsgi_request, queryset, 'random words')[0])

    def test_get_search_results_matches_found(self):
        reg_lt_admin = RegisteredLTServiceAdmin(model=RegisteredLTService, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        response = self.client.post(ADMIN_ENDPOINT)
        print(queryset)
        print(reg_lt_admin.get_search_results(response.wsgi_request, queryset, 'Recognizer'))
        self.assertTrue(reg_lt_admin.get_search_results(response.wsgi_request, queryset, 'Recognizer'))

    def test_get_search_results_empty(self):
        reg_lt_admin = RegisteredLTServiceAdmin(model=RegisteredLTService, admin_site=AdminSite())
        queryset = MetadataRecord.objects.all()
        response = self.client.post(ADMIN_ENDPOINT)
        print(queryset)
        print(reg_lt_admin.get_search_results(response.wsgi_request, queryset, ''))
        self.assertTrue(reg_lt_admin.get_search_results(response.wsgi_request, queryset, ''))

    def test_reg_lt_admin_model_get_mdr_status(self):
        reg_lt_admin = RegisteredLTServiceAdmin(model=RegisteredLTService, admin_site=AdminSite())
        reg_lt_admin.save_model(obj=self.metadata_records[1], request=MockRequest(user=self.user),
                                form=None, change=None)
        self.assertEquals(reg_lt_admin.get_metadata_status(self.instance), 'INT')

    def test_reg_lt_admin_model_get_type(self):
        reg_lt_admin = RegisteredLTServiceAdmin(model=RegisteredLTService, admin_site=AdminSite())
        reg_lt_admin.save_model(obj=self.metadata_records[1], request=MockRequest(user=self.user),
                                form=None, change=None)
        self.assertEquals(reg_lt_admin.get_type(self.instance), 'Machine Translation')

    def test_reg_lt_admin_model_response_change_no_save_and_publish(self):
        # ltd_no_specs = copy.deepcopy(self.ltd_data)
        # ltd_no_specs['resource_approved'] = False
        xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(xml_test_data[0], encoding='utf-8') as xml_file:
            xml_input = xml_file.read()
        xml_data = xmltodict.parse(xml_input, xml_attribs=True)['ms:MetadataRecord']
        serializer = MetadataRecordSerializer(xml_data=xml_data)
        if serializer.is_valid():
            xml_instance = serializer.save()
            manager = Manager.objects.get(id=xml_instance.management_object.id)
            try:
                manager.curator = self.user
            except ELGUser.DoesNotExist:
                manager.curator = self.user
            manager.save()
            xml_instance.management_object = manager
            xml_instance.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')

        ltd_no_specs = {
            'metadata_record': xml_instance.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120fafsfdffasx'
        }
        serializer = RegisteredLTServiceSerializer(data=ltd_no_specs)
        if serializer.is_valid():
            no_specs_instance = serializer.save()
            no_specs_completed = copy.deepcopy(ltd_no_specs)
            no_specs_completed['elg_execution_location'] = 'http://www.executionlocation.com/differentLink'
            no_specs_completed['status'] = 'COMPLETED'
            no_specs_completed['elg_hosted'] = True
            serializer_completed = RegisteredLTServiceSerializer(no_specs_instance, data=no_specs_completed)
            serializer_completed.is_valid()
            instance_completed = serializer_completed.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        reg_lt_admin = RegisteredLTServiceAdmin(model=RegisteredLTService, admin_site=AdminSite())
        response = self.client.post(ADMIN_ENDPOINT)
        reg_lt_admin.response_change(response.wsgi_request, instance_completed)
        service = RegisteredLTService.objects.all().first()
        self.assertTrue(service)
        all_messages = [msg for msg in get_messages(response.wsgi_request)]
        self.assertIn('The LT Service Registration', all_messages[0].message)
        self.assertIn('Travel English grammar', all_messages[0].message)
        self.assertIn('was changed successfully.', all_messages[0].message)


class TestMetadataStatusFilter(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.user, cls.token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.service_stats = {
            'user': 'preferred_username',
            'start_at': '2020-01-01',
            'end_at': '2020-02-02',
            'lt_service': 'x0f2143120x',
            'bytes': 12,
            'elg_resource': None,
            'status': 'succeeded',
            'call_type': 'PermissionsRequest'
        }

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()
    
    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_queryset(self):
        reg_lt_admin = RegisteredLTServiceAdmin(model=RegisteredLTService, admin_site=AdminSite())
        response = self.client.post(ADMIN_ENDPOINT)
        status = [
            ('p', 'Published'),
            ('u', 'Unpublished'),
            ('g', 'Ingested'),
            ('i', 'Internal'),
            ('d', 'Draft')
        ]
        status_filter = MetadataStatusFilter(request=response.wsgi_request,
                                             params=status,
                                             model=RegisteredLTService,
                                             model_admin=reg_lt_admin)
        queryset = status_filter.queryset(response.wsgi_request, RegisteredLTService.objects.all())
        print(queryset)
        self.assertTrue(queryset)

    def test_queryset_with_value(self):
        reg_lt_admin = RegisteredLTServiceAdmin(model=RegisteredLTService, admin_site=AdminSite())
        value_dict = {'metadata-status': 'p'}
        response = self.client.post(ADMIN_ENDPOINT, data=value_dict)
        status_filter = MetadataStatusFilter(request=response.wsgi_request,
                                             params=value_dict,
                                             model=RegisteredLTService,
                                             model_admin=reg_lt_admin)
        queryset = status_filter.queryset(response.wsgi_request, RegisteredLTService.objects.all())
        print(queryset)
        self.assertFalse(queryset)