from keycloak import KeycloakOpenID
from rest_framework.test import APIClient
from django.conf import settings


API_ENDPOINT = '/api/registry/'
keycloak = KeycloakOpenID(server_url=settings.KEYCLOAK_SERVER_URL,
                          client_id=settings.KEYCLOAK_CLIENT,
                          realm_name='')
