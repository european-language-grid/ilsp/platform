import copy
import datetime
import json
import logging

from django.conf import settings
from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.timezone import now

from accounts.models import UserQuotas
from analytics.admin import ServiceStatsAdmin
from analytics.models import ServiceStats
from management.models import Validator
from processing.models import RegisteredLTService
from processing.serializers import RegisteredLTServiceSerializer
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer
from test_utils import import_metadata, api_call, MockRequest, get_test_user, EndpointTestCase

LOGGER = logging.getLogger(__name__)

REG_LT_ENDPOINT = f'/{settings.HOME_PREFIX}/api/processing/lt_service/'
BATCH_REG_LT_ENDPOINT = f'/{settings.HOME_PREFIX}/api/processing/lt_service/batch_update'
PP_ENDPOINT = f'/{settings.HOME_PREFIX}/api/processing/process_permissions/'
S_STATS_ENDPOINT = f'/{settings.HOME_PREFIX}/api/processing/service-stats/'


class TestRegisteredLtServiceEndpoint(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.raw_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'accessor_id': 'x0f2143120x'
        }
        cls.metadata_records[1].management_object.ingest()
        cls.serializer = RegisteredLTServiceSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.update_data = {
            'elg_execution_location': 'http://www.executionlocation.com/different_link/'
        }

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete(explicit=True)
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_retrieve_reg_lt_service_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/'
                            , self.client, auth=self.authenticate(token))
        print(response.get('json_content'))
        self.assertEquals(response.get('status_code'), 200)

    def test_cannot_retrieve_reg_lt_service_anonymous(self):
        response = api_call('GET', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_retrieve_reg_lt_service_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_retrieve_reg_lt_service_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Technical')
        response = api_call('GET', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_retrieve_reg_lt_service_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Legal')
        response = api_call('GET', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_retrieve_reg_lt_service_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Metadata')
        response = api_call('GET', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_retrieve_cannot_reg_lt_service_provider(self):
        user, token = self.provider, self.provider_token

        response = api_call('GET', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_update_reg_lt_service_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('PATCH', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token), data=self.update_data)
        self.assertEquals(response.get('status_code'), 200)

    def test_update_reg_lt_service_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('PATCH', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token), data=self.update_data)
        self.assertEquals(response.get('status_code'), 200)

    def test_cannot_update_reg_lt_service_anonymous(self):
        response = api_call('PATCH', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            data=self.update_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_update_reg_lt_service_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('PATCH', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token), data=self.update_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_update_reg_lt_service_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Technical')
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.save()
        response = api_call('PATCH', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token), data=self.update_data)
        self.assertEquals(response.get('status_code'), 200)

    def test_update_reg_lt_service_technical_validator_when_completed(self):
        user, token = self.tech_val, self.tech_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Technical')
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.save()
        update_complete = copy.deepcopy(self.raw_data)
        update_complete['status'] = 'COMPLETED'
        update_complete['elg_hosted'] = True
        update_complete['elg_execution_location'] = 'http://www.executionlocation.com/different_link/'
        response = api_call('PATCH', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token), data=update_complete)
        self.assertEquals(response.get('status_code'), 200)

    def test_update_reg_lt_service_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Legal')
        response = api_call('PATCH', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token), data=self.update_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_update_reg_lt_service_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Metadata')
        response = api_call('PATCH', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token), data=self.update_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_update_reg_lt_service_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('PATCH', f'{REG_LT_ENDPOINT}{self.metadata_records[1].pk}/', self.client,
                            auth=self.authenticate(token), data=self.update_data)
        self.assertEquals(response.get('status_code'), 403)


class TestBatchRegisteredLtServiceEndpoint(EndpointTestCase):
    test_data = ['registry/tests/fixtures/tool.json']
    MNG_ENDPOINT = f'/{settings.HOME_PREFIX}/api/management/'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        data_0 = json.loads(open(cls.test_data[0], encoding='utf-8').read())
        data_0['described_entity']['resource_name'] = {'en': 'functional 0'}
        data_0['described_entity']['resource_short_name'] = {}
        data_0['described_entity']['lr_identifier'] = []
        data_0['described_entity']['replaces'] = []
        serializer_0 = MetadataRecordSerializer(data=data_0)
        try:
            serializer_0.is_valid()
            cls.instance_0 = serializer_0.save()
            cls.instance_0.management_object.functional_service = True
            cls.instance_0.management_object.save()
        except ValidationError as err:
            LOGGER.info(err)
        data_1 = json.loads(open(cls.test_data[0], encoding='utf-8').read())
        data_1['described_entity']['resource_name'] = {'en': 'functional 1'}
        data_1['described_entity']['resource_short_name'] = {}
        data_1['described_entity']['lr_identifier'] = []
        data_1['described_entity']['replaces'] = []
        serializer_1 = MetadataRecordSerializer(data=data_1)
        try:
            serializer_1.is_valid()
            cls.instance_1 = serializer_1.save()
            cls.instance_1.management_object.functional_service = True
            cls.instance_1.management_object.save()
        except ValidationError as err:
            LOGGER.info(err)

        data_2 = json.loads(open(cls.test_data[0], encoding='utf-8').read())
        data_2['described_entity']['resource_name'] = {'en': 'functional 2'}
        data_2['described_entity']['resource_short_name'] = {}
        data_2['described_entity']['lr_identifier'] = []
        data_2['described_entity']['version'] = '11.0'
        data_2['described_entity']['replaces'] = []
        serializer_2 = MetadataRecordSerializer(data=data_2)
        try:
            serializer_2.is_valid()
            cls.instance_2 = serializer_2.save()
            cls.instance_2.management_object.functional_service = True
            cls.instance_2.management_object.save()
        except ValidationError as err:
            LOGGER.info(err)

        data_3 = json.loads(open(cls.test_data[0], encoding='utf-8').read())
        data_3['described_entity']['resource_name'] = {'en': 'non functional'}
        data_3['described_entity']['resource_short_name'] = {}
        data_3['described_entity']['lr_identifier'] = []
        data_3['described_entity']['version'] = '12.0'
        data_3['described_entity']['replaces'] = []
        serializer_3 = MetadataRecordSerializer(data=data_3)
        try:
            serializer_3.is_valid()
            cls.instance_3 = serializer_3.save()
        except ValidationError as err:
            LOGGER.info(err)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete(explicit=True)
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_retrieve_batch_lt_service_superuser(self):
        user, token = self.admin, self.admin_token
        api_call('PATCH',
                 f'{self.MNG_ENDPOINT}ingest/?record_id__in={self.instance_0.id},{self.instance_1.id},{self.instance_2.id},{self.instance_3.id}',
                 self.client, auth=self.authenticate(self.admin_token))
        ingested_records = MetadataRecord.objects.filter(id__in=[self.instance_0.id, self.instance_1.id,
                                                                 self.instance_2.id, self.instance_3.id])
        for record in ingested_records:
            record.management_object.technical_validator = self.tech_val
            record.management_object.metadata_validator = self.tech_val
            record.management_object.save()

        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        # no records len = 296
        # the 3 elg compatible service records len = 736
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('content'))
        self.assertTrue(len(response.get('content').decode()), 296)

    def test_retrieve_batch_lt_service_content_manager(self):
        user, token = self.c_m, self.c_m_token
        api_call('PATCH',
                 f'{self.MNG_ENDPOINT}ingest/?record_id__in={self.instance_0.id},{self.instance_1.id},{self.instance_2.id},{self.instance_3.id}',
                 self.client, auth=self.authenticate(self.admin_token))
        ingested_records = MetadataRecord.objects.filter(id__in=[self.instance_0.id, self.instance_1.id,
                                                                 self.instance_2.id, self.instance_3.id])
        for record in ingested_records:
            record.management_object.technical_validator = self.tech_val
            record.management_object.metadata_validator = self.tech_val
            record.management_object.save()

        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        # no records len = 296
        # the 3 elg compatible service records len = 736
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('content'))
        self.assertTrue(len(response.get('content').decode()), 296)

    def test_retrieve_batch_lt_service_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Technical')
        api_call('PATCH',
                 f'{self.MNG_ENDPOINT}ingest/?record_id__in={self.instance_0.id},{self.instance_1.id},{self.instance_2.id},{self.instance_3.id}',
                 self.client, auth=self.authenticate(self.admin_token))
        ingested_records = MetadataRecord.objects.filter(id__in=[self.instance_0.id, self.instance_1.id,
                                                                 self.instance_2.id, self.instance_3.id])
        for record in ingested_records:
            record.management_object.technical_validator = user
            record.management_object.metadata_validator = user
            record.management_object.save()

        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        # no records len = 296
        # the 3 elg compatible service records len = 736
        print(response.get('content'))
        print(response.get('content').decode())
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('content'))
        self.assertTrue(len(response.get('content').decode()), 736)

    def test_retrieve_batch_lt_service_technical_validator_no_matches(self):
        user, token = self.tech_val, self.tech_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Technical')
        api_call('PATCH',
                 f'{self.MNG_ENDPOINT}ingest/?record_id__in={self.instance_0.id},{self.instance_1.id},{self.instance_2.id},{self.instance_3.id}',
                 self.client, auth=self.authenticate(self.admin_token))
        ingested_records = MetadataRecord.objects.filter(id__in=[self.instance_0.id, self.instance_1.id,
                                                                 self.instance_2.id, self.instance_3.id])

        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        # no records len = 296
        # the 3 elg compatible service records len = 736
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(response.get('content'))
        self.assertTrue(len(response.get('content').decode()), 296)

    def test_cannot_retrieve_batch_lt_service_anonymous(self):
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_retrieve_batch_lt_service_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_retrieve_batch_lt_service_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Legal')
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_retrieve_batch_lt_service_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Metadata')
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_retrieve_cannot_batch_lt_service_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    # TODO fix tsv data pass in the test
    # def test_batch_update_lt_service_technical_validator(self):
    #     user, token = self.tech_val, self.tech_val_token
    #     Validator.objects.get_or_create(user=user, assigned_for='Technical')
    #     api_call('PATCH',
    #              f'{self.MNG_ENDPOINT}ingest/?record_id__in={self.instance_0.id},{self.instance_1.id},{self.instance_2.id},{self.instance_3.id}',
    #              self.client, auth=self.authenticate(self.admin_token))
    #     ingested_records = MetadataRecord.objects.filter(id__in=[self.instance_0.id, self.instance_1.id,
    #                                                              self.instance_2.id, self.instance_3.id])
    #     for record in ingested_records:
    #         record.management_object.technical_validator = user
    #         record.management_object.metadata_validator = user
    #         record.management_object.save()
    #
    #     response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
    #     # no records len = 296
    #     # the 3 elg compatible service records len = 736
    #     print(response.get('content'))
    #     print(response.get('content').decode())
    #     content = dict()
    #     content['file'] = open('test_fixtures/services-batch-update-empty.tsv', encoding='utf-8')
    #     print(content)
    #     print(content['file'].read())
    #     update_response = api_call('PUT', f'{BATCH_REG_LT_ENDPOINT}/', self.client,
    #                                auth=self.authenticate(token), data=content, data_format='multipart')
    #     print(update_response.get('status'))
    #     print(update_response.get('json_content'))
    #     print(update_response.get('content'))
    #     self.assertEquals(response.get('status_code'), 200)
    #     self.assertTrue(response.get('content'))
    #     self.assertTrue(len(response.get('content').decode()), 736)

    def test_cannot_batch_update_lt_service_anonymous(self):
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client)
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_batch_update_lt_service_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_batch_update_lt_service_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Legal')
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_batch_update_lt_service_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        Validator.objects.get_or_create(user=user, assigned_for='Metadata')
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_batch_update_cannot_batch_lt_service_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('GET', f'{BATCH_REG_LT_ENDPOINT}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)


class TestProcessPermissionsEndpoint(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.flat_rate_consumer, cls.flat_rate_consumer_token = get_test_user('flat_rate_consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)
        cls.metadata_records[1].management_object.functional_service = True
        cls.metadata_records[1].management_object.save()

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.update_data = copy.deepcopy(cls.ltd_data)
            cls.update_data['elg_execution_location'] = 'http://newexecloca.com'
            cls.update_data['status'] = 'COMPLETED'
            cls.update_data['elg_hosted'] = True
            cls.update_seriliazer = RegisteredLTServiceSerializer(cls.instance, data=cls.update_data)
            if cls.update_seriliazer.is_valid():
                cls.update_instance = cls.update_seriliazer.save()
            else:
                LOGGER.info(cls.update_seriliazer.errors)
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete(explicit=True)
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_process_permissions_passes_with_correct_data(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)

    def test_process_permissions_tester_loses_no_quotas(self):
        user, token = self.provider, self.provider_token
        elg_tester = Group.objects.get(name='elg_tester')
        user.groups.add(elg_tester)
        initial_bytes = user.quotas.remaining_daily_bytes
        initial_calls = user.quotas.remaining_daily_calls
        print(initial_calls)
        print(initial_bytes)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)
        user_after, _ = get_test_user('provider')
        print(user_after.quotas.remaining_daily_bytes)
        print(user_after.quotas.remaining_daily_calls)
        self.assertEquals(user_after.quotas.remaining_daily_bytes, initial_bytes)
        self.assertEquals(user_after.quotas.remaining_daily_calls, initial_calls)
        user.groups.remove(elg_tester)

    def test_process_permissions_response_contains_expected_fields(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(
            tuple(response.get('json_content').keys()), tuple(['corellationID', 'type', 'permission', 'allowedCalls',
                                                               'allowedBytes', 'consumed', 'elgExecutionLocation',
                                                               'user', 'service_info', ]))

    def test_process_permissions_fails_with_no_data(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('json_content'), {'detail': 'No JSON payload found'})
        self.assertEquals(response.get('status_code'), 400)

    def test_cannot_process_permissions_anonymous(self):
        response = api_call('POST', PP_ENDPOINT, self.client, data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)
        print(response.get('json_content'))

    def test_process_permissions_version_permissions(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        test_data = ['registry/tests/fixtures/tool.json',
                     'registry/tests/fixtures/tool_version.json']
        version_auto_data = json.loads(open(test_data[0], encoding='utf-8').read())
        version_1_1_data = json.loads(open(test_data[1], encoding='utf-8').read())
        serializer_auto = MetadataRecordSerializer(data=version_auto_data)
        serializer_auto.is_valid()
        instance_auto = serializer_auto.save()
        instance_auto.management_object.functional_service = True
        instance_auto.management_object.save()
        lt_auto, _ = RegisteredLTService.objects.get_or_create(metadata_record=instance_auto)
        instance_auto.management_object.ingest()
        ingested_auto = MetadataRecord.objects.get(id=instance_auto.id)
        lt_auto = RegisteredLTService.objects.get(metadata_record__id=ingested_auto.id)
        lt_auto.tool_type = 'http://w3id.org/meta-share/omtd-share/TextToSpeechSynthesis'
        lt_auto.accessor_id = 'asdazxc'
        lt_auto.elg_execution_location = 'https://www.loc.gr'
        lt_auto.status = 'COMPLETED'
        lt_auto.elg_hosted = True
        lt_auto.save()
        ingested_auto.management_object.handle_version_publication([])
        ingested_auto.management_object.publish()
        serializer_1_1 = MetadataRecordSerializer(data=version_1_1_data)
        serializer_1_1.is_valid()
        instance_1_1 = serializer_1_1.save()
        instance_1_1.management_object.functional_service = True
        instance_1_1.management_object.save()
        lt_1_1, _ = RegisteredLTService.objects.get_or_create(metadata_record=instance_1_1)
        instance_1_1.management_object.ingest()
        ingested_1_1 = MetadataRecord.objects.get(id=instance_1_1.id)
        lt_1_1 = RegisteredLTService.objects.get(metadata_record__id=ingested_1_1.id)
        lt_1_1.tool_type = 'http://w3id.org/meta-share/omtd-share/TextToSpeechSynthesis'
        lt_1_1.accessor_id = 'asdazxc'
        lt_1_1.elg_execution_location = 'https://www.loc.gr'
        lt_1_1.status = 'COMPLETED'
        lt_1_1.elg_hosted = True
        lt_1_1.save()
        ingested_1_1.management_object.handle_version_publication([])
        ingested_1_1.management_object.publish()
        raw_data['ltServiceID'] = 'asdazxc'
        response_latest = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        self.assertEquals(response_latest.get('status_code'), 200)
        self.assertEquals(response_latest['json_content']['service_info']['serviceName'],
                          lt_1_1.__str__())
        raw_data['ltServiceVersion'] = '9.0'
        response_active = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        self.assertEquals(response_active.get('status_code'), 200)
        self.assertEquals(response_active['json_content']['service_info']['serviceName'],
                          lt_auto.__str__())

    def test_process_permissions_returns_NYI(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type'] = 'ServiceCompletionRequest'
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        self.assertEquals(response.get('json_content'), {'detail': 'Not yet Implemented'})
        self.assertEquals(response.get('status_code'), 501)

    def test_process_permissions_fails_with_bad_data(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['type'] = 'fail'
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        self.assertEquals(response.get('json_content'), {'detail': 'Invalid or no request type'})
        self.assertEquals(response.get('status_code'), 400)

    def test_fails_with_invalid_reserve(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['reserve']['nBytes'] = 0
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        self.assertEquals(response.get('json_content'), {'detail': 'Invalid "reserve" input (1, 0))'})
        self.assertEquals(response.get('status_code'), 400)

    def test_fails_with_lt_service_not_found(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['ltServiceID'] = 'random id not found'
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        self.assertEquals(response.get('status_code'), 404)
        self.assertEquals(response.get('json_content'), {'detail': 'LT Service not found'})

    def test_passes_when_ingested(self):
        user, token = self.admin, self.admin_token
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.ingest()
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()

    def test_passes_when_published(self):
        user, token = self.admin, self.admin_token
        quotas = UserQuotas.objects.get(user=user)
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.handle_version_publication([])
        self.metadata_records[1].management_object.publish(force=True)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()

    def test_quotas_change_consumer(self):
        user, token = self.consumer, self.consumer_token
        quotas = UserQuotas.objects.get(user=user)
        initial_daily_calls = quotas.remaining_daily_calls
        initial_daily_bytes = quotas.remaining_daily_bytes
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.handle_version_publication([])
        self.metadata_records[1].management_object.publish(force=True)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()
        quotas = UserQuotas.objects.get(user=user)
        self.assertTrue(quotas.remaining_daily_calls < initial_daily_calls)
        self.assertTrue(quotas.remaining_daily_bytes < initial_daily_bytes)

    def test_quotas_change_provider(self):
        user, token = self.provider, self.provider_token
        quotas = UserQuotas.objects.get(user=user)
        initial_daily_calls = quotas.remaining_daily_calls
        initial_daily_bytes = quotas.remaining_daily_bytes
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.handle_version_publication([])
        self.metadata_records[1].management_object.publish(force=True)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()
        quotas = UserQuotas.objects.get(user=user)
        self.assertTrue(quotas.remaining_daily_calls < initial_daily_calls)
        self.assertTrue(quotas.remaining_daily_bytes < initial_daily_bytes)

    def test_quotas_change_content_manager(self):
        user, token = self.c_m, self.c_m_token
        quotas = UserQuotas.objects.get(user=user)
        initial_daily_calls = quotas.remaining_daily_calls
        initial_daily_bytes = quotas.remaining_daily_bytes
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.handle_version_publication([])
        self.metadata_records[1].management_object.publish(force=True)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()
        quotas = UserQuotas.objects.get(user=user)
        self.assertTrue(quotas.remaining_daily_calls < initial_daily_calls)
        self.assertTrue(quotas.remaining_daily_bytes < initial_daily_bytes)

    def test_quotas_change_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        quotas = UserQuotas.objects.get(user=user)
        initial_daily_calls = quotas.remaining_daily_calls
        initial_daily_bytes = quotas.remaining_daily_bytes
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.handle_version_publication([])
        self.metadata_records[1].management_object.publish(force=True)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()
        quotas = UserQuotas.objects.get(user=user)
        self.assertTrue(quotas.remaining_daily_calls < initial_daily_calls)
        self.assertTrue(quotas.remaining_daily_bytes < initial_daily_bytes)

    def test_quotas_change_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        quotas = UserQuotas.objects.get(user=user)
        initial_daily_calls = quotas.remaining_daily_calls
        initial_daily_bytes = quotas.remaining_daily_bytes
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.handle_version_publication([])
        self.metadata_records[1].management_object.publish(force=True)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()
        quotas = UserQuotas.objects.get(user=user)
        self.assertTrue(quotas.remaining_daily_calls < initial_daily_calls)
        self.assertTrue(quotas.remaining_daily_bytes < initial_daily_bytes)

    def test_quotas_no_change_superuser(self):
        user, token = self.admin, self.admin_token
        quotas = UserQuotas.objects.get(user=user)
        initial_daily_calls = quotas.remaining_daily_calls
        initial_daily_bytes = quotas.remaining_daily_bytes
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.handle_version_publication([])
        self.metadata_records[1].management_object.publish(force=True)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()
        quotas = UserQuotas.objects.get(user=user)
        self.assertTrue(quotas.remaining_daily_calls == initial_daily_calls)
        self.assertTrue(quotas.remaining_daily_bytes == initial_daily_bytes)

    def test_quotas_no_change_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        quotas = UserQuotas.objects.get(user=user)
        initial_daily_calls = quotas.remaining_daily_calls
        initial_daily_bytes = quotas.remaining_daily_bytes
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.handle_version_publication([])
        self.metadata_records[1].management_object.publish(force=True)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()
        quotas = UserQuotas.objects.get(user=user)
        self.assertTrue(quotas.remaining_daily_calls == initial_daily_calls)
        self.assertTrue(quotas.remaining_daily_bytes == initial_daily_bytes)

    def test_quotas_no_change_flatrate_consumer(self):
        user, token = self.flat_rate_consumer, self.flat_rate_consumer_token
        quotas = UserQuotas.objects.get(user=user)
        initial_daily_calls = quotas.remaining_daily_calls
        initial_daily_bytes = quotas.remaining_daily_bytes
        ltd_data = copy.deepcopy(self.ltd_data)
        previous_status = copy.deepcopy(self.metadata_records[1].management_object.status)
        self.metadata_records[1].management_object.technical_validator = self.tech_val
        self.metadata_records[1].management_object.handle_version_publication([])
        self.metadata_records[1].management_object.publish(force=True)
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        self.metadata_records[1].management_object.status = previous_status
        self.metadata_records[1].management_object.save()
        quotas = UserQuotas.objects.get(user=user)
        self.assertTrue(quotas.remaining_daily_calls == initial_daily_calls)
        self.assertTrue(quotas.remaining_daily_bytes == initial_daily_bytes)

    def test_fails_with_not_completed(self):
        user, token = self.admin, self.admin_token
        ltd_data = copy.deepcopy(self.ltd_data)
        ltd_data['status'] = 'PENDING'
        serializer = RegisteredLTServiceSerializer(self.instance, data=ltd_data)
        if serializer.is_valid():
            instance = serializer.save()
        response = api_call('POST', PP_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response.get('json_content'))
        self.assertEquals(response.get('status_code'), 406)
        self.assertEquals(response.get('json_content'), {
            'detail': 'This service registration has not been completed',
            'service_status': 'PENDING'
        })


class TestServiceStatsEndpoint(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.json_file = open(cls.test_data[1], 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.service_stats = {
            'user': 'elg-system',
            'start_at': now(),
            'end_at': now(),
            'lt_service': 'x0f2143120x',
            'bytes': 12,
            'elg_resource': None,
            'status': 'succeeded',
            'call_type': 'PermissionsRequest'
        }

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete(explicit=True)
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_service_stats_passes_with_correct_data_for_superuser(self):
        user, token = self.admin, self.admin_token
        service_stats = copy.deepcopy(self.service_stats)
        response = api_call('POST', f'{S_STATS_ENDPOINT}?token={settings.API_KEY}', self.client, data=service_stats)
        self.assertEquals(response.get('status_code'), 200)

    def test_service_stats_fails_for_not_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('POST', S_STATS_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=self.service_stats)
        self.assertEquals(response.get('status_code'), 403)

    def test_service_stats_admin_model_duration(self):
        user, token = self.admin, self.admin_token
        service_stats = copy.deepcopy(self.service_stats)
        service_stats['user'] = user
        service_stats['lt_service'] = self.instance
        service_stats['start_at'] = datetime.datetime(2020, 5, 5, 23, 59, 59)
        service_stats['end_at'] = datetime.datetime(2020, 6, 5, 23, 59, 59)
        service_stats_instance = ServiceStats.objects.create(**service_stats)
        service_stats_admin = ServiceStatsAdmin(model=ServiceStats, admin_site=AdminSite())
        service_stats_admin.save_model(obj=service_stats_instance, request=MockRequest(user=user),
                                       form=None, change=None)
        self.assertEquals(service_stats_admin.duration(service_stats_instance),
                          (service_stats['end_at'] - service_stats['start_at']).total_seconds())
