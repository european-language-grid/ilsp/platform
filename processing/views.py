# VIEWS
import csv
import io
import logging
from datetime import datetime

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from rest_framework.exceptions import ValidationError as ValErr
from django.db.models import Q
from django.http import HttpResponse
from rest_framework import status, viewsets, generics, mixins
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rolepermissions.checkers import has_role

from accounts import elg_permissions
from accounts.models import ELGUser
from accounts.user_index.documents import UserDocument
from analytics.consumer_index.documents import MyUsageDocument
from analytics.models import ServiceStats
from management.catalog_report_index.documents import CatalogReportDocument
from management.management_dashboard_index.documents import MyValidationsDocument
from management.models import INGESTED
from processing import filters
from processing.models import RegisteredLTService, TOOL_TYPE
from processing.permissions import UpdateLTServicePermission
from processing.serializers import RegisteredLTServiceSerializer
from registry.models import MetadataRecord
from registry.search_indexes.documents import MetadataDocument

LOGGER = logging.getLogger(__name__)

REQUEST_RESPONSE_MAP = {
    'PermissionsRequest': 'PermissionsResponse',
}


def get_processing_permissions(data, user: ELGUser):
    LOGGER.info('Json body:\n {}'.format(data))
    response = {
        'corellationID': data.get('corellationID'),
        'type': REQUEST_RESPONSE_MAP.get(data.get('type')),
        'permission': None,
        'allowedCalls': 0,
        'allowedBytes': 0,
        'consumed': {
            'nCalls': 1,
            'nBytes': data.get('reserve').get('nBytes')
        }
    }

    if data.get('reserve').get('nBytes') <= 0:
        return Response({'detail': f'Invalid "reserve" input ('
                                   f'{data.get("reserve").get("nCalls")}, {data.get("reserve").get("nBytes")}))'},
                        status.HTTP_400_BAD_REQUEST
                        )

    lt_service_qr = RegisteredLTService.objects.filter(accessor_id=data.get('ltServiceID'))
    if data.get('ltServiceVersion'):
        lt_service = lt_service_qr.filter(
            Q(metadata_record__described_entity__languageresource__version=data.get('ltServiceVersion')) &
            ((Q(metadata_record__management_object__is_active_version=True)
              & Q(metadata_record__management_object__status='p')) | Q(metadata_record__management_object__status='g'))
        ).first()
    else:
        lt_service = lt_service_qr.filter(metadata_record__management_object__is_latest_version=True).first()
        if not lt_service:
            lt_service = lt_service_qr.last()
    if not lt_service:
        return Response({'detail': 'LT Service not found'}, status.HTTP_404_NOT_FOUND)
    if lt_service.status != 'COMPLETED':
        return Response({
            'detail': 'This service registration has not been completed',
            'service_status': lt_service.status
        }, status.HTTP_406_NOT_ACCEPTABLE)
    metadata_record = lt_service.metadata_record
    tool_type = lt_service.tool_type

    if tool_type == 'http://w3id.org/meta-share/omtd-share/SpeechRecognition':
        remaining_daily_bytes = user.quotas.remaining_daily_bytes_asr
        response['allowedBytes'] = user.quotas.remaining_daily_bytes_asr
    else:
        remaining_daily_bytes = user.quotas.remaining_daily_bytes
        response['allowedBytes'] = user.quotas.remaining_daily_bytes

    response['allowedCalls'] = user.quotas.remaining_daily_calls

    if metadata_record.management_object.status in ['i', 'g'] and \
            metadata_record.management_object.requires_login and \
            not metadata_record.management_object.curator == user and \
            not user.is_superuser and not (metadata_record.management_object.technical_validator == user
                                           and has_role(user, 'technical_validator')):
        response['permission'] = 'NOT_AUTHORIZED'

    elif remaining_daily_bytes - data.get('reserve').get('nBytes') >= 0 \
            and user.quotas.remaining_daily_calls > 0:
        response['permission'] = 'AUTHORIZED_QUOTAS_RESERVED'
        if not (has_role(user, 'elg_tester')
                or has_role(user, 'flat_rate_consumer')
                or has_role(user, 'technical_validator')
                or user.is_superuser
        ):
            if tool_type == 'http://w3id.org/meta-share/omtd-share/SpeechRecognition':
                user.quotas.remaining_daily_bytes_asr -= data.get('reserve').get('nBytes')
            else:
                user.quotas.remaining_daily_bytes -= data.get('reserve').get('nBytes')

            user.quotas.remaining_daily_calls -= 1
            user.quotas.save()
            UserDocument().update(user)

        # since quotas are reserved add the execution location
        response['elgExecutionLocation'] = lt_service.elg_execution_location

    else:
        response['permission'] = 'AUTHORIZED_QUOTAS_EXCEEDED'

    response['allowedBytes'] = user.quotas.remaining_daily_bytes
    response['allowedCalls'] = user.quotas.remaining_daily_calls

    response['user'] = dict(username=user.username, id=user.id, keycloak_id=user.keycloak_id)

    # Additional Information for service
    service_information = dict(
        serviceName=lt_service.__str__(),
        metadataRecordID=lt_service.metadata_record.metadata_record_identifier.value,
        serviceMetadataID=[identifier.value for identifier in
                           lt_service.metadata_record.described_entity.lr_identifier.all()]
    )

    response['service_info'] = service_information

    return Response(response, status.HTTP_200_OK)


@api_view(http_method_names=['POST'])
@permission_classes((elg_permissions.IsAuthenticated,))
def process_permissions(request):
    # {
    #     "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
    #     "type": "PermissionsRequest",
    #     "ltServiceID": "annie",
    #     "user_token": "galanisd",
    #     "reserve": {
    #         "nCalls": 1,
    #         "nBytes": 12
    #     }
    # }

    if not request.data:
        return Response({'detail': 'No JSON payload found'}, status.HTTP_400_BAD_REQUEST)

    if request.data.get('type') == 'PermissionsRequest':
        processing_permissions = get_processing_permissions(request.data, request.user)
        LOGGER.info('Processing permissions:\n {}'.format(processing_permissions.data))
        return processing_permissions
    elif request.data.get('type') == 'ServiceCompletionRequest':
        return Response({'detail': 'Not yet Implemented'}, status.HTTP_501_NOT_IMPLEMENTED)
        # TODO handle payload info for job completion
    else:
        return Response({'detail': 'Invalid or no request type'}, status.HTTP_400_BAD_REQUEST)


@api_view(http_method_names=['POST'])
@permission_classes((elg_permissions.PrivateAccess,))
def set_service_stats(request):
    try:
        user = ELGUser.objects.get(username=request.data.get('user'))
        lt_service = RegisteredLTService.objects.get(accessor_id=request.data.get('lt_service'))
        service_stats = ServiceStats.objects.create(
            user=user,
            # user_ip=request.data.get('user_ip'), # TODO do not track user ip
            start_at=datetime.strptime(request.data.get('start_at'), '%Y-%m-%dT%H:%M:%S.%f%z'),
            end_at=datetime.strptime(request.data.get('end_at'), '%Y-%m-%dT%H:%M:%S.%f%z'),
            lt_service=lt_service,
            bytes=request.data.get('bytes'),
            elg_resource=request.data.get('elg_resource'),
            status=request.data.get('status'),
            call_type=request.data.get('call_type')
        )
        MyUsageDocument().update(service_stats)
        MetadataDocument().catalogue_update(lt_service.metadata_record)
        CatalogReportDocument().catalogue_report_update(lt_service.metadata_record.management_object)
    except Exception as e:
        return Response(e.args, status.HTTP_400_BAD_REQUEST)

    return Response(status.HTTP_200_OK)


class RegisteredLTServiceView(mixins.RetrieveModelMixin,
                              mixins.UpdateModelMixin,
                              viewsets.GenericViewSet):
    lookup_field = 'metadata_record__pk'
    queryset = RegisteredLTService.objects.all()
    serializer_class = RegisteredLTServiceSerializer
    allowed_roles = ['admin', 'technical_validator', 'content_manager']
    permission_classes = [elg_permissions.IsSuperUserOrTechnicalValidatorOrContentManager]

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        updated_permission_classes = [elg_permissions.IsSuperUserOrTechnicalValidatorOrContentManager]
        if self.action == 'update' or self.action == 'partial_update':
            updated_permission_classes.append(UpdateLTServicePermission)
        return [permission() for permission in updated_permission_classes]

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        try:
            self.perform_update(serializer)
        except ValidationError as err:
            return Response(err.messages, status=status.HTTP_400_BAD_REQUEST)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        manager = instance.metadata_record.management_object
        MyValidationsDocument().update(manager)
        return Response(serializer.data, status=status.HTTP_200_OK)


class BatchUpdateRegisteredLTService(mixins.RetrieveModelMixin,
                                     mixins.UpdateModelMixin,
                                     viewsets.GenericViewSet):
    queryset = RegisteredLTService.objects.all()
    serializer_class = RegisteredLTServiceSerializer
    allowed_roles = ['admin', 'technical_validator', 'content_manager']
    permission_classes = [elg_permissions.IsSuperUserOrTechnicalValidatorOrContentManager]

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        updated_permission_classes = [elg_permissions.IsSuperUserOrTechnicalValidatorOrContentManager]
        if self.action == 'update' or self.action == 'partial_update':
            updated_permission_classes.append(UpdateLTServicePermission)
        return [permission() for permission in updated_permission_classes]

    def get_queryset(self):
        user = self.request.user
        return RegisteredLTService.objects.filter(
            Q(status__in=['Pending', 'PENDING', 'New', 'NEW']),
            Q(metadata_record__management_object__deleted=False),
            Q(metadata_record__management_object__status=INGESTED),
            Q(metadata_record__management_object__technical_validator=user),
            Q(metadata_record__management_object__technically_valid__isnull=True)
        ).distinct()

    def retrieve(self, request, *args, **kwargs):
        '''
        return csv of all pending technical validations of user
        '''
        title = f'Pending-services-report_{self.request.user.username}_{datetime.today().strftime("%Y-%m-%d")}'
        output = io.StringIO()
        header = [
            'Service ID',
            'Resource ID',
            'Tool Type',
            'ELG Execution Location',
            'OpenAPI Spec URL',
            'ELG GUI URL',
            'YAML File',
            'Status',
            'ELG Hosted',
            'Accessor ID',
            'Docker Download Location (Read-only)',
            'Service Adapter Download Location (Read-only)',
            'Execution Location (Read-only)',
            'Private Resource (Read-only)',
            'Record Version (Read-only)'
        ]
        tsv_writer = csv.writer(output, delimiter='\t')
        tsv_writer.writerow(header)
        services = self.get_queryset()
        for service in services:
            service_data = self.get_serializer(service).data
            service_id = service.pk
            resource_id = service.metadata_record.pk
            tool_type = service_data.get('tool_type', '')
            if tool_type:
                tool_type = [tt[0] for tt in TOOL_TYPE if tt[1] == tool_type][0]
            elg_execution_location = service_data.get('elg_execution_location', '')
            open_api_spec_url = service_data.get('openAPI_spec_url', '')
            elg_gui_url = service_data.get('elg_gui_url', '')
            yaml_file = service_data.get('yaml_file', '')
            status = service_data.get('status', '')
            elg_hosted = service_data.get('elg_hosted', '')
            accessor_id = service_data.get('accessor_id', '')
            docker_download_location = service_data.get('docker_download_location', '')
            service_adapter_download_location = service_data.get('service_adapter_download_location', '')
            execution_location = service_data.get('execution_location', '')
            private_resource = service_data.get('private_resource', '')
            record_version = service_data.get('record_version', '')
            tsv_writer.writerow([
                service_id,
                resource_id,
                tool_type,
                elg_execution_location,
                open_api_spec_url,
                elg_gui_url,
                yaml_file,
                status,
                elg_hosted,
                accessor_id,
                docker_download_location,
                service_adapter_download_location,
                execution_location,
                private_resource,
                record_version
            ])
        output.seek(0)
        response = HttpResponse(output.read(), content_type='text/tab-separated-values')
        output.close()
        response['Content-Disposition'] = "attachment; filename={}.tsv".format(title)
        return response

    def update(self, request, *args, **kwargs):
        '''
        take csv and update all services
        '''
        response_dict = {'successful_ids': [],
                         'failed_ids': [],
                         'services': []}
        fieldnames = [
            'Service ID',
            'Resource ID',
            'Tool Type',
            'ELG Execution Location',
            'OpenAPI Spec URL',
            'ELG GUI URL',
            'YAML File',
            'Status',
            'ELG Hosted',
            'Accessor ID',
        ]
        if not request.data.get('file'):
            return Response({'detail': f'Please provide us with a tsv file of the services to be registered.'},
                            status=status.HTTP_400_BAD_REQUEST)
        csv_file = request.data['file']
        decoded_file = csv_file.read().decode('utf-8')
        io_string = io.StringIO(decoded_file)
        reader = csv.DictReader(io_string, delimiter='\t')
        if not len(set(fieldnames).intersection(set(reader.fieldnames))) == len(set(fieldnames)):
            return Response({'detail': f'Please make sure that all '
                                       f'of the following headers exist in your file \n {fieldnames}'},
                            status=status.HTTP_400_BAD_REQUEST)
        for row in reader:
            service_id = int(row['Service ID'])
            try:
                service = RegisteredLTService.objects.get(id=service_id)
            except ObjectDoesNotExist:
                response_dict['failed_ids'].append(service_id)
                response_dict['services'].append({f'[ID: {service_id}]': f'Could not find service with id {service_id}'})
                continue

            if not int(row['Resource ID']) == service.metadata_record.pk:
                response_dict['failed_ids'].append(service_id)
                response_dict['services'].append({f'[ID: {service_id}] {service}': 'There has been a mismatch between '
                                                                                   'the (service_id, record_id) '
                                                                                   'combination in the database and'
                                                                                   'the provided '
                                                                                   '(Service ID, Resource ID) '
                                                                                   'combination in the, please make '
                                                                                   'sure that the provided Service ID, '
                                                                                   'Resource ID match those of '
                                                                                   'the database.'})
                continue

            elg_hosted = True if row['ELG Hosted'] in ['True', 'true', 'TRUE', True, 'yes', 'Yes', 'YES'] else False
            update_dict = {
                'metadata_record': service.metadata_record.pk,
                'tool_type': row['Tool Type'],
                'elg_execution_location': row['ELG Execution Location'],
                'openAPI_spec_url': row['OpenAPI Spec URL'],
                'elg_gui_url': row['ELG GUI URL'],
                'yaml_file': row['YAML File'],
                'status': row['Status'],
                'elg_hosted': elg_hosted,
                'accessor_id': row['Accessor ID']
            }
            serializer = self.get_serializer(service, data=update_dict)
            try:
                serializer.is_valid(raise_exception=True)
                self.perform_update(serializer)
                response_dict['successful_ids'].append(service_id)
                response_dict['services'].append({f'[ID: {service_id}] {service}': serializer.data})
                if getattr(service, '_prefetched_objects_cache', None):
                    # If 'prefetch_related' has been applied to a queryset, we need to
                    # forcibly invalidate the prefetch cache on the instance.
                    service._prefetched_objects_cache = {}
                manager = service.metadata_record.management_object
                MyValidationsDocument().update(manager)
            except (ValidationError, ValErr):
                response_dict['failed_ids'].append(service_id)
                response_dict['services'].append({f'[ID: {service_id}] {service}': serializer.errors})
        csv_file.close()
        return Response(response_dict, status=status.HTTP_200_OK)
