TEMPLATE = {
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1"
    ],
    "@id": "http://example.com/ServiceOffering1_SD",  # TODO
    "type": [
        "VerifiablePresentation"
    ],
    "verifiableCredential":
        [
            {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1",
                    "https://w3id.org/security/suites/jws-2020/v1"
                ],
                "@id": "https://example.com/ServiceOffering1.json",  # TODO
                "type": [
                    "VerifiableCredential"
                ],
                "issuer": "https://live.european-language-grid.eu/",
                "issuanceDate": "",
                "credentialSubject": {
                    "@id": "",
                    "@type": "",
                    "gax-core:offeredBy": {"@id": ""},
                    "gax-trust-framework:termsAndConditions": {
                        "dct:title": "",
                        "gax-trust-framework:content": {
                            "@value": "",
                            "@type": "xsd:anyURI"
                        },
                        "@type": "gax-trust-framework:TermsAndConditions",
                        "gax-trust-framework:hash": ""  # TODO
                    },
                    "gax-trust-framework:dataAccountExport": {
                        "@type": "gax-trust-framework:DataAccountExport",
                        "gax-trust-framework:formatType": "application/zip",
                        "gax-trust-framework:accessType": "digital",
                        "gax-trust-framework:requestType": "API"
                    },
                    "gax-trust-framework:name": "",
                    "dct:description": "",
                    "dcat:keyword": [],
                    "gax-trust-framework:endpoint": {
                        "@type": "gax-trust-framework:Endpoint",
                        "gax-trust-framework:endpointURL": {
                            "@value": "",  # access location / download location
                            "@type": "xsd:anyURI"
                        }
                    },
                    "@context": {
                        "gax-trust-framework": "https://w3id.org/gaia-x/gax-trust-framework#",
                        "xsd": "http://www.w3.org/2001/XMLSchema#",
                        "dcat": "http://www.w3.org/ns/dcat#",
                        "gax-core": "https://w3id.org/gaia-x/core#",
                        "foaf": "http://xmlns.com/foaf/0.1/",
                        "dct": "http://purl.org/dc/terms/"
                    }
                }
            }
        ]
}
