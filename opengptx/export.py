import copy
import json
import logging
import os
import uuid
from datetime import datetime, timezone
from typing import Dict, List

import requests
from hashids import Hashids

from catalogue_backend.settings import settings
from management import ELG_STORAGE
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer
from registry.utils.string_utils import h_encode
from .dalicc import _get_odrl
from .templates import TEMPLATE

LOGGER = logging.getLogger(__name__)

LR_TYPE = {
    "Corpus": "corpus",
    "ToolService": "tool-service",
    "LexicalConceptualResource": "lcr",
    "LanguageDescription": "ld"
}

LR_TYPE_URI = {
    "Corpus": "http://w3id.org/meta-share/meta-share/corpus1",
    "ToolService": "http://w3id.org/meta-share/meta-share/toolService1",
    "LexicalConceptualResource": "http://w3id.org/meta-share/meta-share/lexicalConceptualResource1",
    "LanguageDescription": "http://w3id.org/meta-share/meta-share/languageDescription1"
}

CATALOG_URL = f'{settings.DJANGO_URL}/{settings.HOME_PREFIX}'

hashids = Hashids(min_length=50)


def _generate_output(record, etag, s3_key, distribution, licence: Dict, idx):
    lr_type = _get_lr_type(record)
    es_url = f"{CATALOG_URL}/api/registry/search/?&id__term={record.get('pk')}"
    print(es_url)
    es_response = requests.get(es_url).json().get("results")[0]
    output = copy.deepcopy(TEMPLATE)
    output["@id"] = f"https://live.european-language-grid.eu/{record.get('pk')}-{idx}-{uuid.uuid4().hex}"
    vc = output["verifiableCredential"][0]
    vc["issuanceDate"] = datetime.now(timezone.utc).isoformat().replace("+00:00", "Z")
    vc["@id"] = f"https://live.european-language-grid.eu/{record.get('pk')}-{idx}-{uuid.uuid4().hex}"
    if lr_type == "ToolService":
        vc["credentialSubject"]["@type"] = "gax-trust-framework:SoftwareOffering"
        # vc["credentialSubject"]["gax-trust-framework:endpoint"]["gax-trust-framework:endpointURL"][
        #     "@value"] = distribution.get("download_location", None) or distribution.get("access_location", None)
    else:
        vc["credentialSubject"]["@type"] = "gax-trust-framework:ServiceOffering"
        # vc["credentialSubject"]["@type"] = "gax-trust-framework:SoftwareOffering"
    vc["credentialSubject"]["gax-trust-framework:endpoint"]["gax-trust-framework:endpointURL"][
        "@value"] = f'{CATALOG_URL}/storage/{h_encode(record.get("pk"))}/{h_encode(distribution.get("pk"))}/' or distribution.get(
        "access_location", None)
    vc["credentialSubject"]["@id"] = f"{CATALOG_URL}/{LR_TYPE[lr_type]}/{record.get('pk')}-{idx}"
    vc["credentialSubject"]["gax-core:offeredBy"][
        "@id"] = f"{CATALOG_URL}/{LR_TYPE[lr_type]}/{record.get('pk')}"  # TODO FIX THIS
    vc["credentialSubject"]["gax-trust-framework:name"] = _get_name(record)
    vc["credentialSubject"]["dct:description"] = _get_description(record)
    vc["credentialSubject"]["dcat:keyword"] = _get_keywords(record)
    vc["credentialSubject"]["dcat:keyword"].extend(es_response.get("keywords"))
    vc["credentialSubject"]["dcat:keyword"].extend(es_response.get("intended_applications"))
    vc["credentialSubject"]["dcat:keyword"].extend(es_response.get("functions"))
    vc["credentialSubject"]["dcat:keyword"].extend(es_response.get("model_function"))

    vc["credentialSubject"]["dcat:keyword"] = [k.lower() for k in vc["credentialSubject"]["dcat:keyword"]]
    if lr_type == "LexicalConceptualResource":
        if record.get("described_entity").get("lr_subclass").get("lcr_subclass"):
            vc["credentialSubject"]["dcat:keyword"].append(
                record.get("described_entity").get("lr_subclass").get("lcr_subclass").split("/")[-1])
    vc["credentialSubject"]["gax-trust-framework:termsAndConditions"]["dct:title"] = licence.get('licence_terms_name')[
        'en']
    if s3_key.endswith(".gz"):
        vc["credentialSubject"]["gax-trust-framework:dataAccountExport"][
            "gax-trust-framework:formatType"] = "application/gzip"
    else:
        vc["credentialSubject"]["gax-trust-framework:dataAccountExport"][
            "gax-trust-framework:formatType"] = "application/zip"
    vc["credentialSubject"]["gax-trust-framework:termsAndConditions"]["gax-trust-framework:hash"] = etag
    vc["credentialSubject"]["gax-trust-framework:termsAndConditions"]["gax-trust-framework:content"][
        "@value"] = _get_licence_url(licence)
    # Try to get an odrl policy from dalicc
    licence_scheme = list(
        filter(lambda y: y.get('licence_identifier_scheme') == 'http://w3id.org/meta-share/meta-share/SPDX',
               licence.get('licence_identifier')))
    if licence_scheme:
        odrl = _get_odrl(licence_scheme[0].get("value"))
        if odrl is not None:
            vc["credentialSubject"]["gax-trust-framework:policy"] = json.dumps(
                _get_odrl(licence_scheme[0].get("value")), ensure_ascii=False)
        else:
            vc["credentialSubject"]["gax-trust-framework:policy"] = "ODRL representation to be added"
    else:
        vc["credentialSubject"]["gax-trust-framework:policy"] = "ODRL representation to be added"
    vc["credentialSubject"]["dcat:landingPage"] = {"@id": f"{CATALOG_URL}/{LR_TYPE[lr_type]}/{record.get('pk')}"}
    vc["credentialSubject"]["dct:language"] = [{"@id": f"http://w3id.org/meta-share/bcp47/language_{lang}"} for lang
                                               in list(set(_get_flat(list(_find_keys(record, "language_id")))))]
    vc["credentialSubject"]["ms:mediaType"] = [{"@id": mt} for mt in
                                               list(set(_get_flat(list(_find_keys(record, "media_type")))))]
    vc["credentialSubject"]["ms:lrType"] = {"@id": LR_TYPE_URI.get(lr_type)}

    return output


def export_to_gaix(record: MetadataRecord):
    """Export metadata to GAIA-X format"""
    mo = record.management_object
    try:
        s3_key = mo.content_files.all()[0].file.name
        etag = ELG_STORAGE.connection.meta.client.head_object(
            Bucket=ELG_STORAGE.bucket_name, Key=mo.content_files.all()[0].file.name).get("ETag").replace("\"", "")
    except IndexError:
        s3_key = "dummy.zip"
        # Just generate a dummy hash by encoding record, described_entity, and dataset_distribution[0] ids
        etag = hashids.encode(
            record.id + record.described_entity.id + record.described_entity.lr_subclass.dataset_distribution.all()[
                0].id)
    rc = MetadataRecordSerializer(record).data
    # if rc.get('management_object').get('has_data'):
    if rc.get("described_entity").get("entity_type") in ["Organization", "Person", "Group"]:
        LOGGER.error(f"Invalid entity type: {rc.get('described_entity').get('entity_type')}")
        return None
    result = list()
    idx = 0
    LOGGER.info("Processing data")
    distr = rc.get("described_entity").get("lr_subclass").get("dataset_distribution") or rc.get(
        "described_entity").get(
        "lr_subclass").get("software_distribution")
    for distribution in distr:
        for licence in distribution.get("licence_terms"):
            result.append(_generate_output(rc, etag, s3_key, distribution, licence, idx))
            idx += 1
    return result
    # LOGGER.error(f"Invalid record with id {rc.get('pk')}. Not an ELG hosted resource")
    print("-------------------------------")
    return None


def _get_lr_type(record: Dict) -> str:
    return record.get("described_entity").get("lr_subclass").get("lr_type")


def _get_name(record: Dict) -> str:
    return record.get("described_entity").get("resource_name")["en"]


def _get_description(record: Dict) -> str:
    return record.get("described_entity").get("description")["en"]


def _get_keywords(record: Dict) -> List[str]:
    result = list()
    result.extend(_get_domains(record))
    return result


def _get_domains(record: Dict) -> List[str]:
    result = list()
    for domain in record.get("described_entity").get("domain"):
        result.append(domain.get("category_label")["en"])
    return result


def _get_licence_url(licence: Dict) -> str:
    pltu = licence.get("licence_terms_url")[0]
    for ltu in licence.get("licence_terms_url"):
        if ltu.startswith("https://creativecommons.org") and not ltu.endswith("legalcode"):
            return ltu
        else:
            continue
    return pltu


def _get_flat(a_list: list) -> List:
    result = list()
    for x in a_list:
        if type(x) is str:
            result.append(x)
        elif type(x) is list:
            result.extend(x)
    return result


def _find_keys(node, kv):
    if isinstance(node, list):
        for i in node:
            for x in _find_keys(i, kv):
                yield x
    elif isinstance(node, dict):
        if kv in node:
            yield node[kv]
        for j in node.values():
            for x in _find_keys(j, kv):
                yield x


def sign_self_description():
    pass


def push_to_opengptx():
    pass

# for r in export_to_gaix(recs):
#     s = sign_self_description(r)
#     push_to_opengptx(s)
