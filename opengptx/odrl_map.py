ODRL = {
    "x": {
        "@context": {
            "cc": "http://creativecommons.org/ns#",
            "dalicc": "https://dalicc.net/ns#",
            "dalicclib": "https://dalicc.net/licenselibrary/",
            "dcat": "http://www.w3.org/ns/dcat#",
            "dct": "http://purl.org/dc/terms/",
            "foaf": "http://xmlns.com/foaf/0.1/",
            "odrl": "http://www.w3.org/ns/odrl/2/",
            "osl": "http://opensource.org/licenses/",
            "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
            "scho": "http://schema.org/",
            "xsd": "http://www.w3.org/2001/XMLSchema#"
        },
        "@graph": [
            {
                "@id": "dalicclib:CC-BY-NC-4.0",
                "@type": "odrl:Set",
                "cc:jurisdiction": "dalicc:worldwide",
                "cc:legalcode": "https://creativecommons.org/licenses/by-nc/4.0/legalcode",
                "cc:license": "dalicclib:CC-BY-4.0",
                "dalicc:LiabilityLimitation": "To the extent possible, in no event will the Licensor be liable to You on any legal theory (including, without limitation, negligence) or otherwise for any direct, special, indirect, incidental, consequential, punitive, exemplary, or other losses, costs, expenses, or damages arising out of this Public License or use of the Licensed Material, even if the Licensor has been advised of the possibility of such losses, costs, expenses, or damages. Where a limitation of liability is not allowed in full or in part, this limitation may not apply to You.",
                "dalicc:WarrantyDisclaimer": "Unless otherwise separately undertaken by the Licensor, to the extent possible, the Licensor offers the Licensed Material as-is and as-available, and makes no representations or warranties of any kind concerning the Licensed Material, whether express, implied, statutory, or other. This includes, without limitation, warranties of title, merchantability, fitness for a particular purpose, non-infringement, absence of latent or other defects, accuracy, or the presence or absence of errors, whether or not known or discoverable. Where disclaimers of warranties are not allowed in full or in part, this disclaimer may not apply to You.",
                "dalicc:validityType": "dalicc:perpetual",
                "dct:alternative": [
                    "Attribution NonCommercial 4.0 International",
                    "Attribution-NonCommercial 4.0",
                    "Attribution-NonCommercial 4.0 International",
                    "CC BY NC 4.0",
                    "CC BY NC 4.0 International",
                    "CC BY-NC 4.0",
                    "CC BY-NC 4.0 International",
                    "CC-BY-NC 4.0",
                    "CC-BY-NC 4.0 International",
                    "Creative Commons BY-NC 4.0",
                    "Creative Commons BY-NC 4.0 International"
                ],
                "dct:publisher": "Creative Commons",
                "dct:source": "https://creativecommons.org/licenses/by-nc/4.0/",
                "dct:title": {
                    "@language": "en",
                    "@value": "Creative Commons Attribution-NonCommercial 4.0 International"
                },
                "foaf:logo": "https://licensebuttons.net/l/by-nc/4.0/88x31.png",
                "odrl:permission": [
                    "_:n08e500e5b6964bcfabd22352127c055cb1",
                    "_:n08e500e5b6964bcfabd22352127c055cb2",
                    "_:n08e500e5b6964bcfabd22352127c055cb5",
                    "_:n08e500e5b6964bcfabd22352127c055cb9",
                    "_:n08e500e5b6964bcfabd22352127c055cb13",
                    "_:n08e500e5b6964bcfabd22352127c055cb14",
                    "_:n08e500e5b6964bcfabd22352127c055cb15",
                    "_:n08e500e5b6964bcfabd22352127c055cb16",
                    "_:n08e500e5b6964bcfabd22352127c055cb17"
                ],
                "odrl:prohibition": [
                    "_:n08e500e5b6964bcfabd22352127c055cb18",
                    "_:n08e500e5b6964bcfabd22352127c055cb19",
                    "_:n08e500e5b6964bcfabd22352127c055cb20"
                ],
                "odrl:target": "_:n08e500e5b6964bcfabd22352127c055cb21"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb1",
                "@type": "odrl:Permission",
                "odrl:action": "odrl:display"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb2",
                "@type": "odrl:Permission",
                "odrl:action": "odrl:distribute",
                "odrl:duty": [
                    "_:n08e500e5b6964bcfabd22352127c055cb3",
                    "_:n08e500e5b6964bcfabd22352127c055cb4"
                ]
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb3",
                "@type": "odrl:Duty",
                "odrl:action": "cc:Attribution"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb4",
                "@type": "odrl:Duty",
                "odrl:action": "cc:Notice"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb5",
                "@type": "odrl:Permission",
                "odrl:action": "odrl:modify",
                "odrl:duty": [
                    "_:n08e500e5b6964bcfabd22352127c055cb6",
                    "_:n08e500e5b6964bcfabd22352127c055cb7",
                    "_:n08e500e5b6964bcfabd22352127c055cb8"
                ]
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb6",
                "@type": "odrl:Duty",
                "odrl:action": "cc:Attribution"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb7",
                "@type": "odrl:Duty",
                "odrl:action": "cc:Notice"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb8",
                "@type": "odrl:Duty",
                "odrl:action": "dalicc:modificationNotice"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb9",
                "@type": "odrl:Permission",
                "odrl:action": "odrl:derive",
                "odrl:duty": [
                    "_:n08e500e5b6964bcfabd22352127c055cb10",
                    "_:n08e500e5b6964bcfabd22352127c055cb11",
                    "_:n08e500e5b6964bcfabd22352127c055cb12"
                ]
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb10",
                "@type": "odrl:Duty",
                "odrl:action": "cc:Attribution"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb11",
                "@type": "odrl:Duty",
                "odrl:action": "cc:Notice"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb12",
                "@type": "odrl:Duty",
                "odrl:action": "dalicc:modificationNotice"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb13",
                "@type": "odrl:Permission",
                "odrl:action": "odrl:present"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb14",
                "@type": "odrl:Permission",
                "odrl:action": "odrl:reproduce"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb15",
                "@type": "odrl:Permission",
                "odrl:action": "cc:DerivativeWorks"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb16",
                "@type": "odrl:Permission",
                "odrl:action": "dalicc:ModifiedWorks"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb17",
                "@type": "odrl:Permission",
                "odrl:action": "dalicc:chargeDistributionFee"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb18",
                "@type": "odrl:Prohibition",
                "odrl:action": "cc:CommercialUse"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb19",
                "@type": "odrl:Prohibition",
                "odrl:action": "dalicc:ChangeLicense"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb20",
                "@type": "odrl:Prohibition",
                "odrl:action": "dalicc:promote"
            },
            {
                "@id": "_:n08e500e5b6964bcfabd22352127c055cb21",
                "@type": "odrl:AssetCollection",
                "dct:type": [
                    "dalicc:CreativeWork",
                    "http://purl.org/dc/dcmitype/Dataset"
                ]
            }
        ]
    }
}

FRAME = {
    "@context": {
        "cc": "http://creativecommons.org/ns#",
        "dalicc": "https://dalicc.net/ns#",
        "dalicclib": "https://dalicc.net/licenselibrary/",
        "dcat": "http://www.w3.org/ns/dcat#",
        "dct": "http://purl.org/dc/terms/",
        "foaf": "http://xmlns.com/foaf/0.1/",
        "odrl": "http://www.w3.org/ns/odrl/2/",
        "osl": "http://opensource.org/licenses/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "scho": "http://schema.org/",
        "xsd": "http://www.w3.org/2001/XMLSchema#",

    },
    "@type": "odrl:Set",
    "odrl:permission": {"@type": "odrl:Permission", "odrl:duty": {"@type": "odrl:Duty"}},
    "odrl:prohibition": {"@type": "odrl:Prohibition"},
    "odrl:target": {"@type": "odrl:AssetCollection"}
}
