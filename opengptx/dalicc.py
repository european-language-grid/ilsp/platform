from typing import Dict

import pyld
import requests

FRAME = {
    "@context": {
        "cc": "http://creativecommons.org/ns#",
        "dalicc": "https://dalicc.net/ns#",
        "dalicclib": "https://dalicc.net/licenselibrary/",
        "dcat": "http://www.w3.org/ns/dcat#",
        "dct": "http://purl.org/dc/terms/",
        "foaf": "http://xmlns.com/foaf/0.1/",
        "odrl": "http://www.w3.org/ns/odrl/2/",
        "osl": "http://opensource.org/licenses/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "scho": "http://schema.org/",
        "xsd": "http://www.w3.org/2001/XMLSchema#",

    },
    "@type": "odrl:Set",
    "odrl:permission": {"@type": "odrl:Permission", "odrl:duty": {"@type": "odrl:Duty"}},
    "odrl:prohibition": {"@type": "odrl:Prohibition"},
    "odrl:target": {"@type": "odrl:AssetCollection"}
}


def _get_odrl(licence_id: str):
    url = "https://api.dalicc.net/licenselibrary/license/{}?format=json-ld&download=false"
    additional_ctx = ["odrl:duty", "odrl:permission", "odrl:prohibition", "odrl:target"]
    response = requests.get(url.format(licence_id))
    if response.json().get("@graph"):
        data = response.json()
        ctx = data.get("@context")
        for additional in additional_ctx:
            data['@context'][additional] = {"@type": "@id"}
        result = pyld.jsonld.compact(pyld.jsonld.frame(data, FRAME), ctx)

        for additional in additional_ctx:
            del result['@context'][additional]
        return result
    return None
