import redis

try:
    conn = redis.StrictRedis(
        host='redis',
        port=6379,
        password='')
    print(conn)
    conn.ping()
    print('Connected!')
except Exception as ex:
    print ('Error:', ex)
    exit('Failed to connect, terminating.')
