SECRET_KEY = 'g9wc=64^fi0z1*ztpllvud)ep!-s=b^m#82$!h=#@d@y@+b64u'
ROOT_URL = 'http://127.0.0.1'
DJANGO_PORT = 8000
DJANGO_URL = 'http://127.0.0.1'
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '192.168.0.3']
DB_NAME = 'mydb'  # TODO k8s Secret
DB_USER = 'myuser'  # TODO k8s Secret
DB_PASS = 'mypass'  # TODO k8s Secret
DB_HOST = '127.0.0.1'  # TODO k8s Secret
ES_URL = '127.0.0.1'
ES_PORT = '9200'  # TODO k8s Secret

REDIS_URL = 'redis'
REDIS_PORT = 6379
BROKER_URL = \
    'redis://redis:6379'
CELERY_RESULT_BACKEND = \
    'redis://redis:6379/0'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Africa/Nairobi'

KEYCLOAK_SERVER_URL = 'https://dev.european-language-grid.eu/auth/'
KEYCLOAK_CLIENT = 'elg_gatekeeper'
KEYCLOAK_ACCESS_CLIENT = 'react-client'
KEYCLOAK_PUBLIC_KEY = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx67McbabauPV/SrxPD7GnpzAQlz5C39W/ZdEaadDpqViHHzLjK0HE5R6YbQ0I1Sayi1LnEqQ+sd+vRd2X3I2kVtHN3brlNJ8XzI1McWsZDDkRHyg2uwvBfBjyBvuaZBT0wSOLEzC9FSisyrl9MdTJuNjtlIyK+jZqTXCal9p2zflEdKKyvC8c+nM3TLX4mow2swTmIBgu+qv97askw/Ja7w1QCc27LwtMKzM0sasYOEHk3JzpmCrhcz9VFEXm2DA0B68T+1fzW6zEbgb+aHinjOqg5d283iZcq2cfG9PtDB5ZYRm99qxsYXvYJocwkQqYpnHltfda1Nsz2TRhEVZZwIDAQAB
-----END PUBLIC KEY-----"""
# SECURITY WARNING: don't run with debug turned on in production!

USE_KEYCLOAK = True
DEBUG = True
SEND_EMAIL = False

if DEBUG:
    ALLOWED_HOSTS.append('testserver')

TEST_AUTH_DATA = {
    'credentials': {
        'client_id': 'react-client',
        'grant_type': 'password',
        'username': 'test-backend',
        'password': '123456'
    },
    'auth_url': 'https://dev.european-language-grid.eu/auth/realms/ELG/protocol/openid-connect/token/'

}

CORS_ORIGIN_WHITELIST = [
    'http://elg-front.ilsp.gr:3000',
    'http://elg-front.ilsp.gr:4000'
]

CORS_ALLOW_HEADERS = [
    "accept",
    "accept-encoding",
    "authorization",
    "content-type",
    "dnt",
    "origin",
    "user-agent",
    "x-csrftoken",
    "x-requested-with",
    "under-construction",
    "functional-service"
]

# S3 SETTINGS

DEV_BUCKET = 'dev-r1a'
S3_PUBLIC_BUCKET = 'elg-public'
AWS_ACCESS_KEY_ID = '371f3c77a96342b4b179abb16728fd60'
AWS_SECRET_ACCESS_KEY = '717e70600f8040c6a5bfa65c8d5a4d3f'
AWS_STORAGE_BUCKET_NAME = DEV_BUCKET
AWS_S3_REGION = 'cbk'
AWS_DEFAULT_ACL = 'private'
AWS_S3_ENDPOINT_URL = 'https://s3.{}.cloud.syseleven.net/'.format(AWS_S3_REGION)
AWS_S3_CUSTOM_DOMAIN = 's3.{}.cloud.syseleven.net/'.format(AWS_S3_REGION)
# AWS_S3_HOST = 's3.{}.amazonaws.com'.format(AWS_S3_REGION)
# AWS_STATIC_LOCATION = 'static'
# AWS_PUBLIC_MEDIA_LOCATION = 'public'
# AWS_PRIVATE_MEDIA_LOCATION = 'private'

CLAMAV_ENABLED = False

CLAMAV_UNIX_SOCKET = '/var/run/clamav/clamd.ctl'
CLAMAV_USE_TCP = False
CLAMAV_TCP_PORT = 3310
CLAMAV_TCP_ADDR = '127.0.0.1'

API_KEY = '7b4d5287-995c-46bd-9230-db7b3e91019d'

EMAIL_HOST = 'smtp.live.com'
EMAIL_HOST_USER = 'leonv@outlook.com.gr'
EMAIL_HOST_PASSWORD = 'voukoutis15'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_FAIL_SILENTLY = False

