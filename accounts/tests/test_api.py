import logging

from test_utils import SimpleSerializerTestCase
from rolepermissions import roles

from accounts.models import UserQuotas
from django.conf import settings
from accounts.api import UserSerializer

LOGGER = logging.getLogger(__name__)


class TestUserSerializer(SimpleSerializerTestCase):

    def setUp(cls):
        cls.user_attributes = {
            'id': 1,
            'keycloak_id': 'x23423x32',
            'username': 'test_username',
            'password': 'test_password',
            'email': 'test@password.com'
        }

        cls.serializer_data_will_fail = {
            'id': 2,
            'username': 'data_username',
            'email': 'data@password.com'
        }

        # cls.user = ELGUser.objects.create(**cls.user_attributes)
        cls.serializer = UserSerializer(data=cls.user_attributes)
        if cls.serializer.is_valid():
            cls.user = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)

    def test_serializer_contains_expected_fields(self):
        data = self.serializer.data
        self.assertTrue(set(data.keys()) == {'id', 'username', 'email'})

    def test_id_field_content(self):
        data = self.serializer.data
        self.assertTrue(data['id'], self.user_attributes['id'])

    def test_id_field_content_should_fail(self):
        self.serializer_data_will_fail['id'] = ''
        serializer_fail = UserSerializer(data=self.serializer_data_will_fail)
        self.assertFalse(serializer_fail.is_valid())
        LOGGER.info('Errors {}'.format(serializer_fail.errors))

    def test_username_field_content(self):
        data = self.serializer.data
        self.assertTrue(data['username'], self.user_attributes['username'])

    def test_username_field_content_should_fail(self):
        self.serializer_data_will_fail['username'] = ''
        serializer_fail = UserSerializer(data=self.serializer_data_will_fail)
        self.assertFalse(serializer_fail.is_valid())
        LOGGER.info('Errors {}'.format(serializer_fail.errors))

    def test_email_field_content(self):
        data = self.serializer.data
        self.assertTrue(data['email'], self.user_attributes['email'])

    def test_email_field_content_empty_should_fail(self):
        self.serializer_data_will_fail['email'] = ''
        serializer_fail = UserSerializer(data=self.serializer_data_will_fail)
        self.assertFalse(serializer_fail.is_valid())
        LOGGER.info('Errors {}'.format(serializer_fail.errors))

    def test_duplicate_user_creation_should_fail(self):
        serializer_fail = UserSerializer(data=self.user_attributes)
        serializer_fail.is_valid()
        LOGGER.info('Errors {}'.format(serializer_fail.errors))
        self.assertFalse(serializer_fail.is_valid())
        self.assertEquals(serializer_fail.errors['username'][0].code, 'unique')

    def test_email_field_content_invalid_email_should_fail(self):
        self.serializer_data_will_fail['email'] = 'not_an_email_address'
        serializer_fail = UserSerializer(data=self.serializer_data_will_fail)
        self.assertFalse(serializer_fail.is_valid())
        LOGGER.info('Errors {}'.format(serializer_fail.errors))

    def test_creates_user_quotas(self):
        initial_quotas = UserQuotas.objects.get(user=self.user)
        print(initial_quotas)
        self.assertTrue(UserQuotas.objects.get(user=self.user))

    def test_user_quotas_refresh_superuser(self):
        initial_quotas = UserQuotas.objects.get(user=self.user)
        self.user.is_superuser = True
        self.user.save()
        UserQuotas.objects.get(user=self.user).refresh()
        quotas_after_update = UserQuotas.objects.get(user=self.user)
        print(quotas_after_update)
        self.assertTrue(quotas_after_update.remaining_daily_bytes == initial_quotas.remaining_daily_bytes * 10)

    def test_user_quotas_refresh_legal_validator(self):
        initial_quotas = UserQuotas.objects.get(user=self.user)
        print(initial_quotas)
        roles.assign_role(self.user, 'legal_validator')
        UserQuotas.objects.get(user=self.user).refresh()
        quotas_after_update = UserQuotas.objects.get(user=self.user)
        print(quotas_after_update)
        self.assertTrue(quotas_after_update.remaining_daily_bytes == initial_quotas.remaining_daily_bytes * 10)

    def test_user_quotas_refresh_technical_validator(self):
        initial_quotas = UserQuotas.objects.get(user=self.user)
        print(initial_quotas)
        roles.assign_role(self.user, 'technical_validator')
        UserQuotas.objects.get(user=self.user).refresh()
        quotas_after_update = UserQuotas.objects.get(user=self.user)
        print(quotas_after_update)
        self.assertTrue(quotas_after_update.remaining_daily_bytes == initial_quotas.remaining_daily_bytes * 10)

    def test_user_quotas_refresh_metadata_validator(self):
        initial_quotas = UserQuotas.objects.get(user=self.user)
        print(initial_quotas)
        roles.assign_role(self.user, 'metadata_validator')
        UserQuotas.objects.get(user=self.user).refresh()
        quotas_after_update = UserQuotas.objects.get(user=self.user)
        print(quotas_after_update)
        self.assertTrue(quotas_after_update.remaining_daily_bytes == initial_quotas.remaining_daily_bytes * 10)

    def test_user_quotas_refresh_other(self):
        initial_quotas = UserQuotas.objects.get(user=self.user)
        roles.assign_role(self.user, 'consumer')
        UserQuotas.objects.get(user=self.user).refresh()
        quotas_after_update = UserQuotas.objects.get(user=self.user)
        print(quotas_after_update)
        self.assertTrue(quotas_after_update.remaining_daily_bytes == initial_quotas.remaining_daily_bytes)

    def test_creates_user_quotas_and_sets_to_zero(self):
        initial_quotas = UserQuotas.objects.get(user=self.user)
        initial_quotas.remaining_daily_bytes = -100
        initial_quotas.save()
        self.assertTrue(initial_quotas.remaining_daily_bytes == 0)

    def test_created_user_quotas_instance_name(self):
        initial_quotas = UserQuotas.objects.get(user=self.user)
        self.assertTrue(str(initial_quotas) ==
                        f'Calls: {initial_quotas.remaining_daily_calls}, '
                        f'Bytes: {initial_quotas.remaining_daily_bytes}, '
                        f'ASR Bytes: {initial_quotas.remaining_daily_bytes_asr}')

    def test_user_serializer_valid(self):
        user_data = {
            'id': 2,
            'username': 'other_username',
            'keycloak_id': 'x23423x32xJJQW',
            'password': 'other_pass',
            'email': 'other@password.com'
        }
        valid_serializer = UserSerializer(data=user_data)
        if valid_serializer.is_valid():
            valid_instance = valid_serializer.save()
        else:
            LOGGER.info(valid_serializer.errors)
        self.assertTrue(valid_serializer.is_valid())
        self.assertTrue(str(valid_instance) == valid_serializer.data['username'])
