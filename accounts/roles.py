from rolepermissions.roles import AbstractUserRole

CONSUMER = {'view_metadata': True}


# class Admin(AbstractUserRole):
#     available_permissions = {
#         'create_medical_record': True,
#     }


class Consumer(AbstractUserRole):
    available_permissions = CONSUMER


class FlatRateConsumer(Consumer):
    pass

class Provider(AbstractUserRole):
    available_permissions = {
        **Consumer.available_permissions,
        **{
            'edit_metadata': True,
            'import_single_metadata_file': True,
            'import_batch_metadata': True,
            'ingest_metadata_record': True,
            'mark_metadata_record_as_deleted': True,
            'upload_dataset': True
        }
    }


class Validator(AbstractUserRole):
    available_permissions = {
        **Consumer.available_permissions,
        **{
            'view_management_object': True
        }
    }


class LegalValidator(Validator):
    available_permissions = {
        **Validator.available_permissions,
        **{
            'view_legal_validation': True
        }
    }


class TechnicalValidator(Validator):
    available_permissions = {
        **Validator.available_permissions,
        **{
            'view_technical_validation': True
        }
    }


class MetadataValidator(Validator):
    available_permissions = {
        **Validator.available_permissions,
        **{
            'view_metadata_validation': True
        }
    }


class ElgTester(Provider):
    available_permissions = {**Provider.available_permissions}


class ContentManager(AbstractUserRole):
    available_permissions = {
        **Consumer.available_permissions,
        **{
            'edit_metadata': True,
            'edit_management_object': True,
            'unpublish_metadata_record': True,
            'mark_metadata_record_as_deleted': True,
        }
    }


class Admin(AbstractUserRole):
    pass
