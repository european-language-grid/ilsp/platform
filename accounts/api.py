from rest_framework import serializers

from accounts.models import ELGUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ELGUser
        fields = '__all__'

    def to_representation(self, obj):
        data = dict()
        data['id'] = obj.id
        data['username'] = obj.username
        data['email'] = obj.email
        return data
