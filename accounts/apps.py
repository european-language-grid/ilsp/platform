from django.apps import AppConfig
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.signals import post_migrate
from django.apps import apps as django_apps


class AccountsConfig(AppConfig):
    name = 'accounts'

    @staticmethod
    def _ensure_validator_permissions(sender, **kwargs):
        from django.contrib.auth.models import Group, Permission
        try:
            technical_validator, created = Group.objects.get_or_create(name='technical_validator')
        except ObjectDoesNotExist:
            pass

    @staticmethod
    def _create_groups(sender, **kwargs):
        from django.contrib.auth.models import Group, Permission
        metadata_validator, created = Group.objects.get_or_create(name='metadata_validator')
        legal_validator, created = Group.objects.get_or_create(name='legal_validator')
        technical_validator, created = Group.objects.get_or_create(name='technical_validator')
        content_manager, created = Group.objects.get_or_create(name='content_manager')
        elg_tester, created = Group.objects.get_or_create(name='elg_tester')
        flat_rate_consumer , created = Group.objects.get_or_create(name='flat_rate_consumer')

    def _post_migration_callback(self, sender, **kwargs):
        processing_config = django_apps.get_app_config('processing')
        post_migrate.send(
            sender=processing_config,
            app_config=processing_config,
            verbosity=1,
            interactive=False
        )
        post_migrate.connect(self._ensure_validator_permissions, sender=processing_config)
        post_migrate.connect(self._create_groups, sender=processing_config)

    def ready(self):
        # On startup, ensure groups and related permissions are as expected
        post_migrate.connect(self._post_migration_callback, sender=self)
