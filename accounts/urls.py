from django.urls import include, path
from rest_framework import routers

from accounts import views
from .user_index import urls as user_index_urls

router = routers.DefaultRouter()

urlpatterns = (
    path('user_quotas/', views.get_user_quotas),
    path('login/', views.login_user),
    path('logout/', views.logout_user),
    path('activate/', views.activate_user),
    path('deactivate/', views.deactivate_user),
    path('user-index/', include(user_index_urls)),
    path('sync/', views.keycloak_sync)
)
