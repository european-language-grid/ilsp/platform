from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import (
    IsAuthenticatedOrReadOnly, SAFE_METHODS,
    IsAuthenticated, BasePermission
)
from rest_framework.renderers import JSONRenderer
from rolepermissions.checkers import has_role

from django.conf import settings
from registry.models import MetadataRecord


def is_owner(user, resource):
    try:
        return bool(
            resource.management_object.curator == user
        )
    except KeyError:
        return None


def response_forbidden(status=status.HTTP_403_FORBIDDEN, reason='Insufficient Rights'):
    response = {
        'status': status,
        'reason': reason
    }
    response = Response(response, status=status)
    response.accepted_renderer = JSONRenderer()
    response.accepted_media_type = "application/json"
    response.renderer_context = {}

    return response


class IsSuperUserOrOwnerOrReadOnly(IsAuthenticatedOrReadOnly):
    """
    The request is authenticated as a user that owns the resource
    or as a superuser, or is a read-only request.
    """

    def has_permission(self, request, view):
        try:
            if view.kwargs.get('pk', None):
                resource = MetadataRecord.objects.get(id=view.kwargs.get('pk', None))
                is_owner_of_resource = is_owner(request.user, resource)
            else:
                pk = request.query_params.get('record_id__in', None).split(',')
                pk_list = [int(_pk) for _pk in pk if _pk]
                resources = MetadataRecord.objects.filter(id__in=pk_list)
                is_owner_of_resource = False
                for resource in resources:
                    if is_owner(request.user, resource):
                        is_owner_of_resource = True
                        break
            return bool(
                request.method in SAFE_METHODS or
                request.user and
                request.user.is_authenticated and
                (request.user.is_superuser or is_owner_of_resource)
            )
        except ObjectDoesNotExist:
            return False


class IsSuperUserOrOwner(IsAuthenticated):
    """
    The request is authenticated as a user that owns the resource
    or as a superuser, or is a read-only request.
    """

    def has_permission(self, request, view):
        obj_pk = view.kwargs.get('pk', view.kwargs.get('metadata_record_of_manager__pk', None))
        try:
            if obj_pk not in [None, 0]:
                resource = MetadataRecord.objects.get(id=obj_pk)
                is_owner_of_resource = is_owner(request.user, resource)
            elif obj_pk == 0:
                is_owner_of_resource = True
            else:
                pk = request.query_params.get('record_id__in', None).split(',')
                pk_list = [int(_pk) for _pk in pk if _pk]
                resources = MetadataRecord.objects.filter(id__in=pk_list)
                is_owner_of_resource = False
                for resource in resources:
                    if is_owner(request.user, resource):
                        is_owner_of_resource = True
                        break
            return bool(
                request.user and
                request.user.is_authenticated and
                (request.user.is_superuser or is_owner_of_resource)
            )
        except ObjectDoesNotExist:
            return False


class IsSuperUserOrOwnerOrValidator(IsAuthenticated):
    """
    The request is authenticated as a user that owns the resource
    or as a superuser, or is a read-only request.
    """

    def has_permission(self, request, view):
        try:
            if view.kwargs.get('pk', None):
                resource = MetadataRecord.objects.get(id=view.kwargs.get('pk', None))
                is_owner_of_resource = is_owner(request.user, resource)
            else:
                pk = request.query_params.get('record_id__in', None).split(',')
                pk_list = [int(_pk) for _pk in pk if _pk]
                resources = MetadataRecord.objects.filter(id__in=pk_list)
                is_owner_of_resource = False
                for resource in resources:
                    if is_owner(request.user, resource):
                        is_owner_of_resource = True
                        break
            return bool(
                request.user and
                request.user.is_authenticated and
                (request.user.is_superuser or
                 is_owner_of_resource or
                 has_role(request.user, 'legal_validator') or
                 has_role(request.user, 'technical_validator') or
                 has_role(request.user, 'metadata_validator'))
            )
        except ObjectDoesNotExist:
            return False


class IsSuperUserOrContentManagerOrValidator(IsAuthenticated):
    """
    The request is authenticated as a user that owns the resource
    or as a superuser, or is a read-only request.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            (request.user.is_superuser or
             has_role(request.user, 'content_manager') or
             has_role(request.user, 'legal_validator') or
             has_role(request.user, 'technical_validator') or
             has_role(request.user, 'metadata_validator'))
        )


class IsSuperUserOrValidator(IsAuthenticated):
    """
    The request is authenticated as a user that owns the resource
    or as a superuser, or is a read-only request.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            (request.user.is_superuser or
             has_role(request.user, 'legal_validator') or
             has_role(request.user, 'technical_validator') or
             has_role(request.user, 'metadata_validator'))
        )


class PrivateAccess(BasePermission):
    """
    The request is authenticated as a user that owns the resource
    or as a superuser, or is a read-only request.
    """

    def has_permission(self, request, view):
        return bool(
            'token' in request.GET and request.GET['token'] == settings.API_KEY
        )


class IsSuperUserOrProviderOrContentManager(IsAuthenticated):
    """
    The request is authenticated as a user that owns the resource
    or as a superuser, or is a read-only request.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            (request.user.is_superuser or
             has_role(request.user, 'provider') or
             has_role(request.user, 'content_manager'))
        )


class IsSuperUserOrProvider(IsAuthenticated):
    """
    The request is authenticated as a user that owns the resource
    or as a superuser, or is a read-only request.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            (request.user.is_superuser or
             has_role(request.user, 'provider'))
        )


class IsSuperUserOrContentManager(IsAuthenticated):
    """
    The request is authenticated as a superuser or content administrator.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            (request.user.is_superuser or
             has_role(request.user, 'content_manager'))
        )


class IsSuperUserOrLegalValidatorOrContentManager(IsAuthenticated):
    """
    The request is authenticated as a superuser or content administrator.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            (request.user.is_superuser or
             has_role(request.user, 'content_manager') or
             has_role(request.user, 'legal_validator')
             )
        )


class IsSuperUserOrMetadataValidatorOrContentManager(IsAuthenticated):
    """
    The request is authenticated as a superuser or content administrator.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            (request.user.is_superuser or
             has_role(request.user, 'content_manager') or
             has_role(request.user, 'metadata_validator')
             )
        )


class IsSuperUserOrTechnicalValidatorOrContentManager(IsAuthenticated):
    """
    The request is authenticated as a superuser or content administrator.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            (request.user.is_superuser or
             has_role(request.user, 'content_manager') or
             has_role(request.user, 'technical_validator')
             )
        )


class IsSuperUserOrOwnerOrTechnicalValidatorOrContentManager(IsAuthenticated):
    """
    The request is authenticated as a superuser or content administrator.
    """

    def has_permission(self, request, view):
        try:
            if view.kwargs.get('pk', None):
                resource = MetadataRecord.objects.get(id=view.kwargs.get('pk', None))
                is_owner_of_resource = is_owner(request.user, resource)
            else:
                pk = request.query_params.get('record_id__in', None).split(',')
                pk_list = [int(_pk) for _pk in pk if _pk]
                resources = MetadataRecord.objects.filter(id__in=pk_list)
                is_owner_of_resource = False
                for resource in resources:
                    if is_owner(request.user, resource):
                        is_owner_of_resource = True
                        break
            return bool(
                request.user and
                request.user.is_authenticated and
                (request.user.is_superuser or
                 is_owner_of_resource or
                 has_role(request.user, 'content_manager') or
                 has_role(request.user, 'technical_validator')
                 )
            )
        except ObjectDoesNotExist:
            return False


class IsSuperUserOrOwnerOrContentManager(IsAuthenticated):
    """
    The request is authenticated as a superuser or content administrator.
    """

    def has_permission(self, request, view):
        try:
            obj_pk = view.kwargs.get('pk', view.kwargs.get('metadata_record_of_manager__pk', None))
            if obj_pk is not None:
                if int(obj_pk) == 0:
                    is_owner_of_resource = True
                else:
                    resource = MetadataRecord.objects.get(id=obj_pk)
                    is_owner_of_resource = is_owner(request.user, resource)
            else:
                pk = request.query_params.get('record_id__in', None).split(',')
                pk_list = [int(_pk) for _pk in pk if _pk]
                resources = MetadataRecord.objects.filter(id__in=pk_list)
                is_owner_of_resource = False
                for resource in resources:
                    if is_owner(request.user, resource):
                        is_owner_of_resource = True
                        break
            return bool(
                request.user and
                request.user.is_authenticated and
                (request.user.is_superuser or
                 is_owner_of_resource or
                 has_role(request.user, 'content_manager'))

            )
        except ObjectDoesNotExist:
            return False


class IsSuperUser(IsAuthenticated):
    """
    The request is authenticated as a user that owns the resource
    or as a superuser, or is a read-only request.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            request.user.is_superuser
        )
