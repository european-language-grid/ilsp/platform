from django.core.exceptions import ObjectDoesNotExist
from django_elasticsearch_dsl import Index, fields
from django_elasticsearch_dsl.documents import DocType
from elasticsearch_dsl import analyzer, normalizer

from accounts.models import ELGUser
from registry.utils.retrieval_utils import retrieve_default_lang

# Name of the Elasticsearch index
elg_user = Index('elg_users')
# See Elasticsearch Indices API reference for available settings
elg_user.settings(
    number_of_shards=1,
    number_of_replicas=0
)

html_strip = analyzer(
    'html_strip',
    tokenizer='standard',
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)
lowercase_normalizer = normalizer(
    'lowercase_normalizer',
    filter=['lowercase']
)


@elg_user.doc_type
class UserDocument(DocType):
    class Django:
        model = ELGUser  # The model associated with this Document

    id = fields.IntegerField(attr='id')

    username = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    keycloak_id = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    email_address = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    first_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    last_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    staff_status = fields.BooleanField()

    user_roles = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
        multi=True
    )

    calls = fields.IntegerField()
    bytes = fields.IntegerField()
    bytes_asr = fields.IntegerField()
    calls_percent = fields.FloatField()
    bytes_percent = fields.FloatField()
    bytes_asr_percent = fields.FloatField()

    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        """
        Return the queryset that should be indexed by this doc type.
        """
        return ELGUser.objects.all()

    # PREPARE FIELDS

    def prepare_username(self, instance):
        return instance.username

    def prepare_email_address(self, instance):
        return instance.email

    def prepare_first_name(self, instance):
        return instance.first_name

    def prepare_last_name(self, instance):
        return instance.last_name

    def prepare_staff_status(self, instance):
        return instance.is_staff

    def prepare_user_roles(self, instance):
        roles = [role.name for role in instance.groups.all()]
        for n, role_name in enumerate(roles):
            if '_validator' in role_name:
                roles[n] = role_name.split(sep='-')[0]
        return roles

    def prepare_calls(self, instance):
        try:
            user_quotas = instance.quotas
            return user_quotas.remaining_daily_calls
        except ObjectDoesNotExist:
            return None

    def prepare_bytes(self, instance):
        try:
            user_quotas = instance.quotas
            return user_quotas.remaining_daily_bytes
        except ObjectDoesNotExist:
            return None

    def prepare_bytes_asr(self, instance):
        try:
            user_quotas = instance.quotas
            return user_quotas.remaining_daily_bytes_asr
        except ObjectDoesNotExist:
            return None

    def prepare_calls_percent(self, instance):
        try:
            user_quotas = instance.quotas
            calls_percent = user_quotas.remaining_daily_calls / user_quotas.max_daily_calls
            return calls_percent
        except ObjectDoesNotExist:
            return None

    def prepare_bytes_percent(self, instance):
        try:
            user_quotas = instance.quotas
            bytes_percent = user_quotas.remaining_daily_bytes / user_quotas.max_daily_bytes
            return bytes_percent
        except ObjectDoesNotExist:
            return None

    def prepare_bytes_asr_percent(self, instance):
        try:
            user_quotas = instance.quotas
            bytes_asr_percent = user_quotas.remaining_daily_bytes_asr / user_quotas.max_daily_bytes_asr
            return bytes_asr_percent
        except ObjectDoesNotExist:
            return None

    def prepare_keycloak_id(self, instance):
        return instance.keycloak_id




