from django_elasticsearch_dsl_drf.constants import (
    LOOKUP_FILTER_TERMS,
    LOOKUP_FILTER_PREFIX, LOOKUP_FILTER_WILDCARD,
    LOOKUP_QUERY_IN, LOOKUP_QUERY_EXCLUDE
)
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
    CompoundSearchFilterBackend,
    FacetedSearchFilterBackend, MultiMatchSearchFilterBackend
)
from django_elasticsearch_dsl_drf.pagination import PageNumberPagination
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet
from elasticsearch_dsl import TermsFacet
from accounts.elg_permissions import IsSuperUserOrContentManager, IsAuthenticated

from accounts.user_index.documents import UserDocument
from accounts.user_index.serializers import UserDocumentSerializer
from utils.index_backends.backends import LuceneStringSearchFilterBackend


class UserDocumentView(BaseDocumentViewSet):
    """The MetadataDocument view."""

    document = UserDocument
    serializer_class = UserDocumentSerializer
    pagination_class = PageNumberPagination
    permission_classes = (IsAuthenticated,)
    lookup_field = 'id'
    filter_backends = [
        LuceneStringSearchFilterBackend,
        FilteringFilterBackend,
        FacetedSearchFilterBackend,
        CompoundSearchFilterBackend,
        MultiMatchSearchFilterBackend,
        DefaultOrderingFilterBackend,
        OrderingFilterBackend,
    ]

    simple_query_string_options = {
        "default_operator": "and",
    }

    # Define search fields
    search_fields = {
        'username': {'boost': 4},
        'email_address': {'boost': 4},
        'first_name': None,
        'last_name': None,
    }
    # Define filter fields
    filter_fields = {
        'id': None,
        'staff_status': 'staff_status',
        'user_roles': 'user_roles',
    }
    # Define ordering fields
    ordering_fields = {
        'id': 'id',
        'username': 'username.raw'
    }
    # Specify default ordering
    ordering = ('_score', 'username.raw', 'id')

    faceted_search_fields = {}

    # Define post-filter filtering fields
    post_filter_fields = {
        'languages': {
            'field': 'languages',
            'lookups': [
                LOOKUP_FILTER_TERMS,
                LOOKUP_FILTER_PREFIX,
                LOOKUP_FILTER_WILDCARD,
                LOOKUP_QUERY_IN,
                LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'licences': {
            'field': 'licences',
            'lookups': [
                LOOKUP_FILTER_TERMS,
                LOOKUP_FILTER_PREFIX,
                LOOKUP_FILTER_WILDCARD,
                LOOKUP_QUERY_IN,
                LOOKUP_QUERY_EXCLUDE,
            ],
        },
    }

    def get_queryset(self):
        queryset = self.search.query()
        queryset.model = self.document.Django.model
        user = self.request.user
        queryset = self.search.query("term", keycloak_id=user.keycloak_id)
        return queryset


