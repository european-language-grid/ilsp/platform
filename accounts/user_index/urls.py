from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import UserDocumentView

router = DefaultRouter()
elg_user = router.register('', UserDocumentView, basename='elg_user')

urlpatterns = [
    path('', include(router.urls)),
]
