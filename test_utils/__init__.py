import json
import logging
import requests
from unittest.mock import patch

from django.test import TestCase
from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase
from rolepermissions import roles
from registry.serializers import MetadataRecordSerializer
from keycloak import KeycloakOpenID

LOGGER = logging.getLogger(__name__)
keycloak = KeycloakOpenID(
    server_url=settings.KEYCLOAK_SERVER_URL,
    client_id=settings.KEYCLOAK_CLIENT,
    realm_name=''
)

# The DOIs that it used for testing the record DOI and the concept DOI mechanisms
# They do not resolve correctly, are only for testing
TEST_DOI_1 = f'{settings.DATACITE_PREFIX}/rn28-db14'
TEST_CONCEPT_DOI_1 = f'{settings.DATACITE_PREFIX}/brt5-fn84'
TEST_DOI_2 = f'{settings.DATACITE_PREFIX}/d2bh-tv59'


class EndpointTestCase(APITestCase):
    fixtures = [
        'test_utils/fixtures/test_described_entity', 'test_utils/fixtures/test_actors',
        'test_utils/fixtures/test_persons', 'test_utils/fixtures/test_groups', 'test_utils/fixtures/test_users'
    ]

    def setUp(self):
        self.patcher_create = patch(
            'management.models.Manager.create_doi',
            return_value=TEST_CONCEPT_DOI_1,
            side_effect=[TEST_CONCEPT_DOI_1, TEST_DOI_1, TEST_DOI_2]
        )
        self.mock_create = self.patcher_create.start()

        self.patcher_update = patch(
            'management.models.update_concept_doi', return_value=None,
        )
        self.mock_update = self.patcher_update.start()

    def tearDown(self):
        self.patcher_create.stop()
        self.patcher_update.stop()


class SimpleSerializerTestCase(TestCase):
    fixtures = [
        'test_utils/fixtures/test_described_entity', 'test_utils/fixtures/test_actors',
        'test_utils/fixtures/test_persons', 'test_utils/fixtures/test_groups', 'test_utils/fixtures/test_users'
    ]


class SerializerTestCase(TestCase):
    fixtures = [
        'test_utils/fixtures/test_described_entity', 'test_utils/fixtures/test_actors',
        'test_utils/fixtures/test_persons', 'test_utils/fixtures/test_groups', 'test_utils/fixtures/test_users'
    ]

    def setUp(self):
        self.patcher_create = patch(
            'management.models.Manager.create_doi',
            return_value=TEST_CONCEPT_DOI_1,
            side_effect=[TEST_CONCEPT_DOI_1, TEST_DOI_1, TEST_DOI_2]
        )
        self.mock_create = self.patcher_create.start()

        self.patcher_update = patch(
            'management.models.update_concept_doi', return_value=None,
        )
        self.mock_update = self.patcher_update.start()

    def tearDown(self):
        self.patcher_create.stop()
        self.patcher_update.stop()

class MockRequest(object):
    def __init__(self, user=None, auth=None):
        self.auth = auth
        self.user = user


def import_metadata(metadata_list, context={}):
    metadata_records = list()
    for metadata in metadata_list:
        if metadata.endswith('.json'):
            with open(metadata, encoding='utf-8') as f:
                data = json.loads(f.read())
            LOGGER.info(f'Importing {metadata}...')
            try:
                user = get_system_user()
            except get_user_model().DoesNotExist:
                user = create_system_user()
            roles.assign_role(user, 'content_manager')
            data['management_object']['curator'] = user.pk
            serializer = MetadataRecordSerializer(data=data, context=context)
            serializer.is_valid()
            if serializer.errors:
                LOGGER.info(serializer.errors)
            serializer.save()
            metadata_records.append(serializer.instance)
    return metadata_records


def create_system_user():
    return get_user_model().objects.create(username=settings.SYSTEM_USER,
                                           is_superuser=True, is_staff=True,
                                           is_active=True)


def get_system_user():
    return get_user_model().objects.get(username=settings.SYSTEM_USER)


def api_call(method, endpoint, client, auth=None, data=None, data_format='json',
             **kwargs):
    auth = auth
    if method.lower() == 'get':
        request = client.get(endpoint, data=data, **kwargs)
    elif method.lower() == 'post':
        request = client.post(endpoint, data=data, format=data_format, **kwargs)
    elif method.lower() == 'put':
        request = client.put(endpoint, data=data, format=data_format, **kwargs)
    elif method.lower() == 'patch':
        request = client.patch(endpoint, data=data, format=data_format,
                               **kwargs)
    elif method.lower() == 'delete':
        request = client.delete(endpoint, data=data, format=data_format,
                                **kwargs)
    elif method.lower() == 'options':
        request = client.options(endpoint, data=data, format=data_format,
                                 **kwargs)
    else:
        raise ValueError(f'Invalid request method "{method}"')
    try:
        return {'status_code': request.status_code, 'json_content': request.json()}
    except ValueError:
        return {'status_code': request.status_code, 'content': request.content}


def get_test_user(role):
    mapping_user_dict = {
        'admin': get_user_model().objects.get(username='test-admin'),
        'content_manager': get_user_model().objects.get(username='test-content-manager@example.com'),
        'provider': get_user_model().objects.get(username=settings.TEST_AUTH_DATA['provider']['username']),
        'consumer': get_user_model().objects.get(username='test-backend'),
        'flat_rate_consumer': get_user_model().objects.get(username='test-backend-flatrate@example.com'),
        'legal_validator': get_user_model().objects.get(username='test-legal-validator'),
        'metadata_validator': get_user_model().objects.get(username='test-metadata-validator'),
        'technical_validator': get_user_model().objects.get(username='test-technical-validator'),
        'elg-system': get_user_model().objects.get(username=settings.SYSTEM_USER)
    }
    user = mapping_user_dict.get(role)
    auth = requests.post(settings.TEST_AUTH_DATA.get('auth_url'), data=settings.TEST_AUTH_DATA.get(role))
    token = auth.json().get('access_token')
    return user, token
