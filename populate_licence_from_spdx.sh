########################################################
# Clone/Pull SPDX git repo and get to the latest release
# and add licences that didn't exist before

# Clone SPDX licence repo
folder="spdx-licence-data-repo"
url="git@github.com:spdx/license-list-data.git"
echo "Cloning SPDX licence repo ${url}"
if ! git clone "${url}" "${folder}" 2>/dev/null && [ -d "${folder}" ] ; then
    echo "Clone failed because the folder ${folder} exists"
fi
# Retrieving latest tag
cd ${folder}
# Get new tags from remote
git fetch --tags
# Get latest tag name
latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)
echo "Checking out the Latest tag ${latestTag}"
# Checkout latest tag
git checkout $latestTag

##################################
# Populate SPDX licence into ELG catalogue
cd ..
python manage.py populate_licences ${folder}