<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:dc="http://purl.org/dc/elements/1.1/" 
    xmlns:lexvo="http://lexvo.org/ontology#"
    xmlns:datacite="http://purl.org/spar/datacite/"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
    xmlns:dcat="http://www.w3.org/ns/dcat#" 
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:elg="https://live.european-language-grid.eu" 
    xmlns:func="http://exslt.org/functions"
    xmlns:literal="http://www.essepuntato.it/2010/06/literalreification/"
    xmlns:locn="http://www.w3.org/ns/locn#" 
    xmlns:dcterms="http://purl.org/dc/terms/"
    extension-element-prefixes="xs rr bsbm-inst rev func" exclude-result-prefixes="func elg"
    version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    <func:function name="elg:value-map">
        <xsl:param name="element"/>
        <xsl:param name="key"/>
        <func:result>
            <xsl:value-of
                select="document('elg-rdf-value-map.xml')/map/*[name() = $element]/item[@key = $key]/text()"
            />
        </func:result>
    </func:function>
    <func:function name="elg:classification">
        <xsl:param name="scheme"/>
        <xsl:param name="value"/>
        <func:result>
            <xsl:value-of
                select="document('elg-rdf-value-map.xml')/map/classification/item[@scheme = $scheme and @value = $value]/text()"
            />
        </func:result>
    </func:function>
    <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
    <xsl:variable name="domain">
        <xsl:text>https://live.european-language-grid.eu/resource/</xsl:text>
    </xsl:variable>
    <xsl:template name="multilang">
        <xsl:param name="prefix"/>
        <xsl:param name="element"/>
        <xsl:element name="{concat($prefix, ':', $element)}">
            <xsl:attribute name="xml:lang">
                <xsl:value-of select="./@xml:lang"/>
            </xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="ms:MetadataRecord">
        <xsl:call-template name="MetadataRecord"/>
    </xsl:template>

    <xsl:template name="MetadataRecord">
        <rdf:RDF>
            <xsl:variable name="entity">
                <xsl:variable name="child" select="//ms:DescribedEntity/*[1]//ms:entityType"/>
                <xsl:choose>
                    <xsl:when test="$child = 'LanguageResource'">
                        <xsl:value-of
                            select="//ms:DescribedEntity/*[1]//ms:LRSubclass/*[1]/ms:lrType"/>
                    </xsl:when>
                    <xsl:when test="$child = 'Project'">
                        <xsl:text>Project</xsl:text>
                    </xsl:when>
                    <xsl:when test="$child = 'Organization'">
                        <xsl:text>Organization</xsl:text>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <ms:MetadataRecord>
               <!-- <xsl:attribute name="rdf:about">
                    <xsl:value-of
                        select="concat($domain, translate($entity, $uppercase, $lowercase), '/', ./@id)"
                    />
                </xsl:attribute>-->
                <ms:metadataCreationDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                    <xsl:value-of select="ms:metadataCreationDate"/>
                </ms:metadataCreationDate>
                <ms:metadataLastDateUpdated rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                    <xsl:value-of select="ms:metadataLastDateUpdated"/>
                </ms:metadataLastDateUpdated>
                <xsl:for-each select="ms:metadataCurator">
                    <xsl:call-template name="MetadataCurator"/>
                </xsl:for-each>
                <ms:compliesWith>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="ms:compliesWith"/>
                    </xsl:attribute>
                </ms:compliesWith>
                <xsl:for-each select="ms:metadataCreator">
                    <xsl:call-template name="MetadataCreator"/>
                </xsl:for-each>
                <xsl:for-each select="ms:sourceOfMetadataRecord">
                    <xsl:call-template name="SourceOfMDrecord"/>
                </xsl:for-each>
                <xsl:for-each select="ms:sourceMetadataRecord">
                    <xsl:call-template name="SourceMDrecord"/>
                </xsl:for-each>
                <xsl:for-each select="ms:revision">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">revision</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:DescribedEntity">
                    <xsl:call-template name="DescribedEntity">
                        <xsl:with-param name="entity-type" select="$entity"/>
                    </xsl:call-template>
                </xsl:for-each>
            </ms:MetadataRecord>
        </rdf:RDF>
    </xsl:template>
    <xsl:template name="Recommended">
        <xsl:param name="class"/>
        <xsl:param name="in-scheme"/>
        <xsl:choose>
            <xsl:when test="./*[name() = concat('ms:', $class, 'Recommended')][1]">
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="./*[name() = concat('ms:', $class, 'Recommended')][1]"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <skos:Concept>
                    <skos:prefLabel>
                        <xsl:value-of select="./*[name() = concat('ms:', $class, 'Other')][1]"/>
                    </skos:prefLabel>
                    <skos:inScheme>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="$in-scheme"/>
                        </xsl:attribute>
                    </skos:inScheme>
                </skos:Concept>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="LRCommons">
        <xsl:param name="parent"/>
        <xsl:for-each select="$parent/ms:resourceName">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">resourceName</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:resourceShortName">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">resourceShortName</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:description">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">description</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:LRIdentifier">
            <xsl:call-template name="HasIdentifier">
                <xsl:with-param name="component">lr</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:logo">
            <ms:logo rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                <xsl:value-of select="."/>
            </ms:logo>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:version">
            <ms:version rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                <xsl:value-of select="."/>
            </ms:version>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:versionDate">
            <ms:versionDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                <xsl:value-of select="."/>
            </ms:versionDate>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:updateFrequency">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">updateFrequency</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:revision">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">revision</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:additionalInfo">
            <xsl:choose>
                <xsl:when test="ms:landingPage">
                    <ms:landingPage rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                        <xsl:value-of select="ms:landingPage"/>
                    </ms:landingPage>
                </xsl:when>
                <xsl:otherwise>
                    <ms:email rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                        <xsl:value-of select="ms:email"/>
                    </ms:email>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:contact">
            <ms:contact>
                <xsl:call-template name="Actor"/>
            </ms:contact>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:mailingListName">
            <ms:mailingListName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                <xsl:value-of select="."/>
            </ms:mailingListName>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:discussionURL">
            <ms:discussionURL rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                <xsl:value-of select="."/>
            </ms:discussionURL>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:citationText">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">citationText</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:iprHolder">
            <ms:iprHolder>
                <xsl:call-template name="Actor"/>
            </ms:iprHolder>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:keyword">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">keyword</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:domain">
            <ms:domain>
                <xsl:message>
                    <xsl:value-of select="."/>
                </xsl:message>
                <xsl:call-template name="Classification">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">Domain</xsl:with-param>
                    <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                </xsl:call-template>
            </ms:domain>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:subject">
            <ms:subject>
                <xsl:call-template name="Classification">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">Subject</xsl:with-param>
                    <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                </xsl:call-template>
            </ms:subject>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:resourceProvider">
            <ms:resourceProvider>
                <xsl:call-template name="Actor"/>
            </ms:resourceProvider>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:publicationDate">
            <ms:publicationDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                <xsl:value-of select="."/>
            </ms:publicationDate>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:resourceCreator">
            <ms:resourceCreator>
                <xsl:call-template name="Actor"/>
            </ms:resourceCreator>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:creationStartDate">
            <ms:creationStartDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                <xsl:value-of select="."/>
            </ms:creationStartDate>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:creationEndDate">
            <ms:creationEndDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                <xsl:value-of select="."/>
            </ms:creationEndDate>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:fundingProject">
            <ms:fundingProject>
                <xsl:call-template name="Project"/>
            </ms:fundingProject>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:creationMode">
            <ms:creationMode>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:creationMode>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:creationDetails">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">creationDetails</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasOriginalSource">
            <ms:hasOriginalSource>
                <xsl:call-template name="GenericLR"/>
            </ms:hasOriginalSource>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:originalSourceDescription">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">originalSourceDescription</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isCreatedBy">
            <ms:isCreatedBy>
                <xsl:call-template name="GenericLR"/>
            </ms:isCreatedBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:intendedApplication">
            <ms:intendedApplication>
                <xsl:call-template name="Recommended">
                    <xsl:with-param name="class">LTClass</xsl:with-param>
                    <xsl:with-param name="in-scheme"
                        >http://w3id.org/meta-share/omtd-share/OperationScheme</xsl:with-param>
                </xsl:call-template>
            </ms:intendedApplication>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:actualUse">
            <ms:actualUse>
                <ms:ActualUse>
                    <!--<xsl:attribute name="rdf:about">
                        <xsl:value-of select="concat($domain, 'actual-use/', ./@id)"/>
                    </xsl:attribute>-->
                    <xsl:for-each select="ms:usedInApplication">
                        <ms:usedInApplication>
                            <xsl:call-template name="Recommended">
                                <xsl:with-param name="class">LTClass</xsl:with-param>
                                <xsl:with-param name="in-scheme"
                                    >http://w3id.org/meta-share/omtd-share/OperationScheme</xsl:with-param>
                            </xsl:call-template>
                        </ms:usedInApplication>
                    </xsl:for-each>
                    <xsl:for-each select="ms:hasOutcome">
                        <ms:hasOutcome>
                            <xsl:call-template name="GenericLR"/>
                        </ms:hasOutcome>
                    </xsl:for-each>
                    <xsl:for-each select="ms:usageProject">
                        <ms:usageProject>
                            <xsl:call-template name="Project"/>
                        </ms:usageProject>
                    </xsl:for-each>
                    <xsl:for-each select="ms:usageReport">
                        <ms:usageReport>
                            <xsl:call-template name="Document"/>
                        </ms:usageReport>
                    </xsl:for-each>
                    <xsl:for-each select="ms:actualUseDetails">
                        <xsl:call-template name="multilang">
                            <xsl:with-param name="prefix">ms</xsl:with-param>
                            <xsl:with-param name="element">actualUseDetails</xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </ms:ActualUse>
            </ms:actualUse>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:validated">
            <ms:validated rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                <xsl:value-of select="."/>
            </ms:validated>
        </xsl:for-each>
        <xsl:if test="$parent/ms:validation">
            <xsl:for-each select="$parent/ms:validation/ms:validated">
                <ms:validated rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:validated>
            </xsl:for-each>
        </xsl:if>
        <xsl:for-each select="$parent/ms:validation">
            <ms:validation>
                <ms:Validation>
                    <!--<xsl:attribute name="rdf:about">
                        <xsl:value-of select="concat($domain, 'validation/', ./@id)"/>
                    </xsl:attribute>-->
                    <xsl:for-each select="ms:validationReport">
                        <ms:validationReport>
                            <xsl:call-template name="Document"/>
                        </ms:validationReport>
                    </xsl:for-each>
                    <xsl:for-each select="ms:validationType">
                        <ms:validationType>
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="."/>
                            </xsl:attribute>
                        </ms:validationType>
                    </xsl:for-each>
                    <xsl:for-each select="ms:validationExtent">
                        <ms:validationExtent>
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="."/>
                            </xsl:attribute>
                        </ms:validationExtent>
                    </xsl:for-each>
                    <xsl:for-each select="ms:validationMode">
                        <ms:validationMode>
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="."/>
                            </xsl:attribute>
                        </ms:validationMode>
                    </xsl:for-each>
                    <xsl:for-each select="ms:isValidatedBy">
                        <ms:isValidatedBy>
                            <xsl:call-template name="GenericLR"/>
                        </ms:isValidatedBy>
                    </xsl:for-each>
                    <xsl:for-each select="ms:validator">
                        <ms:validator>
                            <xsl:call-template name="Actor"/>
                        </ms:validator>
                    </xsl:for-each>
                    <xsl:for-each select="ms:validationDetails">
                        <xsl:call-template name="multilang">
                            <xsl:with-param name="prefix">ms</xsl:with-param>
                            <xsl:with-param name="element">validationDetails</xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </ms:Validation>
            </ms:validation>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isDocumentedBy">
            <ms:isDocumentedBy>
                <xsl:call-template name="Document"/>
            </ms:isDocumentedBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isDecribedBy">
            <ms:isDecribedBy>
                <xsl:call-template name="Document"/>
            </ms:isDecribedBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isToBeCitedBy">
            <ms:isToBeCitedBy>
                <xsl:call-template name="Document"/>
            </ms:isToBeCitedBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isCitedBy">
            <ms:isCitedBy>
                <xsl:call-template name="Document"/>
            </ms:isCitedBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isReviewedBy">
            <ms:isReviewedBy>
                <xsl:call-template name="Document"/>
            </ms:isReviewedBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isPartOf">
            <ms:isPartOf>
                <xsl:call-template name="GenericLR"/>
            </ms:isPartOf>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isPartWith">
            <ms:isPartWith>
                <xsl:call-template name="GenericLR"/>
            </ms:isPartWith>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isSimilar">
            <ms:isSimilar>
                <xsl:call-template name="GenericLR"/>
            </ms:isSimilar>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isExactMatchWith">
            <ms:isExactMatchWith>
                <xsl:call-template name="GenericLR"/>
            </ms:isExactMatchWith>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasMetadata">
            <ms:hasMetadata>
                <ms:MetaDataRecord>
                    <!--<xsl:attribute name="rdf:about">
                        <xsl:value-of select="concat($domain, 'metadata-record/', ./@id)"/>
                    </xsl:attribute>-->
                    <xsl:for-each select="ms:MetadataRecordIdentifier">
                        <xsl:call-template name="HasIdentifier">
                            <xsl:with-param name="component">mdrecord</xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </ms:MetaDataRecord>
            </ms:hasMetadata>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isArchivedBy">
            <ms:isArchivedBy>
                <xsl:call-template name="GenericLR"/>
            </ms:isArchivedBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isContinuationOf">
            <ms:isContinuationOf>
                <xsl:call-template name="GenericLR"/>
            </ms:isContinuationOf>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:replaces">
            <ms:replaces>
                <xsl:call-template name="GenericLR"/>
            </ms:replaces>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isRelatedToLR">
            <ms:isRelatedToLR>
                <xsl:call-template name="GenericLR"/>
            </ms:isRelatedToLR>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isVersionOf">
            <ms:isVersionOf>
                <xsl:call-template name="GenericLR"/>
            </ms:isVersionOf>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:relation">
            <ms:relation>
                <ms:Relation>
                    <!--<xsl:attribute name="rdf:about">
                        <xsl:value-of select="concat($domain, 'relation/', ./@id)"/>
                    </xsl:attribute>-->
                    <xsl:for-each select="ms:relationType">
                        <xsl:call-template name="multilang">
                            <xsl:with-param name="prefix">ms</xsl:with-param>
                            <xsl:with-param name="element">relationType</xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                    <xsl:for-each select="ms:relatedLR">
                        <ms:relatedLR>
                            <xsl:call-template name="GenericLR"/>
                        </ms:relatedLR>
                    </xsl:for-each>
                </ms:Relation>
            </ms:relation>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:supportType">
            <ms:supportType>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:supportType>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:compliesWith">
            <ms:compliesWith>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:compliesWith>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:qualifiedAttribution">
            <ms:qualifiedAttribution>
                <xsl:call-template name="Attribution"/>
            </ms:qualifiedAttribution>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isCompatibleWith">
            <ms:isCompatibleWith>
                <xsl:call-template name="GenericLR"/>
            </ms:isCompatibleWith>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasPart">
            <ms:hasPart>
                <xsl:call-template name="GenericLR"/>
            </ms:hasPart>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasVersion">
            <ms:hasVersion>
                <xsl:call-template name="GenericLR"/>
            </ms:hasVersion>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isContinuedBy">
            <ms:isContinuedBy>
                <xsl:call-template name="GenericLR"/>
            </ms:isContinuedBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isReplacedWith">
            <ms:isReplacedWith>
                <xsl:call-template name="GenericLR"/>
            </ms:isReplacedWith>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasOutcome">
            <ms:hasOutcome>
                <xsl:call-template name="GenericLR"/>
            </ms:hasOutcome>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasAlignedVersion">
            <ms:hasAlignedVersion>
                <xsl:call-template name="GenericLR"/>
            </ms:hasAlignedVersion>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasAnnotatedVersion">
            <ms:hasAnnotatedVersion>
                <xsl:call-template name="GenericLR"/>
            </ms:hasAnnotatedVersion>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasConvertedVersion">
            <ms:hasConvertedVersion>
                <xsl:call-template name="GenericLR"/>
            </ms:hasConvertedVersion>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasArchived">
            <ms:hasArchived>
                <xsl:call-template name="GenericLR"/>
            </ms:hasArchived>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasEvaluated">
            <ms:hasEvaluated>
                <xsl:call-template name="GenericLR"/>
            </ms:hasEvaluated>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasElicited">
            <ms:hasElicited>
                <xsl:call-template name="GenericLR"/>
            </ms:hasElicited>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasCreated">
            <ms:hasCreated>
                <xsl:call-template name="GenericLR"/>
            </ms:hasCreated>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasAnnotated">
            <ms:hasAnnotated>
                <xsl:call-template name="GenericLR"/>
            </ms:hasAnnotated>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasAnalysed">
            <ms:hasAnalysed>
                <xsl:call-template name="GenericLR"/>
            </ms:hasAnalysed>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:hasValidated">
            <ms:hasValidated>
                <xsl:call-template name="GenericLR"/>
            </ms:hasValidated>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:accesses">
            <ms:accesses>
                <xsl:call-template name="GenericLR"/>
            </ms:accesses>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:displays">
            <ms:displays>
                <xsl:call-template name="GenericLR"/>
            </ms:displays>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:edits">
            <ms:edits>
                <xsl:call-template name="GenericLR"/>
            </ms:edits>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:queries">
            <ms:queries>
                <xsl:call-template name="GenericLR"/>
            </ms:queries>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isTypesystemOf">
            <ms:isTypesystemOf>
                <xsl:call-template name="GenericLR"/>
            </ms:isTypesystemOf>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isMLModelOf">
            <ms:isMLModelOf>
                <xsl:call-template name="GenericLR"/>
            </ms:isMLModelOf>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isAnnotationResourceOf">
            <ms:isAnnotationResourceOf>
                <xsl:call-template name="GenericLR"/>
            </ms:isAnnotationResourceOf>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isAnnotationSchemaOf">
            <ms:isAnnotationSchemaOf>
                <xsl:call-template name="GenericLR"/>
            </ms:isAnnotationSchemaOf>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isTagsetOf">
            <ms:isTagsetOf>
                <xsl:call-template name="GenericLR"/>
            </ms:isTagsetOf>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isRequiredLRBy">
            <ms:isRequiredLRBy>
                <xsl:call-template name="GenericLR"/>
            </ms:isRequiredLRBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isSupplementedBy">
            <ms:isSupplementedBy>
                <xsl:call-template name="GenericLR"/>
            </ms:isSupplementedBy>
        </xsl:for-each>
        <xsl:for-each select="$parent/ms:isSupplementTo">
            <ms:isSupplementTo>
                <xsl:call-template name="GenericLR"/>
            </ms:isSupplementTo>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="Attribution">
        <ms:Attribution>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'attribution/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:attributedAgent">
                <ms:attributedAgent>
                    <xsl:call-template name="Actor"/>
                </ms:attributedAgent>
            </xsl:for-each>
            <xsl:for-each select="ms:hadRole">
                <ms:hadRole>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">Role</xsl:with-param>
                    </xsl:call-template>
                </ms:hadRole>
            </xsl:for-each>
        </ms:Attribution>
    </xsl:template>
    <xsl:template name="Document">
        <ms:Document>
            <!-- <xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'document/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:title">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">title</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:DocumentIdentifier">
                <xsl:call-template name="HasIdentifier">
                    <xsl:with-param name="component">document</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
        </ms:Document>
    </xsl:template>
    <xsl:template name="Classification">
        <xsl:param name="prefix"/>
        <xsl:param name="element"/>
        <xsl:param name="scheme"/>
        <xsl:variable name="classification-scheme"
            select="child::*[substring(name(), string-length(name()) - string-length('Identifier') + 1) = 'Identifier']/@*[substring(name(), string-length(name()) - string-length('Scheme') + 1) = 'Scheme']"/>
        <xsl:variable name="identifier"
            select="child::*[substring(name(), string-length(name()) - string-length('Identifier') + 1) = 'Identifier']/text()"/>
        <!-- Try to get value map -->
        <xsl:variable name="mapped-value">
            <xsl:value-of select="elg:classification($classification-scheme, $identifier)"/>
        </xsl:variable>
        <xsl:message>
            <xsl:value-of select="$mapped-value"/>
        </xsl:message>
        <xsl:choose>
            <xsl:when test="$mapped-value != ''">
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="$mapped-value"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{concat($prefix, ':', $element)}">
                    <!--<xsl:attribute name="rdf:about">
                        <xsl:value-of
                            select="concat($domain, concat(translate($element, $uppercase, $lowercase), '/', ./@id))"
                        />
                    </xsl:attribute>-->
                    <xsl:for-each select="ms:categoryLabel">
                        <xsl:call-template name="multilang">
                            <xsl:with-param name="prefix">ms</xsl:with-param>
                            <xsl:with-param name="element">categoryLabel</xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name="multilang">
                            <xsl:with-param name="prefix">rdfs</xsl:with-param>
                            <xsl:with-param name="element">label</xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                    <xsl:for-each select="./*[name() = concat('ms:', $element, 'Identifier')]">
                        <xsl:call-template name="HasIdentifier">
                            <xsl:with-param name="component">classification</xsl:with-param>
                            <xsl:with-param name="inner" select="$element"/>
                            <xsl:with-param name="scheme">
                                <xsl:value-of select="$scheme"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="Actor">
        <xsl:choose>
            <xsl:when test="ms:Person">
                <xsl:for-each select="ms:Person">
                    <xsl:call-template name="Person"/>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="ms:Organization">
                <xsl:for-each select="ms:Organization">
                    <xsl:call-template name="Organization"/>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="ms:Group">
                <xsl:for-each select="ms:Group">
                    <xsl:call-template name="Group"/>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="Group">
        <ms:Group>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'organization/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:organizationName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">organizationName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:organizationShortName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">organizationShortName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:OrganizationIdentifier">
                <xsl:call-template name="HasIdentifier">
                    <xsl:with-param name="component">org</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:website">
                <ms:website rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                    <xsl:value-of select="."/>
                </ms:website>
            </xsl:for-each>
        </ms:Group>
    </xsl:template>
    <xsl:template name="MetadataCurator">
        <ms:metadataCurator>
            <xsl:call-template name="Person"/>
        </ms:metadataCurator>
    </xsl:template>
    <xsl:template name="MetadataCreator">
        <ms:metadataCreator>
            <xsl:call-template name="Person"/>
        </ms:metadataCreator>
    </xsl:template>
    <xsl:template name="Person">
        <ms:Person>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'person/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:email">
                <ms:email rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:email>
            </xsl:for-each>
            <xsl:for-each select="ms:PersonalIdentifier">
                <xsl:call-template name="HasIdentifier">
                    <xsl:with-param name="component">person</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:givenName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">givenName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:surname">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">surname</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
        </ms:Person>
    </xsl:template>
    <xsl:template name="HasIdentifier">
        <xsl:param name="component"/>
        <xsl:param name="inner"/>
        <xsl:param name="scheme"/>
        <datacite:hasIdentifier>
            <xsl:choose>
                <xsl:when test="$component = 'person'">
                    <xsl:call-template name="Identifier">
                        <xsl:with-param name="entity">Personal</xsl:with-param>
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$component = 'repository'">
                    <xsl:call-template name="Identifier">
                        <xsl:with-param name="entity">Repository</xsl:with-param>
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$component = 'lr'">
                    <xsl:call-template name="Identifier">
                        <xsl:with-param name="entity">LR</xsl:with-param>
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$component = 'mdrecord'">
                    <xsl:call-template name="Identifier">
                        <xsl:with-param name="entity">MetadataRecord</xsl:with-param>
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$component = 'licence'">
                    <xsl:call-template name="Identifier">
                        <xsl:with-param name="entity">Licence</xsl:with-param>
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$component = 'org'">
                    <xsl:call-template name="Identifier">
                        <xsl:with-param name="entity">Organization</xsl:with-param>
                        <xsl:with-param name="prefix">datacite</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$component = 'project'">
                    <xsl:call-template name="Identifier">
                        <xsl:with-param name="entity">Project</xsl:with-param>
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$component = 'document'">
                    <xsl:call-template name="Identifier">
                        <xsl:with-param name="entity">Document</xsl:with-param>
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$component = 'classification'">
                    <xsl:call-template name="Identifier">
                        <xsl:with-param name="entity" select="$inner"/>
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="rdfs">true</xsl:with-param>
                        <xsl:with-param name="scheme" select="$scheme"/>
                    </xsl:call-template>
                </xsl:when>

            </xsl:choose>
        </datacite:hasIdentifier>
    </xsl:template>
    <xsl:template name="Identifier">
        <xsl:param name="entity"/>
        <xsl:param name="prefix"/>
        <xsl:param name="rdfs"/>
        <xsl:param name="scheme"/>
        <xsl:variable name="selected-scheme">
            <xsl:choose>
                <xsl:when test="$scheme != ''">
                    <xsl:value-of select="$scheme"/>
                </xsl:when>
                <xsl:otherwise>IdentifierScheme</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:element name="{concat($prefix, ':', $entity, 'Identifier')}">
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of
                    select="concat($domain, translate($entity, $uppercase, $lowercase), '-identifier/', ./@id)"
                />
            </xsl:attribute>-->
            <xsl:choose>
                <xsl:when test="$rdfs = 'true'">
                    <literal:hasLiteralValue>
                        <xsl:value-of select="."/>
                    </literal:hasLiteralValue>
                    <ms:conformsTo>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of
                                select="./@*[name() = concat('ms:', $entity, $selected-scheme)]"/>
                        </xsl:attribute>
                    </ms:conformsTo>
                </xsl:when>
                <xsl:otherwise>
                    <literal:hasLiteralValue>
                        <xsl:value-of select="."/>
                    </literal:hasLiteralValue>
                    <datacite:usesIdentifierScheme>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of
                                select="./@*[name() = concat('ms:', $entity, $selected-scheme)]"/>
                        </xsl:attribute>
                    </datacite:usesIdentifierScheme>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
    <xsl:template name="SourceMDrecord">
        <ms:sourceMetadataRecord>
            <ms:MetaDataRecord>
                <!--<xsl:attribute name="rdf:about">
                    <xsl:value-of select="concat($domain, 'metadata-record/', ./@id)"/>
                </xsl:attribute>-->
                <xsl:for-each select="ms:MetadataRecordIdentifier">
                    <xsl:call-template name="HasIdentifier">
                        <xsl:with-param name="component">mdrecord</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
            </ms:MetaDataRecord>
        </ms:sourceMetadataRecord>
    </xsl:template>
    <xsl:template name="SourceOfMDrecord">
        <ms:sourceOfMetadataRecord>
            <dcat:Catalog>
                <!--<xsl:attribute name="rdf:about">
                    <xsl:value-of select="concat($domain, 'repository/', ./@id)"/>
                </xsl:attribute>-->
                <xsl:for-each select="ms:RepositoryIdentifier">
                    <xsl:call-template name="HasIdentifier">
                        <xsl:with-param name="component">repository</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:repositoryName">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">repositoryName</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:repositoryURL">
                    <foaf:homepage rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                        <xsl:value-of select="."/>
                    </foaf:homepage>
                </xsl:for-each>
            </dcat:Catalog>
        </ms:sourceOfMetadataRecord>
    </xsl:template>
    <xsl:template name="DescribedEntity">
        <xsl:param name="entity-type"/>
        <ms:describedEntity>
            <xsl:choose>
                <xsl:when test="$entity-type = 'Corpus'">
                    <xsl:call-template name="Corpus"/>
                </xsl:when>
                <xsl:when test="$entity-type = 'ToolService'">
                    <xsl:call-template name="ToolService"/>
                </xsl:when>
                <xsl:when test="$entity-type = 'LexicalConceptualResource'">
                    <xsl:call-template name="LexicalConceptualResource"/>
                </xsl:when>
                <xsl:when test="$entity-type = 'LanguageDescription'">
                    <xsl:call-template name="LanguageDescription"/>
                </xsl:when>
                <xsl:when test="$entity-type = 'Project'">
                    <xsl:for-each select="ms:Project">
                        <xsl:call-template name="Project"/>
                    </xsl:for-each>
                </xsl:when>
                <xsl:when test="$entity-type = 'Organization'">
                    <xsl:for-each select="ms:Organization">
                        <xsl:call-template name="Organization"/>
                    </xsl:for-each>
                </xsl:when>
            </xsl:choose>
        </ms:describedEntity>
    </xsl:template>
    <xsl:template name="Project">
        <ms:Project>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'project/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:ProjectIdentifier">
                <xsl:call-template name="HasIdentifier">
                    <xsl:with-param name="component">project</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:projectName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">projectName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:projectShortName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">projectShortName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:projectAlternativeName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">projectAlternativeName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:fundingType">
                <ms:fundingType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:fundingType>
            </xsl:for-each>
            <xsl:for-each select="ms:funder">
                <ms:funder>
                    <xsl:call-template name="Actor"/>
                </ms:funder>
            </xsl:for-each>
            <xsl:for-each select="ms:fundingCountry">
                <ms:fundingCountry>
                    <xsl:variable name="mapped-value">
                        <xsl:value-of select="elg:value-map('country', .)"/>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$mapped-value != ''">
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="elg:value-map('country', .)"/>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <dcterms:Location>
                                <rdfs:label>
                                    <xsl:value-of select="."/>
                                </rdfs:label>
                            </dcterms:Location>
                        </xsl:otherwise>
                    </xsl:choose>
                </ms:fundingCountry>
            </xsl:for-each>
            <xsl:for-each select="ms:projectStartDate">
                <ms:projectStartDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                    <xsl:value-of select="."/>
                </ms:projectStartDate>
            </xsl:for-each>
            <xsl:for-each select="ms:projectEndDate">
                <ms:projectEndDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                    <xsl:value-of select="."/>
                </ms:projectEndDate>
            </xsl:for-each>
            <xsl:for-each select="ms:email">
                <ms:email rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:email>
            </xsl:for-each>
            <xsl:for-each select="ms:website">
                <ms:website rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                    <xsl:value-of select="."/>
                </ms:website>
            </xsl:for-each>
            <xsl:for-each select="ms:logo">
                <ms:logo rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                    <xsl:value-of select="."/>
                </ms:logo>
            </xsl:for-each>
            <xsl:for-each select="ms:LTArea">
                <ms:LTArea>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">LTClass</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/OperationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:LTArea>
            </xsl:for-each>
            <xsl:for-each select="ms:domain">
                <ms:domain>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">Domain</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:domain>
            </xsl:for-each>
            <xsl:for-each select="ms:subject">
                <ms:subject>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">Subject</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:subject>
            </xsl:for-each>
            <xsl:for-each select="ms:keyword">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">keyword</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:socialMediaOccupationalAccount">
                <ms:socialMediaOccupationalAccount>
                    <xsl:call-template name="SocialMediaAccount"/>
                </ms:socialMediaOccupationalAccount>
            </xsl:for-each>
            <xsl:for-each select="ms:grantNumber">
                <ms:grantNumber rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:grantNumber>
            </xsl:for-each>
            <xsl:for-each select="ms:projectSummary">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">projectSummary</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:cost">
                <ms:cost>
                    <xsl:call-template name="Cost"/>
                </ms:cost>
            </xsl:for-each>
            <xsl:for-each select="ms:ecMaxContribution">
                <ms:ecMaxContribution>
                    <xsl:call-template name="Cost"/>
                </ms:ecMaxContribution>
            </xsl:for-each>
            <xsl:for-each select="ms:nationalMaxContribution">
                <ms:nationalMaxContribution>
                    <xsl:call-template name="Cost"/>
                </ms:nationalMaxContribution>
            </xsl:for-each>
            <xsl:for-each select="ms:fundingSchemeCategory">
                <ms:fundingSchemeCategory rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:fundingSchemeCategory>
            </xsl:for-each>
            <xsl:for-each select="ms:status">
                <ms:status rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:status>
            </xsl:for-each>
            <xsl:for-each select="ms:relatedCall">
                <ms:relatedCall rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:relatedCall>
            </xsl:for-each>
            <xsl:for-each select="ms:relatedProgramme">
                <ms:relatedProgramme rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:relatedProgramme>
            </xsl:for-each>
            <xsl:for-each select="ms:relatedSubprogramme">
                <ms:relatedSubprogramme rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:relatedSubprogramme>
            </xsl:for-each>
            <xsl:for-each select="ms:coordinator">
                <ms:coordinator>
                    <xsl:call-template name="Organization"/>
                </ms:coordinator>
            </xsl:for-each>
            <xsl:for-each select="ms:participatingOrganization">
                <ms:participatingOrganization>
                    <xsl:call-template name="Organization"/>
                </ms:participatingOrganization>
            </xsl:for-each>
            <xsl:for-each select="ms:isRelatedToDocument">
                <ms:isRelatedToDocument>
                    <xsl:call-template name="Document"/>
                </ms:isRelatedToDocument>
            </xsl:for-each>
            <xsl:for-each select="ms:projectReport">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">projectReport</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:replacesProject">
                <ms:replacesProject>
                    <xsl:call-template name="Project"/>
                </ms:replacesProject>
            </xsl:for-each>
        </ms:Project>
    </xsl:template>
    <xsl:template name="SocialMediaAccount">
        <ms:SocialMediaOccupationalAccount>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'social-media-occupational-account/', ./@id)"
                />
            </xsl:attribute>-->
            <xsl:for-each select="@ms:socialMediaOccupationalAccountType">
                <ms:socialMediaOccupationalAccountType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:socialMediaOccupationalAccountType>
            </xsl:for-each>
            <rdf:value rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                <xsl:value-of select="."/>
            </rdf:value>
        </ms:SocialMediaOccupationalAccount>
    </xsl:template>
    <xsl:template name="Organization">
        <ms:Organization>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'organization/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:OrganizationIdentifier">
                <xsl:call-template name="HasIdentifier">
                    <xsl:with-param name="component">org</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:organizationName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">organizationName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:organizationShortName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">organizationShortName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:organizationAlternativeName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">organizationAlternativeName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:organizationRole">
                <ms:organizationRole>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:organizationRole>
            </xsl:for-each>
            <xsl:for-each select="ms:organizationLegalStatus">
                <ms:organizationLegalStatus>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:organizationLegalStatus>
            </xsl:for-each>
            <xsl:for-each select="ms:startup">
                <ms:startup rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:startup>
            </xsl:for-each>
            <xsl:for-each select="ms:organizationBio">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">organizationBio</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:logo">
                <ms:logo rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                    <xsl:value-of select="."/>
                </ms:logo>
            </xsl:for-each>
            <xsl:for-each select="ms:LTArea">
                <ms:LTArea>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">LTClass</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/OperationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:LTArea>
            </xsl:for-each>
            <xsl:for-each select="ms:serviceOffered">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">serviceOffered</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:discipline">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">discipline</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>

            <xsl:for-each select="ms:domain">
                <ms:domain>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">Domain</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:domain>
            </xsl:for-each>
            <xsl:for-each select="ms:keyword">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">keyword</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:memberOfAssociation">
                <ms:memberOfAssociation>
                    <xsl:call-template name="Organization"/>
                </ms:memberOfAssociation>
            </xsl:for-each>
            <xsl:for-each select="ms:email">
                <ms:email rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:email>
            </xsl:for-each>
            <xsl:for-each select="ms:website">
                <ms:website rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                    <xsl:value-of select="."/>
                </ms:website>
            </xsl:for-each>
            <xsl:for-each select="ms:headOfficeAddress">
                <ms:headOfficeAddress>
                    <xsl:call-template name="AddressSet"/>
                </ms:headOfficeAddress>
            </xsl:for-each>
            <xsl:for-each select="ms:otherOfficeAddress">
                <ms:otherOfficeAddress>
                    <xsl:call-template name="AddressSet"/>
                </ms:otherOfficeAddress>
            </xsl:for-each>
            <xsl:for-each select="ms:telephoneNumber">
                <ms:telephoneNumber rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:telephoneNumber>
            </xsl:for-each>
            <xsl:for-each select="ms:faxNumber">
                <ms:faxNumber rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:faxNumber>
            </xsl:for-each>
            <xsl:for-each select="ms:socialMediaOccupationalAccount">
                <ms:socialMediaOccupationalAccount>
                    <xsl:call-template name="SocialMediaAccount"/>
                </ms:socialMediaOccupationalAccount>
            </xsl:for-each>
            <xsl:for-each select="ms:divisionCategory">
                <ms:divisionCategory>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:divisionCategory>
            </xsl:for-each>
            <xsl:for-each select="ms:isDivisionOf">
                <ms:isDivisionOf>
                    <xsl:call-template name="Organization"/>
                </ms:isDivisionOf>
            </xsl:for-each>
            <xsl:for-each select="ms:replacesOrganization">
                <ms:replacesOrganization>
                    <xsl:call-template name="Organization"/>
                </ms:replacesOrganization>
            </xsl:for-each>
        </ms:Organization>
    </xsl:template>
    <xsl:template name="AddressSet">
        <ms:AddressSet>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'address-set/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:address">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">address</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:region">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">region</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:zipCode">
                <ms:zipCode rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:zipCode>
            </xsl:for-each>
            <xsl:for-each select="ms:city">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">city</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:country">
                <ms:country>
                    <xsl:variable name="mapped-value">
                        <xsl:value-of select="elg:value-map('country', .)"/>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$mapped-value != ''">
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="elg:value-map('country', .)"/>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <dcterms:Location>
                                <rdfs:label>
                                    <xsl:value-of select="."/>
                                </rdfs:label>
                            </dcterms:Location>
                        </xsl:otherwise>
                    </xsl:choose>
                </ms:country>
            </xsl:for-each>
        </ms:AddressSet>
    </xsl:template>
    <xsl:template name="ToolService">
        <ms:ToolService>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'lr/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:call-template name="LRCommons">
                <xsl:with-param name="parent" select="ms:LanguageResource"/>
            </xsl:call-template>
            <xsl:for-each select="//ms:LRSubclass/ms:ToolService">
                <xsl:for-each select="ms:function">
                    <ms:function>
                        <xsl:call-template name="Recommended">
                            <xsl:with-param name="class">LTClass</xsl:with-param>
                            <xsl:with-param name="in-scheme"
                                >http://w3id.org/meta-share/omtd-share/OperationScheme</xsl:with-param>
                        </xsl:call-template>
                    </ms:function>
                </xsl:for-each>
                <xsl:for-each select="ms:SoftwareDistribution">
                    <xsl:call-template name="SoftwareDistribution"/>
                </xsl:for-each>
                <xsl:for-each select="ms:languageDependent">
                    <ms:languageDependent rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                        <xsl:value-of select="."/>
                    </ms:languageDependent>
                </xsl:for-each>
                <xsl:for-each select="ms:inputContentResource">
                    <ms:inputContentResource>
                        <xsl:call-template name="ProcessingResource"/>
                    </ms:inputContentResource>
                </xsl:for-each>
                <xsl:for-each select="ms:outputResource">
                    <ms:outputResource>
                        <xsl:call-template name="ProcessingResource"/>
                    </ms:outputResource>
                </xsl:for-each>
                <xsl:for-each select="ms:requiresLR">
                    <ms:requiresLR>
                        <xsl:call-template name="GenericLR"/>
                    </ms:requiresLR>
                </xsl:for-each>
                <xsl:for-each select="ms:typesystem">
                    <ms:typesystem>
                        <xsl:call-template name="GenericLR"/>
                    </ms:typesystem>
                </xsl:for-each>
                <xsl:for-each select="ms:annotationSchema">
                    <ms:annotationSchema>
                        <xsl:call-template name="GenericLR"/>
                    </ms:annotationSchema>
                </xsl:for-each>
                <xsl:for-each select="ms:annotationResource">
                    <ms:annotationResource>
                        <xsl:call-template name="GenericLR"/>
                    </ms:annotationResource>
                </xsl:for-each>
                <xsl:for-each select="ms:mlModel">
                    <ms:mlModel>
                        <xsl:call-template name="GenericLR"/>
                    </ms:mlModel>
                </xsl:for-each>
                <xsl:for-each select="ms:developmentFramework">
                    <ms:developmentFramework>
                        <xsl:call-template name="Recommended">
                            <xsl:with-param name="class">DevelopmentFramework</xsl:with-param>
                            <xsl:with-param name="in-scheme"
                                >http://w3id.org/meta-share/meta-share/DevelopmentFrameworkScheme</xsl:with-param>
                        </xsl:call-template>
                    </ms:developmentFramework>
                </xsl:for-each>
                <xsl:for-each select="ms:framework">
                    <ms:framework>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:framework>
                </xsl:for-each>
                <xsl:for-each select="ms:formalism">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">formalism</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:method">
                    <ms:method>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:method>
                </xsl:for-each>
                <xsl:for-each select="ms:implementationLanguage">
                    <ms:implementationLanguage
                        rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                        <xsl:value-of select="."/>
                    </ms:implementationLanguage>
                </xsl:for-each>
                <xsl:for-each select="ms:requiresSoftware">
                    <ms:requiresSoftware rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                        <xsl:value-of select="."/>
                    </ms:requiresSoftware>
                </xsl:for-each>
                <xsl:for-each select="ms:requiredHardware">
                    <ms:requiredHardware>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:requiredHardware>
                </xsl:for-each>
                <xsl:for-each select="ms:runningEnvironmentDetails">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">runningEnvironmentDetails</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:runningTime">
                    <ms:runningTime rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                        <xsl:value-of select="."/>
                    </ms:runningTime>
                </xsl:for-each>
                <xsl:for-each select="ms:trl">
                    <ms:trl>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:trl>
                </xsl:for-each>
                <xsl:for-each select="ms:evaluated">
                    <ms:evaluated rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                        <xsl:value-of select="."/>
                    </ms:evaluated>
                </xsl:for-each>
                <xsl:for-each select="ms:evaluation">
                    <ms:evaluation>
                        <xsl:call-template name="Evaluation"/>
                    </ms:evaluation>
                </xsl:for-each>
                <xsl:for-each select="ms:previousAnnotationTypesPolicy">
                    <ms:previousAnnotationTypesPolicy>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:previousAnnotationTypesPolicy>
                </xsl:for-each>
                <xsl:for-each select="ms:parameter">
                    <ms:parameter>
                        <xsl:call-template name="Parameter"/>
                    </ms:parameter>
                </xsl:for-each>
            </xsl:for-each>
        </ms:ToolService>
    </xsl:template>
    <xsl:template name="Parameter">
        <ms:Parameter>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'parameter/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:parameterName">
                <ms:parameterName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:parameterName>
            </xsl:for-each>
            <xsl:for-each select="ms:parameterLabel">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">parameterLabel</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:parameterDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">parameterDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:parameterType">
                <ms:parameterType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:parameterType>
            </xsl:for-each>
            <xsl:for-each select="ms:optional">
                <ms:optional rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:optional>
            </xsl:for-each>
            <xsl:for-each select="ms:multiValue">
                <ms:multiValue rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:multiValue>
            </xsl:for-each>
            <xsl:for-each select="ms:defaultValue">
                <ms:defaultValue rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:defaultValue>
            </xsl:for-each>
            <xsl:for-each select="ms:dataFormat">
                <ms:format>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:EnumerationValue">
                <ms:enumerationValue>
                    <xsl:call-template name="EnumerationValue"/>
                </ms:enumerationValue>
            </xsl:for-each>
        </ms:Parameter>
    </xsl:template>
    <xsl:template name="EnumerationValue">
        <ms:EnumerationValue>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'enumeration-value/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:valueName">
                <ms:valueName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:valueName>
            </xsl:for-each>
            <xsl:for-each select="ms:valueLabel">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">valueLabel</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:valueDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">valueDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
        </ms:EnumerationValue>
    </xsl:template>
    <xsl:template name="Evaluation">
        <ms:Evaluation>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'evaluation/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:evaluationLevel">
                <ms:evaluationLevel>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:evaluationLevel>
            </xsl:for-each>
            <xsl:for-each select="ms:evaluationType">
                <ms:evaluationType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:evaluationType>
            </xsl:for-each>
            <xsl:for-each select="ms:evaluationCriterion">
                <ms:evaluationCriterion>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:evaluationCriterion>
            </xsl:for-each>
            <xsl:for-each select="ms:evaluationMeasure">
                <ms:evaluationMeasure>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:evaluationMeasure>
            </xsl:for-each>
            <xsl:for-each select="ms:goldStandardLocation">
                <ms:goldStandardLocation rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                    <xsl:value-of select="."/>
                </ms:goldStandardLocation>
            </xsl:for-each>
            <xsl:for-each select="ms:performanceIndicator">
                <ms:performanceIndicator>
                    <xsl:call-template name="PerformanceIndicator"/>
                </ms:performanceIndicator>
            </xsl:for-each>
            <xsl:for-each select="ms:evaluationReport">
                <ms:evaluationReport>
                    <xsl:call-template name="Document"/>
                </ms:evaluationReport>
            </xsl:for-each>
            <xsl:for-each select="ms:isEvaluatedBy">
                <ms:isEvaluatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isEvaluatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:evaluationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">evaluationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:evaluator">
                <ms:evaluator>
                    <xsl:call-template name="Actor"/>
                </ms:evaluator>
            </xsl:for-each>
        </ms:Evaluation>
    </xsl:template>
    <xsl:template name="PerformanceIndicator">
        <ms:PerformanceIndicator>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'performance-indicator/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:metric">
                <ms:metric>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:metric>
            </xsl:for-each>
            <xsl:for-each select="ms:measure">
                <ms:measure rdf:datatype="http://www.w3.org/2001/XMLSchema#float">
                    <xsl:value-of select="."/>
                </ms:measure>
            </xsl:for-each>
            <xsl:for-each select="ms:unitOfMeasureMetric">
                <ms:unitOfMeasureMetric rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:unitOfMeasureMetric>
            </xsl:for-each>
        </ms:PerformanceIndicator>
    </xsl:template>
    <xsl:template name="ProcessingResource">
        <ms:ProcessingResource>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'processing-resource/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:processingResourceType">
                <ms:processingResourceType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:processingResourceType>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:dataFormat">
                <ms:format>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:mimetype">
                <ms:mimetype>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mimetype>
            </xsl:for-each>
            <xsl:for-each select="ms:characterEncoding">
                <ms:characterEncoding>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:characterEncoding>
            </xsl:for-each>
            <xsl:for-each select="ms:sample">
                <ms:sample>
                    <xsl:call-template name="Sample"/>
                </ms:sample>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationType">
                <ms:annotationType>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">annotationType</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/AnnotationTypeScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:annotationType>
            </xsl:for-each>
            <xsl:for-each select="ms:segmentationLevel">
                <ms:segmentationLevel>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:segmentationLevel>
            </xsl:for-each>
            <xsl:for-each select="ms:typesystem">
                <ms:typesystem>
                    <xsl:call-template name="GenericLR"/>
                </ms:typesystem>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationSchema">
                <ms:annotationSchema>
                    <xsl:call-template name="GenericLR"/>
                </ms:annotationSchema>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationResource">
                <ms:annotationResource>
                    <xsl:call-template name="GenericLR"/>
                </ms:annotationResource>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">modalityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
        </ms:ProcessingResource>
    </xsl:template>
    <xsl:template name="Sample">
        <ms:Sample>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'sample/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:sampleText">
                <ms:sampleText rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:sampleText>
            </xsl:for-each>
            <xsl:for-each select="ms:samplesLocation">
                <ms:samplesLocation rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                    <xsl:value-of select="."/>
                </ms:samplesLocation>
            </xsl:for-each>
            <xsl:for-each select="ms:tag">
                <ms:tag rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:tag>
            </xsl:for-each>
        </ms:Sample>
    </xsl:template>
    <xsl:template name="SoftwareDistribution">
        <ms:distribution>
            <ms:SoftwareDistribution>
                <!--<xsl:attribute name="rdf:about">
                    <xsl:value-of select="concat($domain, 'software-distribution/', ./@id)"/>
                </xsl:attribute>-->
                <xsl:for-each select="ms:SoftwareDistributionForm">
                    <ms:distributionForm>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:distributionForm>
                </xsl:for-each>
                <xsl:for-each select="ms:executionLocation">
                    <ms:executionLocation rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                        <xsl:value-of select="."/>
                    </ms:executionLocation>
                </xsl:for-each>
                <xsl:for-each select="ms:downloadLocation">
                    <ms:downloadLocation rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                        <xsl:value-of select="."/>
                    </ms:downloadLocation>
                </xsl:for-each>
                <xsl:for-each select="ms:dockerDownloadLocation">
                    <ms:dockerDownloadLocation
                        rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                        <xsl:value-of select="."/>
                    </ms:dockerDownloadLocation>
                </xsl:for-each>
                <xsl:for-each select="ms:serviceAdapterDownloadLocation">
                    <ms:serviceAdapterDownloadLocation
                        rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                        <xsl:value-of select="."/>
                    </ms:serviceAdapterDownloadLocation>
                </xsl:for-each>
                <xsl:for-each select="ms:accessLocation">
                    <ms:accessLocation rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                        <xsl:value-of select="."/>
                    </ms:accessLocation>
                </xsl:for-each>
                <xsl:for-each select="ms:demoLocation">
                    <ms:demoLocation rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                        <xsl:value-of select="."/>
                    </ms:demoLocation>
                </xsl:for-each>
                <xsl:for-each select="ms:privateResource">
                    <ms:privateResource rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                        <xsl:value-of select="."/>
                    </ms:privateResource>
                </xsl:for-each>
                <xsl:for-each select="ms:isDescribedBy">
                    <ms:isDescribedBy>
                        <xsl:call-template name="Document"/>
                    </ms:isDescribedBy>
                </xsl:for-each>
                <xsl:for-each select="ms:additionalHWRequirements">
                    <ms:additionalHWRequirements
                        rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                        <xsl:value-of select="."/>
                    </ms:additionalHWRequirements>
                </xsl:for-each>
                <xsl:for-each select="ms:command">
                    <ms:command rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                        <xsl:value-of select="."/>
                    </ms:command>
                </xsl:for-each>
                <xsl:for-each select="ms:webServiceType">
                    <ms:webServiceType>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:webServiceType>
                </xsl:for-each>
                <xsl:for-each select="ms:operatingSystem">
                    <ms:operatingSystem>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:operatingSystem>
                </xsl:for-each>
                <xsl:for-each select="ms:packageFormat">
                    <ms:packageFormat>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:packageFormat>
                </xsl:for-each>
                <xsl:for-each select="ms:licenceTerms">
                    <ms:licenceTerms>
                        <xsl:call-template name="LicenceTerms"/>
                    </ms:licenceTerms>
                </xsl:for-each>
                <xsl:for-each select="ms:cost">
                    <ms:cost>
                        <xsl:call-template name="Cost"/>
                    </ms:cost>
                </xsl:for-each>
                <xsl:for-each select="ms:membershipInstitution">
                    <ms:membershipInstitution>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:membershipInstitution>
                </xsl:for-each>
                <xsl:for-each select="ms:attributionText">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">attributionText</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:copyrightStatement">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">copyrightStatement</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:availabilityStartDate">
                    <ms:availabilityStartDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                        <xsl:value-of select="."/>
                    </ms:availabilityStartDate>
                </xsl:for-each>
                <xsl:for-each select="ms:availabilityEndDate">
                    <ms:availabilityEndDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                        <xsl:value-of select="."/>
                    </ms:availabilityEndDate>
                </xsl:for-each>
                <xsl:for-each select="ms:distributionRightsHolder">
                    <ms:distributionRightsHolder>
                        <xsl:call-template name="Actor"/>
                    </ms:distributionRightsHolder>
                </xsl:for-each>
                <xsl:for-each select="ms:accessRights">
                    <ms:accessRights>
                        <xsl:call-template name="Classification">
                            <xsl:with-param name="prefix">ms</xsl:with-param>
                            <xsl:with-param name="element">AccessRightsStatement</xsl:with-param>
                            <xsl:with-param name="scheme">Scheme</xsl:with-param>
                        </xsl:call-template>
                    </ms:accessRights>
                </xsl:for-each>
            </ms:SoftwareDistribution>
        </ms:distribution>
    </xsl:template>
    <xsl:template name="LexicalConceptualResource">
        <ms:LexicalConceptualResource>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'lr/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:call-template name="LRCommons">
                <xsl:with-param name="parent" select="ms:LanguageResource"/>
            </xsl:call-template>
            <xsl:for-each select="//ms:LRSubclass/ms:LexicalConceptualResource">
                <xsl:for-each select="ms:lcrSubclass">
                    <ms:lcrSubclass>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:lcrSubclass>
                </xsl:for-each>
                <xsl:for-each select="ms:encodingLevel">
                    <ms:encodingLevel>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:encodingLevel>
                </xsl:for-each>
                <xsl:for-each select="ms:ContentType">
                    <ms:linguisticInformatuion>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:linguisticInformatuion>
                </xsl:for-each>
                <xsl:for-each select="ms:externalReference">
                    <ms:externalReference>
                        <xsl:call-template name="GenericLR"/>
                    </ms:externalReference>
                </xsl:for-each>
                <xsl:for-each select="ms:theoreticModel">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">theoreticModel</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:extratextualInformation">
                    <ms:extratextualInformation>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:extratextualInformation>
                </xsl:for-each>
                <xsl:for-each select="ms:extratextualInformationUnit">
                    <ms:extratextualInformationUnit>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:extratextualInformationUnit>
                </xsl:for-each>
                <xsl:for-each select="ms:unspecifiedPart">
                    <xsl:call-template name="UnspecifiedPart"/>
                </xsl:for-each>
                <xsl:for-each select="ms:LexicalConceptualResourceMediaPart">
                    <xsl:call-template name="MediaPart"/>
                </xsl:for-each>
                <xsl:for-each select="ms:DatasetDistribution">
                    <xsl:call-template name="DatasetDistribution"/>
                </xsl:for-each>
                <xsl:for-each select="ms:personalDataIncluded">
                    <ms:personalDataIncluded>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:personalDataIncluded>
                </xsl:for-each>
                <xsl:for-each select="ms:personalDataDetails">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">personalDataDetails</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:sensitiveDataIncluded">
                    <ms:sensitiveDataIncluded>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:sensitiveDataIncluded>
                </xsl:for-each>
                <xsl:for-each select="ms:sensitiveDataDetails">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">sensitiveDataDetails</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:anonymized">
                    <ms:anonymized>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:anonymized>
                </xsl:for-each>
                <xsl:for-each select="ms:anonymizationDetails">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">anonymizationDetails</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:isAnalysedBy">
                    <ms:isAnalysedBy>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isAnalysedBy>
                </xsl:for-each>
                <xsl:for-each select="ms:isEditedBy">
                    <ms:isEditedBy>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isEditedBy>
                </xsl:for-each>
                <xsl:for-each select="ms:isElicitedBy">
                    <ms:isElicitedBy>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isElicitedBy>
                </xsl:for-each>
            </xsl:for-each>
            <xsl:for-each select="ms:isConvertedVersionOf">
                <ms:isConvertedVersionOf>
                    <xsl:call-template name="GenericLR"/>
                </ms:isConvertedVersionOf>
            </xsl:for-each>
            <xsl:for-each select="ms:timeCoverage">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">timeCoverage</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:geographicCoverage">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">geographicCoverage</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:temporal">
                <xsl:call-template name="Period"/>
            </xsl:for-each>
            <xsl:for-each select="ms:spatial">
                <xsl:call-template name="Spatial"/>
            </xsl:for-each>
        </ms:LexicalConceptualResource>
    </xsl:template>
    <xsl:template name="LanguageDescription">
        <xsl:variable name="context"
            select="ms:LanguageResource/ms:LRSubclass/ms:LanguageDescription"/>
        <xsl:choose>
            <xsl:when test="$context/ms:LanguageDescriptionSubclass">
                <xsl:choose>
                    <xsl:when test="$context/ms:LanguageDescriptionSubclass/ms:Grammar">
                        <ms:Grammar>
                            <xsl:call-template name="LDDetails"/>
                        </ms:Grammar>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when
                                test="$context/ms:LanguageDescriptionSubclass/ms:Model/ms:NGramModel">
                                <ms:NGramModel>
                                    <xsl:call-template name="LDDetails"/>
                                </ms:NGramModel>
                            </xsl:when>
                            <xsl:otherwise>
                                <ms:Model>
                                    <xsl:call-template name="LDDetails"/>
                                </ms:Model>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <ms:LanguageDescription>
                    <xsl:call-template name="LDDetails"/>
                </ms:LanguageDescription>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="LDDetails">
        <!--<xsl:attribute name="rdf:about">
            <xsl:value-of select="concat($domain, 'lr/', ./@id)"/>
        </xsl:attribute>-->
        <xsl:call-template name="LRCommons">
            <xsl:with-param name="parent" select="ms:LanguageResource"/>
        </xsl:call-template>
        <xsl:variable name="context" select="ms:LanguageResource/ms:LRSubclass"/>
        <xsl:for-each select="$context/ms:LanguageDescription">
            <xsl:for-each select="ms:ldSubclass">
                <ms:ldSubclass>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:ldSubclass>
            </xsl:for-each>
            <xsl:for-each select="ms:LanguageDescriptionSubclass">
                <xsl:call-template name="LanguageDescriptionSubclass"/>
            </xsl:for-each>
            <xsl:for-each select="ms:requiresSoftware">
                <ms:requiresSoftware rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:requiresSoftware>
            </xsl:for-each>
            <xsl:for-each select="ms:requiredHardware">
                <ms:requiredHardware>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:requiredHardware>
            </xsl:for-each>
            <xsl:for-each select="ms:additionalHWRequirements">
                <ms:additionalHWRequirements rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:additionalHWRequirements>
            </xsl:for-each>
            <xsl:for-each select="ms:runningEnvironmentDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">runningEnvironmentDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:unspecifiedPart">
                <xsl:call-template name="UnspecifiedPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:LanguageDescriptionMediaPart">
                <xsl:call-template name="MediaPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:DatasetDistribution">
                <xsl:call-template name="DatasetDistribution"/>
            </xsl:for-each>
            <xsl:for-each select="ms:personalDataIncluded">
                <ms:personalDataIncluded>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:personalDataIncluded>
            </xsl:for-each>
            <xsl:for-each select="ms:personalDataDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">personalDataDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:sensitiveDataIncluded">
                <ms:sensitiveDataIncluded>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sensitiveDataIncluded>
            </xsl:for-each>
            <xsl:for-each select="ms:sensitiveDataDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">sensitiveDataDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:anonymized">
                <ms:anonymized>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:anonymized>
            </xsl:for-each>
            <xsl:for-each select="ms:anonymizationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">anonymizationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:isAnalysedBy">
                <ms:isAnalysedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isAnalysedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:isEditedBy">
                <ms:isEditedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isEditedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:isElicitedBy">
                <ms:isElicitedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isElicitedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:isConvertedVersionOf">
                <ms:isConvertedVersionOf>
                    <xsl:call-template name="GenericLR"/>
                </ms:isConvertedVersionOf>
            </xsl:for-each>
            <xsl:for-each select="ms:timeCoverage">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">timeCoverage</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:geographicCoverage">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">geographicCoverage</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:temporal">
                <xsl:call-template name="Period"/>
            </xsl:for-each>
            <xsl:for-each select="ms:spatial">
                <xsl:call-template name="Spatial"/>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="LanguageDescriptionSubclass">
        <xsl:for-each select="ms:Grammar">
            <xsl:call-template name="Grammar"/>
        </xsl:for-each>
        <xsl:for-each select="ms:Model">
            <xsl:call-template name="Model"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="Grammar">
        <xsl:for-each select="ms:encodingLevel">
            <ms:encodingLevel>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:encodingLevel>
        </xsl:for-each>
        <xsl:for-each select="ms:theoreticModel">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">theoreticModel</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:formalism">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">formalism</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:ldTask">
            <ms:ldTask>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:ldTask>
        </xsl:for-each>
        <xsl:for-each select="ms:grammaticalPhenomenaCoverage">
            <ms:grammaticalPhenomenaCoverage>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:grammaticalPhenomenaCoverage>
        </xsl:for-each>
        <xsl:for-each select="ms:weightedGrammar">
            <ms:weightedGrammar rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                <xsl:value-of select="."/>
            </ms:weightedGrammar>
        </xsl:for-each>
        <xsl:for-each select="ms:requiresLR">
            <ms:requiresLR>
                <xsl:call-template name="GenericLR"/>
            </ms:requiresLR>
        </xsl:for-each>
        <xsl:for-each select="ms:relatedLexiconType">
            <ms:relatedLexiconType>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:relatedLexiconType>
        </xsl:for-each>
        <xsl:for-each select="ms:attachedLexiconPosition">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">attachedLexiconPosition</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:compatibleLexiconType">
            <ms:compatibleLexiconType>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:compatibleLexiconType>
        </xsl:for-each>
        <xsl:for-each select="ms:robustness">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">robustness</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:shallowness">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">shallowness</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:output">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">output</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="RecommendedInstances">
        <xsl:param name="class"/>
        <xsl:param name="element-name"/>
        <xsl:choose>
            <xsl:when test="./*[name() = concat('ms:', $class, 'Recommended')][1]">
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="./*[name() = concat('ms:', $class, 'Recommended')][1]"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{$element-name}">
                    <skos:prefLabel>
                        <xsl:value-of select="./*[name() = concat('ms:', $class, 'Other')][1]"/>
                    </skos:prefLabel>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="Model">
        <xsl:for-each select="ms:modelType">
            <ms:modelType>
                <xsl:call-template name="RecommendedInstances">
                    <xsl:with-param name="class">modelType</xsl:with-param>
                    <xsl:with-param name="element-name">ms:ModelType</xsl:with-param>
                </xsl:call-template>
            </ms:modelType>
        </xsl:for-each>
        <xsl:for-each select="ms:modelFunction">
            <ms:modelFunction>
                <xsl:call-template name="Recommended">
                    <xsl:with-param name="class">modelFunction</xsl:with-param>
                    <xsl:with-param name="in-scheme"
                        >http://w3id.org/meta-share/omtd-share/ModelFunctionScheme</xsl:with-param>
                </xsl:call-template>
            </ms:modelFunction>
        </xsl:for-each>
        <xsl:for-each select="ms:modelVariant">
            <ms:modelVariant rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                <xsl:value-of select="."/>
            </ms:modelVariant>
        </xsl:for-each>
        <xsl:for-each select="ms:typesystem">
            <ms:typesystem>
                <xsl:call-template name="GenericLR"/>
            </ms:typesystem>
        </xsl:for-each>
        <xsl:for-each select="ms:annotationSchema">
            <ms:annotationSchema>
                <xsl:call-template name="GenericLR"/>
            </ms:annotationSchema>
        </xsl:for-each>
        <xsl:for-each select="ms:annotationResource">
            <ms:annotationResource>
                <xsl:call-template name="GenericLR"/>
            </ms:annotationResource>
        </xsl:for-each>
        <xsl:for-each select="ms:tagset">
            <ms:tagset>
                <xsl:call-template name="GenericLR"/>
            </ms:tagset>
        </xsl:for-each>
        <xsl:for-each select="ms:method">
            <ms:method>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:method>
        </xsl:for-each>
        <xsl:for-each select="ms:developmentFramework">
            <ms:developmentFramework>
                <xsl:call-template name="RecommendedInstances">
                    <xsl:with-param name="class">DevelopmentFramework</xsl:with-param>
                    <xsl:with-param name="element-name">ms:DevelopmentFramework</xsl:with-param>
                </xsl:call-template>
            </ms:developmentFramework>
        </xsl:for-each>
        <xsl:for-each select="ms:algorithm">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">algorithm</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:algorithmDetails">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">algorithmDetails</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:hasOriginalSource">
            <ms:hasOriginalSource>
                <xsl:call-template name="GenericLR"/>
            </ms:hasOriginalSource>
        </xsl:for-each>
        <xsl:for-each select="ms:trainingCorpusDetails">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">trainingCorpusDetails</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:trainingProcessDetails">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">trainingProcessDetails</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:biasDetails">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">biasDetails</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:requiresLR">
            <ms:requiresLR>
                <xsl:call-template name="GenericLR"/>
            </ms:requiresLR>
        </xsl:for-each>
        <xsl:for-each select="ms:NGramModel">
            <xsl:call-template name="NGramModel"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="NGramModel">
        <xsl:for-each select="ms:baseItem">
            <ms:baseItem>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:baseItem>
        </xsl:for-each>
        <xsl:for-each select="ms:order">
            <ms:order rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                <xsl:value-of select="."/>
            </ms:order>
        </xsl:for-each>
        <xsl:for-each select="ms:perplexity">
            <ms:perplexity rdf:datatype="http://www.w3.org/2001/XMLSchema#float">
                <xsl:value-of select="."/>
            </ms:perplexity>
        </xsl:for-each>
        <xsl:for-each select="ms:isFactored">
            <ms:isFactored rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                <xsl:value-of select="."/>
            </ms:isFactored>
        </xsl:for-each>
        <xsl:for-each select="ms:factor">
            <ms:factor rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                <xsl:value-of select="."/>
            </ms:factor>
        </xsl:for-each>
        <xsl:for-each select="ms:smoothing">
            <ms:smoothing rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                <xsl:value-of select="."/>
            </ms:smoothing>
        </xsl:for-each>
        <xsl:for-each select="ms:interpolated">
            <ms:interpolated rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                <xsl:value-of select="."/>
            </ms:interpolated>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="Corpus">
        <ms:Corpus>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'lr/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:call-template name="LRCommons">
                <xsl:with-param name="parent" select="ms:LanguageResource"/>
            </xsl:call-template>
            <xsl:for-each select="//ms:LRSubclass/ms:Corpus">
                <xsl:for-each select="ms:corpusSubclass">
                    <ms:corpusSubclass>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:corpusSubclass>
                </xsl:for-each>
                <xsl:for-each select="ms:unspecifiedPart">
                    <xsl:call-template name="UnspecifiedPart"/>
                </xsl:for-each>
                <xsl:for-each select="ms:CorpusMediaPart">
                    <xsl:call-template name="MediaPart"/>
                </xsl:for-each>
                <xsl:for-each select="ms:DatasetDistribution">
                    <xsl:call-template name="DatasetDistribution"/>
                </xsl:for-each>
                <xsl:for-each select="ms:personalDataIncluded">
                    <ms:personalDataIncluded>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:personalDataIncluded>
                </xsl:for-each>
                <xsl:for-each select="ms:personalDataDetails">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">personalDataDetails</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:sensitiveDataIncluded">
                    <ms:sensitiveDataIncluded>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:sensitiveDataIncluded>
                </xsl:for-each>
                <xsl:for-each select="ms:sensitiveDataDetails">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">sensitiveDataDetails</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:anonymized">
                    <ms:anonymized>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:anonymized>
                </xsl:for-each>
                <xsl:for-each select="ms:anonymizationDetails">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">anonymizationDetails</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:isAnalysedBy">
                    <ms:isAnalysedBy>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isAnalysedBy>
                </xsl:for-each>
                <xsl:for-each select="ms:isEditedBy">
                    <ms:isEditedBy>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isEditedBy>
                </xsl:for-each>
                <xsl:for-each select="ms:isElicitedBy">
                    <ms:isElicitedBy>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isElicitedBy>
                </xsl:for-each>
                <xsl:for-each select="ms:isAnnotatedVersionOf">
                    <ms:isAnnotatedVersionOf>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isAnnotatedVersionOf>
                </xsl:for-each>
                <xsl:for-each select="ms:isAlignedVersionOf">
                    <ms:isAlignedVersionOf>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isAlignedVersionOf>
                </xsl:for-each>
                <xsl:for-each select="ms:isConvertedVersionOf">
                    <ms:isConvertedVersionOf>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isConvertedVersionOf>
                </xsl:for-each>
                <xsl:for-each select="ms:timeCoverage">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">timeCoverage</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:geographicCoverage">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">geographicCoverage</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:temporal">
                    <xsl:call-template name="Period"/>
                </xsl:for-each>
                <xsl:for-each select="ms:spatial">
                    <xsl:call-template name="Spatial"/>
                </xsl:for-each>
                <xsl:for-each select="ms:register">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">register</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:userQuery">
                    <ms:userQuery rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                        <xsl:value-of select="."/>
                    </ms:userQuery>
                </xsl:for-each>
            </xsl:for-each>
        </ms:Corpus>
    </xsl:template>
    <xsl:template name="Spatial">
        <dcterms:spatial>
            <xsl:attribute name="rdf:resource">
                <xsl:value-of select="."/>
            </xsl:attribute>
        </dcterms:spatial>
    </xsl:template>
    <xsl:template name="DatasetDistribution">
        <ms:distribution>
            <ms:DatasetDistribution>
                <!--<xsl:attribute name="rdf:about">
                    <xsl:value-of select="concat($domain, 'dataset-distribution/', ./@id)"/>
                </xsl:attribute>-->
                <xsl:for-each select="ms:DatasetDistributionForm">
                    <ms:distributionForm>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:distributionForm>
                </xsl:for-each>
                <xsl:for-each select="ms:downloadLocation">
                    <ms:downloadLocation rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                        <xsl:value-of select="."/>
                    </ms:downloadLocation>
                </xsl:for-each>
                <xsl:for-each select="ms:accessLocation">
                    <ms:accessLocation rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                        <xsl:value-of select="."/>
                    </ms:accessLocation>
                </xsl:for-each>
                <xsl:for-each select="ms:privateResource">
                    <ms:privateResource rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                        <xsl:value-of select="."/>
                    </ms:privateResource>
                </xsl:for-each>
                <xsl:for-each select="ms:isAccessedBy">
                    <ms:isAccessedBy>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isAccessedBy>
                </xsl:for-each>
                <xsl:for-each select="ms:isDisplayedBy">
                    <ms:isDisplayedBy>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isDisplayedBy>
                </xsl:for-each>
                <xsl:for-each select="ms:isQueriedBy">
                    <ms:isQueriedBy>
                        <xsl:call-template name="GenericLR"/>
                    </ms:isQueriedBy>
                </xsl:for-each>
                <xsl:for-each select="ms:samplesLocation">
                    <ms:samplesLocation rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                        <xsl:value-of select="."/>
                    </ms:samplesLocation>
                </xsl:for-each>
                <xsl:for-each select="ms:packageFormat">
                    <ms:packageFormat>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:packageFormat>
                </xsl:for-each>
                <xsl:for-each select="ms:distributionTextFeature">
                    <ms:distributionFeature>
                        <xsl:call-template name="DistTextFeature"/>
                    </ms:distributionFeature>
                </xsl:for-each>
                <xsl:for-each select="ms:distributionTextNumericalFeature">
                    <ms:distributionFeature>
                        <xsl:call-template name="DistTextNumFeature"/>
                    </ms:distributionFeature>
                </xsl:for-each>
                <xsl:for-each select="ms:distributionAudioFeature">
                    <ms:distributionFeature>
                        <xsl:call-template name="DistAudioFeature"/>
                    </ms:distributionFeature>
                </xsl:for-each>
                <xsl:for-each select="ms:distributionImageFeature">
                    <ms:distributionFeature>
                        <xsl:call-template name="DistImageFeature"/>
                    </ms:distributionFeature>
                </xsl:for-each>
                <xsl:for-each select="ms:distributionVideoFeature">
                    <ms:distributionFeature>
                        <xsl:call-template name="DistVideoFeature"/>
                    </ms:distributionFeature>
                </xsl:for-each>
                <xsl:for-each select="ms:distributionUnspecifiedFeature">
                    <xsl:call-template name="DistUnspecifiedFeature"/>
                </xsl:for-each>
                <xsl:for-each select="ms:licenceTerms">
                    <ms:licenceTerms>
                        <xsl:call-template name="LicenceTerms"/>
                    </ms:licenceTerms>
                </xsl:for-each>
                <xsl:for-each select="ms:attributionText">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">attributionText</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:cost">
                    <ms:cost>
                        <xsl:call-template name="Cost"/>
                    </ms:cost>
                </xsl:for-each>
                <xsl:for-each select="ms:membershipInstitution">
                    <ms:membershipInstitution>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </ms:membershipInstitution>
                </xsl:for-each>
                <xsl:for-each select="ms:copyrightStatement">
                    <xsl:call-template name="multilang">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">copyrightStatement</xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:for-each select="ms:availabilityStartDate">
                    <ms:availabilityStartDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                        <xsl:value-of select="."/>
                    </ms:availabilityStartDate>
                </xsl:for-each>
                <xsl:for-each select="ms:availabilityEndDate">
                    <ms:availabilityEndDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                        <xsl:value-of select="."/>
                    </ms:availabilityEndDate>
                </xsl:for-each>
                <xsl:for-each select="ms:distributionRightsHolder">
                    <ms:distributionRightsHolder>
                        <xsl:call-template name="Actor"/>
                    </ms:distributionRightsHolder>
                </xsl:for-each>
                <xsl:for-each select="ms:accessRights">
                    <ms:accessRights>
                        <xsl:call-template name="Classification">
                            <xsl:with-param name="prefix">ms</xsl:with-param>
                            <xsl:with-param name="element">AccessRightsStatement</xsl:with-param>
                            <xsl:with-param name="scheme">Scheme</xsl:with-param>
                        </xsl:call-template>
                    </ms:accessRights>
                </xsl:for-each>
            </ms:DatasetDistribution>
        </ms:distribution>
    </xsl:template>
    <xsl:template name="DistUnspecifiedFeature">
        <xsl:for-each select="ms:size">
            <ms:size>
                <xsl:call-template name="Size"/>
            </ms:size>
        </xsl:for-each>
        <xsl:for-each select="ms:dataFormat">
            <ms:format>
                <xsl:call-template name="Recommended">
                    <xsl:with-param name="class">dataFormat</xsl:with-param>
                    <xsl:with-param name="in-scheme"
                        >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                </xsl:call-template>
            </ms:format>
        </xsl:for-each>
        <xsl:for-each select="ms:mimetype">
            <ms:mimetype>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:mimetype>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="LicenceTerms">
        <ms:LicenceTerms>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'licence/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:licenceTermsName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">licenceTermsName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:licenceTermsURL">
                <ms:licenceTermsURL rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">
                    <xsl:value-of select="."/>
                </ms:licenceTermsURL>
            </xsl:for-each>
            <xsl:for-each select="ms:conditionOfUse">
                <ms:conditionOfUse>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:conditionOfUse>
            </xsl:for-each>
            <xsl:for-each select="ms:licenceCategory">
                <ms:licenceCategory>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:licenceCategory>
            </xsl:for-each>
            <xsl:for-each select="ms:LicenceIdentifier">
                <xsl:call-template name="HasIdentifier">
                    <xsl:with-param name="component">licence</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
        </ms:LicenceTerms>
    </xsl:template>
    <xsl:template name="Cost">
        <ms:Cost>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'cost/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:amount">
                <ms:amount rdf:datatype="http://www.w3.org/2001/XMLSchema#float">
                    <xsl:value-of select="."/>
                </ms:amount>
            </xsl:for-each>
            <xsl:for-each select="ms:currency">
                <ms:currency>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:currency>
            </xsl:for-each>
        </ms:Cost>
    </xsl:template>
    <xsl:template name="Size">
        <ms:Size>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'size/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:amount">
                <ms:amount rdf:datatype="http://www.w3.org/2001/XMLSchema#float">
                    <xsl:value-of select="."/>
                </ms:amount>
            </xsl:for-each>
            <xsl:for-each select="ms:sizeUnit">
                <ms:sizeUnit>
                    <xsl:call-template name="RecommendedInstances">
                        <xsl:with-param name="class">sizeUnit</xsl:with-param>
                        <xsl:with-param name="element-name">ms:SizeUnit</xsl:with-param>
                    </xsl:call-template>
                </ms:sizeUnit>
            </xsl:for-each>
            <xsl:for-each select="ms:sizeText">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">sizeText</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:domain">
                <ms:domain>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">Domain</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:domain>
            </xsl:for-each>
            <xsl:for-each select="ms:TextGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">TextGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
            <xsl:for-each select="ms:AudioGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">AudioGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
            <xsl:for-each select="ms:SpeechGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">SpeechGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
            <xsl:for-each select="ms:VideoGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">VideoGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
            <xsl:for-each select="ms:ImageGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">ImageGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
        </ms:Size>
    </xsl:template>
    <xsl:template name="MS-LANG">
        <xsl:param name="map-to"/>
        <ms:Language>
            <!--<rdfs:label>TODO MAP id to language name from ISOVocabularies. LanguageId->name, regionID-> name, scriptId -> name, variantId -> name, glotto -> name, languageVariety(en)</rdfs:label>-->
            <ms:languageTag rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                <xsl:value-of select="ms:languageTag"/>
            </ms:languageTag>
            <ms:languageId rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                <xsl:value-of select="ms:languageId"/>
            </ms:languageId>
            <xsl:for-each select="ms:scriptId">
                <lexvo:usesScript>                    
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://lexvo.org/id/script/', .)"/>
                    </xsl:attribute>
                </lexvo:usesScript>
                <ms:scriptId rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:scriptId>
            </xsl:for-each>
            <xsl:for-each select="ms:regionId">
                <ms:usedInRegion>
                    <xsl:variable name="mapped-value">
                        <xsl:value-of select="elg:value-map('country', .)"/>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$mapped-value != ''">
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="elg:value-map('country', .)"/>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <dcterms:Location>
                                <rdfs:label>
                                    <xsl:value-of select="."/>
                                </rdfs:label>
                            </dcterms:Location>
                        </xsl:otherwise>
                    </xsl:choose>
                </ms:usedInRegion>
                <ms:regionId rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:regionId>
            </xsl:for-each>
            <xsl:for-each select="ms:variantId">
                <ms:variantId rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:variantId>
                <!--<ms:variant>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('https://w3id.org/meta-share/meta-share/', .)"
                        />
                    </xsl:attribute>
                </ms:variant>-->
            </xsl:for-each>
            <xsl:for-each select="ms:glottologCode">
                <ms:glottologCode rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:glottologCode>
            </xsl:for-each>
            <xsl:for-each select="ms:languageVarietyName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">languageVarietyName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:if test="ms:glottologCode and $map-to = 'glottocode'">
                <skos:broadMatch>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of
                            select="concat('https://glottolog.org/resource/languoid/id/', ms:glottologCode)"
                        />
                    </xsl:attribute>
                </skos:broadMatch>
            </xsl:if>
            <xsl:if test="$map-to = ''">
                <skos:broadMatch>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="elg:value-map('lexvo', ms:languageId)"/>
                    </xsl:attribute>
                </skos:broadMatch>
            </xsl:if>
        </ms:Language>
    </xsl:template>
    <xsl:template name="Language">
        <xsl:choose>
            <xsl:when
                test="count(child::*) = 2 or (count(child::*) = 3 and ms:glottologCode) and ms:languageId != 'mis'">
                <xsl:variable name="lexvo-value">
                    <xsl:value-of select="elg:value-map('lexvo', ms:languageId)"/>
                </xsl:variable>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="elg:value-map('lexvo', ms:languageId)"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="count(child::*) >= 3 and ms:languageId != 'mis'">
                <xsl:call-template name="MS-LANG"/>
            </xsl:when>
            <xsl:when test="ms:languageId = 'mis'">
                <xsl:choose>
                    <xsl:when test="count(child::*) = 2">
                        <xsl:attribute name="rdf:resource">
                            <xsl:text>http://www.lexvo.org/page/iso639-3/mis</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <xsl:when test="count(child::*) = 3 and ms:glottologCode">
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of
                                select="concat('https://glottolog.org/resource/languoid/id/', ms:glottologCode)"
                            />
                        </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="MS-LANG">
                            <xsl:with-param name="map-to">glottocode</xsl:with-param>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="DistAudioFeature">
        <ms:DistributionAudioFeature>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'distribution-audio-feature/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:size">
                <ms:size>
                    <xsl:call-template name="Size"/>
                </ms:size>
            </xsl:for-each>
            <xsl:for-each select="ms:durationOfAudio">
                <ms:durationOfAudio>
                    <xsl:call-template name="Duration"/>
                </ms:durationOfAudio>
            </xsl:for-each>
            <xsl:for-each select="ms:durationOfEffectiveSpeech">
                <ms:durationOfEffectiveSpeech>
                    <xsl:call-template name="Duration"/>
                </ms:durationOfEffectiveSpeech>
            </xsl:for-each>
            <xsl:for-each select="ms:dataFormat">
                <ms:format>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:audioFormat">
                <ms:format>
                    <xsl:call-template name="AudioFormat"/>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:mimetype">
                <ms:mimetype>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mimetype>
            </xsl:for-each>
        </ms:DistributionAudioFeature>
    </xsl:template>
    <xsl:template name="DistTextFeature">
        <ms:DistributionTextFeature>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'distribution-text-feature/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:size">
                <ms:size>
                    <xsl:call-template name="Size"/>
                </ms:size>
            </xsl:for-each>
            <xsl:for-each select="ms:dataFormat">
                <ms:format>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:mimetype">
                <ms:mimetype>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mimetype>
            </xsl:for-each>
            <xsl:for-each select="ms:characterEncoding">
                <ms:characterEncoding>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:characterEncoding>
            </xsl:for-each>
        </ms:DistributionTextFeature>
    </xsl:template>
    <xsl:template name="DistTextNumFeature">
        <ms:DistributionTextNumericalFeature>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of
                    select="concat($domain, 'distribution-text-numerical-feature/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:size">
                <ms:size>
                    <xsl:call-template name="Size"/>
                </ms:size>
            </xsl:for-each>
            <xsl:for-each select="ms:dataFormat">
                <ms:format>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:mimetype">
                <ms:mimetype>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mimetype>
            </xsl:for-each>
        </ms:DistributionTextNumericalFeature>
    </xsl:template>
    <xsl:template name="DistImageFeature">
        <ms:DistributionImageFeature>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'distribution-image-feature/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:size">
                <ms:size>
                    <xsl:call-template name="Size"/>
                </ms:size>
            </xsl:for-each>
            <xsl:for-each select="ms:dataFormat">
                <ms:format>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:imageFormat">
                <ms:format>
                    <xsl:call-template name="ImageFormat"/>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:mimetype">
                <ms:mimetype>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mimetype>
            </xsl:for-each>
        </ms:DistributionImageFeature>
    </xsl:template>
    <xsl:template name="ImageFormat">
        <ms:ImageFormat>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'video-format/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:dataFormat">
                <ms:dataFormat>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:dataFormat>
            </xsl:for-each>
            <xsl:for-each select="ms:colourSpace">
                <ms:colourSpace>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:colourSpace>
            </xsl:for-each>
            <xsl:for-each select="ms:colourDepth">
                <ms:colourDepth rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:colourDepth>
            </xsl:for-each>
            <xsl:for-each select="ms:resolution">
                <ms:resolution>
                    <xsl:call-template name="Resolution"/>
                </ms:resolution>
            </xsl:for-each>
            <xsl:for-each select="ms:visualModelling">
                <ms:visualModelling>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:visualModelling>
            </xsl:for-each>
            <xsl:for-each select="ms:compressed">
                <ms:compressed rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:compressed>
            </xsl:for-each>
            <xsl:for-each select="ms:compressionName">
                <ms:compressionName>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:compressionName>
            </xsl:for-each>
            <xsl:for-each select="ms:compressionLoss">
                <ms:compressionLoss rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:compressionLoss>
            </xsl:for-each>
            <xsl:for-each select="ms:rasterOrVectorGraphics">
                <ms:rasterOrVectorGraphics>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:rasterOrVectorGraphics>
            </xsl:for-each>
            <xsl:for-each select="ms:quality">
                <ms:quality>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:quality>
            </xsl:for-each>
        </ms:ImageFormat>
    </xsl:template>
    <xsl:template name="DistVideoFeature">
        <ms:DistributionVideoFeature>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'distribution-video-feature/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:size">
                <ms:size>
                    <xsl:call-template name="Size"/>
                </ms:size>
            </xsl:for-each>
            <xsl:for-each select="ms:dataFormat">
                <ms:format>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:videoFormat">
                <ms:format>
                    <xsl:call-template name="VideoFormat"/>
                </ms:format>
            </xsl:for-each>
            <xsl:for-each select="ms:mimetype">
                <ms:mimetype>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mimetype>
            </xsl:for-each>
        </ms:DistributionVideoFeature>
    </xsl:template>
    <xsl:template name="VideoFormat">
        <ms:VideoFormat>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'video-format/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:dataFormat">
                <ms:dataFormat>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:dataFormat>
            </xsl:for-each>
            <xsl:for-each select="ms:colourSpace">
                <ms:colourSpace>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:colourSpace>
            </xsl:for-each>
            <xsl:for-each select="ms:colourDepth">
                <ms:colourDepth rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:colourDepth>
            </xsl:for-each>
            <xsl:for-each select="ms:frameRate">
                <ms:frameRate rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:frameRate>
            </xsl:for-each>
            <xsl:for-each select="ms:resolution">
                <ms:resolution>
                    <xsl:call-template name="Resolution"/>
                </ms:resolution>
            </xsl:for-each>
            <xsl:for-each select="ms:visualModelling">
                <ms:visualModelling>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:visualModelling>
            </xsl:for-each>
            <xsl:for-each select="ms:fidelity">
                <ms:fidelity rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:fidelity>
            </xsl:for-each>
            <xsl:for-each select="ms:compressed">
                <ms:compressed rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:compressed>
            </xsl:for-each>
            <xsl:for-each select="ms:compressionName">
                <ms:compressionName>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:compressionName>
            </xsl:for-each>
            <xsl:for-each select="ms:compressionLoss">
                <ms:compressionLoss rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:compressionLoss>
            </xsl:for-each>
        </ms:VideoFormat>
    </xsl:template>
    <xsl:template name="Resolution">
        <ms:Resolution>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'resolution/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:sizeWidth">
                <ms:sizeWidth rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:sizeWidth>
            </xsl:for-each>
            <xsl:for-each select="ms:sizeHeight">
                <ms:sizeHeight rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:sizeHeight>
            </xsl:for-each>
            <xsl:for-each select="ms:resolutionStandard">
                <ms:resolutionStandard>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:resolutionStandard>
            </xsl:for-each>
        </ms:Resolution>
    </xsl:template>
    <xsl:template name="AudioFormat">
        <ms:AudioFormat>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'audio-format/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:dataFormat">
                <ms:dataFormat>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">dataFormat</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/DataFormatScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:dataFormat>
            </xsl:for-each>
            <xsl:for-each select="ms:signalEncoding">
                <ms:signalEncoding>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:signalEncoding>
            </xsl:for-each>
            <xsl:for-each select="ms:samplingRate">
                <ms:samplingRate rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:samplingRate>
            </xsl:for-each>
            <xsl:for-each select="ms:quantization">
                <ms:quantization rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:quantization>
            </xsl:for-each>
            <xsl:for-each select="ms:byteOrder">
                <ms:byteOrder>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:byteOrder>
            </xsl:for-each>
            <xsl:for-each select="ms:signConvention">
                <ms:signConvention>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:signConvention>
            </xsl:for-each>
            <xsl:for-each select="ms:audioQualityMeasureIncluded">
                <ms:audioQualityMeasureIncluded>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:audioQualityMeasureIncluded>
            </xsl:for-each>
            <xsl:for-each select="ms:numberOfTracks">
                <ms:numberOfTracks rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:numberOfTracks>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingQuality">
                <ms:recordingQuality>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:recordingQuality>
            </xsl:for-each>
            <xsl:for-each select="ms:compressed">
                <ms:compressed rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:compressed>
            </xsl:for-each>
            <xsl:for-each select="ms:compressionName">
                <ms:compressionName>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:compressionName>
            </xsl:for-each>
            <xsl:for-each select="ms:compressionLoss">
                <ms:compressionLoss rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:compressionLoss>
            </xsl:for-each>
        </ms:AudioFormat>
    </xsl:template>
    <xsl:template name="UnspecifiedPart">
        <xsl:for-each select="ms:lingualityType">
            <ms:lingualityType>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:lingualityType>
        </xsl:for-each>
        <xsl:for-each select="ms:multilingualityType">
            <ms:multilingualityType>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:multilingualityType>
        </xsl:for-each>
        <xsl:for-each select="ms:multilingualityTypeDetails">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ms:language">
            <ms:language>
                <xsl:call-template name="Language"/>
            </ms:language>
        </xsl:for-each>
        <xsl:for-each select="ms:metalanguage">
            <ms:metalanguage>
                <xsl:call-template name="Language"/>
            </ms:metalanguage>
        </xsl:for-each>
        <xsl:for-each select="ms:modalityType">
            <ms:modalityType>
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </ms:modalityType>
        </xsl:for-each>
        <xsl:for-each select="ms:modalityTypeDetails">
            <xsl:call-template name="multilang">
                <xsl:with-param name="prefix">ms</xsl:with-param>
                <xsl:with-param name="element">modalityTypeDetails</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="MediaPart">
        <ms:mediaPart>
            <xsl:for-each select="ms:CorpusTextPart">
                <xsl:call-template name="CorpusTextPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:CorpusAudioPart">
                <xsl:call-template name="CorpusAudioPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:CorpusVideoPart">
                <xsl:call-template name="CorpusVideoPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:CorpusImagePart">
                <xsl:call-template name="CorpusImagePart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:CorpusTextNumericalPart">
                <xsl:call-template name="CorpusTextNumericalPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:LexicalConceptualResourceTextPart">
                <xsl:call-template name="LexicalConceptualResourceTextPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:LexicalConceptualResourceAudioPart">
                <xsl:call-template name="LexicalConceptualResourceAudioPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:LexicalConceptualResourceVideoPart">
                <xsl:call-template name="LexicalConceptualResourceVideoPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:LexicalConceptualResourceImagePart">
                <xsl:call-template name="LexicalConceptualResourceImagePart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:LanguageDescriptionTextPart">
                <xsl:call-template name="LanguageDescriptionTextPart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:LanguageDescriptionImagePart">
                <xsl:call-template name="LanguageDescriptionImagePart"/>
            </xsl:for-each>
            <xsl:for-each select="ms:LanguageDescriptionVideoPart">
                <xsl:call-template name="LanguageDescriptionVideoPart"/>
            </xsl:for-each>
        </ms:mediaPart>
    </xsl:template>
    <xsl:template name="CorpusImagePart">
        <ms:CorpusImagePart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'corpus-image-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:ImageGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">ImageGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
            <xsl:for-each select="ms:annotation">
                <ms:annotation>
                    <xsl:call-template name="Annotation"/>
                </ms:annotation>
            </xsl:for-each>
            <xsl:for-each select="ms:typeOfImageContent">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">typeOfImageContent</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:textIncludedInImage">
                <ms:textIncludedInImage>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:textIncludedInImage>
            </xsl:for-each>
            <xsl:for-each select="ms:staticElement">
                <ms:staticElement>
                    <xsl:call-template name="StaticElement"/>
                </ms:staticElement>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDeviceType">
                <ms:capturingDeviceType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:capturingDeviceType>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDeviceTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">capturingDeviceTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">capturingDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingEnvironment">
                <ms:capturingEnvironment>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:capturingEnvironment>
            </xsl:for-each>
            <xsl:for-each select="ms:sensorTechnology">
                <ms:sensorTechnology rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:sensorTechnology>
            </xsl:for-each>
            <xsl:for-each select="ms:sceneIllumination">
                <ms:sceneIllumination>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sceneIllumination>
            </xsl:for-each>
            <xsl:for-each select="ms:creationMode">
                <ms:creationMode>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:creationMode>
            </xsl:for-each>
            <xsl:for-each select="ms:isCreatedBy">
                <ms:isCreatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isCreatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:hasOriginalSource">
                <ms:hasOriginalSource>
                    <xsl:call-template name="GenericLR"/>
                </ms:hasOriginalSource>
            </xsl:for-each>
            <xsl:for-each select="ms:originalSourceDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">originalSourceDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:syntheticData">
                <ms:syntheticData rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:syntheticData>
            </xsl:for-each>
            <xsl:for-each select="ms:creationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">creationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:linkToOtherMedia">
                <ms:linkToOtherMedia>
                    <xsl:call-template name="LinkToOtherMedia"/>
                </ms:linkToOtherMedia>
            </xsl:for-each>
        </ms:CorpusImagePart>
    </xsl:template>
    <xsl:template name="DynamicElement">
        <ms:DynamicElement>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'dynamic-element/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:typeOfElement">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">typeOfTextNumericalContent</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:bodyPart">
                <ms:bodyPart>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:bodyPart>
            </xsl:for-each>
            <xsl:for-each select="ms:distractor">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">distractor</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:interactiveMedia">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">interactiveMedia</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:faceView">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">faceView</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:faceExpression">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">faceExpression</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:bodyMovement">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">bodyMovement</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:gesture">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">gesture</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:handArmMovement">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">handArmMovement</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:handManipulation">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">handManipulation</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:headMovement">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">headMovement</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:eyeMovement">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">eyeMovement</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:posesPerSubject">
                <ms:posesPerSubject rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:posesPerSubject>
            </xsl:for-each>
        </ms:DynamicElement>
    </xsl:template>
    <xsl:template name="StaticElement">
        <ms:StaticElement>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'static-element/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:typeOfElement">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">typeOfTextNumericalContent</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:bodyPart">
                <ms:bodyPart>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:bodyPart>
            </xsl:for-each>
            <xsl:for-each select="ms:faceView">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">faceView</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:faceExpression">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">faceExpression</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:artifactPart">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">artifactPart</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:landscapePart">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">landscapePart</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:personDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">personDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:thingDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">thingDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:organizationDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">organizationDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:eventDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">eventDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
        </ms:StaticElement>
    </xsl:template>
    <xsl:template name="CorpusTextNumericalPart">
        <ms:CorpusTextNumericalPart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'corpus-text-numerical-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:annotation">
                <ms:annotation>
                    <xsl:call-template name="Annotation"/>
                </ms:annotation>
            </xsl:for-each>
            <xsl:for-each select="ms:typeOfTextNumericalContent">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">typeOfTextNumericalContent</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingDeviceType">
                <ms:recordingDeviceType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:recordingDeviceType>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingDeviceTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">recordingDeviceTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingPlatformSoftware">
                <ms:recordingPlatformSoftware rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:recordingPlatformSoftware>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingEnvironment">
                <ms:recordingEnvironment>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:recordingEnvironment>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannel">
                <ms:sourceChannel>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sourceChannel>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannelType">
                <ms:sourceChannelType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sourceChannelType>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannelName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">sourceChannelName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannelDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">sourceChannelDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:recorder">
                <ms:recorder>
                    <xsl:call-template name="Actor"/>
                </ms:recorder>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDeviceType">
                <ms:capturingDeviceType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:capturingDeviceType>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDeviceTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">capturingDeviceTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">capturingDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingEnvironment">
                <ms:capturingEnvironment>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:capturingEnvironment>
            </xsl:for-each>
            <xsl:for-each select="ms:sensorTechnology">
                <ms:sensorTechnology rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:sensorTechnology>
            </xsl:for-each>
            <xsl:for-each select="ms:sceneIllumination">
                <ms:sceneIllumination>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sceneIllumination>
            </xsl:for-each>
            <xsl:for-each select="ms:numberOfParticipants">
                <ms:numberOfParticipants rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:numberOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:ageGroupOfParticipants">
                <ms:ageGroupOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:ageGroupOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:ageRangeStartOfParticipants">
                <ms:ageRangeStartOfParticipants rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:ageRangeStartOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:ageRangeEndOfParticipants">
                <ms:ageRangeEndOfParticipants rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:ageRangeEndOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:sexOfParticipants">
                <ms:sexOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sexOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:originOfParticipants">
                <ms:originOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:originOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:dialectAccentOfParticipants">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">dialectAccentOfParticipants</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:geographicDistributionOfParticipants">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element"
                        >geographicDistributionOfParticipants</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:hearingImpairmentOfParticipants">
                <ms:hearingImpairmentOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:hearingImpairmentOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:speakingImpairmentOfParticipants">
                <ms:speakingImpairmentOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:speakingImpairmentOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:numberOfTrainedSpeakers">
                <ms:numberOfTrainedSpeakers rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:numberOfTrainedSpeakers>
            </xsl:for-each>
            <xsl:for-each select="ms:speechInfluence">
                <ms:speechInfluence>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:speechInfluence>
            </xsl:for-each>
            <xsl:for-each select="ms:creationMode">
                <ms:creationMode>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:creationMode>
            </xsl:for-each>
            <xsl:for-each select="ms:isCreatedBy">
                <ms:isCreatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isCreatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:hasOriginalSource">
                <ms:hasOriginalSource>
                    <xsl:call-template name="GenericLR"/>
                </ms:hasOriginalSource>
            </xsl:for-each>
            <xsl:for-each select="ms:originalSourceDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">originalSourceDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:syntheticData">
                <ms:syntheticData rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:syntheticData>
            </xsl:for-each>
            <xsl:for-each select="ms:creationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">creationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:linkToOtherMedia">
                <ms:linkToOtherMedia>
                    <xsl:call-template name="LinkToOtherMedia"/>
                </ms:linkToOtherMedia>
            </xsl:for-each>
        </ms:CorpusTextNumericalPart>
    </xsl:template>
    <xsl:template name="CorpusVideoPart">
        <ms:CorpusVideoPart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'corpus-video-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:VideoGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">VideoGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
            <xsl:for-each select="ms:annotation">
                <ms:annotation>
                    <xsl:call-template name="Annotation"/>
                </ms:annotation>
            </xsl:for-each>
            <xsl:for-each select="ms:typeOfVideoContent">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">typeOfVideoContent</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:textIncludedInVideo">
                <ms:textIncludedInVideo>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:textIncludedInVideo>
            </xsl:for-each>
            <xsl:for-each select="ms:dynamicElement">
                <ms:dynamicElement>
                    <xsl:call-template name="DynamicElement"/>
                </ms:dynamicElement>
            </xsl:for-each>
            <xsl:for-each select="ms:naturality">
                <ms:naturality>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:naturality>
            </xsl:for-each>
            <xsl:for-each select="ms:conversationalType">
                <ms:conversationalType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:conversationalType>
            </xsl:for-each>
            <xsl:for-each select="ms:scenarioType">
                <ms:scenarioType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:scenarioType>
            </xsl:for-each>
            <xsl:for-each select="ms:audience">
                <ms:audience>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:audience>
            </xsl:for-each>
            <xsl:for-each select="ms:interactivity">
                <ms:interactivity>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:interactivity>
            </xsl:for-each>
            <xsl:for-each select="ms:interaction">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">interaction</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingDeviceType">
                <ms:recordingDeviceType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:recordingDeviceType>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingDeviceTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">recordingDeviceTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingPlatformSoftware">
                <ms:recordingPlatformSoftware rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:recordingPlatformSoftware>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingEnvironment">
                <ms:recordingEnvironment>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:recordingEnvironment>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannel">
                <ms:sourceChannel>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sourceChannel>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannelType">
                <ms:sourceChannelType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sourceChannelType>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannelName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">sourceChannelName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannelDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">sourceChannelDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:recorder">
                <ms:recorder>
                    <xsl:call-template name="Actor"/>
                </ms:recorder>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDeviceType">
                <ms:capturingDeviceType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:capturingDeviceType>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDeviceTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">capturingDeviceTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">capturingDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingEnvironment">
                <ms:capturingEnvironment>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:capturingEnvironment>
            </xsl:for-each>
            <xsl:for-each select="ms:sensorTechnology">
                <ms:sensorTechnology rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:sensorTechnology>
            </xsl:for-each>
            <xsl:for-each select="ms:sceneIllumination">
                <ms:sceneIllumination>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sceneIllumination>
            </xsl:for-each>
            <xsl:for-each select="ms:numberOfParticipants">
                <ms:numberOfParticipants rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:numberOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:ageGroupOfParticipants">
                <ms:ageGroupOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:ageGroupOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:ageRangeStartOfParticipants">
                <ms:ageRangeStartOfParticipants rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:ageRangeStartOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:ageRangeEndOfParticipants">
                <ms:ageRangeEndOfParticipants rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:ageRangeEndOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:sexOfParticipants">
                <ms:sexOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sexOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:originOfParticipants">
                <ms:originOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:originOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:dialectAccentOfParticipants">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">dialectAccentOfParticipants</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:geographicDistributionOfParticipants">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element"
                        >geographicDistributionOfParticipants</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:hearingImpairmentOfParticipants">
                <ms:hearingImpairmentOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:hearingImpairmentOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:speakingImpairmentOfParticipants">
                <ms:speakingImpairmentOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:speakingImpairmentOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:numberOfTrainedSpeakers">
                <ms:numberOfTrainedSpeakers rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:numberOfTrainedSpeakers>
            </xsl:for-each>
            <xsl:for-each select="ms:speechInfluence">
                <ms:speechInfluence>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:speechInfluence>
            </xsl:for-each>
            <xsl:for-each select="ms:creationMode">
                <ms:creationMode>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:creationMode>
            </xsl:for-each>
            <xsl:for-each select="ms:isCreatedBy">
                <ms:isCreatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isCreatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:hasOriginalSource">
                <ms:hasOriginalSource>
                    <xsl:call-template name="GenericLR"/>
                </ms:hasOriginalSource>
            </xsl:for-each>
            <xsl:for-each select="ms:originalSourceDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">originalSourceDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:syntheticData">
                <ms:syntheticData rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:syntheticData>
            </xsl:for-each>
            <xsl:for-each select="ms:creationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">creationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:linkToOtherMedia">
                <ms:linkToOtherMedia>
                    <xsl:call-template name="LinkToOtherMedia"/>
                </ms:linkToOtherMedia>
            </xsl:for-each>
        </ms:CorpusVideoPart>
    </xsl:template>
    <xsl:template name="CorpusAudioPart">
        <ms:CorpusAudioPart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'corpus-audio-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:AudioGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">AudioGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
            <xsl:for-each select="ms:SpeechGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">SpeechGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
            <xsl:for-each select="ms:annotation">
                <ms:annotation>
                    <xsl:call-template name="Annotation"/>
                </ms:annotation>
            </xsl:for-each>
            <xsl:for-each select="ms:speechItem">
                <ms:speechItem>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:speechItem>
            </xsl:for-each>
            <xsl:for-each select="ms:nonSpeechItem">
                <ms:nonSpeechItem>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:nonSpeechItem>
            </xsl:for-each>
            <xsl:for-each select="ms:legend">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">legend</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:noiseLevel">
                <ms:noiseLevel>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:noiseLevel>
            </xsl:for-each>
            <xsl:for-each select="ms:naturality">
                <ms:naturality>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:naturality>
            </xsl:for-each>
            <xsl:for-each select="ms:conversationalType">
                <ms:conversationalType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:conversationalType>
            </xsl:for-each>
            <xsl:for-each select="ms:scenarioType">
                <ms:scenarioType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:scenarioType>
            </xsl:for-each>
            <xsl:for-each select="ms:audience">
                <ms:audience>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:audience>
            </xsl:for-each>
            <xsl:for-each select="ms:interactivity">
                <ms:interactivity>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:interactivity>
            </xsl:for-each>
            <xsl:for-each select="ms:interaction">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">interaction</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingDeviceType">
                <ms:recordingDeviceType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:recordingDeviceType>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingDeviceTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">recordingDeviceTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingPlatformSoftware">
                <ms:recordingPlatformSoftware rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:recordingPlatformSoftware>
            </xsl:for-each>
            <xsl:for-each select="ms:recordingEnvironment">
                <ms:recordingEnvironment>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:recordingEnvironment>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannel">
                <ms:sourceChannel>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sourceChannel>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannelType">
                <ms:sourceChannelType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sourceChannelType>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannelName">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">sourceChannelName</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:sourceChannelDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">sourceChannelDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:recorder">
                <ms:recorder>
                    <xsl:call-template name="Actor"/>
                </ms:recorder>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDeviceType">
                <ms:capturingDeviceType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:capturingDeviceType>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDeviceTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">capturingDeviceTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">capturingDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:capturingEnvironment">
                <ms:capturingEnvironment>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:capturingEnvironment>
            </xsl:for-each>
            <xsl:for-each select="ms:sensorTechnology">
                <ms:sensorTechnology rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                    <xsl:value-of select="."/>
                </ms:sensorTechnology>
            </xsl:for-each>
            <xsl:for-each select="ms:sceneIllumination">
                <ms:sceneIllumination>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sceneIllumination>
            </xsl:for-each>
            <xsl:for-each select="ms:numberOfParticipants">
                <ms:numberOfParticipants rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:numberOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:ageGroupOfParticipants">
                <ms:ageGroupOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:ageGroupOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:ageRangeStartOfParticipants">
                <ms:ageRangeStartOfParticipants rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:ageRangeStartOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:ageRangeEndOfParticipants">
                <ms:ageRangeEndOfParticipants rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:ageRangeEndOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:sexOfParticipants">
                <ms:sexOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:sexOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:originOfParticipants">
                <ms:originOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:originOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:dialectAccentOfParticipants">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">dialectAccentOfParticipants</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:geographicDistributionOfParticipants">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element"
                        >geographicDistributionOfParticipants</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:hearingImpairmentOfParticipants">
                <ms:hearingImpairmentOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:hearingImpairmentOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:speakingImpairmentOfParticipants">
                <ms:speakingImpairmentOfParticipants>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:speakingImpairmentOfParticipants>
            </xsl:for-each>
            <xsl:for-each select="ms:numberOfTrainedSpeakers">
                <ms:numberOfTrainedSpeakers rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="."/>
                </ms:numberOfTrainedSpeakers>
            </xsl:for-each>
            <xsl:for-each select="ms:speechInfluence">
                <ms:speechInfluence>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:speechInfluence>
            </xsl:for-each>
            <xsl:for-each select="ms:creationMode">
                <ms:creationMode>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:creationMode>
            </xsl:for-each>
            <xsl:for-each select="ms:isCreatedBy">
                <ms:isCreatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isCreatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:hasOriginalSource">
                <ms:hasOriginalSource>
                    <xsl:call-template name="GenericLR"/>
                </ms:hasOriginalSource>
            </xsl:for-each>
            <xsl:for-each select="ms:originalSourceDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">originalSourceDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:syntheticData">
                <ms:syntheticData rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:syntheticData>
            </xsl:for-each>
            <xsl:for-each select="ms:creationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">creationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:linkToOtherMedia">
                <ms:linkToOtherMedia>
                    <xsl:call-template name="LinkToOtherMedia"/>
                </ms:linkToOtherMedia>
            </xsl:for-each>
        </ms:CorpusAudioPart>
    </xsl:template>
    <xsl:template name="CorpusTextPart">
        <ms:CorpusTextPart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'corpus-text-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:textType">
                <ms:textType>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">TextType</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:textType>
            </xsl:for-each>
            <xsl:for-each select="ms:TextGenre">
                <ms:genre>
                    <xsl:call-template name="Classification">
                        <xsl:with-param name="prefix">ms</xsl:with-param>
                        <xsl:with-param name="element">TextGenre</xsl:with-param>
                        <xsl:with-param name="scheme">ClassificationScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:genre>
            </xsl:for-each>
            <xsl:for-each select="ms:annotation">
                <ms:annotation>
                    <xsl:call-template name="Annotation"/>
                </ms:annotation>
            </xsl:for-each>
            <xsl:for-each select="ms:creationMode">
                <ms:creationMode>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:creationMode>
            </xsl:for-each>
            <xsl:for-each select="ms:isCreatedBy">
                <ms:isCreatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isCreatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:hasOriginalSource">
                <ms:hasOriginalSource>
                    <xsl:call-template name="GenericLR"/>
                </ms:hasOriginalSource>
            </xsl:for-each>
            <xsl:for-each select="ms:originalSourceDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">originalSourceDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:syntheticData">
                <ms:syntheticData rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:syntheticData>
            </xsl:for-each>
            <xsl:for-each select="ms:creationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">creationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:linkToOtherMedia">
                <ms:linkToOtherMedia>
                    <xsl:call-template name="LinkToOtherMedia"/>
                </ms:linkToOtherMedia>
            </xsl:for-each>
        </ms:CorpusTextPart>
    </xsl:template>
    <xsl:template name="LanguageDescriptionTextPart">
        <ms:LanguageDescriptionTextPart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'ld-text-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:metalanguage">
                <ms:metalanguage>
                    <xsl:call-template name="Language"/>
                </ms:metalanguage>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:creationMode">
                <ms:creationMode>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:creationMode>
            </xsl:for-each>
            <xsl:for-each select="ms:creationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">creationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:isCreatedBy">
                <ms:isCreatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isCreatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:hasOriginalSource">
                <ms:hasOriginalSource>
                    <xsl:call-template name="GenericLR"/>
                </ms:hasOriginalSource>
            </xsl:for-each>
            <xsl:for-each select="ms:originalSourceDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">originalSourceDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:linkToOtherMedia">
                <ms:linkToOtherMedia>
                    <xsl:call-template name="LinkToOtherMedia"/>
                </ms:linkToOtherMedia>
            </xsl:for-each>
        </ms:LanguageDescriptionTextPart>
    </xsl:template>
    <xsl:template name="LanguageDescriptionImagePart">
        <ms:LanguageDescriptionImagePart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'ld-image-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:metalanguage">
                <ms:metalanguage>
                    <xsl:call-template name="Language"/>
                </ms:metalanguage>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:typeOfImageContent">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">typeOfImageContent</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:textIncludedInImage">
                <ms:textIncludedInImage>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:textIncludedInImage>
            </xsl:for-each>
            <xsl:for-each select="ms:staticElement">
                <ms:staticElement>
                    <xsl:call-template name="StaticElement"/>
                </ms:staticElement>
            </xsl:for-each>
            <xsl:for-each select="ms:creationMode">
                <ms:creationMode>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:creationMode>
            </xsl:for-each>
            <xsl:for-each select="ms:creationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">creationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:isCreatedBy">
                <ms:isCreatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isCreatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:hasOriginalSource">
                <ms:hasOriginalSource>
                    <xsl:call-template name="GenericLR"/>
                </ms:hasOriginalSource>
            </xsl:for-each>
            <xsl:for-each select="ms:originalSourceDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">originalSourceDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:linkToOtherMedia">
                <ms:linkToOtherMedia>
                    <xsl:call-template name="LinkToOtherMedia"/>
                </ms:linkToOtherMedia>
            </xsl:for-each>
        </ms:LanguageDescriptionImagePart>
    </xsl:template>
    <xsl:template name="LanguageDescriptionVideoPart">
        <ms:LanguageDescriptionVideoPart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'ld-video-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:metalanguage">
                <ms:metalanguage>
                    <xsl:call-template name="Language"/>
                </ms:metalanguage>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:typeOfVideoContent">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">typeOfVideoContent</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:textIncludedInVideo">
                <ms:textIncludedInVideo>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:textIncludedInVideo>
            </xsl:for-each>
            <xsl:for-each select="ms:dynamicElement">
                <ms:dynamicElement>
                    <xsl:call-template name="DynamicElement"/>
                </ms:dynamicElement>
            </xsl:for-each>
            <xsl:for-each select="ms:creationMode">
                <ms:creationMode>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:creationMode>
            </xsl:for-each>
            <xsl:for-each select="ms:creationDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">creationDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:isCreatedBy">
                <ms:isCreatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isCreatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:hasOriginalSource">
                <ms:hasOriginalSource>
                    <xsl:call-template name="GenericLR"/>
                </ms:hasOriginalSource>
            </xsl:for-each>
            <xsl:for-each select="ms:originalSourceDescription">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">originalSourceDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:linkToOtherMedia">
                <ms:linkToOtherMedia>
                    <xsl:call-template name="LinkToOtherMedia"/>
                </ms:linkToOtherMedia>
            </xsl:for-each>
        </ms:LanguageDescriptionVideoPart>
    </xsl:template>
    <xsl:template name="LexicalConceptualResourceTextPart">
        <ms:LexicalConceptualResourceTextPart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'lcr-text-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:metalanguage">
                <ms:metalanguage>
                    <xsl:call-template name="Language"/>
                </ms:metalanguage>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
        </ms:LexicalConceptualResourceTextPart>
    </xsl:template>
    <xsl:template name="LexicalConceptualResourceAudioPart">
        <ms:LexicalConceptualResourceAudioPart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'lcr-audio-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:metalanguage">
                <ms:metalanguage>
                    <xsl:call-template name="Language"/>
                </ms:metalanguage>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:speechItem">
                <ms:speechItem>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:speechItem>
            </xsl:for-each>
            <xsl:for-each select="ms:nonSpeechItem">
                <ms:nonSpeechItem>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:nonSpeechItem>
            </xsl:for-each>
            <xsl:for-each select="ms:legend">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">legend</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:noiseLevel">
                <ms:noiseLevel>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:noiseLevel>
            </xsl:for-each>
        </ms:LexicalConceptualResourceAudioPart>
    </xsl:template>
    <xsl:template name="LexicalConceptualResourceVideoPart">
        <ms:LexicalConceptualResourceVideoPart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'lcr-video-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:metalanguage">
                <ms:metalanguage>
                    <xsl:call-template name="Language"/>
                </ms:metalanguage>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:typeOfVideoContent">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">typeOfVideoContent</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:textIncludedInVideo">
                <ms:textIncludedInVideo>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:textIncludedInVideo>
            </xsl:for-each>
            <xsl:for-each select="ms:dynamicElement">
                <ms:dynamicElement>
                    <xsl:call-template name="DynamicElement"/>
                </ms:dynamicElement>
            </xsl:for-each>
        </ms:LexicalConceptualResourceVideoPart>
    </xsl:template>
    <xsl:template name="LexicalConceptualResourceImagePart">
        <ms:LexicalConceptualResourceImagePart>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'lcr-image-part/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:mediaType">
                <ms:mediaType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:mediaType>
            </xsl:for-each>
            <xsl:for-each select="ms:lingualityType">
                <ms:lingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:lingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityType">
                <ms:multilingualityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:multilingualityType>
            </xsl:for-each>
            <xsl:for-each select="ms:multilingualityTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">multilingualityTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:language">
                <ms:language>
                    <xsl:call-template name="Language"/>
                </ms:language>
            </xsl:for-each>
            <xsl:for-each select="ms:metalanguage">
                <ms:metalanguage>
                    <xsl:call-template name="Language"/>
                </ms:metalanguage>
            </xsl:for-each>
            <xsl:for-each select="ms:modalityType">
                <ms:modalityType>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:modalityType>
            </xsl:for-each>
            <xsl:for-each select="ms:typeOfImageContent">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">typeOfImageContent</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:textIncludedInImage">
                <ms:textIncludedInImage>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:textIncludedInImage>
            </xsl:for-each>
            <xsl:for-each select="ms:staticElement">
                <ms:staticElement>
                    <xsl:call-template name="StaticElement"/>
                </ms:staticElement>
            </xsl:for-each>
        </ms:LexicalConceptualResourceImagePart>
    </xsl:template>
    <xsl:template name="Annotation">
        <ms:Annotation>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'annotation/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:annotationType">
                <ms:annotationType>
                    <xsl:call-template name="Recommended">
                        <xsl:with-param name="class">annotationType</xsl:with-param>
                        <xsl:with-param name="in-scheme"
                            >http://w3id.org/meta-share/omtd-share/AnnotationTypeScheme</xsl:with-param>
                    </xsl:call-template>
                </ms:annotationType>
            </xsl:for-each>
            <xsl:for-each select="ms:annotatedElement">
                <ms:annotatedElement>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:annotatedElement>
            </xsl:for-each>
            <xsl:for-each select="ms:segmentationLevel">
                <ms:segmentationLevel>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:segmentationLevel>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationStandoff">
                <ms:annotationStandoff rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:annotationStandoff>
            </xsl:for-each>
            <xsl:for-each select="ms:guidelines">
                <ms:guidelines>
                    <xsl:call-template name="Document"/>
                </ms:guidelines>
            </xsl:for-each>
            <xsl:for-each select="ms:typesystem">
                <ms:typesystem>
                    <xsl:call-template name="GenericLR"/>
                </ms:typesystem>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationResource">
                <ms:annotationResource>
                    <xsl:call-template name="GenericLR"/>
                </ms:annotationResource>
            </xsl:for-each>
            <xsl:for-each select="ms:theoreticModel">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">theoreticModel</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationMode">
                <ms:annotationMode>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:annotationMode>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationModeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">annotationModeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:isAnnotatedBy">
                <ms:isAnnotatedBy>
                    <xsl:call-template name="GenericLR"/>
                </ms:isAnnotatedBy>
            </xsl:for-each>
            <xsl:for-each select="ms:annotator">
                <ms:annotator>
                    <xsl:call-template name="Actor"/>
                </ms:annotator>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationStartDate">
                <ms:annotationStartDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                    <xsl:value-of select="."/>
                </ms:annotationStartDate>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationEndDate">
                <ms:annotationEndDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                    <xsl:value-of select="."/>
                </ms:annotationEndDate>
            </xsl:for-each>
            <xsl:for-each select="ms:interannotatorAgreement">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">interannotatorAgreement</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:intraannotatorAgreement">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">intraannotatorAgreement</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:annotationReport">
                <ms:annotationReport>
                    <xsl:call-template name="Document"/>
                </ms:annotationReport>
            </xsl:for-each>
        </ms:Annotation>
    </xsl:template>
    <xsl:template name="LinkToOtherMedia">
        <ms:LinkToOtherMedia>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'link-to-other-media/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:otherMedia">
                <ms:otherMedia>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:otherMedia>
            </xsl:for-each>
            <xsl:for-each select="ms:mediaTypeDetails">
                <xsl:call-template name="multilang">
                    <xsl:with-param name="prefix">ms</xsl:with-param>
                    <xsl:with-param name="element">mediaTypeDetails</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="ms:synchronizedWithText">
                <ms:synchronizedWithText rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:synchronizedWithText>
            </xsl:for-each>
            <xsl:for-each select="ms:synchronizedWithAudio">
                <ms:synchronizedWithAudio rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:synchronizedWithAudio>
            </xsl:for-each>
            <xsl:for-each select="ms:synchronizedWithVideo">
                <ms:synchronizedWithVideo rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:synchronizedWithVideo>
            </xsl:for-each>
            <xsl:for-each select="ms:synchronizedWithImage">
                <ms:synchronizedWithImage rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:synchronizedWithImage>
            </xsl:for-each>
            <xsl:for-each select="ms:synchronizedWithTextNumerical">
                <ms:synchronizedWithTextNumerical
                    rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">
                    <xsl:value-of select="."/>
                </ms:synchronizedWithTextNumerical>
            </xsl:for-each>
        </ms:LinkToOtherMedia>
    </xsl:template>
    <xsl:template name="Duration">
        <ms:Duration>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'duration/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:for-each select="ms:amount">
                <ms:amount rdf:datatype="http://www.w3.org/2001/XMLSchema#float">
                    <xsl:value-of select="."/>
                </ms:amount>
            </xsl:for-each>
            <xsl:for-each select="ms:durationUnit">
                <ms:durationUnit>
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </ms:durationUnit>
            </xsl:for-each>
        </ms:Duration>
    </xsl:template>
    <xsl:template name="Period">
        <dcterms:temporal>
            <dcterms:PeriodOfTime>
                <!--<xsl:attribute name="rdf:about">
                    <xsl:value-of select="concat($domain, 'time-period/', ./@id)"/>
                </xsl:attribute>-->
                <dcat:startDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                    <xsl:value-of select="ms:startDate"/>
                </dcat:startDate>
                <dcat:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">
                    <xsl:value-of select="ms:endDate"/>
                </dcat:endDate>
            </dcterms:PeriodOfTime>
        </dcterms:temporal>
    </xsl:template>
    <xsl:template name="GenericLR">
        <ms:LanguageResource>
            <!--<xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($domain, 'lr/', ./@id)"/>
            </xsl:attribute>-->
            <xsl:call-template name="LRCommons">
                <xsl:with-param name="parent" select="."/>
            </xsl:call-template>
        </ms:LanguageResource>
    </xsl:template>
</xsl:stylesheet>
