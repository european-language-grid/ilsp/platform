#!/bin/sh
set -e

#Start cron daemon
crond

# In deploy of docker image just apply existing migrations
python manage.py migrate --noinput


echo "COLLECTING STATIC FILES"
python manage.py collectstatic --no-input

gunicorn catalogue_backend.wsgi:application -c gunicorn_config.py