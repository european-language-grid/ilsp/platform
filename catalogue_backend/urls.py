"""catalogue_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, register_converter
from graphene_django.views import GraphQLView
from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from catalogue_backend.settings.settings import HOME_PREFIX, STATIC_ROOT, DEBUG
from registry.utils.string_utils import HashIdConverter
from . import views

router = routers.DefaultRouter()
register_converter(HashIdConverter, "hashid")


urlpatterns = [
    path(f'{HOME_PREFIX}/', views.index),
    path(f'{HOME_PREFIX}/admin/', admin.site.urls),
    path(f'{HOME_PREFIX}/graphql/', GraphQLView.as_view(graphiql=True)),
    path(f'{HOME_PREFIX}/api/', include(router.urls)),
    path(f'{HOME_PREFIX}/api/accounts/', include('accounts.urls')),
    path(f'{HOME_PREFIX}/api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    path(f'{HOME_PREFIX}/api/registry/', include('registry.urls', namespace='registry')),
    path(f'{HOME_PREFIX}/api/management/', include(('management.urls', 'management'), namespace='management')),
    path(f'{HOME_PREFIX}/api/oai/', include(('oai.urls', 'oai'), namespace='oai')),
    path(f'{HOME_PREFIX}/api/processing/', include(('processing.urls', 'processing'), namespace='processing')),
    path(f'{HOME_PREFIX}/api/cnsmr/anltcs/', include(('analytics.urls', 'analytics'), namespace='analytics')),
    path(f'{HOME_PREFIX}/api/docs/', include_docs_urls(title='ELG Backend API')),
]

import django.views.static

urlpatterns += [
    path('static/', django.views.static.serve, {'document_root': STATIC_ROOT, 'show_indexes': DEBUG})
]


from management.views import permanent_download_url


urlpatterns += [
    path(f'{HOME_PREFIX}/storage/<hashid:pk>/<hashid:dist_pk>/', permanent_download_url, name='permanent_download_url')
]
