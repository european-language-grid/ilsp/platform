import graphene
import graphql_api.schema


class Query(graphql_api.schema.Query, graphene.ObjectType):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass
