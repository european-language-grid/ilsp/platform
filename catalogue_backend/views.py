from django.http import JsonResponse


def index(request):
    """
    The root of the platform should return a JSON response, since the
    platform is intended for only for REST calls
    """
    return JsonResponse({
        'message': 'Welcome to the European Language Grid Platform API',
        'api_url': '/api'
    })
