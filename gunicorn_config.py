import logging

from catalogue_backend.settings import settings

bind = '0.0.0.0:80'
workers = 4
timeout = 180
reload = True
raw_env = [
    'DJANGO_SETTINGS_MODULE=catalogue_backend.settings.settings'
]
loglevel = 'debug' if settings.DEBUG else 'info'
capture_output = True

LOGGER = logging.getLogger(__name__)
