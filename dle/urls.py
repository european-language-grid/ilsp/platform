from django.urls import path

from dle.views import language_dle_info, all_lang_info

app_name = 'dle'

urlpatterns = (
    path('', language_dle_info),
    path('lang_count_info/', all_lang_info, name='all_language_info'),
)
