from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

from .documents import DLEReportDocument


class DLEReportDocumentSerializer(DocumentSerializer):
    """Serializer for the MetadataDocument."""

    class Meta(object):
        """Meta options."""

        # Specify the correspondent document class
        document = DLEReportDocument

        # List the serializer fields. Note, that the order of the fields
        # is preserved in the ViewSet.
        fields = (
            'id',
            'resource_name',
            'resource_short_name',
            'lr_distinction',
            'resource_type',
            'subclass',
            'type_of_access',
            'media_type',
            'input_media_type',
            'output_media_type',
            'annotation_type',
            'languages',
            'language_id',
            'language_tag',
            'glottolog_code',
            'linguality_type',
            'functions',
            'domain',
            'language_independent',
            'dle_score',
            'publication_date'
        )
