from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import DLEReportDocumentView

router = DefaultRouter()
dml_report = router.register('', DLEReportDocumentView, basename='dle_report_index')

urlpatterns = [
    path('', include(router.urls))
]
