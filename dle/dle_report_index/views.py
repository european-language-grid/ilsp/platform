from django_elasticsearch_dsl_drf.constants import (
    LOOKUP_FILTER_TERMS,
    LOOKUP_FILTER_PREFIX, LOOKUP_FILTER_WILDCARD,
    LOOKUP_QUERY_IN, LOOKUP_QUERY_EXCLUDE
)
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
    CompoundSearchFilterBackend,
    FacetedSearchFilterBackend, MultiMatchSearchFilterBackend
)
from django_elasticsearch_dsl_drf.pagination import PageNumberPagination, LimitOffsetPagination
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet
from elasticsearch_dsl import TermsFacet
from rest_framework.permissions import AllowAny

from utils.index_backends.backends import LuceneStringSearchFilterBackend
from .documents import DLEReportDocument
from .serializers import DLEReportDocumentSerializer
from django_elasticsearch_dsl_drf.pagination import PageNumberPagination


class CustomPageNumberPagination(PageNumberPagination):
    """Custom page number pagination."""

    def get_paginated_response_context(self, data):
        __data = super(
            CustomPageNumberPagination,
            self
        ).get_paginated_response_context(data)
        __data.append(
            ('current_page', int(self.request.query_params.get('page', 1)))
        )
        __data.append(
            ('page_size', self.get_page_size(self.request))
        )

        return sorted(__data)


class DLEReportDocumentView(BaseDocumentViewSet):
    """The MetadataDocument view."""

    def __init__(self, *args, **kwargs):
        super(DLEReportDocumentView, self).__init__(*args, **kwargs)
        self.search = self.search.extra(track_total_hits=True)

    document = DLEReportDocument
    serializer_class = DLEReportDocumentSerializer
    pagination_class = LimitOffsetPagination
    permission_classes = (AllowAny,)
    lookup_field = 'id'
    filter_backends = [
        LuceneStringSearchFilterBackend,
        FilteringFilterBackend,
        FacetedSearchFilterBackend,
        CompoundSearchFilterBackend,
        MultiMatchSearchFilterBackend,
        OrderingFilterBackend,
        DefaultOrderingFilterBackend,
    ]

    simple_query_string_options = {
        "default_operator": "and",
    }

    # Define search fields
    search_fields = {}
    # Define filter fields
    filter_fields = {
        'id': None,
        'resource_name': 'resource_name.raw',
        'resource_type': 'resource_type.raw',
        'subclass': 'subclass.raw',
        'lr_distinction': 'lr_distinction.raw',
        'media_type': 'media_type.raw',
        'input_media_type': 'input_media_type.raw',
        'output_media_type': 'output_media_type.raw',
        'annotation_type': 'annotation_type.raw',
        'language': 'languages',
        'language_id': 'language_id',
        'language_tag': 'language_tag',
        'glottolog_code': 'glottolog_code',
        'type_of_access': 'type_of_access',
        'function': 'functions',
        'language_independent': 'language_independent',
        'linguality_type': 'linguality_type',
        'domain': 'domain',
        'publication_date': 'publication_date'
    }
    # Define ordering fields
    ordering_fields = {
        'id': 'id',
        'resource_name': 'resource_name.raw',
        'resource_type': 'resource_type.raw',
        'publication_date': 'publication_date'
    }
    # Specify default ordering
    ordering = ('_score', 'resource_type.raw', 'id')

    # multi_match_search_fields = (
    #     'resource_name',
    #     'description',
    # )

    nested_filter_fields = {}

    faceted_search_fields = {
        'type_of_access': {
            'field': 'type_of_access',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'annotation_type': {
            'field': 'annotation_type',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'domain': {
            'field': 'domain',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'media_type': {
            'field': 'media_type',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'input_media_type': {
            'field': 'input_media_type',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'output_media_type': {
            'field': 'output_media_type',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'language': {
            'field': 'languages',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'language_id': {
            'field': 'language_id',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'language_tag': {
            'field': 'language_tag',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'glottolog_code': {
            'field': 'glottolog_code',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'linguality_type': {
            'field': 'linguality_type',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'lr_distinction': {
            'field': 'lr_distinction',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },

        'resource_type': {
            'field': 'resource_type',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },

        'subclass': {
            'field': 'subclass',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },

        'language_independent': {
            'field': 'language_independent',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'function': {
            'field': 'functions',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'publication_date': {
            'field': 'publication_date',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

    }

    # Define post-filter filtering fields
    post_filter_fields = {
        'languages': {
            'field': 'languages',
            'lookups': [
                LOOKUP_FILTER_TERMS,
                LOOKUP_FILTER_PREFIX,
                LOOKUP_FILTER_WILDCARD,
                LOOKUP_QUERY_IN,
                LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'licences': {
            'field': 'licences',
            'lookups': [
                LOOKUP_FILTER_TERMS,
                LOOKUP_FILTER_PREFIX,
                LOOKUP_FILTER_WILDCARD,
                LOOKUP_QUERY_IN,
                LOOKUP_QUERY_EXCLUDE,
            ],
        },
    }
