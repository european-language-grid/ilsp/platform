import logging

from django.db.models import QuerySet, Q
from django_elasticsearch_dsl import Index, fields
from django_elasticsearch_dsl.documents import DocType
from language_tags import tags

from dle.models import DLEData
from management.models import PUBLISHED
from registry.choices import (
    LINGUALITY_TYPE_CHOICES, MEDIA_TYPE_CHOICES, MODEL_TYPE_RECOMMENDED_CHOICES,
    LD_SUBCLASS_CHOICES, CORPUS_SUBCLASS_CHOICES, LCR_SUBCLASS_CHOICES, ANNOTATION_TYPE_RECOMMENDED_CHOICES
)
from registry.models import MetadataRecord
from registry.utils.checks import is_tool_service
from registry.utils.retrieval_utils import retrieve_default_lang
from utils.index_utils import (
    lowercase_normalizer, map_resource_type, map_ld_subclass,
    map_condition_of_use, map_access_rights_statement
)
from utils.ms_ontology_service.requests import get_function_categories

LOGGER = logging.getLogger(__name__)

dle_report = Index('dle_report')
# See Elasticsearch Indices API reference for available settings
dle_report.settings(
    number_of_shards=4,
    number_of_replicas=2
)


# metadata_record.analyzer(synonym_analyzer)


@dle_report.doc_type
class DLEReportDocument(DocType):
    class Django:
        model = DLEData  # The model associated with this Document

    id = fields.IntegerField(attr='id')

    resource_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    lr_distinction = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    resource_type = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    subclass = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    type_of_access = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    media_type = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    input_media_type = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    output_media_type = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    linguality_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    annotation_type = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    languages = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    language_id = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    language_tag = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    glottolog_code = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    language_independent = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    functions = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    domain = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    dle_score = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    publication_date = fields.DateField()

    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        """
        Return the queryset that should be indexed by this doc type.
        """
        return MetadataRecord.objects.filter(
            Q(described_entity__entity_type='LanguageResource') &
            Q(management_object__status=PUBLISHED) &
            Q(management_object__deleted=False) &
            Q(management_object__is_latest_version=True) &
            Q(management_object__hide_record_as_duplicate=False)
        )

    def dle_report_update(self, thing, refresh=None, action="index", raise_on_error=False, **kwargs):
        """
        Update each document in ES for a model, iterable of models or queryset
        """
        if action == "index":
            if isinstance(thing, MetadataRecord):
                thing = self.get_queryset().filter(pk=thing.pk)
            elif isinstance(thing, QuerySet):
                thing = self.get_queryset().intersection(thing)
            elif isinstance(thing, list):
                thing = self.get_queryset().filter(pk__in=[instance.pk for instance in thing])
        try:
            if raise_on_error:
                result = super().update(thing, refresh, action=action, raise_on_error=raise_on_error, **kwargs)
            else:
                result = super().update(thing, refresh, action, **kwargs)
        except Exception:
            LOGGER.exception('Error in elastic indexer update')
        else:
            return result

    # PREPARE FIELDS
    def prepare_resource_name(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return retrieve_default_lang(instance.described_entity.resource_name)
        elif instance.described_entity.entity_type == 'Project':
            return retrieve_default_lang(instance.described_entity.project_name)
        elif instance.described_entity.entity_type == 'Organization':
            return retrieve_default_lang(instance.described_entity.organization_name)

    def prepare_resource_type(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(instance.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(
                    LD_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type

    def prepare_lr_distinction(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            lr_type = instance.described_entity.lr_subclass.lr_type
            if lr_type in ['LanguageDescription', 'Corpus', 'LexicalConceptualResource']:
                return 'Dataset'
            elif lr_type == 'ToolService':
                return 'Software'

    def prepare_subclass(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            lr_type = instance.described_entity.lr_subclass.lr_type
            if lr_type == 'LanguageDescription':
                if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/grammar':
                    return ['Grammar']
                elif instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                    model_type = []
                    if instance.described_entity.lr_subclass.language_description_subclass.model_type:
                        for _type in instance.described_entity.lr_subclass.language_description_subclass.model_type:
                            try:
                                model_type.append(MODEL_TYPE_RECOMMENDED_CHOICES.__getitem__(_type))
                            except KeyError:
                                model_type.append(_type)
                        return model_type
                else:
                    return ['Other']
            if lr_type == 'Corpus':
                try:
                    return CORPUS_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.corpus_subclass)
                except KeyError:
                    return None
            if lr_type == 'LexicalConceptualResource':
                try:
                    return LCR_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.lcr_subclass)
                except KeyError:
                    return None

    def prepare_type_of_access(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                # Get distributions
                distributions = None
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    distributions = instance.described_entity.lr_subclass.dataset_distribution.all()
                elif resource_type == 'ToolService':
                    distributions = instance.described_entity.lr_subclass.software_distribution.all()
                for distribution in distributions:
                    # Map condition_of_use
                    for licence in distribution.licence_terms.all():
                        for c_o_u in licence.condition_of_use:
                            condition = map_condition_of_use(c_o_u)
                            if condition:
                                result.append(condition)
                    # Map access_rights_statements
                    for access_right in distribution.access_rights.all():
                        conditions = map_access_rights_statement(retrieve_default_lang(access_right.category_label))
                        if conditions:
                            for condition in conditions:
                                result.append(condition)

            except IndexError:
                return result
        return list(set(result))

    def prepare_media_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result.append('unspecified')
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'media_type', None):
                            continue
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type))
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result.append('unspecified')
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if not getattr(media_part, 'media_type', None):
                            continue
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type))
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result.append('unspecified')
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                result.append('unspecified')
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            if not getattr(media_part, 'media_type', None):
                                continue
                            result.append(MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type))
            except IndexError:
                return result
        return list(set(result))

    def prepare_input_media_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'ToolService':
                    for _input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        if not getattr(_input_resource, 'media_type', None):
                            continue
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(_input_resource.media_type))
            except IndexError:
                return result
        return list(set(result))

    def prepare_output_media_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'ToolService':
                    for _output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        if not getattr(_output_resource, 'media_type', None):
                            continue
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(_output_resource.media_type))
            except IndexError:
                return result
        return list(set(result))

    def prepare_annotation_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'annotation', None):
                            continue
                        else:
                            for _annotation in media_part.annotation.all():
                                for _type in _annotation.annotation_type:
                                    try:
                                        result.append(ANNOTATION_TYPE_RECOMMENDED_CHOICES.__getitem__(_type))
                                    except KeyError:
                                        result.append(_type)
                # elif resource_type == 'ToolService':
                #     for _output_resource in instance.described_entity.lr_subclass.output_resource.all():
                #         if not getattr(_output_resource, 'annotation_type', None):
                #             continue
                #         else:
                #             try:
                #                 result.append(
                #                     ANNOTATION_TYPE_RECOMMENDED_CHOICES.__getitem__(_output_resource.annotation_type))
                #             except KeyError:
                #                 result.append(_output_resource.annotation_type)
            except IndexError:
                return result
        return list(set(result))

    def prepare_language_id(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_id)
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_id)
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_id)
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                for language in unspecified_part.language.all():
                                    result.append(language.language_id)
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            for language in media_part.language.all():
                                result.append(language.language_id)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        for language in input_resource.language.all():
                            result.append(language.language_id)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        for language in output_resource.language.all():
                            result.append(language.language_id)
            except IndexError:
                return result
        return list(set(result))

    def prepare_language_tag(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_tag)
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        for language in media_part.language.all():
                            result.append(language.language_tag)
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_tag)
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        for language in media_part.language.all():
                            result.append(language.language_tag)
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                result.append(language.language_tag)
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                for language in unspecified_part.language.all():
                                    result.append(language.language_tag)
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            for language in media_part.language.all():
                                result.append(language.language_tag)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        for language in input_resource.language.all():
                            result.append(language.language_tag)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        for language in output_resource.language.all():
                            result.append(language.language_tag)
            except IndexError:
                return result
        return list(set(result))

    def prepare_glottolog_code(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                if getattr(language, 'glottolog_code', None):
                                    result.append(language.glottolog_code)
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        for language in media_part.language.all():
                            if getattr(language, 'glottolog_code', None):
                                result.append(language.glottolog_code)
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                if getattr(language, 'glottolog_code', None):
                                    result.append(language.glottolog_code)
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        for language in media_part.language.all():
                            if getattr(language, 'glottolog_code', None):
                                result.append(language.glottolog_code)
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            for language in unspecified_part.language.all():
                                if getattr(language, 'glottolog_code', None):
                                    result.append(language.glottolog_code)
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                for language in unspecified_part.language.all():
                                    if getattr(language, 'glottolog_code', None):
                                        result.append(language.glottolog_code)
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            for language in media_part.language.all():
                                if getattr(language, 'glottolog_code', None):
                                    result.append(language.glottolog_code)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        for language in input_resource.language.all():
                            if getattr(language, 'glottolog_code', None):
                                result.append(language.glottolog_code)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        for language in output_resource.language.all():
                            if getattr(language, 'glottolog_code', None):
                                result.append(language.glottolog_code)
            except IndexError:
                return result
        return list(set(result))

    def _check_language_list(self, part, language_list=None, not_in_language_list=False):
        result = list()
        for lang in part.language.all():
            if language_list is None:
                result.extend([tags.description(lang.language_id)[0]
                               for lang in part.language.all()])
            else:
                if not_in_language_list:
                    if tags.description(lang.language_id)[0] not in language_list:
                        result.extend([tags.description(lang.language_id)[0]])
                else:
                    if tags.description(lang.language_id)[0] in language_list:
                        result.extend([tags.description(lang.language_id)[0]])
        return result

    def _from_language_list(self, instance, language_list=None, not_in_language_list=False):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result_lang = self._check_language_list(
                                instance.described_entity.lr_subclass.unspecified_part, language_list,
                                not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        result_lang = self._check_language_list(media_part, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result_lang = self._check_language_list(
                                instance.described_entity.lr_subclass.unspecified_part, language_list,
                                not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        result_lang = self._check_language_list(media_part, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result_lang = self._check_language_list(
                                instance.described_entity.lr_subclass.unspecified_part, language_list,
                                not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                result_lang = self._check_language_list(
                                    instance.described_entity.lr_subclass.unspecified_part, language_list,
                                    not_in_language_list)
                                if result_lang:
                                    result.extend(result_lang)
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            result_lang = self._check_language_list(media_part, language_list, not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        result_lang = self._check_language_list(input_resource, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        result_lang = self._check_language_list(output_resource, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
            except IndexError:
                return result
        return list(set(result))

    def prepare_languages(self, instance):
        # handle different cases, based on resource_type
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance)
        else:
            return []

    def prepare_linguality_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part and getattr(unspecified_part, 'linguality_type', None):
                            result.append(LINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.linguality_type))
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part and getattr(unspecified_part, 'linguality_type', None):
                            result.append(LINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.linguality_type))
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part and getattr(unspecified_part, 'linguality_type', None):
                            result.append(LINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.linguality_type))
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part and getattr(unspecified_part, 'linguality_type', None):
                                result.append(LINGUALITY_TYPE_CHOICES.__getitem__(unspecified_part.linguality_type))
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            if not getattr(media_part, 'linguality_type', None):
                                continue
                            result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
            except IndexError:
                return result
        return list(set(result))

    def prepare_functions(self, instance):
        categories = list()
        if is_tool_service(instance):
            for f in instance.described_entity.lr_subclass.function:
                try:
                    categories.extend(get_function_categories(f.split('/')[-1]))
                except KeyError:
                    categories.extend(get_function_categories(f))
        return list(categories)

    def prepare_domain(self, instance):
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            for _domain in instance.described_entity.domain.all():
                result.append(_domain.category_label['en'])
            return result

    def prepare_language_independent(self, instance):
        if is_tool_service(instance):
            return not instance.described_entity.lr_subclass.language_dependent
        else:
            return None

    def prepare_dle_score(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            if instance.management_object.dle_score:
                return instance.management_object.dle_score
            else:
                return 0.0

    def prepare_publication_date(self, instance):
        return instance.management_object.publication_date
