import requests

from catalogue_backend.settings.private_settings import DLE_API_KEY, DLE_BACKEND_URL


class DLEService:
    service_url = f'{DLE_BACKEND_URL}/api/dle'
    auth = {'Authorization': f'api-key {DLE_API_KEY}'}

    @classmethod
    def notify_create(cls, elg_id_list):
        data = {'elg-id': elg_id_list}
        response = requests.post(f"{cls.service_url}/notify", json=data, headers=cls.auth)
        return response.json(), response.status_code

    @classmethod
    def notify_delete(cls, elg_id_list):
        data = {'elg-id': elg_id_list}
        response = requests.delete(f"{cls.service_url}/notify", json=data, headers=cls.auth)
        return response.json(), response.status_code
