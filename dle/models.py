from registry.models import MetadataRecord


class DLEData(MetadataRecord):
    class Meta:
        proxy = True
