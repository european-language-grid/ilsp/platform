from elasticsearch import Elasticsearch
from catalogue_backend.settings.settings import ES_URL

es = Elasticsearch(ES_URL)