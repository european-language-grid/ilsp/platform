# Create your views here.
import math
from collections import Counter
from datetime import datetime

from elasticsearch.helpers import scan
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from dle import es


QUERY_MAP = {
    "arag1245": {"language_id": ["an"]},
    "basq1248": {"language_id": ["eu"]},
    "bosn1245": {"language_id": ["bs"]},
    "bret1244": {"language_id": ["br"]},
    "bulg1262": {"language_id": ["bg"]},
    "stan1289": {"language_id": ["ca"]},
    "corn1251": {"language_id": ["kw"]},
    "croa1245": {"language_id": ["hr"]},
    "czec1258": {"language_id": ["cs"]},
    "dani1285": {"language_id": ["da"]},
    "stan1293": {"language_id": ["en"]},
    "faro1244": {"language_id": ["fo"]},
    "finn1318": {"language_id": ["fi"]},
    "stan1290": {"language_id": ["fr"]},
    "west2354": {"language_id": ["fy"]},
    "gali1258": {"language_id": ["gl"]},
    "wels1247": {"language_id": ["cy"]},
    "stan1295": {"language_id": ["de"]},
    "hung1274": {"language_id": ["hu"]},
    "icel1247": {"language_id": ["is"]},
    "iris1253": {"language_id": ["ga"]},
    "mode1248": {"language_id": ["el"]},
    "ital1282": {"language_id": ["it"]},
    "stan1288": {"language_id": ["es"]},
    "luxe1241": {"language_id": ["lb"]},
    "scot1245": {"language_id": ["gd"]},
    "manx1243": {"language_id": ["gv"]},
    "lith1251": {"language_id": ["lt"]},
    "latv1249": {"language_id": ["lv"]},
    "mace1250": {"language_id": ["mk"]},
    "malt1254": {"language_id": ["mt"]},
    "norw1259": {"language_id": ["nb"]},
    "dutc1256": {"language_id": ["nl"]},
    "norw1262": {"language_id": ["nn"]},
    "norw1258": {"language_id": ["no"]},
    "occi1239": {"language_id": ["oc"]},
    "poli1260": {"language_id": ["pl"]},
    "port1283": {"language_id": ["pt"]},
    "roma1327": {"language_id": ["ro"]},
    "nort2671": {"language_id": ["se"]},
    "slov1269": {"language_id": ["sk"]},
    "slov1268": {"language_id": ["sl"]},
    "alba1267": {"language_id": ["sq"]},
    "serb1264": {"language_id": ["sr"]},
    "swed1254": {"language_id": ["sv"]},
    "nucl1301": {"language_id": ["tr"]},
    "tata1255": {"language_id": ["tt"]},
    "yidd1255": {"language_id": ["yi"]},
    "arbe1236": {"language_id": ["aae"]},
    "astu1245": {"language_id": ["ast"]},
    "cimb1238": {"language_id": ["cim"]},
    "kash1274": {"language_id": ["csb"]},
    "lowe1385": {"language_id": ["dsb"]},
    "emil1241": {"language_id": ["egl"]},
    "esto1258": {"language_id": ["ekk", "et"]},
    "torn1244": {"language_id": ["fit"]},
    "nort2626": {"language_id": ["frr"]},
    "friu1240": {"language_id": ["fur"]},
    "swis1247": {"language_id": ["gsw"]},
    "uppe1395": {"language_id": ["hsb"]},
    "kare1335": {"language_id": ["krl"]},
    "ligu1248": {"language_id": ["lij"]},
    "ladi1250": {"language_id": ["lld"]},
    "lomb1257": {"language_id": ["lmo"]},
    "east2282": {"language_id": ["ltg"]},
    "moch1255": {"language_id": ["mhn"]},
    "mira1251": {"language_id": ["mwl"]},
    "neap1235": {"language_id": ["nap"]},
    "jerr1238": {"language_id": ["nrf"]},
    "pica1241": {"language_id": ["pcd"]},
    "piem1238": {"language_id": ["pms"]},
    "reun1238": {"language_id": ["rcf"]},
    "roma1328": {"language_id": ["rgn"]},
    "roma1329": {"language_id": ["rom"]},
    "rusy1239": {"language_id": ["rue"]},
    "arom1237": {"language_id": ["rup"]},
    "sici1248": {"language_id": ["scn"]},
    "kild1236": {"language_id": ["sjd"]},
    "pite1240": {"language_id": ["sje"]},
    "sout2674": {"language_id": ["sma"]},
    "lule1254": {"language_id": ["smj"]},
    "inar1241": {"language_id": ["smn"]},
    "skol1241": {"language_id": ["sms"]},
    "vene1258": {"language_id": ["vec"]},
    "sout2679": {"language_id": ["vro"]},
    "wals1238": {"language_id": ["wae"]},
    "fran1269": {"language_id": ["frp"]},
    "nort2628": {"language_id": ["frs"]},
    "gall1275": {"language_tag": ["fr-gallo"], "glottolog_code": ["gall1275"]},
    "apul1236": {"glottolog_code": ["apul1236"]},
    "plau1238": {"language_id": ["pdt"]},
    # "saam1281": {
    #     "language_id": ["smi", "sma", "sju", "sje", "smj", "sme", "sjk", "smn", "sms", "sia", "sjd", "sjt"]},
    "sard1257": {"language_id": ["sc", "sro", "src", "sdc", "ssn"]},
    #"sorb1249": {"language_id": ["wen"]},
    "ukra1253": {"language_id": ["uk", "ukr"]},
    "nucl1302": {"language_id": ["ka", "geo", "kat"]},
}


def _get_search_results(params):
    match = [{'terms': {k: v}} for k, v in params.items()]
    resp = scan(es, index='dle_report', scroll='10s', size=10000,
                query={"query": {"bool": {"should": match}}},
                clear_scroll=True
                )
    results = list(resp)
    return results


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def language_dle_info(request):
    q_params = dict()
    for k, v in request.data.get('params').items():
        if v:
            q_params.update({k: v})
    results = _get_search_results(q_params)
    return Response(results)


@api_view(['GET'])
@permission_classes([permissions.AllowAny])
def all_lang_info(request):
    result_dict = dict()
    for k, v in QUERY_MAP.items():
        results = _get_search_results(v)
        result_dict.update({k: get_resource_counts(results)})
    total_counts = {}
    for lang_counts in result_dict.values():
        for k, v in lang_counts.items():
            try:
                total_counts[k] += v
            except KeyError:
                total_counts[k] = v
    result_dict['total'] = dict(total_counts)
    return Response(result_dict)


def get_resource_counts(info):
    # default dicts to be used in function aggregations
    return_dict = {
        'Human Computer Interaction Collection': 0,
        'Information Extraction and Information Retrieval Collection': 0,
        'Natural Language Generation Collection': 0,
        'Speech Processing Collection': 0,
        'Support operation Collection': 0,
        'Text Processing Collection': 0,
        'Translation Technologies Collection': 0,
        'Image / Video Processing Collection': 0,
        'Other': 0,
        'Corpus': 0,
        'Model': 0,
        'Grammar': 0,
        'Lexical/Conceptual resource': 0,
        'Uncategorized Language Description': 0
    }

    for _doc in info:
        resource_type = _doc['_source'].get('resource_type')
        if resource_type == 'Tool/Service':
            categories = _doc['_source'].get('functions', [])
            for category in set(categories):
                if category != 'undefined':
                    return_dict[category] += 1
        else:
            return_dict[resource_type] += 1
    return return_dict


def get_software_count(info, response):
    # default dicts to be used in function aggregations
    access_types = {
        'other specific restrictions': 0,
        'commercial uses not allowed': 0,
        'unspecified': 0,
        'derivatives not allowed': 0,
        'no conditions': 0,
        'research use allowed': 0,
        'redistribution not allowed': 0,
        'attribution required': 0
    }

    media_types = {
        'audio': 0,
        'text': 0,
        'numerical text': 0,
        'image': 0,
        'video': 0
    }

    _dle_score = info.get('dle_score') or 0.0
    response['software']['count'] += 1
    response['software']['dle_score'] += _dle_score or 0.0
    for type_of_access in info.get('type_of_access'):
        _get_or_set_key(response['software'], 'type_of_access', type_of_access)
        _get_or_set_key(response, 'type_of_access', type_of_access)
    for input_media_type in info.get('input_media_type'):
        _get_or_set_key(response['software'], 'input_media_type', input_media_type)
    for output_media_type in info.get('output_media_type'):
        _get_or_set_key(response['software'], 'output_media_type', output_media_type)
    if info.get('language_independent'):
        response['software']['language_independent']['true'] += 1
    else:
        response['software']['language_independent']['false'] += 1
    for function in info.get('functions'):
        _get_or_set_key(response['software'], 'functions', function, count=True)
        if 'type_of_access' not in response['software']['functions'][function].keys():
            response['software']['functions'][function]['type_of_access'] = access_types
        if 'input_media_type' not in response['software']['functions'][function].keys():
            response['software']['functions'][function]['input_media_type'] = media_types.copy()
        if 'output_media_type' not in response['software']['functions'][function].keys():
            response['software']['functions'][function]['output_media_type'] = media_types.copy()
        for type_of_access in info.get('type_of_access'):
            _get_or_set_key(response['software']['functions'][function], 'type_of_access', type_of_access)
        for input_media_type in info.get('input_media_type'):
            _get_or_set_key(response['software']['functions'][function], 'input_media_type', input_media_type)
        for output_media_type in info.get('output_media_type'):
            _get_or_set_key(response['software']['functions'][function], 'output_media_type', output_media_type)
    for domain in info.get('domain'):
        _get_or_set_key(response['software'], 'domain', domain)
        _get_or_set_key(response, 'domain', domain)


def get_dataset_count(info, response):
    _resource_type = info.get('resource_type')
    _dle_score = info.get('dle_score') or 0.0
    resource_type = response['datasets']['resource_type'][_resource_type]
    response['datasets']['count'] += 1
    resource_type['count'] += 1
    response['datasets']['dle_score'] += _dle_score or 0.0
    subclass = info.get('subclass')
    if isinstance(subclass, list):
        for subcl in subclass:
            _get_or_set_key(resource_type, 'subclass', subcl)
    else:
        _get_or_set_key(resource_type, 'subclass', subclass)
    for type_of_access in info.get('type_of_access'):
        _get_or_set_key(resource_type, 'type_of_access', type_of_access)
        _get_or_set_key(response, 'type_of_access', type_of_access)
        _get_or_set_key(response['datasets'], 'type_of_access', type_of_access)

    for linguality_type in info.get('linguality_type'):
        resource_type['linguality'][linguality_type] += 1
        response['datasets']['linguality'][linguality_type] += 1

    for media_type in info.get('media_type'):
        _get_or_set_key(resource_type, 'media_type', media_type)
        _get_or_set_key(response['datasets'], 'media_type', media_type)
    if _resource_type == 'Corpus':
        if info.get('subclass') in ['annotated corpus', 'annotations corpus']:
            resource_type['annotated'] = True
        for _ in info.get('annotation_type'):
            resource_type['annotation_types'] += 1
    for domain in info.get('domain'):
        _get_or_set_key(resource_type, 'domain', domain)
        _get_or_set_key(response['datasets'], 'domain', domain)
        _get_or_set_key(response, 'domain', domain)


def _get_or_set_key(d, key_p, key_c, count=None):
    if count:
        try:
            d[key_p][key_c]['count'] += 1
        except TypeError:
            d[key_p] = {key_c: {'count': 1}}
        except KeyError:
            d[key_p][key_c] = dict(count=1)
    else:
        try:
            d[key_p][key_c] += 1
        except TypeError:
            d[key_p] = {key_c: 1}
        except KeyError:
            try:
                d[key_p][key_c] = 1
            except KeyError:
                d[key_p] = {key_c: 1}
