import datetime
import logging

from django.conf import settings
from django.contrib.auth import get_user_model
from catalogue_backend.celery import app as celery_app

from email_notifications.email_notification_utils import (
    legal_validation_assignment_mail, metadata_validation_assignment_mail, technical_validation_assignment_mail,
    unassign_validator_notification_mail, content_manager_action_notification_mail, curator_action_notification_mail,
    automatic_validation_assignment_notifications, email_consent_notification_mail, admin_system_issue_notification,
    version_inactive_user_notification, curator_ingestion_notification_mail
)

LOGGER = logging.getLogger(__name__)


@celery_app.task(name='validation_assignment_emailing')
def manual_validation_assignment_notifications(updated_ids, vld_type, request_user, previous_validator_list=None):
    if not settings.SEND_EMAIL:
        return
    # avoid cyclic import error
    from management.models import Manager
    vld_mapping = {
        'legal_validator': {
            'notification_type': legal_validation_assignment_mail
        },
        'metadata_validator': {
            'notification_type': metadata_validation_assignment_mail
        },
        'technical_validator': {
            'notification_type': technical_validation_assignment_mail
        }
    }
    records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
    record_validator_pairing = []
    validators = []
    validator_dict = {}
    for record in records:
        record_validator_pairing.append((record.metadata_record_of_manager,
                                         getattr(record, vld_type),
                                         f'{vld_type if getattr(record, vld_type) else ""}'))
    for i in record_validator_pairing:
        validators.append(i[1])
    validator_set = set(validators)
    for validator in validator_set:
        validator_dict[validator] = [(record, vld) for record, record_validator, vld
                                     in record_validator_pairing if validator == record_validator]
        records_to_be_validated = [record for record, vld in validator_dict[validator] if vld == vld_type]
        vld_mapping[vld_type]['notification_type'](to_emails=[validator.email],
                                                   name=' '.join([validator.first_name,
                                                                  validator.last_name]),
                                                   records=records_to_be_validated,
                                                   request_user=request_user
                                                   )
    if previous_validator_list:
        previously_assigned_validators_set = set()
        for i in previous_validator_list:
            previously_assigned_validators_set.add(i[0])
        # previously_assigned_validators_set = set(previous_validator_list)
        for previous_validator_id in previously_assigned_validators_set:
            previous_validator_dict = {}
            previous_validator = get_user_model().objects.get(id=previous_validator_id)
            previous_validator_dict[previous_validator.username] = [mdr_id for p_vpd, mdr_id in previous_validator_list
                                                                    if p_vpd == previous_validator_id]
            reassigned_records = records.filter(metadata_record_of_manager__pk__in
                                                =previous_validator_dict[previous_validator.username])
            unassign_validator_notification_mail(vld_type=vld_type,
                                                 to_emails=[previous_validator.email],
                                                 name=' '.join([previous_validator.first_name,
                                                                previous_validator.last_name]),
                                                 records=reassigned_records,
                                                 request_user=request_user)


@celery_app.task(name='ingestion_emailing')
def ingestion_notifications(updated_ids, not_valid_ids):
    if not settings.SEND_EMAIL:
        return
    # avoid cyclic import error
    from management.models import Manager
    content_managers = get_user_model().objects.filter(
        groups__name='content_manager'
    )
    records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
    invalid_records = Manager.objects.filter(metadata_record_of_manager__pk__in=list(not_valid_ids.keys()))
    record_curator_pairing = []
    curators = []
    curator_dict = {'curator_notification': dict(),
                    'admin_notification': dict()}
    for record in records:
        record_curator_pairing.append((record.metadata_record_of_manager, record.curator))
    for record in invalid_records:
        record_curator_pairing.append((record.metadata_record_of_manager, record.curator))
    for i in record_curator_pairing:
        curators.append(i[1])
    curator_set = set(curators)
    for curator in curator_set:
        curator_dict['curator_notification'][curator.username] = [(record, not_valid_ids.get(record.id)) for
                                                                  record, record_curator
                                                                  in record_curator_pairing if
                                                                  curator == record_curator]
        curator_dict['admin_notification'][curator.username] = [record for record, record_curator
                                                                in record_curator_pairing if (
                                                                            curator == record_curator
                                                                            and not not_valid_ids.get(record.id))]
        content_manager_action_notification_mail(
            action='ingestion',
            content_managers=content_managers,
            records=curator_dict['admin_notification'][curator.username],
            curator=curator
        )
        curator_ingestion_notification_mail(
            to_emails=[curator.email],
            name=' '.join([curator.first_name,
                           curator.last_name]),
            records=curator_dict['curator_notification'][curator.username]
        )
    automatic_validation_assignment_notifications(records)


@celery_app.task(name='general_action_emailing')
def curator_content_manager_notifications(updated_ids, action, review_comments=None, claimed_dict=None):
    if not settings.SEND_EMAIL:
        return
    # avoid cyclic import error
    from management.models import Manager
    from registry.models import MetadataRecord
    content_managers = get_user_model().objects.filter(
        groups__name='content_manager'
    )
    record_curator_pairing = []
    curators = []
    curator_dict = {}
    if not claimed_dict:
        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        for record in records:
            record_curator_pairing.append((record.metadata_record_of_manager, record.curator))
    else:
        for claimee, record_id in claimed_dict.items():
            record_curator_pairing.append((MetadataRecord.objects.get(pk__in=record_id),
                                           get_user_model().objects.get(username=claimee)))
    for i in record_curator_pairing:
        curators.append(i[1])
    curator_set = set(curators)
    for curator in curator_set:
        curator_dict[curator.username] = [record for record, record_curator
                                          in record_curator_pairing if curator == record_curator]
        if action not in ['unpublication', 'restore', 'create_version', 'copy_resource']:
            content_manager_action_notification_mail(
                action=action,
                content_managers=content_managers,
                records=curator_dict[curator.username],
                curator=curator,
                review_comments=review_comments
            )
        if action not in ['unpublication_request', 'report', 'claim']:
            curator_action_notification_mail(
                action=action,
                to_emails=[curator.email],
                name=' '.join([curator.first_name,
                               curator.last_name]),
                records=curator_dict[curator.username],
                review_comments=review_comments
            )


@celery_app.task(name='personal_info_consent_email')
def personal_info_consent_email(first_name, last_name, email):
    if first_name == 'To Whom It May' and last_name == 'Concern':
        name_list = [first_name, last_name]
    else:
        name_list = ['Dear', first_name, last_name]

    email_consent_notification_mail(
        to_emails=email,
        name=' '.join(name_list)
    )


@celery_app.task(name='system_issue_notification_email')
def system_issue_notification(email, pk, resource_type, choice, field_name):
    admin_system_issue_notification(
        to_emails=email,
        pk=pk,
        resource_type=resource_type,
        choice=choice,
        field_name=field_name
    )


@celery_app.task(name='version_inactive_notification')
def version_inactive_notification(new_version_pk, old_version_pk, auto=True):
    from registry.models import MetadataRecord
    two_months_ago = datetime.datetime.now() - datetime.timedelta(days=60)
    new_version = MetadataRecord.objects.get(id=new_version_pk)
    old_version = MetadataRecord.objects.get(id=old_version_pk)
    target_user_list_pks = []
    if getattr(old_version, 'lt_service', None):
        target_user_list_pks = old_version.lt_service.lt_processing_jobs.filter(
            start_at__gte=two_months_ago).values_list(
            'user', flat=True
        )
    if not auto:
        content_managers = get_user_model().objects.filter(
            groups__name='content_manager'
        )
        for c_m in content_managers:
            target_user_list_pks.append(c_m.pk)
    for user_id in target_user_list_pks:
        user = get_user_model().objects.get(id=user_id)
        version_inactive_user_notification(user=user, new_version=new_version, old_version=old_version)
