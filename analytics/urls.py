from rest_framework import routers
from .consumer_index import urls as dashboard_urls
from django.urls import include, path
import analytics.views as views

router = routers.DefaultRouter()

app_name = 'analytics'

urlpatterns = (
 path('', include(router.urls)),
 path('consumer_grid/', include(dashboard_urls)),
 path('get_record_stats/<int:pk>/', views.landing_page_stats, name='landing_page_stats'),
 path('user_download_stats/<int:pk>/', views.UserDownloadStatsView.as_view(), name='user_download_stats'),
 path('service_stats/<int:pk>/', views.ServiceStatsView.as_view(), name='service_stats'),
)
