from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import MyDownloadsDocumentView, MyUsageDocumentView

router = DefaultRouter()
router.register('my_downloads', MyDownloadsDocumentView, basename='consumer_dashboard_downloads')
router.register('my_usage', MyUsageDocumentView, basename='consumer_dashboard_usage')


urlpatterns = [
    path('', include(router.urls)),
]
