from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

from .documents import MyDownloadsDocument, MyUsageDocument


class MyDownloadsDocumentSerializer(DocumentSerializer):
    """Serializer for the ManagementDashboardRecords document."""

    class Meta(object):
        """Meta options."""

        # Specify the correspondent document class
        document = MyDownloadsDocument

        # List the serializer fields. Note, that the order of the fields
        # is preserved in the ViewSet.
        fields = (
            'id',
            'record_id',
            'distribution_id',
            'user',
            'resource_name',
            'resource_type',
            'version',
            'download_date',
            'file_name',
            'is_active_version',
            'is_latest_version',
            'licences',
            'source_of_download',
            'mapped_source_of_download',
            'number_of_downloads',
            'other_downloads'
        )


class MyUsageDocumentSerializer(DocumentSerializer):
    """Serializer for the ManagementDashboardRecords document."""

    class Meta(object):
        """Meta options."""

        # Specify the correspondent document class
        document = MyUsageDocument

        # List the serializer fields. Note, that the order of the fields
        # is preserved in the ViewSet.
        fields = (
            'id',
            'record_id',
            'resource_type',
            'user',
            'resource_name',
            'version',
            'status',
            'execution_start',
            'duration',
            'call_type',
            'mapped_call_type',
            'bytes',
            'is_active_version',
            'is_latest_version',
            'number_of_usages',
            'other_usages'
        )
