from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django_elasticsearch_dsl import Index, fields
from django_elasticsearch_dsl.documents import DocType
from elasticsearch_dsl import analyzer, normalizer

from analytics.models import UserDownloadStats, ServiceStats
from management.models import Manager, INGESTED
# Name of the Elasticsearch index
from processing.models import RegisteredLTService
from registry.choices import LD_SUBCLASS_CHOICES
from registry.models import MetadataRecord
from registry.utils.retrieval_utils import retrieve_default_lang

my_usage_index = Index('my_usage_index')
# See Elasticsearch Indices API reference for available settings
my_usage_index.settings(
    number_of_shards=4,
    number_of_replicas=2
)

my_downloads_index = Index('my_downloads_index')
my_downloads_index.settings(
    number_of_shards=4,
    number_of_replicas=2
)

html_strip = analyzer(
    'html_strip',
    tokenizer='standard',
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)

lowercase_normalizer = normalizer(
    'lowercase_normalizer',
    filter=['lowercase']
)
lowercase_analyzer = analyzer(
    'lowercase_analyzer',
    tokenizer="keyword",
    filter=['lowercase'],
)


def get_name_from_instance(instance):
    if instance.name:
        return [instance.name[settings.REQUIRED_LANGUAGE_TAG]]
    elif instance.identifiers:
        return [identifier.value for identifier in instance.identifiers.all()]


def map_resource_type(resource_type):
    resource_mapping = {
        'LanguageDescription': 'Language description',
        'LexicalConceptualResource': 'Lexical/Conceptual resource',
        'ToolService': 'Tool/Service'
    }
    if resource_type in resource_mapping:
        return resource_mapping[resource_type]
    return resource_type


def map_ld_subclass(ld_subclass):
    ld_mapping = {
        'model': 'Model',
        'grammar': 'Grammar',
        'other': 'Uncategorized Language Description'
    }
    return ld_mapping.get(ld_subclass, ld_subclass)


@my_downloads_index.doc_type
class MyDownloadsDocument(DocType):
    class Django:
        model = UserDownloadStats  # The model associated with this DocType

    id = fields.IntegerField()
    record_id = fields.IntegerField()
    distribution_id = fields.IntegerField(multi=True)

    user = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    resource_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    resource_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    resource_type_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    keycloak_id = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    file_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    is_active_version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    is_latest_version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    download_date = fields.DateField()

    licences = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        multi=True
    )

    licences_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    source_of_download = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    mapped_source_of_download = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    number_of_downloads = fields.IntegerField()

    other_downloads = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        multi=True
    )

    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        return UserDownloadStats.objects.filter(is_latest_download=True)

    # PREPARE FIELDS

    def prepare_id(self, instance):
        try:
            return instance.pk
        except ObjectDoesNotExist:
            return None

    def prepare_record_id(self, instance):
        try:
            return instance.metadata_record.pk
        except MetadataRecord.DoesNotExist:
            return None

    def prepare_distribution_id(self, instance):
        if instance.distribution_of_downloaded_dataset:
            return instance.distribution_of_downloaded_dataset.id
        return None

    def prepare_user(self, instance):
        if instance.user:
            return instance.user.username
        return None

    def prepare_version(self, instance):
        record = instance.metadata_record
        if record.described_entity.entity_type == 'LanguageResource':
            return map_resource_type(record.described_entity.version)

    def prepare_keycloak_id(self, instance):
        if instance.user:
            return instance.user.keycloak_id
        return None

    def prepare_resource_name(self, instance):
        resource_name_mapping = {
            'LanguageResource': 'resource_name',
            'Project': 'project_name',
            'Organization': 'organization_name',
            'Group': 'organization_name',
            'Person': 'surname',
            'Document': 'title',
            'LicenceTerms': 'licence_terms_name',
            'Repository': 'repository_name'
        }
        record = instance.metadata_record
        entity_type = record.described_entity.entity_type
        name = getattr(record.described_entity, resource_name_mapping[entity_type])
        return_name = retrieve_default_lang(name)
        if entity_type == 'Person':
            given_name = getattr(record.described_entity, 'given_name')
            return_name = return_name + ' ' + retrieve_default_lang(given_name)
        return return_name

    def prepare_resource_type(self, instance):
        metadata_record = instance.metadata_record
        if metadata_record.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(metadata_record.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(LD_SUBCLASS_CHOICES.__getitem__(metadata_record.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type
        else:
            return metadata_record.described_entity.entity_type

    def prepare_resource_type_facet(self, instance):
        metadata_record = instance.metadata_record
        if metadata_record.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(metadata_record.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(LD_SUBCLASS_CHOICES.__getitem__(metadata_record.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type
        else:
            return metadata_record.described_entity.entity_type

    def prepare_file_name(self, instance):
        file_name = None
        record = instance.metadata_record
        if record.management_object.has_content_file:
            dist = instance.distribution_of_downloaded_dataset
            if record.described_entity.entity_type == 'LanguageResource' and dist:
                lr_type = record.described_entity.lr_subclass.lr_type
                if lr_type == 'ToolService':
                    file_name = dist.package.file.name
                else:
                    file_name = dist.dataset.file.name
        return file_name

    def prepare_is_active_version(self, instance):
        record = instance.metadata_record
        if record.described_entity.entity_type == 'LanguageResource':
            return record.management_object.is_active_version

    def prepare_is_latest_version(self, instance):
        record = instance.metadata_record
        if record.described_entity.entity_type == 'LanguageResource':
            return record.management_object.is_latest_version

    def prepare_download_date(self, instance):
        try:
            return instance.downloaded_at
        except Manager.DoesNotExist:
            return None

    def prepare_licences(self, instance):
        licences = instance.licenses
        if licences:
            return licences
        return []

    def prepare_licences_facet(self, instance):
        licences = instance.licenses
        if licences:
            return licences
        return []

    def prepare_source_of_download(self, instance):
        return instance.source_of_download

    def prepare_mapped_source_of_download(self, instance):
        return instance.mapped_source_of_download

    def prepare_number_of_downloads(self, instance):
        # add one to the count for the latest download
        number_of_downloads = instance.aggregated_downloads.all().count() + 1
        return number_of_downloads

    def prepare_other_downloads(self, instance):
        other_download_ids = list(set(instance.aggregated_downloads.all().values_list('pk', flat=True)))
        return other_download_ids


@my_usage_index.doc_type
class MyUsageDocument(DocType):
    class Django:
        model = ServiceStats  # The model associated with this DocType

    id = fields.IntegerField()
    record_id = fields.IntegerField()

    resource_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    resource_type_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    user = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    keycloak_id = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    resource_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    status = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    call_type = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    mapped_call_type = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    execution_start = fields.DateField()
    duration = fields.IntegerField()

    is_active_version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    is_latest_version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    bytes = fields.IntegerField()
    number_of_usages = fields.IntegerField()
    other_usages = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        multi=True
    )

    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        return ServiceStats.objects.filter(is_latest_usage=True)

    # PREPARE FIELDS

    def prepare_id(self, instance):
        try:
            return instance.pk
        except ObjectDoesNotExist:
            return None

    def prepare_record_id(self, instance):
        try:
            lt_service = getattr(instance, 'lt_service', None)
            if lt_service:
                return instance.lt_service.metadata_record.pk
        except MetadataRecord.DoesNotExist:
            return None

    def prepare_resource_type(self, instance):
        lt_service = getattr(instance, 'lt_service', None)
        if lt_service:
            metadata_record = lt_service.metadata_record
            if metadata_record.described_entity.entity_type == 'LanguageResource':
                mapped_resource_type = map_resource_type(metadata_record.described_entity.lr_subclass.lr_type)
                if mapped_resource_type == 'Language description':
                    return map_ld_subclass(LD_SUBCLASS_CHOICES.__getitem__(metadata_record.described_entity.lr_subclass.ld_subclass))
                return mapped_resource_type
            else:
                return metadata_record.described_entity.entity_type

    def prepare_resource_type_facet(self, instance):
        lt_service = getattr(instance, 'lt_service', None)
        if lt_service:
            metadata_record = lt_service.metadata_record
            if metadata_record.described_entity.entity_type == 'LanguageResource':
                mapped_resource_type = map_resource_type(metadata_record.described_entity.lr_subclass.lr_type)
                if mapped_resource_type == 'Language description':
                    return map_ld_subclass(LD_SUBCLASS_CHOICES.__getitem__(metadata_record.described_entity.lr_subclass.ld_subclass))
                return mapped_resource_type
            else:
                return metadata_record.described_entity.entity_type

    def prepare_user(self, instance):
        if instance.user:
            return instance.user.username
        return None

    def prepare_keycloak_id(self, instance):
        if instance.user:
            return instance.user.keycloak_id
        return None

    def prepare_version(self, instance):
        lt_service = getattr(instance, 'lt_service', None)
        if lt_service:
            record = lt_service.metadata_record
            if record.described_entity.entity_type == 'LanguageResource':
                return map_resource_type(record.described_entity.version)

    def prepare_resource_name(self, instance):
        resource_name_mapping = {
            'LanguageResource': 'resource_name',
            'Project': 'project_name',
            'Organization': 'organization_name',
            'Group': 'organization_name',
            'Person': 'surname',
            'Document': 'title',
            'LicenceTerms': 'licence_terms_name',
            'Repository': 'repository_name'
        }
        lt_service = getattr(instance, 'lt_service', None)
        if lt_service:
            record = lt_service.metadata_record
            entity_type = record.described_entity.entity_type
            name = getattr(record.described_entity, resource_name_mapping[entity_type])
            return_name = retrieve_default_lang(name)
            if entity_type == 'Person':
                given_name = getattr(record.described_entity, 'given_name')
                return_name = return_name + ' ' + retrieve_default_lang(given_name)
            return return_name


    def prepare_status(self, instance):
        return instance.status

    def prepare_is_active_version(self, instance):
        lt_service = getattr(instance, 'lt_service', None)
        if lt_service:
            record = lt_service.metadata_record
            if record.described_entity.entity_type == 'LanguageResource':
                return record.management_object.is_active_version

    def prepare_is_latest_version(self, instance):
        lt_service = getattr(instance, 'lt_service', None)
        if lt_service:
            record = lt_service.metadata_record
            if record.described_entity.entity_type == 'LanguageResource':
                return record.management_object.is_latest_version

    def prepare_call_type(self, instance):
        return instance.call_type

    def prepare_mapped_call_type(self, instance):
        return instance.mapped_call_type

    def prepare_bytes(self, instance):
        return instance.bytes

    def prepare_number_of_usages(self, instance):
        # add 1 to the count for latest usage
        number_of_usages = instance.aggregated_usages.all().count() + 1
        return number_of_usages

    def prepare_execution_start(self, instance):
        try:
            return instance.start_at
        except Manager.DoesNotExist:
            return None

    def prepare_duration(self, instance):
        try:
            return instance.duration
        except Manager.DoesNotExist:
            return None

    def prepare_other_usages(self, instance):
        other_usage_ids = list(set(instance.aggregated_usages.all().values_list('pk', flat=True)))
        return other_usage_ids


