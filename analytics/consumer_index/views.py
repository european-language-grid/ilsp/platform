from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
    CompoundSearchFilterBackend,
    MultiMatchSearchFilterBackend, FacetedSearchFilterBackend)
from django_elasticsearch_dsl_drf.pagination import PageNumberPagination
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet
from elasticsearch_dsl import TermsFacet, Q

from accounts import elg_permissions
from utils.index_backends.backends import LuceneStringSearchFilterBackend
from .documents import MyDownloadsDocument, MyUsageDocument
from .serializers import MyDownloadsDocumentSerializer, MyUsageDocumentSerializer


class MyDownloadsDocumentView(BaseDocumentViewSet):
    """The MetadataDocument view."""
    permission_classes = (elg_permissions.IsAuthenticated,)
    document = MyDownloadsDocument
    serializer_class = MyDownloadsDocumentSerializer
    pagination_class = PageNumberPagination
    lookup_field = 'id'
    filter_backends = [
        LuceneStringSearchFilterBackend,
        FilteringFilterBackend,
        FacetedSearchFilterBackend,
        CompoundSearchFilterBackend,
        MultiMatchSearchFilterBackend,
        DefaultOrderingFilterBackend,
        OrderingFilterBackend,
    ]

    simple_query_string_options = {
        "default_operator": "and",
    }

    # Define search fields
    search_fields = {
        'resource_name': {'boost': 4},
        'resource_type': {'boost': 5},
        'keycloak_id': None
    }
    # Define filter fields
    filter_fields = {
        'id': None,
        'resource_name': 'resource_name.raw',
        'resource_type': 'resource_type_facet.raw',
        'licence': 'licences_facet.raw'
    }
    # Define ordering fields
    ordering_fields = {
        'id': 'id',
        'resource_name': 'resource_name.raw',
        'download_date': 'download_date',
        'resource_type': 'resource_type.raw',
    }
    # Specify default ordering
    ordering = ('-download_date', '_score', 'id', 'resource_name.raw', 'resource_type.raw')

    faceted_search_fields = {
        'resource_type': {
            'field': 'resource_type_facet',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },
        'licence': {
            'field': 'licences_facet',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        }
    }

    def get_queryset(self):
        queryset = self.search.query()
        queryset.model = self.document.Django.model
        user = self.request.user
        queryset = self.search.query("term", keycloak_id=user.keycloak_id)
        return queryset


class MyUsageDocumentView(BaseDocumentViewSet):
    """The MetadataDocument view."""
    permission_classes = (elg_permissions.IsAuthenticated,)
    document = MyUsageDocument
    serializer_class = MyUsageDocumentSerializer
    pagination_class = PageNumberPagination
    lookup_field = 'id'
    filter_backends = [
        LuceneStringSearchFilterBackend,
        FilteringFilterBackend,
        FacetedSearchFilterBackend,
        CompoundSearchFilterBackend,
        MultiMatchSearchFilterBackend,
        DefaultOrderingFilterBackend,
        OrderingFilterBackend,
    ]

    simple_query_string_options = {
        "default_operator": "and",
    }

    # Define search fields
    search_fields = {
        'resource_name': {'boost': 4},
        'user': None,
        'status': None
    }
    # Define filter fields
    filter_fields = {
        'id': None,
        'resource_name': 'resource_name.raw',
        'status': 'status.raw',
        'resource_type': 'resource_type_facet.raw'
    }
    # Define ordering fields
    ordering_fields = {
        'execution_start': 'execution_start',
        'id': 'id',
        'resource_name': 'resource_name.raw',
        'resource_type': 'resource_type.raw'
    }
    # Specify default ordering
    ordering = ('-execution_start', '_score', 'id', 'resource_name.raw', 'resource_type.raw')

    faceted_search_fields = {
        'status': {
            'field': 'status',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },
        'resource_type': {
            'field': 'resource_type_facet',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        }
    }

    def get_queryset(self):
        queryset = self.search.query()
        queryset.model = self.document.Django.model
        user = self.request.user
        queryset = self.search.query("term", keycloak_id=user.keycloak_id)
        return queryset
