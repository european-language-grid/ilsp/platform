import abc
import httpagentparser


class CallTypeMapperInterface(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'get_mapping') and
                callable(subclass.get_mapping))

    @abc.abstractmethod
    def get_mapping(self, call_type: str):
        """Get mapping for a dataset"""
        raise NotImplementedError


class CallTypeMapperSimple(CallTypeMapperInterface):
    prefixes = {
        'Apache-HttpClient': 'Java',
        'curl': 'curl',
        'Go-http-client': 'Go',
        'Java-http-client': 'Java',
        'okhttp': 'Java',
        'PostmanRuntime': 'Postman',
        'PycURL': 'Python',
        'python-requests': 'Python',
        'RestSharp': '.NET',
        'unirest-java': 'Java'
    }

    def get_mapping(self, call_type: str) -> str:

        # browser
        if call_type.startswith('Mozilla'):
            parser = httpagentparser.detect(call_type)
            browser = parser.get('browser', {})
            return 'Browser/' + browser.get('name', '')
        else:
            for prefix in CallTypeMapperSimple.prefixes:
                if call_type.startswith(prefix):
                    return CallTypeMapperSimple.prefixes[prefix]

        return "Unknown"
