from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

# Create your models here.
from django.dispatch import Signal

from accounts.models import ELGUser
from analytics.utils.call_type_mapping_utils import CallTypeMapperSimple

from catalogue_backend.settings import settings
from processing.models import RegisteredLTService
from registry.models import MetadataRecord
from registry.search_indexes.documents import MetadataDocument


class ServiceStats(models.Model):
    """
    Stores information on processing requests by ELG users
    """

    class Meta:
        verbose_name = 'Service Execution Statistics'
        verbose_name_plural = 'Service Execution Statistics'

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        help_text='The ELG user that called this processing service',
        related_name='user_processing_jobs',
        on_delete=models.SET_NULL,
        null=True
    )

    lt_service = models.ForeignKey(
        RegisteredLTService,
        help_text='The ELG LT Service that this processing job has called',
        related_name='lt_processing_jobs',
        on_delete=models.CASCADE,
        null=True
    )

    start_at = models.DateTimeField(
        verbose_name='Execution Start Date/Time',
        help_text='Specifies the date and time this processing job was initiated',
        null=True
    )

    end_at = models.DateTimeField(
        verbose_name='Execution End Date/Time',
        help_text='Specifies the date and time this processing job was initiated',
        null=True
    )

    bytes = models.BigIntegerField()

    elg_resource = models.ForeignKey(
        MetadataRecord,
        help_text='The ELG resource that was used as input source for this '
                  'processing job (if input source is ELG resource)',
        null=True,
        related_name='resource',
        on_delete=models.CASCADE
    )

    status = models.CharField(
        choices=(('succeeded', 'Succeeded'), ('failed', 'Failed')),
        max_length=9,
        blank=True
    )

    user_ip = models.CharField(
        max_length=100,
        null=True
    )

    call_type = models.CharField(
        max_length=500,
        blank=True
    )

    is_latest_usage = models.BooleanField(null=True)

    aggregated_usages = models.ManyToManyField(
        'self',
        symmetrical=True,
        blank=True
    )

    @classmethod
    def aggregate(cls, lt_service):
        return cls.objects.filter(lt_service=lt_service).count()

    @property
    def duration(self):
        duration = None
        if self.end_at and self.start_at:
            duration = (self.end_at - self.start_at).total_seconds()
        return duration

    @property
    def mapped_call_type(self):
        mapper = CallTypeMapperSimple()
        if self.call_type:
            return mapper.get_mapping(self.call_type)
        else:
            return ''

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super().save()
        if self.user and self.lt_service and self.is_latest_usage is None:
            self.aggregate_usages()

    def aggregate_usages(self):
        from analytics.consumer_index.documents import MyUsageDocument
        other_usages = ServiceStats.objects.filter(
            user=self.user, lt_service=self.lt_service).exclude(id=self.id)
        self.aggregated_usages.add(
            *other_usages
        )
        if self.start_at and not self.aggregated_usages.filter(start_at__gte=self.start_at):
            self.is_latest_usage = True
            self.save()
            MyUsageDocument().update(self)
            previous_latest = self.aggregated_usages.filter(start_at__lte=self.start_at, is_latest_usage=True)
            for _u in previous_latest:
                _u.is_latest_usage = False
                _u.save()
            if previous_latest:
                MyUsageDocument().update(previous_latest, action='delete', raise_on_error=False)
        else:
            self.is_latest_usage = False
            self.save()

    def delete(self, using=None, keep_parents=False):
        super().delete()
        MetadataDocument().catalogue_update(self.lt_service.metadata_record)

    def __str__(self):
        return f'{self.lt_service} ({self.user})'


class MetadataRecordStats(models.Model):
    class Meta:
        verbose_name_plural = 'Metadata Record Stats'

    metadata_record = models.OneToOneField(
        MetadataRecord,
        on_delete=models.CASCADE,  # keep the stats
        related_name='stats',
        null=True
    )

    views = models.IntegerField(default=0)
    downloads = models.IntegerField(default=0)

    @property
    def all_versions_views(self):
        if self.metadata_record:
            other_versions_pks = self.metadata_record.management_object.versioned_record_managers.values_list('metadata_record_of_manager__pk', flat=True)
            other_versions = MetadataRecord.objects.filter(pk__in=other_versions_pks)
            all_version_views = self.views
            for ver in other_versions:
                try:
                    all_version_views += ver.stats.views
                except ObjectDoesNotExist:
                    continue
            return all_version_views
        return 0

    @property
    def all_versions_downloads(self):
        if self.metadata_record:
            other_versions_pks = self.metadata_record.management_object.versioned_record_managers.values_list(
                'metadata_record_of_manager__pk', flat=True)
            other_versions = MetadataRecord.objects.filter(pk__in=other_versions_pks)
            all_version_downloads = self.downloads
            for ver in other_versions:
                try:
                    all_version_downloads += ver.stats.downloads
                except ObjectDoesNotExist:
                    continue
            return all_version_downloads
        return 0

    def increment_views(self):
        if self.metadata_record.management_object.status == 'p':
            self.views = self.views + 1
            self.save()
            MetadataDocument().catalogue_update(self.metadata_record)

    def increment_downloads(self):
        if self.metadata_record.management_object.status == 'p':
            self.downloads = self.downloads + 1
            self.save()
            MetadataDocument().catalogue_update(self.metadata_record)

    def delete(self, using=None, keep_parents=False):
        super().delete()
        MetadataDocument().catalogue_update(self.metadata_record)

    def __str__(self):
        return f'{self.metadata_record}'


class UserDownloadStats(models.Model):
    class Meta:
        verbose_name_plural = 'User Download Stats'

    user = models.ForeignKey(
        ELGUser,
        on_delete=models.SET_NULL,
        null=True,
        related_name='download_stats'
    )

    user_ip = models.CharField(
        max_length=100,
        blank=True
    )

    downloaded_at = models.DateTimeField(
        auto_now_add=True
    )

    source_of_download = models.CharField(
        max_length=1000,
        blank=True
    )

    licenses = ArrayField(
        models.CharField(max_length=200),
        null=True
    )

    downloaded_distribution_id = models.IntegerField(
        blank=True, null=True,
    )

    metadata_record = models.ForeignKey(
        MetadataRecord,
        on_delete=models.CASCADE,
        null=True
    )

    is_latest_download = models.BooleanField(blank=True, null=True)

    aggregated_downloads = models.ManyToManyField(
        'self',
        symmetrical=True,
        blank=True
    )

    def __str__(self):
        return f'{self.user}'

    @property
    def mapped_source_of_download(self):
        mapper = CallTypeMapperSimple()
        if self.source_of_download:
            return mapper.get_mapping(self.source_of_download)
        else:
            return ''

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super().save()
        if self.user and self.metadata_record and self.is_latest_download is None:
            self.aggregate_downloads()

    def aggregate_downloads(self):
        from analytics.consumer_index.documents import MyDownloadsDocument
        other_downloads = UserDownloadStats.objects.filter(
            user=self.user, metadata_record=self.metadata_record).exclude(id=self.id)
        self.aggregated_downloads.add(
            *other_downloads
        )
        if self.downloaded_at and not self.aggregated_downloads.filter(downloaded_at__gte=self.downloaded_at):
            self.is_latest_download = True
            self.save()
            MyDownloadsDocument().update(self)
            previous_latest = self.aggregated_downloads.filter(downloaded_at__lte=self.downloaded_at, is_latest_download=True)
            for dl in previous_latest:
                dl.is_latest_download = False
                dl.save()
            if previous_latest:
                MyDownloadsDocument().update(previous_latest, action='delete', raise_on_error=False)
        else:
            self.is_latest_download = False
            self.save()

    @property
    def distribution_of_downloaded_dataset(self):
        if self.metadata_record and self.downloaded_distribution_id:
            if self.metadata_record.described_entity.entity_type == 'LanguageResource':
                lr_type = self.metadata_record.described_entity.lr_subclass.lr_type
                if lr_type == 'ToolService':
                    distributions = self.metadata_record.described_entity.lr_subclass.software_distribution.all()
                else:
                    distributions = self.metadata_record.described_entity.lr_subclass.dataset_distribution.all()
                dl_dist = distributions.get(pk=self.downloaded_distribution_id)
                return dl_dist
            else:
                return None
        return None
