from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
    CompoundSearchFilterBackend,
    MultiMatchSearchFilterBackend)
from django_elasticsearch_dsl_drf.pagination import PageNumberPagination
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet
from rest_framework.permissions import AllowAny

# from analytics.analytics_index.documents import ProcessingJobDocument
# from analytics.analytics_index.serializers import ProcessingJobDocumentSerializer


class ProcessingJobDocumentView(BaseDocumentViewSet):
    """The MetadataDocument view."""

    # document = ProcessingJobDocument
    # serializer_class = ProcessingJobDocumentSerializer
    pagination_class = PageNumberPagination
    permission_classes = (AllowAny,)
    lookup_field = 'id'
    filter_backends = [
        FilteringFilterBackend,
        CompoundSearchFilterBackend,
        MultiMatchSearchFilterBackend,
        DefaultOrderingFilterBackend,
        OrderingFilterBackend,
    ]

    simple_query_string_options = {
        "default_operator": "and",
    }

    # Define search fields
    search_fields = {
        'user': None,
        'lt_service': None,
        'start_at': None,
        'end_at': None,
        'input_source': None,
        'input_type': None,
        'elg_resource': None
    }
    # Define filter fields
    filter_fields = {
        'id': None,
        'user': 'user.raw',
        'lt_service': 'lt_service.raw',
        'input_source': 'input_source.raw',
        'input_type': 'input_type.raw',
        'elg_resource': 'elg_resource.raw'
    }
    # Define ordering fields
    ordering_fields = {
        'id': 'id',
        'lt_service': 'lt_service.raw'
    }
    # Specify default ordering
    ordering = ('_score', 'lt_service.raw', 'id',)
