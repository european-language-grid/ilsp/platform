from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

# from .documents import ProcessingJobDocument


# class ProcessingJobDocumentSerializer(DocumentSerializer):
#     """Serializer for the ProcessingJob document."""
#
#     class Meta(object):
#         """Meta options."""
#
#         # Specify the correspondent document class
#         document = ProcessingJobDocument
#
#         # List the serializer fields. Note, that the order of the fields
#         # is preserved in the ViewSet.
#         fields = (
#             'id',
#             'user',
#             'lt_service',
#             'start_at',
#             'end_at',
#             'bytes_processed ',
#             'input_source',
#             'input_type',
#             'elg_resource',
#         )
