from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from analytics.models import MetadataRecordStats, ServiceStats, UserDownloadStats


class MetadataRecordStatsSerializer(serializers.ModelSerializer):
    all_versions_views = serializers.SerializerMethodField(read_only=True)
    all_versions_downloads = serializers.SerializerMethodField(read_only=True)
    service_used = serializers.SerializerMethodField(read_only=True)
    all_version_services_used = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = MetadataRecordStats
        fields = (
            'pk',
            'metadata_record',
            'views',
            'all_versions_views',
            'downloads',
            'all_versions_downloads',
            'service_used',
            'all_version_services_used'
        )

        extra_kwargs = {
            'docker_download_location': {'read_only': True},
            'service_adapter_download_location': {'read_only': True}
        }

    def get_all_versions_views(self, obj=None):
        return obj.all_versions_views

    def get_all_versions_downloads(self, obj=None):
        return obj.all_versions_downloads

    def get_service_used(self, obj=None):
        record = getattr(obj, 'metadata_record', None)
        service_execution_count = 0
        if record:
            if record.management_object.functional_service:
                try:
                    lt_service = record.lt_service
                    service_execution_count += ServiceStats.aggregate(lt_service)
                except ObjectDoesNotExist:
                    service_execution_count += 0
                return service_execution_count
        return service_execution_count

    def get_all_version_services_used(self, obj=None):
        record = getattr(obj, 'metadata_record', None)
        service_execution_count = 0
        if record:
            if record.management_object.functional_service:
                try:
                    lt_service = record.lt_service
                    all_version_services_qr = lt_service.all_version_services.all()
                    all_version_services = [ver for ver in all_version_services_qr]
                    all_version_services.append(lt_service)
                    for service in all_version_services:
                        service_execution_count += ServiceStats.aggregate(service)
                except ObjectDoesNotExist:
                    service_execution_count += 0
                return service_execution_count
        return 0


class ServiceStatsSerializer(serializers.ModelSerializer):
    duration = serializers.SerializerMethodField(read_only=True)
    mapped_call_type = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ServiceStats
        fields = (
            'pk',
            'user',
            'lt_service',
            'start_at',
            'end_at',
            'duration',
            'bytes',
            'elg_resource',
            'status',
            'user_ip',
            'call_type',
            'mapped_call_type',
            'is_latest_usage',
            'aggregated_usages'
        )

    def get_fields(self):
        fields = super().get_fields()
        for field in fields.values():
            field.read_only = True
        return fields

    def get_duration(self, obj=None):
        duration = None
        if obj:
            duration = obj.duration
        return duration

    def get_mapped_call_type(self, obj):
        mapped_call_type = None
        if obj:
            mapped_call_type = obj.mapped_call_type
        return mapped_call_type


class UserDownloadStatsSerializer(serializers.ModelSerializer):
    file_name = serializers.SerializerMethodField(read_only=True)
    mapped_source_of_download = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = UserDownloadStats
        fields = ('pk',
                  'user',
                  'user_ip',
                  'downloaded_at',
                  'source_of_download',
                  'mapped_source_of_download',
                  'licenses',
                  'downloaded_distribution_id',
                  'metadata_record',
                  'file_name',
                  'is_latest_download',
                  'aggregated_downloads'
                  )

    def get_fields(self):
        fields = super().get_fields()
        for field in fields.values():
            field.read_only = True
        return fields

    def get_file_name(self, obj=None):
        file_name = None
        record = getattr(obj, 'metadata_record', None)
        if record.management_object.has_content_file:
            dist = obj.distribution_of_downloaded_dataset
            if record.described_entity.entity_type == 'LanguageResource' and dist:
                lr_type = record.described_entity.lr_subclass.lr_type
                if lr_type == 'ToolService':
                    file_name = dist.package.file.name
                else:
                    file_name = dist.dataset.file.name
        return file_name

    def get_mapped_source_of_download(self, obj):
        mapped_source_of_download = None
        if obj:
            mapped_source_of_download = obj.mapped_source_of_download
        return mapped_source_of_download
