from django.contrib import admin

# Register your models here.
from analytics import models
from utils.admin_utils import ReadOnlyModelAdmin


class AnalyticsAdmin(ReadOnlyModelAdmin):
    def __init__(self, *args, **kwargs):
        super(AnalyticsAdmin, self).__init__(*args, **kwargs)
        self.readonly_fields = [f.name for f in self.model._meta.get_fields()]


class ServiceStatsAdmin(AnalyticsAdmin):
    list_display = [
        'username',
        'lt_service',
        'call_type',
        'bytes',
        'elg_resource',
        'start_at',
        'end_at',
        'duration',
        'status'
    ]

    list_filter = ['status']

    search_fields = ['lt_service__metadata_record__described_entity__languageresource__resource_name',
                     'call_type', 'status', 'user__username']

    def duration(self, obj):
        return (obj.end_at - obj.start_at).total_seconds()

    def username(self, obj):
        if obj.user:
            return str(obj.user.username)
        else:
            return ""


class UserDownloadStatsAdmin(AnalyticsAdmin):
    list_display = [
        'user',
        'user_ip',
        'metadata_record',
        'source_of_download',
        'downloaded_at',
        'licenses'
    ]

    search_fields = ['user__username', 'metadata_record__described_entity__languageresource__resource_name__en']


class MetadataRecordStatsAdmin(AnalyticsAdmin):
    list_display = [
        'metadata_record',
        'views',
        'downloads'
    ]

    search_fields = ['metadata_record__described_entity__languageresource__resource_name__en']

    ordering = ('-views', '-downloads')


admin.site.register(models.ServiceStats, ServiceStatsAdmin)
admin.site.register(models.UserDownloadStats, UserDownloadStatsAdmin)
admin.site.register(models.MetadataRecordStats, MetadataRecordStatsAdmin)
