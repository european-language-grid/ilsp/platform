import copy
import datetime
import json
import logging

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from test_utils import SerializerTestCase

from analytics.models import ServiceStats, MetadataRecordStats, UserDownloadStats
from django.conf import settings
from processing.serializers import RegisteredLTServiceSerializer
from test_utils import import_metadata, get_test_user

LOGGER = logging.getLogger(__name__)



class TestServiceStatsModel(SerializerTestCase):
    fixtures = [
        'test_utils/fixtures/test_described_entity', 'test_utils/fixtures/test_actors',
        'test_utils/fixtures/test_persons', 'test_utils/fixtures/test_groups', 'test_utils/fixtures/test_users'
    ]
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpTestData(cls):
        cls.user, cls.token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfile.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            raw_data = copy.deepcopy(cls.ltd_data)
            raw_data['elg_execution_location'] = 'http://www.executionlocation.com/different_link'
            raw_data['status'] = 'COMPLETED'
            raw_data['elg_hosted'] = True
            update_serializer = RegisteredLTServiceSerializer(cls.instance, data=raw_data)
            try:
                update_serializer.is_valid()
                update_instance = update_serializer.save()
            except ValidationError:
                LOGGER.info(ValidationError)
        else:
            LOGGER.info(cls.serializer.errors)
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.service_stats = {
            'user': cls.user,
            'start_at': datetime.datetime.now(),
            'end_at': datetime.datetime.now(),
            'lt_service': cls.instance,
            'bytes': 12,
            'elg_resource': None,
            'status': 'succeeded',
            'call_type': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) '
                         'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36 OPR/85.0.4341.60'
        }

    def test_service_stats_creation(self):
        service_stats_instance = ServiceStats.objects.create(**self.service_stats)
        self.assertEquals(str(service_stats_instance), f'{self.instance} ({self.user})')

    def test_service_stats_deletion(self):
        service_stats_instance = ServiceStats.objects.create(**self.service_stats)
        service_stats_instance.delete()
        try:
            obj = ServiceStats.objects.get(id=service_stats_instance.pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestMetadataRecordStats(SerializerTestCase):
    fixtures = [
        'test_utils/fixtures/test_described_entity', 'test_utils/fixtures/test_actors',
        'test_utils/fixtures/test_persons', 'test_utils/fixtures/test_groups', 'test_utils/fixtures/test_users'
    ]
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpTestData(cls):
        cls.user, cls.token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfile.com',
            'status': 'PENDING',
            'accessor_id': 'x0f214x3120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            raw_data = copy.deepcopy(cls.ltd_data)
            raw_data['elg_execution_location'] = 'http://www.executionlocation.com/different_link'
            raw_data['status'] = 'COMPLETED'
            raw_data['elg_hosted'] = True
            update_serializer = RegisteredLTServiceSerializer(cls.instance, data=raw_data)
            try:
                update_serializer.is_valid()
                update_instance = update_serializer.save()
            except ValidationError:
                LOGGER.info(ValidationError)
        else:
            LOGGER.info(cls.serializer.errors)
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.mdr_stats = {
            'metadata_record': cls.metadata_records[1],
            'views': 0,
            'downloads': 0
        }

    def test_mdr_stats_creation(self):
        mdr_stats_instance = MetadataRecordStats.objects.create(**self.mdr_stats)
        self.assertEquals(str(mdr_stats_instance), f'{self.metadata_records[1]}')

    def test_mdr_stats_do_not_increment_views_when_not_published(self):
        mdr_stats_instance = MetadataRecordStats.objects.create(**self.mdr_stats)
        initial_views = mdr_stats_instance.views
        mdr_stats_instance.increment_views()
        views_after = mdr_stats_instance.views
        self.assertTrue(views_after == initial_views)

    def test_mdr_stats_do_not_increment_downloads_when_not_published(self):
        mdr_stats_instance = MetadataRecordStats.objects.create(**self.mdr_stats)
        initial_dl = mdr_stats_instance.downloads
        mdr_stats_instance.increment_downloads()
        dl_after = mdr_stats_instance.downloads
        self.assertTrue(dl_after == initial_dl)

    def test_mdr_stats_increment_views_when_published(self):
        instance = self.metadata_records[1]
        instance.management_object.status = 'p'
        instance.management_object.save()
        mdr_stats_instance = MetadataRecordStats.objects.create(**self.mdr_stats)
        initial_views = mdr_stats_instance.views
        mdr_stats_instance.increment_views()
        views_after = mdr_stats_instance.views
        self.assertTrue(views_after > initial_views)
        instance.management_object.status = 'i'
        instance.management_object.save()

    def test_mdr_stats_increment_downloads_when_published(self):
        instance = self.metadata_records[1]
        instance.management_object.status = 'p'
        instance.management_object.save()
        mdr_stats_instance = MetadataRecordStats.objects.create(**self.mdr_stats)
        initial_dl = mdr_stats_instance.downloads
        mdr_stats_instance.increment_downloads()
        dl_after = mdr_stats_instance.downloads
        self.assertTrue(dl_after > initial_dl)
        instance.management_object.status = 'i'
        instance.management_object.save()

    def test_mdr_stats_deletion(self):
        mdr_stats_instance = MetadataRecordStats.objects.create(**self.mdr_stats)
        mdr_stats_instance.delete()
        try:
            obj = MetadataRecordStats.objects.get(id=mdr_stats_instance.pk)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestUserDownloadStats(SerializerTestCase):
    fixtures = [
        'test_utils/fixtures/test_described_entity', 'test_utils/fixtures/test_actors',
        'test_utils/fixtures/test_persons', 'test_utils/fixtures/test_groups', 'test_utils/fixtures/test_users'
    ]
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    raw_mdr = json.loads(open(test_data[1], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpTestData(cls):
        cls.user, cls.token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.test_data)

        cls.ltd_data = {
            'metadata_record': cls.metadata_records[1].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfile.com',
            'status': 'PENDING',
            'accessor_id': 'x0f214312x0x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.ltd_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            raw_data = copy.deepcopy(cls.ltd_data)
            raw_data['elg_execution_location'] = 'http://www.executionlocation.com/different_link'
            raw_data['elg_hosted'] = True
            update_serializer = RegisteredLTServiceSerializer(cls.instance, data=raw_data)
            try:
                update_serializer.is_valid()
                update_instance = update_serializer.save()
            except ValidationError:
                LOGGER.info(ValidationError)
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.raw_data = {
            "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
            "type": "PermissionsRequest",
            "ltServiceID": "x0f2143120x",
            "user_token": "preferred_username",
            "reserve": {
                "nCalls": 1,
                "nBytes": 12
            }
        }
        cls.user_stats = {
            'user': cls.user,
            'user_ip': 'an ip address',
            'downloaded_at': '2020-01-01',
            'licenses': ["ELG-ENT-LIC-050320-00000769"],
            'metadata_record': cls.metadata_records[1],
        }

    def test_user_stats_creation(self):
        user_stats_instance = UserDownloadStats.objects.create(**self.user_stats)
        self.assertEquals(str(user_stats_instance), f'{self.user}')
