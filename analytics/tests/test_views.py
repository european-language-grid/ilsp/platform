import datetime

from analytics.models import UserDownloadStats, ServiceStats
from analytics.views import landing_page_stats
import copy
import json
import logging
from django.conf import settings

from management.models import ContentFile
from processing.models import RegisteredLTService
from registry.models import MetadataRecord, DatasetDistribution
from test_utils import EndpointTestCase, get_test_user, import_metadata, api_call

LOGGER = logging.getLogger(__name__)

GET_STATS_ENDPOINT = f'/{settings.HOME_PREFIX}/api/cnsmr/anltcs/get_record_stats/'
USER_DOWNLOAD_STATS_ENDPOINT = f'/{settings.HOME_PREFIX}/api/cnsmr/anltcs/user_download_stats/'
SERVICE_STATS_ENDPOINT = f'/{settings.HOME_PREFIX}/api/cnsmr/anltcs/service_stats/'


class TestGetStatsEndpoint(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'registry/tests/fixtures/tool.json',
                 'registry/tests/fixtures/tool_version.json', 'registry/tests/fixtures/corpus.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    raw_data_tool = json.loads(open(test_data[1], encoding='utf-8').read())
    raw_data_corpus = json.loads(open(test_data[2], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_get_stats_any_mdr_superuser(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        response = api_call('GET', f'{GET_STATS_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content'), {'record': {'views': 0}})

    def test_get_stats_functional_and_version_superuser(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[1]
        md.management_object.functional_service = True
        md.management_object.save()
        lt_service = RegisteredLTService.objects.get_or_create(metadata_record=md)
        response = api_call('GET', f'{GET_STATS_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content'), {'record': {'views': 0, 'service_used': 0},
                                                         'versions': {'views': 0, 'service_used': 0}})

    def test_get_stats_dataset_superuser(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[3]
        ContentFile.objects.get_or_create(file=f'{md.management_object.identifier}/test_filename.zip',
                                          record_manager=md.management_object)
        dist = DatasetDistribution.objects.get(id=md.described_entity.lr_subclass.dataset_distribution.first().id)
        dist.dataset = ContentFile.objects.get(file=f'{md.management_object.identifier}/test_filename.zip')
        dist.save()
        response = api_call('GET', f'{GET_STATS_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content'), {'record': {'views': 0, 'downloads': 0}})

    def test_get_stats_any_mdr_content_manager(self):
        user, token = self.c_m, self.c_m_token
        md = self.metadata_records[0]
        response = api_call('GET', f'{GET_STATS_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content'), {'record': {'views': 0}})

    def test_get_stats_any_mdr_provider_own(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.curator = user
        md.management_object.save()
        response = api_call('GET', f'{GET_STATS_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content'), {'record': {'views': 0}})

    def test_get_stats_any_mdr_provider_other(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.curator = self.admin
        md.management_object.save()
        response = api_call('GET', f'{GET_STATS_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content'), {'record': {'views': 0}})

    def test_get_stats_any_mdr_consumer(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        response = api_call('GET', f'{GET_STATS_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content'), {'record': {'views': 0}})

    def test_get_stats_any_mdr_anon(self):
        md = self.metadata_records[0]
        response = api_call('GET', f'{GET_STATS_ENDPOINT}{md.id}/', self.client)
        self.assertEquals(response.get('status_code'), 200)


class TestUserDownloadStatsEndpoint(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'registry/tests/fixtures/tool.json',
                 'registry/tests/fixtures/tool_version.json', 'registry/tests/fixtures/corpus.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    raw_data_tool = json.loads(open(test_data[1], encoding='utf-8').read())
    raw_data_corpus = json.loads(open(test_data[2], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_user_download_stats_superuser(self):
        user, token = self.admin, self.admin_token
        ud1 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[1])
        ud2 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[1])
        ud3 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[2])
        response = api_call('GET', f'{USER_DOWNLOAD_STATS_ENDPOINT}{ud2.id}/', self.client,
                            auth=self.authenticate(token))
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        id_list = [i.get('pk') for i in response.get('json_content', [])]
        self.assertEquals(id_list, [ud1.id, ud2.id])

    def test_user_download_stats_content_manager(self):
        user, token = self.c_m, self.c_m_token
        ud1 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[1])
        ud2 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[1])
        ud3 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[2])
        response = api_call('GET', f'{USER_DOWNLOAD_STATS_ENDPOINT}{ud2.id}/', self.client,
                            auth=self.authenticate(token))
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        id_list = [i.get('pk') for i in response.get('json_content', [])]
        self.assertEquals(id_list, [ud1.id, ud2.id])

    def test_user_download_stats_provider(self):
        user, token = self.provider, self.provider_token
        ud1 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[1])
        ud2 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[1])
        ud3 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[2])
        response = api_call('GET', f'{USER_DOWNLOAD_STATS_ENDPOINT}{ud2.id}/', self.client,
                            auth=self.authenticate(token))
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        id_list = [i.get('pk') for i in response.get('json_content', [])]
        self.assertEquals(id_list, [ud1.id, ud2.id])

    def test_user_download_stats_consumer(self):
        user, token = self.consumer, self.consumer_token
        ud1 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[1])
        ud2 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[1])
        ud3 = UserDownloadStats.objects.create(user=user, metadata_record=self.metadata_records[2])
        response = api_call('GET', f'{USER_DOWNLOAD_STATS_ENDPOINT}{ud2.id}/', self.client,
                            auth=self.authenticate(token))
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        id_list = [i.get('pk') for i in response.get('json_content', [])]
        self.assertEquals(id_list, [ud1.id, ud2.id])

    def test_user_download_stats_anon(self):
        ud2 = UserDownloadStats.objects.create(metadata_record=self.metadata_records[1])
        response = api_call('GET', f'{USER_DOWNLOAD_STATS_ENDPOINT}{ud2.id}/', self.client)
        self.assertEquals(response.get('status_code'), 403)


class TestServiceStatsEndpoint(EndpointTestCase):
    test_data = ['registry/tests/fixtures/tool.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)
        cls.lt_service = RegisteredLTService.objects.create(metadata_record=cls.metadata_records[0])

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_service_stats_superuser(self):
        user, token = self.admin, self.admin_token
        ud1 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        ud2 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        ud3 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        response = api_call('GET', f'{SERVICE_STATS_ENDPOINT}{ud3.id}/', self.client,
                            auth=self.authenticate(token))
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        id_list = [i.get('pk') for i in response.get('json_content', [])]
        self.assertEquals(id_list, [ud1.id, ud2.id, ud3.id])

    def test_service_stats_content_manager(self):
        user, token = self.c_m, self.c_m_token
        ud1 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        ud2 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        ud3 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        response = api_call('GET', f'{SERVICE_STATS_ENDPOINT}{ud3.id}/', self.client,
                            auth=self.authenticate(token))
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        id_list = [i.get('pk') for i in response.get('json_content', [])]
        self.assertEquals(id_list, [ud1.id, ud2.id, ud3.id])

    def test_service_stats_provider(self):
        user, token = self.provider, self.provider_token
        ud1 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        ud2 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        ud3 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        response = api_call('GET', f'{SERVICE_STATS_ENDPOINT}{ud3.id}/', self.client,
                            auth=self.authenticate(token))
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        id_list = [i.get('pk') for i in response.get('json_content', [])]
        self.assertEquals(id_list, [ud1.id, ud2.id, ud3.id])

    def test_service_stats_consumer(self):
        user, token = self.consumer, self.consumer_token
        ud1 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        ud2 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        ud3 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=user)
        response = api_call('GET', f'{SERVICE_STATS_ENDPOINT}{ud3.id}/', self.client,
                            auth=self.authenticate(token))
        print(response)
        self.assertEquals(response.get('status_code'), 200)
        id_list = [i.get('pk') for i in response.get('json_content', [])]
        self.assertEquals(id_list, [ud1.id, ud2.id, ud3.id])

    def test_service_stats_anon(self):
        ud3 = ServiceStats.objects.create(lt_service=self.lt_service,
                                          start_at=datetime.datetime.now(),
                                          bytes=1000,
                                          elg_resource=self.metadata_records[0],
                                          user=self.admin)
        response = api_call('GET', f'{SERVICE_STATS_ENDPOINT}{ud3.id}/', self.client)
        self.assertEquals(response.get('status_code'), 403)
