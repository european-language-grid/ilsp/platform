# Create your views here.
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status, generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from analytics.models import MetadataRecordStats, UserDownloadStats, ServiceStats
from analytics.serializers import MetadataRecordStatsSerializer, UserDownloadStatsSerializer, ServiceStatsSerializer
from registry.models import MetadataRecord
from registry.permissions import RetrieveMetadataRecordPermission


@api_view(http_method_names=['GET'])
@permission_classes([AllowAny])
def service_stats_aggregations(request):
    pass


@api_view(http_method_names=['GET'])
@permission_classes([RetrieveMetadataRecordPermission])
def landing_page_stats(request, pk):
    return_dict = {}
    record_dict = {'record': {}}
    versions_dict = {'versions': {}}
    has_versions = False
    record = MetadataRecord.objects.get(pk=pk)
    record_manager = record.management_object
    record_stats, _ = MetadataRecordStats.objects.get_or_create(metadata_record=record)
    serializer = MetadataRecordStatsSerializer(instance=record_stats)
    stats_data = serializer.data
    if record_manager.versioned_record_managers.exists():
        has_versions = True
    record_dict['record']['views'] = stats_data.get('views')
    if has_versions:
        versions_dict['versions']['views'] = stats_data.get('all_versions_views')
    if record_manager.has_content_file:
        record_dict['record']['downloads'] = stats_data.get('downloads')
        if has_versions:
            versions_dict['versions']['downloads'] = stats_data.get('all_versions_downloads')
    if record_manager.functional_service:
        record_dict['record']['service_used'] = stats_data.get('service_used')
        if has_versions:
            versions_dict['versions']['service_used'] = stats_data.get('all_version_services_used')
    return_dict.update(record_dict)
    if has_versions:
        return_dict.update(versions_dict)
    return Response(return_dict, status=status.HTTP_200_OK)


class UserDownloadStatsView(generics.RetrieveAPIView):
    """
    Bulk list user download stats
    """
    serializer_class = UserDownloadStatsSerializer
    queryset = UserDownloadStats
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        pk = self.kwargs.pop('pk')
        initial_instance = self.queryset.objects.get(id=pk)
        instances = list(initial_instance.aggregated_downloads.all())
        instances.append(initial_instance)
        content = list()
        for instance in instances:
            content.append(self.serializer_class(instance=instance).data)
        return Response(
            content,
            status=status.HTTP_200_OK
        )


class ServiceStatsView(generics.RetrieveAPIView):
    """
    Bulk list service stats
    """
    serializer_class = ServiceStatsSerializer
    queryset = ServiceStats
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        pk = self.kwargs.pop('pk')
        print(pk)
        initial_instance = self.queryset.objects.get(id=pk)
        instances = list(initial_instance.aggregated_usages.all())
        instances.append(initial_instance)
        content = list()
        for instance in instances:
            content.append(self.serializer_class(instance=instance).data)

        return Response(
            content,
            status=status.HTTP_200_OK
        )