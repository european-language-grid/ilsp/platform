from rest_framework import routers
from django.urls import include, path

from . import views

app_name = 'oai'

router = routers.DefaultRouter()
router.register('zenodo-harvesting', views.ZenodoRecordCreationView, basename='zenodoharvesting')


urlpatterns = (
 path('', include(router.urls)),
)