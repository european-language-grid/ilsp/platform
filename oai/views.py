import csv
import json

from rest_framework import viewsets, status
from rest_framework.generics import CreateAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from oai.zenodo_utils import HarvestZenodo, zenodo_record_creation


class ZenodoRecordCreationView(viewsets.ViewSet, UpdateAPIView, RetrieveAPIView, CreateAPIView):
    """Zenodo record handling view"""

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        updated_permission_classes = [IsAdminUser]
        return [permission() for permission in updated_permission_classes]

    def retrieve(self, request, *args, **kwargs):
        """
        retrieve all zenodo records
        """
        z_h = HarvestZenodo()
        harvested_files = z_h.get_all_harvested_records()
        # TODO fix
        return Response(harvested_files)

    def update(self, request, *args, **kwargs):
        """
        delete all zenodo files at service
        """
        z_h = HarvestZenodo()
        z_h.delete_all_harvested_records()
        return Response(
            {'Zenodo file deletion initialized'},
            status=status.HTTP_200_OK
        )

    def create(self, request, *args, **kwargs):
        """
        create zenodo records
        """
        if not (request.data.get('json_file') and request.data.get('csv_file')):
            return Response(
                {'A json file of records and a csv file of selections are required'},
                status=status.HTTP_400_BAD_REQUEST
            )
        json_file = request.FILES['json_file'].temporary_file_path()
        with open(json_file, 'r', encoding='UTF-8') as f:
            print('here')
            json_file = json.load(f)
        csv_file = request.FILES['csv_file'].temporary_file_path()
        with open(csv_file, 'r', encoding='UTF-8') as csv_f:
            reader = csv.DictReader(csv_f, delimiter=',')
            csv_list = [row for row in reader]
        zenodo_record_creation.apply_async(args=[json_file, csv_list])
        return Response(
                {'Zenodo record creation initialized'},
                status=status.HTTP_200_OK
            )
