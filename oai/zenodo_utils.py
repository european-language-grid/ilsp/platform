import csv
import logging
import json
import os
import re
import time
from collections import Counter
from json import JSONDecodeError

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from rest_framework.exceptions import ValidationError
from packaging import version as _ver
from pyexpat import ExpatError
from urllib.parse import quote

import requests
import xmltodict
from django.conf import settings
from nested_lookup import nested_delete
from rest_framework.response import Response

from accounts.models import ELGUser
from catalogue_backend.celery import app as celery_app
from email_notifications.email_notification_utils import zenodo_harvesting_service_call_fail, \
    harvested_zenodo_record_update
from harvesting_scripts.harvesting_utils import harvested_record_serializer_validation, harvesting_duplicate_handling, \
    harvesting_versions_of_existing_records_handling
from management.catalog_report_index.documents import CatalogReportDocument
from management.utils.publication_field_generation import AutomaticPublicationFieldGeneration
from oai.models import ZenodoRecord
from registry import choices
from registry.models import Person, Repository, MetadataRecord, DescribedEntity
from registry.retrieval_mechanisms import retrieve_licence_by_licence_terms_and_name, \
    retrieve_licence_terms_given_only_name
from registry.search_indexes.documents import MetadataDocument
from registry.serializers import GenericLicenceTermsSerializer, GenericLanguageResourceSerializer, \
    GenericPersonSerializer, GenericRepositorySerializer, MetadataRecordSerializer
from registry.view_tasks import compute_dle_score
from utils.index_utils import access_rights_statement_mapping

LOGGER = logging.getLogger(__name__)

class ZenodoOAIMapping:

    def __init__(self, data):
        # data > (dict, lang_set, inferred)
        self.all_data = data
        self.data = data[0]
        self.language = data[1]
        self.lr_type = data[2]
        self.entity_type = data[3]
        self.inferred = True if data[4] in [True, 'True', 'true', 'TRUE', 'Yes', 'YES', 'yes'] else False
        self.mapping_errors = []
        self.map_zenodo_lang_ids = {
        'lav': 'lv',
        'nld': 'nl',
        'tir': 'ti',
        'eng': 'en',
        'oci': 'oc',
        'fin': 'fi',
        'swa': 'sw',
        'dan': 'da',
        'isl': 'ice',
        'tur': 'tr',
        'deu': 'de',
        'rus': 'ru',
        'hin': 'hi',
        'hrv': 'hr',
        'tha': 'th',
        'fra': 'fr',
        'lug': 'lg',
        'nob': 'nb',
        'bod': 'bo',
        'tam': 'ta',
        'ita': 'it',
        'lit': 'lt',
        'zho': 'zh',
        'mal': 'ml',
        'vie': 'vi',
        'ell': 'el',
        'est': 'et',
        'spa': 'es',
        'hun': 'hu',
        'urd': 'ur',
        'por': 'pt',
        'nya': 'ny',
        'san': 'sa',
        'msa': 'ms',
        'kor': 'ko',
        'cat': 'ca',
        'twi': 'tw',
        'ron': 'ro',
        'swe': 'sv',
        'khm': 'km',
        'zha': 'za',
        'fas': 'fa',
        'ltz': 'lb',
        'zul': 'zu',
        'amh': 'am',
        'ces': 'cs',
        'slk': 'sk',
        'mya': 'my',
        'bis': 'bi',
        'ukr': 'uk',
        'lat': 'la',
        'slv': 'sl',
        'ind': 'id',
        'wol': 'wo',
        'uzb': 'uz',
        'jpn': 'ja',
        'heb': 'he',
        'pol': 'pl',
        'ben': 'bn',
        'ara': 'ar',
        'spanish': "Spanish; Castilian",
        'castilian': "Spanish; Castilian",
        'dutch': "Dutch; Flemish",
        'flemish': "Dutch; Flemish",
        'catalan': "Catalan; Valencian",
        'valencian': "Catalan; Valencian",
        'alsatian': "Swiss German; Alemannic; Alsatian",
        'swiss german': "Swiss German; Alemannic; Alsatian",
        'alemannic': "Swiss German; Alemannic; Alsatian",
    }
        self.ACCESS_TOKEN = 'qS7kRkmvQze3xaWOGLiEt0RJWhh94XTL9baKpd4UDkqQ6qXEMuY39B6XnxzL'

    def map_language(self):
        lang_ids = []
        issue_langs = set()
        langs = []
        if isinstance(self.language, str):
            lang_map = {
                'CA': 'CAT',
                'DUT': 'NLD',
                'FRE': 'FRA',
                'GER': 'DEU',
                'ID': 'MQA',
                'LA': 'LAT',
                'RO': 'ROM',
                'RUM': 'RUS',
                'TI': 'TIG',
                'TW': 'TWI',
                'YO': 'KNA',
                'VI': 'VIE'
            }
            lang_split = self.language.split('/')
            lang_id = lang_split.pop()
            if lang_map.get(lang_id):
                lang_split.append(lang_map.get(lang_id))
                self.language = '/'.join(lang_split)
            try:
                response = requests.get(self.language)
                lang_xml = response._content.decode()
                lang_dict = xmltodict.parse(lang_xml, xml_attribs=True)['rdf:RDF']
            except (JSONDecodeError, ExpatError):
                lang_dict = {}
            lang_notation = lang_dict.get('rdf:Description', {}).get('skos:notation', [])
            lang_labels = lang_dict.get('rdf:Description', {}).get('skos:prefLabel', [])
            if not isinstance(lang_notation, list):
                lang_notation = [lang_notation]
            if lang_notation:
                for notation in lang_notation:
                    if notation.get('@rdf:datatype') == "http://publications.europa.eu/ontology/euvoc#ISO_639_1":
                        lang = notation.get('#text').lower()
                        if lang:
                            lang = self.map_zenodo_lang_ids.get(lang, lang).strip()
                            langs.append(lang.lower())
                            break
                    elif notation.get('@rdf:datatype') == "http://publications.europa.eu/ontology/euvoc#ISO_639_3":
                        lang = notation.get('#text').lower()
                        if lang:
                            lang = self.map_zenodo_lang_ids.get(lang, lang).strip()
                            langs.append(lang.lower())
                            break
            elif lang_labels:
                if not isinstance(lang_labels, list):
                    lang_labels = [lang_labels]
                if lang_labels:
                    for _label in lang_labels:
                        if _label.get('@xml:lang') == 'en':
                            lang = _label.get('#text').lower()
                            if lang:
                                lang = self.map_zenodo_lang_ids.get(lang, lang).strip()
                                langs.append(lang.lower())
                                break
                        elif _label.get('@xml:lang') == self.language.split('/')[-1].lower():
                            lang = _label.get('#text').lower()
                            if lang:
                                lang = self.map_zenodo_lang_ids.get(lang, lang).strip()
                                langs.append(lang.lower())
                                break
                        elif _label.get('@xml:lang') == lang_id.lower():
                            lang = _label.get('#text').lower()
                            if lang:
                                lang = self.map_zenodo_lang_ids.get(lang, lang).strip()
                                langs.append(lang.lower())
                                break
        else:
            langs = self.language
        for lang in langs:
            lang = self.map_zenodo_lang_ids.get(lang, lang).strip()
            try:
                _lang = choices.LANGUAGE_ID_CHOICES.__getitem__(lang)
            except KeyError:
                _lang = None
            add_lang = 0
            if _lang:
                lang_ids.append(lang)
                add_lang = 1
            else:
                for tag, _lang in choices.LANGUAGE_ID_CHOICES:
                    if _lang.lower() == lang.lower():
                        lang_ids.append(tag)
                        add_lang = 1
            if not add_lang:
                issue_langs.add(lang)
        languages = []
        if lang_ids:
            for _tag in lang_ids:
                if _tag and not _tag == 'mis':
                    language_dict = {'language_id': _tag}
                    if language_dict not in languages:
                        languages.append(language_dict)
        if issue_langs:
            print('issue lang', issue_langs)
        return languages

    def map_lr_type(self, metadata):
        add_to_query = None
        lr_identifier = self.map_lr_identifier(metadata)
        base_query = Q(Q(management_object__deleted=False) & ~Q(management_object__status='d'))
        lr_identifiers = []
        for lr_id in lr_identifier:
            if 'http' in lr_id['value'] and not 'https' in lr_id['value']:
                lr_identifiers.append({'lr_identifier_scheme': lr_id['lr_identifier_scheme'],
                                       'value': lr_id['value'].replace('http', 'https')})
            elif 'https' in lr_id['value']:
                lr_identifiers.append({'lr_identifier_scheme': lr_id['lr_identifier_scheme'],
                                       'value': lr_id['value'].replace('https', 'http')})
            lr_identifiers.append(lr_id)
        for lr_id in lr_identifiers:
            new_query = Q(Q(**{
                'described_entity__languageresource__lr_identifier__lr_identifier_scheme': lr_id['lr_identifier_scheme']})
                          & Q(**{'described_entity__languageresource__lr_identifier__value': lr_id['value']}))
            if not add_to_query:
                add_to_query = new_query
            else:
                add_to_query = add_to_query | new_query
        additional_info = self.get_additional_info(metadata)
        landing_pages = []
        for a_i in additional_info:
            if 'http' in a_i['landing_page'] and not 'https' in a_i['landing_page']:
                landing_pages.append({'landing_page': a_i['landing_page'].replace('http', 'https')})
            elif 'https' in a_i['landing_page']:
                landing_pages.append({'landing_page': a_i['landing_page'].replace('https', 'http')})
            landing_pages.append(a_i)
        for a_i in landing_pages:
            new_query = Q(**{'described_entity__languageresource__additional_info__landing_page__icontains':
                                 a_i['landing_page'].split('#.')[0]})
            if not add_to_query:
                add_to_query = new_query
            else:
                add_to_query = add_to_query | new_query
        new_query = Q(
            Q(**{'described_entity__languageresource__resource_name__en__iexact':
                     metadata.get('dct:title', '')}))
        if not add_to_query:
            add_to_query = new_query
        else:
            add_to_query = add_to_query | new_query

        add_to_query = add_to_query & base_query
        found_similar_queryset = MetadataRecord.objects.filter(add_to_query).distinct()
        lr_types_found = list(found_similar_queryset.exclude(
            described_entity__languageresource__lr_subclass__lr_type='LanguageDescription').values_list(
            'described_entity__languageresource__lr_subclass__lr_type', flat=True))
        ld_types = list(found_similar_queryset.filter(
            described_entity__languageresource__lr_subclass__lr_type='LanguageDescription').values_list(
            'described_entity__languageresource__lr_subclass__languagedescription__language_description_subclass__ld_subclass_type'
        ))
        lr_types_found.extend(ld_types)
        if self.lr_type in lr_types_found:
            return self.lr_type
        else:
            lr_type_occurrences = Counter(lr_types_found)
            return lr_type_occurrences.most_common(1)[0][0]

    def map_media_parts(self, type):
        language = self.map_language()
        media_part = {}
        if type == 'unspecified':
            media_part = {
                'language': language
            }
        return media_part

    def map_input_content_resource(self):
        languages = self.map_language()
        input_resource = {'processing_resource_type': 'http://w3id.org/meta-share/meta-share/unspecified'}
        if languages:
            input_resource.update({'languages': languages})
        return [input_resource]

    def map_distribution_features(self, type, dist):
        feature_dict = dict()
        data_format = 'http://w3id.org/meta-share/meta-share/unspecified'
        size = [{'size_unit': 'http://w3id.org/meta-share/meta-share/byte',
                 'amount': dist.get('dcat:byteSize', 0)}]
        mimetype = []
        m_type = dist.get('dcat:mediaType')
        if m_type:
            data_format = m_type
            m_response = requests.get(f'{settings.MS_ONTOLOGY_SERVICE}dataformat/?mimetype={quote(m_type)}').json()
            if m_response:
                mimetype.extend(m_response)
                d_f = requests.get(
                    f'{settings.MS_ONTOLOGY_SERVICE}mimetype/?dataformat={quote(m_response[0].split("/")[-1])}').json()
                if d_f:
                    for _df in d_f:
                        data_format = _df[1]
                        break
        if type == 'unspecified':
            feature_dict = {'size': size,
                            'data_format': [data_format],
                            'mimetype': mimetype}
        return feature_dict

    def map_generic_licences(self, dist):
        licences = []
        licence_terms_url = dist.get('dct:license', {}).get('@rdf:resource', '')
        if licence_terms_url:
            licence = {'licence_terms_url': [licence_terms_url]}
            request_user = ELGUser.objects.get(username=settings.SYSTEM_USER)
            licence_retrieved = retrieve_licence_by_licence_terms_and_name(licence, request_user)
            if not licence_retrieved:
                licence_dict = {'licence_terms_name': {'en': ' '.join(licence_terms_url.split('/')[-3:])},
                                'licence_terms_url': [licence_terms_url],
                                'condition_of_use': ['http://w3id.org/meta-share/meta-share/unspecified']}
                lser = GenericLicenceTermsSerializer(data=licence_dict)
                if lser.is_valid():
                    licence_retrieved = lser.save()
                else:
                    print('ERROR', lser.errors)
            if licence_retrieved:
                clean_data = nested_delete(GenericLicenceTermsSerializer(licence_retrieved).data, 'pk')
                licences.append(clean_data)
        return licences

    def map_distribution(self, type, metadata):
        distributions = metadata.get('dcat:distribution')
        if not isinstance(distributions, list):
            distributions = [distributions]
        md_access_rights = metadata.get('dct:accessRights', metadata.get('dct:rights', []))
        access_rights = []
        for a_r in md_access_rights:
            rights_statement = a_r.get('dct:RightsStatement', {})
            label = rights_statement.get('rdfs:label', '')
            if not label:
                label = rights_statement.get('@rdf:about', '').split('/')[-1]
            if label:
                a_r_mapping = {
                    'open': 'Open Access',
                    'closed': 'Closed Access',
                    'Restricted': 'Restricted Access',
                    'restricted': 'Restricted Access'
                }
                l = a_r_mapping.get(label, label)
                access_rights.append({'category_label': {'en': l}})
        licence_init = self.map_generic_licences(metadata)
        if not licence_init:
            for dist in distributions:
                dist = dist.get('dcat:Distribution', dist)
                if set(dist.keys()).union({'dcat:accessURL', 'dct:license'}) == set(dist.keys()).intersection(
                        {'dcat:accessURL', 'dct:license'}):
                    licence_init = self.map_generic_licences(dist)
                    break
        if not licence_init:
            for dist in distributions:
                dist = dist.get('dcat:Distribution', dist)
                if set(dist.keys()).union({'dcat:accessURL', 'dct:rights'}) == set(dist.keys()).intersection(
                        {'dcat:accessURL', 'dct:rights'}):
                    licence = {
                        'licence_terms_url': [dist['dct:rights'].get('dct:RightsStatement', {}).get('@rdf:about')]}
                    if licence:
                        request_user = ELGUser.objects.get(username=settings.SYSTEM_USER)
                        licence_retrieved = retrieve_licence_by_licence_terms_and_name(licence, request_user)
                        if licence_retrieved:
                            clean_data = nested_delete(GenericLicenceTermsSerializer(licence_retrieved).data, 'pk')
                            licence_init = [clean_data]
                            break
        distribution_list = []
        for dist in distributions:
            dist = dist.get('dcat:Distribution', dist)
            access_loc = dist.get('dcat:accessURL')
            if access_loc and isinstance(access_loc, dict):
                access_location = access_loc['@rdf:resource']
            else:
                access_location = dist.get('dcat:accessURL', '')
            dl_loc = dist.get('dcat:downloadURL')
            if dl_loc and isinstance(dl_loc, dict):
                download_location = dl_loc.get('@rdf:resource')
            else:
                download_location = dist.get('dcat:downloadURL')
            licence_dist = self.map_generic_licences(dist)
            if not licence_dist:
                licence_dist = licence_init
            if type == 'software':
                distribution = {
                    'software_distribution_form': 'http://w3id.org/meta-share/meta-share/other',
                    'availability_start_date': self.fix_dates(metadata.get('dct:available')),
                    'distribution_rights_holder': self.map_contributor_type('duv:hasDistributor', metadata)
                }
                if not access_location:
                    if isinstance(metadata.get('foaf:page', {}), dict):
                        access_location = metadata.get('foaf:page', {})['@rdf:resource']
                    else:
                        landing_page = metadata.get('foaf:page', '')
                        if isinstance(landing_page, dict):
                            landing_page = landing_page['@rdf:resource']
                        elif isinstance(landing_page, list):
                            for l_p in landing_page:
                                if l_p['@rdf:resource'] == metadata.get('dct:identifier')['#text']:
                                    landing_page = str(l_p['@rdf:resource'])
                        access_location = landing_page
                distribution.update({'access_location': access_location})
                if download_location:
                    distribution.update({'download_location': quote(download_location).replace('%3A', ':')})
                    distribution.update({'private_resource': False})
            else:
                distribution = {
                    'dataset_distribution_form': 'http://w3id.org/meta-share/meta-share/downloadable',
                    'distribution_unspecified_feature': self.map_distribution_features('unspecified', dist),
                    'availability_start_date': self.fix_dates(metadata.get('dct:available')),
                    'distribution_rights_holder': self.map_contributor_type('duv:hasDistributor', metadata)
                }
                if not access_location:
                    if isinstance(metadata.get('foaf:page', {}), dict):
                        access_location = metadata.get('foaf:page', {})['@rdf:resource']
                    else:
                        landing_page = metadata.get('foaf:page', '')
                        if isinstance(landing_page, dict):
                            landing_page = landing_page['@rdf:resource']
                        elif isinstance(landing_page, list):
                            for l_p in landing_page:
                                if l_p['@rdf:resource'] == metadata.get('dct:identifier')['#text']:
                                    landing_page = str(l_p['@rdf:resource'])
                        access_location = landing_page
                distribution.update({'access_location': access_location})
                if download_location:
                    distribution.update({'download_location': quote(download_location).replace('%3A', ':')})
                    distribution.update({'private_resource': False})
                if len(distributions) > 1 \
                        and set(dist.keys()).union({'dcat:accessURL', 'dct:license'}) \
                        == set(dist.keys()).intersection({'dcat:accessURL', 'dct:license'}):
                    continue
            if licence_dist:
                distribution.update({'licence_terms': licence_dist})
            else:
                distribution.update({'access_rights': access_rights})
                distribution.update({'licence_terms': []})
            distribution_list.append(distribution)
        return distribution_list

    def map_ld_subclass(self, ld_type):
        ld_subclass = {}
        if ld_type == 'Model':
            ld_subclass = {
                'ld_subclass_type': 'Model',
                'model_function': ['http://w3id.org/meta-share/meta-share/unspecified']
            }
        elif ld_type == 'Grammar':
            ld_subclass = {
                'ld_subclass_type': 'Grammar',
                'encoding_level': 'http://w3id.org/meta-share/meta-share/unspecified'
            }
        return ld_subclass

    def map_api_version(self, record_json):
        v_pattern = re.compile(r'^(unspecified)?(([0-9]+\.)?([0-9]+\.)?[0-9]+?)?(1\.0\.0 \(automatically assigned\))?$')
        metadata = record_json.get('metadata', dict())
        record_version = metadata.get('relations', dict()).get('version')
        version = '1.0.0 (automatically assigned)'
        version_date = ''
        if metadata.get('publication_date'):
            version_date = self.fix_dates(metadata.get('publication_date'))
        if metadata.get('version'):
            version_api = re.sub('^[^0-9]+', '', metadata.get('version', ''))
            if isinstance(version_api, _ver.Version) or v_pattern.match(version_api):
                version = version_api
            else:
                version = str(record_version[0]['index']) + '.1'
        elif record_version:
            version = str(record_version[0]['index'] + 1)
        elif version_date:
            version = version_date.replace('-', '.')
        return version, version_date

    def map_version(self, metadata):
        v_pattern = re.compile(r'^(unspecified)?(([0-9]+\.)?([0-9]+\.)?[0-9]+?)?(1\.0\.0 \(automatically assigned\))?$')
        version = '1.0.0 (automatically assigned)'
        version_date = self.fix_dates(metadata.get('dct:issued', ''))
        if metadata.get('owl:versionInfo', ''):
            version_oai = re.sub('^[^0-9]+', '', metadata.get('owl:versionInfo', ''))
            version_oai = version_oai.replace('-', '.')
            if isinstance(version_oai, _ver.Version) or v_pattern.match(version_oai):
                version = version_oai
        else:
            lr_id = self.map_lr_identifier(metadata)[0].get('value', '').split('.')[-1]
            response = requests.get(f'https://zenodo.org/api/records/{lr_id}',
                                    params={'access_token': self.ACCESS_TOKEN})
            record_json = response.json()
            if 'message' in record_json.keys() and '100' in record_json['message']:
                print('Waiting 65 seconds to to avoid Zenodo API timeout')
                time.sleep(65)
                response = requests.get(f'https://zenodo.org/api/records/{lr_id}',
                                        params={'access_token': self.ACCESS_TOKEN})
                record_json = response.json()
            version, version_date = self.map_api_version(record_json)
        return version, version_date

    def map_geolocation(self, metadata):
        locations = metadata.get('dct:spatial', dict())
        if isinstance(locations, dict):
            if locations.get('dct:Location', {}).get('dct:geographicName'):
                return [{'en': locations.get('dct:Location', {}).get('dct:geographicName')}]
            else:
                return None
        else:
            loc = []
            for location in locations:
                if location.get('dct:Location', {}).get('dct:geographicName'):
                    loc.append(location.get('dct:Location', {}).get('dct:geographicName'))
            if loc:
                return loc
            else:
                return None

    def map_lr_subclass(self, metadata):
        lr_type = self.map_lr_type(metadata)

        lr_subclass = {}
        if self.entity_type == 'dataset':
            if lr_type == 'Corpus':
                lr_subclass.update({
                    'lr_type': lr_type,
                    'corpus_subclass': 'http://w3id.org/meta-share/meta-share/unspecified',
                    'unspecified_part': self.map_media_parts('unspecified'),
                    'dataset_distribution': self.map_distribution('dataset', metadata),
                    'requires_lr': self.map_related_ids('requiresLR', metadata),
                    'geographic_coverage': self.map_geolocation(metadata)
                })
            elif lr_type == 'LexicalConceptualResource':
                lr_subclass.update({
                    'lr_type': lr_type,
                    'encoding_level': ['http://w3id.org/meta-share/meta-share/unspecified'],
                    'unspecified_part': self.map_media_parts('unspecified'),
                    'dataset_distribution': self.map_distribution('dataset', metadata),
                    'requires_lr': self.map_related_ids('requiresLR', metadata),
                    'geographic_coverage': self.map_geolocation(metadata)
                })
            elif lr_type == 'Model':
                lr_subclass.update({
                    'lr_type': 'LanguageDescription',
                    'ld_subclass': 'http://w3id.org/meta-share/meta-share/model',
                    'unspecified_part': self.map_media_parts('unspecified'),
                    'dataset_distribution': self.map_distribution('dataset', metadata),
                    'requires_lr': self.map_related_ids('requiresLR', metadata),
                    'geographic_coverage': self.map_geolocation(metadata),
                    'language_description_subclass': self.map_ld_subclass(lr_type)
                })
            elif lr_type == 'Grammar':
                lr_subclass.update({
                    'lr_type': 'LanguageDescription',
                    'ld_subclass': 'http://w3id.org/meta-share/meta-share/grammar',
                    'unspecified_part': self.map_media_parts('unspecified'),
                    'dataset_distribution': self.map_distribution('dataset', metadata),
                    'requires_lr': self.map_related_ids('requiresLR', metadata),
                    'geographic_coverage': self.map_geolocation(metadata),
                    'language_description_subclass': self.map_ld_subclass(lr_type)
                })
        else:
            lr_subclass.update({
                'lr_type': 'ToolService',
                'function': ['http://w3id.org/meta-share/meta-share/unspecified'],
                'language_dependent': False,
                'input_content_resource': self.map_input_content_resource(),
                'software_distribution': self.map_distribution('software', metadata),
                'requires_lr': self.map_related_ids('requiresLR', metadata)
            })
        return lr_subclass

    def map_lr_identifier(self, metadata):
        lr_identifier_values = set()
        if metadata.get('dct:identifier', {}):
            lr_identifier_values.add(metadata.get('dct:identifier', {}).get('#text', ''))
        elif metadata.get('adms:identifier'):
            lr_identifier_values.add(metadata.get('adms:identifier', {}).get('#text', ''))
        elif metadata.get('adms:identifier'):
            lr_identifier_values.add(metadata.get('adms:Identifier', {}).get('#text', ''))
        if lr_identifier_values:
            lr_identifier = []
            for lr_id in lr_identifier_values:
                lr_identifier.append({'lr_identifier_scheme': 'http://purl.org/spar/datacite/doi',
                                      'value': lr_id})
            return lr_identifier

    def map_lr(self):
        metadata = self.data.get('rdf:Description', dict())
        resource_name = {'en': metadata.get('dct:title', '')}
        version, version_date = self.map_version(metadata)
        print('version', version)
        print('version_date', version_date)
        language_resource = {
            'entity_type': 'LanguageResource',
            'lr_identifier': self.map_lr_identifier(metadata),
            'resource_name': resource_name,
            'description': self.map_description(metadata),
            'version': version,
            'additional_info': self.get_additional_info(metadata),
            'contact': self.map_contributor_type('dcat:contactPoint', metadata),
            'keyword': self.map_keywords(metadata),
            'subject': self.map_subject(metadata),
            'resource_provider': self.map_contributor_type('dct:publisher', metadata),
            'publication_date': self.fix_dates(metadata.get('dct:issued', '')),
            'resource_creator': self.map_generic_person(metadata.get('dct:creator', [])),
            'creation_end_date': self.fix_dates(metadata.get('dct:created', '')),
            'funding_project': self.map_generic_proj(metadata.get('frapo:isFundedBy', [])),
            # 'has_original_source': [],
            'is_documented_by': self.map_related_ids('isDocumentedBy', metadata),
            'is_described_by': self.map_related_ids('isDescribedBy', metadata),
            'is_to_be_cited_by': self.map_related_ids('isToBeCitedBy', metadata),
            'is_reviewed_by': self.map_related_ids('isReviewedBy', metadata),
            'is_part_of': self.map_related_ids('isPartOf', metadata),
            'replaces': self.map_related_versions('replaces', version, metadata),
            'is_replaced_with': self.map_related_versions('is_replaced_with', version, metadata),
            'is_related_to_lr': self.map_related_ids('isRelatedToLR', metadata),
            'lr_subclass': self.map_lr_subclass(metadata)
        }
        if version_date:
            language_resource.update({'version_date': version_date})
        # add collected correctly
        return language_resource

    def fix_dates(self, date):
        if date:
            return_date = ''
            if isinstance(date, list):
                for _d in date:
                    if _d.get('@rdf:datatype') in ['http://www.w3.org/2001/XMLSchema#date',
                                                   'http://www.w3.org/2001/XMLSchema#dateTime']:
                        return_date = _d.get('#text').split('/')[-1].replace('.', '-')
                    elif _d.get('@rdf:datatype') == 'http://www.w3.org/2001/XMLSchema#gYear' and not return_date:
                        return_date = f"{_d.get('#text').split('/')[-1]}-01-01"
            elif isinstance(date, dict):
                if date.get('@rdf:datatype') in ['http://www.w3.org/2001/XMLSchema#date',
                                                 'http://www.w3.org/2001/XMLSchema#dateTime']:
                    return_date = date.get('#text').split('/')[-1].replace('.', '-')
                elif date.get('@rdf:datatype') == 'http://www.w3.org/2001/XMLSchema#gYear' and not return_date:
                    return_date = f"{date.get('#text').split('/')[-1]}-01-01"
            elif isinstance(date, str):
                return_date = date.replace('.', '-')
            return_date = return_date.replace('.', '-')
            return return_date

    def map_description(self, metadata):
        desc = metadata.get('dct:description')
        if isinstance(desc, list):
            description = desc[0]
            for i in range(1, len(desc)):
                description += '\n' + '<notes-element' + 'notes: ' + desc[i] + '</notes-element>'
        else:
            description = desc
        return {'en': description[:10000]}

    def map_keywords(self, metadata):
        kws = metadata.get('dcat:keyword', [])
        if not isinstance(kws, list):
            kws = [kws]
        keywords = []
        for kw in kws:
            if isinstance(kw, str) and len(kw) < 100:
                kw_list = kw.split(',')
                for i in kw_list:
                    if len(i) > 1:
                        keywords.append(i.strip())
            elif isinstance(kw, list):
                for i in kw:
                    keywords.append(i.strip())
        if not keywords:
            keywords.append(self.lr_type)
        kw_return = []
        kw_l = []
        for kw in keywords:
            if kw.lower() not in kw_l:
                kw_l.append(kw.lower())
                kw_return.append({'en': kw})
        return kw_return

    def map_subject(self, metadata):
        subjects = metadata.get('dct:subject', [])
        if not isinstance(subjects, list):
            subjects = [subjects]
        subject_list = []
        for _sub in subjects:
            if 'https://www.wikidata.org/wiki/' in _sub.get('@rdf:resource', ''):
                slug = _sub['@rdf:resource'].split('/')[-1]
                subject_id = {'subject_classification_scheme': 'http://w3id.org/meta-share/meta-share/unspecified',
                              'value': slug}
                try:
                    response = requests.get(f'https://www.wikidata.org/wiki/Special:EntityData/{slug}.json')
                    subject_json = response.json()
                    cat_label = subject_json.get('labels', {}).get('en', {}).get('value', '')
                except JSONDecodeError:
                    cat_label = ''
                if cat_label:
                    subject_list.append({
                        'category_label': {'en': cat_label},
                        'subject_identifier': subject_id
                    })
            elif 'https://id.loc.gov/authorities/subjects/' in _sub.get('@rdf:resource', ''):
                slug = _sub['@rdf:resource'].split('/')[-1].split('.')[0]
                try:
                    response = requests.get(f'https://id.loc.gov/authorities/subjects/{slug}.skos.json')
                    subject_json = response.json()
                except JSONDecodeError:
                    subject_json = []
                for _d in subject_json:
                    if isinstance(_d, dict) and "http://www.w3.org/2008/05/skos-xl#literalForm" in _d.keys():
                        sub_categories = _d["http://www.w3.org/2008/05/skos-xl#literalForm"][0].get('@value', '')
                        for _cat in sub_categories.split(','):
                            subject_id = {
                                'subject_classification_scheme': 'http://w3id.org/meta-share/meta-share/unspecified',
                                'value': slug}
                            cat_label = _cat.strip()
                            if cat_label:
                                subject_list.append({
                                    'category_label': {'en': cat_label},
                                    'subject_identifier': subject_id
                                })
            # TODO add at the end
            else:
                subject_list.append({
                    'category_label': {'en': 'Unknown Label'},
                })
        return subject_list

    def map_contributor_type(self, contribution, metadata):
        contributors = metadata.get(contribution, [])
        contributor_list = []
        if not isinstance(contributors, list):
            contributors = [contributors]
        for _con in contributors:
            contributor = _con.get('rdf:Description')
            if contributor and contributor.get('rdf:type', {}).get('@rdf:resource', '') in [
                'http://xmlns.com/foaf/0.1/Agent', 'http://www.w3.org/2006/vcard/ns#Individual']:
                gen_person = self.map_generic_person([_con])
                if gen_person:
                    contributor_list.extend(gen_person)
            else:
                org = _con.get('foaf:Agent') or _con.get('foaf:Organization')
                gen_org = self.map_generic_org([org])
                if gen_org:
                    contributor_list.extend(gen_org)
        return contributor_list

    def map_related_ids(self, relation, metadata):
        relations = {
            'Doc': {
                'isReviewedBy': {'schema:review': 'many'},
            },
            'LR': {
                'isPartOf': {'dct:isPartOf': 'many'},
                'replaces': {'dct:replaces': 'many'},
                'isRelatedToLR': {'dct:relation': 'many'},
                'requiresLR': {'dct:requires': 'many'},
            }
        }
        for _t in relations.keys():
            if _t == 'LR' and relation in relations[_t].keys():
                for k, v in relations[_t][relation].items():
                    relation_list = []
                    dcat_attr = metadata.get(k, [])
                    if not dcat_attr:
                        if v == 'many':
                            return relation_list
                        elif v == 'single':
                            return None
                    if isinstance(dcat_attr, dict):
                        check_lr = dcat_attr['@rdf:resource']
                        if relation == 'isPartOf' and any(['doi' not in check_lr, 'zenodo' in check_lr]):
                            break
                        gen_lr = self.map_generic_lr(check_lr)
                        if gen_lr and v == 'many':
                            relation_list.append(gen_lr)
                        elif v == 'single':
                            if gen_lr:
                                relation_list = gen_lr
                            else:
                                relation_list = None
                        return relation_list
                    elif isinstance(dcat_attr, list):
                        relation_list = []
                        for page in dcat_attr:
                            gen_lr = self.map_generic_lr(page['@rdf:resource'])
                            if gen_lr and v == 'many':
                                relation_list.append(gen_lr)
                            elif v == 'single':
                                if gen_lr:
                                    relation_list = gen_lr
                                else:
                                    relation_list = None
                        return relation_list
                    break
                break

    def map_related_versions(self, relation, version, metadata):
        relation_list = []
        concept_record = self.get_concept_record(metadata)
        date_issued = self.fix_dates(metadata.get('dct:issued', ''))
        lr_ids = self.map_lr_identifier(metadata)
        check_with = [lr_id.get('value', '') for lr_id in lr_ids if
                      lr_id.get('lr_identifier_scheme', '') == 'http://purl.org/spar/datacite/doi'][0]
        try:
            check_elg = ZenodoRecord.objects.get(zenodo_concept_record=concept_record)
            print('zenodo records', check_elg)
            print('elg_versions',
                  list(check_elg.metadata_record.all().values_list('described_entity__languageresource__version')))
            print('zenodo_record_version', version)
            for z_instance in check_elg.metadata_record.all():
                gen_lr = z_instance.described_entity
                lr_version = gen_lr.version
                lr_doi = \
                gen_lr.lr_identifier.filter(lr_identifier_scheme='http://purl.org/spar/datacite/doi').values_list(
                    'value', flat=True)[0]
                print(lr_version)
                initial_serializer = GenericLanguageResourceSerializer(instance=gen_lr)
                clean_lr = nested_delete(initial_serializer.data, 'pk')
                if gen_lr and relation == 'replaces':
                    if (str(gen_lr.publication_date) < date_issued
                        or _ver.parse(lr_version) < _ver.parse(version)
                        or (check_with and lr_doi and check_with.split('.')[-1] < lr_doi.split('.')[-1])) \
                            and clean_lr not in relation_list:
                        relation_list.append(clean_lr)
                if gen_lr and relation == 'is_replaced_with':
                    if (str(gen_lr.publication_date) > date_issued
                        or _ver.parse(lr_version) > _ver.parse(version)
                        or (check_with and lr_doi and check_with.split('.')[-1] < lr_doi.split('.')[-1])) \
                            and clean_lr not in relation_list:
                        relation_list.append(clean_lr)
        except ObjectDoesNotExist:
            return relation_list
        return relation_list

    def get_additional_info(self, metadata):
        return_list = []
        if isinstance(metadata.get('foaf:page'), dict):
            landing_page = metadata.get('foaf:page', {})['@rdf:resource']
        elif isinstance(metadata.get('foaf:page'), list):
            for l_p in metadata.get('foaf:page', []):
                if l_p['@rdf:resource'] == metadata.get('dct:identifier')['#text']:
                    landing_page = str(l_p['@rdf:resource'])
        else:
            landing_page = metadata.get('foaf:page', '')
        if landing_page:
            if 'zenodo.org' in landing_page:
                l_page = landing_page
            else:
                l_page = f"https://zenodo.org/record/{landing_page.split('.')[-1]}"
            return_list.append({'landing_page': l_page})
        return return_list

    def map_generic_lr(self, lr_identifier):
        lr_id = None
        if 'zenodo' in lr_identifier:
            lr_id = lr_identifier.split('.')[-1]
            concept_record_identifier = self.get_concept_record(self.data).split('.')[-1]
            if lr_id == concept_record_identifier:
                lr_id = None
        if lr_id:
            # TODO request only for doi relations, sta alla na valoume to onoma me vasi kati allo (isos to onoma tou resource + relation)
            try:
                response = requests.get(f'https://zenodo.org/api/records/{lr_id}',
                                        params={'access_token': self.ACCESS_TOKEN})
                lr_json = response.json()
                if 'message' in lr_json.keys() and '100' in lr_json['message']:
                    print('Waiting 65 seconds to to avoid Zenodo API timeout')
                    time.sleep(65)
                    response = requests.get(f'https://zenodo.org/api/records/{lr_id}',
                                            params={'access_token': self.ACCESS_TOKEN})
                    lr_json = response.json()
            except:
                lr_json = {}
            gen_metadata = lr_json.get('metadata')
            if lr_json and gen_metadata:
                gen_lr = {
                    'resource_name': {'en': lr_json['metadata'].get('title', '')},
                    'lr_identifier': [{'lr_identifier_scheme': 'http://purl.org/spar/datacite/doi',
                                       'value': lr_identifier}],
                }
                gen_lr.update()
                return gen_lr
        else:
            de = DescribedEntity.objects.filter(languageresource__resource_name__en__icontains='untitled').last()
            u_no = 0
            if de:
                untitled_no = de.resource_name['en'].split(' ')[-1]
                try:
                    u_no = int(untitled_no) + 1
                except ValueError:
                    pass
            gen_lr = {
                'resource_name': {'en': f'untitled harvested lr {u_no}'},
                'lr_identifier': [{'lr_identifier_scheme': 'http://purl.org/spar/datacite/doi',
                                   'value': lr_identifier}],
            }
            return gen_lr
        return None

    def map_generic_proj(self, projects):
        mapped_proj = []
        if projects:
            prj = self.data.get('foaf:Project')
        else:
            return mapped_proj
        if not isinstance(prj, list):
            prj = [prj]
        for proj in prj:
            proj_dict = {}
            project_identifier = []
            value = proj.get('dct:identifier', {}).get('#text', '')
            if value:
                project_identifier.append({
                    'project_identifier_scheme': 'http://w3id.org/meta-share/meta-share/OpenAIRE',
                    'value': value
                })
            proj_dict.update({
                'entity_type': 'Project',
                'project_name': {'en': proj.get('dct:title')},
                'project_identifier': project_identifier,
                'grant_number': value
            })
            funder = self.map_contributor_type('schema:funder', proj) \
                     or self.map_contributor_type('frapo:isAwardedBy', proj)
            if funder:
                proj_dict.update({'funder': funder})
                is_ec = False
                for _f in funder:
                    if _f.get('organization_name', '') == {'en': 'European Commission'}:
                        is_ec = True
                        break
                if is_ec:
                    proj_dict.update({'funding_type': ['http://w3id.org/meta-share/meta-share/euFunds']})
            mapped_proj.append(proj_dict)
        return mapped_proj

    def map_generic_org(self, organizations):
        mapped_orgs = []
        for org in organizations:
            organization_identifier = []
            if org.get('dct:identifier'):
                organization_identifier.append({'organization_identifier_scheme': 'http://purl.org/spar/datacite/doi',
                                                'value': org.get('dct:identifier', {}).get('#text')})
            if org.get('foaf:name'):
                org_name = org.get('foaf:name')
            if org.get('vcard:organization-name'):
                org_name = org.get('vcard:organization-name')
            mapped_orgs.append({
                'actor_type': 'Organization',
                'organization_name': {'en': org_name},
                'organization_identifier': organization_identifier
            })
        return mapped_orgs

    def get_concept_record(self, metadata):
        print('GETTING CR')
        dcat_attr = metadata.get('dct:isVersionOf', [])
        print(dcat_attr)
        if not dcat_attr:
            return ''
        if isinstance(dcat_attr, dict):
            return dcat_attr.get('@rdf:resource', '')
        elif isinstance(dcat_attr, list):
            for page in dcat_attr:
                return page.get('@rdf:resource', '')
        else:
            return ''

    def map_affiliations(self, organization):
        org = organization.get('foaf:Organization', {})
        if org:
            org_name = org.get('foaf:name')
            if org_name:
                return {'affiliated_organization': {
                    'actor_type': 'Organization',
                    'organization_name': {
                        'en': org_name
                    }
                }}

    def map_generic_person(self, people):
        mapped_people = []
        pattern = re.compile(r'^([\w\-.]+)(\s*(.+)\s*\b(\w+.+))?$')
        if not isinstance(people, list):
            people = [people]
        for i in people:
            i = i.get('rdf:Description', i)
            name = i.get('foaf:name') or i.get('vcard:fn')
            given_name = i.get('foaf:givenName') or i.get('vcard:given-name')
            surname = i.get('foaf:familyName') or i.get('vcard:family-name')
            if not (given_name and surname) and (name and pattern.findall(name)):
                (surname, _, _, given_name) = pattern.findall(name)[0]
            affiliations = i.get('org:memberOf', '')
            affiliation = []
            if affiliations:
                affiliation.append(
                    self.map_affiliations(affiliations)
                )
            personal_identifier = []
            # TODO xtipa to api me to orcid gia ton person
            if i.get('dct:identifier'):
                value = i.get('dct:identifier', {}).get('#text')
                personal_identifier.append({'personal_identifier_scheme': 'http://purl.org/spar/datacite/orcid',
                                            'value': value})
            append_dict = {
                'actor_type': 'Person',
                'given_name': {'en': given_name},
                'surname': {'en': surname},
                'personal_identifier': personal_identifier,
                'affiliation': affiliation
            }
            if given_name and surname:
                mapped_people.append(append_dict)
        return mapped_people

    def create_elg_record(self):
        try:
            curator = Person.objects.get(given_name__en='ELG', surname__en='SYSTEM')
            curator_serializer = GenericPersonSerializer(curator)
        except ObjectDoesNotExist:
            curator_serializer = GenericPersonSerializer(
                data={
                    'given_name': {'en': 'ELG'},
                    'surname': {'en': 'SYSTEM'}
                }
            )
            curator_serializer.is_valid()
            curator_serializer.save()

        try:
            repo = Repository.objects.get(repository_name__en='Zenodo')
            repository_serializer = GenericRepositorySerializer(repo)
        except ObjectDoesNotExist:
            repository_serializer = GenericRepositorySerializer(
                data={
                    'repository_name': {'en': 'Zenodo'}
                }
            )
            repository_serializer.is_valid()
            repository_serializer.save()
        time_it = time.time()
        described_entity = self.map_lr()
        time_it2 = time.time()
        print(f'LR MAPPING: {time_it2 - time_it}')
        time_it = time.time()
        data = dict(described_entity=described_entity)
        mdr_serializer = MetadataRecordSerializer(data=data,
                                                  context={'for_information_only': True}
                                                  )
        mdr_serializer.initial_data['metadata_curator'] = [curator_serializer.data]
        mdr_serializer.initial_data['source_of_metadata_record'] = repository_serializer.data
        mdr_serializer.initial_data['management_object'] = {}
        mdr_serializer.initial_data['management_object']['for_information_only'] = True
        if self.inferred:
            mdr_serializer.initial_data['management_object']['inferred_language'] = True
        valid_serializer, duplicates_found = harvested_record_serializer_validation(mdr_serializer)
        time_it2 = time.time()
        print(f'SERIALIZATION: {time_it2 - time_it}')
        time_it = time.time()
        if valid_serializer:
            if duplicates_found:
                mdr_serializer.context.update({'duplicates_found': True})
            instance = mdr_serializer.save()
            manager = instance.management_object
            manager.curator = ELGUser.objects.get(username='elg-system')
            manager.save()
            time_it2 = time.time()
            print(f'INSTANCE SAVE: {time_it2 - time_it}')
            if instance:
                zenodo_concept_record = self.get_concept_record(self.data.get('rdf:Description', dict()))
                if zenodo_concept_record:
                    zr, created = ZenodoRecord.objects.get_or_create(zenodo_concept_record=zenodo_concept_record)
                    if not created:
                        print('zenodo record found')
                        elg_z_r = zr.metadata_record.all().order_by('pk')
                        versions_to_remove = []
                        if elg_z_r.count() >= 10:
                            version_numbers = list(
                                elg_z_r.values_list('described_entity__languageresource__version', flat=True))
                            correct_order = sorted([_ver.parse(_v) for _v in version_numbers], reverse=True)[:9]
                            for i in elg_z_r:
                                if _ver.parse(i.described_entity.version) not in correct_order:
                                    versions_to_remove.append(i)
                            instance.management_object.versions_exceeding_limit = True
                            instance.management_object.save()
                        for record in versions_to_remove:
                            zr.metadata_record.remove(record)
                        if versions_to_remove:
                            MetadataDocument().catalogue_update(versions_to_remove, action='delete',
                                                                raise_on_error=True)
                            CatalogReportDocument().catalogue_report_update(versions_to_remove, action='delete',
                                                                            raise_on_error=True)
                        for _v in versions_to_remove:
                            _v.management_object.unpublish(unpublication_reason='too many zenodo versions')
                            _v.delete()
                    else:
                        print('zenodo record created')
                    zr.metadata_record.add(instance)
            time_it2 = time.time()
            print(f'REST: {time_it2 - time_it}')
            time_it = time.time()
            final_instance = MetadataRecord.objects.get(id=instance.id)
            final_instance.management_object.handle_version_publication([])
            time_it2 = time.time()
            print(f'VERSIONING: {time_it2 - time_it}')
            time_it = time.time()
            final_instance.management_object.publish(force=True)
            time_it2 = time.time()
            print(f'PUBLICATION: {time_it2 - time_it}')
            time_it = time.time()
            MetadataDocument().catalogue_update(final_instance)
            CatalogReportDocument().catalogue_report_update(final_instance.management_object)
            compute_dle_score.apply_async(args=[[final_instance.id]])
            print('created', instance)
            return instance
        else:
            mdr_serializer.is_valid(raise_exception=True)


class HarvestZenodo:

    def __init__(self, json_file=None, csv_file=None, *args, **kwargs):
        self.json_file = json_file
        self.csv_file = csv_file

    def initiate_zenodo_harvesting(self):
        response = requests.post(f'{settings.ZENODO_HARVESTING_SERVICE}/initiate/')
        if not response.status_code == 200:
            zenodo_harvesting_service_call_fail(response.status_code, response.json())
        return 'Zenodo harvesting initiated'

    def get_all_harvested_records(self):
        response = requests.post(f'{settings.ZENODO_HARVESTING_SERVICE}/retrieve/')
        # TODO fix
        return Response(response)

    def delete_all_harvested_records(self):
        response = requests.post(f'{settings.ZENODO_HARVESTING_SERVICE}/purge/')
        if response.status_code == 200:
            return 'Harvested files deleted'
        else:
            return response.json()

    def get_zenodo_data(self):
        if not (self.json_file or self.csv_file):
            return 'A json file of records and a csv file of selections are required'

        print('get_data')
        return_list = []
        print(self.csv_file)
        for row in self.csv_file:
            print(row.keys())
            break
        for row in self.csv_file:
            month_index = row['id_to_add'].split('_')
            #_month = month_index[0]
            _index = int(month_index[1])
            if row.get('final_prediction') == '1':
                lang = self.json_file[_index][1]
                if isinstance(lang, set):
                    lang_list = list(lang)
                else:
                    lang_list = lang
                return_list.append([self.json_file[_index][0][0], lang_list, row['final_class'], row['type']])
        print('got_data')
        return return_list

    def create_zenodo_records(self):
        errors = {}
        doc_list = self.get_zenodo_data()
        added = 0
        i = 0
        print('here')
        for doc in doc_list:
            i += 1
            LOGGER.info(f'creating record {i}/{len(doc_list)}')
            _mapping = ZenodoOAIMapping(doc)
            try:
                record, created, mapping_errors = _mapping.create_elg_record()
                if mapping_errors:
                    errors[f'record {i}'] = mapping_errors
            except ValidationError as err:
                created = False
                if not 'duplication_error' in err.__dict__['detail'].keys():
                    try:
                        errors[f'record {i}'].append(err)
                    except KeyError:
                        errors[f'record {i}'] = [err]
            if created:
                added += 1
        harvested_zenodo_record_update(added, errors)


@celery_app.task(name='zenodo_record_creation')
def zenodo_record_creation(json_file, csv_file):
    print(os.getcwd())
    LOGGER.info('celery_me')
    z_h = HarvestZenodo(json_file=json_file, csv_file=csv_file)
    z_h.create_zenodo_records()
    return 'Zenodo record creation initialized'