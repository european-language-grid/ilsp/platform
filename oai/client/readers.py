from lxml import etree
from oaipmh import common
from oaipmh.metadata import text_type, Error


class MetadataReader(object):
    """A default implementation of a reader based on fields.
    """

    def __init__(self, fields, namespaces=None):
        self._fields = fields
        self._namespaces = namespaces or {}

    def __call__(self, element):
        map = {}
        # create XPathEvaluator for this element
        xpath_evaluator = etree.XPathEvaluator(element,
                                               namespaces=self._namespaces)

        e = xpath_evaluator.evaluate
        # now extra field info according to xpath expr
        for field_name, (field_type, expr) in list(self._fields.items()):
            if field_type == 'bytes':
                value = str(e(expr))
            elif field_type == 'bytesList':
                value = [str(item) for item in e(expr)]
            elif field_type == 'text':
                # make sure we get back unicode strings instead
                # of lxml.etree._ElementUnicodeResult objects.
                value = text_type(e(expr))
            elif field_type == 'textList':
                # make sure we get back unicode strings instead
                # of lxml.etree._ElementUnicodeResult objects.
                value = [text_type(v) for v in e(expr)]
            elif field_type == 'object':
                value = e(expr)
            else:
                raise Error("Unknown field type: %s" % field_type)
            map[field_name] = value
        return common.Metadata(element, map)


elg_reader = MetadataReader(
    fields={
        'record': ('object', 'ms:MetadataRecord')
    },
    namespaces={
        'ms': 'http://w3id.org/meta-share/meta-share/',
        'None': 'http://w3id.org/meta-share/meta-share/'
    }
)
