from django import forms

from oai.models import IGNORE_LR_TYPES_CHOICES


class ProviderAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    ignore_lr_types = forms.MultipleChoiceField(choices=IGNORE_LR_TYPES_CHOICES, required=False)
    last_date_harvested = forms.DateField(required=False, disabled=True)


class FileImportForm(forms.Form):
    json_file = forms.FileField()
    csv_file = forms.FileField()
