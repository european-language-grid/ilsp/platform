import json
import os

import requests
from django.conf import settings
from django.contrib import admin
from registry.models import Repository
from . import models
from .forms import ProviderAdminForm, FileImportForm
import csv
import io
import logging
from django.contrib import (
    admin, messages
)
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse, path
from django.utils.safestring import mark_safe
from oai.zenodo_utils import HarvestZenodo, zenodo_record_creation

LOGGER = logging.getLogger(__name__)

admin.autodiscover()


class RepositoryInline(admin.StackedInline):
    model = Repository
    extra = 1


class ProviderAdmin(admin.ModelAdmin):
    form = ProviderAdminForm


class ZenodoHarvesting(models.ZenodoRecord):
    """
    A proxy model for Manager to handle Legal Validations' relevant fields
    """

    class Meta:
        proxy = True
        verbose_name = 'Zenodo Harvesting'
        verbose_name_plural = 'Zenodo Harvesting'

    def __str__(self):
        return self.metadata_record.all().first().__str__()

class ZenodoRecordAdmin(admin.ModelAdmin):

    change_list_template = 'admin/zenodo_oai/change_list.html'

    def has_add_permission(self, request, obj=None):
        return False

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('create-zenodo-records/', self.create_zenodo_records),
            path('delete-zenodo-files/', self.delete_zenodo_files),
            path('get-zenodo-files/', self.get_zenodo_files),
        ]
        return my_urls + urls

    def create_zenodo_records(self, request):
        if request.method == "POST":
            csv_file = request.FILES["csv_file"].temporary_file_path()
            json_file = request.FILES["json_file"].temporary_file_path()
            if not csv_file.endswith('.csv'):
                self.message_user(
                    request,
                    f'Please make sure you add a csv file',
                    messages.WARNING
                )
            if not json_file.endswith('.json'):
                self.message_user(
                    request,
                    f'Please make sure you add a json file',
                    messages.WARNING
                )
            json_file = request.FILES['json_file'].temporary_file_path()
            with open(json_file, 'r', encoding='UTF-8') as json_f:
                print('here')
                json_file = json.load(json_f)
            with open(csv_file, 'r', encoding='UTF-8') as csv_f:
                reader = csv.DictReader(csv_f, delimiter=',')
                csv_list = [row for row in reader]
            zenodo_record_creation.apply_async(args=[json_file, csv_list])
            self.message_user(
                request,
                f'Zenodo record curation initialized.',
                messages.SUCCESS
            )
            return HttpResponseRedirect(mark_safe(
                ".."
            ))
        form = FileImportForm()
        payload = {"form": form}
        return render(
            request, "admin/zenodo_oai/file_upload_form.html", payload
        )

    def delete_zenodo_files(self):
        pass

    def get_zenodo_files(self):
        pass


admin.site.register(models.Provider, ProviderAdmin)
admin.site.register(ZenodoHarvesting, ZenodoRecordAdmin)
