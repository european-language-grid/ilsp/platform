from rest_framework.exceptions import ValidationError
from registry.models import MetadataRecord
from django.db.models import Q
from packaging import version as _ver

from registry.search_indexes.documents import MetadataDocument


def harvested_record_serializer_validation(serializer):
    duplicates_found = False
    try:
        valid_serializer = serializer.is_valid()
    except ValidationError as err:
        if 'duplication_error' in err.__dict__['detail'].keys():
            serializer.context.update({'allow_duplicates': True})
            try:
                valid_serializer = serializer.is_valid(raise_exception=True)
                duplicates_found = True
            except ValidationError:
                valid_serializer = False
        else:
            valid_serializer = False
    return valid_serializer, duplicates_found


def harvesting_versions_of_existing_records_handling(instance):
    potential_duplicates = []
    instance_version = instance.described_entity.version
    instance_resource_name = instance.described_entity.resource_name
    existing_versions = MetadataRecord.objects.filter(
        described_entity__languageresource__resource_name=instance_resource_name).exclude(
        (Q(source_of_metadata_record=instance.source_of_metadata_record) | Q(pk=instance.pk)))

    if instance_version in ['unspecified', '1.0.0 (automatically assigned)']:
        specified_versions = existing_versions.exclude(
            described_entity__languageresource__version__in=['unspecified', '1.0.0 (automatically assigned)'])
        if specified_versions:
            potential_duplicates.extend([s_v for s_v in specified_versions])
    else:
        unspecified_versions = existing_versions.filter(
            described_entity__languageresource__version__in=['unspecified', '1.0.0 (automatically assigned)'])
        if unspecified_versions:
            potential_duplicates.extend([u_v for u_v in unspecified_versions])

    if potential_duplicates:
        instance.management_object.potential_duplicate = True
        for p_d in potential_duplicates:
            instance.described_entity.is_potential_duplicate_with.add(p_d.described_entity)
            p_d.described_entity.is_potential_duplicate_with.add(instance.described_entity)
            p_d.management_object.potential_duplicate = True
            p_d.management_object.save()


def harvesting_duplicate_handling(instance, check_landing_page=False):
    actual_duplicate = False
    potential_duplicate = False
    actual_duplicate_query = None
    potential_duplicate_query = None
    base_query = Q(Q(management_object__deleted=False) & ~Q(management_object__status='d'))
    lr_identifiers = []
    for lr_id in instance.described_entity.lr_identifier.all():
        if 'http' in lr_id.value and not 'https' in lr_id.value:
            lr_identifiers.append({'lr_identifier_scheme': lr_id.lr_identifier_scheme,
                                  'value': lr_id.value.replace('http', 'https')})
        elif 'https' in lr_id.value:
            lr_identifiers.append({'lr_identifier_scheme': lr_id.lr_identifier_scheme,
                                  'value': lr_id.value.replace('https', 'http')})
        lr_identifiers.append(lr_id)
    for lr_id in lr_identifiers:
        try:
            new_query = Q(Q(**{'described_entity__languageresource__lr_identifier__lr_identifier_scheme': lr_id.lr_identifier_scheme})
                          & Q(**{'described_entity__languageresource__lr_identifier__value': lr_id.value}))
        except AttributeError:
            new_query = Q(Q(**{'described_entity__languageresource__lr_identifier__lr_identifier_scheme': lr_id['lr_identifier_scheme']})
                          & Q(**{'described_entity__languageresource__lr_identifier__value': lr_id['value']}))
        if not actual_duplicate_query:
            actual_duplicate_query = new_query
        else:
            actual_duplicate_query = actual_duplicate_query | new_query
    landing_pages = []
    for a_i in instance.described_entity.additional_info.all():
        if 'http' in a_i.landing_page and not 'https' in a_i.landing_page:
            landing_pages.append({'landing_page': a_i.landing_page.replace('http', 'https')})
        elif 'https' in a_i.landing_page:
            landing_pages.append({'landing_page': a_i.landing_page.replace('https', 'http')})
        landing_pages.append(a_i)
    if check_landing_page:
        for a_i in landing_pages:
            try:
                new_query = Q(**{'described_entity__languageresource__additional_info__landing_page__icontains': a_i.landing_page.split('#.')[0]})
            except AttributeError:
                new_query = Q(**{'described_entity__languageresource__additional_info__landing_page__icontains': a_i['landing_page'].split('#.')[0]})
            if not actual_duplicate_query:
                actual_duplicate_query = new_query
            else:
                actual_duplicate_query = actual_duplicate_query | new_query
    actual_duplicate_query = actual_duplicate_query & base_query
    actual_duplicate_queryset = [md for md in MetadataRecord.objects.filter(
        actual_duplicate_query).distinct().exclude(pk=instance.pk)]
    if actual_duplicate_queryset:
        actual_duplicate = True

    new_query = Q(
        Q(**{'described_entity__languageresource__resource_name__en__iexact': instance.described_entity.resource_name['en']})
        & Q(**{'described_entity__languageresource__version': instance.described_entity.version}))
    if not potential_duplicate_query:
        potential_duplicate_query = new_query
    else:
        potential_duplicate_query = potential_duplicate_query | new_query

    if instance.described_entity.resource_short_name:
        new_query = Q(
            Q(**{
                'described_entity__languageresource__resource_short_name__en__iexact': instance.described_entity.resource_short_name['en']})
            & Q(**{'described_entity__languageresource__version': instance.described_entity.version}))
        if not potential_duplicate_query:
            potential_duplicate_query = new_query
        else:
            potential_duplicate_query = potential_duplicate_query | new_query
    potential_duplicate_query = potential_duplicate_query & base_query
    potential_duplicate_queryset = [md for md in MetadataRecord.objects.filter(
        potential_duplicate_query).distinct().exclude(pk=instance.pk)
                                    if not md in actual_duplicate_queryset]

    # taking care of duplicates/potential duplicates with versions of same record
    if actual_duplicate:
        for d in actual_duplicate_queryset:
            vers_of_d = [vd.metadata_record_of_manager for vd in
                         d.metadata_record_of_described_entity.management_object.versioned_record_managers.all()]
            for md_v in vers_of_d:
                if md_v in potential_duplicate_queryset:
                    potential_duplicate_queryset.remove(md_v)

    if potential_duplicate_queryset:
        potential_duplicate = True

    if actual_duplicate:
        instance.management_object.duplicate = True
        instance.management_object.save()
        for md in actual_duplicate_queryset:
            instance.described_entity.is_exact_match_with.add(md.described_entity)
            md.described_entity.is_exact_match_with.add(instance.described_entity)
            md.management_object.duplicate = True
            md.management_object.save()

    if potential_duplicate:
        instance.management_object.potential_duplicate = True
        instance.management_object.save()
        for md in potential_duplicate_queryset:
            instance.described_entity.is_potential_duplicate_with.add(md.described_entity)
            md.described_entity.is_potential_duplicate_with.add(instance.described_entity)
            md.management_object.potential_duplicate = True
            md.management_object.save()


def harvesting_version_and_duplicate_handling(instance):
    change = False
    versions = instance.management_object.versioned_record_managers.all()
    if versions.exists():
        is_dup = False
        is_p_dup = False
        for ver in versions:
            vers_of_ver = [v.metadata_record_of_manager for v in ver.versioned_record_managers.all()]
            for md_v in vers_of_ver:
                if instance.management_object.duplicate and \
                        md_v.described_entity.is_exact_match_with.filter(
                            metadata_record_of_described_entity__pk=instance.pk).exists():
                    is_dup = True
                    break
                if instance.management_object.potential_duplicate and \
                        md_v.described_entity.is_potential_duplicate_with.filter(
                            metadata_record_of_described_entity__pk=instance.pk).exists():
                    is_p_dup = True
                    break
            if is_dup or is_p_dup:
                change = True
                instance.management_object.versioned_record_managers.remove(ver)
                instance.described_entity.is_replaced_with.remove(ver.metadata_record_of_manager.described_entity)
                instance.described_entity.replaces.remove(ver.metadata_record_of_manager.described_entity)
                ver.generate_landing_page_display()
                MetadataDocument().catalogue_update(ver.metadata_record_of_manager)
        if change:
            versioned_managers = instance.management_object.versioned_record_managers.all()
            if versioned_managers.exists():
                s_l = sorted(list(versioned_managers.values_list(
                    'metadata_record_of_manager__described_entity__languageresource__version',
                    flat=True)),
                    reverse=True,
                    key=lambda x: _ver.parse(x))
                if _ver.parse(instance.described_entity.version) > _ver.parse(s_l[0]):
                    for _v in versioned_managers:
                        if _v.is_latest_version:
                            _v.is_latest_version = False
                            _v.save()
                            _v.generate_landing_page_display()
                            MetadataDocument().catalogue_update(_v.metadata_record_of_manager)
                    if not instance.management_object.is_latest_version:
                        instance.management_object.is_latest_version = True
                elif _ver.parse(instance.described_entity.version) < _ver.parse(s_l[0]):
                    if instance.management_object.is_latest_version:
                        instance.management_object.is_latest_version = False
            else:
                if not instance.management_object.is_latest_version:
                    instance.management_object.is_latest_version = True
            instance.management_object.save()
            instance.management_object.generate_landing_page_display()
            MetadataDocument().catalogue_update(ver.metadata_record_of_manager)
