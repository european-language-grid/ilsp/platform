data_template = {
    "source_of_metadata_record": {
        "repository_name": {"en": "Hugging Face"},
        "repository_url": "https://huggingface.co"
    },
    "source_metadata_record":
        {
            "metadata_record_identifier":
                {
                    "metadataRecordIdentifierScheme": "http://w3id.org/meta-share/meta-share/other",
                    "value": None
                }
        },
    "described_entity": {
        "entity_type": "LanguageResource",
        "resource_name": {"en": None},
        "description": {"en": None},
        "logo": None,
        "additional_info": [
            {
                "landing_page": "[https://huggingface.co/datasets]/[id]"
            }
        ],
        "keyword": [
            {"en": "corpus"},
            {"en": "[benchmark full split, task value, type value]"}
        ],
        "intended_application": "[[task_categories mapped in tasks], [task_ids if not 'other' mapped in tasks] ]",
        "is_to_be_cited_by": [],
        "lr_subclass": {
            "lr_type": "Corpus",
            "corpus_subclass": None,
            "corpus_media_part": [{
                "corpus_media_type": "CorpusTextPart",
                "media_type": "http://w3id.org/meta-share/meta-share/text",
                "linguality_type": None,
                "multilinguality_type": None,
                "language": [
                    {
                        "language_tag": None
                    }
                ]
            }],
            "dataset_distribution": [
                {
                    "dataset_distribution_form": "http://w3id.org/meta-share/meta-share/downloadable",
                    "access_location": "[https://huggingface.co/datasets]/[id]",
                    "private_resource": None,
                    "distribution_text_feature": [{
                        "size": [{
                            "amount": 0,
                            "size_unit": "http://w3id.org/meta-share/meta-share/unspecified"
                        }],
                        "data_format": "http://w3id.org/meta-share/omtd-share/unspecified"
                    }],
                    "licence_terms":
                        {
                            "licence_terms_name": None,
                            "licence_terms_url": "http://www.dummy.org",
                            "condition_of_use": "http://w3id.org/meta-share/meta-share/unspecified",
                            "licence_identifier": [
                                {
                                    "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/spdx",
                                    "value": None
                                }
                            ]
                        }
                }],
            "personal_data_included": False,
            "sensitive_data_included": False
        }
    }
}
