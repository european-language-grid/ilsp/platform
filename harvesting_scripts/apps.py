from django.apps import AppConfig


class AdHocHarvestConfig(AppConfig):
    name = 'harvesting_scripts'
